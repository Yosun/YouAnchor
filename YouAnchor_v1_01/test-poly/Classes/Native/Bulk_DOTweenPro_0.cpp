﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// DG.Tweening.Core.ABSAnimationComponent
struct ABSAnimationComponent_t262169234;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// DG.Tweening.DOTweenPath
struct DOTweenPath_t3192963685;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// DG.Tweening.Plugins.Core.PathCore.Path
struct Path_t3614338981;
// DG.Tweening.TweenCallback
struct TweenCallback_t3727756325;
// System.Collections.Generic.List`1<DG.Tweening.TweenCallback>
struct List_1_t904863771;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>
struct TweenerCore_3_t3040139253;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// System.String
struct String_t;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// DG.Tweening.Tween
struct Tween_t2342918553;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1068524471;
// DG.Tweening.DOTweenVisualManager
struct DOTweenVisualManager_t1560353112;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// DG.Tweening.TweenCallback[]
struct TweenCallbackU5BU5D_t96914856;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Void
struct Void_t1185182177;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// DG.Tweening.Core.DOTweenComponent
struct DOTweenComponent_t828035757;
// DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder
struct CatmullRomDecoder_t2053048079;
// DG.Tweening.Plugins.Core.PathCore.LinearDecoder
struct LinearDecoder_t2708327777;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// DG.Tweening.Plugins.Core.PathCore.ControlPoint[]
struct ControlPointU5BU5D_t1567961855;
// DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder
struct ABSPathDecoder_t2613982196;
// DG.Tweening.TweenCallback`1<System.Int32>
struct TweenCallback_1_t3009965658;
// DG.Tweening.EaseFunction
struct EaseFunction_t3531141372;
// System.Type
struct Type_t;
// DG.Tweening.Sequence
struct Sequence_t2050373119;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>
struct DOGetter_1_t3465109668;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>
struct DOSetter_1_t477454500;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>
struct ABSTweenPlugin_3_t2576148903;

extern RuntimeClass* DOTween_t2744875806_il2cpp_TypeInfo_var;
extern RuntimeClass* TweenCallback_t3727756325_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_get_Count_m576380744_RuntimeMethod_var;
extern const RuntimeMethod* Path_Draw_m8564983_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m2060438683_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisSpriteRenderer_t3235626157_m4281600501_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisRigidbody_t3916780224_m2140521468_RuntimeMethod_var;
extern const RuntimeMethod* Nullable_1__ctor_m133295941_RuntimeMethod_var;
extern const RuntimeMethod* TweenSettingsExtensions_SetDelay_TisTweenerCore_3_t3040139253_m1178560492_RuntimeMethod_var;
extern const RuntimeMethod* TweenSettingsExtensions_SetLoops_TisTweenerCore_3_t3040139253_m3803960963_RuntimeMethod_var;
extern const RuntimeMethod* TweenSettingsExtensions_SetAutoKill_TisTweenerCore_3_t3040139253_m1506332555_RuntimeMethod_var;
extern const RuntimeMethod* TweenSettingsExtensions_SetUpdate_TisTweenerCore_3_t3040139253_m2375151364_RuntimeMethod_var;
extern const RuntimeMethod* DOTweenPath_U3CAwakeU3Eb__38_0_m3881372633_RuntimeMethod_var;
extern const RuntimeMethod* TweenSettingsExtensions_OnKill_TisTweenerCore_3_t3040139253_m2092851215_RuntimeMethod_var;
extern const RuntimeMethod* TweenSettingsExtensions_SetSpeedBased_TisTweenerCore_3_t3040139253_m914173068_RuntimeMethod_var;
extern const RuntimeMethod* TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3040139253_m3071670034_RuntimeMethod_var;
extern const RuntimeMethod* TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3040139253_m2476880271_RuntimeMethod_var;
extern const RuntimeMethod* TweenSettingsExtensions_SetId_TisTweenerCore_3_t3040139253_m994168359_RuntimeMethod_var;
extern const RuntimeMethod* UnityEvent_Invoke_m3065672636_RuntimeMethod_var;
extern const RuntimeMethod* TweenSettingsExtensions_OnStart_TisTweenerCore_3_t3040139253_m2399400913_RuntimeMethod_var;
extern const RuntimeMethod* TweenSettingsExtensions_OnPlay_TisTweenerCore_3_t3040139253_m3727247487_RuntimeMethod_var;
extern const RuntimeMethod* TweenSettingsExtensions_OnUpdate_TisTweenerCore_3_t3040139253_m3414834117_RuntimeMethod_var;
extern const RuntimeMethod* TweenSettingsExtensions_OnStepComplete_TisTweenerCore_3_t3040139253_m2372572972_RuntimeMethod_var;
extern const RuntimeMethod* TweenSettingsExtensions_OnComplete_TisTweenerCore_3_t3040139253_m371879841_RuntimeMethod_var;
extern const RuntimeMethod* TweenSettingsExtensions_OnRewind_TisTweenerCore_3_t3040139253_m2866982129_RuntimeMethod_var;
extern const RuntimeMethod* TweenExtensions_Play_TisTweenerCore_3_t3040139253_m2536939817_RuntimeMethod_var;
extern const RuntimeMethod* TweenExtensions_Pause_TisTweenerCore_3_t3040139253_m1549830538_RuntimeMethod_var;
extern const uint32_t DOTweenPath_Awake_m2110691293_MetadataUsageId;
extern RuntimeClass* Path_t3614338981_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_ToArray_m466715028_RuntimeMethod_var;
extern const RuntimeMethod* Nullable_1__ctor_m2967195975_RuntimeMethod_var;
extern const uint32_t DOTweenPath_Reset_m1656620755_MetadataUsageId;
extern const RuntimeMethod* TweenExtensions_Play_TisTween_t2342918553_m1902296975_RuntimeMethod_var;
extern const uint32_t DOTweenPath_DOPlay_m1915979208_MetadataUsageId;
extern const RuntimeMethod* TweenExtensions_Pause_TisTween_t2342918553_m1759043349_RuntimeMethod_var;
extern const uint32_t DOTweenPath_DOPause_m2214012112_MetadataUsageId;
extern RuntimeClass* Debugger_t1756157868_il2cpp_TypeInfo_var;
extern const uint32_t DOTweenPath_DORestart_m3261849928_MetadataUsageId;
extern const uint32_t DOTweenPath_GetTween_m2361100768_MetadataUsageId;
extern String_t* _stringLiteral2966973123;
extern const uint32_t DOTweenPath_GetDrawPoints_m2910045167_MetadataUsageId;
extern RuntimeClass* Vector3U5BU5D_t1718750761_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1_get_Item_m200663048_RuntimeMethod_var;
extern const uint32_t DOTweenPath_GetFullWps_m4236143325_MetadataUsageId;
extern const uint32_t DOTweenPath_ReEvaluateRelativeTween_m1807793085_MetadataUsageId;
extern RuntimeClass* KeyframeU5BU5D_t1068524471_il2cpp_TypeInfo_var;
extern RuntimeClass* AnimationCurve_t3046754366_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t899420910_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m2503402603_RuntimeMethod_var;
extern String_t* _stringLiteral757602046;
extern const uint32_t DOTweenPath__ctor_m1702575186_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisABSAnimationComponent_t262169234_m2746596450_RuntimeMethod_var;
extern const uint32_t DOTweenVisualManager_Awake_m3981621639_MetadataUsageId;
extern const uint32_t DOTweenVisualManager_Update_m462811389_MetadataUsageId;
extern const uint32_t DOTweenVisualManager_OnEnable_m2832498305_MetadataUsageId;
extern const uint32_t DOTweenVisualManager_OnDisable_m2964460033_MetadataUsageId;

struct Vector3U5BU5D_t1718750761;
struct ControlPointU5BU5D_t1567961855;
struct KeyframeU5BU5D_t1068524471;


#ifndef U3CMODULEU3E_T692745556_H
#define U3CMODULEU3E_T692745556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745556 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745556_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef DEBUGGER_T1756157868_H
#define DEBUGGER_T1756157868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Debugger
struct  Debugger_t1756157868  : public RuntimeObject
{
public:

public:
};

struct Debugger_t1756157868_StaticFields
{
public:
	// System.Int32 DG.Tweening.Core.Debugger::logPriority
	int32_t ___logPriority_0;

public:
	inline static int32_t get_offset_of_logPriority_0() { return static_cast<int32_t>(offsetof(Debugger_t1756157868_StaticFields, ___logPriority_0)); }
	inline int32_t get_logPriority_0() const { return ___logPriority_0; }
	inline int32_t* get_address_of_logPriority_0() { return &___logPriority_0; }
	inline void set_logPriority_0(int32_t value)
	{
		___logPriority_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGGER_T1756157868_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef LIST_1_T904863771_H
#define LIST_1_T904863771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<DG.Tweening.TweenCallback>
struct  List_1_t904863771  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TweenCallbackU5BU5D_t96914856* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t904863771, ____items_1)); }
	inline TweenCallbackU5BU5D_t96914856* get__items_1() const { return ____items_1; }
	inline TweenCallbackU5BU5D_t96914856** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TweenCallbackU5BU5D_t96914856* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t904863771, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t904863771, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t904863771_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	TweenCallbackU5BU5D_t96914856* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t904863771_StaticFields, ___EmptyArray_4)); }
	inline TweenCallbackU5BU5D_t96914856* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline TweenCallbackU5BU5D_t96914856** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(TweenCallbackU5BU5D_t96914856* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T904863771_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef LIST_1_T899420910_H
#define LIST_1_T899420910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct  List_1_t899420910  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_t1718750761* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t899420910, ____items_1)); }
	inline Vector3U5BU5D_t1718750761* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_t1718750761** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_t1718750761* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t899420910, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t899420910, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t899420910_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	Vector3U5BU5D_t1718750761* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t899420910_StaticFields, ___EmptyArray_4)); }
	inline Vector3U5BU5D_t1718750761* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline Vector3U5BU5D_t1718750761** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(Vector3U5BU5D_t1718750761* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T899420910_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef UNITYEVENT_T2581268647_H
#define UNITYEVENT_T2581268647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t2581268647  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t2581268647, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T2581268647_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef KEYFRAME_T4206410242_H
#define KEYFRAME_T4206410242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Keyframe
struct  Keyframe_t4206410242 
{
public:
	// System.Single UnityEngine.Keyframe::m_Time
	float ___m_Time_0;
	// System.Single UnityEngine.Keyframe::m_Value
	float ___m_Value_1;
	// System.Single UnityEngine.Keyframe::m_InTangent
	float ___m_InTangent_2;
	// System.Single UnityEngine.Keyframe::m_OutTangent
	float ___m_OutTangent_3;

public:
	inline static int32_t get_offset_of_m_Time_0() { return static_cast<int32_t>(offsetof(Keyframe_t4206410242, ___m_Time_0)); }
	inline float get_m_Time_0() const { return ___m_Time_0; }
	inline float* get_address_of_m_Time_0() { return &___m_Time_0; }
	inline void set_m_Time_0(float value)
	{
		___m_Time_0 = value;
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(Keyframe_t4206410242, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_m_InTangent_2() { return static_cast<int32_t>(offsetof(Keyframe_t4206410242, ___m_InTangent_2)); }
	inline float get_m_InTangent_2() const { return ___m_InTangent_2; }
	inline float* get_address_of_m_InTangent_2() { return &___m_InTangent_2; }
	inline void set_m_InTangent_2(float value)
	{
		___m_InTangent_2 = value;
	}

	inline static int32_t get_offset_of_m_OutTangent_3() { return static_cast<int32_t>(offsetof(Keyframe_t4206410242, ___m_OutTangent_3)); }
	inline float get_m_OutTangent_3() const { return ___m_OutTangent_3; }
	inline float* get_address_of_m_OutTangent_3() { return &___m_OutTangent_3; }
	inline void set_m_OutTangent_3(float value)
	{
		___m_OutTangent_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYFRAME_T4206410242_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef ORIENTTYPE_T1731166963_H
#define ORIENTTYPE_T1731166963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.OrientType
struct  OrientType_t1731166963 
{
public:
	// System.Int32 DG.Tweening.Plugins.Options.OrientType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OrientType_t1731166963, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTTYPE_T1731166963_H
#ifndef CONTROLPOINT_T3892672090_H
#define CONTROLPOINT_T3892672090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.PathCore.ControlPoint
struct  ControlPoint_t3892672090 
{
public:
	// UnityEngine.Vector3 DG.Tweening.Plugins.Core.PathCore.ControlPoint::a
	Vector3_t3722313464  ___a_0;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Core.PathCore.ControlPoint::b
	Vector3_t3722313464  ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(ControlPoint_t3892672090, ___a_0)); }
	inline Vector3_t3722313464  get_a_0() const { return ___a_0; }
	inline Vector3_t3722313464 * get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Vector3_t3722313464  value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(ControlPoint_t3892672090, ___b_1)); }
	inline Vector3_t3722313464  get_b_1() const { return ___b_1; }
	inline Vector3_t3722313464 * get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(Vector3_t3722313464  value)
	{
		___b_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLPOINT_T3892672090_H
#ifndef SPECIALSTARTUPMODE_T1644068939_H
#define SPECIALSTARTUPMODE_T1644068939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Enums.SpecialStartupMode
struct  SpecialStartupMode_t1644068939 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.SpecialStartupMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SpecialStartupMode_t1644068939, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALSTARTUPMODE_T1644068939_H
#ifndef AUTOPLAY_T1346164433_H
#define AUTOPLAY_T1346164433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.AutoPlay
struct  AutoPlay_t1346164433 
{
public:
	// System.Int32 DG.Tweening.AutoPlay::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AutoPlay_t1346164433, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOPLAY_T1346164433_H
#ifndef LOGBEHAVIOUR_T1548882435_H
#define LOGBEHAVIOUR_T1548882435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LogBehaviour
struct  LogBehaviour_t1548882435 
{
public:
	// System.Int32 DG.Tweening.LogBehaviour::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogBehaviour_t1548882435, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGBEHAVIOUR_T1548882435_H
#ifndef HANDLESTYPE_T4074904290_H
#define HANDLESTYPE_T4074904290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.HandlesType
struct  HandlesType_t4074904290 
{
public:
	// System.Int32 DG.Tweening.HandlesType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HandlesType_t4074904290, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDLESTYPE_T4074904290_H
#ifndef NULLABLE_1_T4278248406_H
#define NULLABLE_1_T4278248406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Color>
struct  Nullable_1_t4278248406 
{
public:
	// T System.Nullable`1::value
	Color_t2555686324  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t4278248406, ___value_0)); }
	inline Color_t2555686324  get_value_0() const { return ___value_0; }
	inline Color_t2555686324 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Color_t2555686324  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t4278248406, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T4278248406_H
#ifndef HANDLESDRAWMODE_T2193450492_H
#define HANDLESDRAWMODE_T2193450492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.HandlesDrawMode
struct  HandlesDrawMode_t2193450492 
{
public:
	// System.Int32 DG.Tweening.HandlesDrawMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HandlesDrawMode_t2193450492, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDLESDRAWMODE_T2193450492_H
#ifndef TWEENTYPE_T1971673186_H
#define TWEENTYPE_T1971673186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.TweenType
struct  TweenType_t1971673186 
{
public:
	// System.Int32 DG.Tweening.TweenType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TweenType_t1971673186, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENTYPE_T1971673186_H
#ifndef PATHMODE_T2165603100_H
#define PATHMODE_T2165603100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.PathMode
struct  PathMode_t2165603100 
{
public:
	// System.Int32 DG.Tweening.PathMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PathMode_t2165603100, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHMODE_T2165603100_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef DOTWEENANIMATIONTYPE_T2748799855_H
#define DOTWEENANIMATIONTYPE_T2748799855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenAnimationType
struct  DOTweenAnimationType_t2748799855 
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenAnimationType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DOTweenAnimationType_t2748799855, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENANIMATIONTYPE_T2748799855_H
#ifndef PATHTYPE_T3777299409_H
#define PATHTYPE_T3777299409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.PathType
struct  PathType_t3777299409 
{
public:
	// System.Int32 DG.Tweening.PathType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PathType_t3777299409, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHTYPE_T3777299409_H
#ifndef DOTWEENINSPECTORMODE_T2656909913_H
#define DOTWEENINSPECTORMODE_T2656909913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenInspectorMode
struct  DOTweenInspectorMode_t2656909913 
{
public:
	// System.Int32 DG.Tweening.DOTweenInspectorMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DOTweenInspectorMode_t2656909913, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENINSPECTORMODE_T2656909913_H
#ifndef VISUALMANAGERPRESET_T2677960456_H
#define VISUALMANAGERPRESET_T2677960456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.VisualManagerPreset
struct  VisualManagerPreset_t2677960456 
{
public:
	// System.Int32 DG.Tweening.Core.VisualManagerPreset::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VisualManagerPreset_t2677960456, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISUALMANAGERPRESET_T2677960456_H
#ifndef TARGETTYPE_T3479356996_H
#define TARGETTYPE_T3479356996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.TargetType
struct  TargetType_t3479356996 
{
public:
	// System.Int32 DG.Tweening.Core.TargetType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TargetType_t3479356996, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETTYPE_T3479356996_H
#ifndef ONENABLEBEHAVIOUR_T3474514863_H
#define ONENABLEBEHAVIOUR_T3474514863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.OnEnableBehaviour
struct  OnEnableBehaviour_t3474514863 
{
public:
	// System.Int32 DG.Tweening.Core.OnEnableBehaviour::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OnEnableBehaviour_t3474514863, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONENABLEBEHAVIOUR_T3474514863_H
#ifndef ONDISABLEBEHAVIOUR_T979035984_H
#define ONDISABLEBEHAVIOUR_T979035984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.OnDisableBehaviour
struct  OnDisableBehaviour_t979035984 
{
public:
	// System.Int32 DG.Tweening.Core.OnDisableBehaviour::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OnDisableBehaviour_t979035984, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDISABLEBEHAVIOUR_T979035984_H
#ifndef LOOPTYPE_T3049802818_H
#define LOOPTYPE_T3049802818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LoopType
struct  LoopType_t3049802818 
{
public:
	// System.Int32 DG.Tweening.LoopType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoopType_t3049802818, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T3049802818_H
#ifndef ANIMATIONCURVE_T3046754366_H
#define ANIMATIONCURVE_T3046754366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationCurve
struct  AnimationCurve_t3046754366  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t3046754366, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // ANIMATIONCURVE_T3046754366_H
#ifndef NULLABLE_1_T1149908250_H
#define NULLABLE_1_T1149908250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Vector3>
struct  Nullable_1_t1149908250 
{
public:
	// T System.Nullable`1::value
	Vector3_t3722313464  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1149908250, ___value_0)); }
	inline Vector3_t3722313464  get_value_0() const { return ___value_0; }
	inline Vector3_t3722313464 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector3_t3722313464  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1149908250, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1149908250_H
#ifndef AXISCONSTRAINT_T2771958344_H
#define AXISCONSTRAINT_T2771958344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.AxisConstraint
struct  AxisConstraint_t2771958344 
{
public:
	// System.Int32 DG.Tweening.AxisConstraint::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisConstraint_t2771958344, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCONSTRAINT_T2771958344_H
#ifndef EASE_T4010715394_H
#define EASE_T4010715394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Ease
struct  Ease_t4010715394 
{
public:
	// System.Int32 DG.Tweening.Ease::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Ease_t4010715394, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASE_T4010715394_H
#ifndef UPDATETYPE_T3937729206_H
#define UPDATETYPE_T3937729206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.UpdateType
struct  UpdateType_t3937729206 
{
public:
	// System.Int32 DG.Tweening.UpdateType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdateType_t3937729206, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATETYPE_T3937729206_H
#ifndef PATHOPTIONS_T2074623791_H
#define PATHOPTIONS_T2074623791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.PathOptions
struct  PathOptions_t2074623791 
{
public:
	// DG.Tweening.PathMode DG.Tweening.Plugins.Options.PathOptions::mode
	int32_t ___mode_0;
	// DG.Tweening.Plugins.Options.OrientType DG.Tweening.Plugins.Options.PathOptions::orientType
	int32_t ___orientType_1;
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.PathOptions::lockPositionAxis
	int32_t ___lockPositionAxis_2;
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.PathOptions::lockRotationAxis
	int32_t ___lockRotationAxis_3;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::isClosedPath
	bool ___isClosedPath_4;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Options.PathOptions::lookAtPosition
	Vector3_t3722313464  ___lookAtPosition_5;
	// UnityEngine.Transform DG.Tweening.Plugins.Options.PathOptions::lookAtTransform
	Transform_t3600365921 * ___lookAtTransform_6;
	// System.Single DG.Tweening.Plugins.Options.PathOptions::lookAhead
	float ___lookAhead_7;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::hasCustomForwardDirection
	bool ___hasCustomForwardDirection_8;
	// UnityEngine.Quaternion DG.Tweening.Plugins.Options.PathOptions::forward
	Quaternion_t2301928331  ___forward_9;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::useLocalPosition
	bool ___useLocalPosition_10;
	// UnityEngine.Transform DG.Tweening.Plugins.Options.PathOptions::parent
	Transform_t3600365921 * ___parent_11;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::isRigidbody
	bool ___isRigidbody_12;
	// UnityEngine.Quaternion DG.Tweening.Plugins.Options.PathOptions::startupRot
	Quaternion_t2301928331  ___startupRot_13;
	// System.Single DG.Tweening.Plugins.Options.PathOptions::startupZRot
	float ___startupZRot_14;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_orientType_1() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___orientType_1)); }
	inline int32_t get_orientType_1() const { return ___orientType_1; }
	inline int32_t* get_address_of_orientType_1() { return &___orientType_1; }
	inline void set_orientType_1(int32_t value)
	{
		___orientType_1 = value;
	}

	inline static int32_t get_offset_of_lockPositionAxis_2() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___lockPositionAxis_2)); }
	inline int32_t get_lockPositionAxis_2() const { return ___lockPositionAxis_2; }
	inline int32_t* get_address_of_lockPositionAxis_2() { return &___lockPositionAxis_2; }
	inline void set_lockPositionAxis_2(int32_t value)
	{
		___lockPositionAxis_2 = value;
	}

	inline static int32_t get_offset_of_lockRotationAxis_3() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___lockRotationAxis_3)); }
	inline int32_t get_lockRotationAxis_3() const { return ___lockRotationAxis_3; }
	inline int32_t* get_address_of_lockRotationAxis_3() { return &___lockRotationAxis_3; }
	inline void set_lockRotationAxis_3(int32_t value)
	{
		___lockRotationAxis_3 = value;
	}

	inline static int32_t get_offset_of_isClosedPath_4() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___isClosedPath_4)); }
	inline bool get_isClosedPath_4() const { return ___isClosedPath_4; }
	inline bool* get_address_of_isClosedPath_4() { return &___isClosedPath_4; }
	inline void set_isClosedPath_4(bool value)
	{
		___isClosedPath_4 = value;
	}

	inline static int32_t get_offset_of_lookAtPosition_5() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___lookAtPosition_5)); }
	inline Vector3_t3722313464  get_lookAtPosition_5() const { return ___lookAtPosition_5; }
	inline Vector3_t3722313464 * get_address_of_lookAtPosition_5() { return &___lookAtPosition_5; }
	inline void set_lookAtPosition_5(Vector3_t3722313464  value)
	{
		___lookAtPosition_5 = value;
	}

	inline static int32_t get_offset_of_lookAtTransform_6() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___lookAtTransform_6)); }
	inline Transform_t3600365921 * get_lookAtTransform_6() const { return ___lookAtTransform_6; }
	inline Transform_t3600365921 ** get_address_of_lookAtTransform_6() { return &___lookAtTransform_6; }
	inline void set_lookAtTransform_6(Transform_t3600365921 * value)
	{
		___lookAtTransform_6 = value;
		Il2CppCodeGenWriteBarrier((&___lookAtTransform_6), value);
	}

	inline static int32_t get_offset_of_lookAhead_7() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___lookAhead_7)); }
	inline float get_lookAhead_7() const { return ___lookAhead_7; }
	inline float* get_address_of_lookAhead_7() { return &___lookAhead_7; }
	inline void set_lookAhead_7(float value)
	{
		___lookAhead_7 = value;
	}

	inline static int32_t get_offset_of_hasCustomForwardDirection_8() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___hasCustomForwardDirection_8)); }
	inline bool get_hasCustomForwardDirection_8() const { return ___hasCustomForwardDirection_8; }
	inline bool* get_address_of_hasCustomForwardDirection_8() { return &___hasCustomForwardDirection_8; }
	inline void set_hasCustomForwardDirection_8(bool value)
	{
		___hasCustomForwardDirection_8 = value;
	}

	inline static int32_t get_offset_of_forward_9() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___forward_9)); }
	inline Quaternion_t2301928331  get_forward_9() const { return ___forward_9; }
	inline Quaternion_t2301928331 * get_address_of_forward_9() { return &___forward_9; }
	inline void set_forward_9(Quaternion_t2301928331  value)
	{
		___forward_9 = value;
	}

	inline static int32_t get_offset_of_useLocalPosition_10() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___useLocalPosition_10)); }
	inline bool get_useLocalPosition_10() const { return ___useLocalPosition_10; }
	inline bool* get_address_of_useLocalPosition_10() { return &___useLocalPosition_10; }
	inline void set_useLocalPosition_10(bool value)
	{
		___useLocalPosition_10 = value;
	}

	inline static int32_t get_offset_of_parent_11() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___parent_11)); }
	inline Transform_t3600365921 * get_parent_11() const { return ___parent_11; }
	inline Transform_t3600365921 ** get_address_of_parent_11() { return &___parent_11; }
	inline void set_parent_11(Transform_t3600365921 * value)
	{
		___parent_11 = value;
		Il2CppCodeGenWriteBarrier((&___parent_11), value);
	}

	inline static int32_t get_offset_of_isRigidbody_12() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___isRigidbody_12)); }
	inline bool get_isRigidbody_12() const { return ___isRigidbody_12; }
	inline bool* get_address_of_isRigidbody_12() { return &___isRigidbody_12; }
	inline void set_isRigidbody_12(bool value)
	{
		___isRigidbody_12 = value;
	}

	inline static int32_t get_offset_of_startupRot_13() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___startupRot_13)); }
	inline Quaternion_t2301928331  get_startupRot_13() const { return ___startupRot_13; }
	inline Quaternion_t2301928331 * get_address_of_startupRot_13() { return &___startupRot_13; }
	inline void set_startupRot_13(Quaternion_t2301928331  value)
	{
		___startupRot_13 = value;
	}

	inline static int32_t get_offset_of_startupZRot_14() { return static_cast<int32_t>(offsetof(PathOptions_t2074623791, ___startupZRot_14)); }
	inline float get_startupZRot_14() const { return ___startupZRot_14; }
	inline float* get_address_of_startupZRot_14() { return &___startupZRot_14; }
	inline void set_startupZRot_14(float value)
	{
		___startupZRot_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.PathOptions
struct PathOptions_t2074623791_marshaled_pinvoke
{
	int32_t ___mode_0;
	int32_t ___orientType_1;
	int32_t ___lockPositionAxis_2;
	int32_t ___lockRotationAxis_3;
	int32_t ___isClosedPath_4;
	Vector3_t3722313464  ___lookAtPosition_5;
	Transform_t3600365921 * ___lookAtTransform_6;
	float ___lookAhead_7;
	int32_t ___hasCustomForwardDirection_8;
	Quaternion_t2301928331  ___forward_9;
	int32_t ___useLocalPosition_10;
	Transform_t3600365921 * ___parent_11;
	int32_t ___isRigidbody_12;
	Quaternion_t2301928331  ___startupRot_13;
	float ___startupZRot_14;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.PathOptions
struct PathOptions_t2074623791_marshaled_com
{
	int32_t ___mode_0;
	int32_t ___orientType_1;
	int32_t ___lockPositionAxis_2;
	int32_t ___lockRotationAxis_3;
	int32_t ___isClosedPath_4;
	Vector3_t3722313464  ___lookAtPosition_5;
	Transform_t3600365921 * ___lookAtTransform_6;
	float ___lookAhead_7;
	int32_t ___hasCustomForwardDirection_8;
	Quaternion_t2301928331  ___forward_9;
	int32_t ___useLocalPosition_10;
	Transform_t3600365921 * ___parent_11;
	int32_t ___isRigidbody_12;
	Quaternion_t2301928331  ___startupRot_13;
	float ___startupZRot_14;
};
#endif // PATHOPTIONS_T2074623791_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef DOTWEEN_T2744875806_H
#define DOTWEEN_T2744875806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTween
struct  DOTween_t2744875806  : public RuntimeObject
{
public:

public:
};

struct DOTween_t2744875806_StaticFields
{
public:
	// System.String DG.Tweening.DOTween::Version
	String_t* ___Version_0;
	// System.Boolean DG.Tweening.DOTween::useSafeMode
	bool ___useSafeMode_1;
	// System.Boolean DG.Tweening.DOTween::showUnityEditorReport
	bool ___showUnityEditorReport_2;
	// System.Single DG.Tweening.DOTween::timeScale
	float ___timeScale_3;
	// System.Boolean DG.Tweening.DOTween::useSmoothDeltaTime
	bool ___useSmoothDeltaTime_4;
	// System.Single DG.Tweening.DOTween::maxSmoothUnscaledTime
	float ___maxSmoothUnscaledTime_5;
	// DG.Tweening.LogBehaviour DG.Tweening.DOTween::_logBehaviour
	int32_t ____logBehaviour_6;
	// System.Boolean DG.Tweening.DOTween::drawGizmos
	bool ___drawGizmos_7;
	// DG.Tweening.UpdateType DG.Tweening.DOTween::defaultUpdateType
	int32_t ___defaultUpdateType_8;
	// System.Boolean DG.Tweening.DOTween::defaultTimeScaleIndependent
	bool ___defaultTimeScaleIndependent_9;
	// DG.Tweening.AutoPlay DG.Tweening.DOTween::defaultAutoPlay
	int32_t ___defaultAutoPlay_10;
	// System.Boolean DG.Tweening.DOTween::defaultAutoKill
	bool ___defaultAutoKill_11;
	// DG.Tweening.LoopType DG.Tweening.DOTween::defaultLoopType
	int32_t ___defaultLoopType_12;
	// System.Boolean DG.Tweening.DOTween::defaultRecyclable
	bool ___defaultRecyclable_13;
	// DG.Tweening.Ease DG.Tweening.DOTween::defaultEaseType
	int32_t ___defaultEaseType_14;
	// System.Single DG.Tweening.DOTween::defaultEaseOvershootOrAmplitude
	float ___defaultEaseOvershootOrAmplitude_15;
	// System.Single DG.Tweening.DOTween::defaultEasePeriod
	float ___defaultEasePeriod_16;
	// DG.Tweening.Core.DOTweenComponent DG.Tweening.DOTween::instance
	DOTweenComponent_t828035757 * ___instance_17;
	// System.Boolean DG.Tweening.DOTween::isUnityEditor
	bool ___isUnityEditor_18;
	// System.Boolean DG.Tweening.DOTween::isDebugBuild
	bool ___isDebugBuild_19;
	// System.Int32 DG.Tweening.DOTween::maxActiveTweenersReached
	int32_t ___maxActiveTweenersReached_20;
	// System.Int32 DG.Tweening.DOTween::maxActiveSequencesReached
	int32_t ___maxActiveSequencesReached_21;
	// System.Collections.Generic.List`1<DG.Tweening.TweenCallback> DG.Tweening.DOTween::GizmosDelegates
	List_1_t904863771 * ___GizmosDelegates_22;
	// System.Boolean DG.Tweening.DOTween::initialized
	bool ___initialized_23;
	// System.Boolean DG.Tweening.DOTween::isQuitting
	bool ___isQuitting_24;

public:
	inline static int32_t get_offset_of_Version_0() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___Version_0)); }
	inline String_t* get_Version_0() const { return ___Version_0; }
	inline String_t** get_address_of_Version_0() { return &___Version_0; }
	inline void set_Version_0(String_t* value)
	{
		___Version_0 = value;
		Il2CppCodeGenWriteBarrier((&___Version_0), value);
	}

	inline static int32_t get_offset_of_useSafeMode_1() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___useSafeMode_1)); }
	inline bool get_useSafeMode_1() const { return ___useSafeMode_1; }
	inline bool* get_address_of_useSafeMode_1() { return &___useSafeMode_1; }
	inline void set_useSafeMode_1(bool value)
	{
		___useSafeMode_1 = value;
	}

	inline static int32_t get_offset_of_showUnityEditorReport_2() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___showUnityEditorReport_2)); }
	inline bool get_showUnityEditorReport_2() const { return ___showUnityEditorReport_2; }
	inline bool* get_address_of_showUnityEditorReport_2() { return &___showUnityEditorReport_2; }
	inline void set_showUnityEditorReport_2(bool value)
	{
		___showUnityEditorReport_2 = value;
	}

	inline static int32_t get_offset_of_timeScale_3() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___timeScale_3)); }
	inline float get_timeScale_3() const { return ___timeScale_3; }
	inline float* get_address_of_timeScale_3() { return &___timeScale_3; }
	inline void set_timeScale_3(float value)
	{
		___timeScale_3 = value;
	}

	inline static int32_t get_offset_of_useSmoothDeltaTime_4() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___useSmoothDeltaTime_4)); }
	inline bool get_useSmoothDeltaTime_4() const { return ___useSmoothDeltaTime_4; }
	inline bool* get_address_of_useSmoothDeltaTime_4() { return &___useSmoothDeltaTime_4; }
	inline void set_useSmoothDeltaTime_4(bool value)
	{
		___useSmoothDeltaTime_4 = value;
	}

	inline static int32_t get_offset_of_maxSmoothUnscaledTime_5() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___maxSmoothUnscaledTime_5)); }
	inline float get_maxSmoothUnscaledTime_5() const { return ___maxSmoothUnscaledTime_5; }
	inline float* get_address_of_maxSmoothUnscaledTime_5() { return &___maxSmoothUnscaledTime_5; }
	inline void set_maxSmoothUnscaledTime_5(float value)
	{
		___maxSmoothUnscaledTime_5 = value;
	}

	inline static int32_t get_offset_of__logBehaviour_6() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ____logBehaviour_6)); }
	inline int32_t get__logBehaviour_6() const { return ____logBehaviour_6; }
	inline int32_t* get_address_of__logBehaviour_6() { return &____logBehaviour_6; }
	inline void set__logBehaviour_6(int32_t value)
	{
		____logBehaviour_6 = value;
	}

	inline static int32_t get_offset_of_drawGizmos_7() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___drawGizmos_7)); }
	inline bool get_drawGizmos_7() const { return ___drawGizmos_7; }
	inline bool* get_address_of_drawGizmos_7() { return &___drawGizmos_7; }
	inline void set_drawGizmos_7(bool value)
	{
		___drawGizmos_7 = value;
	}

	inline static int32_t get_offset_of_defaultUpdateType_8() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultUpdateType_8)); }
	inline int32_t get_defaultUpdateType_8() const { return ___defaultUpdateType_8; }
	inline int32_t* get_address_of_defaultUpdateType_8() { return &___defaultUpdateType_8; }
	inline void set_defaultUpdateType_8(int32_t value)
	{
		___defaultUpdateType_8 = value;
	}

	inline static int32_t get_offset_of_defaultTimeScaleIndependent_9() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultTimeScaleIndependent_9)); }
	inline bool get_defaultTimeScaleIndependent_9() const { return ___defaultTimeScaleIndependent_9; }
	inline bool* get_address_of_defaultTimeScaleIndependent_9() { return &___defaultTimeScaleIndependent_9; }
	inline void set_defaultTimeScaleIndependent_9(bool value)
	{
		___defaultTimeScaleIndependent_9 = value;
	}

	inline static int32_t get_offset_of_defaultAutoPlay_10() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultAutoPlay_10)); }
	inline int32_t get_defaultAutoPlay_10() const { return ___defaultAutoPlay_10; }
	inline int32_t* get_address_of_defaultAutoPlay_10() { return &___defaultAutoPlay_10; }
	inline void set_defaultAutoPlay_10(int32_t value)
	{
		___defaultAutoPlay_10 = value;
	}

	inline static int32_t get_offset_of_defaultAutoKill_11() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultAutoKill_11)); }
	inline bool get_defaultAutoKill_11() const { return ___defaultAutoKill_11; }
	inline bool* get_address_of_defaultAutoKill_11() { return &___defaultAutoKill_11; }
	inline void set_defaultAutoKill_11(bool value)
	{
		___defaultAutoKill_11 = value;
	}

	inline static int32_t get_offset_of_defaultLoopType_12() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultLoopType_12)); }
	inline int32_t get_defaultLoopType_12() const { return ___defaultLoopType_12; }
	inline int32_t* get_address_of_defaultLoopType_12() { return &___defaultLoopType_12; }
	inline void set_defaultLoopType_12(int32_t value)
	{
		___defaultLoopType_12 = value;
	}

	inline static int32_t get_offset_of_defaultRecyclable_13() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultRecyclable_13)); }
	inline bool get_defaultRecyclable_13() const { return ___defaultRecyclable_13; }
	inline bool* get_address_of_defaultRecyclable_13() { return &___defaultRecyclable_13; }
	inline void set_defaultRecyclable_13(bool value)
	{
		___defaultRecyclable_13 = value;
	}

	inline static int32_t get_offset_of_defaultEaseType_14() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultEaseType_14)); }
	inline int32_t get_defaultEaseType_14() const { return ___defaultEaseType_14; }
	inline int32_t* get_address_of_defaultEaseType_14() { return &___defaultEaseType_14; }
	inline void set_defaultEaseType_14(int32_t value)
	{
		___defaultEaseType_14 = value;
	}

	inline static int32_t get_offset_of_defaultEaseOvershootOrAmplitude_15() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultEaseOvershootOrAmplitude_15)); }
	inline float get_defaultEaseOvershootOrAmplitude_15() const { return ___defaultEaseOvershootOrAmplitude_15; }
	inline float* get_address_of_defaultEaseOvershootOrAmplitude_15() { return &___defaultEaseOvershootOrAmplitude_15; }
	inline void set_defaultEaseOvershootOrAmplitude_15(float value)
	{
		___defaultEaseOvershootOrAmplitude_15 = value;
	}

	inline static int32_t get_offset_of_defaultEasePeriod_16() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultEasePeriod_16)); }
	inline float get_defaultEasePeriod_16() const { return ___defaultEasePeriod_16; }
	inline float* get_address_of_defaultEasePeriod_16() { return &___defaultEasePeriod_16; }
	inline void set_defaultEasePeriod_16(float value)
	{
		___defaultEasePeriod_16 = value;
	}

	inline static int32_t get_offset_of_instance_17() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___instance_17)); }
	inline DOTweenComponent_t828035757 * get_instance_17() const { return ___instance_17; }
	inline DOTweenComponent_t828035757 ** get_address_of_instance_17() { return &___instance_17; }
	inline void set_instance_17(DOTweenComponent_t828035757 * value)
	{
		___instance_17 = value;
		Il2CppCodeGenWriteBarrier((&___instance_17), value);
	}

	inline static int32_t get_offset_of_isUnityEditor_18() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___isUnityEditor_18)); }
	inline bool get_isUnityEditor_18() const { return ___isUnityEditor_18; }
	inline bool* get_address_of_isUnityEditor_18() { return &___isUnityEditor_18; }
	inline void set_isUnityEditor_18(bool value)
	{
		___isUnityEditor_18 = value;
	}

	inline static int32_t get_offset_of_isDebugBuild_19() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___isDebugBuild_19)); }
	inline bool get_isDebugBuild_19() const { return ___isDebugBuild_19; }
	inline bool* get_address_of_isDebugBuild_19() { return &___isDebugBuild_19; }
	inline void set_isDebugBuild_19(bool value)
	{
		___isDebugBuild_19 = value;
	}

	inline static int32_t get_offset_of_maxActiveTweenersReached_20() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___maxActiveTweenersReached_20)); }
	inline int32_t get_maxActiveTweenersReached_20() const { return ___maxActiveTweenersReached_20; }
	inline int32_t* get_address_of_maxActiveTweenersReached_20() { return &___maxActiveTweenersReached_20; }
	inline void set_maxActiveTweenersReached_20(int32_t value)
	{
		___maxActiveTweenersReached_20 = value;
	}

	inline static int32_t get_offset_of_maxActiveSequencesReached_21() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___maxActiveSequencesReached_21)); }
	inline int32_t get_maxActiveSequencesReached_21() const { return ___maxActiveSequencesReached_21; }
	inline int32_t* get_address_of_maxActiveSequencesReached_21() { return &___maxActiveSequencesReached_21; }
	inline void set_maxActiveSequencesReached_21(int32_t value)
	{
		___maxActiveSequencesReached_21 = value;
	}

	inline static int32_t get_offset_of_GizmosDelegates_22() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___GizmosDelegates_22)); }
	inline List_1_t904863771 * get_GizmosDelegates_22() const { return ___GizmosDelegates_22; }
	inline List_1_t904863771 ** get_address_of_GizmosDelegates_22() { return &___GizmosDelegates_22; }
	inline void set_GizmosDelegates_22(List_1_t904863771 * value)
	{
		___GizmosDelegates_22 = value;
		Il2CppCodeGenWriteBarrier((&___GizmosDelegates_22), value);
	}

	inline static int32_t get_offset_of_initialized_23() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___initialized_23)); }
	inline bool get_initialized_23() const { return ___initialized_23; }
	inline bool* get_address_of_initialized_23() { return &___initialized_23; }
	inline void set_initialized_23(bool value)
	{
		___initialized_23 = value;
	}

	inline static int32_t get_offset_of_isQuitting_24() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___isQuitting_24)); }
	inline bool get_isQuitting_24() const { return ___isQuitting_24; }
	inline bool* get_address_of_isQuitting_24() { return &___isQuitting_24; }
	inline void set_isQuitting_24(bool value)
	{
		___isQuitting_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEEN_T2744875806_H
#ifndef ABSSEQUENTIABLE_T3376041011_H
#define ABSSEQUENTIABLE_T3376041011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.ABSSequentiable
struct  ABSSequentiable_t3376041011  : public RuntimeObject
{
public:
	// DG.Tweening.TweenType DG.Tweening.Core.ABSSequentiable::tweenType
	int32_t ___tweenType_0;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedPosition
	float ___sequencedPosition_1;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedEndPosition
	float ___sequencedEndPosition_2;
	// DG.Tweening.TweenCallback DG.Tweening.Core.ABSSequentiable::onStart
	TweenCallback_t3727756325 * ___onStart_3;

public:
	inline static int32_t get_offset_of_tweenType_0() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___tweenType_0)); }
	inline int32_t get_tweenType_0() const { return ___tweenType_0; }
	inline int32_t* get_address_of_tweenType_0() { return &___tweenType_0; }
	inline void set_tweenType_0(int32_t value)
	{
		___tweenType_0 = value;
	}

	inline static int32_t get_offset_of_sequencedPosition_1() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___sequencedPosition_1)); }
	inline float get_sequencedPosition_1() const { return ___sequencedPosition_1; }
	inline float* get_address_of_sequencedPosition_1() { return &___sequencedPosition_1; }
	inline void set_sequencedPosition_1(float value)
	{
		___sequencedPosition_1 = value;
	}

	inline static int32_t get_offset_of_sequencedEndPosition_2() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___sequencedEndPosition_2)); }
	inline float get_sequencedEndPosition_2() const { return ___sequencedEndPosition_2; }
	inline float* get_address_of_sequencedEndPosition_2() { return &___sequencedEndPosition_2; }
	inline void set_sequencedEndPosition_2(float value)
	{
		___sequencedEndPosition_2 = value;
	}

	inline static int32_t get_offset_of_onStart_3() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___onStart_3)); }
	inline TweenCallback_t3727756325 * get_onStart_3() const { return ___onStart_3; }
	inline TweenCallback_t3727756325 ** get_address_of_onStart_3() { return &___onStart_3; }
	inline void set_onStart_3(TweenCallback_t3727756325 * value)
	{
		___onStart_3 = value;
		Il2CppCodeGenWriteBarrier((&___onStart_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSSEQUENTIABLE_T3376041011_H
#ifndef PATH_T3614338981_H
#define PATH_T3614338981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.PathCore.Path
struct  Path_t3614338981  : public RuntimeObject
{
public:
	// System.Single[] DG.Tweening.Plugins.Core.PathCore.Path::wpLengths
	SingleU5BU5D_t1444911251* ___wpLengths_2;
	// DG.Tweening.PathType DG.Tweening.Plugins.Core.PathCore.Path::type
	int32_t ___type_3;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::subdivisionsXSegment
	int32_t ___subdivisionsXSegment_4;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::subdivisions
	int32_t ___subdivisions_5;
	// UnityEngine.Vector3[] DG.Tweening.Plugins.Core.PathCore.Path::wps
	Vector3U5BU5D_t1718750761* ___wps_6;
	// DG.Tweening.Plugins.Core.PathCore.ControlPoint[] DG.Tweening.Plugins.Core.PathCore.Path::controlPoints
	ControlPointU5BU5D_t1567961855* ___controlPoints_7;
	// System.Single DG.Tweening.Plugins.Core.PathCore.Path::length
	float ___length_8;
	// System.Boolean DG.Tweening.Plugins.Core.PathCore.Path::isFinalized
	bool ___isFinalized_9;
	// System.Single[] DG.Tweening.Plugins.Core.PathCore.Path::timesTable
	SingleU5BU5D_t1444911251* ___timesTable_10;
	// System.Single[] DG.Tweening.Plugins.Core.PathCore.Path::lengthsTable
	SingleU5BU5D_t1444911251* ___lengthsTable_11;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::linearWPIndex
	int32_t ___linearWPIndex_12;
	// DG.Tweening.Plugins.Core.PathCore.Path DG.Tweening.Plugins.Core.PathCore.Path::_incrementalClone
	Path_t3614338981 * ____incrementalClone_13;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::_incrementalIndex
	int32_t ____incrementalIndex_14;
	// DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder DG.Tweening.Plugins.Core.PathCore.Path::_decoder
	ABSPathDecoder_t2613982196 * ____decoder_15;
	// System.Boolean DG.Tweening.Plugins.Core.PathCore.Path::_changed
	bool ____changed_16;
	// UnityEngine.Vector3[] DG.Tweening.Plugins.Core.PathCore.Path::nonLinearDrawWps
	Vector3U5BU5D_t1718750761* ___nonLinearDrawWps_17;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Core.PathCore.Path::targetPosition
	Vector3_t3722313464  ___targetPosition_18;
	// System.Nullable`1<UnityEngine.Vector3> DG.Tweening.Plugins.Core.PathCore.Path::lookAtPosition
	Nullable_1_t1149908250  ___lookAtPosition_19;
	// UnityEngine.Color DG.Tweening.Plugins.Core.PathCore.Path::gizmoColor
	Color_t2555686324  ___gizmoColor_20;

public:
	inline static int32_t get_offset_of_wpLengths_2() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___wpLengths_2)); }
	inline SingleU5BU5D_t1444911251* get_wpLengths_2() const { return ___wpLengths_2; }
	inline SingleU5BU5D_t1444911251** get_address_of_wpLengths_2() { return &___wpLengths_2; }
	inline void set_wpLengths_2(SingleU5BU5D_t1444911251* value)
	{
		___wpLengths_2 = value;
		Il2CppCodeGenWriteBarrier((&___wpLengths_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_subdivisionsXSegment_4() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___subdivisionsXSegment_4)); }
	inline int32_t get_subdivisionsXSegment_4() const { return ___subdivisionsXSegment_4; }
	inline int32_t* get_address_of_subdivisionsXSegment_4() { return &___subdivisionsXSegment_4; }
	inline void set_subdivisionsXSegment_4(int32_t value)
	{
		___subdivisionsXSegment_4 = value;
	}

	inline static int32_t get_offset_of_subdivisions_5() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___subdivisions_5)); }
	inline int32_t get_subdivisions_5() const { return ___subdivisions_5; }
	inline int32_t* get_address_of_subdivisions_5() { return &___subdivisions_5; }
	inline void set_subdivisions_5(int32_t value)
	{
		___subdivisions_5 = value;
	}

	inline static int32_t get_offset_of_wps_6() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___wps_6)); }
	inline Vector3U5BU5D_t1718750761* get_wps_6() const { return ___wps_6; }
	inline Vector3U5BU5D_t1718750761** get_address_of_wps_6() { return &___wps_6; }
	inline void set_wps_6(Vector3U5BU5D_t1718750761* value)
	{
		___wps_6 = value;
		Il2CppCodeGenWriteBarrier((&___wps_6), value);
	}

	inline static int32_t get_offset_of_controlPoints_7() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___controlPoints_7)); }
	inline ControlPointU5BU5D_t1567961855* get_controlPoints_7() const { return ___controlPoints_7; }
	inline ControlPointU5BU5D_t1567961855** get_address_of_controlPoints_7() { return &___controlPoints_7; }
	inline void set_controlPoints_7(ControlPointU5BU5D_t1567961855* value)
	{
		___controlPoints_7 = value;
		Il2CppCodeGenWriteBarrier((&___controlPoints_7), value);
	}

	inline static int32_t get_offset_of_length_8() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___length_8)); }
	inline float get_length_8() const { return ___length_8; }
	inline float* get_address_of_length_8() { return &___length_8; }
	inline void set_length_8(float value)
	{
		___length_8 = value;
	}

	inline static int32_t get_offset_of_isFinalized_9() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___isFinalized_9)); }
	inline bool get_isFinalized_9() const { return ___isFinalized_9; }
	inline bool* get_address_of_isFinalized_9() { return &___isFinalized_9; }
	inline void set_isFinalized_9(bool value)
	{
		___isFinalized_9 = value;
	}

	inline static int32_t get_offset_of_timesTable_10() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___timesTable_10)); }
	inline SingleU5BU5D_t1444911251* get_timesTable_10() const { return ___timesTable_10; }
	inline SingleU5BU5D_t1444911251** get_address_of_timesTable_10() { return &___timesTable_10; }
	inline void set_timesTable_10(SingleU5BU5D_t1444911251* value)
	{
		___timesTable_10 = value;
		Il2CppCodeGenWriteBarrier((&___timesTable_10), value);
	}

	inline static int32_t get_offset_of_lengthsTable_11() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___lengthsTable_11)); }
	inline SingleU5BU5D_t1444911251* get_lengthsTable_11() const { return ___lengthsTable_11; }
	inline SingleU5BU5D_t1444911251** get_address_of_lengthsTable_11() { return &___lengthsTable_11; }
	inline void set_lengthsTable_11(SingleU5BU5D_t1444911251* value)
	{
		___lengthsTable_11 = value;
		Il2CppCodeGenWriteBarrier((&___lengthsTable_11), value);
	}

	inline static int32_t get_offset_of_linearWPIndex_12() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___linearWPIndex_12)); }
	inline int32_t get_linearWPIndex_12() const { return ___linearWPIndex_12; }
	inline int32_t* get_address_of_linearWPIndex_12() { return &___linearWPIndex_12; }
	inline void set_linearWPIndex_12(int32_t value)
	{
		___linearWPIndex_12 = value;
	}

	inline static int32_t get_offset_of__incrementalClone_13() { return static_cast<int32_t>(offsetof(Path_t3614338981, ____incrementalClone_13)); }
	inline Path_t3614338981 * get__incrementalClone_13() const { return ____incrementalClone_13; }
	inline Path_t3614338981 ** get_address_of__incrementalClone_13() { return &____incrementalClone_13; }
	inline void set__incrementalClone_13(Path_t3614338981 * value)
	{
		____incrementalClone_13 = value;
		Il2CppCodeGenWriteBarrier((&____incrementalClone_13), value);
	}

	inline static int32_t get_offset_of__incrementalIndex_14() { return static_cast<int32_t>(offsetof(Path_t3614338981, ____incrementalIndex_14)); }
	inline int32_t get__incrementalIndex_14() const { return ____incrementalIndex_14; }
	inline int32_t* get_address_of__incrementalIndex_14() { return &____incrementalIndex_14; }
	inline void set__incrementalIndex_14(int32_t value)
	{
		____incrementalIndex_14 = value;
	}

	inline static int32_t get_offset_of__decoder_15() { return static_cast<int32_t>(offsetof(Path_t3614338981, ____decoder_15)); }
	inline ABSPathDecoder_t2613982196 * get__decoder_15() const { return ____decoder_15; }
	inline ABSPathDecoder_t2613982196 ** get_address_of__decoder_15() { return &____decoder_15; }
	inline void set__decoder_15(ABSPathDecoder_t2613982196 * value)
	{
		____decoder_15 = value;
		Il2CppCodeGenWriteBarrier((&____decoder_15), value);
	}

	inline static int32_t get_offset_of__changed_16() { return static_cast<int32_t>(offsetof(Path_t3614338981, ____changed_16)); }
	inline bool get__changed_16() const { return ____changed_16; }
	inline bool* get_address_of__changed_16() { return &____changed_16; }
	inline void set__changed_16(bool value)
	{
		____changed_16 = value;
	}

	inline static int32_t get_offset_of_nonLinearDrawWps_17() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___nonLinearDrawWps_17)); }
	inline Vector3U5BU5D_t1718750761* get_nonLinearDrawWps_17() const { return ___nonLinearDrawWps_17; }
	inline Vector3U5BU5D_t1718750761** get_address_of_nonLinearDrawWps_17() { return &___nonLinearDrawWps_17; }
	inline void set_nonLinearDrawWps_17(Vector3U5BU5D_t1718750761* value)
	{
		___nonLinearDrawWps_17 = value;
		Il2CppCodeGenWriteBarrier((&___nonLinearDrawWps_17), value);
	}

	inline static int32_t get_offset_of_targetPosition_18() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___targetPosition_18)); }
	inline Vector3_t3722313464  get_targetPosition_18() const { return ___targetPosition_18; }
	inline Vector3_t3722313464 * get_address_of_targetPosition_18() { return &___targetPosition_18; }
	inline void set_targetPosition_18(Vector3_t3722313464  value)
	{
		___targetPosition_18 = value;
	}

	inline static int32_t get_offset_of_lookAtPosition_19() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___lookAtPosition_19)); }
	inline Nullable_1_t1149908250  get_lookAtPosition_19() const { return ___lookAtPosition_19; }
	inline Nullable_1_t1149908250 * get_address_of_lookAtPosition_19() { return &___lookAtPosition_19; }
	inline void set_lookAtPosition_19(Nullable_1_t1149908250  value)
	{
		___lookAtPosition_19 = value;
	}

	inline static int32_t get_offset_of_gizmoColor_20() { return static_cast<int32_t>(offsetof(Path_t3614338981, ___gizmoColor_20)); }
	inline Color_t2555686324  get_gizmoColor_20() const { return ___gizmoColor_20; }
	inline Color_t2555686324 * get_address_of_gizmoColor_20() { return &___gizmoColor_20; }
	inline void set_gizmoColor_20(Color_t2555686324  value)
	{
		___gizmoColor_20 = value;
	}
};

struct Path_t3614338981_StaticFields
{
public:
	// DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder DG.Tweening.Plugins.Core.PathCore.Path::_catmullRomDecoder
	CatmullRomDecoder_t2053048079 * ____catmullRomDecoder_0;
	// DG.Tweening.Plugins.Core.PathCore.LinearDecoder DG.Tweening.Plugins.Core.PathCore.Path::_linearDecoder
	LinearDecoder_t2708327777 * ____linearDecoder_1;

public:
	inline static int32_t get_offset_of__catmullRomDecoder_0() { return static_cast<int32_t>(offsetof(Path_t3614338981_StaticFields, ____catmullRomDecoder_0)); }
	inline CatmullRomDecoder_t2053048079 * get__catmullRomDecoder_0() const { return ____catmullRomDecoder_0; }
	inline CatmullRomDecoder_t2053048079 ** get_address_of__catmullRomDecoder_0() { return &____catmullRomDecoder_0; }
	inline void set__catmullRomDecoder_0(CatmullRomDecoder_t2053048079 * value)
	{
		____catmullRomDecoder_0 = value;
		Il2CppCodeGenWriteBarrier((&____catmullRomDecoder_0), value);
	}

	inline static int32_t get_offset_of__linearDecoder_1() { return static_cast<int32_t>(offsetof(Path_t3614338981_StaticFields, ____linearDecoder_1)); }
	inline LinearDecoder_t2708327777 * get__linearDecoder_1() const { return ____linearDecoder_1; }
	inline LinearDecoder_t2708327777 ** get_address_of__linearDecoder_1() { return &____linearDecoder_1; }
	inline void set__linearDecoder_1(LinearDecoder_t2708327777 * value)
	{
		____linearDecoder_1 = value;
		Il2CppCodeGenWriteBarrier((&____linearDecoder_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATH_T3614338981_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef RENDERER_T2627027031_H
#define RENDERER_T2627027031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t2627027031  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T2627027031_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef TWEEN_T2342918553_H
#define TWEEN_T2342918553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Tween
struct  Tween_t2342918553  : public ABSSequentiable_t3376041011
{
public:
	// System.Single DG.Tweening.Tween::timeScale
	float ___timeScale_4;
	// System.Boolean DG.Tweening.Tween::isBackwards
	bool ___isBackwards_5;
	// System.Object DG.Tweening.Tween::id
	RuntimeObject * ___id_6;
	// System.Object DG.Tweening.Tween::target
	RuntimeObject * ___target_7;
	// DG.Tweening.UpdateType DG.Tweening.Tween::updateType
	int32_t ___updateType_8;
	// System.Boolean DG.Tweening.Tween::isIndependentUpdate
	bool ___isIndependentUpdate_9;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPlay
	TweenCallback_t3727756325 * ___onPlay_10;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPause
	TweenCallback_t3727756325 * ___onPause_11;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onRewind
	TweenCallback_t3727756325 * ___onRewind_12;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onUpdate
	TweenCallback_t3727756325 * ___onUpdate_13;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onStepComplete
	TweenCallback_t3727756325 * ___onStepComplete_14;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onComplete
	TweenCallback_t3727756325 * ___onComplete_15;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onKill
	TweenCallback_t3727756325 * ___onKill_16;
	// DG.Tweening.TweenCallback`1<System.Int32> DG.Tweening.Tween::onWaypointChange
	TweenCallback_1_t3009965658 * ___onWaypointChange_17;
	// System.Boolean DG.Tweening.Tween::isFrom
	bool ___isFrom_18;
	// System.Boolean DG.Tweening.Tween::isBlendable
	bool ___isBlendable_19;
	// System.Boolean DG.Tweening.Tween::isRecyclable
	bool ___isRecyclable_20;
	// System.Boolean DG.Tweening.Tween::isSpeedBased
	bool ___isSpeedBased_21;
	// System.Boolean DG.Tweening.Tween::autoKill
	bool ___autoKill_22;
	// System.Single DG.Tweening.Tween::duration
	float ___duration_23;
	// System.Int32 DG.Tweening.Tween::loops
	int32_t ___loops_24;
	// DG.Tweening.LoopType DG.Tweening.Tween::loopType
	int32_t ___loopType_25;
	// System.Single DG.Tweening.Tween::delay
	float ___delay_26;
	// System.Boolean DG.Tweening.Tween::isRelative
	bool ___isRelative_27;
	// DG.Tweening.Ease DG.Tweening.Tween::easeType
	int32_t ___easeType_28;
	// DG.Tweening.EaseFunction DG.Tweening.Tween::customEase
	EaseFunction_t3531141372 * ___customEase_29;
	// System.Single DG.Tweening.Tween::easeOvershootOrAmplitude
	float ___easeOvershootOrAmplitude_30;
	// System.Single DG.Tweening.Tween::easePeriod
	float ___easePeriod_31;
	// System.Type DG.Tweening.Tween::typeofT1
	Type_t * ___typeofT1_32;
	// System.Type DG.Tweening.Tween::typeofT2
	Type_t * ___typeofT2_33;
	// System.Type DG.Tweening.Tween::typeofTPlugOptions
	Type_t * ___typeofTPlugOptions_34;
	// System.Boolean DG.Tweening.Tween::active
	bool ___active_35;
	// System.Boolean DG.Tweening.Tween::isSequenced
	bool ___isSequenced_36;
	// DG.Tweening.Sequence DG.Tweening.Tween::sequenceParent
	Sequence_t2050373119 * ___sequenceParent_37;
	// System.Int32 DG.Tweening.Tween::activeId
	int32_t ___activeId_38;
	// DG.Tweening.Core.Enums.SpecialStartupMode DG.Tweening.Tween::specialStartupMode
	int32_t ___specialStartupMode_39;
	// System.Boolean DG.Tweening.Tween::creationLocked
	bool ___creationLocked_40;
	// System.Boolean DG.Tweening.Tween::startupDone
	bool ___startupDone_41;
	// System.Boolean DG.Tweening.Tween::playedOnce
	bool ___playedOnce_42;
	// System.Single DG.Tweening.Tween::position
	float ___position_43;
	// System.Single DG.Tweening.Tween::fullDuration
	float ___fullDuration_44;
	// System.Int32 DG.Tweening.Tween::completedLoops
	int32_t ___completedLoops_45;
	// System.Boolean DG.Tweening.Tween::isPlaying
	bool ___isPlaying_46;
	// System.Boolean DG.Tweening.Tween::isComplete
	bool ___isComplete_47;
	// System.Single DG.Tweening.Tween::elapsedDelay
	float ___elapsedDelay_48;
	// System.Boolean DG.Tweening.Tween::delayComplete
	bool ___delayComplete_49;
	// System.Int32 DG.Tweening.Tween::miscInt
	int32_t ___miscInt_50;

public:
	inline static int32_t get_offset_of_timeScale_4() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___timeScale_4)); }
	inline float get_timeScale_4() const { return ___timeScale_4; }
	inline float* get_address_of_timeScale_4() { return &___timeScale_4; }
	inline void set_timeScale_4(float value)
	{
		___timeScale_4 = value;
	}

	inline static int32_t get_offset_of_isBackwards_5() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isBackwards_5)); }
	inline bool get_isBackwards_5() const { return ___isBackwards_5; }
	inline bool* get_address_of_isBackwards_5() { return &___isBackwards_5; }
	inline void set_isBackwards_5(bool value)
	{
		___isBackwards_5 = value;
	}

	inline static int32_t get_offset_of_id_6() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___id_6)); }
	inline RuntimeObject * get_id_6() const { return ___id_6; }
	inline RuntimeObject ** get_address_of_id_6() { return &___id_6; }
	inline void set_id_6(RuntimeObject * value)
	{
		___id_6 = value;
		Il2CppCodeGenWriteBarrier((&___id_6), value);
	}

	inline static int32_t get_offset_of_target_7() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___target_7)); }
	inline RuntimeObject * get_target_7() const { return ___target_7; }
	inline RuntimeObject ** get_address_of_target_7() { return &___target_7; }
	inline void set_target_7(RuntimeObject * value)
	{
		___target_7 = value;
		Il2CppCodeGenWriteBarrier((&___target_7), value);
	}

	inline static int32_t get_offset_of_updateType_8() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___updateType_8)); }
	inline int32_t get_updateType_8() const { return ___updateType_8; }
	inline int32_t* get_address_of_updateType_8() { return &___updateType_8; }
	inline void set_updateType_8(int32_t value)
	{
		___updateType_8 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_9() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isIndependentUpdate_9)); }
	inline bool get_isIndependentUpdate_9() const { return ___isIndependentUpdate_9; }
	inline bool* get_address_of_isIndependentUpdate_9() { return &___isIndependentUpdate_9; }
	inline void set_isIndependentUpdate_9(bool value)
	{
		___isIndependentUpdate_9 = value;
	}

	inline static int32_t get_offset_of_onPlay_10() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onPlay_10)); }
	inline TweenCallback_t3727756325 * get_onPlay_10() const { return ___onPlay_10; }
	inline TweenCallback_t3727756325 ** get_address_of_onPlay_10() { return &___onPlay_10; }
	inline void set_onPlay_10(TweenCallback_t3727756325 * value)
	{
		___onPlay_10 = value;
		Il2CppCodeGenWriteBarrier((&___onPlay_10), value);
	}

	inline static int32_t get_offset_of_onPause_11() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onPause_11)); }
	inline TweenCallback_t3727756325 * get_onPause_11() const { return ___onPause_11; }
	inline TweenCallback_t3727756325 ** get_address_of_onPause_11() { return &___onPause_11; }
	inline void set_onPause_11(TweenCallback_t3727756325 * value)
	{
		___onPause_11 = value;
		Il2CppCodeGenWriteBarrier((&___onPause_11), value);
	}

	inline static int32_t get_offset_of_onRewind_12() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onRewind_12)); }
	inline TweenCallback_t3727756325 * get_onRewind_12() const { return ___onRewind_12; }
	inline TweenCallback_t3727756325 ** get_address_of_onRewind_12() { return &___onRewind_12; }
	inline void set_onRewind_12(TweenCallback_t3727756325 * value)
	{
		___onRewind_12 = value;
		Il2CppCodeGenWriteBarrier((&___onRewind_12), value);
	}

	inline static int32_t get_offset_of_onUpdate_13() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onUpdate_13)); }
	inline TweenCallback_t3727756325 * get_onUpdate_13() const { return ___onUpdate_13; }
	inline TweenCallback_t3727756325 ** get_address_of_onUpdate_13() { return &___onUpdate_13; }
	inline void set_onUpdate_13(TweenCallback_t3727756325 * value)
	{
		___onUpdate_13 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdate_13), value);
	}

	inline static int32_t get_offset_of_onStepComplete_14() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onStepComplete_14)); }
	inline TweenCallback_t3727756325 * get_onStepComplete_14() const { return ___onStepComplete_14; }
	inline TweenCallback_t3727756325 ** get_address_of_onStepComplete_14() { return &___onStepComplete_14; }
	inline void set_onStepComplete_14(TweenCallback_t3727756325 * value)
	{
		___onStepComplete_14 = value;
		Il2CppCodeGenWriteBarrier((&___onStepComplete_14), value);
	}

	inline static int32_t get_offset_of_onComplete_15() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onComplete_15)); }
	inline TweenCallback_t3727756325 * get_onComplete_15() const { return ___onComplete_15; }
	inline TweenCallback_t3727756325 ** get_address_of_onComplete_15() { return &___onComplete_15; }
	inline void set_onComplete_15(TweenCallback_t3727756325 * value)
	{
		___onComplete_15 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_15), value);
	}

	inline static int32_t get_offset_of_onKill_16() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onKill_16)); }
	inline TweenCallback_t3727756325 * get_onKill_16() const { return ___onKill_16; }
	inline TweenCallback_t3727756325 ** get_address_of_onKill_16() { return &___onKill_16; }
	inline void set_onKill_16(TweenCallback_t3727756325 * value)
	{
		___onKill_16 = value;
		Il2CppCodeGenWriteBarrier((&___onKill_16), value);
	}

	inline static int32_t get_offset_of_onWaypointChange_17() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onWaypointChange_17)); }
	inline TweenCallback_1_t3009965658 * get_onWaypointChange_17() const { return ___onWaypointChange_17; }
	inline TweenCallback_1_t3009965658 ** get_address_of_onWaypointChange_17() { return &___onWaypointChange_17; }
	inline void set_onWaypointChange_17(TweenCallback_1_t3009965658 * value)
	{
		___onWaypointChange_17 = value;
		Il2CppCodeGenWriteBarrier((&___onWaypointChange_17), value);
	}

	inline static int32_t get_offset_of_isFrom_18() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isFrom_18)); }
	inline bool get_isFrom_18() const { return ___isFrom_18; }
	inline bool* get_address_of_isFrom_18() { return &___isFrom_18; }
	inline void set_isFrom_18(bool value)
	{
		___isFrom_18 = value;
	}

	inline static int32_t get_offset_of_isBlendable_19() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isBlendable_19)); }
	inline bool get_isBlendable_19() const { return ___isBlendable_19; }
	inline bool* get_address_of_isBlendable_19() { return &___isBlendable_19; }
	inline void set_isBlendable_19(bool value)
	{
		___isBlendable_19 = value;
	}

	inline static int32_t get_offset_of_isRecyclable_20() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isRecyclable_20)); }
	inline bool get_isRecyclable_20() const { return ___isRecyclable_20; }
	inline bool* get_address_of_isRecyclable_20() { return &___isRecyclable_20; }
	inline void set_isRecyclable_20(bool value)
	{
		___isRecyclable_20 = value;
	}

	inline static int32_t get_offset_of_isSpeedBased_21() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isSpeedBased_21)); }
	inline bool get_isSpeedBased_21() const { return ___isSpeedBased_21; }
	inline bool* get_address_of_isSpeedBased_21() { return &___isSpeedBased_21; }
	inline void set_isSpeedBased_21(bool value)
	{
		___isSpeedBased_21 = value;
	}

	inline static int32_t get_offset_of_autoKill_22() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___autoKill_22)); }
	inline bool get_autoKill_22() const { return ___autoKill_22; }
	inline bool* get_address_of_autoKill_22() { return &___autoKill_22; }
	inline void set_autoKill_22(bool value)
	{
		___autoKill_22 = value;
	}

	inline static int32_t get_offset_of_duration_23() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___duration_23)); }
	inline float get_duration_23() const { return ___duration_23; }
	inline float* get_address_of_duration_23() { return &___duration_23; }
	inline void set_duration_23(float value)
	{
		___duration_23 = value;
	}

	inline static int32_t get_offset_of_loops_24() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___loops_24)); }
	inline int32_t get_loops_24() const { return ___loops_24; }
	inline int32_t* get_address_of_loops_24() { return &___loops_24; }
	inline void set_loops_24(int32_t value)
	{
		___loops_24 = value;
	}

	inline static int32_t get_offset_of_loopType_25() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___loopType_25)); }
	inline int32_t get_loopType_25() const { return ___loopType_25; }
	inline int32_t* get_address_of_loopType_25() { return &___loopType_25; }
	inline void set_loopType_25(int32_t value)
	{
		___loopType_25 = value;
	}

	inline static int32_t get_offset_of_delay_26() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___delay_26)); }
	inline float get_delay_26() const { return ___delay_26; }
	inline float* get_address_of_delay_26() { return &___delay_26; }
	inline void set_delay_26(float value)
	{
		___delay_26 = value;
	}

	inline static int32_t get_offset_of_isRelative_27() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isRelative_27)); }
	inline bool get_isRelative_27() const { return ___isRelative_27; }
	inline bool* get_address_of_isRelative_27() { return &___isRelative_27; }
	inline void set_isRelative_27(bool value)
	{
		___isRelative_27 = value;
	}

	inline static int32_t get_offset_of_easeType_28() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___easeType_28)); }
	inline int32_t get_easeType_28() const { return ___easeType_28; }
	inline int32_t* get_address_of_easeType_28() { return &___easeType_28; }
	inline void set_easeType_28(int32_t value)
	{
		___easeType_28 = value;
	}

	inline static int32_t get_offset_of_customEase_29() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___customEase_29)); }
	inline EaseFunction_t3531141372 * get_customEase_29() const { return ___customEase_29; }
	inline EaseFunction_t3531141372 ** get_address_of_customEase_29() { return &___customEase_29; }
	inline void set_customEase_29(EaseFunction_t3531141372 * value)
	{
		___customEase_29 = value;
		Il2CppCodeGenWriteBarrier((&___customEase_29), value);
	}

	inline static int32_t get_offset_of_easeOvershootOrAmplitude_30() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___easeOvershootOrAmplitude_30)); }
	inline float get_easeOvershootOrAmplitude_30() const { return ___easeOvershootOrAmplitude_30; }
	inline float* get_address_of_easeOvershootOrAmplitude_30() { return &___easeOvershootOrAmplitude_30; }
	inline void set_easeOvershootOrAmplitude_30(float value)
	{
		___easeOvershootOrAmplitude_30 = value;
	}

	inline static int32_t get_offset_of_easePeriod_31() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___easePeriod_31)); }
	inline float get_easePeriod_31() const { return ___easePeriod_31; }
	inline float* get_address_of_easePeriod_31() { return &___easePeriod_31; }
	inline void set_easePeriod_31(float value)
	{
		___easePeriod_31 = value;
	}

	inline static int32_t get_offset_of_typeofT1_32() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___typeofT1_32)); }
	inline Type_t * get_typeofT1_32() const { return ___typeofT1_32; }
	inline Type_t ** get_address_of_typeofT1_32() { return &___typeofT1_32; }
	inline void set_typeofT1_32(Type_t * value)
	{
		___typeofT1_32 = value;
		Il2CppCodeGenWriteBarrier((&___typeofT1_32), value);
	}

	inline static int32_t get_offset_of_typeofT2_33() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___typeofT2_33)); }
	inline Type_t * get_typeofT2_33() const { return ___typeofT2_33; }
	inline Type_t ** get_address_of_typeofT2_33() { return &___typeofT2_33; }
	inline void set_typeofT2_33(Type_t * value)
	{
		___typeofT2_33 = value;
		Il2CppCodeGenWriteBarrier((&___typeofT2_33), value);
	}

	inline static int32_t get_offset_of_typeofTPlugOptions_34() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___typeofTPlugOptions_34)); }
	inline Type_t * get_typeofTPlugOptions_34() const { return ___typeofTPlugOptions_34; }
	inline Type_t ** get_address_of_typeofTPlugOptions_34() { return &___typeofTPlugOptions_34; }
	inline void set_typeofTPlugOptions_34(Type_t * value)
	{
		___typeofTPlugOptions_34 = value;
		Il2CppCodeGenWriteBarrier((&___typeofTPlugOptions_34), value);
	}

	inline static int32_t get_offset_of_active_35() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___active_35)); }
	inline bool get_active_35() const { return ___active_35; }
	inline bool* get_address_of_active_35() { return &___active_35; }
	inline void set_active_35(bool value)
	{
		___active_35 = value;
	}

	inline static int32_t get_offset_of_isSequenced_36() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isSequenced_36)); }
	inline bool get_isSequenced_36() const { return ___isSequenced_36; }
	inline bool* get_address_of_isSequenced_36() { return &___isSequenced_36; }
	inline void set_isSequenced_36(bool value)
	{
		___isSequenced_36 = value;
	}

	inline static int32_t get_offset_of_sequenceParent_37() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___sequenceParent_37)); }
	inline Sequence_t2050373119 * get_sequenceParent_37() const { return ___sequenceParent_37; }
	inline Sequence_t2050373119 ** get_address_of_sequenceParent_37() { return &___sequenceParent_37; }
	inline void set_sequenceParent_37(Sequence_t2050373119 * value)
	{
		___sequenceParent_37 = value;
		Il2CppCodeGenWriteBarrier((&___sequenceParent_37), value);
	}

	inline static int32_t get_offset_of_activeId_38() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___activeId_38)); }
	inline int32_t get_activeId_38() const { return ___activeId_38; }
	inline int32_t* get_address_of_activeId_38() { return &___activeId_38; }
	inline void set_activeId_38(int32_t value)
	{
		___activeId_38 = value;
	}

	inline static int32_t get_offset_of_specialStartupMode_39() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___specialStartupMode_39)); }
	inline int32_t get_specialStartupMode_39() const { return ___specialStartupMode_39; }
	inline int32_t* get_address_of_specialStartupMode_39() { return &___specialStartupMode_39; }
	inline void set_specialStartupMode_39(int32_t value)
	{
		___specialStartupMode_39 = value;
	}

	inline static int32_t get_offset_of_creationLocked_40() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___creationLocked_40)); }
	inline bool get_creationLocked_40() const { return ___creationLocked_40; }
	inline bool* get_address_of_creationLocked_40() { return &___creationLocked_40; }
	inline void set_creationLocked_40(bool value)
	{
		___creationLocked_40 = value;
	}

	inline static int32_t get_offset_of_startupDone_41() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___startupDone_41)); }
	inline bool get_startupDone_41() const { return ___startupDone_41; }
	inline bool* get_address_of_startupDone_41() { return &___startupDone_41; }
	inline void set_startupDone_41(bool value)
	{
		___startupDone_41 = value;
	}

	inline static int32_t get_offset_of_playedOnce_42() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___playedOnce_42)); }
	inline bool get_playedOnce_42() const { return ___playedOnce_42; }
	inline bool* get_address_of_playedOnce_42() { return &___playedOnce_42; }
	inline void set_playedOnce_42(bool value)
	{
		___playedOnce_42 = value;
	}

	inline static int32_t get_offset_of_position_43() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___position_43)); }
	inline float get_position_43() const { return ___position_43; }
	inline float* get_address_of_position_43() { return &___position_43; }
	inline void set_position_43(float value)
	{
		___position_43 = value;
	}

	inline static int32_t get_offset_of_fullDuration_44() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___fullDuration_44)); }
	inline float get_fullDuration_44() const { return ___fullDuration_44; }
	inline float* get_address_of_fullDuration_44() { return &___fullDuration_44; }
	inline void set_fullDuration_44(float value)
	{
		___fullDuration_44 = value;
	}

	inline static int32_t get_offset_of_completedLoops_45() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___completedLoops_45)); }
	inline int32_t get_completedLoops_45() const { return ___completedLoops_45; }
	inline int32_t* get_address_of_completedLoops_45() { return &___completedLoops_45; }
	inline void set_completedLoops_45(int32_t value)
	{
		___completedLoops_45 = value;
	}

	inline static int32_t get_offset_of_isPlaying_46() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isPlaying_46)); }
	inline bool get_isPlaying_46() const { return ___isPlaying_46; }
	inline bool* get_address_of_isPlaying_46() { return &___isPlaying_46; }
	inline void set_isPlaying_46(bool value)
	{
		___isPlaying_46 = value;
	}

	inline static int32_t get_offset_of_isComplete_47() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isComplete_47)); }
	inline bool get_isComplete_47() const { return ___isComplete_47; }
	inline bool* get_address_of_isComplete_47() { return &___isComplete_47; }
	inline void set_isComplete_47(bool value)
	{
		___isComplete_47 = value;
	}

	inline static int32_t get_offset_of_elapsedDelay_48() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___elapsedDelay_48)); }
	inline float get_elapsedDelay_48() const { return ___elapsedDelay_48; }
	inline float* get_address_of_elapsedDelay_48() { return &___elapsedDelay_48; }
	inline void set_elapsedDelay_48(float value)
	{
		___elapsedDelay_48 = value;
	}

	inline static int32_t get_offset_of_delayComplete_49() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___delayComplete_49)); }
	inline bool get_delayComplete_49() const { return ___delayComplete_49; }
	inline bool* get_address_of_delayComplete_49() { return &___delayComplete_49; }
	inline void set_delayComplete_49(bool value)
	{
		___delayComplete_49 = value;
	}

	inline static int32_t get_offset_of_miscInt_50() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___miscInt_50)); }
	inline int32_t get_miscInt_50() const { return ___miscInt_50; }
	inline int32_t* get_address_of_miscInt_50() { return &___miscInt_50; }
	inline void set_miscInt_50(int32_t value)
	{
		___miscInt_50 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEEN_T2342918553_H
#ifndef TWEENCALLBACK_T3727756325_H
#define TWEENCALLBACK_T3727756325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.TweenCallback
struct  TweenCallback_t3727756325  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENCALLBACK_T3727756325_H
#ifndef RIGIDBODY_T3916780224_H
#define RIGIDBODY_T3916780224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody
struct  Rigidbody_t3916780224  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY_T3916780224_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef TWEENER_T436044680_H
#define TWEENER_T436044680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Tweener
struct  Tweener_t436044680  : public Tween_t2342918553
{
public:
	// System.Boolean DG.Tweening.Tweener::hasManuallySetStartValue
	bool ___hasManuallySetStartValue_51;
	// System.Boolean DG.Tweening.Tweener::isFromAllowed
	bool ___isFromAllowed_52;

public:
	inline static int32_t get_offset_of_hasManuallySetStartValue_51() { return static_cast<int32_t>(offsetof(Tweener_t436044680, ___hasManuallySetStartValue_51)); }
	inline bool get_hasManuallySetStartValue_51() const { return ___hasManuallySetStartValue_51; }
	inline bool* get_address_of_hasManuallySetStartValue_51() { return &___hasManuallySetStartValue_51; }
	inline void set_hasManuallySetStartValue_51(bool value)
	{
		___hasManuallySetStartValue_51 = value;
	}

	inline static int32_t get_offset_of_isFromAllowed_52() { return static_cast<int32_t>(offsetof(Tweener_t436044680, ___isFromAllowed_52)); }
	inline bool get_isFromAllowed_52() const { return ___isFromAllowed_52; }
	inline bool* get_address_of_isFromAllowed_52() { return &___isFromAllowed_52; }
	inline void set_isFromAllowed_52(bool value)
	{
		___isFromAllowed_52 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENER_T436044680_H
#ifndef SPRITERENDERER_T3235626157_H
#define SPRITERENDERER_T3235626157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpriteRenderer
struct  SpriteRenderer_t3235626157  : public Renderer_t2627027031
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITERENDERER_T3235626157_H
#ifndef ABSANIMATIONCOMPONENT_T262169234_H
#define ABSANIMATIONCOMPONENT_T262169234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.ABSAnimationComponent
struct  ABSAnimationComponent_t262169234  : public MonoBehaviour_t3962482529
{
public:
	// DG.Tweening.UpdateType DG.Tweening.Core.ABSAnimationComponent::updateType
	int32_t ___updateType_2;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::isSpeedBased
	bool ___isSpeedBased_3;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnStart
	bool ___hasOnStart_4;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnPlay
	bool ___hasOnPlay_5;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnUpdate
	bool ___hasOnUpdate_6;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnStepComplete
	bool ___hasOnStepComplete_7;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnComplete
	bool ___hasOnComplete_8;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnTweenCreated
	bool ___hasOnTweenCreated_9;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnRewind
	bool ___hasOnRewind_10;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onStart
	UnityEvent_t2581268647 * ___onStart_11;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onPlay
	UnityEvent_t2581268647 * ___onPlay_12;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onUpdate
	UnityEvent_t2581268647 * ___onUpdate_13;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onStepComplete
	UnityEvent_t2581268647 * ___onStepComplete_14;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onComplete
	UnityEvent_t2581268647 * ___onComplete_15;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onTweenCreated
	UnityEvent_t2581268647 * ___onTweenCreated_16;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onRewind
	UnityEvent_t2581268647 * ___onRewind_17;
	// DG.Tweening.Tween DG.Tweening.Core.ABSAnimationComponent::tween
	Tween_t2342918553 * ___tween_18;

public:
	inline static int32_t get_offset_of_updateType_2() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___updateType_2)); }
	inline int32_t get_updateType_2() const { return ___updateType_2; }
	inline int32_t* get_address_of_updateType_2() { return &___updateType_2; }
	inline void set_updateType_2(int32_t value)
	{
		___updateType_2 = value;
	}

	inline static int32_t get_offset_of_isSpeedBased_3() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___isSpeedBased_3)); }
	inline bool get_isSpeedBased_3() const { return ___isSpeedBased_3; }
	inline bool* get_address_of_isSpeedBased_3() { return &___isSpeedBased_3; }
	inline void set_isSpeedBased_3(bool value)
	{
		___isSpeedBased_3 = value;
	}

	inline static int32_t get_offset_of_hasOnStart_4() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnStart_4)); }
	inline bool get_hasOnStart_4() const { return ___hasOnStart_4; }
	inline bool* get_address_of_hasOnStart_4() { return &___hasOnStart_4; }
	inline void set_hasOnStart_4(bool value)
	{
		___hasOnStart_4 = value;
	}

	inline static int32_t get_offset_of_hasOnPlay_5() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnPlay_5)); }
	inline bool get_hasOnPlay_5() const { return ___hasOnPlay_5; }
	inline bool* get_address_of_hasOnPlay_5() { return &___hasOnPlay_5; }
	inline void set_hasOnPlay_5(bool value)
	{
		___hasOnPlay_5 = value;
	}

	inline static int32_t get_offset_of_hasOnUpdate_6() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnUpdate_6)); }
	inline bool get_hasOnUpdate_6() const { return ___hasOnUpdate_6; }
	inline bool* get_address_of_hasOnUpdate_6() { return &___hasOnUpdate_6; }
	inline void set_hasOnUpdate_6(bool value)
	{
		___hasOnUpdate_6 = value;
	}

	inline static int32_t get_offset_of_hasOnStepComplete_7() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnStepComplete_7)); }
	inline bool get_hasOnStepComplete_7() const { return ___hasOnStepComplete_7; }
	inline bool* get_address_of_hasOnStepComplete_7() { return &___hasOnStepComplete_7; }
	inline void set_hasOnStepComplete_7(bool value)
	{
		___hasOnStepComplete_7 = value;
	}

	inline static int32_t get_offset_of_hasOnComplete_8() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnComplete_8)); }
	inline bool get_hasOnComplete_8() const { return ___hasOnComplete_8; }
	inline bool* get_address_of_hasOnComplete_8() { return &___hasOnComplete_8; }
	inline void set_hasOnComplete_8(bool value)
	{
		___hasOnComplete_8 = value;
	}

	inline static int32_t get_offset_of_hasOnTweenCreated_9() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnTweenCreated_9)); }
	inline bool get_hasOnTweenCreated_9() const { return ___hasOnTweenCreated_9; }
	inline bool* get_address_of_hasOnTweenCreated_9() { return &___hasOnTweenCreated_9; }
	inline void set_hasOnTweenCreated_9(bool value)
	{
		___hasOnTweenCreated_9 = value;
	}

	inline static int32_t get_offset_of_hasOnRewind_10() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnRewind_10)); }
	inline bool get_hasOnRewind_10() const { return ___hasOnRewind_10; }
	inline bool* get_address_of_hasOnRewind_10() { return &___hasOnRewind_10; }
	inline void set_hasOnRewind_10(bool value)
	{
		___hasOnRewind_10 = value;
	}

	inline static int32_t get_offset_of_onStart_11() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onStart_11)); }
	inline UnityEvent_t2581268647 * get_onStart_11() const { return ___onStart_11; }
	inline UnityEvent_t2581268647 ** get_address_of_onStart_11() { return &___onStart_11; }
	inline void set_onStart_11(UnityEvent_t2581268647 * value)
	{
		___onStart_11 = value;
		Il2CppCodeGenWriteBarrier((&___onStart_11), value);
	}

	inline static int32_t get_offset_of_onPlay_12() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onPlay_12)); }
	inline UnityEvent_t2581268647 * get_onPlay_12() const { return ___onPlay_12; }
	inline UnityEvent_t2581268647 ** get_address_of_onPlay_12() { return &___onPlay_12; }
	inline void set_onPlay_12(UnityEvent_t2581268647 * value)
	{
		___onPlay_12 = value;
		Il2CppCodeGenWriteBarrier((&___onPlay_12), value);
	}

	inline static int32_t get_offset_of_onUpdate_13() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onUpdate_13)); }
	inline UnityEvent_t2581268647 * get_onUpdate_13() const { return ___onUpdate_13; }
	inline UnityEvent_t2581268647 ** get_address_of_onUpdate_13() { return &___onUpdate_13; }
	inline void set_onUpdate_13(UnityEvent_t2581268647 * value)
	{
		___onUpdate_13 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdate_13), value);
	}

	inline static int32_t get_offset_of_onStepComplete_14() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onStepComplete_14)); }
	inline UnityEvent_t2581268647 * get_onStepComplete_14() const { return ___onStepComplete_14; }
	inline UnityEvent_t2581268647 ** get_address_of_onStepComplete_14() { return &___onStepComplete_14; }
	inline void set_onStepComplete_14(UnityEvent_t2581268647 * value)
	{
		___onStepComplete_14 = value;
		Il2CppCodeGenWriteBarrier((&___onStepComplete_14), value);
	}

	inline static int32_t get_offset_of_onComplete_15() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onComplete_15)); }
	inline UnityEvent_t2581268647 * get_onComplete_15() const { return ___onComplete_15; }
	inline UnityEvent_t2581268647 ** get_address_of_onComplete_15() { return &___onComplete_15; }
	inline void set_onComplete_15(UnityEvent_t2581268647 * value)
	{
		___onComplete_15 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_15), value);
	}

	inline static int32_t get_offset_of_onTweenCreated_16() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onTweenCreated_16)); }
	inline UnityEvent_t2581268647 * get_onTweenCreated_16() const { return ___onTweenCreated_16; }
	inline UnityEvent_t2581268647 ** get_address_of_onTweenCreated_16() { return &___onTweenCreated_16; }
	inline void set_onTweenCreated_16(UnityEvent_t2581268647 * value)
	{
		___onTweenCreated_16 = value;
		Il2CppCodeGenWriteBarrier((&___onTweenCreated_16), value);
	}

	inline static int32_t get_offset_of_onRewind_17() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onRewind_17)); }
	inline UnityEvent_t2581268647 * get_onRewind_17() const { return ___onRewind_17; }
	inline UnityEvent_t2581268647 ** get_address_of_onRewind_17() { return &___onRewind_17; }
	inline void set_onRewind_17(UnityEvent_t2581268647 * value)
	{
		___onRewind_17 = value;
		Il2CppCodeGenWriteBarrier((&___onRewind_17), value);
	}

	inline static int32_t get_offset_of_tween_18() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___tween_18)); }
	inline Tween_t2342918553 * get_tween_18() const { return ___tween_18; }
	inline Tween_t2342918553 ** get_address_of_tween_18() { return &___tween_18; }
	inline void set_tween_18(Tween_t2342918553 * value)
	{
		___tween_18 = value;
		Il2CppCodeGenWriteBarrier((&___tween_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSANIMATIONCOMPONENT_T262169234_H
#ifndef TWEENERCORE_3_T3040139253_H
#define TWEENERCORE_3_T3040139253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>
struct  TweenerCore_3_t3040139253  : public Tweener_t436044680
{
public:
	// T2 DG.Tweening.Core.TweenerCore`3::startValue
	Path_t3614338981 * ___startValue_53;
	// T2 DG.Tweening.Core.TweenerCore`3::endValue
	Path_t3614338981 * ___endValue_54;
	// T2 DG.Tweening.Core.TweenerCore`3::changeValue
	Path_t3614338981 * ___changeValue_55;
	// TPlugOptions DG.Tweening.Core.TweenerCore`3::plugOptions
	PathOptions_t2074623791  ___plugOptions_56;
	// DG.Tweening.Core.DOGetter`1<T1> DG.Tweening.Core.TweenerCore`3::getter
	DOGetter_1_t3465109668 * ___getter_57;
	// DG.Tweening.Core.DOSetter`1<T1> DG.Tweening.Core.TweenerCore`3::setter
	DOSetter_1_t477454500 * ___setter_58;
	// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions> DG.Tweening.Core.TweenerCore`3::tweenPlugin
	ABSTweenPlugin_3_t2576148903 * ___tweenPlugin_59;

public:
	inline static int32_t get_offset_of_startValue_53() { return static_cast<int32_t>(offsetof(TweenerCore_3_t3040139253, ___startValue_53)); }
	inline Path_t3614338981 * get_startValue_53() const { return ___startValue_53; }
	inline Path_t3614338981 ** get_address_of_startValue_53() { return &___startValue_53; }
	inline void set_startValue_53(Path_t3614338981 * value)
	{
		___startValue_53 = value;
		Il2CppCodeGenWriteBarrier((&___startValue_53), value);
	}

	inline static int32_t get_offset_of_endValue_54() { return static_cast<int32_t>(offsetof(TweenerCore_3_t3040139253, ___endValue_54)); }
	inline Path_t3614338981 * get_endValue_54() const { return ___endValue_54; }
	inline Path_t3614338981 ** get_address_of_endValue_54() { return &___endValue_54; }
	inline void set_endValue_54(Path_t3614338981 * value)
	{
		___endValue_54 = value;
		Il2CppCodeGenWriteBarrier((&___endValue_54), value);
	}

	inline static int32_t get_offset_of_changeValue_55() { return static_cast<int32_t>(offsetof(TweenerCore_3_t3040139253, ___changeValue_55)); }
	inline Path_t3614338981 * get_changeValue_55() const { return ___changeValue_55; }
	inline Path_t3614338981 ** get_address_of_changeValue_55() { return &___changeValue_55; }
	inline void set_changeValue_55(Path_t3614338981 * value)
	{
		___changeValue_55 = value;
		Il2CppCodeGenWriteBarrier((&___changeValue_55), value);
	}

	inline static int32_t get_offset_of_plugOptions_56() { return static_cast<int32_t>(offsetof(TweenerCore_3_t3040139253, ___plugOptions_56)); }
	inline PathOptions_t2074623791  get_plugOptions_56() const { return ___plugOptions_56; }
	inline PathOptions_t2074623791 * get_address_of_plugOptions_56() { return &___plugOptions_56; }
	inline void set_plugOptions_56(PathOptions_t2074623791  value)
	{
		___plugOptions_56 = value;
	}

	inline static int32_t get_offset_of_getter_57() { return static_cast<int32_t>(offsetof(TweenerCore_3_t3040139253, ___getter_57)); }
	inline DOGetter_1_t3465109668 * get_getter_57() const { return ___getter_57; }
	inline DOGetter_1_t3465109668 ** get_address_of_getter_57() { return &___getter_57; }
	inline void set_getter_57(DOGetter_1_t3465109668 * value)
	{
		___getter_57 = value;
		Il2CppCodeGenWriteBarrier((&___getter_57), value);
	}

	inline static int32_t get_offset_of_setter_58() { return static_cast<int32_t>(offsetof(TweenerCore_3_t3040139253, ___setter_58)); }
	inline DOSetter_1_t477454500 * get_setter_58() const { return ___setter_58; }
	inline DOSetter_1_t477454500 ** get_address_of_setter_58() { return &___setter_58; }
	inline void set_setter_58(DOSetter_1_t477454500 * value)
	{
		___setter_58 = value;
		Il2CppCodeGenWriteBarrier((&___setter_58), value);
	}

	inline static int32_t get_offset_of_tweenPlugin_59() { return static_cast<int32_t>(offsetof(TweenerCore_3_t3040139253, ___tweenPlugin_59)); }
	inline ABSTweenPlugin_3_t2576148903 * get_tweenPlugin_59() const { return ___tweenPlugin_59; }
	inline ABSTweenPlugin_3_t2576148903 ** get_address_of_tweenPlugin_59() { return &___tweenPlugin_59; }
	inline void set_tweenPlugin_59(ABSTweenPlugin_3_t2576148903 * value)
	{
		___tweenPlugin_59 = value;
		Il2CppCodeGenWriteBarrier((&___tweenPlugin_59), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENERCORE_3_T3040139253_H
#ifndef DOTWEENVISUALMANAGER_T1560353112_H
#define DOTWEENVISUALMANAGER_T1560353112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenVisualManager
struct  DOTweenVisualManager_t1560353112  : public MonoBehaviour_t3962482529
{
public:
	// DG.Tweening.Core.VisualManagerPreset DG.Tweening.DOTweenVisualManager::preset
	int32_t ___preset_2;
	// DG.Tweening.Core.OnEnableBehaviour DG.Tweening.DOTweenVisualManager::onEnableBehaviour
	int32_t ___onEnableBehaviour_3;
	// DG.Tweening.Core.OnDisableBehaviour DG.Tweening.DOTweenVisualManager::onDisableBehaviour
	int32_t ___onDisableBehaviour_4;
	// System.Boolean DG.Tweening.DOTweenVisualManager::_requiresRestartFromSpawnPoint
	bool ____requiresRestartFromSpawnPoint_5;
	// DG.Tweening.Core.ABSAnimationComponent DG.Tweening.DOTweenVisualManager::_animComponent
	ABSAnimationComponent_t262169234 * ____animComponent_6;

public:
	inline static int32_t get_offset_of_preset_2() { return static_cast<int32_t>(offsetof(DOTweenVisualManager_t1560353112, ___preset_2)); }
	inline int32_t get_preset_2() const { return ___preset_2; }
	inline int32_t* get_address_of_preset_2() { return &___preset_2; }
	inline void set_preset_2(int32_t value)
	{
		___preset_2 = value;
	}

	inline static int32_t get_offset_of_onEnableBehaviour_3() { return static_cast<int32_t>(offsetof(DOTweenVisualManager_t1560353112, ___onEnableBehaviour_3)); }
	inline int32_t get_onEnableBehaviour_3() const { return ___onEnableBehaviour_3; }
	inline int32_t* get_address_of_onEnableBehaviour_3() { return &___onEnableBehaviour_3; }
	inline void set_onEnableBehaviour_3(int32_t value)
	{
		___onEnableBehaviour_3 = value;
	}

	inline static int32_t get_offset_of_onDisableBehaviour_4() { return static_cast<int32_t>(offsetof(DOTweenVisualManager_t1560353112, ___onDisableBehaviour_4)); }
	inline int32_t get_onDisableBehaviour_4() const { return ___onDisableBehaviour_4; }
	inline int32_t* get_address_of_onDisableBehaviour_4() { return &___onDisableBehaviour_4; }
	inline void set_onDisableBehaviour_4(int32_t value)
	{
		___onDisableBehaviour_4 = value;
	}

	inline static int32_t get_offset_of__requiresRestartFromSpawnPoint_5() { return static_cast<int32_t>(offsetof(DOTweenVisualManager_t1560353112, ____requiresRestartFromSpawnPoint_5)); }
	inline bool get__requiresRestartFromSpawnPoint_5() const { return ____requiresRestartFromSpawnPoint_5; }
	inline bool* get_address_of__requiresRestartFromSpawnPoint_5() { return &____requiresRestartFromSpawnPoint_5; }
	inline void set__requiresRestartFromSpawnPoint_5(bool value)
	{
		____requiresRestartFromSpawnPoint_5 = value;
	}

	inline static int32_t get_offset_of__animComponent_6() { return static_cast<int32_t>(offsetof(DOTweenVisualManager_t1560353112, ____animComponent_6)); }
	inline ABSAnimationComponent_t262169234 * get__animComponent_6() const { return ____animComponent_6; }
	inline ABSAnimationComponent_t262169234 ** get_address_of__animComponent_6() { return &____animComponent_6; }
	inline void set__animComponent_6(ABSAnimationComponent_t262169234 * value)
	{
		____animComponent_6 = value;
		Il2CppCodeGenWriteBarrier((&____animComponent_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENVISUALMANAGER_T1560353112_H
#ifndef DOTWEENPATH_T3192963685_H
#define DOTWEENPATH_T3192963685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenPath
struct  DOTweenPath_t3192963685  : public ABSAnimationComponent_t262169234
{
public:
	// System.Single DG.Tweening.DOTweenPath::delay
	float ___delay_19;
	// System.Single DG.Tweening.DOTweenPath::duration
	float ___duration_20;
	// DG.Tweening.Ease DG.Tweening.DOTweenPath::easeType
	int32_t ___easeType_21;
	// UnityEngine.AnimationCurve DG.Tweening.DOTweenPath::easeCurve
	AnimationCurve_t3046754366 * ___easeCurve_22;
	// System.Int32 DG.Tweening.DOTweenPath::loops
	int32_t ___loops_23;
	// System.String DG.Tweening.DOTweenPath::id
	String_t* ___id_24;
	// DG.Tweening.LoopType DG.Tweening.DOTweenPath::loopType
	int32_t ___loopType_25;
	// DG.Tweening.Plugins.Options.OrientType DG.Tweening.DOTweenPath::orientType
	int32_t ___orientType_26;
	// UnityEngine.Transform DG.Tweening.DOTweenPath::lookAtTransform
	Transform_t3600365921 * ___lookAtTransform_27;
	// UnityEngine.Vector3 DG.Tweening.DOTweenPath::lookAtPosition
	Vector3_t3722313464  ___lookAtPosition_28;
	// System.Single DG.Tweening.DOTweenPath::lookAhead
	float ___lookAhead_29;
	// System.Boolean DG.Tweening.DOTweenPath::autoPlay
	bool ___autoPlay_30;
	// System.Boolean DG.Tweening.DOTweenPath::autoKill
	bool ___autoKill_31;
	// System.Boolean DG.Tweening.DOTweenPath::relative
	bool ___relative_32;
	// System.Boolean DG.Tweening.DOTweenPath::isLocal
	bool ___isLocal_33;
	// System.Boolean DG.Tweening.DOTweenPath::isClosedPath
	bool ___isClosedPath_34;
	// System.Int32 DG.Tweening.DOTweenPath::pathResolution
	int32_t ___pathResolution_35;
	// DG.Tweening.PathMode DG.Tweening.DOTweenPath::pathMode
	int32_t ___pathMode_36;
	// DG.Tweening.AxisConstraint DG.Tweening.DOTweenPath::lockRotation
	int32_t ___lockRotation_37;
	// System.Boolean DG.Tweening.DOTweenPath::assignForwardAndUp
	bool ___assignForwardAndUp_38;
	// UnityEngine.Vector3 DG.Tweening.DOTweenPath::forwardDirection
	Vector3_t3722313464  ___forwardDirection_39;
	// UnityEngine.Vector3 DG.Tweening.DOTweenPath::upDirection
	Vector3_t3722313464  ___upDirection_40;
	// System.Boolean DG.Tweening.DOTweenPath::tweenRigidbody
	bool ___tweenRigidbody_41;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> DG.Tweening.DOTweenPath::wps
	List_1_t899420910 * ___wps_42;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> DG.Tweening.DOTweenPath::fullWps
	List_1_t899420910 * ___fullWps_43;
	// DG.Tweening.Plugins.Core.PathCore.Path DG.Tweening.DOTweenPath::path
	Path_t3614338981 * ___path_44;
	// DG.Tweening.DOTweenInspectorMode DG.Tweening.DOTweenPath::inspectorMode
	int32_t ___inspectorMode_45;
	// DG.Tweening.PathType DG.Tweening.DOTweenPath::pathType
	int32_t ___pathType_46;
	// DG.Tweening.HandlesType DG.Tweening.DOTweenPath::handlesType
	int32_t ___handlesType_47;
	// System.Boolean DG.Tweening.DOTweenPath::livePreview
	bool ___livePreview_48;
	// DG.Tweening.HandlesDrawMode DG.Tweening.DOTweenPath::handlesDrawMode
	int32_t ___handlesDrawMode_49;
	// System.Single DG.Tweening.DOTweenPath::perspectiveHandleSize
	float ___perspectiveHandleSize_50;
	// System.Boolean DG.Tweening.DOTweenPath::showIndexes
	bool ___showIndexes_51;
	// System.Boolean DG.Tweening.DOTweenPath::showWpLength
	bool ___showWpLength_52;
	// UnityEngine.Color DG.Tweening.DOTweenPath::pathColor
	Color_t2555686324  ___pathColor_53;
	// UnityEngine.Vector3 DG.Tweening.DOTweenPath::lastSrcPosition
	Vector3_t3722313464  ___lastSrcPosition_54;
	// System.Boolean DG.Tweening.DOTweenPath::wpsDropdown
	bool ___wpsDropdown_55;
	// System.Single DG.Tweening.DOTweenPath::dropToFloorOffset
	float ___dropToFloorOffset_56;

public:
	inline static int32_t get_offset_of_delay_19() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___delay_19)); }
	inline float get_delay_19() const { return ___delay_19; }
	inline float* get_address_of_delay_19() { return &___delay_19; }
	inline void set_delay_19(float value)
	{
		___delay_19 = value;
	}

	inline static int32_t get_offset_of_duration_20() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___duration_20)); }
	inline float get_duration_20() const { return ___duration_20; }
	inline float* get_address_of_duration_20() { return &___duration_20; }
	inline void set_duration_20(float value)
	{
		___duration_20 = value;
	}

	inline static int32_t get_offset_of_easeType_21() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___easeType_21)); }
	inline int32_t get_easeType_21() const { return ___easeType_21; }
	inline int32_t* get_address_of_easeType_21() { return &___easeType_21; }
	inline void set_easeType_21(int32_t value)
	{
		___easeType_21 = value;
	}

	inline static int32_t get_offset_of_easeCurve_22() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___easeCurve_22)); }
	inline AnimationCurve_t3046754366 * get_easeCurve_22() const { return ___easeCurve_22; }
	inline AnimationCurve_t3046754366 ** get_address_of_easeCurve_22() { return &___easeCurve_22; }
	inline void set_easeCurve_22(AnimationCurve_t3046754366 * value)
	{
		___easeCurve_22 = value;
		Il2CppCodeGenWriteBarrier((&___easeCurve_22), value);
	}

	inline static int32_t get_offset_of_loops_23() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___loops_23)); }
	inline int32_t get_loops_23() const { return ___loops_23; }
	inline int32_t* get_address_of_loops_23() { return &___loops_23; }
	inline void set_loops_23(int32_t value)
	{
		___loops_23 = value;
	}

	inline static int32_t get_offset_of_id_24() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___id_24)); }
	inline String_t* get_id_24() const { return ___id_24; }
	inline String_t** get_address_of_id_24() { return &___id_24; }
	inline void set_id_24(String_t* value)
	{
		___id_24 = value;
		Il2CppCodeGenWriteBarrier((&___id_24), value);
	}

	inline static int32_t get_offset_of_loopType_25() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___loopType_25)); }
	inline int32_t get_loopType_25() const { return ___loopType_25; }
	inline int32_t* get_address_of_loopType_25() { return &___loopType_25; }
	inline void set_loopType_25(int32_t value)
	{
		___loopType_25 = value;
	}

	inline static int32_t get_offset_of_orientType_26() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___orientType_26)); }
	inline int32_t get_orientType_26() const { return ___orientType_26; }
	inline int32_t* get_address_of_orientType_26() { return &___orientType_26; }
	inline void set_orientType_26(int32_t value)
	{
		___orientType_26 = value;
	}

	inline static int32_t get_offset_of_lookAtTransform_27() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___lookAtTransform_27)); }
	inline Transform_t3600365921 * get_lookAtTransform_27() const { return ___lookAtTransform_27; }
	inline Transform_t3600365921 ** get_address_of_lookAtTransform_27() { return &___lookAtTransform_27; }
	inline void set_lookAtTransform_27(Transform_t3600365921 * value)
	{
		___lookAtTransform_27 = value;
		Il2CppCodeGenWriteBarrier((&___lookAtTransform_27), value);
	}

	inline static int32_t get_offset_of_lookAtPosition_28() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___lookAtPosition_28)); }
	inline Vector3_t3722313464  get_lookAtPosition_28() const { return ___lookAtPosition_28; }
	inline Vector3_t3722313464 * get_address_of_lookAtPosition_28() { return &___lookAtPosition_28; }
	inline void set_lookAtPosition_28(Vector3_t3722313464  value)
	{
		___lookAtPosition_28 = value;
	}

	inline static int32_t get_offset_of_lookAhead_29() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___lookAhead_29)); }
	inline float get_lookAhead_29() const { return ___lookAhead_29; }
	inline float* get_address_of_lookAhead_29() { return &___lookAhead_29; }
	inline void set_lookAhead_29(float value)
	{
		___lookAhead_29 = value;
	}

	inline static int32_t get_offset_of_autoPlay_30() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___autoPlay_30)); }
	inline bool get_autoPlay_30() const { return ___autoPlay_30; }
	inline bool* get_address_of_autoPlay_30() { return &___autoPlay_30; }
	inline void set_autoPlay_30(bool value)
	{
		___autoPlay_30 = value;
	}

	inline static int32_t get_offset_of_autoKill_31() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___autoKill_31)); }
	inline bool get_autoKill_31() const { return ___autoKill_31; }
	inline bool* get_address_of_autoKill_31() { return &___autoKill_31; }
	inline void set_autoKill_31(bool value)
	{
		___autoKill_31 = value;
	}

	inline static int32_t get_offset_of_relative_32() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___relative_32)); }
	inline bool get_relative_32() const { return ___relative_32; }
	inline bool* get_address_of_relative_32() { return &___relative_32; }
	inline void set_relative_32(bool value)
	{
		___relative_32 = value;
	}

	inline static int32_t get_offset_of_isLocal_33() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___isLocal_33)); }
	inline bool get_isLocal_33() const { return ___isLocal_33; }
	inline bool* get_address_of_isLocal_33() { return &___isLocal_33; }
	inline void set_isLocal_33(bool value)
	{
		___isLocal_33 = value;
	}

	inline static int32_t get_offset_of_isClosedPath_34() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___isClosedPath_34)); }
	inline bool get_isClosedPath_34() const { return ___isClosedPath_34; }
	inline bool* get_address_of_isClosedPath_34() { return &___isClosedPath_34; }
	inline void set_isClosedPath_34(bool value)
	{
		___isClosedPath_34 = value;
	}

	inline static int32_t get_offset_of_pathResolution_35() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___pathResolution_35)); }
	inline int32_t get_pathResolution_35() const { return ___pathResolution_35; }
	inline int32_t* get_address_of_pathResolution_35() { return &___pathResolution_35; }
	inline void set_pathResolution_35(int32_t value)
	{
		___pathResolution_35 = value;
	}

	inline static int32_t get_offset_of_pathMode_36() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___pathMode_36)); }
	inline int32_t get_pathMode_36() const { return ___pathMode_36; }
	inline int32_t* get_address_of_pathMode_36() { return &___pathMode_36; }
	inline void set_pathMode_36(int32_t value)
	{
		___pathMode_36 = value;
	}

	inline static int32_t get_offset_of_lockRotation_37() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___lockRotation_37)); }
	inline int32_t get_lockRotation_37() const { return ___lockRotation_37; }
	inline int32_t* get_address_of_lockRotation_37() { return &___lockRotation_37; }
	inline void set_lockRotation_37(int32_t value)
	{
		___lockRotation_37 = value;
	}

	inline static int32_t get_offset_of_assignForwardAndUp_38() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___assignForwardAndUp_38)); }
	inline bool get_assignForwardAndUp_38() const { return ___assignForwardAndUp_38; }
	inline bool* get_address_of_assignForwardAndUp_38() { return &___assignForwardAndUp_38; }
	inline void set_assignForwardAndUp_38(bool value)
	{
		___assignForwardAndUp_38 = value;
	}

	inline static int32_t get_offset_of_forwardDirection_39() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___forwardDirection_39)); }
	inline Vector3_t3722313464  get_forwardDirection_39() const { return ___forwardDirection_39; }
	inline Vector3_t3722313464 * get_address_of_forwardDirection_39() { return &___forwardDirection_39; }
	inline void set_forwardDirection_39(Vector3_t3722313464  value)
	{
		___forwardDirection_39 = value;
	}

	inline static int32_t get_offset_of_upDirection_40() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___upDirection_40)); }
	inline Vector3_t3722313464  get_upDirection_40() const { return ___upDirection_40; }
	inline Vector3_t3722313464 * get_address_of_upDirection_40() { return &___upDirection_40; }
	inline void set_upDirection_40(Vector3_t3722313464  value)
	{
		___upDirection_40 = value;
	}

	inline static int32_t get_offset_of_tweenRigidbody_41() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___tweenRigidbody_41)); }
	inline bool get_tweenRigidbody_41() const { return ___tweenRigidbody_41; }
	inline bool* get_address_of_tweenRigidbody_41() { return &___tweenRigidbody_41; }
	inline void set_tweenRigidbody_41(bool value)
	{
		___tweenRigidbody_41 = value;
	}

	inline static int32_t get_offset_of_wps_42() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___wps_42)); }
	inline List_1_t899420910 * get_wps_42() const { return ___wps_42; }
	inline List_1_t899420910 ** get_address_of_wps_42() { return &___wps_42; }
	inline void set_wps_42(List_1_t899420910 * value)
	{
		___wps_42 = value;
		Il2CppCodeGenWriteBarrier((&___wps_42), value);
	}

	inline static int32_t get_offset_of_fullWps_43() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___fullWps_43)); }
	inline List_1_t899420910 * get_fullWps_43() const { return ___fullWps_43; }
	inline List_1_t899420910 ** get_address_of_fullWps_43() { return &___fullWps_43; }
	inline void set_fullWps_43(List_1_t899420910 * value)
	{
		___fullWps_43 = value;
		Il2CppCodeGenWriteBarrier((&___fullWps_43), value);
	}

	inline static int32_t get_offset_of_path_44() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___path_44)); }
	inline Path_t3614338981 * get_path_44() const { return ___path_44; }
	inline Path_t3614338981 ** get_address_of_path_44() { return &___path_44; }
	inline void set_path_44(Path_t3614338981 * value)
	{
		___path_44 = value;
		Il2CppCodeGenWriteBarrier((&___path_44), value);
	}

	inline static int32_t get_offset_of_inspectorMode_45() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___inspectorMode_45)); }
	inline int32_t get_inspectorMode_45() const { return ___inspectorMode_45; }
	inline int32_t* get_address_of_inspectorMode_45() { return &___inspectorMode_45; }
	inline void set_inspectorMode_45(int32_t value)
	{
		___inspectorMode_45 = value;
	}

	inline static int32_t get_offset_of_pathType_46() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___pathType_46)); }
	inline int32_t get_pathType_46() const { return ___pathType_46; }
	inline int32_t* get_address_of_pathType_46() { return &___pathType_46; }
	inline void set_pathType_46(int32_t value)
	{
		___pathType_46 = value;
	}

	inline static int32_t get_offset_of_handlesType_47() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___handlesType_47)); }
	inline int32_t get_handlesType_47() const { return ___handlesType_47; }
	inline int32_t* get_address_of_handlesType_47() { return &___handlesType_47; }
	inline void set_handlesType_47(int32_t value)
	{
		___handlesType_47 = value;
	}

	inline static int32_t get_offset_of_livePreview_48() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___livePreview_48)); }
	inline bool get_livePreview_48() const { return ___livePreview_48; }
	inline bool* get_address_of_livePreview_48() { return &___livePreview_48; }
	inline void set_livePreview_48(bool value)
	{
		___livePreview_48 = value;
	}

	inline static int32_t get_offset_of_handlesDrawMode_49() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___handlesDrawMode_49)); }
	inline int32_t get_handlesDrawMode_49() const { return ___handlesDrawMode_49; }
	inline int32_t* get_address_of_handlesDrawMode_49() { return &___handlesDrawMode_49; }
	inline void set_handlesDrawMode_49(int32_t value)
	{
		___handlesDrawMode_49 = value;
	}

	inline static int32_t get_offset_of_perspectiveHandleSize_50() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___perspectiveHandleSize_50)); }
	inline float get_perspectiveHandleSize_50() const { return ___perspectiveHandleSize_50; }
	inline float* get_address_of_perspectiveHandleSize_50() { return &___perspectiveHandleSize_50; }
	inline void set_perspectiveHandleSize_50(float value)
	{
		___perspectiveHandleSize_50 = value;
	}

	inline static int32_t get_offset_of_showIndexes_51() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___showIndexes_51)); }
	inline bool get_showIndexes_51() const { return ___showIndexes_51; }
	inline bool* get_address_of_showIndexes_51() { return &___showIndexes_51; }
	inline void set_showIndexes_51(bool value)
	{
		___showIndexes_51 = value;
	}

	inline static int32_t get_offset_of_showWpLength_52() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___showWpLength_52)); }
	inline bool get_showWpLength_52() const { return ___showWpLength_52; }
	inline bool* get_address_of_showWpLength_52() { return &___showWpLength_52; }
	inline void set_showWpLength_52(bool value)
	{
		___showWpLength_52 = value;
	}

	inline static int32_t get_offset_of_pathColor_53() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___pathColor_53)); }
	inline Color_t2555686324  get_pathColor_53() const { return ___pathColor_53; }
	inline Color_t2555686324 * get_address_of_pathColor_53() { return &___pathColor_53; }
	inline void set_pathColor_53(Color_t2555686324  value)
	{
		___pathColor_53 = value;
	}

	inline static int32_t get_offset_of_lastSrcPosition_54() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___lastSrcPosition_54)); }
	inline Vector3_t3722313464  get_lastSrcPosition_54() const { return ___lastSrcPosition_54; }
	inline Vector3_t3722313464 * get_address_of_lastSrcPosition_54() { return &___lastSrcPosition_54; }
	inline void set_lastSrcPosition_54(Vector3_t3722313464  value)
	{
		___lastSrcPosition_54 = value;
	}

	inline static int32_t get_offset_of_wpsDropdown_55() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___wpsDropdown_55)); }
	inline bool get_wpsDropdown_55() const { return ___wpsDropdown_55; }
	inline bool* get_address_of_wpsDropdown_55() { return &___wpsDropdown_55; }
	inline void set_wpsDropdown_55(bool value)
	{
		___wpsDropdown_55 = value;
	}

	inline static int32_t get_offset_of_dropToFloorOffset_56() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___dropToFloorOffset_56)); }
	inline float get_dropToFloorOffset_56() const { return ___dropToFloorOffset_56; }
	inline float* get_address_of_dropToFloorOffset_56() { return &___dropToFloorOffset_56; }
	inline void set_dropToFloorOffset_56(float value)
	{
		___dropToFloorOffset_56 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENPATH_T3192963685_H
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t3722313464  m_Items[1];

public:
	inline Vector3_t3722313464  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t3722313464 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t3722313464  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t3722313464  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t3722313464 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t3722313464  value)
	{
		m_Items[index] = value;
	}
};
// DG.Tweening.Plugins.Core.PathCore.ControlPoint[]
struct ControlPointU5BU5D_t1567961855  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ControlPoint_t3892672090  m_Items[1];

public:
	inline ControlPoint_t3892672090  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ControlPoint_t3892672090 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ControlPoint_t3892672090  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ControlPoint_t3892672090  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ControlPoint_t3892672090 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ControlPoint_t3892672090  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1068524471  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Keyframe_t4206410242  m_Items[1];

public:
	inline Keyframe_t4206410242  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Keyframe_t4206410242 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Keyframe_t4206410242  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Keyframe_t4206410242  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Keyframe_t4206410242 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Keyframe_t4206410242  value)
	{
		m_Items[index] = value;
	}
};


// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
extern "C"  int32_t List_1_get_Count_m576380744_gshared (List_1_t899420910 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Void System.Nullable`1<UnityEngine.Vector3>::.ctor(!0)
extern "C"  void Nullable_1__ctor_m133295941_gshared (Nullable_1_t1149908250 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetDelay<System.Object>(!!0,System.Single)
extern "C"  RuntimeObject * TweenSettingsExtensions_SetDelay_TisRuntimeObject_m361579755_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, float p1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<System.Object>(!!0,System.Int32,DG.Tweening.LoopType)
extern "C"  RuntimeObject * TweenSettingsExtensions_SetLoops_TisRuntimeObject_m1192961630_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, int32_t p1, int32_t p2, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetAutoKill<System.Object>(!!0,System.Boolean)
extern "C"  RuntimeObject * TweenSettingsExtensions_SetAutoKill_TisRuntimeObject_m593263294_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, bool p1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetUpdate<System.Object>(!!0,DG.Tweening.UpdateType)
extern "C"  RuntimeObject * TweenSettingsExtensions_SetUpdate_TisRuntimeObject_m2999996877_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, int32_t p1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnKill<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  RuntimeObject * TweenSettingsExtensions_OnKill_TisRuntimeObject_m1405886013_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, TweenCallback_t3727756325 * p1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetSpeedBased<System.Object>(!!0)
extern "C"  RuntimeObject * TweenSettingsExtensions_SetSpeedBased_TisRuntimeObject_m1809956742_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<System.Object>(!!0,UnityEngine.AnimationCurve)
extern "C"  RuntimeObject * TweenSettingsExtensions_SetEase_TisRuntimeObject_m1518755549_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, AnimationCurve_t3046754366 * p1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<System.Object>(!!0,DG.Tweening.Ease)
extern "C"  RuntimeObject * TweenSettingsExtensions_SetEase_TisRuntimeObject_m2697960120_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, int32_t p1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetId<System.Object>(!!0,System.Object)
extern "C"  RuntimeObject * TweenSettingsExtensions_SetId_TisRuntimeObject_m3240495869_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnStart<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  RuntimeObject * TweenSettingsExtensions_OnStart_TisRuntimeObject_m3322437883_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, TweenCallback_t3727756325 * p1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnPlay<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  RuntimeObject * TweenSettingsExtensions_OnPlay_TisRuntimeObject_m3776164416_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, TweenCallback_t3727756325 * p1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnUpdate<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  RuntimeObject * TweenSettingsExtensions_OnUpdate_TisRuntimeObject_m4115200285_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, TweenCallback_t3727756325 * p1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnStepComplete<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  RuntimeObject * TweenSettingsExtensions_OnStepComplete_TisRuntimeObject_m2935354448_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, TweenCallback_t3727756325 * p1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  RuntimeObject * TweenSettingsExtensions_OnComplete_TisRuntimeObject_m3432849958_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, TweenCallback_t3727756325 * p1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnRewind<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  RuntimeObject * TweenSettingsExtensions_OnRewind_TisRuntimeObject_m4151156780_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, TweenCallback_t3727756325 * p1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenExtensions::Play<System.Object>(!!0)
extern "C"  RuntimeObject * TweenExtensions_Play_TisRuntimeObject_m1290795777_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenExtensions::Pause<System.Object>(!!0)
extern "C"  RuntimeObject * TweenExtensions_Pause_TisRuntimeObject_m1911018533_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<UnityEngine.Vector3>::ToArray()
extern "C"  Vector3U5BU5D_t1718750761* List_1_ToArray_m466715028_gshared (List_1_t899420910 * __this, const RuntimeMethod* method);
// System.Void System.Nullable`1<UnityEngine.Color>::.ctor(!0)
extern "C"  void Nullable_1__ctor_m2967195975_gshared (Nullable_1_t4278248406 * __this, Color_t2555686324  p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
extern "C"  Vector3_t3722313464  List_1_get_Item_m200663048_gshared (List_1_t899420910 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
extern "C"  void List_1__ctor_m2503402603_gshared (List_1_t899420910 * __this, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
#define List_1_get_Count_m576380744(__this, method) ((  int32_t (*) (List_1_t899420910 *, const RuntimeMethod*))List_1_get_Count_m576380744_gshared)(__this, method)
// System.Void DG.Tweening.Plugins.Core.PathCore.Path::AssignDecoder(DG.Tweening.PathType)
extern "C"  void Path_AssignDecoder_m3239877656 (Path_t3614338981 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void TweenCallback__ctor_m3086472496 (TweenCallback_t3727756325 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::Add(!0)
#define List_1_Add_m2060438683(__this, p0, method) ((  void (*) (List_1_t904863771 *, TweenCallback_t3727756325 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t3600365921 * Transform_get_parent_m835071599 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Subtraction_m3073674971 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.DOTweenPath::ReEvaluateRelativeTween()
extern "C"  void DOTweenPath_ReEvaluateRelativeTween_m1807793085 (DOTweenPath_t3192963685 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
#define Component_GetComponent_TisSpriteRenderer_t3235626157_m4281600501(__this, method) ((  SpriteRenderer_t3235626157 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t3916780224_m2140521468(__this, method) ((  Rigidbody_t3916780224 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern "C"  TweenerCore_3_t3040139253 * ShortcutExtensions_DOPath_m3404366840 (RuntimeObject * __this /* static, unused */, Rigidbody_t3916780224 * p0, Path_t3614338981 * p1, float p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>,System.Boolean,DG.Tweening.AxisConstraint,DG.Tweening.AxisConstraint)
extern "C"  TweenerCore_3_t3040139253 * TweenSettingsExtensions_SetOptions_m2229198199 (RuntimeObject * __this /* static, unused */, TweenerCore_3_t3040139253 * p0, bool p1, int32_t p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOLocalPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern "C"  TweenerCore_3_t3040139253 * ShortcutExtensions_DOLocalPath_m2803671522 (RuntimeObject * __this /* static, unused */, Rigidbody_t3916780224 * p0, Path_t3614338981 * p1, float p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOPath(UnityEngine.Transform,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern "C"  TweenerCore_3_t3040139253 * ShortcutExtensions_DOPath_m1737185059 (RuntimeObject * __this /* static, unused */, Transform_t3600365921 * p0, Path_t3614338981 * p1, float p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOLocalPath(UnityEngine.Transform,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern "C"  TweenerCore_3_t3040139253 * ShortcutExtensions_DOLocalPath_m3367906890 (RuntimeObject * __this /* static, unused */, Transform_t3600365921 * p0, Path_t3614338981 * p1, float p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Nullable`1<UnityEngine.Vector3>::.ctor(!0)
#define Nullable_1__ctor_m133295941(__this, p0, method) ((  void (*) (Nullable_1_t1149908250 *, Vector3_t3722313464 , const RuntimeMethod*))Nullable_1__ctor_m133295941_gshared)(__this, p0, method)
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.TweenSettingsExtensions::SetLookAt(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>,UnityEngine.Transform,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Vector3>)
extern "C"  TweenerCore_3_t3040139253 * TweenSettingsExtensions_SetLookAt_m2541161551 (RuntimeObject * __this /* static, unused */, TweenerCore_3_t3040139253 * p0, Transform_t3600365921 * p1, Nullable_1_t1149908250  p2, Nullable_1_t1149908250  p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.TweenSettingsExtensions::SetLookAt(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>,UnityEngine.Vector3,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Vector3>)
extern "C"  TweenerCore_3_t3040139253 * TweenSettingsExtensions_SetLookAt_m1165050034 (RuntimeObject * __this /* static, unused */, TweenerCore_3_t3040139253 * p0, Vector3_t3722313464  p1, Nullable_1_t1149908250  p2, Nullable_1_t1149908250  p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.TweenSettingsExtensions::SetLookAt(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>,System.Single,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Vector3>)
extern "C"  TweenerCore_3_t3040139253 * TweenSettingsExtensions_SetLookAt_m3381879294 (RuntimeObject * __this /* static, unused */, TweenerCore_3_t3040139253 * p0, float p1, Nullable_1_t1149908250  p2, Nullable_1_t1149908250  p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 DG.Tweening.TweenSettingsExtensions::SetDelay<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,System.Single)
#define TweenSettingsExtensions_SetDelay_TisTweenerCore_3_t3040139253_m1178560492(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3040139253 * (*) (RuntimeObject * /* static, unused */, TweenerCore_3_t3040139253 *, float, const RuntimeMethod*))TweenSettingsExtensions_SetDelay_TisRuntimeObject_m361579755_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,System.Int32,DG.Tweening.LoopType)
#define TweenSettingsExtensions_SetLoops_TisTweenerCore_3_t3040139253_m3803960963(__this /* static, unused */, p0, p1, p2, method) ((  TweenerCore_3_t3040139253 * (*) (RuntimeObject * /* static, unused */, TweenerCore_3_t3040139253 *, int32_t, int32_t, const RuntimeMethod*))TweenSettingsExtensions_SetLoops_TisRuntimeObject_m1192961630_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetAutoKill<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,System.Boolean)
#define TweenSettingsExtensions_SetAutoKill_TisTweenerCore_3_t3040139253_m1506332555(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3040139253 * (*) (RuntimeObject * /* static, unused */, TweenerCore_3_t3040139253 *, bool, const RuntimeMethod*))TweenSettingsExtensions_SetAutoKill_TisRuntimeObject_m593263294_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetUpdate<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.UpdateType)
#define TweenSettingsExtensions_SetUpdate_TisTweenerCore_3_t3040139253_m2375151364(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3040139253 * (*) (RuntimeObject * /* static, unused */, TweenerCore_3_t3040139253 *, int32_t, const RuntimeMethod*))TweenSettingsExtensions_SetUpdate_TisRuntimeObject_m2999996877_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnKill<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnKill_TisTweenerCore_3_t3040139253_m2092851215(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3040139253 * (*) (RuntimeObject * /* static, unused */, TweenerCore_3_t3040139253 *, TweenCallback_t3727756325 *, const RuntimeMethod*))TweenSettingsExtensions_OnKill_TisRuntimeObject_m1405886013_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetSpeedBased<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0)
#define TweenSettingsExtensions_SetSpeedBased_TisTweenerCore_3_t3040139253_m914173068(__this /* static, unused */, p0, method) ((  TweenerCore_3_t3040139253 * (*) (RuntimeObject * /* static, unused */, TweenerCore_3_t3040139253 *, const RuntimeMethod*))TweenSettingsExtensions_SetSpeedBased_TisRuntimeObject_m1809956742_gshared)(__this /* static, unused */, p0, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,UnityEngine.AnimationCurve)
#define TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3040139253_m3071670034(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3040139253 * (*) (RuntimeObject * /* static, unused */, TweenerCore_3_t3040139253 *, AnimationCurve_t3046754366 *, const RuntimeMethod*))TweenSettingsExtensions_SetEase_TisRuntimeObject_m1518755549_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.Ease)
#define TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3040139253_m2476880271(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3040139253 * (*) (RuntimeObject * /* static, unused */, TweenerCore_3_t3040139253 *, int32_t, const RuntimeMethod*))TweenSettingsExtensions_SetEase_TisRuntimeObject_m2697960120_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C"  bool String_IsNullOrEmpty_m2969720369 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 DG.Tweening.TweenSettingsExtensions::SetId<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,System.Object)
#define TweenSettingsExtensions_SetId_TisTweenerCore_3_t3040139253_m994168359(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3040139253 * (*) (RuntimeObject * /* static, unused */, TweenerCore_3_t3040139253 *, RuntimeObject *, const RuntimeMethod*))TweenSettingsExtensions_SetId_TisRuntimeObject_m3240495869_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnStart<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnStart_TisTweenerCore_3_t3040139253_m2399400913(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3040139253 * (*) (RuntimeObject * /* static, unused */, TweenerCore_3_t3040139253 *, TweenCallback_t3727756325 *, const RuntimeMethod*))TweenSettingsExtensions_OnStart_TisRuntimeObject_m3322437883_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnPlay<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnPlay_TisTweenerCore_3_t3040139253_m3727247487(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3040139253 * (*) (RuntimeObject * /* static, unused */, TweenerCore_3_t3040139253 *, TweenCallback_t3727756325 *, const RuntimeMethod*))TweenSettingsExtensions_OnPlay_TisRuntimeObject_m3776164416_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnUpdate<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnUpdate_TisTweenerCore_3_t3040139253_m3414834117(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3040139253 * (*) (RuntimeObject * /* static, unused */, TweenerCore_3_t3040139253 *, TweenCallback_t3727756325 *, const RuntimeMethod*))TweenSettingsExtensions_OnUpdate_TisRuntimeObject_m4115200285_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnStepComplete<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnStepComplete_TisTweenerCore_3_t3040139253_m2372572972(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3040139253 * (*) (RuntimeObject * /* static, unused */, TweenerCore_3_t3040139253 *, TweenCallback_t3727756325 *, const RuntimeMethod*))TweenSettingsExtensions_OnStepComplete_TisRuntimeObject_m2935354448_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnComplete_TisTweenerCore_3_t3040139253_m371879841(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3040139253 * (*) (RuntimeObject * /* static, unused */, TweenerCore_3_t3040139253 *, TweenCallback_t3727756325 *, const RuntimeMethod*))TweenSettingsExtensions_OnComplete_TisRuntimeObject_m3432849958_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnRewind<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnRewind_TisTweenerCore_3_t3040139253_m2866982129(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3040139253 * (*) (RuntimeObject * /* static, unused */, TweenerCore_3_t3040139253 *, TweenCallback_t3727756325 *, const RuntimeMethod*))TweenSettingsExtensions_OnRewind_TisRuntimeObject_m4151156780_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenExtensions::Play<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0)
#define TweenExtensions_Play_TisTweenerCore_3_t3040139253_m2536939817(__this /* static, unused */, p0, method) ((  TweenerCore_3_t3040139253 * (*) (RuntimeObject * /* static, unused */, TweenerCore_3_t3040139253 *, const RuntimeMethod*))TweenExtensions_Play_TisRuntimeObject_m1290795777_gshared)(__this /* static, unused */, p0, method)
// !!0 DG.Tweening.TweenExtensions::Pause<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0)
#define TweenExtensions_Pause_TisTweenerCore_3_t3040139253_m1549830538(__this /* static, unused */, p0, method) ((  TweenerCore_3_t3040139253 * (*) (RuntimeObject * /* static, unused */, TweenerCore_3_t3040139253 *, const RuntimeMethod*))TweenExtensions_Pause_TisRuntimeObject_m1911018533_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.Events.UnityEvent::Invoke()
extern "C"  void UnityEvent_Invoke_m3065672636 (UnityEvent_t2581268647 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0[] System.Collections.Generic.List`1<UnityEngine.Vector3>::ToArray()
#define List_1_ToArray_m466715028(__this, method) ((  Vector3U5BU5D_t1718750761* (*) (List_1_t899420910 *, const RuntimeMethod*))List_1_ToArray_m466715028_gshared)(__this, method)
// System.Void System.Nullable`1<UnityEngine.Color>::.ctor(!0)
#define Nullable_1__ctor_m2967195975(__this, p0, method) ((  void (*) (Nullable_1_t4278248406 *, Color_t2555686324 , const RuntimeMethod*))Nullable_1__ctor_m2967195975_gshared)(__this, p0, method)
// System.Void DG.Tweening.Plugins.Core.PathCore.Path::.ctor(DG.Tweening.PathType,UnityEngine.Vector3[],System.Int32,System.Nullable`1<UnityEngine.Color>)
extern "C"  void Path__ctor_m3861603545 (Path_t3614338981 * __this, int32_t p0, Vector3U5BU5D_t1718750761* p1, int32_t p2, Nullable_1_t4278248406  p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenExtensions::Kill(DG.Tweening.Tween,System.Boolean)
extern "C"  void TweenExtensions_Kill_m1633104976 (RuntimeObject * __this /* static, unused */, Tween_t2342918553 * p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 DG.Tweening.TweenExtensions::Play<DG.Tweening.Tween>(!!0)
#define TweenExtensions_Play_TisTween_t2342918553_m1902296975(__this /* static, unused */, p0, method) ((  Tween_t2342918553 * (*) (RuntimeObject * /* static, unused */, Tween_t2342918553 *, const RuntimeMethod*))TweenExtensions_Play_TisRuntimeObject_m1290795777_gshared)(__this /* static, unused */, p0, method)
// System.Void DG.Tweening.TweenExtensions::PlayBackwards(DG.Tweening.Tween)
extern "C"  void TweenExtensions_PlayBackwards_m3827745165 (RuntimeObject * __this /* static, unused */, Tween_t2342918553 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenExtensions::PlayForward(DG.Tweening.Tween)
extern "C"  void TweenExtensions_PlayForward_m685237463 (RuntimeObject * __this /* static, unused */, Tween_t2342918553 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 DG.Tweening.TweenExtensions::Pause<DG.Tweening.Tween>(!!0)
#define TweenExtensions_Pause_TisTween_t2342918553_m1759043349(__this /* static, unused */, p0, method) ((  Tween_t2342918553 * (*) (RuntimeObject * /* static, unused */, Tween_t2342918553 *, const RuntimeMethod*))TweenExtensions_Pause_TisRuntimeObject_m1911018533_gshared)(__this /* static, unused */, p0, method)
// System.Void DG.Tweening.TweenExtensions::TogglePause(DG.Tweening.Tween)
extern "C"  void TweenExtensions_TogglePause_m3442996308 (RuntimeObject * __this /* static, unused */, Tween_t2342918553 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenExtensions::Rewind(DG.Tweening.Tween,System.Boolean)
extern "C"  void TweenExtensions_Rewind_m3650664630 (RuntimeObject * __this /* static, unused */, Tween_t2342918553 * p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.Debugger::LogNullTween(DG.Tweening.Tween)
extern "C"  void Debugger_LogNullTween_m4275844667 (RuntimeObject * __this /* static, unused */, Tween_t2342918553 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenExtensions::Restart(DG.Tweening.Tween,System.Boolean,System.Single)
extern "C"  void TweenExtensions_Restart_m4193702017 (RuntimeObject * __this /* static, unused */, Tween_t2342918553 * p0, bool p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenExtensions::Complete(DG.Tweening.Tween)
extern "C"  void TweenExtensions_Complete_m1826451228 (RuntimeObject * __this /* static, unused */, Tween_t2342918553 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.Debugger::LogInvalidTween(DG.Tweening.Tween)
extern "C"  void Debugger_LogInvalidTween_m382179192 (RuntimeObject * __this /* static, unused */, Tween_t2342918553 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.Debugger::LogWarning(System.Object)
extern "C"  void Debugger_LogWarning_m4070894882 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
#define List_1_get_Item_m200663048(__this, p0, method) ((  Vector3_t3722313464  (*) (List_1_t899420910 *, int32_t, const RuntimeMethod*))List_1_get_Item_m200663048_gshared)(__this, p0, method)
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3_op_Equality_m4231250055 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Addition_m779775034 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Keyframe::.ctor(System.Single,System.Single)
extern "C"  void Keyframe__ctor_m391431887 (Keyframe_t4206410242 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve__ctor_m1565662948 (AnimationCurve_t3046754366 * __this, KeyframeU5BU5D_t1068524471* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t3722313464  Vector3_get_forward_m3100859705 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C"  Vector3_t3722313464  Vector3_get_up_m3584168373 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
#define List_1__ctor_m2503402603(__this, method) ((  void (*) (List_1_t899420910 *, const RuntimeMethod*))List_1__ctor_m2503402603_gshared)(__this, method)
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m2943235014 (Color_t2555686324 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.ABSAnimationComponent::.ctor()
extern "C"  void ABSAnimationComponent__ctor_m4239799620 (ABSAnimationComponent_t262169234 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<DG.Tweening.Core.ABSAnimationComponent>()
#define Component_GetComponent_TisABSAnimationComponent_t262169234_m2746596450(__this, method) ((  ABSAnimationComponent_t262169234 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m565254235 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.Core.ABSAnimationComponent::.ctor()
extern "C"  void ABSAnimationComponent__ctor_m4239799620 (ABSAnimationComponent_t262169234 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.DOTweenPath::Awake()
extern "C"  void DOTweenPath_Awake_m2110691293 (DOTweenPath_t3192963685 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenPath_Awake_m2110691293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TweenerCore_3_t3040139253 * V_0 = NULL;
	Rigidbody_t3916780224 * V_1 = NULL;
	Transform_t3600365921 * V_2 = NULL;
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	ControlPoint_t3892672090  V_7;
	memset(&V_7, 0, sizeof(V_7));
	int32_t V_8 = 0;
	Nullable_1_t1149908250  V_9;
	memset(&V_9, 0, sizeof(V_9));
	TweenerCore_3_t3040139253 * G_B24_0 = NULL;
	TweenerCore_3_t3040139253 * G_B28_0 = NULL;
	{
		Path_t3614338981 * L_0 = __this->get_path_44();
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		List_1_t899420910 * L_1 = __this->get_wps_42();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m576380744(L_1, /*hidden argument*/List_1_get_Count_m576380744_RuntimeMethod_var);
		if ((((int32_t)L_2) < ((int32_t)1)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_3 = __this->get_inspectorMode_45();
		if ((!(((uint32_t)L_3) == ((uint32_t)3))))
		{
			goto IL_0020;
		}
	}

IL_001f:
	{
		return;
	}

IL_0020:
	{
		Path_t3614338981 * L_4 = __this->get_path_44();
		Path_t3614338981 * L_5 = __this->get_path_44();
		NullCheck(L_5);
		int32_t L_6 = L_5->get_type_3();
		NullCheck(L_4);
		Path_AssignDecoder_m3239877656(L_4, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2744875806_il2cpp_TypeInfo_var);
		bool L_7 = ((DOTween_t2744875806_StaticFields*)il2cpp_codegen_static_fields_for(DOTween_t2744875806_il2cpp_TypeInfo_var))->get_isUnityEditor_18();
		if (!L_7)
		{
			goto IL_0069;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2744875806_il2cpp_TypeInfo_var);
		List_1_t904863771 * L_8 = ((DOTween_t2744875806_StaticFields*)il2cpp_codegen_static_fields_for(DOTween_t2744875806_il2cpp_TypeInfo_var))->get_GizmosDelegates_22();
		Path_t3614338981 * L_9 = __this->get_path_44();
		intptr_t L_10 = (intptr_t)Path_Draw_m8564983_RuntimeMethod_var;
		TweenCallback_t3727756325 * L_11 = (TweenCallback_t3727756325 *)il2cpp_codegen_object_new(TweenCallback_t3727756325_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3086472496(L_11, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		List_1_Add_m2060438683(L_8, L_11, /*hidden argument*/List_1_Add_m2060438683_RuntimeMethod_var);
		Path_t3614338981 * L_12 = __this->get_path_44();
		Color_t2555686324  L_13 = __this->get_pathColor_53();
		NullCheck(L_12);
		L_12->set_gizmoColor_20(L_13);
	}

IL_0069:
	{
		bool L_14 = __this->get_isLocal_33();
		if (!L_14)
		{
			goto IL_015c;
		}
	}
	{
		Transform_t3600365921 * L_15 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		V_2 = L_15;
		Transform_t3600365921 * L_16 = V_2;
		NullCheck(L_16);
		Transform_t3600365921 * L_17 = Transform_get_parent_m835071599(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_18 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_17, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_015c;
		}
	}
	{
		Transform_t3600365921 * L_19 = V_2;
		NullCheck(L_19);
		Transform_t3600365921 * L_20 = Transform_get_parent_m835071599(L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		Transform_t3600365921 * L_21 = V_2;
		NullCheck(L_21);
		Vector3_t3722313464  L_22 = Transform_get_position_m36019626(L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		Path_t3614338981 * L_23 = __this->get_path_44();
		NullCheck(L_23);
		Vector3U5BU5D_t1718750761* L_24 = L_23->get_wps_6();
		NullCheck(L_24);
		V_4 = (((int32_t)((int32_t)(((RuntimeArray *)L_24)->max_length))));
		V_5 = 0;
		goto IL_00de;
	}

IL_00ae:
	{
		Path_t3614338981 * L_25 = __this->get_path_44();
		NullCheck(L_25);
		Vector3U5BU5D_t1718750761* L_26 = L_25->get_wps_6();
		int32_t L_27 = V_5;
		Path_t3614338981 * L_28 = __this->get_path_44();
		NullCheck(L_28);
		Vector3U5BU5D_t1718750761* L_29 = L_28->get_wps_6();
		int32_t L_30 = V_5;
		NullCheck(L_29);
		int32_t L_31 = L_30;
		Vector3_t3722313464  L_32 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		Vector3_t3722313464  L_33 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_34 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		NullCheck(L_26);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(L_27), (Vector3_t3722313464 )L_34);
		int32_t L_35 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_35, (int32_t)1));
	}

IL_00de:
	{
		int32_t L_36 = V_5;
		int32_t L_37 = V_4;
		if ((((int32_t)L_36) < ((int32_t)L_37)))
		{
			goto IL_00ae;
		}
	}
	{
		Path_t3614338981 * L_38 = __this->get_path_44();
		NullCheck(L_38);
		ControlPointU5BU5D_t1567961855* L_39 = L_38->get_controlPoints_7();
		NullCheck(L_39);
		V_4 = (((int32_t)((int32_t)(((RuntimeArray *)L_39)->max_length))));
		V_6 = 0;
		goto IL_0156;
	}

IL_00f8:
	{
		Path_t3614338981 * L_40 = __this->get_path_44();
		NullCheck(L_40);
		ControlPointU5BU5D_t1567961855* L_41 = L_40->get_controlPoints_7();
		int32_t L_42 = V_6;
		NullCheck(L_41);
		int32_t L_43 = L_42;
		ControlPoint_t3892672090  L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		V_7 = L_44;
		Vector3_t3722313464 * L_45 = (&V_7)->get_address_of_a_0();
		Vector3_t3722313464 * L_46 = L_45;
		Vector3_t3722313464  L_47 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_48 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, (*(Vector3_t3722313464 *)L_46), L_47, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)L_46 = L_48;
		Vector3_t3722313464 * L_49 = (&V_7)->get_address_of_b_1();
		Vector3_t3722313464 * L_50 = L_49;
		Vector3_t3722313464  L_51 = V_3;
		Vector3_t3722313464  L_52 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, (*(Vector3_t3722313464 *)L_50), L_51, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)L_50 = L_52;
		Path_t3614338981 * L_53 = __this->get_path_44();
		NullCheck(L_53);
		ControlPointU5BU5D_t1567961855* L_54 = L_53->get_controlPoints_7();
		int32_t L_55 = V_6;
		ControlPoint_t3892672090  L_56 = V_7;
		NullCheck(L_54);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(L_55), (ControlPoint_t3892672090 )L_56);
		int32_t L_57 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_57, (int32_t)1));
	}

IL_0156:
	{
		int32_t L_58 = V_6;
		int32_t L_59 = V_4;
		if ((((int32_t)L_58) < ((int32_t)L_59)))
		{
			goto IL_00f8;
		}
	}

IL_015c:
	{
		bool L_60 = __this->get_relative_32();
		if (!L_60)
		{
			goto IL_016a;
		}
	}
	{
		DOTweenPath_ReEvaluateRelativeTween_m1807793085(__this, /*hidden argument*/NULL);
	}

IL_016a:
	{
		int32_t L_61 = __this->get_pathMode_36();
		if ((!(((uint32_t)L_61) == ((uint32_t)1))))
		{
			goto IL_0188;
		}
	}
	{
		SpriteRenderer_t3235626157 * L_62 = Component_GetComponent_TisSpriteRenderer_t3235626157_m4281600501(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3235626157_m4281600501_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_63 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_62, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_63)
		{
			goto IL_0188;
		}
	}
	{
		__this->set_pathMode_36(2);
	}

IL_0188:
	{
		Rigidbody_t3916780224 * L_64 = Component_GetComponent_TisRigidbody_t3916780224_m2140521468(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t3916780224_m2140521468_RuntimeMethod_var);
		V_1 = L_64;
		bool L_65 = __this->get_tweenRigidbody_41();
		if (!L_65)
		{
			goto IL_0201;
		}
	}
	{
		Rigidbody_t3916780224 * L_66 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_67 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_66, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_67)
		{
			goto IL_0201;
		}
	}
	{
		bool L_68 = __this->get_isLocal_33();
		if (L_68)
		{
			goto IL_01d4;
		}
	}
	{
		Rigidbody_t3916780224 * L_69 = V_1;
		Path_t3614338981 * L_70 = __this->get_path_44();
		float L_71 = __this->get_duration_20();
		int32_t L_72 = __this->get_pathMode_36();
		TweenerCore_3_t3040139253 * L_73 = ShortcutExtensions_DOPath_m3404366840(NULL /*static, unused*/, L_69, L_70, L_71, L_72, /*hidden argument*/NULL);
		bool L_74 = __this->get_isClosedPath_34();
		int32_t L_75 = __this->get_lockRotation_37();
		TweenerCore_3_t3040139253 * L_76 = TweenSettingsExtensions_SetOptions_m2229198199(NULL /*static, unused*/, L_73, L_74, 0, L_75, /*hidden argument*/NULL);
		G_B24_0 = L_76;
		goto IL_01fe;
	}

IL_01d4:
	{
		Rigidbody_t3916780224 * L_77 = V_1;
		Path_t3614338981 * L_78 = __this->get_path_44();
		float L_79 = __this->get_duration_20();
		int32_t L_80 = __this->get_pathMode_36();
		TweenerCore_3_t3040139253 * L_81 = ShortcutExtensions_DOLocalPath_m2803671522(NULL /*static, unused*/, L_77, L_78, L_79, L_80, /*hidden argument*/NULL);
		bool L_82 = __this->get_isClosedPath_34();
		int32_t L_83 = __this->get_lockRotation_37();
		TweenerCore_3_t3040139253 * L_84 = TweenSettingsExtensions_SetOptions_m2229198199(NULL /*static, unused*/, L_81, L_82, 0, L_83, /*hidden argument*/NULL);
		G_B24_0 = L_84;
	}

IL_01fe:
	{
		V_0 = G_B24_0;
		goto IL_026a;
	}

IL_0201:
	{
		bool L_85 = __this->get_isLocal_33();
		if (L_85)
		{
			goto IL_023a;
		}
	}
	{
		Transform_t3600365921 * L_86 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Path_t3614338981 * L_87 = __this->get_path_44();
		float L_88 = __this->get_duration_20();
		int32_t L_89 = __this->get_pathMode_36();
		TweenerCore_3_t3040139253 * L_90 = ShortcutExtensions_DOPath_m1737185059(NULL /*static, unused*/, L_86, L_87, L_88, L_89, /*hidden argument*/NULL);
		bool L_91 = __this->get_isClosedPath_34();
		int32_t L_92 = __this->get_lockRotation_37();
		TweenerCore_3_t3040139253 * L_93 = TweenSettingsExtensions_SetOptions_m2229198199(NULL /*static, unused*/, L_90, L_91, 0, L_92, /*hidden argument*/NULL);
		G_B28_0 = L_93;
		goto IL_0269;
	}

IL_023a:
	{
		Transform_t3600365921 * L_94 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Path_t3614338981 * L_95 = __this->get_path_44();
		float L_96 = __this->get_duration_20();
		int32_t L_97 = __this->get_pathMode_36();
		TweenerCore_3_t3040139253 * L_98 = ShortcutExtensions_DOLocalPath_m3367906890(NULL /*static, unused*/, L_94, L_95, L_96, L_97, /*hidden argument*/NULL);
		bool L_99 = __this->get_isClosedPath_34();
		int32_t L_100 = __this->get_lockRotation_37();
		TweenerCore_3_t3040139253 * L_101 = TweenSettingsExtensions_SetOptions_m2229198199(NULL /*static, unused*/, L_98, L_99, 0, L_100, /*hidden argument*/NULL);
		G_B28_0 = L_101;
	}

IL_0269:
	{
		V_0 = G_B28_0;
	}

IL_026a:
	{
		int32_t L_102 = __this->get_orientType_26();
		V_8 = L_102;
		int32_t L_103 = V_8;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_103, (int32_t)1)))
		{
			case 0:
			{
				goto IL_0343;
			}
			case 1:
			{
				goto IL_028c;
			}
			case 2:
			{
				goto IL_02f3;
			}
		}
	}
	{
		goto IL_0391;
	}

IL_028c:
	{
		Transform_t3600365921 * L_104 = __this->get_lookAtTransform_27();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_105 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_104, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_105)
		{
			goto IL_0391;
		}
	}
	{
		bool L_106 = __this->get_assignForwardAndUp_38();
		if (!L_106)
		{
			goto IL_02cd;
		}
	}
	{
		TweenerCore_3_t3040139253 * L_107 = V_0;
		Transform_t3600365921 * L_108 = __this->get_lookAtTransform_27();
		Vector3_t3722313464  L_109 = __this->get_forwardDirection_39();
		Nullable_1_t1149908250  L_110;
		memset(&L_110, 0, sizeof(L_110));
		Nullable_1__ctor_m133295941((&L_110), L_109, /*hidden argument*/Nullable_1__ctor_m133295941_RuntimeMethod_var);
		Vector3_t3722313464  L_111 = __this->get_upDirection_40();
		Nullable_1_t1149908250  L_112;
		memset(&L_112, 0, sizeof(L_112));
		Nullable_1__ctor_m133295941((&L_112), L_111, /*hidden argument*/Nullable_1__ctor_m133295941_RuntimeMethod_var);
		TweenSettingsExtensions_SetLookAt_m2541161551(NULL /*static, unused*/, L_107, L_108, L_110, L_112, /*hidden argument*/NULL);
		goto IL_0391;
	}

IL_02cd:
	{
		TweenerCore_3_t3040139253 * L_113 = V_0;
		Transform_t3600365921 * L_114 = __this->get_lookAtTransform_27();
		il2cpp_codegen_initobj((&V_9), sizeof(Nullable_1_t1149908250 ));
		Nullable_1_t1149908250  L_115 = V_9;
		il2cpp_codegen_initobj((&V_9), sizeof(Nullable_1_t1149908250 ));
		Nullable_1_t1149908250  L_116 = V_9;
		TweenSettingsExtensions_SetLookAt_m2541161551(NULL /*static, unused*/, L_113, L_114, L_115, L_116, /*hidden argument*/NULL);
		goto IL_0391;
	}

IL_02f3:
	{
		bool L_117 = __this->get_assignForwardAndUp_38();
		if (!L_117)
		{
			goto IL_0320;
		}
	}
	{
		TweenerCore_3_t3040139253 * L_118 = V_0;
		Vector3_t3722313464  L_119 = __this->get_lookAtPosition_28();
		Vector3_t3722313464  L_120 = __this->get_forwardDirection_39();
		Nullable_1_t1149908250  L_121;
		memset(&L_121, 0, sizeof(L_121));
		Nullable_1__ctor_m133295941((&L_121), L_120, /*hidden argument*/Nullable_1__ctor_m133295941_RuntimeMethod_var);
		Vector3_t3722313464  L_122 = __this->get_upDirection_40();
		Nullable_1_t1149908250  L_123;
		memset(&L_123, 0, sizeof(L_123));
		Nullable_1__ctor_m133295941((&L_123), L_122, /*hidden argument*/Nullable_1__ctor_m133295941_RuntimeMethod_var);
		TweenSettingsExtensions_SetLookAt_m1165050034(NULL /*static, unused*/, L_118, L_119, L_121, L_123, /*hidden argument*/NULL);
		goto IL_0391;
	}

IL_0320:
	{
		TweenerCore_3_t3040139253 * L_124 = V_0;
		Vector3_t3722313464  L_125 = __this->get_lookAtPosition_28();
		il2cpp_codegen_initobj((&V_9), sizeof(Nullable_1_t1149908250 ));
		Nullable_1_t1149908250  L_126 = V_9;
		il2cpp_codegen_initobj((&V_9), sizeof(Nullable_1_t1149908250 ));
		Nullable_1_t1149908250  L_127 = V_9;
		TweenSettingsExtensions_SetLookAt_m1165050034(NULL /*static, unused*/, L_124, L_125, L_126, L_127, /*hidden argument*/NULL);
		goto IL_0391;
	}

IL_0343:
	{
		bool L_128 = __this->get_assignForwardAndUp_38();
		if (!L_128)
		{
			goto IL_0370;
		}
	}
	{
		TweenerCore_3_t3040139253 * L_129 = V_0;
		float L_130 = __this->get_lookAhead_29();
		Vector3_t3722313464  L_131 = __this->get_forwardDirection_39();
		Nullable_1_t1149908250  L_132;
		memset(&L_132, 0, sizeof(L_132));
		Nullable_1__ctor_m133295941((&L_132), L_131, /*hidden argument*/Nullable_1__ctor_m133295941_RuntimeMethod_var);
		Vector3_t3722313464  L_133 = __this->get_upDirection_40();
		Nullable_1_t1149908250  L_134;
		memset(&L_134, 0, sizeof(L_134));
		Nullable_1__ctor_m133295941((&L_134), L_133, /*hidden argument*/Nullable_1__ctor_m133295941_RuntimeMethod_var);
		TweenSettingsExtensions_SetLookAt_m3381879294(NULL /*static, unused*/, L_129, L_130, L_132, L_134, /*hidden argument*/NULL);
		goto IL_0391;
	}

IL_0370:
	{
		TweenerCore_3_t3040139253 * L_135 = V_0;
		float L_136 = __this->get_lookAhead_29();
		il2cpp_codegen_initobj((&V_9), sizeof(Nullable_1_t1149908250 ));
		Nullable_1_t1149908250  L_137 = V_9;
		il2cpp_codegen_initobj((&V_9), sizeof(Nullable_1_t1149908250 ));
		Nullable_1_t1149908250  L_138 = V_9;
		TweenSettingsExtensions_SetLookAt_m3381879294(NULL /*static, unused*/, L_135, L_136, L_137, L_138, /*hidden argument*/NULL);
	}

IL_0391:
	{
		TweenerCore_3_t3040139253 * L_139 = V_0;
		float L_140 = __this->get_delay_19();
		TweenerCore_3_t3040139253 * L_141 = TweenSettingsExtensions_SetDelay_TisTweenerCore_3_t3040139253_m1178560492(NULL /*static, unused*/, L_139, L_140, /*hidden argument*/TweenSettingsExtensions_SetDelay_TisTweenerCore_3_t3040139253_m1178560492_RuntimeMethod_var);
		int32_t L_142 = __this->get_loops_23();
		int32_t L_143 = __this->get_loopType_25();
		TweenerCore_3_t3040139253 * L_144 = TweenSettingsExtensions_SetLoops_TisTweenerCore_3_t3040139253_m3803960963(NULL /*static, unused*/, L_141, L_142, L_143, /*hidden argument*/TweenSettingsExtensions_SetLoops_TisTweenerCore_3_t3040139253_m3803960963_RuntimeMethod_var);
		bool L_145 = __this->get_autoKill_31();
		TweenerCore_3_t3040139253 * L_146 = TweenSettingsExtensions_SetAutoKill_TisTweenerCore_3_t3040139253_m1506332555(NULL /*static, unused*/, L_144, L_145, /*hidden argument*/TweenSettingsExtensions_SetAutoKill_TisTweenerCore_3_t3040139253_m1506332555_RuntimeMethod_var);
		int32_t L_147 = ((ABSAnimationComponent_t262169234 *)__this)->get_updateType_2();
		TweenerCore_3_t3040139253 * L_148 = TweenSettingsExtensions_SetUpdate_TisTweenerCore_3_t3040139253_m2375151364(NULL /*static, unused*/, L_146, L_147, /*hidden argument*/TweenSettingsExtensions_SetUpdate_TisTweenerCore_3_t3040139253_m2375151364_RuntimeMethod_var);
		intptr_t L_149 = (intptr_t)DOTweenPath_U3CAwakeU3Eb__38_0_m3881372633_RuntimeMethod_var;
		TweenCallback_t3727756325 * L_150 = (TweenCallback_t3727756325 *)il2cpp_codegen_object_new(TweenCallback_t3727756325_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3086472496(L_150, __this, L_149, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnKill_TisTweenerCore_3_t3040139253_m2092851215(NULL /*static, unused*/, L_148, L_150, /*hidden argument*/TweenSettingsExtensions_OnKill_TisTweenerCore_3_t3040139253_m2092851215_RuntimeMethod_var);
		bool L_151 = ((ABSAnimationComponent_t262169234 *)__this)->get_isSpeedBased_3();
		if (!L_151)
		{
			goto IL_03e5;
		}
	}
	{
		TweenerCore_3_t3040139253 * L_152 = V_0;
		TweenSettingsExtensions_SetSpeedBased_TisTweenerCore_3_t3040139253_m914173068(NULL /*static, unused*/, L_152, /*hidden argument*/TweenSettingsExtensions_SetSpeedBased_TisTweenerCore_3_t3040139253_m914173068_RuntimeMethod_var);
	}

IL_03e5:
	{
		int32_t L_153 = __this->get_easeType_21();
		if ((!(((uint32_t)L_153) == ((uint32_t)((int32_t)37)))))
		{
			goto IL_03fe;
		}
	}
	{
		TweenerCore_3_t3040139253 * L_154 = V_0;
		AnimationCurve_t3046754366 * L_155 = __this->get_easeCurve_22();
		TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3040139253_m3071670034(NULL /*static, unused*/, L_154, L_155, /*hidden argument*/TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3040139253_m3071670034_RuntimeMethod_var);
		goto IL_040b;
	}

IL_03fe:
	{
		TweenerCore_3_t3040139253 * L_156 = V_0;
		int32_t L_157 = __this->get_easeType_21();
		TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3040139253_m2476880271(NULL /*static, unused*/, L_156, L_157, /*hidden argument*/TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3040139253_m2476880271_RuntimeMethod_var);
	}

IL_040b:
	{
		String_t* L_158 = __this->get_id_24();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_159 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_158, /*hidden argument*/NULL);
		if (L_159)
		{
			goto IL_0425;
		}
	}
	{
		TweenerCore_3_t3040139253 * L_160 = V_0;
		String_t* L_161 = __this->get_id_24();
		TweenSettingsExtensions_SetId_TisTweenerCore_3_t3040139253_m994168359(NULL /*static, unused*/, L_160, L_161, /*hidden argument*/TweenSettingsExtensions_SetId_TisTweenerCore_3_t3040139253_m994168359_RuntimeMethod_var);
	}

IL_0425:
	{
		bool L_162 = ((ABSAnimationComponent_t262169234 *)__this)->get_hasOnStart_4();
		if (!L_162)
		{
			goto IL_044f;
		}
	}
	{
		UnityEvent_t2581268647 * L_163 = ((ABSAnimationComponent_t262169234 *)__this)->get_onStart_11();
		if (!L_163)
		{
			goto IL_0456;
		}
	}
	{
		TweenerCore_3_t3040139253 * L_164 = V_0;
		UnityEvent_t2581268647 * L_165 = ((ABSAnimationComponent_t262169234 *)__this)->get_onStart_11();
		intptr_t L_166 = (intptr_t)UnityEvent_Invoke_m3065672636_RuntimeMethod_var;
		TweenCallback_t3727756325 * L_167 = (TweenCallback_t3727756325 *)il2cpp_codegen_object_new(TweenCallback_t3727756325_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3086472496(L_167, L_165, L_166, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnStart_TisTweenerCore_3_t3040139253_m2399400913(NULL /*static, unused*/, L_164, L_167, /*hidden argument*/TweenSettingsExtensions_OnStart_TisTweenerCore_3_t3040139253_m2399400913_RuntimeMethod_var);
		goto IL_0456;
	}

IL_044f:
	{
		((ABSAnimationComponent_t262169234 *)__this)->set_onStart_11((UnityEvent_t2581268647 *)NULL);
	}

IL_0456:
	{
		bool L_168 = ((ABSAnimationComponent_t262169234 *)__this)->get_hasOnPlay_5();
		if (!L_168)
		{
			goto IL_0480;
		}
	}
	{
		UnityEvent_t2581268647 * L_169 = ((ABSAnimationComponent_t262169234 *)__this)->get_onPlay_12();
		if (!L_169)
		{
			goto IL_0487;
		}
	}
	{
		TweenerCore_3_t3040139253 * L_170 = V_0;
		UnityEvent_t2581268647 * L_171 = ((ABSAnimationComponent_t262169234 *)__this)->get_onPlay_12();
		intptr_t L_172 = (intptr_t)UnityEvent_Invoke_m3065672636_RuntimeMethod_var;
		TweenCallback_t3727756325 * L_173 = (TweenCallback_t3727756325 *)il2cpp_codegen_object_new(TweenCallback_t3727756325_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3086472496(L_173, L_171, L_172, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnPlay_TisTweenerCore_3_t3040139253_m3727247487(NULL /*static, unused*/, L_170, L_173, /*hidden argument*/TweenSettingsExtensions_OnPlay_TisTweenerCore_3_t3040139253_m3727247487_RuntimeMethod_var);
		goto IL_0487;
	}

IL_0480:
	{
		((ABSAnimationComponent_t262169234 *)__this)->set_onPlay_12((UnityEvent_t2581268647 *)NULL);
	}

IL_0487:
	{
		bool L_174 = ((ABSAnimationComponent_t262169234 *)__this)->get_hasOnUpdate_6();
		if (!L_174)
		{
			goto IL_04b1;
		}
	}
	{
		UnityEvent_t2581268647 * L_175 = ((ABSAnimationComponent_t262169234 *)__this)->get_onUpdate_13();
		if (!L_175)
		{
			goto IL_04b8;
		}
	}
	{
		TweenerCore_3_t3040139253 * L_176 = V_0;
		UnityEvent_t2581268647 * L_177 = ((ABSAnimationComponent_t262169234 *)__this)->get_onUpdate_13();
		intptr_t L_178 = (intptr_t)UnityEvent_Invoke_m3065672636_RuntimeMethod_var;
		TweenCallback_t3727756325 * L_179 = (TweenCallback_t3727756325 *)il2cpp_codegen_object_new(TweenCallback_t3727756325_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3086472496(L_179, L_177, L_178, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnUpdate_TisTweenerCore_3_t3040139253_m3414834117(NULL /*static, unused*/, L_176, L_179, /*hidden argument*/TweenSettingsExtensions_OnUpdate_TisTweenerCore_3_t3040139253_m3414834117_RuntimeMethod_var);
		goto IL_04b8;
	}

IL_04b1:
	{
		((ABSAnimationComponent_t262169234 *)__this)->set_onUpdate_13((UnityEvent_t2581268647 *)NULL);
	}

IL_04b8:
	{
		bool L_180 = ((ABSAnimationComponent_t262169234 *)__this)->get_hasOnStepComplete_7();
		if (!L_180)
		{
			goto IL_04e2;
		}
	}
	{
		UnityEvent_t2581268647 * L_181 = ((ABSAnimationComponent_t262169234 *)__this)->get_onStepComplete_14();
		if (!L_181)
		{
			goto IL_04e9;
		}
	}
	{
		TweenerCore_3_t3040139253 * L_182 = V_0;
		UnityEvent_t2581268647 * L_183 = ((ABSAnimationComponent_t262169234 *)__this)->get_onStepComplete_14();
		intptr_t L_184 = (intptr_t)UnityEvent_Invoke_m3065672636_RuntimeMethod_var;
		TweenCallback_t3727756325 * L_185 = (TweenCallback_t3727756325 *)il2cpp_codegen_object_new(TweenCallback_t3727756325_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3086472496(L_185, L_183, L_184, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnStepComplete_TisTweenerCore_3_t3040139253_m2372572972(NULL /*static, unused*/, L_182, L_185, /*hidden argument*/TweenSettingsExtensions_OnStepComplete_TisTweenerCore_3_t3040139253_m2372572972_RuntimeMethod_var);
		goto IL_04e9;
	}

IL_04e2:
	{
		((ABSAnimationComponent_t262169234 *)__this)->set_onStepComplete_14((UnityEvent_t2581268647 *)NULL);
	}

IL_04e9:
	{
		bool L_186 = ((ABSAnimationComponent_t262169234 *)__this)->get_hasOnComplete_8();
		if (!L_186)
		{
			goto IL_0513;
		}
	}
	{
		UnityEvent_t2581268647 * L_187 = ((ABSAnimationComponent_t262169234 *)__this)->get_onComplete_15();
		if (!L_187)
		{
			goto IL_051a;
		}
	}
	{
		TweenerCore_3_t3040139253 * L_188 = V_0;
		UnityEvent_t2581268647 * L_189 = ((ABSAnimationComponent_t262169234 *)__this)->get_onComplete_15();
		intptr_t L_190 = (intptr_t)UnityEvent_Invoke_m3065672636_RuntimeMethod_var;
		TweenCallback_t3727756325 * L_191 = (TweenCallback_t3727756325 *)il2cpp_codegen_object_new(TweenCallback_t3727756325_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3086472496(L_191, L_189, L_190, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnComplete_TisTweenerCore_3_t3040139253_m371879841(NULL /*static, unused*/, L_188, L_191, /*hidden argument*/TweenSettingsExtensions_OnComplete_TisTweenerCore_3_t3040139253_m371879841_RuntimeMethod_var);
		goto IL_051a;
	}

IL_0513:
	{
		((ABSAnimationComponent_t262169234 *)__this)->set_onComplete_15((UnityEvent_t2581268647 *)NULL);
	}

IL_051a:
	{
		bool L_192 = ((ABSAnimationComponent_t262169234 *)__this)->get_hasOnRewind_10();
		if (!L_192)
		{
			goto IL_0544;
		}
	}
	{
		UnityEvent_t2581268647 * L_193 = ((ABSAnimationComponent_t262169234 *)__this)->get_onRewind_17();
		if (!L_193)
		{
			goto IL_054b;
		}
	}
	{
		TweenerCore_3_t3040139253 * L_194 = V_0;
		UnityEvent_t2581268647 * L_195 = ((ABSAnimationComponent_t262169234 *)__this)->get_onRewind_17();
		intptr_t L_196 = (intptr_t)UnityEvent_Invoke_m3065672636_RuntimeMethod_var;
		TweenCallback_t3727756325 * L_197 = (TweenCallback_t3727756325 *)il2cpp_codegen_object_new(TweenCallback_t3727756325_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3086472496(L_197, L_195, L_196, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnRewind_TisTweenerCore_3_t3040139253_m2866982129(NULL /*static, unused*/, L_194, L_197, /*hidden argument*/TweenSettingsExtensions_OnRewind_TisTweenerCore_3_t3040139253_m2866982129_RuntimeMethod_var);
		goto IL_054b;
	}

IL_0544:
	{
		((ABSAnimationComponent_t262169234 *)__this)->set_onRewind_17((UnityEvent_t2581268647 *)NULL);
	}

IL_054b:
	{
		bool L_198 = __this->get_autoPlay_30();
		if (!L_198)
		{
			goto IL_055c;
		}
	}
	{
		TweenerCore_3_t3040139253 * L_199 = V_0;
		TweenExtensions_Play_TisTweenerCore_3_t3040139253_m2536939817(NULL /*static, unused*/, L_199, /*hidden argument*/TweenExtensions_Play_TisTweenerCore_3_t3040139253_m2536939817_RuntimeMethod_var);
		goto IL_0563;
	}

IL_055c:
	{
		TweenerCore_3_t3040139253 * L_200 = V_0;
		TweenExtensions_Pause_TisTweenerCore_3_t3040139253_m1549830538(NULL /*static, unused*/, L_200, /*hidden argument*/TweenExtensions_Pause_TisTweenerCore_3_t3040139253_m1549830538_RuntimeMethod_var);
	}

IL_0563:
	{
		TweenerCore_3_t3040139253 * L_201 = V_0;
		((ABSAnimationComponent_t262169234 *)__this)->set_tween_18(L_201);
		bool L_202 = ((ABSAnimationComponent_t262169234 *)__this)->get_hasOnTweenCreated_9();
		if (!L_202)
		{
			goto IL_0585;
		}
	}
	{
		UnityEvent_t2581268647 * L_203 = ((ABSAnimationComponent_t262169234 *)__this)->get_onTweenCreated_16();
		if (!L_203)
		{
			goto IL_0585;
		}
	}
	{
		UnityEvent_t2581268647 * L_204 = ((ABSAnimationComponent_t262169234 *)__this)->get_onTweenCreated_16();
		NullCheck(L_204);
		UnityEvent_Invoke_m3065672636(L_204, /*hidden argument*/NULL);
	}

IL_0585:
	{
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::Reset()
extern "C"  void DOTweenPath_Reset_m1656620755 (DOTweenPath_t3192963685 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenPath_Reset_m1656620755_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_pathType_46();
		List_1_t899420910 * L_1 = __this->get_wps_42();
		NullCheck(L_1);
		Vector3U5BU5D_t1718750761* L_2 = List_1_ToArray_m466715028(L_1, /*hidden argument*/List_1_ToArray_m466715028_RuntimeMethod_var);
		Color_t2555686324  L_3 = __this->get_pathColor_53();
		Nullable_1_t4278248406  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Nullable_1__ctor_m2967195975((&L_4), L_3, /*hidden argument*/Nullable_1__ctor_m2967195975_RuntimeMethod_var);
		Path_t3614338981 * L_5 = (Path_t3614338981 *)il2cpp_codegen_object_new(Path_t3614338981_il2cpp_TypeInfo_var);
		Path__ctor_m3861603545(L_5, L_0, L_2, ((int32_t)10), L_4, /*hidden argument*/NULL);
		__this->set_path_44(L_5);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::OnDestroy()
extern "C"  void DOTweenPath_OnDestroy_m993684891 (DOTweenPath_t3192963685 * __this, const RuntimeMethod* method)
{
	{
		Tween_t2342918553 * L_0 = ((ABSAnimationComponent_t262169234 *)__this)->get_tween_18();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Tween_t2342918553 * L_1 = ((ABSAnimationComponent_t262169234 *)__this)->get_tween_18();
		NullCheck(L_1);
		bool L_2 = L_1->get_active_35();
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		Tween_t2342918553 * L_3 = ((ABSAnimationComponent_t262169234 *)__this)->get_tween_18();
		TweenExtensions_Kill_m1633104976(NULL /*static, unused*/, L_3, (bool)0, /*hidden argument*/NULL);
	}

IL_0021:
	{
		((ABSAnimationComponent_t262169234 *)__this)->set_tween_18((Tween_t2342918553 *)NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DOPlay()
extern "C"  void DOTweenPath_DOPlay_m1915979208 (DOTweenPath_t3192963685 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenPath_DOPlay_m1915979208_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Tween_t2342918553 * L_0 = ((ABSAnimationComponent_t262169234 *)__this)->get_tween_18();
		TweenExtensions_Play_TisTween_t2342918553_m1902296975(NULL /*static, unused*/, L_0, /*hidden argument*/TweenExtensions_Play_TisTween_t2342918553_m1902296975_RuntimeMethod_var);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DOPlayBackwards()
extern "C"  void DOTweenPath_DOPlayBackwards_m4267902458 (DOTweenPath_t3192963685 * __this, const RuntimeMethod* method)
{
	{
		Tween_t2342918553 * L_0 = ((ABSAnimationComponent_t262169234 *)__this)->get_tween_18();
		TweenExtensions_PlayBackwards_m3827745165(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DOPlayForward()
extern "C"  void DOTweenPath_DOPlayForward_m1030105119 (DOTweenPath_t3192963685 * __this, const RuntimeMethod* method)
{
	{
		Tween_t2342918553 * L_0 = ((ABSAnimationComponent_t262169234 *)__this)->get_tween_18();
		TweenExtensions_PlayForward_m685237463(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DOPause()
extern "C"  void DOTweenPath_DOPause_m2214012112 (DOTweenPath_t3192963685 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenPath_DOPause_m2214012112_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Tween_t2342918553 * L_0 = ((ABSAnimationComponent_t262169234 *)__this)->get_tween_18();
		TweenExtensions_Pause_TisTween_t2342918553_m1759043349(NULL /*static, unused*/, L_0, /*hidden argument*/TweenExtensions_Pause_TisTween_t2342918553_m1759043349_RuntimeMethod_var);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DOTogglePause()
extern "C"  void DOTweenPath_DOTogglePause_m2317917341 (DOTweenPath_t3192963685 * __this, const RuntimeMethod* method)
{
	{
		Tween_t2342918553 * L_0 = ((ABSAnimationComponent_t262169234 *)__this)->get_tween_18();
		TweenExtensions_TogglePause_m3442996308(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DORewind()
extern "C"  void DOTweenPath_DORewind_m3870791404 (DOTweenPath_t3192963685 * __this, const RuntimeMethod* method)
{
	{
		Tween_t2342918553 * L_0 = ((ABSAnimationComponent_t262169234 *)__this)->get_tween_18();
		TweenExtensions_Rewind_m3650664630(NULL /*static, unused*/, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DORestart(System.Boolean)
extern "C"  void DOTweenPath_DORestart_m3261849928 (DOTweenPath_t3192963685 * __this, bool ___fromHere0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenPath_DORestart_m3261849928_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Tween_t2342918553 * L_0 = ((ABSAnimationComponent_t262169234 *)__this)->get_tween_18();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1756157868_StaticFields*)il2cpp_codegen_static_fields_for(Debugger_t1756157868_il2cpp_TypeInfo_var))->get_logPriority_0();
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_001b;
		}
	}
	{
		Tween_t2342918553 * L_2 = ((ABSAnimationComponent_t262169234 *)__this)->get_tween_18();
		Debugger_LogNullTween_m4275844667(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}

IL_001c:
	{
		bool L_3 = ___fromHere0;
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		bool L_4 = __this->get_relative_32();
		if (!L_4)
		{
			goto IL_0035;
		}
	}
	{
		bool L_5 = __this->get_isLocal_33();
		if (L_5)
		{
			goto IL_0035;
		}
	}
	{
		DOTweenPath_ReEvaluateRelativeTween_m1807793085(__this, /*hidden argument*/NULL);
	}

IL_0035:
	{
		Tween_t2342918553 * L_6 = ((ABSAnimationComponent_t262169234 *)__this)->get_tween_18();
		TweenExtensions_Restart_m4193702017(NULL /*static, unused*/, L_6, (bool)1, (-1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DOComplete()
extern "C"  void DOTweenPath_DOComplete_m1298430917 (DOTweenPath_t3192963685 * __this, const RuntimeMethod* method)
{
	{
		Tween_t2342918553 * L_0 = ((ABSAnimationComponent_t262169234 *)__this)->get_tween_18();
		TweenExtensions_Complete_m1826451228(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DOKill()
extern "C"  void DOTweenPath_DOKill_m3819449880 (DOTweenPath_t3192963685 * __this, const RuntimeMethod* method)
{
	{
		Tween_t2342918553 * L_0 = ((ABSAnimationComponent_t262169234 *)__this)->get_tween_18();
		TweenExtensions_Kill_m1633104976(NULL /*static, unused*/, L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// DG.Tweening.Tween DG.Tweening.DOTweenPath::GetTween()
extern "C"  Tween_t2342918553 * DOTweenPath_GetTween_m2361100768 (DOTweenPath_t3192963685 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenPath_GetTween_m2361100768_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Tween_t2342918553 * L_0 = ((ABSAnimationComponent_t262169234 *)__this)->get_tween_18();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		Tween_t2342918553 * L_1 = ((ABSAnimationComponent_t262169234 *)__this)->get_tween_18();
		NullCheck(L_1);
		bool L_2 = L_1->get_active_35();
		if (L_2)
		{
			goto IL_003f;
		}
	}

IL_0015:
	{
		int32_t L_3 = ((Debugger_t1756157868_StaticFields*)il2cpp_codegen_static_fields_for(Debugger_t1756157868_il2cpp_TypeInfo_var))->get_logPriority_0();
		if ((((int32_t)L_3) <= ((int32_t)1)))
		{
			goto IL_003d;
		}
	}
	{
		Tween_t2342918553 * L_4 = ((ABSAnimationComponent_t262169234 *)__this)->get_tween_18();
		if (L_4)
		{
			goto IL_0032;
		}
	}
	{
		Tween_t2342918553 * L_5 = ((ABSAnimationComponent_t262169234 *)__this)->get_tween_18();
		Debugger_LogNullTween_m4275844667(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		goto IL_003d;
	}

IL_0032:
	{
		Tween_t2342918553 * L_6 = ((ABSAnimationComponent_t262169234 *)__this)->get_tween_18();
		Debugger_LogInvalidTween_m382179192(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return (Tween_t2342918553 *)NULL;
	}

IL_003f:
	{
		Tween_t2342918553 * L_7 = ((ABSAnimationComponent_t262169234 *)__this)->get_tween_18();
		return L_7;
	}
}
// UnityEngine.Vector3[] DG.Tweening.DOTweenPath::GetDrawPoints()
extern "C"  Vector3U5BU5D_t1718750761* DOTweenPath_GetDrawPoints_m2910045167 (DOTweenPath_t3192963685 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenPath_GetDrawPoints_m2910045167_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Path_t3614338981 * L_0 = __this->get_path_44();
		NullCheck(L_0);
		Vector3U5BU5D_t1718750761* L_1 = L_0->get_wps_6();
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		Path_t3614338981 * L_2 = __this->get_path_44();
		NullCheck(L_2);
		Vector3U5BU5D_t1718750761* L_3 = L_2->get_nonLinearDrawWps_17();
		if (L_3)
		{
			goto IL_0026;
		}
	}

IL_001a:
	{
		Debugger_LogWarning_m4070894882(NULL /*static, unused*/, _stringLiteral2966973123, /*hidden argument*/NULL);
		return (Vector3U5BU5D_t1718750761*)NULL;
	}

IL_0026:
	{
		int32_t L_4 = __this->get_pathType_46();
		if (L_4)
		{
			goto IL_003a;
		}
	}
	{
		Path_t3614338981 * L_5 = __this->get_path_44();
		NullCheck(L_5);
		Vector3U5BU5D_t1718750761* L_6 = L_5->get_wps_6();
		return L_6;
	}

IL_003a:
	{
		Path_t3614338981 * L_7 = __this->get_path_44();
		NullCheck(L_7);
		Vector3U5BU5D_t1718750761* L_8 = L_7->get_nonLinearDrawWps_17();
		return L_8;
	}
}
// UnityEngine.Vector3[] DG.Tweening.DOTweenPath::GetFullWps()
extern "C"  Vector3U5BU5D_t1718750761* DOTweenPath_GetFullWps_m4236143325 (DOTweenPath_t3192963685 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenPath_GetFullWps_m4236143325_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Vector3U5BU5D_t1718750761* V_2 = NULL;
	int32_t V_3 = 0;
	{
		List_1_t899420910 * L_0 = __this->get_wps_42();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m576380744(L_0, /*hidden argument*/List_1_get_Count_m576380744_RuntimeMethod_var);
		V_0 = L_1;
		int32_t L_2 = V_0;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)1));
		bool L_3 = __this->get_isClosedPath_34();
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_4 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_001c:
	{
		int32_t L_5 = V_1;
		V_2 = ((Vector3U5BU5D_t1718750761*)SZArrayNew(Vector3U5BU5D_t1718750761_il2cpp_TypeInfo_var, (uint32_t)L_5));
		Vector3U5BU5D_t1718750761* L_6 = V_2;
		Transform_t3600365921 * L_7 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t3722313464  L_8 = Transform_get_position_m36019626(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (Vector3_t3722313464 )L_8);
		V_3 = 0;
		goto IL_0052;
	}

IL_0039:
	{
		Vector3U5BU5D_t1718750761* L_9 = V_2;
		int32_t L_10 = V_3;
		List_1_t899420910 * L_11 = __this->get_wps_42();
		int32_t L_12 = V_3;
		NullCheck(L_11);
		Vector3_t3722313464  L_13 = List_1_get_Item_m200663048(L_11, L_12, /*hidden argument*/List_1_get_Item_m200663048_RuntimeMethod_var);
		NullCheck(L_9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1))), (Vector3_t3722313464 )L_13);
		int32_t L_14 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_0052:
	{
		int32_t L_15 = V_3;
		int32_t L_16 = V_0;
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0039;
		}
	}
	{
		bool L_17 = __this->get_isClosedPath_34();
		if (!L_17)
		{
			goto IL_006e;
		}
	}
	{
		Vector3U5BU5D_t1718750761* L_18 = V_2;
		int32_t L_19 = V_1;
		Vector3U5BU5D_t1718750761* L_20 = V_2;
		NullCheck(L_20);
		int32_t L_21 = 0;
		Vector3_t3722313464  L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_18);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_subtract((int32_t)L_19, (int32_t)1))), (Vector3_t3722313464 )L_22);
	}

IL_006e:
	{
		Vector3U5BU5D_t1718750761* L_23 = V_2;
		return L_23;
	}
}
// System.Void DG.Tweening.DOTweenPath::ReEvaluateRelativeTween()
extern "C"  void DOTweenPath_ReEvaluateRelativeTween_m1807793085 (DOTweenPath_t3192963685 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenPath_ReEvaluateRelativeTween_m1807793085_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	ControlPoint_t3892672090  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector3_t3722313464  L_2 = V_0;
		Vector3_t3722313464  L_3 = __this->get_lastSrcPosition_54();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		bool L_4 = Vector3_op_Equality_m4231250055(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001b;
		}
	}
	{
		return;
	}

IL_001b:
	{
		Vector3_t3722313464  L_5 = V_0;
		Vector3_t3722313464  L_6 = __this->get_lastSrcPosition_54();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_7 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		Path_t3614338981 * L_8 = __this->get_path_44();
		NullCheck(L_8);
		Vector3U5BU5D_t1718750761* L_9 = L_8->get_wps_6();
		NullCheck(L_9);
		V_2 = (((int32_t)((int32_t)(((RuntimeArray *)L_9)->max_length))));
		V_3 = 0;
		goto IL_0066;
	}

IL_003a:
	{
		Path_t3614338981 * L_10 = __this->get_path_44();
		NullCheck(L_10);
		Vector3U5BU5D_t1718750761* L_11 = L_10->get_wps_6();
		int32_t L_12 = V_3;
		Path_t3614338981 * L_13 = __this->get_path_44();
		NullCheck(L_13);
		Vector3U5BU5D_t1718750761* L_14 = L_13->get_wps_6();
		int32_t L_15 = V_3;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Vector3_t3722313464  L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		Vector3_t3722313464  L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_19 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_11);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(L_12), (Vector3_t3722313464 )L_19);
		int32_t L_20 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1));
	}

IL_0066:
	{
		int32_t L_21 = V_3;
		int32_t L_22 = V_2;
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_003a;
		}
	}
	{
		Path_t3614338981 * L_23 = __this->get_path_44();
		NullCheck(L_23);
		ControlPointU5BU5D_t1567961855* L_24 = L_23->get_controlPoints_7();
		NullCheck(L_24);
		V_2 = (((int32_t)((int32_t)(((RuntimeArray *)L_24)->max_length))));
		V_4 = 0;
		goto IL_00db;
	}

IL_007d:
	{
		Path_t3614338981 * L_25 = __this->get_path_44();
		NullCheck(L_25);
		ControlPointU5BU5D_t1567961855* L_26 = L_25->get_controlPoints_7();
		int32_t L_27 = V_4;
		NullCheck(L_26);
		int32_t L_28 = L_27;
		ControlPoint_t3892672090  L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		V_5 = L_29;
		Vector3_t3722313464 * L_30 = (&V_5)->get_address_of_a_0();
		Vector3_t3722313464 * L_31 = L_30;
		Vector3_t3722313464  L_32 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_33 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, (*(Vector3_t3722313464 *)L_31), L_32, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)L_31 = L_33;
		Vector3_t3722313464 * L_34 = (&V_5)->get_address_of_b_1();
		Vector3_t3722313464 * L_35 = L_34;
		Vector3_t3722313464  L_36 = V_1;
		Vector3_t3722313464  L_37 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, (*(Vector3_t3722313464 *)L_35), L_36, /*hidden argument*/NULL);
		*(Vector3_t3722313464 *)L_35 = L_37;
		Path_t3614338981 * L_38 = __this->get_path_44();
		NullCheck(L_38);
		ControlPointU5BU5D_t1567961855* L_39 = L_38->get_controlPoints_7();
		int32_t L_40 = V_4;
		ControlPoint_t3892672090  L_41 = V_5;
		NullCheck(L_39);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(L_40), (ControlPoint_t3892672090 )L_41);
		int32_t L_42 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_42, (int32_t)1));
	}

IL_00db:
	{
		int32_t L_43 = V_4;
		int32_t L_44 = V_2;
		if ((((int32_t)L_43) < ((int32_t)L_44)))
		{
			goto IL_007d;
		}
	}
	{
		Vector3_t3722313464  L_45 = V_0;
		__this->set_lastSrcPosition_54(L_45);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::.ctor()
extern "C"  void DOTweenPath__ctor_m1702575186 (DOTweenPath_t3192963685 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenPath__ctor_m1702575186_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_duration_20((1.0f));
		__this->set_easeType_21(6);
		KeyframeU5BU5D_t1068524471* L_0 = ((KeyframeU5BU5D_t1068524471*)SZArrayNew(KeyframeU5BU5D_t1068524471_il2cpp_TypeInfo_var, (uint32_t)2));
		Keyframe_t4206410242  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Keyframe__ctor_m391431887((&L_1), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Keyframe_t4206410242 )L_1);
		KeyframeU5BU5D_t1068524471* L_2 = L_0;
		Keyframe_t4206410242  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Keyframe__ctor_m391431887((&L_3), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Keyframe_t4206410242 )L_3);
		AnimationCurve_t3046754366 * L_4 = (AnimationCurve_t3046754366 *)il2cpp_codegen_object_new(AnimationCurve_t3046754366_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m1565662948(L_4, L_2, /*hidden argument*/NULL);
		__this->set_easeCurve_22(L_4);
		__this->set_loops_23(1);
		__this->set_id_24(_stringLiteral757602046);
		__this->set_lookAhead_29((0.01f));
		__this->set_autoPlay_30((bool)1);
		__this->set_autoKill_31((bool)1);
		__this->set_pathResolution_35(((int32_t)10));
		__this->set_pathMode_36(1);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_5 = Vector3_get_forward_m3100859705(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_forwardDirection_39(L_5);
		Vector3_t3722313464  L_6 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_upDirection_40(L_6);
		List_1_t899420910 * L_7 = (List_1_t899420910 *)il2cpp_codegen_object_new(List_1_t899420910_il2cpp_TypeInfo_var);
		List_1__ctor_m2503402603(L_7, /*hidden argument*/List_1__ctor_m2503402603_RuntimeMethod_var);
		__this->set_wps_42(L_7);
		List_1_t899420910 * L_8 = (List_1_t899420910 *)il2cpp_codegen_object_new(List_1_t899420910_il2cpp_TypeInfo_var);
		List_1__ctor_m2503402603(L_8, /*hidden argument*/List_1__ctor_m2503402603_RuntimeMethod_var);
		__this->set_fullWps_43(L_8);
		__this->set_livePreview_48((bool)1);
		__this->set_perspectiveHandleSize_50((0.5f));
		__this->set_showIndexes_51((bool)1);
		Color_t2555686324  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Color__ctor_m2943235014((&L_9), (1.0f), (1.0f), (1.0f), (0.5f), /*hidden argument*/NULL);
		__this->set_pathColor_53(L_9);
		ABSAnimationComponent__ctor_m4239799620(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::<Awake>b__38_0()
extern "C"  void DOTweenPath_U3CAwakeU3Eb__38_0_m3881372633 (DOTweenPath_t3192963685 * __this, const RuntimeMethod* method)
{
	{
		((ABSAnimationComponent_t262169234 *)__this)->set_tween_18((Tween_t2342918553 *)NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.DOTweenVisualManager::Awake()
extern "C"  void DOTweenVisualManager_Awake_m3981621639 (DOTweenVisualManager_t1560353112 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenVisualManager_Awake_m3981621639_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ABSAnimationComponent_t262169234 * L_0 = Component_GetComponent_TisABSAnimationComponent_t262169234_m2746596450(__this, /*hidden argument*/Component_GetComponent_TisABSAnimationComponent_t262169234_m2746596450_RuntimeMethod_var);
		__this->set__animComponent_6(L_0);
		return;
	}
}
// System.Void DG.Tweening.DOTweenVisualManager::Update()
extern "C"  void DOTweenVisualManager_Update_m462811389 (DOTweenVisualManager_t1560353112 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenVisualManager_Update_m462811389_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get__requiresRestartFromSpawnPoint_5();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ABSAnimationComponent_t262169234 * L_1 = __this->get__animComponent_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}

IL_0016:
	{
		return;
	}

IL_0017:
	{
		__this->set__requiresRestartFromSpawnPoint_5((bool)0);
		ABSAnimationComponent_t262169234 * L_3 = __this->get__animComponent_6();
		NullCheck(L_3);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DORestart(System.Boolean) */, L_3, (bool)1);
		return;
	}
}
// System.Void DG.Tweening.DOTweenVisualManager::OnEnable()
extern "C"  void DOTweenVisualManager_OnEnable_m2832498305 (DOTweenVisualManager_t1560353112 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenVisualManager_OnEnable_m2832498305_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_onEnableBehaviour_3();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)1)))
		{
			case 0:
			{
				goto IL_001c;
			}
			case 1:
			{
				goto IL_0036;
			}
			case 2:
			{
				goto IL_0051;
			}
		}
	}
	{
		return;
	}

IL_001c:
	{
		ABSAnimationComponent_t262169234 * L_2 = __this->get__animComponent_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0058;
		}
	}
	{
		ABSAnimationComponent_t262169234 * L_4 = __this->get__animComponent_6();
		NullCheck(L_4);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DOPlay() */, L_4);
		return;
	}

IL_0036:
	{
		ABSAnimationComponent_t262169234 * L_5 = __this->get__animComponent_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_5, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0058;
		}
	}
	{
		ABSAnimationComponent_t262169234 * L_7 = __this->get__animComponent_6();
		NullCheck(L_7);
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DORestart(System.Boolean) */, L_7, (bool)0);
		return;
	}

IL_0051:
	{
		__this->set__requiresRestartFromSpawnPoint_5((bool)1);
	}

IL_0058:
	{
		return;
	}
}
// System.Void DG.Tweening.DOTweenVisualManager::OnDisable()
extern "C"  void DOTweenVisualManager_OnDisable_m2964460033 (DOTweenVisualManager_t1560353112 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenVisualManager_OnDisable_m2964460033_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		__this->set__requiresRestartFromSpawnPoint_5((bool)0);
		int32_t L_0 = __this->get_onDisableBehaviour_4();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)1)))
		{
			case 0:
			{
				goto IL_002b;
			}
			case 1:
			{
				goto IL_0048;
			}
			case 2:
			{
				goto IL_0062;
			}
			case 3:
			{
				goto IL_007c;
			}
			case 4:
			{
				goto IL_00a1;
			}
		}
	}
	{
		return;
	}

IL_002b:
	{
		ABSAnimationComponent_t262169234 * L_2 = __this->get__animComponent_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_00c5;
		}
	}
	{
		ABSAnimationComponent_t262169234 * L_4 = __this->get__animComponent_6();
		NullCheck(L_4);
		VirtActionInvoker0::Invoke(7 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DOPause() */, L_4);
		return;
	}

IL_0048:
	{
		ABSAnimationComponent_t262169234 * L_5 = __this->get__animComponent_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_5, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_00c5;
		}
	}
	{
		ABSAnimationComponent_t262169234 * L_7 = __this->get__animComponent_6();
		NullCheck(L_7);
		VirtActionInvoker0::Invoke(9 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DORewind() */, L_7);
		return;
	}

IL_0062:
	{
		ABSAnimationComponent_t262169234 * L_8 = __this->get__animComponent_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_8, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00c5;
		}
	}
	{
		ABSAnimationComponent_t262169234 * L_10 = __this->get__animComponent_6();
		NullCheck(L_10);
		VirtActionInvoker0::Invoke(12 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DOKill() */, L_10);
		return;
	}

IL_007c:
	{
		ABSAnimationComponent_t262169234 * L_11 = __this->get__animComponent_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_11, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00c5;
		}
	}
	{
		ABSAnimationComponent_t262169234 * L_13 = __this->get__animComponent_6();
		NullCheck(L_13);
		VirtActionInvoker0::Invoke(11 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DOComplete() */, L_13);
		ABSAnimationComponent_t262169234 * L_14 = __this->get__animComponent_6();
		NullCheck(L_14);
		VirtActionInvoker0::Invoke(12 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DOKill() */, L_14);
		return;
	}

IL_00a1:
	{
		ABSAnimationComponent_t262169234 * L_15 = __this->get__animComponent_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_16 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_15, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00ba;
		}
	}
	{
		ABSAnimationComponent_t262169234 * L_17 = __this->get__animComponent_6();
		NullCheck(L_17);
		VirtActionInvoker0::Invoke(12 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DOKill() */, L_17);
	}

IL_00ba:
	{
		GameObject_t1113636619 * L_18 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
	}

IL_00c5:
	{
		return;
	}
}
// System.Void DG.Tweening.DOTweenVisualManager::.ctor()
extern "C"  void DOTweenVisualManager__ctor_m1279007047 (DOTweenVisualManager_t1560353112 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
