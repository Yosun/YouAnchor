﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Char[]
struct CharU5BU5D_t3528271667;
// System.IO.TextWriter
struct TextWriter_t3478189236;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter>
struct IList_1_t2862426328;
// System.Func`1<Newtonsoft.Json.JsonSerializerSettings>
struct Func_1_t1569001961;
// System.String
struct String_t;
// Newtonsoft.Json.JsonSerializerSettings
struct JsonSerializerSettings_t2139255314;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// Newtonsoft.Json.Utilities.PropertyNameTable/Entry[]
struct EntryU5BU5D_t1995962374;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.CanvasGroup
struct CanvasGroup_t4083511760;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.UI.Text
struct Text_t1901882714;
// System.Type
struct Type_t;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Void
struct Void_t1185182177;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// Newtonsoft.Json.JsonConverterCollection
struct JsonConverterCollection_t3675964822;
// Newtonsoft.Json.Serialization.IContractResolver
struct IContractResolver_t4193385603;
// Newtonsoft.Json.Serialization.ITraceWriter
struct ITraceWriter_t2623073143;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t1493878338;
// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_t274213469;
// Newtonsoft.Json.Serialization.IReferenceResolver
struct IReferenceResolver_t2564061104;
// System.Globalization.CultureInfo
struct CultureInfo_t4157843068;
// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>
struct EventHandler_1_t2935836205;
// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>
struct List_1_t4000102456;
// System.ComponentModel.AddingNewEventArgs
struct AddingNewEventArgs_t2974299087;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.ComponentModel.NotifyCollectionChangedEventArgs
struct NotifyCollectionChangedEventArgs_t1368105863;
// System.ComponentModel.PropertyChangingEventArgs
struct PropertyChangingEventArgs_t2067745136;
// Newtonsoft.Json.JsonWriter/State[][]
struct StateU5BU5DU5BU5D_t3190318925;
// System.IO.TextReader
struct TextReader_t283511965;
// Newtonsoft.Json.IArrayPool`1<System.Char>
struct IArrayPool_1_t3621664784;
// Newtonsoft.Json.Utilities.PropertyNameTable
struct PropertyNameTable_t4130830590;
// Newtonsoft.Json.Utilities.Base64Encoder
struct Base64Encoder_t3724986751;
// System.Boolean[]
struct BooleanU5BU5D_t2897418192;
// System.Func`1<Newtonsoft.Json.Serialization.IReferenceResolver>
struct Func_1_t1993807751;
// DG.Tweening.Core.ABSAnimationComponent
struct ABSAnimationComponent_t262169234;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// DG.Tweening.Tween
struct Tween_t2342918553;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// DG.Tweening.Plugins.Core.PathCore.Path
struct Path_t3614338981;




#ifndef U3CMODULEU3E_T692745557_H
#define U3CMODULEU3E_T692745557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745557 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745557_H
#ifndef U3CMODULEU3E_T692745555_H
#define U3CMODULEU3E_T692745555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745555 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745555_H
#ifndef U3CMODULEU3E_T692745556_H
#define U3CMODULEU3E_T692745556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745556 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745556_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef JSONTOKENUTILS_T2823043526_H
#define JSONTOKENUTILS_T2823043526_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.JsonTokenUtils
struct  JsonTokenUtils_t2823043526  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTOKENUTILS_T2823043526_H
#ifndef BASE64ENCODER_T3724986751_H
#define BASE64ENCODER_T3724986751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.Base64Encoder
struct  Base64Encoder_t3724986751  : public RuntimeObject
{
public:
	// System.Char[] Newtonsoft.Json.Utilities.Base64Encoder::_charsLine
	CharU5BU5D_t3528271667* ____charsLine_0;
	// System.IO.TextWriter Newtonsoft.Json.Utilities.Base64Encoder::_writer
	TextWriter_t3478189236 * ____writer_1;
	// System.Byte[] Newtonsoft.Json.Utilities.Base64Encoder::_leftOverBytes
	ByteU5BU5D_t4116647657* ____leftOverBytes_2;
	// System.Int32 Newtonsoft.Json.Utilities.Base64Encoder::_leftOverBytesCount
	int32_t ____leftOverBytesCount_3;

public:
	inline static int32_t get_offset_of__charsLine_0() { return static_cast<int32_t>(offsetof(Base64Encoder_t3724986751, ____charsLine_0)); }
	inline CharU5BU5D_t3528271667* get__charsLine_0() const { return ____charsLine_0; }
	inline CharU5BU5D_t3528271667** get_address_of__charsLine_0() { return &____charsLine_0; }
	inline void set__charsLine_0(CharU5BU5D_t3528271667* value)
	{
		____charsLine_0 = value;
		Il2CppCodeGenWriteBarrier((&____charsLine_0), value);
	}

	inline static int32_t get_offset_of__writer_1() { return static_cast<int32_t>(offsetof(Base64Encoder_t3724986751, ____writer_1)); }
	inline TextWriter_t3478189236 * get__writer_1() const { return ____writer_1; }
	inline TextWriter_t3478189236 ** get_address_of__writer_1() { return &____writer_1; }
	inline void set__writer_1(TextWriter_t3478189236 * value)
	{
		____writer_1 = value;
		Il2CppCodeGenWriteBarrier((&____writer_1), value);
	}

	inline static int32_t get_offset_of__leftOverBytes_2() { return static_cast<int32_t>(offsetof(Base64Encoder_t3724986751, ____leftOverBytes_2)); }
	inline ByteU5BU5D_t4116647657* get__leftOverBytes_2() const { return ____leftOverBytes_2; }
	inline ByteU5BU5D_t4116647657** get_address_of__leftOverBytes_2() { return &____leftOverBytes_2; }
	inline void set__leftOverBytes_2(ByteU5BU5D_t4116647657* value)
	{
		____leftOverBytes_2 = value;
		Il2CppCodeGenWriteBarrier((&____leftOverBytes_2), value);
	}

	inline static int32_t get_offset_of__leftOverBytesCount_3() { return static_cast<int32_t>(offsetof(Base64Encoder_t3724986751, ____leftOverBytesCount_3)); }
	inline int32_t get__leftOverBytesCount_3() const { return ____leftOverBytesCount_3; }
	inline int32_t* get_address_of__leftOverBytesCount_3() { return &____leftOverBytesCount_3; }
	inline void set__leftOverBytesCount_3(int32_t value)
	{
		____leftOverBytesCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASE64ENCODER_T3724986751_H
#ifndef U3CU3EC__DISPLAYCLASS90_0_T3738200698_H
#define U3CU3EC__DISPLAYCLASS90_0_T3738200698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonSerializerSettings/<>c__DisplayClass90_0
struct  U3CU3Ec__DisplayClass90_0_t3738200698  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS90_0_T3738200698_H
#ifndef COLLECTION_1_T4286429759_H
#define COLLECTION_1_T4286429759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.JsonConverter>
struct  Collection_1_t4286429759  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1::list
	RuntimeObject* ___list_0;
	// System.Object System.Collections.ObjectModel.Collection`1::syncRoot
	RuntimeObject * ___syncRoot_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Collection_1_t4286429759, ___list_0)); }
	inline RuntimeObject* get_list_0() const { return ___list_0; }
	inline RuntimeObject** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(RuntimeObject* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_syncRoot_1() { return static_cast<int32_t>(offsetof(Collection_1_t4286429759, ___syncRoot_1)); }
	inline RuntimeObject * get_syncRoot_1() const { return ___syncRoot_1; }
	inline RuntimeObject ** get_address_of_syncRoot_1() { return &___syncRoot_1; }
	inline void set_syncRoot_1(RuntimeObject * value)
	{
		___syncRoot_1 = value;
		Il2CppCodeGenWriteBarrier((&___syncRoot_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTION_1_T4286429759_H
#ifndef JSONCONVERT_T3077351166_H
#define JSONCONVERT_T3077351166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonConvert
struct  JsonConvert_t3077351166  : public RuntimeObject
{
public:

public:
};

struct JsonConvert_t3077351166_StaticFields
{
public:
	// System.Func`1<Newtonsoft.Json.JsonSerializerSettings> Newtonsoft.Json.JsonConvert::<DefaultSettings>k__BackingField
	Func_1_t1569001961 * ___U3CDefaultSettingsU3Ek__BackingField_0;
	// System.String Newtonsoft.Json.JsonConvert::True
	String_t* ___True_1;
	// System.String Newtonsoft.Json.JsonConvert::False
	String_t* ___False_2;
	// System.String Newtonsoft.Json.JsonConvert::Null
	String_t* ___Null_3;
	// System.String Newtonsoft.Json.JsonConvert::Undefined
	String_t* ___Undefined_4;
	// System.String Newtonsoft.Json.JsonConvert::PositiveInfinity
	String_t* ___PositiveInfinity_5;
	// System.String Newtonsoft.Json.JsonConvert::NegativeInfinity
	String_t* ___NegativeInfinity_6;
	// System.String Newtonsoft.Json.JsonConvert::NaN
	String_t* ___NaN_7;
	// Newtonsoft.Json.JsonSerializerSettings Newtonsoft.Json.JsonConvert::InitialSerializerSettings
	JsonSerializerSettings_t2139255314 * ___InitialSerializerSettings_8;

public:
	inline static int32_t get_offset_of_U3CDefaultSettingsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JsonConvert_t3077351166_StaticFields, ___U3CDefaultSettingsU3Ek__BackingField_0)); }
	inline Func_1_t1569001961 * get_U3CDefaultSettingsU3Ek__BackingField_0() const { return ___U3CDefaultSettingsU3Ek__BackingField_0; }
	inline Func_1_t1569001961 ** get_address_of_U3CDefaultSettingsU3Ek__BackingField_0() { return &___U3CDefaultSettingsU3Ek__BackingField_0; }
	inline void set_U3CDefaultSettingsU3Ek__BackingField_0(Func_1_t1569001961 * value)
	{
		___U3CDefaultSettingsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultSettingsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_True_1() { return static_cast<int32_t>(offsetof(JsonConvert_t3077351166_StaticFields, ___True_1)); }
	inline String_t* get_True_1() const { return ___True_1; }
	inline String_t** get_address_of_True_1() { return &___True_1; }
	inline void set_True_1(String_t* value)
	{
		___True_1 = value;
		Il2CppCodeGenWriteBarrier((&___True_1), value);
	}

	inline static int32_t get_offset_of_False_2() { return static_cast<int32_t>(offsetof(JsonConvert_t3077351166_StaticFields, ___False_2)); }
	inline String_t* get_False_2() const { return ___False_2; }
	inline String_t** get_address_of_False_2() { return &___False_2; }
	inline void set_False_2(String_t* value)
	{
		___False_2 = value;
		Il2CppCodeGenWriteBarrier((&___False_2), value);
	}

	inline static int32_t get_offset_of_Null_3() { return static_cast<int32_t>(offsetof(JsonConvert_t3077351166_StaticFields, ___Null_3)); }
	inline String_t* get_Null_3() const { return ___Null_3; }
	inline String_t** get_address_of_Null_3() { return &___Null_3; }
	inline void set_Null_3(String_t* value)
	{
		___Null_3 = value;
		Il2CppCodeGenWriteBarrier((&___Null_3), value);
	}

	inline static int32_t get_offset_of_Undefined_4() { return static_cast<int32_t>(offsetof(JsonConvert_t3077351166_StaticFields, ___Undefined_4)); }
	inline String_t* get_Undefined_4() const { return ___Undefined_4; }
	inline String_t** get_address_of_Undefined_4() { return &___Undefined_4; }
	inline void set_Undefined_4(String_t* value)
	{
		___Undefined_4 = value;
		Il2CppCodeGenWriteBarrier((&___Undefined_4), value);
	}

	inline static int32_t get_offset_of_PositiveInfinity_5() { return static_cast<int32_t>(offsetof(JsonConvert_t3077351166_StaticFields, ___PositiveInfinity_5)); }
	inline String_t* get_PositiveInfinity_5() const { return ___PositiveInfinity_5; }
	inline String_t** get_address_of_PositiveInfinity_5() { return &___PositiveInfinity_5; }
	inline void set_PositiveInfinity_5(String_t* value)
	{
		___PositiveInfinity_5 = value;
		Il2CppCodeGenWriteBarrier((&___PositiveInfinity_5), value);
	}

	inline static int32_t get_offset_of_NegativeInfinity_6() { return static_cast<int32_t>(offsetof(JsonConvert_t3077351166_StaticFields, ___NegativeInfinity_6)); }
	inline String_t* get_NegativeInfinity_6() const { return ___NegativeInfinity_6; }
	inline String_t** get_address_of_NegativeInfinity_6() { return &___NegativeInfinity_6; }
	inline void set_NegativeInfinity_6(String_t* value)
	{
		___NegativeInfinity_6 = value;
		Il2CppCodeGenWriteBarrier((&___NegativeInfinity_6), value);
	}

	inline static int32_t get_offset_of_NaN_7() { return static_cast<int32_t>(offsetof(JsonConvert_t3077351166_StaticFields, ___NaN_7)); }
	inline String_t* get_NaN_7() const { return ___NaN_7; }
	inline String_t** get_address_of_NaN_7() { return &___NaN_7; }
	inline void set_NaN_7(String_t* value)
	{
		___NaN_7 = value;
		Il2CppCodeGenWriteBarrier((&___NaN_7), value);
	}

	inline static int32_t get_offset_of_InitialSerializerSettings_8() { return static_cast<int32_t>(offsetof(JsonConvert_t3077351166_StaticFields, ___InitialSerializerSettings_8)); }
	inline JsonSerializerSettings_t2139255314 * get_InitialSerializerSettings_8() const { return ___InitialSerializerSettings_8; }
	inline JsonSerializerSettings_t2139255314 ** get_address_of_InitialSerializerSettings_8() { return &___InitialSerializerSettings_8; }
	inline void set_InitialSerializerSettings_8(JsonSerializerSettings_t2139255314 * value)
	{
		___InitialSerializerSettings_8 = value;
		Il2CppCodeGenWriteBarrier((&___InitialSerializerSettings_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONVERT_T3077351166_H
#ifndef JSONCONVERTER_T1047106545_H
#define JSONCONVERTER_T1047106545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonConverter
struct  JsonConverter_t1047106545  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONVERTER_T1047106545_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef REFLECTIONDELEGATEFACTORY_T2528576452_H
#define REFLECTIONDELEGATEFACTORY_T2528576452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionDelegateFactory
struct  ReflectionDelegateFactory_t2528576452  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONDELEGATEFACTORY_T2528576452_H
#ifndef ENTRY_T2924091039_H
#define ENTRY_T2924091039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.PropertyNameTable/Entry
struct  Entry_t2924091039  : public RuntimeObject
{
public:
	// System.String Newtonsoft.Json.Utilities.PropertyNameTable/Entry::Value
	String_t* ___Value_0;
	// System.Int32 Newtonsoft.Json.Utilities.PropertyNameTable/Entry::HashCode
	int32_t ___HashCode_1;
	// Newtonsoft.Json.Utilities.PropertyNameTable/Entry Newtonsoft.Json.Utilities.PropertyNameTable/Entry::Next
	Entry_t2924091039 * ___Next_2;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(Entry_t2924091039, ___Value_0)); }
	inline String_t* get_Value_0() const { return ___Value_0; }
	inline String_t** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(String_t* value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((&___Value_0), value);
	}

	inline static int32_t get_offset_of_HashCode_1() { return static_cast<int32_t>(offsetof(Entry_t2924091039, ___HashCode_1)); }
	inline int32_t get_HashCode_1() const { return ___HashCode_1; }
	inline int32_t* get_address_of_HashCode_1() { return &___HashCode_1; }
	inline void set_HashCode_1(int32_t value)
	{
		___HashCode_1 = value;
	}

	inline static int32_t get_offset_of_Next_2() { return static_cast<int32_t>(offsetof(Entry_t2924091039, ___Next_2)); }
	inline Entry_t2924091039 * get_Next_2() const { return ___Next_2; }
	inline Entry_t2924091039 ** get_address_of_Next_2() { return &___Next_2; }
	inline void set_Next_2(Entry_t2924091039 * value)
	{
		___Next_2 = value;
		Il2CppCodeGenWriteBarrier((&___Next_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T2924091039_H
#ifndef PROPERTYNAMETABLE_T4130830590_H
#define PROPERTYNAMETABLE_T4130830590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.PropertyNameTable
struct  PropertyNameTable_t4130830590  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.PropertyNameTable::_count
	int32_t ____count_1;
	// Newtonsoft.Json.Utilities.PropertyNameTable/Entry[] Newtonsoft.Json.Utilities.PropertyNameTable::_entries
	EntryU5BU5D_t1995962374* ____entries_2;
	// System.Int32 Newtonsoft.Json.Utilities.PropertyNameTable::_mask
	int32_t ____mask_3;

public:
	inline static int32_t get_offset_of__count_1() { return static_cast<int32_t>(offsetof(PropertyNameTable_t4130830590, ____count_1)); }
	inline int32_t get__count_1() const { return ____count_1; }
	inline int32_t* get_address_of__count_1() { return &____count_1; }
	inline void set__count_1(int32_t value)
	{
		____count_1 = value;
	}

	inline static int32_t get_offset_of__entries_2() { return static_cast<int32_t>(offsetof(PropertyNameTable_t4130830590, ____entries_2)); }
	inline EntryU5BU5D_t1995962374* get__entries_2() const { return ____entries_2; }
	inline EntryU5BU5D_t1995962374** get_address_of__entries_2() { return &____entries_2; }
	inline void set__entries_2(EntryU5BU5D_t1995962374* value)
	{
		____entries_2 = value;
		Il2CppCodeGenWriteBarrier((&____entries_2), value);
	}

	inline static int32_t get_offset_of__mask_3() { return static_cast<int32_t>(offsetof(PropertyNameTable_t4130830590, ____mask_3)); }
	inline int32_t get__mask_3() const { return ____mask_3; }
	inline int32_t* get_address_of__mask_3() { return &____mask_3; }
	inline void set__mask_3(int32_t value)
	{
		____mask_3 = value;
	}
};

struct PropertyNameTable_t4130830590_StaticFields
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.PropertyNameTable::HashCodeRandomizer
	int32_t ___HashCodeRandomizer_0;

public:
	inline static int32_t get_offset_of_HashCodeRandomizer_0() { return static_cast<int32_t>(offsetof(PropertyNameTable_t4130830590_StaticFields, ___HashCodeRandomizer_0)); }
	inline int32_t get_HashCodeRandomizer_0() const { return ___HashCodeRandomizer_0; }
	inline int32_t* get_address_of_HashCodeRandomizer_0() { return &___HashCodeRandomizer_0; }
	inline void set_HashCodeRandomizer_0(int32_t value)
	{
		___HashCodeRandomizer_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYNAMETABLE_T4130830590_H
#ifndef NOTIFYCOLLECTIONCHANGEDEVENTARGS_T1368105863_H
#define NOTIFYCOLLECTIONCHANGEDEVENTARGS_T1368105863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.NotifyCollectionChangedEventArgs
struct  NotifyCollectionChangedEventArgs_t1368105863  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFYCOLLECTIONCHANGEDEVENTARGS_T1368105863_H
#ifndef EVENTARGS_T3591816995_H
#define EVENTARGS_T3591816995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3591816995  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3591816995_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3591816995 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3591816995_StaticFields, ___Empty_0)); }
	inline EventArgs_t3591816995 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3591816995 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3591816995 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3591816995_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef ADDINGNEWEVENTARGS_T2974299087_H
#define ADDINGNEWEVENTARGS_T2974299087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.AddingNewEventArgs
struct  AddingNewEventArgs_t2974299087  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDINGNEWEVENTARGS_T2974299087_H
#ifndef SHORTCUTEXTENSIONS43_T1788548476_H
#define SHORTCUTEXTENSIONS43_T1788548476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions43
struct  ShortcutExtensions43_t1788548476  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHORTCUTEXTENSIONS43_T1788548476_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T1295327389_H
#define U3CU3EC__DISPLAYCLASS3_0_T1295327389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t1295327389  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image DG.Tweening.ShortcutExtensions46/<>c__DisplayClass3_0::target
	Image_t2670269651 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t1295327389, ___target_0)); }
	inline Image_t2670269651 * get_target_0() const { return ___target_0; }
	inline Image_t2670269651 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Image_t2670269651 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T1295327389_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T2861411330_H
#define U3CU3EC__DISPLAYCLASS4_0_T2861411330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t2861411330  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::target
	Image_t2670269651 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t2861411330, ___target_0)); }
	inline Image_t2670269651 * get_target_0() const { return ___target_0; }
	inline Image_t2670269651 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Image_t2670269651 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T2861411330_H
#ifndef U3CU3EC__DISPLAYCLASS0_0_T892042862_H
#define U3CU3EC__DISPLAYCLASS0_0_T892042862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_t892042862  : public RuntimeObject
{
public:
	// UnityEngine.CanvasGroup DG.Tweening.ShortcutExtensions46/<>c__DisplayClass0_0::target
	CanvasGroup_t4083511760 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t892042862, ___target_0)); }
	inline CanvasGroup_t4083511760 * get_target_0() const { return ___target_0; }
	inline CanvasGroup_t4083511760 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(CanvasGroup_t4083511760 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS0_0_T892042862_H
#ifndef SHORTCUTEXTENSIONS46_T3362526588_H
#define SHORTCUTEXTENSIONS46_T3362526588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46
struct  ShortcutExtensions46_t3362526588  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHORTCUTEXTENSIONS46_T3362526588_H
#ifndef DOTWEENUTILS46_T543860410_H
#define DOTWEENUTILS46_T543860410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenUtils46
struct  DOTweenUtils46_t543860410  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENUTILS46_T543860410_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_T1635241055_H
#define U3CU3EC__DISPLAYCLASS8_0_T1635241055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_t1635241055  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.ShortcutExtensions43/<>c__DisplayClass8_0::target
	Rigidbody2D_t939494601 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t1635241055, ___target_0)); }
	inline Rigidbody2D_t939494601 * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_t939494601 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_t939494601 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_T1635241055_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T2038525582_H
#define U3CU3EC__DISPLAYCLASS5_0_T2038525582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t2038525582  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.ShortcutExtensions43/<>c__DisplayClass5_0::target
	Rigidbody2D_t939494601 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t2038525582, ___target_0)); }
	inline Rigidbody2D_t939494601 * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_t939494601 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_t939494601 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T2038525582_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T2845094636_H
#define U3CU3EC__DISPLAYCLASS3_0_T2845094636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t2845094636  : public RuntimeObject
{
public:
	// UnityEngine.SpriteRenderer DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0::target
	SpriteRenderer_t3235626157 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t2845094636, ___target_0)); }
	inline SpriteRenderer_t3235626157 * get_target_0() const { return ___target_0; }
	inline SpriteRenderer_t3235626157 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(SpriteRenderer_t3235626157 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T2845094636_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_T116211281_H
#define U3CU3EC__DISPLAYCLASS2_0_T116211281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t116211281  : public RuntimeObject
{
public:
	// UnityEngine.SpriteRenderer DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0::target
	SpriteRenderer_t3235626157 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t116211281, ___target_0)); }
	inline SpriteRenderer_t3235626157 * get_target_0() const { return ___target_0; }
	inline SpriteRenderer_t3235626157 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(SpriteRenderer_t3235626157 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_T116211281_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_T2635598378_H
#define U3CU3EC__DISPLAYCLASS13_0_T2635598378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_t2635598378  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.ShortcutExtensions46/<>c__DisplayClass13_0::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t2635598378, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_T2635598378_H
#ifndef U3CU3EC__DISPLAYCLASS31_0_T1090461940_H
#define U3CU3EC__DISPLAYCLASS31_0_T1090461940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass31_0
struct  U3CU3Ec__DisplayClass31_0_t1090461940  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text DG.Tweening.ShortcutExtensions46/<>c__DisplayClass31_0::target
	Text_t1901882714 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_0_t1090461940, ___target_0)); }
	inline Text_t1901882714 * get_target_0() const { return ___target_0; }
	inline Text_t1901882714 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Text_t1901882714 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS31_0_T1090461940_H
#ifndef U3CU3EC__DISPLAYCLASS25_0_T289052047_H
#define U3CU3EC__DISPLAYCLASS25_0_T289052047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass25_0
struct  U3CU3Ec__DisplayClass25_0_t289052047  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.ShortcutExtensions46/<>c__DisplayClass25_0::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass25_0_t289052047, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS25_0_T289052047_H
#ifndef U3CU3EC__DISPLAYCLASS32_0_T3811451124_H
#define U3CU3EC__DISPLAYCLASS32_0_T3811451124_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass32_0
struct  U3CU3Ec__DisplayClass32_0_t3811451124  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text DG.Tweening.ShortcutExtensions46/<>c__DisplayClass32_0::target
	Text_t1901882714 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass32_0_t3811451124, ___target_0)); }
	inline Text_t1901882714 * get_target_0() const { return ___target_0; }
	inline Text_t1901882714 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Text_t1901882714 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS32_0_T3811451124_H
#ifndef U3CU3EC__DISPLAYCLASS23_0_T4201682319_H
#define U3CU3EC__DISPLAYCLASS23_0_T4201682319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass23_0
struct  U3CU3Ec__DisplayClass23_0_t4201682319  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.ShortcutExtensions46/<>c__DisplayClass23_0::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass23_0_t4201682319, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS23_0_T4201682319_H
#ifndef U3CU3EC__DISPLAYCLASS33_0_T1472798964_H
#define U3CU3EC__DISPLAYCLASS33_0_T1472798964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass33_0
struct  U3CU3Ec__DisplayClass33_0_t1472798964  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text DG.Tweening.ShortcutExtensions46/<>c__DisplayClass33_0::target
	Text_t1901882714 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t1472798964, ___target_0)); }
	inline Text_t1901882714 * get_target_0() const { return ___target_0; }
	inline Text_t1901882714 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Text_t1901882714 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS33_0_T1472798964_H
#ifndef U3CU3EC__DISPLAYCLASS22_0_T2245367183_H
#define U3CU3EC__DISPLAYCLASS22_0_T2245367183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass22_0
struct  U3CU3Ec__DisplayClass22_0_t2245367183  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.ShortcutExtensions46/<>c__DisplayClass22_0::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t2245367183, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS22_0_T2245367183_H
#ifndef U3CU3EC__DISPLAYCLASS16_0_T1443957290_H
#define U3CU3EC__DISPLAYCLASS16_0_T1443957290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass16_0
struct  U3CU3Ec__DisplayClass16_0_t1443957290  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.ShortcutExtensions46/<>c__DisplayClass16_0::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_t1443957290, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS16_0_T1443957290_H
#ifndef JSONEXCEPTION_T3720114400_H
#define JSONEXCEPTION_T3720114400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonException
struct  JsonException_t3720114400  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONEXCEPTION_T3720114400_H
#ifndef JSONEXTENSIONDATAATTRIBUTE_T1775849281_H
#define JSONEXTENSIONDATAATTRIBUTE_T1775849281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonExtensionDataAttribute
struct  JsonExtensionDataAttribute_t1775849281  : public Attribute_t861562559
{
public:
	// System.Boolean Newtonsoft.Json.JsonExtensionDataAttribute::<WriteData>k__BackingField
	bool ___U3CWriteDataU3Ek__BackingField_0;
	// System.Boolean Newtonsoft.Json.JsonExtensionDataAttribute::<ReadData>k__BackingField
	bool ___U3CReadDataU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CWriteDataU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JsonExtensionDataAttribute_t1775849281, ___U3CWriteDataU3Ek__BackingField_0)); }
	inline bool get_U3CWriteDataU3Ek__BackingField_0() const { return ___U3CWriteDataU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CWriteDataU3Ek__BackingField_0() { return &___U3CWriteDataU3Ek__BackingField_0; }
	inline void set_U3CWriteDataU3Ek__BackingField_0(bool value)
	{
		___U3CWriteDataU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CReadDataU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonExtensionDataAttribute_t1775849281, ___U3CReadDataU3Ek__BackingField_1)); }
	inline bool get_U3CReadDataU3Ek__BackingField_1() const { return ___U3CReadDataU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CReadDataU3Ek__BackingField_1() { return &___U3CReadDataU3Ek__BackingField_1; }
	inline void set_U3CReadDataU3Ek__BackingField_1(bool value)
	{
		___U3CReadDataU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONEXTENSIONDATAATTRIBUTE_T1775849281_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef JSONREQUIREDATTRIBUTE_T2327646188_H
#define JSONREQUIREDATTRIBUTE_T2327646188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonRequiredAttribute
struct  JsonRequiredAttribute_t2327646188  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONREQUIREDATTRIBUTE_T2327646188_H
#ifndef LATEBOUNDREFLECTIONDELEGATEFACTORY_T925499913_H
#define LATEBOUNDREFLECTIONDELEGATEFACTORY_T925499913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory
struct  LateBoundReflectionDelegateFactory_t925499913  : public ReflectionDelegateFactory_t2528576452
{
public:

public:
};

struct LateBoundReflectionDelegateFactory_t925499913_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::_instance
	LateBoundReflectionDelegateFactory_t925499913 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(LateBoundReflectionDelegateFactory_t925499913_StaticFields, ____instance_0)); }
	inline LateBoundReflectionDelegateFactory_t925499913 * get__instance_0() const { return ____instance_0; }
	inline LateBoundReflectionDelegateFactory_t925499913 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(LateBoundReflectionDelegateFactory_t925499913 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LATEBOUNDREFLECTIONDELEGATEFACTORY_T925499913_H
#ifndef JSONCONVERTERCOLLECTION_T3675964822_H
#define JSONCONVERTERCOLLECTION_T3675964822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonConverterCollection
struct  JsonConverterCollection_t3675964822  : public Collection_1_t4286429759
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONVERTERCOLLECTION_T3675964822_H
#ifndef JSONCONVERTERATTRIBUTE_T3132853130_H
#define JSONCONVERTERATTRIBUTE_T3132853130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonConverterAttribute
struct  JsonConverterAttribute_t3132853130  : public Attribute_t861562559
{
public:
	// System.Type Newtonsoft.Json.JsonConverterAttribute::_converterType
	Type_t * ____converterType_0;
	// System.Object[] Newtonsoft.Json.JsonConverterAttribute::<ConverterParameters>k__BackingField
	ObjectU5BU5D_t2843939325* ___U3CConverterParametersU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of__converterType_0() { return static_cast<int32_t>(offsetof(JsonConverterAttribute_t3132853130, ____converterType_0)); }
	inline Type_t * get__converterType_0() const { return ____converterType_0; }
	inline Type_t ** get_address_of__converterType_0() { return &____converterType_0; }
	inline void set__converterType_0(Type_t * value)
	{
		____converterType_0 = value;
		Il2CppCodeGenWriteBarrier((&____converterType_0), value);
	}

	inline static int32_t get_offset_of_U3CConverterParametersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonConverterAttribute_t3132853130, ___U3CConverterParametersU3Ek__BackingField_1)); }
	inline ObjectU5BU5D_t2843939325* get_U3CConverterParametersU3Ek__BackingField_1() const { return ___U3CConverterParametersU3Ek__BackingField_1; }
	inline ObjectU5BU5D_t2843939325** get_address_of_U3CConverterParametersU3Ek__BackingField_1() { return &___U3CConverterParametersU3Ek__BackingField_1; }
	inline void set_U3CConverterParametersU3Ek__BackingField_1(ObjectU5BU5D_t2843939325* value)
	{
		___U3CConverterParametersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConverterParametersU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONVERTERATTRIBUTE_T3132853130_H
#ifndef JSONIGNOREATTRIBUTE_T836745302_H
#define JSONIGNOREATTRIBUTE_T836745302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonIgnoreAttribute
struct  JsonIgnoreAttribute_t836745302  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONIGNOREATTRIBUTE_T836745302_H
#ifndef JSONCONSTRUCTORATTRIBUTE_T2226951669_H
#define JSONCONSTRUCTORATTRIBUTE_T2226951669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonConstructorAttribute
struct  JsonConstructorAttribute_t2226951669  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONSTRUCTORATTRIBUTE_T2226951669_H
#ifndef PROPERTYCHANGINGEVENTARGS_T2067745136_H
#define PROPERTYCHANGINGEVENTARGS_T2067745136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.PropertyChangingEventArgs
struct  PropertyChangingEventArgs_t2067745136  : public EventArgs_t3591816995
{
public:
	// System.String System.ComponentModel.PropertyChangingEventArgs::<PropertyName>k__BackingField
	String_t* ___U3CPropertyNameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CPropertyNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PropertyChangingEventArgs_t2067745136, ___U3CPropertyNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CPropertyNameU3Ek__BackingField_1() const { return ___U3CPropertyNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CPropertyNameU3Ek__BackingField_1() { return &___U3CPropertyNameU3Ek__BackingField_1; }
	inline void set_U3CPropertyNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CPropertyNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPropertyNameU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYCHANGINGEVENTARGS_T2067745136_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef NULLABLE_1_T1819850047_H
#define NULLABLE_1_T1819850047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t1819850047 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1819850047, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1819850047, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1819850047_H
#ifndef NULLABLE_1_T378540539_H
#define NULLABLE_1_T378540539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t378540539 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T378540539_H
#ifndef STRINGBUFFER_T2235727887_H
#define STRINGBUFFER_T2235727887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.StringBuffer
struct  StringBuffer_t2235727887 
{
public:
	// System.Char[] Newtonsoft.Json.Utilities.StringBuffer::_buffer
	CharU5BU5D_t3528271667* ____buffer_0;
	// System.Int32 Newtonsoft.Json.Utilities.StringBuffer::_position
	int32_t ____position_1;

public:
	inline static int32_t get_offset_of__buffer_0() { return static_cast<int32_t>(offsetof(StringBuffer_t2235727887, ____buffer_0)); }
	inline CharU5BU5D_t3528271667* get__buffer_0() const { return ____buffer_0; }
	inline CharU5BU5D_t3528271667** get_address_of__buffer_0() { return &____buffer_0; }
	inline void set__buffer_0(CharU5BU5D_t3528271667* value)
	{
		____buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_0), value);
	}

	inline static int32_t get_offset_of__position_1() { return static_cast<int32_t>(offsetof(StringBuffer_t2235727887, ____position_1)); }
	inline int32_t get__position_1() const { return ____position_1; }
	inline int32_t* get_address_of__position_1() { return &____position_1; }
	inline void set__position_1(int32_t value)
	{
		____position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.StringBuffer
struct StringBuffer_t2235727887_marshaled_pinvoke
{
	uint8_t* ____buffer_0;
	int32_t ____position_1;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.StringBuffer
struct StringBuffer_t2235727887_marshaled_com
{
	uint8_t* ____buffer_0;
	int32_t ____position_1;
};
#endif // STRINGBUFFER_T2235727887_H
#ifndef STRINGREFERENCE_T2912309144_H
#define STRINGREFERENCE_T2912309144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.StringReference
struct  StringReference_t2912309144 
{
public:
	// System.Char[] Newtonsoft.Json.Utilities.StringReference::_chars
	CharU5BU5D_t3528271667* ____chars_0;
	// System.Int32 Newtonsoft.Json.Utilities.StringReference::_startIndex
	int32_t ____startIndex_1;
	// System.Int32 Newtonsoft.Json.Utilities.StringReference::_length
	int32_t ____length_2;

public:
	inline static int32_t get_offset_of__chars_0() { return static_cast<int32_t>(offsetof(StringReference_t2912309144, ____chars_0)); }
	inline CharU5BU5D_t3528271667* get__chars_0() const { return ____chars_0; }
	inline CharU5BU5D_t3528271667** get_address_of__chars_0() { return &____chars_0; }
	inline void set__chars_0(CharU5BU5D_t3528271667* value)
	{
		____chars_0 = value;
		Il2CppCodeGenWriteBarrier((&____chars_0), value);
	}

	inline static int32_t get_offset_of__startIndex_1() { return static_cast<int32_t>(offsetof(StringReference_t2912309144, ____startIndex_1)); }
	inline int32_t get__startIndex_1() const { return ____startIndex_1; }
	inline int32_t* get_address_of__startIndex_1() { return &____startIndex_1; }
	inline void set__startIndex_1(int32_t value)
	{
		____startIndex_1 = value;
	}

	inline static int32_t get_offset_of__length_2() { return static_cast<int32_t>(offsetof(StringReference_t2912309144, ____length_2)); }
	inline int32_t get__length_2() const { return ____length_2; }
	inline int32_t* get_address_of__length_2() { return &____length_2; }
	inline void set__length_2(int32_t value)
	{
		____length_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.StringReference
struct StringReference_t2912309144_marshaled_pinvoke
{
	uint8_t* ____chars_0;
	int32_t ____startIndex_1;
	int32_t ____length_2;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.StringReference
struct StringReference_t2912309144_marshaled_com
{
	uint8_t* ____chars_0;
	int32_t ____startIndex_1;
	int32_t ____length_2;
};
#endif // STRINGREFERENCE_T2912309144_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef STREAMINGCONTEXTSTATES_T3580100459_H
#define STREAMINGCONTEXTSTATES_T3580100459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContextStates
struct  StreamingContextStates_t3580100459 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StreamingContextStates_t3580100459, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGCONTEXTSTATES_T3580100459_H
#ifndef REFERENCELOOPHANDLING_T1329130968_H
#define REFERENCELOOPHANDLING_T1329130968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ReferenceLoopHandling
struct  ReferenceLoopHandling_t1329130968 
{
public:
	// System.Int32 Newtonsoft.Json.ReferenceLoopHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReferenceLoopHandling_t1329130968, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCELOOPHANDLING_T1329130968_H
#ifndef NULLVALUEHANDLING_T3402136445_H
#define NULLVALUEHANDLING_T3402136445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.NullValueHandling
struct  NullValueHandling_t3402136445 
{
public:
	// System.Int32 Newtonsoft.Json.NullValueHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NullValueHandling_t3402136445, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLVALUEHANDLING_T3402136445_H
#ifndef MISSINGMEMBERHANDLING_T4043300624_H
#define MISSINGMEMBERHANDLING_T4043300624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MissingMemberHandling
struct  MissingMemberHandling_t4043300624 
{
public:
	// System.Int32 Newtonsoft.Json.MissingMemberHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MissingMemberHandling_t4043300624, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSINGMEMBERHANDLING_T4043300624_H
#ifndef FORMATTERASSEMBLYSTYLE_T868039825_H
#define FORMATTERASSEMBLYSTYLE_T868039825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
struct  FormatterAssemblyStyle_t868039825 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.FormatterAssemblyStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FormatterAssemblyStyle_t868039825, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTERASSEMBLYSTYLE_T868039825_H
#ifndef JSONSERIALIZATIONEXCEPTION_T4152118729_H
#define JSONSERIALIZATIONEXCEPTION_T4152118729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonSerializationException
struct  JsonSerializationException_t4152118729  : public JsonException_t3720114400
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZATIONEXCEPTION_T4152118729_H
#ifndef LOOPTYPE_T3049802818_H
#define LOOPTYPE_T3049802818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LoopType
struct  LoopType_t3049802818 
{
public:
	// System.Int32 DG.Tweening.LoopType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoopType_t3049802818, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T3049802818_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef STATE_T879723262_H
#define STATE_T879723262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonReader/State
struct  State_t879723262 
{
public:
	// System.Int32 Newtonsoft.Json.JsonReader/State::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(State_t879723262, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T879723262_H
#ifndef TYPENAMEHANDLING_T3731987346_H
#define TYPENAMEHANDLING_T3731987346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.TypeNameHandling
struct  TypeNameHandling_t3731987346 
{
public:
	// System.Int32 Newtonsoft.Json.TypeNameHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TypeNameHandling_t3731987346, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPENAMEHANDLING_T3731987346_H
#ifndef UPDATETYPE_T3937729206_H
#define UPDATETYPE_T3937729206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.UpdateType
struct  UpdateType_t3937729206 
{
public:
	// System.Int32 DG.Tweening.UpdateType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdateType_t3937729206, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATETYPE_T3937729206_H
#ifndef PATHTYPE_T3777299409_H
#define PATHTYPE_T3777299409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.PathType
struct  PathType_t3777299409 
{
public:
	// System.Int32 DG.Tweening.PathType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PathType_t3777299409, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHTYPE_T3777299409_H
#ifndef AXISCONSTRAINT_T2771958344_H
#define AXISCONSTRAINT_T2771958344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.AxisConstraint
struct  AxisConstraint_t2771958344 
{
public:
	// System.Int32 DG.Tweening.AxisConstraint::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisConstraint_t2771958344, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCONSTRAINT_T2771958344_H
#ifndef PATHMODE_T2165603100_H
#define PATHMODE_T2165603100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.PathMode
struct  PathMode_t2165603100 
{
public:
	// System.Int32 DG.Tweening.PathMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PathMode_t2165603100, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHMODE_T2165603100_H
#ifndef PARSERTIMEZONE_T1439545141_H
#define PARSERTIMEZONE_T1439545141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ParserTimeZone
struct  ParserTimeZone_t1439545141 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.ParserTimeZone::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParserTimeZone_t1439545141, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSERTIMEZONE_T1439545141_H
#ifndef ORIENTTYPE_T1731166963_H
#define ORIENTTYPE_T1731166963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.OrientType
struct  OrientType_t1731166963 
{
public:
	// System.Int32 DG.Tweening.Plugins.Options.OrientType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OrientType_t1731166963, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTTYPE_T1731166963_H
#ifndef WRITESTATE_T2626176463_H
#define WRITESTATE_T2626176463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.WriteState
struct  WriteState_t2626176463 
{
public:
	// System.Int32 Newtonsoft.Json.WriteState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WriteState_t2626176463, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITESTATE_T2626176463_H
#ifndef STATE_T2595666649_H
#define STATE_T2595666649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter/State
struct  State_t2595666649 
{
public:
	// System.Int32 Newtonsoft.Json.JsonWriter/State::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(State_t2595666649, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T2595666649_H
#ifndef EASE_T4010715394_H
#define EASE_T4010715394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Ease
struct  Ease_t4010715394 
{
public:
	// System.Int32 DG.Tweening.Ease::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Ease_t4010715394, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASE_T4010715394_H
#ifndef JSONTOKEN_T1917433489_H
#define JSONTOKEN_T1917433489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonToken
struct  JsonToken_t1917433489 
{
public:
	// System.Int32 Newtonsoft.Json.JsonToken::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JsonToken_t1917433489, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTOKEN_T1917433489_H
#ifndef NOTIFYCOLLECTIONCHANGEDACTION_T2928948188_H
#define NOTIFYCOLLECTIONCHANGEDACTION_T2928948188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.NotifyCollectionChangedAction
struct  NotifyCollectionChangedAction_t2928948188 
{
public:
	// System.Int32 System.ComponentModel.NotifyCollectionChangedAction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NotifyCollectionChangedAction_t2928948188, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFYCOLLECTIONCHANGEDACTION_T2928948188_H
#ifndef DATEFORMATHANDLING_T1376167855_H
#define DATEFORMATHANDLING_T1376167855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateFormatHandling
struct  DateFormatHandling_t1376167855 
{
public:
	// System.Int32 Newtonsoft.Json.DateFormatHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateFormatHandling_t1376167855, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEFORMATHANDLING_T1376167855_H
#ifndef DATEPARSEHANDLING_T2751701876_H
#define DATEPARSEHANDLING_T2751701876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateParseHandling
struct  DateParseHandling_t2751701876 
{
public:
	// System.Int32 Newtonsoft.Json.DateParseHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateParseHandling_t2751701876, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEPARSEHANDLING_T2751701876_H
#ifndef DATETIMEZONEHANDLING_T3002599730_H
#define DATETIMEZONEHANDLING_T3002599730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateTimeZoneHandling
struct  DateTimeZoneHandling_t3002599730 
{
public:
	// System.Int32 Newtonsoft.Json.DateTimeZoneHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeZoneHandling_t3002599730, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEZONEHANDLING_T3002599730_H
#ifndef FLOATFORMATHANDLING_T562907821_H
#define FLOATFORMATHANDLING_T562907821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.FloatFormatHandling
struct  FloatFormatHandling_t562907821 
{
public:
	// System.Int32 Newtonsoft.Json.FloatFormatHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FloatFormatHandling_t562907821, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATFORMATHANDLING_T562907821_H
#ifndef FLOATPARSEHANDLING_T1006965658_H
#define FLOATPARSEHANDLING_T1006965658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.FloatParseHandling
struct  FloatParseHandling_t1006965658 
{
public:
	// System.Int32 Newtonsoft.Json.FloatParseHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FloatParseHandling_t1006965658, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATPARSEHANDLING_T1006965658_H
#ifndef FORMATTING_T2192044929_H
#define FORMATTING_T2192044929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Formatting
struct  Formatting_t2192044929 
{
public:
	// System.Int32 Newtonsoft.Json.Formatting::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Formatting_t2192044929, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTING_T2192044929_H
#ifndef JSONCONTAINERTYPE_T3191599701_H
#define JSONCONTAINERTYPE_T3191599701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonContainerType
struct  JsonContainerType_t3191599701 
{
public:
	// System.Int32 Newtonsoft.Json.JsonContainerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JsonContainerType_t3191599701, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTAINERTYPE_T3191599701_H
#ifndef METADATAPROPERTYHANDLING_T1042169553_H
#define METADATAPROPERTYHANDLING_T1042169553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MetadataPropertyHandling
struct  MetadataPropertyHandling_t1042169553 
{
public:
	// System.Int32 Newtonsoft.Json.MetadataPropertyHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MetadataPropertyHandling_t1042169553, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METADATAPROPERTYHANDLING_T1042169553_H
#ifndef CONSTRUCTORHANDLING_T3189370124_H
#define CONSTRUCTORHANDLING_T3189370124_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ConstructorHandling
struct  ConstructorHandling_t3189370124 
{
public:
	// System.Int32 Newtonsoft.Json.ConstructorHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConstructorHandling_t3189370124, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRUCTORHANDLING_T3189370124_H
#ifndef HANDLESDRAWMODE_T2193450492_H
#define HANDLESDRAWMODE_T2193450492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.HandlesDrawMode
struct  HandlesDrawMode_t2193450492 
{
public:
	// System.Int32 DG.Tweening.HandlesDrawMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HandlesDrawMode_t2193450492, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDLESDRAWMODE_T2193450492_H
#ifndef HANDLESTYPE_T4074904290_H
#define HANDLESTYPE_T4074904290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.HandlesType
struct  HandlesType_t4074904290 
{
public:
	// System.Int32 DG.Tweening.HandlesType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HandlesType_t4074904290, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDLESTYPE_T4074904290_H
#ifndef DOTWEENINSPECTORMODE_T2656909913_H
#define DOTWEENINSPECTORMODE_T2656909913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenInspectorMode
struct  DOTweenInspectorMode_t2656909913 
{
public:
	// System.Int32 DG.Tweening.DOTweenInspectorMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DOTweenInspectorMode_t2656909913, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENINSPECTORMODE_T2656909913_H
#ifndef DOTWEENANIMATIONTYPE_T2748799855_H
#define DOTWEENANIMATIONTYPE_T2748799855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenAnimationType
struct  DOTweenAnimationType_t2748799855 
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenAnimationType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DOTweenAnimationType_t2748799855, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENANIMATIONTYPE_T2748799855_H
#ifndef ONDISABLEBEHAVIOUR_T979035984_H
#define ONDISABLEBEHAVIOUR_T979035984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.OnDisableBehaviour
struct  OnDisableBehaviour_t979035984 
{
public:
	// System.Int32 DG.Tweening.Core.OnDisableBehaviour::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OnDisableBehaviour_t979035984, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDISABLEBEHAVIOUR_T979035984_H
#ifndef ONENABLEBEHAVIOUR_T3474514863_H
#define ONENABLEBEHAVIOUR_T3474514863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.OnEnableBehaviour
struct  OnEnableBehaviour_t3474514863 
{
public:
	// System.Int32 DG.Tweening.Core.OnEnableBehaviour::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OnEnableBehaviour_t3474514863, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONENABLEBEHAVIOUR_T3474514863_H
#ifndef TARGETTYPE_T3479356996_H
#define TARGETTYPE_T3479356996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.TargetType
struct  TargetType_t3479356996 
{
public:
	// System.Int32 DG.Tweening.Core.TargetType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TargetType_t3479356996, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETTYPE_T3479356996_H
#ifndef VISUALMANAGERPRESET_T2677960456_H
#define VISUALMANAGERPRESET_T2677960456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.VisualManagerPreset
struct  VisualManagerPreset_t2677960456 
{
public:
	// System.Int32 DG.Tweening.Core.VisualManagerPreset::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VisualManagerPreset_t2677960456, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISUALMANAGERPRESET_T2677960456_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef STRINGESCAPEHANDLING_T4077875565_H
#define STRINGESCAPEHANDLING_T4077875565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.StringEscapeHandling
struct  StringEscapeHandling_t4077875565 
{
public:
	// System.Int32 Newtonsoft.Json.StringEscapeHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StringEscapeHandling_t4077875565, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGESCAPEHANDLING_T4077875565_H
#ifndef READTYPE_T340786580_H
#define READTYPE_T340786580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ReadType
struct  ReadType_t340786580 
{
public:
	// System.Int32 Newtonsoft.Json.ReadType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReadType_t340786580, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READTYPE_T340786580_H
#ifndef OBJECTCREATIONHANDLING_T3160794531_H
#define OBJECTCREATIONHANDLING_T3160794531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ObjectCreationHandling
struct  ObjectCreationHandling_t3160794531 
{
public:
	// System.Int32 Newtonsoft.Json.ObjectCreationHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ObjectCreationHandling_t3160794531, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCREATIONHANDLING_T3160794531_H
#ifndef JSONWRITEREXCEPTION_T1282940098_H
#define JSONWRITEREXCEPTION_T1282940098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriterException
struct  JsonWriterException_t1282940098  : public JsonException_t3720114400
{
public:
	// System.String Newtonsoft.Json.JsonWriterException::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(JsonWriterException_t1282940098, ___U3CPathU3Ek__BackingField_11)); }
	inline String_t* get_U3CPathU3Ek__BackingField_11() const { return ___U3CPathU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_11() { return &___U3CPathU3Ek__BackingField_11; }
	inline void set_U3CPathU3Ek__BackingField_11(String_t* value)
	{
		___U3CPathU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONWRITEREXCEPTION_T1282940098_H
#ifndef PRESERVEREFERENCESHANDLING_T815988812_H
#define PRESERVEREFERENCESHANDLING_T815988812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.PreserveReferencesHandling
struct  PreserveReferencesHandling_t815988812 
{
public:
	// System.Int32 Newtonsoft.Json.PreserveReferencesHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PreserveReferencesHandling_t815988812, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESERVEREFERENCESHANDLING_T815988812_H
#ifndef JSONREADEREXCEPTION_T3553144781_H
#define JSONREADEREXCEPTION_T3553144781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonReaderException
struct  JsonReaderException_t3553144781  : public JsonException_t3720114400
{
public:
	// System.Int32 Newtonsoft.Json.JsonReaderException::<LineNumber>k__BackingField
	int32_t ___U3CLineNumberU3Ek__BackingField_11;
	// System.Int32 Newtonsoft.Json.JsonReaderException::<LinePosition>k__BackingField
	int32_t ___U3CLinePositionU3Ek__BackingField_12;
	// System.String Newtonsoft.Json.JsonReaderException::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CLineNumberU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(JsonReaderException_t3553144781, ___U3CLineNumberU3Ek__BackingField_11)); }
	inline int32_t get_U3CLineNumberU3Ek__BackingField_11() const { return ___U3CLineNumberU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CLineNumberU3Ek__BackingField_11() { return &___U3CLineNumberU3Ek__BackingField_11; }
	inline void set_U3CLineNumberU3Ek__BackingField_11(int32_t value)
	{
		___U3CLineNumberU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CLinePositionU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(JsonReaderException_t3553144781, ___U3CLinePositionU3Ek__BackingField_12)); }
	inline int32_t get_U3CLinePositionU3Ek__BackingField_12() const { return ___U3CLinePositionU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CLinePositionU3Ek__BackingField_12() { return &___U3CLinePositionU3Ek__BackingField_12; }
	inline void set_U3CLinePositionU3Ek__BackingField_12(int32_t value)
	{
		___U3CLinePositionU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonReaderException_t3553144781, ___U3CPathU3Ek__BackingField_13)); }
	inline String_t* get_U3CPathU3Ek__BackingField_13() const { return ___U3CPathU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_13() { return &___U3CPathU3Ek__BackingField_13; }
	inline void set_U3CPathU3Ek__BackingField_13(String_t* value)
	{
		___U3CPathU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONREADEREXCEPTION_T3553144781_H
#ifndef MEMBERSERIALIZATION_T1095097135_H
#define MEMBERSERIALIZATION_T1095097135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MemberSerialization
struct  MemberSerialization_t1095097135 
{
public:
	// System.Int32 Newtonsoft.Json.MemberSerialization::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MemberSerialization_t1095097135, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERSERIALIZATION_T1095097135_H
#ifndef REQUIRED_T1591382370_H
#define REQUIRED_T1591382370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Required
struct  Required_t1591382370 
{
public:
	// System.Int32 Newtonsoft.Json.Required::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Required_t1591382370, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIRED_T1591382370_H
#ifndef DEFAULTVALUEHANDLING_T3240998650_H
#define DEFAULTVALUEHANDLING_T3240998650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DefaultValueHandling
struct  DefaultValueHandling_t3240998650 
{
public:
	// System.Int32 Newtonsoft.Json.DefaultValueHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DefaultValueHandling_t3240998650, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTVALUEHANDLING_T3240998650_H
#ifndef NULLABLE_1_T2729527740_H
#define NULLABLE_1_T2729527740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.FloatParseHandling>
struct  Nullable_1_t2729527740 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2729527740, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2729527740, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2729527740_H
#ifndef NULLABLE_1_T2285469903_H
#define NULLABLE_1_T2285469903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.FloatFormatHandling>
struct  Nullable_1_t2285469903 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2285469903, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2285469903, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2285469903_H
#ifndef NULLABLE_1_T179296662_H
#define NULLABLE_1_T179296662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DateParseHandling>
struct  Nullable_1_t179296662 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t179296662, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t179296662, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T179296662_H
#ifndef NULLABLE_1_T430194516_H
#define NULLABLE_1_T430194516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling>
struct  Nullable_1_t430194516 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t430194516, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t430194516, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T430194516_H
#ifndef NULLABLE_1_T3098729937_H
#define NULLABLE_1_T3098729937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DateFormatHandling>
struct  Nullable_1_t3098729937 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3098729937, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3098729937, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3098729937_H
#ifndef NULLABLE_1_T1505470351_H
#define NULLABLE_1_T1505470351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.StringEscapeHandling>
struct  Nullable_1_t1505470351 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1505470351, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1505470351, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1505470351_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef NULLABLE_1_T2764731635_H
#define NULLABLE_1_T2764731635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.MetadataPropertyHandling>
struct  Nullable_1_t2764731635 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2764731635, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2764731635, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2764731635_H
#ifndef NULLABLE_1_T616964910_H
#define NULLABLE_1_T616964910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.ConstructorHandling>
struct  Nullable_1_t616964910 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t616964910, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t616964910, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T616964910_H
#ifndef NULLABLE_1_T1470895410_H
#define NULLABLE_1_T1470895410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.MissingMemberHandling>
struct  Nullable_1_t1470895410 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1470895410, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1470895410, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1470895410_H
#ifndef NULLABLE_1_T588389317_H
#define NULLABLE_1_T588389317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>
struct  Nullable_1_t588389317 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t588389317, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t588389317, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T588389317_H
#ifndef NULLABLE_1_T829731231_H
#define NULLABLE_1_T829731231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.NullValueHandling>
struct  Nullable_1_t829731231 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t829731231, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t829731231, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T829731231_H
#ifndef NULLABLE_1_T2538550894_H
#define NULLABLE_1_T2538550894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.PreserveReferencesHandling>
struct  Nullable_1_t2538550894 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2538550894, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2538550894, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2538550894_H
#ifndef NULLABLE_1_T668593436_H
#define NULLABLE_1_T668593436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>
struct  Nullable_1_t668593436 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t668593436, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t668593436, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T668593436_H
#ifndef NULLABLE_1_T2590601907_H
#define NULLABLE_1_T2590601907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>
struct  Nullable_1_t2590601907 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2590601907, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2590601907, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2590601907_H
#ifndef NULLABLE_1_T3914607011_H
#define NULLABLE_1_T3914607011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.Formatting>
struct  Nullable_1_t3914607011 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3914607011, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3914607011, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3914607011_H
#ifndef DATETIMEPARSER_T3754458704_H
#define DATETIMEPARSER_T3754458704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.DateTimeParser
struct  DateTimeParser_t3754458704 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Year
	int32_t ___Year_0;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Month
	int32_t ___Month_1;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Day
	int32_t ___Day_2;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Hour
	int32_t ___Hour_3;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Minute
	int32_t ___Minute_4;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Second
	int32_t ___Second_5;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Fraction
	int32_t ___Fraction_6;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::ZoneHour
	int32_t ___ZoneHour_7;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::ZoneMinute
	int32_t ___ZoneMinute_8;
	// Newtonsoft.Json.Utilities.ParserTimeZone Newtonsoft.Json.Utilities.DateTimeParser::Zone
	int32_t ___Zone_9;
	// System.Char[] Newtonsoft.Json.Utilities.DateTimeParser::_text
	CharU5BU5D_t3528271667* ____text_10;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::_end
	int32_t ____end_11;

public:
	inline static int32_t get_offset_of_Year_0() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704, ___Year_0)); }
	inline int32_t get_Year_0() const { return ___Year_0; }
	inline int32_t* get_address_of_Year_0() { return &___Year_0; }
	inline void set_Year_0(int32_t value)
	{
		___Year_0 = value;
	}

	inline static int32_t get_offset_of_Month_1() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704, ___Month_1)); }
	inline int32_t get_Month_1() const { return ___Month_1; }
	inline int32_t* get_address_of_Month_1() { return &___Month_1; }
	inline void set_Month_1(int32_t value)
	{
		___Month_1 = value;
	}

	inline static int32_t get_offset_of_Day_2() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704, ___Day_2)); }
	inline int32_t get_Day_2() const { return ___Day_2; }
	inline int32_t* get_address_of_Day_2() { return &___Day_2; }
	inline void set_Day_2(int32_t value)
	{
		___Day_2 = value;
	}

	inline static int32_t get_offset_of_Hour_3() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704, ___Hour_3)); }
	inline int32_t get_Hour_3() const { return ___Hour_3; }
	inline int32_t* get_address_of_Hour_3() { return &___Hour_3; }
	inline void set_Hour_3(int32_t value)
	{
		___Hour_3 = value;
	}

	inline static int32_t get_offset_of_Minute_4() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704, ___Minute_4)); }
	inline int32_t get_Minute_4() const { return ___Minute_4; }
	inline int32_t* get_address_of_Minute_4() { return &___Minute_4; }
	inline void set_Minute_4(int32_t value)
	{
		___Minute_4 = value;
	}

	inline static int32_t get_offset_of_Second_5() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704, ___Second_5)); }
	inline int32_t get_Second_5() const { return ___Second_5; }
	inline int32_t* get_address_of_Second_5() { return &___Second_5; }
	inline void set_Second_5(int32_t value)
	{
		___Second_5 = value;
	}

	inline static int32_t get_offset_of_Fraction_6() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704, ___Fraction_6)); }
	inline int32_t get_Fraction_6() const { return ___Fraction_6; }
	inline int32_t* get_address_of_Fraction_6() { return &___Fraction_6; }
	inline void set_Fraction_6(int32_t value)
	{
		___Fraction_6 = value;
	}

	inline static int32_t get_offset_of_ZoneHour_7() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704, ___ZoneHour_7)); }
	inline int32_t get_ZoneHour_7() const { return ___ZoneHour_7; }
	inline int32_t* get_address_of_ZoneHour_7() { return &___ZoneHour_7; }
	inline void set_ZoneHour_7(int32_t value)
	{
		___ZoneHour_7 = value;
	}

	inline static int32_t get_offset_of_ZoneMinute_8() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704, ___ZoneMinute_8)); }
	inline int32_t get_ZoneMinute_8() const { return ___ZoneMinute_8; }
	inline int32_t* get_address_of_ZoneMinute_8() { return &___ZoneMinute_8; }
	inline void set_ZoneMinute_8(int32_t value)
	{
		___ZoneMinute_8 = value;
	}

	inline static int32_t get_offset_of_Zone_9() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704, ___Zone_9)); }
	inline int32_t get_Zone_9() const { return ___Zone_9; }
	inline int32_t* get_address_of_Zone_9() { return &___Zone_9; }
	inline void set_Zone_9(int32_t value)
	{
		___Zone_9 = value;
	}

	inline static int32_t get_offset_of__text_10() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704, ____text_10)); }
	inline CharU5BU5D_t3528271667* get__text_10() const { return ____text_10; }
	inline CharU5BU5D_t3528271667** get_address_of__text_10() { return &____text_10; }
	inline void set__text_10(CharU5BU5D_t3528271667* value)
	{
		____text_10 = value;
		Il2CppCodeGenWriteBarrier((&____text_10), value);
	}

	inline static int32_t get_offset_of__end_11() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704, ____end_11)); }
	inline int32_t get__end_11() const { return ____end_11; }
	inline int32_t* get_address_of__end_11() { return &____end_11; }
	inline void set__end_11(int32_t value)
	{
		____end_11 = value;
	}
};

struct DateTimeParser_t3754458704_StaticFields
{
public:
	// System.Int32[] Newtonsoft.Json.Utilities.DateTimeParser::Power10
	Int32U5BU5D_t385246372* ___Power10_12;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy
	int32_t ___Lzyyyy_13;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_
	int32_t ___Lzyyyy__14;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_MM
	int32_t ___Lzyyyy_MM_15;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_MM_
	int32_t ___Lzyyyy_MM__16;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_MM_dd
	int32_t ___Lzyyyy_MM_dd_17;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lzyyyy_MM_ddT
	int32_t ___Lzyyyy_MM_ddT_18;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH
	int32_t ___LzHH_19;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH_
	int32_t ___LzHH__20;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH_mm
	int32_t ___LzHH_mm_21;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH_mm_
	int32_t ___LzHH_mm__22;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::LzHH_mm_ss
	int32_t ___LzHH_mm_ss_23;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lz_
	int32_t ___Lz__24;
	// System.Int32 Newtonsoft.Json.Utilities.DateTimeParser::Lz_zz
	int32_t ___Lz_zz_25;

public:
	inline static int32_t get_offset_of_Power10_12() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704_StaticFields, ___Power10_12)); }
	inline Int32U5BU5D_t385246372* get_Power10_12() const { return ___Power10_12; }
	inline Int32U5BU5D_t385246372** get_address_of_Power10_12() { return &___Power10_12; }
	inline void set_Power10_12(Int32U5BU5D_t385246372* value)
	{
		___Power10_12 = value;
		Il2CppCodeGenWriteBarrier((&___Power10_12), value);
	}

	inline static int32_t get_offset_of_Lzyyyy_13() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704_StaticFields, ___Lzyyyy_13)); }
	inline int32_t get_Lzyyyy_13() const { return ___Lzyyyy_13; }
	inline int32_t* get_address_of_Lzyyyy_13() { return &___Lzyyyy_13; }
	inline void set_Lzyyyy_13(int32_t value)
	{
		___Lzyyyy_13 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy__14() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704_StaticFields, ___Lzyyyy__14)); }
	inline int32_t get_Lzyyyy__14() const { return ___Lzyyyy__14; }
	inline int32_t* get_address_of_Lzyyyy__14() { return &___Lzyyyy__14; }
	inline void set_Lzyyyy__14(int32_t value)
	{
		___Lzyyyy__14 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM_15() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704_StaticFields, ___Lzyyyy_MM_15)); }
	inline int32_t get_Lzyyyy_MM_15() const { return ___Lzyyyy_MM_15; }
	inline int32_t* get_address_of_Lzyyyy_MM_15() { return &___Lzyyyy_MM_15; }
	inline void set_Lzyyyy_MM_15(int32_t value)
	{
		___Lzyyyy_MM_15 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM__16() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704_StaticFields, ___Lzyyyy_MM__16)); }
	inline int32_t get_Lzyyyy_MM__16() const { return ___Lzyyyy_MM__16; }
	inline int32_t* get_address_of_Lzyyyy_MM__16() { return &___Lzyyyy_MM__16; }
	inline void set_Lzyyyy_MM__16(int32_t value)
	{
		___Lzyyyy_MM__16 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM_dd_17() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704_StaticFields, ___Lzyyyy_MM_dd_17)); }
	inline int32_t get_Lzyyyy_MM_dd_17() const { return ___Lzyyyy_MM_dd_17; }
	inline int32_t* get_address_of_Lzyyyy_MM_dd_17() { return &___Lzyyyy_MM_dd_17; }
	inline void set_Lzyyyy_MM_dd_17(int32_t value)
	{
		___Lzyyyy_MM_dd_17 = value;
	}

	inline static int32_t get_offset_of_Lzyyyy_MM_ddT_18() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704_StaticFields, ___Lzyyyy_MM_ddT_18)); }
	inline int32_t get_Lzyyyy_MM_ddT_18() const { return ___Lzyyyy_MM_ddT_18; }
	inline int32_t* get_address_of_Lzyyyy_MM_ddT_18() { return &___Lzyyyy_MM_ddT_18; }
	inline void set_Lzyyyy_MM_ddT_18(int32_t value)
	{
		___Lzyyyy_MM_ddT_18 = value;
	}

	inline static int32_t get_offset_of_LzHH_19() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704_StaticFields, ___LzHH_19)); }
	inline int32_t get_LzHH_19() const { return ___LzHH_19; }
	inline int32_t* get_address_of_LzHH_19() { return &___LzHH_19; }
	inline void set_LzHH_19(int32_t value)
	{
		___LzHH_19 = value;
	}

	inline static int32_t get_offset_of_LzHH__20() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704_StaticFields, ___LzHH__20)); }
	inline int32_t get_LzHH__20() const { return ___LzHH__20; }
	inline int32_t* get_address_of_LzHH__20() { return &___LzHH__20; }
	inline void set_LzHH__20(int32_t value)
	{
		___LzHH__20 = value;
	}

	inline static int32_t get_offset_of_LzHH_mm_21() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704_StaticFields, ___LzHH_mm_21)); }
	inline int32_t get_LzHH_mm_21() const { return ___LzHH_mm_21; }
	inline int32_t* get_address_of_LzHH_mm_21() { return &___LzHH_mm_21; }
	inline void set_LzHH_mm_21(int32_t value)
	{
		___LzHH_mm_21 = value;
	}

	inline static int32_t get_offset_of_LzHH_mm__22() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704_StaticFields, ___LzHH_mm__22)); }
	inline int32_t get_LzHH_mm__22() const { return ___LzHH_mm__22; }
	inline int32_t* get_address_of_LzHH_mm__22() { return &___LzHH_mm__22; }
	inline void set_LzHH_mm__22(int32_t value)
	{
		___LzHH_mm__22 = value;
	}

	inline static int32_t get_offset_of_LzHH_mm_ss_23() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704_StaticFields, ___LzHH_mm_ss_23)); }
	inline int32_t get_LzHH_mm_ss_23() const { return ___LzHH_mm_ss_23; }
	inline int32_t* get_address_of_LzHH_mm_ss_23() { return &___LzHH_mm_ss_23; }
	inline void set_LzHH_mm_ss_23(int32_t value)
	{
		___LzHH_mm_ss_23 = value;
	}

	inline static int32_t get_offset_of_Lz__24() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704_StaticFields, ___Lz__24)); }
	inline int32_t get_Lz__24() const { return ___Lz__24; }
	inline int32_t* get_address_of_Lz__24() { return &___Lz__24; }
	inline void set_Lz__24(int32_t value)
	{
		___Lz__24 = value;
	}

	inline static int32_t get_offset_of_Lz_zz_25() { return static_cast<int32_t>(offsetof(DateTimeParser_t3754458704_StaticFields, ___Lz_zz_25)); }
	inline int32_t get_Lz_zz_25() const { return ___Lz_zz_25; }
	inline int32_t* get_address_of_Lz_zz_25() { return &___Lz_zz_25; }
	inline void set_Lz_zz_25(int32_t value)
	{
		___Lz_zz_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Utilities.DateTimeParser
struct DateTimeParser_t3754458704_marshaled_pinvoke
{
	int32_t ___Year_0;
	int32_t ___Month_1;
	int32_t ___Day_2;
	int32_t ___Hour_3;
	int32_t ___Minute_4;
	int32_t ___Second_5;
	int32_t ___Fraction_6;
	int32_t ___ZoneHour_7;
	int32_t ___ZoneMinute_8;
	int32_t ___Zone_9;
	uint8_t* ____text_10;
	int32_t ____end_11;
};
// Native definition for COM marshalling of Newtonsoft.Json.Utilities.DateTimeParser
struct DateTimeParser_t3754458704_marshaled_com
{
	int32_t ___Year_0;
	int32_t ___Month_1;
	int32_t ___Day_2;
	int32_t ___Hour_3;
	int32_t ___Minute_4;
	int32_t ___Second_5;
	int32_t ___Fraction_6;
	int32_t ___ZoneHour_7;
	int32_t ___ZoneMinute_8;
	int32_t ___Zone_9;
	uint8_t* ____text_10;
	int32_t ____end_11;
};
#endif // DATETIMEPARSER_T3754458704_H
#ifndef JSONPOSITION_T2528027714_H
#define JSONPOSITION_T2528027714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonPosition
struct  JsonPosition_t2528027714 
{
public:
	// Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonPosition::Type
	int32_t ___Type_1;
	// System.Int32 Newtonsoft.Json.JsonPosition::Position
	int32_t ___Position_2;
	// System.String Newtonsoft.Json.JsonPosition::PropertyName
	String_t* ___PropertyName_3;
	// System.Boolean Newtonsoft.Json.JsonPosition::HasIndex
	bool ___HasIndex_4;

public:
	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(JsonPosition_t2528027714, ___Type_1)); }
	inline int32_t get_Type_1() const { return ___Type_1; }
	inline int32_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(int32_t value)
	{
		___Type_1 = value;
	}

	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(JsonPosition_t2528027714, ___Position_2)); }
	inline int32_t get_Position_2() const { return ___Position_2; }
	inline int32_t* get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(int32_t value)
	{
		___Position_2 = value;
	}

	inline static int32_t get_offset_of_PropertyName_3() { return static_cast<int32_t>(offsetof(JsonPosition_t2528027714, ___PropertyName_3)); }
	inline String_t* get_PropertyName_3() const { return ___PropertyName_3; }
	inline String_t** get_address_of_PropertyName_3() { return &___PropertyName_3; }
	inline void set_PropertyName_3(String_t* value)
	{
		___PropertyName_3 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyName_3), value);
	}

	inline static int32_t get_offset_of_HasIndex_4() { return static_cast<int32_t>(offsetof(JsonPosition_t2528027714, ___HasIndex_4)); }
	inline bool get_HasIndex_4() const { return ___HasIndex_4; }
	inline bool* get_address_of_HasIndex_4() { return &___HasIndex_4; }
	inline void set_HasIndex_4(bool value)
	{
		___HasIndex_4 = value;
	}
};

struct JsonPosition_t2528027714_StaticFields
{
public:
	// System.Char[] Newtonsoft.Json.JsonPosition::SpecialCharacters
	CharU5BU5D_t3528271667* ___SpecialCharacters_0;

public:
	inline static int32_t get_offset_of_SpecialCharacters_0() { return static_cast<int32_t>(offsetof(JsonPosition_t2528027714_StaticFields, ___SpecialCharacters_0)); }
	inline CharU5BU5D_t3528271667* get_SpecialCharacters_0() const { return ___SpecialCharacters_0; }
	inline CharU5BU5D_t3528271667** get_address_of_SpecialCharacters_0() { return &___SpecialCharacters_0; }
	inline void set_SpecialCharacters_0(CharU5BU5D_t3528271667* value)
	{
		___SpecialCharacters_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpecialCharacters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_t2528027714_marshaled_pinvoke
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	char* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
// Native definition for COM marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_t2528027714_marshaled_com
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	Il2CppChar* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
#endif // JSONPOSITION_T2528027714_H
#ifndef NULLABLE_1_T3051693050_H
#define NULLABLE_1_T3051693050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>
struct  Nullable_1_t3051693050 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3051693050, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3051693050, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3051693050_H
#ifndef NULLABLE_1_T1159582132_H
#define NULLABLE_1_T1159582132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.TypeNameHandling>
struct  Nullable_1_t1159582132 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1159582132, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1159582132, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1159582132_H
#ifndef NULLABLE_1_T3313944452_H
#define NULLABLE_1_T3313944452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.Required>
struct  Nullable_1_t3313944452 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3313944452, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3313944452, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3313944452_H
#ifndef STREAMINGCONTEXT_T3711869237_H
#define STREAMINGCONTEXT_T3711869237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContext
struct  StreamingContext_t3711869237 
{
public:
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::state
	int32_t ___state_0;
	// System.Object System.Runtime.Serialization.StreamingContext::additional
	RuntimeObject * ___additional_1;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(StreamingContext_t3711869237, ___state_0)); }
	inline int32_t get_state_0() const { return ___state_0; }
	inline int32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(int32_t value)
	{
		___state_0 = value;
	}

	inline static int32_t get_offset_of_additional_1() { return static_cast<int32_t>(offsetof(StreamingContext_t3711869237, ___additional_1)); }
	inline RuntimeObject * get_additional_1() const { return ___additional_1; }
	inline RuntimeObject ** get_address_of_additional_1() { return &___additional_1; }
	inline void set_additional_1(RuntimeObject * value)
	{
		___additional_1 = value;
		Il2CppCodeGenWriteBarrier((&___additional_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t3711869237_marshaled_pinvoke
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t3711869237_marshaled_com
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
#endif // STREAMINGCONTEXT_T3711869237_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef NULLABLE_1_T1139464023_H
#define NULLABLE_1_T1139464023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Runtime.Serialization.StreamingContext>
struct  Nullable_1_t1139464023 
{
public:
	// T System.Nullable`1::value
	StreamingContext_t3711869237  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1139464023, ___value_0)); }
	inline StreamingContext_t3711869237  get_value_0() const { return ___value_0; }
	inline StreamingContext_t3711869237 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(StreamingContext_t3711869237  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1139464023, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1139464023_H
#ifndef JSONCONTAINERATTRIBUTE_T3874748064_H
#define JSONCONTAINERATTRIBUTE_T3874748064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonContainerAttribute
struct  JsonContainerAttribute_t3874748064  : public Attribute_t861562559
{
public:
	// System.Type Newtonsoft.Json.JsonContainerAttribute::<ItemConverterType>k__BackingField
	Type_t * ___U3CItemConverterTypeU3Ek__BackingField_0;
	// System.Object[] Newtonsoft.Json.JsonContainerAttribute::<ItemConverterParameters>k__BackingField
	ObjectU5BU5D_t2843939325* ___U3CItemConverterParametersU3Ek__BackingField_1;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonContainerAttribute::_isReference
	Nullable_1_t1819850047  ____isReference_2;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonContainerAttribute::_itemIsReference
	Nullable_1_t1819850047  ____itemIsReference_3;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.JsonContainerAttribute::_itemReferenceLoopHandling
	Nullable_1_t3051693050  ____itemReferenceLoopHandling_4;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.JsonContainerAttribute::_itemTypeNameHandling
	Nullable_1_t1159582132  ____itemTypeNameHandling_5;

public:
	inline static int32_t get_offset_of_U3CItemConverterTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JsonContainerAttribute_t3874748064, ___U3CItemConverterTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CItemConverterTypeU3Ek__BackingField_0() const { return ___U3CItemConverterTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CItemConverterTypeU3Ek__BackingField_0() { return &___U3CItemConverterTypeU3Ek__BackingField_0; }
	inline void set_U3CItemConverterTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CItemConverterTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemConverterTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CItemConverterParametersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonContainerAttribute_t3874748064, ___U3CItemConverterParametersU3Ek__BackingField_1)); }
	inline ObjectU5BU5D_t2843939325* get_U3CItemConverterParametersU3Ek__BackingField_1() const { return ___U3CItemConverterParametersU3Ek__BackingField_1; }
	inline ObjectU5BU5D_t2843939325** get_address_of_U3CItemConverterParametersU3Ek__BackingField_1() { return &___U3CItemConverterParametersU3Ek__BackingField_1; }
	inline void set_U3CItemConverterParametersU3Ek__BackingField_1(ObjectU5BU5D_t2843939325* value)
	{
		___U3CItemConverterParametersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemConverterParametersU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of__isReference_2() { return static_cast<int32_t>(offsetof(JsonContainerAttribute_t3874748064, ____isReference_2)); }
	inline Nullable_1_t1819850047  get__isReference_2() const { return ____isReference_2; }
	inline Nullable_1_t1819850047 * get_address_of__isReference_2() { return &____isReference_2; }
	inline void set__isReference_2(Nullable_1_t1819850047  value)
	{
		____isReference_2 = value;
	}

	inline static int32_t get_offset_of__itemIsReference_3() { return static_cast<int32_t>(offsetof(JsonContainerAttribute_t3874748064, ____itemIsReference_3)); }
	inline Nullable_1_t1819850047  get__itemIsReference_3() const { return ____itemIsReference_3; }
	inline Nullable_1_t1819850047 * get_address_of__itemIsReference_3() { return &____itemIsReference_3; }
	inline void set__itemIsReference_3(Nullable_1_t1819850047  value)
	{
		____itemIsReference_3 = value;
	}

	inline static int32_t get_offset_of__itemReferenceLoopHandling_4() { return static_cast<int32_t>(offsetof(JsonContainerAttribute_t3874748064, ____itemReferenceLoopHandling_4)); }
	inline Nullable_1_t3051693050  get__itemReferenceLoopHandling_4() const { return ____itemReferenceLoopHandling_4; }
	inline Nullable_1_t3051693050 * get_address_of__itemReferenceLoopHandling_4() { return &____itemReferenceLoopHandling_4; }
	inline void set__itemReferenceLoopHandling_4(Nullable_1_t3051693050  value)
	{
		____itemReferenceLoopHandling_4 = value;
	}

	inline static int32_t get_offset_of__itemTypeNameHandling_5() { return static_cast<int32_t>(offsetof(JsonContainerAttribute_t3874748064, ____itemTypeNameHandling_5)); }
	inline Nullable_1_t1159582132  get__itemTypeNameHandling_5() const { return ____itemTypeNameHandling_5; }
	inline Nullable_1_t1159582132 * get_address_of__itemTypeNameHandling_5() { return &____itemTypeNameHandling_5; }
	inline void set__itemTypeNameHandling_5(Nullable_1_t1159582132  value)
	{
		____itemTypeNameHandling_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTAINERATTRIBUTE_T3874748064_H
#ifndef JSONSERIALIZER_T1424496335_H
#define JSONSERIALIZER_T1424496335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonSerializer
struct  JsonSerializer_t1424496335  : public RuntimeObject
{
public:
	// Newtonsoft.Json.TypeNameHandling Newtonsoft.Json.JsonSerializer::_typeNameHandling
	int32_t ____typeNameHandling_0;
	// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle Newtonsoft.Json.JsonSerializer::_typeNameAssemblyFormat
	int32_t ____typeNameAssemblyFormat_1;
	// Newtonsoft.Json.PreserveReferencesHandling Newtonsoft.Json.JsonSerializer::_preserveReferencesHandling
	int32_t ____preserveReferencesHandling_2;
	// Newtonsoft.Json.ReferenceLoopHandling Newtonsoft.Json.JsonSerializer::_referenceLoopHandling
	int32_t ____referenceLoopHandling_3;
	// Newtonsoft.Json.MissingMemberHandling Newtonsoft.Json.JsonSerializer::_missingMemberHandling
	int32_t ____missingMemberHandling_4;
	// Newtonsoft.Json.ObjectCreationHandling Newtonsoft.Json.JsonSerializer::_objectCreationHandling
	int32_t ____objectCreationHandling_5;
	// Newtonsoft.Json.NullValueHandling Newtonsoft.Json.JsonSerializer::_nullValueHandling
	int32_t ____nullValueHandling_6;
	// Newtonsoft.Json.DefaultValueHandling Newtonsoft.Json.JsonSerializer::_defaultValueHandling
	int32_t ____defaultValueHandling_7;
	// Newtonsoft.Json.ConstructorHandling Newtonsoft.Json.JsonSerializer::_constructorHandling
	int32_t ____constructorHandling_8;
	// Newtonsoft.Json.MetadataPropertyHandling Newtonsoft.Json.JsonSerializer::_metadataPropertyHandling
	int32_t ____metadataPropertyHandling_9;
	// Newtonsoft.Json.JsonConverterCollection Newtonsoft.Json.JsonSerializer::_converters
	JsonConverterCollection_t3675964822 * ____converters_10;
	// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.JsonSerializer::_contractResolver
	RuntimeObject* ____contractResolver_11;
	// Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.JsonSerializer::_traceWriter
	RuntimeObject* ____traceWriter_12;
	// System.Collections.IEqualityComparer Newtonsoft.Json.JsonSerializer::_equalityComparer
	RuntimeObject* ____equalityComparer_13;
	// System.Runtime.Serialization.SerializationBinder Newtonsoft.Json.JsonSerializer::_binder
	SerializationBinder_t274213469 * ____binder_14;
	// System.Runtime.Serialization.StreamingContext Newtonsoft.Json.JsonSerializer::_context
	StreamingContext_t3711869237  ____context_15;
	// Newtonsoft.Json.Serialization.IReferenceResolver Newtonsoft.Json.JsonSerializer::_referenceResolver
	RuntimeObject* ____referenceResolver_16;
	// System.Nullable`1<Newtonsoft.Json.Formatting> Newtonsoft.Json.JsonSerializer::_formatting
	Nullable_1_t3914607011  ____formatting_17;
	// System.Nullable`1<Newtonsoft.Json.DateFormatHandling> Newtonsoft.Json.JsonSerializer::_dateFormatHandling
	Nullable_1_t3098729937  ____dateFormatHandling_18;
	// System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling> Newtonsoft.Json.JsonSerializer::_dateTimeZoneHandling
	Nullable_1_t430194516  ____dateTimeZoneHandling_19;
	// System.Nullable`1<Newtonsoft.Json.DateParseHandling> Newtonsoft.Json.JsonSerializer::_dateParseHandling
	Nullable_1_t179296662  ____dateParseHandling_20;
	// System.Nullable`1<Newtonsoft.Json.FloatFormatHandling> Newtonsoft.Json.JsonSerializer::_floatFormatHandling
	Nullable_1_t2285469903  ____floatFormatHandling_21;
	// System.Nullable`1<Newtonsoft.Json.FloatParseHandling> Newtonsoft.Json.JsonSerializer::_floatParseHandling
	Nullable_1_t2729527740  ____floatParseHandling_22;
	// System.Nullable`1<Newtonsoft.Json.StringEscapeHandling> Newtonsoft.Json.JsonSerializer::_stringEscapeHandling
	Nullable_1_t1505470351  ____stringEscapeHandling_23;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonSerializer::_culture
	CultureInfo_t4157843068 * ____culture_24;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonSerializer::_maxDepth
	Nullable_1_t378540539  ____maxDepth_25;
	// System.Boolean Newtonsoft.Json.JsonSerializer::_maxDepthSet
	bool ____maxDepthSet_26;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonSerializer::_checkAdditionalContent
	Nullable_1_t1819850047  ____checkAdditionalContent_27;
	// System.String Newtonsoft.Json.JsonSerializer::_dateFormatString
	String_t* ____dateFormatString_28;
	// System.Boolean Newtonsoft.Json.JsonSerializer::_dateFormatStringSet
	bool ____dateFormatStringSet_29;
	// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs> Newtonsoft.Json.JsonSerializer::Error
	EventHandler_1_t2935836205 * ___Error_30;

public:
	inline static int32_t get_offset_of__typeNameHandling_0() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____typeNameHandling_0)); }
	inline int32_t get__typeNameHandling_0() const { return ____typeNameHandling_0; }
	inline int32_t* get_address_of__typeNameHandling_0() { return &____typeNameHandling_0; }
	inline void set__typeNameHandling_0(int32_t value)
	{
		____typeNameHandling_0 = value;
	}

	inline static int32_t get_offset_of__typeNameAssemblyFormat_1() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____typeNameAssemblyFormat_1)); }
	inline int32_t get__typeNameAssemblyFormat_1() const { return ____typeNameAssemblyFormat_1; }
	inline int32_t* get_address_of__typeNameAssemblyFormat_1() { return &____typeNameAssemblyFormat_1; }
	inline void set__typeNameAssemblyFormat_1(int32_t value)
	{
		____typeNameAssemblyFormat_1 = value;
	}

	inline static int32_t get_offset_of__preserveReferencesHandling_2() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____preserveReferencesHandling_2)); }
	inline int32_t get__preserveReferencesHandling_2() const { return ____preserveReferencesHandling_2; }
	inline int32_t* get_address_of__preserveReferencesHandling_2() { return &____preserveReferencesHandling_2; }
	inline void set__preserveReferencesHandling_2(int32_t value)
	{
		____preserveReferencesHandling_2 = value;
	}

	inline static int32_t get_offset_of__referenceLoopHandling_3() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____referenceLoopHandling_3)); }
	inline int32_t get__referenceLoopHandling_3() const { return ____referenceLoopHandling_3; }
	inline int32_t* get_address_of__referenceLoopHandling_3() { return &____referenceLoopHandling_3; }
	inline void set__referenceLoopHandling_3(int32_t value)
	{
		____referenceLoopHandling_3 = value;
	}

	inline static int32_t get_offset_of__missingMemberHandling_4() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____missingMemberHandling_4)); }
	inline int32_t get__missingMemberHandling_4() const { return ____missingMemberHandling_4; }
	inline int32_t* get_address_of__missingMemberHandling_4() { return &____missingMemberHandling_4; }
	inline void set__missingMemberHandling_4(int32_t value)
	{
		____missingMemberHandling_4 = value;
	}

	inline static int32_t get_offset_of__objectCreationHandling_5() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____objectCreationHandling_5)); }
	inline int32_t get__objectCreationHandling_5() const { return ____objectCreationHandling_5; }
	inline int32_t* get_address_of__objectCreationHandling_5() { return &____objectCreationHandling_5; }
	inline void set__objectCreationHandling_5(int32_t value)
	{
		____objectCreationHandling_5 = value;
	}

	inline static int32_t get_offset_of__nullValueHandling_6() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____nullValueHandling_6)); }
	inline int32_t get__nullValueHandling_6() const { return ____nullValueHandling_6; }
	inline int32_t* get_address_of__nullValueHandling_6() { return &____nullValueHandling_6; }
	inline void set__nullValueHandling_6(int32_t value)
	{
		____nullValueHandling_6 = value;
	}

	inline static int32_t get_offset_of__defaultValueHandling_7() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____defaultValueHandling_7)); }
	inline int32_t get__defaultValueHandling_7() const { return ____defaultValueHandling_7; }
	inline int32_t* get_address_of__defaultValueHandling_7() { return &____defaultValueHandling_7; }
	inline void set__defaultValueHandling_7(int32_t value)
	{
		____defaultValueHandling_7 = value;
	}

	inline static int32_t get_offset_of__constructorHandling_8() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____constructorHandling_8)); }
	inline int32_t get__constructorHandling_8() const { return ____constructorHandling_8; }
	inline int32_t* get_address_of__constructorHandling_8() { return &____constructorHandling_8; }
	inline void set__constructorHandling_8(int32_t value)
	{
		____constructorHandling_8 = value;
	}

	inline static int32_t get_offset_of__metadataPropertyHandling_9() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____metadataPropertyHandling_9)); }
	inline int32_t get__metadataPropertyHandling_9() const { return ____metadataPropertyHandling_9; }
	inline int32_t* get_address_of__metadataPropertyHandling_9() { return &____metadataPropertyHandling_9; }
	inline void set__metadataPropertyHandling_9(int32_t value)
	{
		____metadataPropertyHandling_9 = value;
	}

	inline static int32_t get_offset_of__converters_10() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____converters_10)); }
	inline JsonConverterCollection_t3675964822 * get__converters_10() const { return ____converters_10; }
	inline JsonConverterCollection_t3675964822 ** get_address_of__converters_10() { return &____converters_10; }
	inline void set__converters_10(JsonConverterCollection_t3675964822 * value)
	{
		____converters_10 = value;
		Il2CppCodeGenWriteBarrier((&____converters_10), value);
	}

	inline static int32_t get_offset_of__contractResolver_11() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____contractResolver_11)); }
	inline RuntimeObject* get__contractResolver_11() const { return ____contractResolver_11; }
	inline RuntimeObject** get_address_of__contractResolver_11() { return &____contractResolver_11; }
	inline void set__contractResolver_11(RuntimeObject* value)
	{
		____contractResolver_11 = value;
		Il2CppCodeGenWriteBarrier((&____contractResolver_11), value);
	}

	inline static int32_t get_offset_of__traceWriter_12() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____traceWriter_12)); }
	inline RuntimeObject* get__traceWriter_12() const { return ____traceWriter_12; }
	inline RuntimeObject** get_address_of__traceWriter_12() { return &____traceWriter_12; }
	inline void set__traceWriter_12(RuntimeObject* value)
	{
		____traceWriter_12 = value;
		Il2CppCodeGenWriteBarrier((&____traceWriter_12), value);
	}

	inline static int32_t get_offset_of__equalityComparer_13() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____equalityComparer_13)); }
	inline RuntimeObject* get__equalityComparer_13() const { return ____equalityComparer_13; }
	inline RuntimeObject** get_address_of__equalityComparer_13() { return &____equalityComparer_13; }
	inline void set__equalityComparer_13(RuntimeObject* value)
	{
		____equalityComparer_13 = value;
		Il2CppCodeGenWriteBarrier((&____equalityComparer_13), value);
	}

	inline static int32_t get_offset_of__binder_14() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____binder_14)); }
	inline SerializationBinder_t274213469 * get__binder_14() const { return ____binder_14; }
	inline SerializationBinder_t274213469 ** get_address_of__binder_14() { return &____binder_14; }
	inline void set__binder_14(SerializationBinder_t274213469 * value)
	{
		____binder_14 = value;
		Il2CppCodeGenWriteBarrier((&____binder_14), value);
	}

	inline static int32_t get_offset_of__context_15() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____context_15)); }
	inline StreamingContext_t3711869237  get__context_15() const { return ____context_15; }
	inline StreamingContext_t3711869237 * get_address_of__context_15() { return &____context_15; }
	inline void set__context_15(StreamingContext_t3711869237  value)
	{
		____context_15 = value;
	}

	inline static int32_t get_offset_of__referenceResolver_16() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____referenceResolver_16)); }
	inline RuntimeObject* get__referenceResolver_16() const { return ____referenceResolver_16; }
	inline RuntimeObject** get_address_of__referenceResolver_16() { return &____referenceResolver_16; }
	inline void set__referenceResolver_16(RuntimeObject* value)
	{
		____referenceResolver_16 = value;
		Il2CppCodeGenWriteBarrier((&____referenceResolver_16), value);
	}

	inline static int32_t get_offset_of__formatting_17() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____formatting_17)); }
	inline Nullable_1_t3914607011  get__formatting_17() const { return ____formatting_17; }
	inline Nullable_1_t3914607011 * get_address_of__formatting_17() { return &____formatting_17; }
	inline void set__formatting_17(Nullable_1_t3914607011  value)
	{
		____formatting_17 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_18() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____dateFormatHandling_18)); }
	inline Nullable_1_t3098729937  get__dateFormatHandling_18() const { return ____dateFormatHandling_18; }
	inline Nullable_1_t3098729937 * get_address_of__dateFormatHandling_18() { return &____dateFormatHandling_18; }
	inline void set__dateFormatHandling_18(Nullable_1_t3098729937  value)
	{
		____dateFormatHandling_18 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_19() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____dateTimeZoneHandling_19)); }
	inline Nullable_1_t430194516  get__dateTimeZoneHandling_19() const { return ____dateTimeZoneHandling_19; }
	inline Nullable_1_t430194516 * get_address_of__dateTimeZoneHandling_19() { return &____dateTimeZoneHandling_19; }
	inline void set__dateTimeZoneHandling_19(Nullable_1_t430194516  value)
	{
		____dateTimeZoneHandling_19 = value;
	}

	inline static int32_t get_offset_of__dateParseHandling_20() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____dateParseHandling_20)); }
	inline Nullable_1_t179296662  get__dateParseHandling_20() const { return ____dateParseHandling_20; }
	inline Nullable_1_t179296662 * get_address_of__dateParseHandling_20() { return &____dateParseHandling_20; }
	inline void set__dateParseHandling_20(Nullable_1_t179296662  value)
	{
		____dateParseHandling_20 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_21() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____floatFormatHandling_21)); }
	inline Nullable_1_t2285469903  get__floatFormatHandling_21() const { return ____floatFormatHandling_21; }
	inline Nullable_1_t2285469903 * get_address_of__floatFormatHandling_21() { return &____floatFormatHandling_21; }
	inline void set__floatFormatHandling_21(Nullable_1_t2285469903  value)
	{
		____floatFormatHandling_21 = value;
	}

	inline static int32_t get_offset_of__floatParseHandling_22() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____floatParseHandling_22)); }
	inline Nullable_1_t2729527740  get__floatParseHandling_22() const { return ____floatParseHandling_22; }
	inline Nullable_1_t2729527740 * get_address_of__floatParseHandling_22() { return &____floatParseHandling_22; }
	inline void set__floatParseHandling_22(Nullable_1_t2729527740  value)
	{
		____floatParseHandling_22 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_23() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____stringEscapeHandling_23)); }
	inline Nullable_1_t1505470351  get__stringEscapeHandling_23() const { return ____stringEscapeHandling_23; }
	inline Nullable_1_t1505470351 * get_address_of__stringEscapeHandling_23() { return &____stringEscapeHandling_23; }
	inline void set__stringEscapeHandling_23(Nullable_1_t1505470351  value)
	{
		____stringEscapeHandling_23 = value;
	}

	inline static int32_t get_offset_of__culture_24() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____culture_24)); }
	inline CultureInfo_t4157843068 * get__culture_24() const { return ____culture_24; }
	inline CultureInfo_t4157843068 ** get_address_of__culture_24() { return &____culture_24; }
	inline void set__culture_24(CultureInfo_t4157843068 * value)
	{
		____culture_24 = value;
		Il2CppCodeGenWriteBarrier((&____culture_24), value);
	}

	inline static int32_t get_offset_of__maxDepth_25() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____maxDepth_25)); }
	inline Nullable_1_t378540539  get__maxDepth_25() const { return ____maxDepth_25; }
	inline Nullable_1_t378540539 * get_address_of__maxDepth_25() { return &____maxDepth_25; }
	inline void set__maxDepth_25(Nullable_1_t378540539  value)
	{
		____maxDepth_25 = value;
	}

	inline static int32_t get_offset_of__maxDepthSet_26() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____maxDepthSet_26)); }
	inline bool get__maxDepthSet_26() const { return ____maxDepthSet_26; }
	inline bool* get_address_of__maxDepthSet_26() { return &____maxDepthSet_26; }
	inline void set__maxDepthSet_26(bool value)
	{
		____maxDepthSet_26 = value;
	}

	inline static int32_t get_offset_of__checkAdditionalContent_27() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____checkAdditionalContent_27)); }
	inline Nullable_1_t1819850047  get__checkAdditionalContent_27() const { return ____checkAdditionalContent_27; }
	inline Nullable_1_t1819850047 * get_address_of__checkAdditionalContent_27() { return &____checkAdditionalContent_27; }
	inline void set__checkAdditionalContent_27(Nullable_1_t1819850047  value)
	{
		____checkAdditionalContent_27 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_28() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____dateFormatString_28)); }
	inline String_t* get__dateFormatString_28() const { return ____dateFormatString_28; }
	inline String_t** get_address_of__dateFormatString_28() { return &____dateFormatString_28; }
	inline void set__dateFormatString_28(String_t* value)
	{
		____dateFormatString_28 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_28), value);
	}

	inline static int32_t get_offset_of__dateFormatStringSet_29() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____dateFormatStringSet_29)); }
	inline bool get__dateFormatStringSet_29() const { return ____dateFormatStringSet_29; }
	inline bool* get_address_of__dateFormatStringSet_29() { return &____dateFormatStringSet_29; }
	inline void set__dateFormatStringSet_29(bool value)
	{
		____dateFormatStringSet_29 = value;
	}

	inline static int32_t get_offset_of_Error_30() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ___Error_30)); }
	inline EventHandler_1_t2935836205 * get_Error_30() const { return ___Error_30; }
	inline EventHandler_1_t2935836205 ** get_address_of_Error_30() { return &___Error_30; }
	inline void set_Error_30(EventHandler_1_t2935836205 * value)
	{
		___Error_30 = value;
		Il2CppCodeGenWriteBarrier((&___Error_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZER_T1424496335_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef JSONREADER_T2369136700_H
#define JSONREADER_T2369136700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonReader
struct  JsonReader_t2369136700  : public RuntimeObject
{
public:
	// Newtonsoft.Json.JsonToken Newtonsoft.Json.JsonReader::_tokenType
	int32_t ____tokenType_0;
	// System.Object Newtonsoft.Json.JsonReader::_value
	RuntimeObject * ____value_1;
	// System.Char Newtonsoft.Json.JsonReader::_quoteChar
	Il2CppChar ____quoteChar_2;
	// Newtonsoft.Json.JsonReader/State Newtonsoft.Json.JsonReader::_currentState
	int32_t ____currentState_3;
	// Newtonsoft.Json.JsonPosition Newtonsoft.Json.JsonReader::_currentPosition
	JsonPosition_t2528027714  ____currentPosition_4;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonReader::_culture
	CultureInfo_t4157843068 * ____culture_5;
	// Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonReader::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_6;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonReader::_maxDepth
	Nullable_1_t378540539  ____maxDepth_7;
	// System.Boolean Newtonsoft.Json.JsonReader::_hasExceededMaxDepth
	bool ____hasExceededMaxDepth_8;
	// Newtonsoft.Json.DateParseHandling Newtonsoft.Json.JsonReader::_dateParseHandling
	int32_t ____dateParseHandling_9;
	// Newtonsoft.Json.FloatParseHandling Newtonsoft.Json.JsonReader::_floatParseHandling
	int32_t ____floatParseHandling_10;
	// System.String Newtonsoft.Json.JsonReader::_dateFormatString
	String_t* ____dateFormatString_11;
	// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition> Newtonsoft.Json.JsonReader::_stack
	List_1_t4000102456 * ____stack_12;
	// System.Boolean Newtonsoft.Json.JsonReader::<CloseInput>k__BackingField
	bool ___U3CCloseInputU3Ek__BackingField_13;
	// System.Boolean Newtonsoft.Json.JsonReader::<SupportMultipleContent>k__BackingField
	bool ___U3CSupportMultipleContentU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of__tokenType_0() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____tokenType_0)); }
	inline int32_t get__tokenType_0() const { return ____tokenType_0; }
	inline int32_t* get_address_of__tokenType_0() { return &____tokenType_0; }
	inline void set__tokenType_0(int32_t value)
	{
		____tokenType_0 = value;
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____value_1)); }
	inline RuntimeObject * get__value_1() const { return ____value_1; }
	inline RuntimeObject ** get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(RuntimeObject * value)
	{
		____value_1 = value;
		Il2CppCodeGenWriteBarrier((&____value_1), value);
	}

	inline static int32_t get_offset_of__quoteChar_2() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____quoteChar_2)); }
	inline Il2CppChar get__quoteChar_2() const { return ____quoteChar_2; }
	inline Il2CppChar* get_address_of__quoteChar_2() { return &____quoteChar_2; }
	inline void set__quoteChar_2(Il2CppChar value)
	{
		____quoteChar_2 = value;
	}

	inline static int32_t get_offset_of__currentState_3() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____currentState_3)); }
	inline int32_t get__currentState_3() const { return ____currentState_3; }
	inline int32_t* get_address_of__currentState_3() { return &____currentState_3; }
	inline void set__currentState_3(int32_t value)
	{
		____currentState_3 = value;
	}

	inline static int32_t get_offset_of__currentPosition_4() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____currentPosition_4)); }
	inline JsonPosition_t2528027714  get__currentPosition_4() const { return ____currentPosition_4; }
	inline JsonPosition_t2528027714 * get_address_of__currentPosition_4() { return &____currentPosition_4; }
	inline void set__currentPosition_4(JsonPosition_t2528027714  value)
	{
		____currentPosition_4 = value;
	}

	inline static int32_t get_offset_of__culture_5() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____culture_5)); }
	inline CultureInfo_t4157843068 * get__culture_5() const { return ____culture_5; }
	inline CultureInfo_t4157843068 ** get_address_of__culture_5() { return &____culture_5; }
	inline void set__culture_5(CultureInfo_t4157843068 * value)
	{
		____culture_5 = value;
		Il2CppCodeGenWriteBarrier((&____culture_5), value);
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_6() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____dateTimeZoneHandling_6)); }
	inline int32_t get__dateTimeZoneHandling_6() const { return ____dateTimeZoneHandling_6; }
	inline int32_t* get_address_of__dateTimeZoneHandling_6() { return &____dateTimeZoneHandling_6; }
	inline void set__dateTimeZoneHandling_6(int32_t value)
	{
		____dateTimeZoneHandling_6 = value;
	}

	inline static int32_t get_offset_of__maxDepth_7() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____maxDepth_7)); }
	inline Nullable_1_t378540539  get__maxDepth_7() const { return ____maxDepth_7; }
	inline Nullable_1_t378540539 * get_address_of__maxDepth_7() { return &____maxDepth_7; }
	inline void set__maxDepth_7(Nullable_1_t378540539  value)
	{
		____maxDepth_7 = value;
	}

	inline static int32_t get_offset_of__hasExceededMaxDepth_8() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____hasExceededMaxDepth_8)); }
	inline bool get__hasExceededMaxDepth_8() const { return ____hasExceededMaxDepth_8; }
	inline bool* get_address_of__hasExceededMaxDepth_8() { return &____hasExceededMaxDepth_8; }
	inline void set__hasExceededMaxDepth_8(bool value)
	{
		____hasExceededMaxDepth_8 = value;
	}

	inline static int32_t get_offset_of__dateParseHandling_9() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____dateParseHandling_9)); }
	inline int32_t get__dateParseHandling_9() const { return ____dateParseHandling_9; }
	inline int32_t* get_address_of__dateParseHandling_9() { return &____dateParseHandling_9; }
	inline void set__dateParseHandling_9(int32_t value)
	{
		____dateParseHandling_9 = value;
	}

	inline static int32_t get_offset_of__floatParseHandling_10() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____floatParseHandling_10)); }
	inline int32_t get__floatParseHandling_10() const { return ____floatParseHandling_10; }
	inline int32_t* get_address_of__floatParseHandling_10() { return &____floatParseHandling_10; }
	inline void set__floatParseHandling_10(int32_t value)
	{
		____floatParseHandling_10 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_11() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____dateFormatString_11)); }
	inline String_t* get__dateFormatString_11() const { return ____dateFormatString_11; }
	inline String_t** get_address_of__dateFormatString_11() { return &____dateFormatString_11; }
	inline void set__dateFormatString_11(String_t* value)
	{
		____dateFormatString_11 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_11), value);
	}

	inline static int32_t get_offset_of__stack_12() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____stack_12)); }
	inline List_1_t4000102456 * get__stack_12() const { return ____stack_12; }
	inline List_1_t4000102456 ** get_address_of__stack_12() { return &____stack_12; }
	inline void set__stack_12(List_1_t4000102456 * value)
	{
		____stack_12 = value;
		Il2CppCodeGenWriteBarrier((&____stack_12), value);
	}

	inline static int32_t get_offset_of_U3CCloseInputU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ___U3CCloseInputU3Ek__BackingField_13)); }
	inline bool get_U3CCloseInputU3Ek__BackingField_13() const { return ___U3CCloseInputU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CCloseInputU3Ek__BackingField_13() { return &___U3CCloseInputU3Ek__BackingField_13; }
	inline void set_U3CCloseInputU3Ek__BackingField_13(bool value)
	{
		___U3CCloseInputU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CSupportMultipleContentU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ___U3CSupportMultipleContentU3Ek__BackingField_14)); }
	inline bool get_U3CSupportMultipleContentU3Ek__BackingField_14() const { return ___U3CSupportMultipleContentU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CSupportMultipleContentU3Ek__BackingField_14() { return &___U3CSupportMultipleContentU3Ek__BackingField_14; }
	inline void set_U3CSupportMultipleContentU3Ek__BackingField_14(bool value)
	{
		___U3CSupportMultipleContentU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONREADER_T2369136700_H
#ifndef ADDINGNEWEVENTHANDLER_T4128025860_H
#define ADDINGNEWEVENTHANDLER_T4128025860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.AddingNewEventHandler
struct  AddingNewEventHandler_t4128025860  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDINGNEWEVENTHANDLER_T4128025860_H
#ifndef NOTIFYCOLLECTIONCHANGEDEVENTHANDLER_T2206564399_H
#define NOTIFYCOLLECTIONCHANGEDEVENTHANDLER_T2206564399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.NotifyCollectionChangedEventHandler
struct  NotifyCollectionChangedEventHandler_t2206564399  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFYCOLLECTIONCHANGEDEVENTHANDLER_T2206564399_H
#ifndef JSONPROPERTYATTRIBUTE_T2223277056_H
#define JSONPROPERTYATTRIBUTE_T2223277056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonPropertyAttribute
struct  JsonPropertyAttribute_t2223277056  : public Attribute_t861562559
{
public:
	// System.Nullable`1<Newtonsoft.Json.NullValueHandling> Newtonsoft.Json.JsonPropertyAttribute::_nullValueHandling
	Nullable_1_t829731231  ____nullValueHandling_0;
	// System.Nullable`1<Newtonsoft.Json.DefaultValueHandling> Newtonsoft.Json.JsonPropertyAttribute::_defaultValueHandling
	Nullable_1_t668593436  ____defaultValueHandling_1;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.JsonPropertyAttribute::_referenceLoopHandling
	Nullable_1_t3051693050  ____referenceLoopHandling_2;
	// System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling> Newtonsoft.Json.JsonPropertyAttribute::_objectCreationHandling
	Nullable_1_t588389317  ____objectCreationHandling_3;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.JsonPropertyAttribute::_typeNameHandling
	Nullable_1_t1159582132  ____typeNameHandling_4;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonPropertyAttribute::_isReference
	Nullable_1_t1819850047  ____isReference_5;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonPropertyAttribute::_order
	Nullable_1_t378540539  ____order_6;
	// System.Nullable`1<Newtonsoft.Json.Required> Newtonsoft.Json.JsonPropertyAttribute::_required
	Nullable_1_t3313944452  ____required_7;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonPropertyAttribute::_itemIsReference
	Nullable_1_t1819850047  ____itemIsReference_8;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.JsonPropertyAttribute::_itemReferenceLoopHandling
	Nullable_1_t3051693050  ____itemReferenceLoopHandling_9;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.JsonPropertyAttribute::_itemTypeNameHandling
	Nullable_1_t1159582132  ____itemTypeNameHandling_10;
	// System.Type Newtonsoft.Json.JsonPropertyAttribute::<ItemConverterType>k__BackingField
	Type_t * ___U3CItemConverterTypeU3Ek__BackingField_11;
	// System.Object[] Newtonsoft.Json.JsonPropertyAttribute::<ItemConverterParameters>k__BackingField
	ObjectU5BU5D_t2843939325* ___U3CItemConverterParametersU3Ek__BackingField_12;
	// System.String Newtonsoft.Json.JsonPropertyAttribute::<PropertyName>k__BackingField
	String_t* ___U3CPropertyNameU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of__nullValueHandling_0() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t2223277056, ____nullValueHandling_0)); }
	inline Nullable_1_t829731231  get__nullValueHandling_0() const { return ____nullValueHandling_0; }
	inline Nullable_1_t829731231 * get_address_of__nullValueHandling_0() { return &____nullValueHandling_0; }
	inline void set__nullValueHandling_0(Nullable_1_t829731231  value)
	{
		____nullValueHandling_0 = value;
	}

	inline static int32_t get_offset_of__defaultValueHandling_1() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t2223277056, ____defaultValueHandling_1)); }
	inline Nullable_1_t668593436  get__defaultValueHandling_1() const { return ____defaultValueHandling_1; }
	inline Nullable_1_t668593436 * get_address_of__defaultValueHandling_1() { return &____defaultValueHandling_1; }
	inline void set__defaultValueHandling_1(Nullable_1_t668593436  value)
	{
		____defaultValueHandling_1 = value;
	}

	inline static int32_t get_offset_of__referenceLoopHandling_2() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t2223277056, ____referenceLoopHandling_2)); }
	inline Nullable_1_t3051693050  get__referenceLoopHandling_2() const { return ____referenceLoopHandling_2; }
	inline Nullable_1_t3051693050 * get_address_of__referenceLoopHandling_2() { return &____referenceLoopHandling_2; }
	inline void set__referenceLoopHandling_2(Nullable_1_t3051693050  value)
	{
		____referenceLoopHandling_2 = value;
	}

	inline static int32_t get_offset_of__objectCreationHandling_3() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t2223277056, ____objectCreationHandling_3)); }
	inline Nullable_1_t588389317  get__objectCreationHandling_3() const { return ____objectCreationHandling_3; }
	inline Nullable_1_t588389317 * get_address_of__objectCreationHandling_3() { return &____objectCreationHandling_3; }
	inline void set__objectCreationHandling_3(Nullable_1_t588389317  value)
	{
		____objectCreationHandling_3 = value;
	}

	inline static int32_t get_offset_of__typeNameHandling_4() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t2223277056, ____typeNameHandling_4)); }
	inline Nullable_1_t1159582132  get__typeNameHandling_4() const { return ____typeNameHandling_4; }
	inline Nullable_1_t1159582132 * get_address_of__typeNameHandling_4() { return &____typeNameHandling_4; }
	inline void set__typeNameHandling_4(Nullable_1_t1159582132  value)
	{
		____typeNameHandling_4 = value;
	}

	inline static int32_t get_offset_of__isReference_5() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t2223277056, ____isReference_5)); }
	inline Nullable_1_t1819850047  get__isReference_5() const { return ____isReference_5; }
	inline Nullable_1_t1819850047 * get_address_of__isReference_5() { return &____isReference_5; }
	inline void set__isReference_5(Nullable_1_t1819850047  value)
	{
		____isReference_5 = value;
	}

	inline static int32_t get_offset_of__order_6() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t2223277056, ____order_6)); }
	inline Nullable_1_t378540539  get__order_6() const { return ____order_6; }
	inline Nullable_1_t378540539 * get_address_of__order_6() { return &____order_6; }
	inline void set__order_6(Nullable_1_t378540539  value)
	{
		____order_6 = value;
	}

	inline static int32_t get_offset_of__required_7() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t2223277056, ____required_7)); }
	inline Nullable_1_t3313944452  get__required_7() const { return ____required_7; }
	inline Nullable_1_t3313944452 * get_address_of__required_7() { return &____required_7; }
	inline void set__required_7(Nullable_1_t3313944452  value)
	{
		____required_7 = value;
	}

	inline static int32_t get_offset_of__itemIsReference_8() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t2223277056, ____itemIsReference_8)); }
	inline Nullable_1_t1819850047  get__itemIsReference_8() const { return ____itemIsReference_8; }
	inline Nullable_1_t1819850047 * get_address_of__itemIsReference_8() { return &____itemIsReference_8; }
	inline void set__itemIsReference_8(Nullable_1_t1819850047  value)
	{
		____itemIsReference_8 = value;
	}

	inline static int32_t get_offset_of__itemReferenceLoopHandling_9() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t2223277056, ____itemReferenceLoopHandling_9)); }
	inline Nullable_1_t3051693050  get__itemReferenceLoopHandling_9() const { return ____itemReferenceLoopHandling_9; }
	inline Nullable_1_t3051693050 * get_address_of__itemReferenceLoopHandling_9() { return &____itemReferenceLoopHandling_9; }
	inline void set__itemReferenceLoopHandling_9(Nullable_1_t3051693050  value)
	{
		____itemReferenceLoopHandling_9 = value;
	}

	inline static int32_t get_offset_of__itemTypeNameHandling_10() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t2223277056, ____itemTypeNameHandling_10)); }
	inline Nullable_1_t1159582132  get__itemTypeNameHandling_10() const { return ____itemTypeNameHandling_10; }
	inline Nullable_1_t1159582132 * get_address_of__itemTypeNameHandling_10() { return &____itemTypeNameHandling_10; }
	inline void set__itemTypeNameHandling_10(Nullable_1_t1159582132  value)
	{
		____itemTypeNameHandling_10 = value;
	}

	inline static int32_t get_offset_of_U3CItemConverterTypeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t2223277056, ___U3CItemConverterTypeU3Ek__BackingField_11)); }
	inline Type_t * get_U3CItemConverterTypeU3Ek__BackingField_11() const { return ___U3CItemConverterTypeU3Ek__BackingField_11; }
	inline Type_t ** get_address_of_U3CItemConverterTypeU3Ek__BackingField_11() { return &___U3CItemConverterTypeU3Ek__BackingField_11; }
	inline void set_U3CItemConverterTypeU3Ek__BackingField_11(Type_t * value)
	{
		___U3CItemConverterTypeU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemConverterTypeU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CItemConverterParametersU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t2223277056, ___U3CItemConverterParametersU3Ek__BackingField_12)); }
	inline ObjectU5BU5D_t2843939325* get_U3CItemConverterParametersU3Ek__BackingField_12() const { return ___U3CItemConverterParametersU3Ek__BackingField_12; }
	inline ObjectU5BU5D_t2843939325** get_address_of_U3CItemConverterParametersU3Ek__BackingField_12() { return &___U3CItemConverterParametersU3Ek__BackingField_12; }
	inline void set_U3CItemConverterParametersU3Ek__BackingField_12(ObjectU5BU5D_t2843939325* value)
	{
		___U3CItemConverterParametersU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemConverterParametersU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CPropertyNameU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t2223277056, ___U3CPropertyNameU3Ek__BackingField_13)); }
	inline String_t* get_U3CPropertyNameU3Ek__BackingField_13() const { return ___U3CPropertyNameU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CPropertyNameU3Ek__BackingField_13() { return &___U3CPropertyNameU3Ek__BackingField_13; }
	inline void set_U3CPropertyNameU3Ek__BackingField_13(String_t* value)
	{
		___U3CPropertyNameU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPropertyNameU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPROPERTYATTRIBUTE_T2223277056_H
#ifndef PROPERTYCHANGINGEVENTHANDLER_T2830353497_H
#define PROPERTYCHANGINGEVENTHANDLER_T2830353497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.PropertyChangingEventHandler
struct  PropertyChangingEventHandler_t2830353497  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYCHANGINGEVENTHANDLER_T2830353497_H
#ifndef JSONWRITER_T1467272295_H
#define JSONWRITER_T1467272295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter
struct  JsonWriter_t1467272295  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition> Newtonsoft.Json.JsonWriter::_stack
	List_1_t4000102456 * ____stack_2;
	// Newtonsoft.Json.JsonPosition Newtonsoft.Json.JsonWriter::_currentPosition
	JsonPosition_t2528027714  ____currentPosition_3;
	// Newtonsoft.Json.JsonWriter/State Newtonsoft.Json.JsonWriter::_currentState
	int32_t ____currentState_4;
	// Newtonsoft.Json.Formatting Newtonsoft.Json.JsonWriter::_formatting
	int32_t ____formatting_5;
	// System.Boolean Newtonsoft.Json.JsonWriter::<CloseOutput>k__BackingField
	bool ___U3CCloseOutputU3Ek__BackingField_6;
	// Newtonsoft.Json.DateFormatHandling Newtonsoft.Json.JsonWriter::_dateFormatHandling
	int32_t ____dateFormatHandling_7;
	// Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonWriter::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_8;
	// Newtonsoft.Json.StringEscapeHandling Newtonsoft.Json.JsonWriter::_stringEscapeHandling
	int32_t ____stringEscapeHandling_9;
	// Newtonsoft.Json.FloatFormatHandling Newtonsoft.Json.JsonWriter::_floatFormatHandling
	int32_t ____floatFormatHandling_10;
	// System.String Newtonsoft.Json.JsonWriter::_dateFormatString
	String_t* ____dateFormatString_11;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonWriter::_culture
	CultureInfo_t4157843068 * ____culture_12;

public:
	inline static int32_t get_offset_of__stack_2() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____stack_2)); }
	inline List_1_t4000102456 * get__stack_2() const { return ____stack_2; }
	inline List_1_t4000102456 ** get_address_of__stack_2() { return &____stack_2; }
	inline void set__stack_2(List_1_t4000102456 * value)
	{
		____stack_2 = value;
		Il2CppCodeGenWriteBarrier((&____stack_2), value);
	}

	inline static int32_t get_offset_of__currentPosition_3() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____currentPosition_3)); }
	inline JsonPosition_t2528027714  get__currentPosition_3() const { return ____currentPosition_3; }
	inline JsonPosition_t2528027714 * get_address_of__currentPosition_3() { return &____currentPosition_3; }
	inline void set__currentPosition_3(JsonPosition_t2528027714  value)
	{
		____currentPosition_3 = value;
	}

	inline static int32_t get_offset_of__currentState_4() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____currentState_4)); }
	inline int32_t get__currentState_4() const { return ____currentState_4; }
	inline int32_t* get_address_of__currentState_4() { return &____currentState_4; }
	inline void set__currentState_4(int32_t value)
	{
		____currentState_4 = value;
	}

	inline static int32_t get_offset_of__formatting_5() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____formatting_5)); }
	inline int32_t get__formatting_5() const { return ____formatting_5; }
	inline int32_t* get_address_of__formatting_5() { return &____formatting_5; }
	inline void set__formatting_5(int32_t value)
	{
		____formatting_5 = value;
	}

	inline static int32_t get_offset_of_U3CCloseOutputU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ___U3CCloseOutputU3Ek__BackingField_6)); }
	inline bool get_U3CCloseOutputU3Ek__BackingField_6() const { return ___U3CCloseOutputU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CCloseOutputU3Ek__BackingField_6() { return &___U3CCloseOutputU3Ek__BackingField_6; }
	inline void set_U3CCloseOutputU3Ek__BackingField_6(bool value)
	{
		___U3CCloseOutputU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_7() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____dateFormatHandling_7)); }
	inline int32_t get__dateFormatHandling_7() const { return ____dateFormatHandling_7; }
	inline int32_t* get_address_of__dateFormatHandling_7() { return &____dateFormatHandling_7; }
	inline void set__dateFormatHandling_7(int32_t value)
	{
		____dateFormatHandling_7 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_8() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____dateTimeZoneHandling_8)); }
	inline int32_t get__dateTimeZoneHandling_8() const { return ____dateTimeZoneHandling_8; }
	inline int32_t* get_address_of__dateTimeZoneHandling_8() { return &____dateTimeZoneHandling_8; }
	inline void set__dateTimeZoneHandling_8(int32_t value)
	{
		____dateTimeZoneHandling_8 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_9() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____stringEscapeHandling_9)); }
	inline int32_t get__stringEscapeHandling_9() const { return ____stringEscapeHandling_9; }
	inline int32_t* get_address_of__stringEscapeHandling_9() { return &____stringEscapeHandling_9; }
	inline void set__stringEscapeHandling_9(int32_t value)
	{
		____stringEscapeHandling_9 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_10() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____floatFormatHandling_10)); }
	inline int32_t get__floatFormatHandling_10() const { return ____floatFormatHandling_10; }
	inline int32_t* get_address_of__floatFormatHandling_10() { return &____floatFormatHandling_10; }
	inline void set__floatFormatHandling_10(int32_t value)
	{
		____floatFormatHandling_10 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_11() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____dateFormatString_11)); }
	inline String_t* get__dateFormatString_11() const { return ____dateFormatString_11; }
	inline String_t** get_address_of__dateFormatString_11() { return &____dateFormatString_11; }
	inline void set__dateFormatString_11(String_t* value)
	{
		____dateFormatString_11 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_11), value);
	}

	inline static int32_t get_offset_of__culture_12() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____culture_12)); }
	inline CultureInfo_t4157843068 * get__culture_12() const { return ____culture_12; }
	inline CultureInfo_t4157843068 ** get_address_of__culture_12() { return &____culture_12; }
	inline void set__culture_12(CultureInfo_t4157843068 * value)
	{
		____culture_12 = value;
		Il2CppCodeGenWriteBarrier((&____culture_12), value);
	}
};

struct JsonWriter_t1467272295_StaticFields
{
public:
	// Newtonsoft.Json.JsonWriter/State[][] Newtonsoft.Json.JsonWriter::StateArray
	StateU5BU5DU5BU5D_t3190318925* ___StateArray_0;
	// Newtonsoft.Json.JsonWriter/State[][] Newtonsoft.Json.JsonWriter::StateArrayTempate
	StateU5BU5DU5BU5D_t3190318925* ___StateArrayTempate_1;

public:
	inline static int32_t get_offset_of_StateArray_0() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295_StaticFields, ___StateArray_0)); }
	inline StateU5BU5DU5BU5D_t3190318925* get_StateArray_0() const { return ___StateArray_0; }
	inline StateU5BU5DU5BU5D_t3190318925** get_address_of_StateArray_0() { return &___StateArray_0; }
	inline void set_StateArray_0(StateU5BU5DU5BU5D_t3190318925* value)
	{
		___StateArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___StateArray_0), value);
	}

	inline static int32_t get_offset_of_StateArrayTempate_1() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295_StaticFields, ___StateArrayTempate_1)); }
	inline StateU5BU5DU5BU5D_t3190318925* get_StateArrayTempate_1() const { return ___StateArrayTempate_1; }
	inline StateU5BU5DU5BU5D_t3190318925** get_address_of_StateArrayTempate_1() { return &___StateArrayTempate_1; }
	inline void set_StateArrayTempate_1(StateU5BU5DU5BU5D_t3190318925* value)
	{
		___StateArrayTempate_1 = value;
		Il2CppCodeGenWriteBarrier((&___StateArrayTempate_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONWRITER_T1467272295_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef JSONDICTIONARYATTRIBUTE_T3890173510_H
#define JSONDICTIONARYATTRIBUTE_T3890173510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonDictionaryAttribute
struct  JsonDictionaryAttribute_t3890173510  : public JsonContainerAttribute_t3874748064
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONDICTIONARYATTRIBUTE_T3890173510_H
#ifndef JSONTEXTREADER_T262560521_H
#define JSONTEXTREADER_T262560521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonTextReader
struct  JsonTextReader_t262560521  : public JsonReader_t2369136700
{
public:
	// System.IO.TextReader Newtonsoft.Json.JsonTextReader::_reader
	TextReader_t283511965 * ____reader_15;
	// System.Char[] Newtonsoft.Json.JsonTextReader::_chars
	CharU5BU5D_t3528271667* ____chars_16;
	// System.Int32 Newtonsoft.Json.JsonTextReader::_charsUsed
	int32_t ____charsUsed_17;
	// System.Int32 Newtonsoft.Json.JsonTextReader::_charPos
	int32_t ____charPos_18;
	// System.Int32 Newtonsoft.Json.JsonTextReader::_lineStartPos
	int32_t ____lineStartPos_19;
	// System.Int32 Newtonsoft.Json.JsonTextReader::_lineNumber
	int32_t ____lineNumber_20;
	// System.Boolean Newtonsoft.Json.JsonTextReader::_isEndOfFile
	bool ____isEndOfFile_21;
	// Newtonsoft.Json.Utilities.StringBuffer Newtonsoft.Json.JsonTextReader::_stringBuffer
	StringBuffer_t2235727887  ____stringBuffer_22;
	// Newtonsoft.Json.Utilities.StringReference Newtonsoft.Json.JsonTextReader::_stringReference
	StringReference_t2912309144  ____stringReference_23;
	// Newtonsoft.Json.IArrayPool`1<System.Char> Newtonsoft.Json.JsonTextReader::_arrayPool
	RuntimeObject* ____arrayPool_24;
	// Newtonsoft.Json.Utilities.PropertyNameTable Newtonsoft.Json.JsonTextReader::NameTable
	PropertyNameTable_t4130830590 * ___NameTable_25;

public:
	inline static int32_t get_offset_of__reader_15() { return static_cast<int32_t>(offsetof(JsonTextReader_t262560521, ____reader_15)); }
	inline TextReader_t283511965 * get__reader_15() const { return ____reader_15; }
	inline TextReader_t283511965 ** get_address_of__reader_15() { return &____reader_15; }
	inline void set__reader_15(TextReader_t283511965 * value)
	{
		____reader_15 = value;
		Il2CppCodeGenWriteBarrier((&____reader_15), value);
	}

	inline static int32_t get_offset_of__chars_16() { return static_cast<int32_t>(offsetof(JsonTextReader_t262560521, ____chars_16)); }
	inline CharU5BU5D_t3528271667* get__chars_16() const { return ____chars_16; }
	inline CharU5BU5D_t3528271667** get_address_of__chars_16() { return &____chars_16; }
	inline void set__chars_16(CharU5BU5D_t3528271667* value)
	{
		____chars_16 = value;
		Il2CppCodeGenWriteBarrier((&____chars_16), value);
	}

	inline static int32_t get_offset_of__charsUsed_17() { return static_cast<int32_t>(offsetof(JsonTextReader_t262560521, ____charsUsed_17)); }
	inline int32_t get__charsUsed_17() const { return ____charsUsed_17; }
	inline int32_t* get_address_of__charsUsed_17() { return &____charsUsed_17; }
	inline void set__charsUsed_17(int32_t value)
	{
		____charsUsed_17 = value;
	}

	inline static int32_t get_offset_of__charPos_18() { return static_cast<int32_t>(offsetof(JsonTextReader_t262560521, ____charPos_18)); }
	inline int32_t get__charPos_18() const { return ____charPos_18; }
	inline int32_t* get_address_of__charPos_18() { return &____charPos_18; }
	inline void set__charPos_18(int32_t value)
	{
		____charPos_18 = value;
	}

	inline static int32_t get_offset_of__lineStartPos_19() { return static_cast<int32_t>(offsetof(JsonTextReader_t262560521, ____lineStartPos_19)); }
	inline int32_t get__lineStartPos_19() const { return ____lineStartPos_19; }
	inline int32_t* get_address_of__lineStartPos_19() { return &____lineStartPos_19; }
	inline void set__lineStartPos_19(int32_t value)
	{
		____lineStartPos_19 = value;
	}

	inline static int32_t get_offset_of__lineNumber_20() { return static_cast<int32_t>(offsetof(JsonTextReader_t262560521, ____lineNumber_20)); }
	inline int32_t get__lineNumber_20() const { return ____lineNumber_20; }
	inline int32_t* get_address_of__lineNumber_20() { return &____lineNumber_20; }
	inline void set__lineNumber_20(int32_t value)
	{
		____lineNumber_20 = value;
	}

	inline static int32_t get_offset_of__isEndOfFile_21() { return static_cast<int32_t>(offsetof(JsonTextReader_t262560521, ____isEndOfFile_21)); }
	inline bool get__isEndOfFile_21() const { return ____isEndOfFile_21; }
	inline bool* get_address_of__isEndOfFile_21() { return &____isEndOfFile_21; }
	inline void set__isEndOfFile_21(bool value)
	{
		____isEndOfFile_21 = value;
	}

	inline static int32_t get_offset_of__stringBuffer_22() { return static_cast<int32_t>(offsetof(JsonTextReader_t262560521, ____stringBuffer_22)); }
	inline StringBuffer_t2235727887  get__stringBuffer_22() const { return ____stringBuffer_22; }
	inline StringBuffer_t2235727887 * get_address_of__stringBuffer_22() { return &____stringBuffer_22; }
	inline void set__stringBuffer_22(StringBuffer_t2235727887  value)
	{
		____stringBuffer_22 = value;
	}

	inline static int32_t get_offset_of__stringReference_23() { return static_cast<int32_t>(offsetof(JsonTextReader_t262560521, ____stringReference_23)); }
	inline StringReference_t2912309144  get__stringReference_23() const { return ____stringReference_23; }
	inline StringReference_t2912309144 * get_address_of__stringReference_23() { return &____stringReference_23; }
	inline void set__stringReference_23(StringReference_t2912309144  value)
	{
		____stringReference_23 = value;
	}

	inline static int32_t get_offset_of__arrayPool_24() { return static_cast<int32_t>(offsetof(JsonTextReader_t262560521, ____arrayPool_24)); }
	inline RuntimeObject* get__arrayPool_24() const { return ____arrayPool_24; }
	inline RuntimeObject** get_address_of__arrayPool_24() { return &____arrayPool_24; }
	inline void set__arrayPool_24(RuntimeObject* value)
	{
		____arrayPool_24 = value;
		Il2CppCodeGenWriteBarrier((&____arrayPool_24), value);
	}

	inline static int32_t get_offset_of_NameTable_25() { return static_cast<int32_t>(offsetof(JsonTextReader_t262560521, ___NameTable_25)); }
	inline PropertyNameTable_t4130830590 * get_NameTable_25() const { return ___NameTable_25; }
	inline PropertyNameTable_t4130830590 ** get_address_of_NameTable_25() { return &___NameTable_25; }
	inline void set_NameTable_25(PropertyNameTable_t4130830590 * value)
	{
		___NameTable_25 = value;
		Il2CppCodeGenWriteBarrier((&___NameTable_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTEXTREADER_T262560521_H
#ifndef JSONTEXTWRITER_T1095349912_H
#define JSONTEXTWRITER_T1095349912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonTextWriter
struct  JsonTextWriter_t1095349912  : public JsonWriter_t1467272295
{
public:
	// System.IO.TextWriter Newtonsoft.Json.JsonTextWriter::_writer
	TextWriter_t3478189236 * ____writer_13;
	// Newtonsoft.Json.Utilities.Base64Encoder Newtonsoft.Json.JsonTextWriter::_base64Encoder
	Base64Encoder_t3724986751 * ____base64Encoder_14;
	// System.Char Newtonsoft.Json.JsonTextWriter::_indentChar
	Il2CppChar ____indentChar_15;
	// System.Int32 Newtonsoft.Json.JsonTextWriter::_indentation
	int32_t ____indentation_16;
	// System.Char Newtonsoft.Json.JsonTextWriter::_quoteChar
	Il2CppChar ____quoteChar_17;
	// System.Boolean Newtonsoft.Json.JsonTextWriter::_quoteName
	bool ____quoteName_18;
	// System.Boolean[] Newtonsoft.Json.JsonTextWriter::_charEscapeFlags
	BooleanU5BU5D_t2897418192* ____charEscapeFlags_19;
	// System.Char[] Newtonsoft.Json.JsonTextWriter::_writeBuffer
	CharU5BU5D_t3528271667* ____writeBuffer_20;
	// Newtonsoft.Json.IArrayPool`1<System.Char> Newtonsoft.Json.JsonTextWriter::_arrayPool
	RuntimeObject* ____arrayPool_21;
	// System.Char[] Newtonsoft.Json.JsonTextWriter::_indentChars
	CharU5BU5D_t3528271667* ____indentChars_22;

public:
	inline static int32_t get_offset_of__writer_13() { return static_cast<int32_t>(offsetof(JsonTextWriter_t1095349912, ____writer_13)); }
	inline TextWriter_t3478189236 * get__writer_13() const { return ____writer_13; }
	inline TextWriter_t3478189236 ** get_address_of__writer_13() { return &____writer_13; }
	inline void set__writer_13(TextWriter_t3478189236 * value)
	{
		____writer_13 = value;
		Il2CppCodeGenWriteBarrier((&____writer_13), value);
	}

	inline static int32_t get_offset_of__base64Encoder_14() { return static_cast<int32_t>(offsetof(JsonTextWriter_t1095349912, ____base64Encoder_14)); }
	inline Base64Encoder_t3724986751 * get__base64Encoder_14() const { return ____base64Encoder_14; }
	inline Base64Encoder_t3724986751 ** get_address_of__base64Encoder_14() { return &____base64Encoder_14; }
	inline void set__base64Encoder_14(Base64Encoder_t3724986751 * value)
	{
		____base64Encoder_14 = value;
		Il2CppCodeGenWriteBarrier((&____base64Encoder_14), value);
	}

	inline static int32_t get_offset_of__indentChar_15() { return static_cast<int32_t>(offsetof(JsonTextWriter_t1095349912, ____indentChar_15)); }
	inline Il2CppChar get__indentChar_15() const { return ____indentChar_15; }
	inline Il2CppChar* get_address_of__indentChar_15() { return &____indentChar_15; }
	inline void set__indentChar_15(Il2CppChar value)
	{
		____indentChar_15 = value;
	}

	inline static int32_t get_offset_of__indentation_16() { return static_cast<int32_t>(offsetof(JsonTextWriter_t1095349912, ____indentation_16)); }
	inline int32_t get__indentation_16() const { return ____indentation_16; }
	inline int32_t* get_address_of__indentation_16() { return &____indentation_16; }
	inline void set__indentation_16(int32_t value)
	{
		____indentation_16 = value;
	}

	inline static int32_t get_offset_of__quoteChar_17() { return static_cast<int32_t>(offsetof(JsonTextWriter_t1095349912, ____quoteChar_17)); }
	inline Il2CppChar get__quoteChar_17() const { return ____quoteChar_17; }
	inline Il2CppChar* get_address_of__quoteChar_17() { return &____quoteChar_17; }
	inline void set__quoteChar_17(Il2CppChar value)
	{
		____quoteChar_17 = value;
	}

	inline static int32_t get_offset_of__quoteName_18() { return static_cast<int32_t>(offsetof(JsonTextWriter_t1095349912, ____quoteName_18)); }
	inline bool get__quoteName_18() const { return ____quoteName_18; }
	inline bool* get_address_of__quoteName_18() { return &____quoteName_18; }
	inline void set__quoteName_18(bool value)
	{
		____quoteName_18 = value;
	}

	inline static int32_t get_offset_of__charEscapeFlags_19() { return static_cast<int32_t>(offsetof(JsonTextWriter_t1095349912, ____charEscapeFlags_19)); }
	inline BooleanU5BU5D_t2897418192* get__charEscapeFlags_19() const { return ____charEscapeFlags_19; }
	inline BooleanU5BU5D_t2897418192** get_address_of__charEscapeFlags_19() { return &____charEscapeFlags_19; }
	inline void set__charEscapeFlags_19(BooleanU5BU5D_t2897418192* value)
	{
		____charEscapeFlags_19 = value;
		Il2CppCodeGenWriteBarrier((&____charEscapeFlags_19), value);
	}

	inline static int32_t get_offset_of__writeBuffer_20() { return static_cast<int32_t>(offsetof(JsonTextWriter_t1095349912, ____writeBuffer_20)); }
	inline CharU5BU5D_t3528271667* get__writeBuffer_20() const { return ____writeBuffer_20; }
	inline CharU5BU5D_t3528271667** get_address_of__writeBuffer_20() { return &____writeBuffer_20; }
	inline void set__writeBuffer_20(CharU5BU5D_t3528271667* value)
	{
		____writeBuffer_20 = value;
		Il2CppCodeGenWriteBarrier((&____writeBuffer_20), value);
	}

	inline static int32_t get_offset_of__arrayPool_21() { return static_cast<int32_t>(offsetof(JsonTextWriter_t1095349912, ____arrayPool_21)); }
	inline RuntimeObject* get__arrayPool_21() const { return ____arrayPool_21; }
	inline RuntimeObject** get_address_of__arrayPool_21() { return &____arrayPool_21; }
	inline void set__arrayPool_21(RuntimeObject* value)
	{
		____arrayPool_21 = value;
		Il2CppCodeGenWriteBarrier((&____arrayPool_21), value);
	}

	inline static int32_t get_offset_of__indentChars_22() { return static_cast<int32_t>(offsetof(JsonTextWriter_t1095349912, ____indentChars_22)); }
	inline CharU5BU5D_t3528271667* get__indentChars_22() const { return ____indentChars_22; }
	inline CharU5BU5D_t3528271667** get_address_of__indentChars_22() { return &____indentChars_22; }
	inline void set__indentChars_22(CharU5BU5D_t3528271667* value)
	{
		____indentChars_22 = value;
		Il2CppCodeGenWriteBarrier((&____indentChars_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTEXTWRITER_T1095349912_H
#ifndef JSONOBJECTATTRIBUTE_T3277381142_H
#define JSONOBJECTATTRIBUTE_T3277381142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonObjectAttribute
struct  JsonObjectAttribute_t3277381142  : public JsonContainerAttribute_t3874748064
{
public:
	// Newtonsoft.Json.MemberSerialization Newtonsoft.Json.JsonObjectAttribute::_memberSerialization
	int32_t ____memberSerialization_6;
	// System.Nullable`1<Newtonsoft.Json.Required> Newtonsoft.Json.JsonObjectAttribute::_itemRequired
	Nullable_1_t3313944452  ____itemRequired_7;

public:
	inline static int32_t get_offset_of__memberSerialization_6() { return static_cast<int32_t>(offsetof(JsonObjectAttribute_t3277381142, ____memberSerialization_6)); }
	inline int32_t get__memberSerialization_6() const { return ____memberSerialization_6; }
	inline int32_t* get_address_of__memberSerialization_6() { return &____memberSerialization_6; }
	inline void set__memberSerialization_6(int32_t value)
	{
		____memberSerialization_6 = value;
	}

	inline static int32_t get_offset_of__itemRequired_7() { return static_cast<int32_t>(offsetof(JsonObjectAttribute_t3277381142, ____itemRequired_7)); }
	inline Nullable_1_t3313944452  get__itemRequired_7() const { return ____itemRequired_7; }
	inline Nullable_1_t3313944452 * get_address_of__itemRequired_7() { return &____itemRequired_7; }
	inline void set__itemRequired_7(Nullable_1_t3313944452  value)
	{
		____itemRequired_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONOBJECTATTRIBUTE_T3277381142_H
#ifndef JSONARRAYATTRIBUTE_T3155801748_H
#define JSONARRAYATTRIBUTE_T3155801748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonArrayAttribute
struct  JsonArrayAttribute_t3155801748  : public JsonContainerAttribute_t3874748064
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONARRAYATTRIBUTE_T3155801748_H
#ifndef JSONSERIALIZERSETTINGS_T2139255314_H
#define JSONSERIALIZERSETTINGS_T2139255314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonSerializerSettings
struct  JsonSerializerSettings_t2139255314  : public RuntimeObject
{
public:
	// System.Nullable`1<Newtonsoft.Json.Formatting> Newtonsoft.Json.JsonSerializerSettings::_formatting
	Nullable_1_t3914607011  ____formatting_2;
	// System.Nullable`1<Newtonsoft.Json.DateFormatHandling> Newtonsoft.Json.JsonSerializerSettings::_dateFormatHandling
	Nullable_1_t3098729937  ____dateFormatHandling_3;
	// System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling> Newtonsoft.Json.JsonSerializerSettings::_dateTimeZoneHandling
	Nullable_1_t430194516  ____dateTimeZoneHandling_4;
	// System.Nullable`1<Newtonsoft.Json.DateParseHandling> Newtonsoft.Json.JsonSerializerSettings::_dateParseHandling
	Nullable_1_t179296662  ____dateParseHandling_5;
	// System.Nullable`1<Newtonsoft.Json.FloatFormatHandling> Newtonsoft.Json.JsonSerializerSettings::_floatFormatHandling
	Nullable_1_t2285469903  ____floatFormatHandling_6;
	// System.Nullable`1<Newtonsoft.Json.FloatParseHandling> Newtonsoft.Json.JsonSerializerSettings::_floatParseHandling
	Nullable_1_t2729527740  ____floatParseHandling_7;
	// System.Nullable`1<Newtonsoft.Json.StringEscapeHandling> Newtonsoft.Json.JsonSerializerSettings::_stringEscapeHandling
	Nullable_1_t1505470351  ____stringEscapeHandling_8;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonSerializerSettings::_culture
	CultureInfo_t4157843068 * ____culture_9;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonSerializerSettings::_checkAdditionalContent
	Nullable_1_t1819850047  ____checkAdditionalContent_10;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonSerializerSettings::_maxDepth
	Nullable_1_t378540539  ____maxDepth_11;
	// System.Boolean Newtonsoft.Json.JsonSerializerSettings::_maxDepthSet
	bool ____maxDepthSet_12;
	// System.String Newtonsoft.Json.JsonSerializerSettings::_dateFormatString
	String_t* ____dateFormatString_13;
	// System.Boolean Newtonsoft.Json.JsonSerializerSettings::_dateFormatStringSet
	bool ____dateFormatStringSet_14;
	// System.Nullable`1<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle> Newtonsoft.Json.JsonSerializerSettings::_typeNameAssemblyFormat
	Nullable_1_t2590601907  ____typeNameAssemblyFormat_15;
	// System.Nullable`1<Newtonsoft.Json.DefaultValueHandling> Newtonsoft.Json.JsonSerializerSettings::_defaultValueHandling
	Nullable_1_t668593436  ____defaultValueHandling_16;
	// System.Nullable`1<Newtonsoft.Json.PreserveReferencesHandling> Newtonsoft.Json.JsonSerializerSettings::_preserveReferencesHandling
	Nullable_1_t2538550894  ____preserveReferencesHandling_17;
	// System.Nullable`1<Newtonsoft.Json.NullValueHandling> Newtonsoft.Json.JsonSerializerSettings::_nullValueHandling
	Nullable_1_t829731231  ____nullValueHandling_18;
	// System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling> Newtonsoft.Json.JsonSerializerSettings::_objectCreationHandling
	Nullable_1_t588389317  ____objectCreationHandling_19;
	// System.Nullable`1<Newtonsoft.Json.MissingMemberHandling> Newtonsoft.Json.JsonSerializerSettings::_missingMemberHandling
	Nullable_1_t1470895410  ____missingMemberHandling_20;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.JsonSerializerSettings::_referenceLoopHandling
	Nullable_1_t3051693050  ____referenceLoopHandling_21;
	// System.Nullable`1<System.Runtime.Serialization.StreamingContext> Newtonsoft.Json.JsonSerializerSettings::_context
	Nullable_1_t1139464023  ____context_22;
	// System.Nullable`1<Newtonsoft.Json.ConstructorHandling> Newtonsoft.Json.JsonSerializerSettings::_constructorHandling
	Nullable_1_t616964910  ____constructorHandling_23;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.JsonSerializerSettings::_typeNameHandling
	Nullable_1_t1159582132  ____typeNameHandling_24;
	// System.Nullable`1<Newtonsoft.Json.MetadataPropertyHandling> Newtonsoft.Json.JsonSerializerSettings::_metadataPropertyHandling
	Nullable_1_t2764731635  ____metadataPropertyHandling_25;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter> Newtonsoft.Json.JsonSerializerSettings::<Converters>k__BackingField
	RuntimeObject* ___U3CConvertersU3Ek__BackingField_26;
	// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.JsonSerializerSettings::<ContractResolver>k__BackingField
	RuntimeObject* ___U3CContractResolverU3Ek__BackingField_27;
	// System.Collections.IEqualityComparer Newtonsoft.Json.JsonSerializerSettings::<EqualityComparer>k__BackingField
	RuntimeObject* ___U3CEqualityComparerU3Ek__BackingField_28;
	// System.Func`1<Newtonsoft.Json.Serialization.IReferenceResolver> Newtonsoft.Json.JsonSerializerSettings::<ReferenceResolverProvider>k__BackingField
	Func_1_t1993807751 * ___U3CReferenceResolverProviderU3Ek__BackingField_29;
	// Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.JsonSerializerSettings::<TraceWriter>k__BackingField
	RuntimeObject* ___U3CTraceWriterU3Ek__BackingField_30;
	// System.Runtime.Serialization.SerializationBinder Newtonsoft.Json.JsonSerializerSettings::<Binder>k__BackingField
	SerializationBinder_t274213469 * ___U3CBinderU3Ek__BackingField_31;
	// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs> Newtonsoft.Json.JsonSerializerSettings::<Error>k__BackingField
	EventHandler_1_t2935836205 * ___U3CErrorU3Ek__BackingField_32;

public:
	inline static int32_t get_offset_of__formatting_2() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ____formatting_2)); }
	inline Nullable_1_t3914607011  get__formatting_2() const { return ____formatting_2; }
	inline Nullable_1_t3914607011 * get_address_of__formatting_2() { return &____formatting_2; }
	inline void set__formatting_2(Nullable_1_t3914607011  value)
	{
		____formatting_2 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_3() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ____dateFormatHandling_3)); }
	inline Nullable_1_t3098729937  get__dateFormatHandling_3() const { return ____dateFormatHandling_3; }
	inline Nullable_1_t3098729937 * get_address_of__dateFormatHandling_3() { return &____dateFormatHandling_3; }
	inline void set__dateFormatHandling_3(Nullable_1_t3098729937  value)
	{
		____dateFormatHandling_3 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_4() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ____dateTimeZoneHandling_4)); }
	inline Nullable_1_t430194516  get__dateTimeZoneHandling_4() const { return ____dateTimeZoneHandling_4; }
	inline Nullable_1_t430194516 * get_address_of__dateTimeZoneHandling_4() { return &____dateTimeZoneHandling_4; }
	inline void set__dateTimeZoneHandling_4(Nullable_1_t430194516  value)
	{
		____dateTimeZoneHandling_4 = value;
	}

	inline static int32_t get_offset_of__dateParseHandling_5() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ____dateParseHandling_5)); }
	inline Nullable_1_t179296662  get__dateParseHandling_5() const { return ____dateParseHandling_5; }
	inline Nullable_1_t179296662 * get_address_of__dateParseHandling_5() { return &____dateParseHandling_5; }
	inline void set__dateParseHandling_5(Nullable_1_t179296662  value)
	{
		____dateParseHandling_5 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_6() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ____floatFormatHandling_6)); }
	inline Nullable_1_t2285469903  get__floatFormatHandling_6() const { return ____floatFormatHandling_6; }
	inline Nullable_1_t2285469903 * get_address_of__floatFormatHandling_6() { return &____floatFormatHandling_6; }
	inline void set__floatFormatHandling_6(Nullable_1_t2285469903  value)
	{
		____floatFormatHandling_6 = value;
	}

	inline static int32_t get_offset_of__floatParseHandling_7() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ____floatParseHandling_7)); }
	inline Nullable_1_t2729527740  get__floatParseHandling_7() const { return ____floatParseHandling_7; }
	inline Nullable_1_t2729527740 * get_address_of__floatParseHandling_7() { return &____floatParseHandling_7; }
	inline void set__floatParseHandling_7(Nullable_1_t2729527740  value)
	{
		____floatParseHandling_7 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_8() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ____stringEscapeHandling_8)); }
	inline Nullable_1_t1505470351  get__stringEscapeHandling_8() const { return ____stringEscapeHandling_8; }
	inline Nullable_1_t1505470351 * get_address_of__stringEscapeHandling_8() { return &____stringEscapeHandling_8; }
	inline void set__stringEscapeHandling_8(Nullable_1_t1505470351  value)
	{
		____stringEscapeHandling_8 = value;
	}

	inline static int32_t get_offset_of__culture_9() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ____culture_9)); }
	inline CultureInfo_t4157843068 * get__culture_9() const { return ____culture_9; }
	inline CultureInfo_t4157843068 ** get_address_of__culture_9() { return &____culture_9; }
	inline void set__culture_9(CultureInfo_t4157843068 * value)
	{
		____culture_9 = value;
		Il2CppCodeGenWriteBarrier((&____culture_9), value);
	}

	inline static int32_t get_offset_of__checkAdditionalContent_10() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ____checkAdditionalContent_10)); }
	inline Nullable_1_t1819850047  get__checkAdditionalContent_10() const { return ____checkAdditionalContent_10; }
	inline Nullable_1_t1819850047 * get_address_of__checkAdditionalContent_10() { return &____checkAdditionalContent_10; }
	inline void set__checkAdditionalContent_10(Nullable_1_t1819850047  value)
	{
		____checkAdditionalContent_10 = value;
	}

	inline static int32_t get_offset_of__maxDepth_11() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ____maxDepth_11)); }
	inline Nullable_1_t378540539  get__maxDepth_11() const { return ____maxDepth_11; }
	inline Nullable_1_t378540539 * get_address_of__maxDepth_11() { return &____maxDepth_11; }
	inline void set__maxDepth_11(Nullable_1_t378540539  value)
	{
		____maxDepth_11 = value;
	}

	inline static int32_t get_offset_of__maxDepthSet_12() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ____maxDepthSet_12)); }
	inline bool get__maxDepthSet_12() const { return ____maxDepthSet_12; }
	inline bool* get_address_of__maxDepthSet_12() { return &____maxDepthSet_12; }
	inline void set__maxDepthSet_12(bool value)
	{
		____maxDepthSet_12 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_13() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ____dateFormatString_13)); }
	inline String_t* get__dateFormatString_13() const { return ____dateFormatString_13; }
	inline String_t** get_address_of__dateFormatString_13() { return &____dateFormatString_13; }
	inline void set__dateFormatString_13(String_t* value)
	{
		____dateFormatString_13 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_13), value);
	}

	inline static int32_t get_offset_of__dateFormatStringSet_14() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ____dateFormatStringSet_14)); }
	inline bool get__dateFormatStringSet_14() const { return ____dateFormatStringSet_14; }
	inline bool* get_address_of__dateFormatStringSet_14() { return &____dateFormatStringSet_14; }
	inline void set__dateFormatStringSet_14(bool value)
	{
		____dateFormatStringSet_14 = value;
	}

	inline static int32_t get_offset_of__typeNameAssemblyFormat_15() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ____typeNameAssemblyFormat_15)); }
	inline Nullable_1_t2590601907  get__typeNameAssemblyFormat_15() const { return ____typeNameAssemblyFormat_15; }
	inline Nullable_1_t2590601907 * get_address_of__typeNameAssemblyFormat_15() { return &____typeNameAssemblyFormat_15; }
	inline void set__typeNameAssemblyFormat_15(Nullable_1_t2590601907  value)
	{
		____typeNameAssemblyFormat_15 = value;
	}

	inline static int32_t get_offset_of__defaultValueHandling_16() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ____defaultValueHandling_16)); }
	inline Nullable_1_t668593436  get__defaultValueHandling_16() const { return ____defaultValueHandling_16; }
	inline Nullable_1_t668593436 * get_address_of__defaultValueHandling_16() { return &____defaultValueHandling_16; }
	inline void set__defaultValueHandling_16(Nullable_1_t668593436  value)
	{
		____defaultValueHandling_16 = value;
	}

	inline static int32_t get_offset_of__preserveReferencesHandling_17() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ____preserveReferencesHandling_17)); }
	inline Nullable_1_t2538550894  get__preserveReferencesHandling_17() const { return ____preserveReferencesHandling_17; }
	inline Nullable_1_t2538550894 * get_address_of__preserveReferencesHandling_17() { return &____preserveReferencesHandling_17; }
	inline void set__preserveReferencesHandling_17(Nullable_1_t2538550894  value)
	{
		____preserveReferencesHandling_17 = value;
	}

	inline static int32_t get_offset_of__nullValueHandling_18() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ____nullValueHandling_18)); }
	inline Nullable_1_t829731231  get__nullValueHandling_18() const { return ____nullValueHandling_18; }
	inline Nullable_1_t829731231 * get_address_of__nullValueHandling_18() { return &____nullValueHandling_18; }
	inline void set__nullValueHandling_18(Nullable_1_t829731231  value)
	{
		____nullValueHandling_18 = value;
	}

	inline static int32_t get_offset_of__objectCreationHandling_19() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ____objectCreationHandling_19)); }
	inline Nullable_1_t588389317  get__objectCreationHandling_19() const { return ____objectCreationHandling_19; }
	inline Nullable_1_t588389317 * get_address_of__objectCreationHandling_19() { return &____objectCreationHandling_19; }
	inline void set__objectCreationHandling_19(Nullable_1_t588389317  value)
	{
		____objectCreationHandling_19 = value;
	}

	inline static int32_t get_offset_of__missingMemberHandling_20() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ____missingMemberHandling_20)); }
	inline Nullable_1_t1470895410  get__missingMemberHandling_20() const { return ____missingMemberHandling_20; }
	inline Nullable_1_t1470895410 * get_address_of__missingMemberHandling_20() { return &____missingMemberHandling_20; }
	inline void set__missingMemberHandling_20(Nullable_1_t1470895410  value)
	{
		____missingMemberHandling_20 = value;
	}

	inline static int32_t get_offset_of__referenceLoopHandling_21() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ____referenceLoopHandling_21)); }
	inline Nullable_1_t3051693050  get__referenceLoopHandling_21() const { return ____referenceLoopHandling_21; }
	inline Nullable_1_t3051693050 * get_address_of__referenceLoopHandling_21() { return &____referenceLoopHandling_21; }
	inline void set__referenceLoopHandling_21(Nullable_1_t3051693050  value)
	{
		____referenceLoopHandling_21 = value;
	}

	inline static int32_t get_offset_of__context_22() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ____context_22)); }
	inline Nullable_1_t1139464023  get__context_22() const { return ____context_22; }
	inline Nullable_1_t1139464023 * get_address_of__context_22() { return &____context_22; }
	inline void set__context_22(Nullable_1_t1139464023  value)
	{
		____context_22 = value;
	}

	inline static int32_t get_offset_of__constructorHandling_23() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ____constructorHandling_23)); }
	inline Nullable_1_t616964910  get__constructorHandling_23() const { return ____constructorHandling_23; }
	inline Nullable_1_t616964910 * get_address_of__constructorHandling_23() { return &____constructorHandling_23; }
	inline void set__constructorHandling_23(Nullable_1_t616964910  value)
	{
		____constructorHandling_23 = value;
	}

	inline static int32_t get_offset_of__typeNameHandling_24() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ____typeNameHandling_24)); }
	inline Nullable_1_t1159582132  get__typeNameHandling_24() const { return ____typeNameHandling_24; }
	inline Nullable_1_t1159582132 * get_address_of__typeNameHandling_24() { return &____typeNameHandling_24; }
	inline void set__typeNameHandling_24(Nullable_1_t1159582132  value)
	{
		____typeNameHandling_24 = value;
	}

	inline static int32_t get_offset_of__metadataPropertyHandling_25() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ____metadataPropertyHandling_25)); }
	inline Nullable_1_t2764731635  get__metadataPropertyHandling_25() const { return ____metadataPropertyHandling_25; }
	inline Nullable_1_t2764731635 * get_address_of__metadataPropertyHandling_25() { return &____metadataPropertyHandling_25; }
	inline void set__metadataPropertyHandling_25(Nullable_1_t2764731635  value)
	{
		____metadataPropertyHandling_25 = value;
	}

	inline static int32_t get_offset_of_U3CConvertersU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ___U3CConvertersU3Ek__BackingField_26)); }
	inline RuntimeObject* get_U3CConvertersU3Ek__BackingField_26() const { return ___U3CConvertersU3Ek__BackingField_26; }
	inline RuntimeObject** get_address_of_U3CConvertersU3Ek__BackingField_26() { return &___U3CConvertersU3Ek__BackingField_26; }
	inline void set_U3CConvertersU3Ek__BackingField_26(RuntimeObject* value)
	{
		___U3CConvertersU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConvertersU3Ek__BackingField_26), value);
	}

	inline static int32_t get_offset_of_U3CContractResolverU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ___U3CContractResolverU3Ek__BackingField_27)); }
	inline RuntimeObject* get_U3CContractResolverU3Ek__BackingField_27() const { return ___U3CContractResolverU3Ek__BackingField_27; }
	inline RuntimeObject** get_address_of_U3CContractResolverU3Ek__BackingField_27() { return &___U3CContractResolverU3Ek__BackingField_27; }
	inline void set_U3CContractResolverU3Ek__BackingField_27(RuntimeObject* value)
	{
		___U3CContractResolverU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContractResolverU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CEqualityComparerU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ___U3CEqualityComparerU3Ek__BackingField_28)); }
	inline RuntimeObject* get_U3CEqualityComparerU3Ek__BackingField_28() const { return ___U3CEqualityComparerU3Ek__BackingField_28; }
	inline RuntimeObject** get_address_of_U3CEqualityComparerU3Ek__BackingField_28() { return &___U3CEqualityComparerU3Ek__BackingField_28; }
	inline void set_U3CEqualityComparerU3Ek__BackingField_28(RuntimeObject* value)
	{
		___U3CEqualityComparerU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEqualityComparerU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_U3CReferenceResolverProviderU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ___U3CReferenceResolverProviderU3Ek__BackingField_29)); }
	inline Func_1_t1993807751 * get_U3CReferenceResolverProviderU3Ek__BackingField_29() const { return ___U3CReferenceResolverProviderU3Ek__BackingField_29; }
	inline Func_1_t1993807751 ** get_address_of_U3CReferenceResolverProviderU3Ek__BackingField_29() { return &___U3CReferenceResolverProviderU3Ek__BackingField_29; }
	inline void set_U3CReferenceResolverProviderU3Ek__BackingField_29(Func_1_t1993807751 * value)
	{
		___U3CReferenceResolverProviderU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CReferenceResolverProviderU3Ek__BackingField_29), value);
	}

	inline static int32_t get_offset_of_U3CTraceWriterU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ___U3CTraceWriterU3Ek__BackingField_30)); }
	inline RuntimeObject* get_U3CTraceWriterU3Ek__BackingField_30() const { return ___U3CTraceWriterU3Ek__BackingField_30; }
	inline RuntimeObject** get_address_of_U3CTraceWriterU3Ek__BackingField_30() { return &___U3CTraceWriterU3Ek__BackingField_30; }
	inline void set_U3CTraceWriterU3Ek__BackingField_30(RuntimeObject* value)
	{
		___U3CTraceWriterU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTraceWriterU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of_U3CBinderU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ___U3CBinderU3Ek__BackingField_31)); }
	inline SerializationBinder_t274213469 * get_U3CBinderU3Ek__BackingField_31() const { return ___U3CBinderU3Ek__BackingField_31; }
	inline SerializationBinder_t274213469 ** get_address_of_U3CBinderU3Ek__BackingField_31() { return &___U3CBinderU3Ek__BackingField_31; }
	inline void set_U3CBinderU3Ek__BackingField_31(SerializationBinder_t274213469 * value)
	{
		___U3CBinderU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBinderU3Ek__BackingField_31), value);
	}

	inline static int32_t get_offset_of_U3CErrorU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314, ___U3CErrorU3Ek__BackingField_32)); }
	inline EventHandler_1_t2935836205 * get_U3CErrorU3Ek__BackingField_32() const { return ___U3CErrorU3Ek__BackingField_32; }
	inline EventHandler_1_t2935836205 ** get_address_of_U3CErrorU3Ek__BackingField_32() { return &___U3CErrorU3Ek__BackingField_32; }
	inline void set_U3CErrorU3Ek__BackingField_32(EventHandler_1_t2935836205 * value)
	{
		___U3CErrorU3Ek__BackingField_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorU3Ek__BackingField_32), value);
	}
};

struct JsonSerializerSettings_t2139255314_StaticFields
{
public:
	// System.Runtime.Serialization.StreamingContext Newtonsoft.Json.JsonSerializerSettings::DefaultContext
	StreamingContext_t3711869237  ___DefaultContext_0;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonSerializerSettings::DefaultCulture
	CultureInfo_t4157843068 * ___DefaultCulture_1;

public:
	inline static int32_t get_offset_of_DefaultContext_0() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314_StaticFields, ___DefaultContext_0)); }
	inline StreamingContext_t3711869237  get_DefaultContext_0() const { return ___DefaultContext_0; }
	inline StreamingContext_t3711869237 * get_address_of_DefaultContext_0() { return &___DefaultContext_0; }
	inline void set_DefaultContext_0(StreamingContext_t3711869237  value)
	{
		___DefaultContext_0 = value;
	}

	inline static int32_t get_offset_of_DefaultCulture_1() { return static_cast<int32_t>(offsetof(JsonSerializerSettings_t2139255314_StaticFields, ___DefaultCulture_1)); }
	inline CultureInfo_t4157843068 * get_DefaultCulture_1() const { return ___DefaultCulture_1; }
	inline CultureInfo_t4157843068 ** get_address_of_DefaultCulture_1() { return &___DefaultCulture_1; }
	inline void set_DefaultCulture_1(CultureInfo_t4157843068 * value)
	{
		___DefaultCulture_1 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultCulture_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZERSETTINGS_T2139255314_H
#ifndef DOTWEENVISUALMANAGER_T1560353112_H
#define DOTWEENVISUALMANAGER_T1560353112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenVisualManager
struct  DOTweenVisualManager_t1560353112  : public MonoBehaviour_t3962482529
{
public:
	// DG.Tweening.Core.VisualManagerPreset DG.Tweening.DOTweenVisualManager::preset
	int32_t ___preset_2;
	// DG.Tweening.Core.OnEnableBehaviour DG.Tweening.DOTweenVisualManager::onEnableBehaviour
	int32_t ___onEnableBehaviour_3;
	// DG.Tweening.Core.OnDisableBehaviour DG.Tweening.DOTweenVisualManager::onDisableBehaviour
	int32_t ___onDisableBehaviour_4;
	// System.Boolean DG.Tweening.DOTweenVisualManager::_requiresRestartFromSpawnPoint
	bool ____requiresRestartFromSpawnPoint_5;
	// DG.Tweening.Core.ABSAnimationComponent DG.Tweening.DOTweenVisualManager::_animComponent
	ABSAnimationComponent_t262169234 * ____animComponent_6;

public:
	inline static int32_t get_offset_of_preset_2() { return static_cast<int32_t>(offsetof(DOTweenVisualManager_t1560353112, ___preset_2)); }
	inline int32_t get_preset_2() const { return ___preset_2; }
	inline int32_t* get_address_of_preset_2() { return &___preset_2; }
	inline void set_preset_2(int32_t value)
	{
		___preset_2 = value;
	}

	inline static int32_t get_offset_of_onEnableBehaviour_3() { return static_cast<int32_t>(offsetof(DOTweenVisualManager_t1560353112, ___onEnableBehaviour_3)); }
	inline int32_t get_onEnableBehaviour_3() const { return ___onEnableBehaviour_3; }
	inline int32_t* get_address_of_onEnableBehaviour_3() { return &___onEnableBehaviour_3; }
	inline void set_onEnableBehaviour_3(int32_t value)
	{
		___onEnableBehaviour_3 = value;
	}

	inline static int32_t get_offset_of_onDisableBehaviour_4() { return static_cast<int32_t>(offsetof(DOTweenVisualManager_t1560353112, ___onDisableBehaviour_4)); }
	inline int32_t get_onDisableBehaviour_4() const { return ___onDisableBehaviour_4; }
	inline int32_t* get_address_of_onDisableBehaviour_4() { return &___onDisableBehaviour_4; }
	inline void set_onDisableBehaviour_4(int32_t value)
	{
		___onDisableBehaviour_4 = value;
	}

	inline static int32_t get_offset_of__requiresRestartFromSpawnPoint_5() { return static_cast<int32_t>(offsetof(DOTweenVisualManager_t1560353112, ____requiresRestartFromSpawnPoint_5)); }
	inline bool get__requiresRestartFromSpawnPoint_5() const { return ____requiresRestartFromSpawnPoint_5; }
	inline bool* get_address_of__requiresRestartFromSpawnPoint_5() { return &____requiresRestartFromSpawnPoint_5; }
	inline void set__requiresRestartFromSpawnPoint_5(bool value)
	{
		____requiresRestartFromSpawnPoint_5 = value;
	}

	inline static int32_t get_offset_of__animComponent_6() { return static_cast<int32_t>(offsetof(DOTweenVisualManager_t1560353112, ____animComponent_6)); }
	inline ABSAnimationComponent_t262169234 * get__animComponent_6() const { return ____animComponent_6; }
	inline ABSAnimationComponent_t262169234 ** get_address_of__animComponent_6() { return &____animComponent_6; }
	inline void set__animComponent_6(ABSAnimationComponent_t262169234 * value)
	{
		____animComponent_6 = value;
		Il2CppCodeGenWriteBarrier((&____animComponent_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENVISUALMANAGER_T1560353112_H
#ifndef ABSANIMATIONCOMPONENT_T262169234_H
#define ABSANIMATIONCOMPONENT_T262169234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.ABSAnimationComponent
struct  ABSAnimationComponent_t262169234  : public MonoBehaviour_t3962482529
{
public:
	// DG.Tweening.UpdateType DG.Tweening.Core.ABSAnimationComponent::updateType
	int32_t ___updateType_2;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::isSpeedBased
	bool ___isSpeedBased_3;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnStart
	bool ___hasOnStart_4;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnPlay
	bool ___hasOnPlay_5;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnUpdate
	bool ___hasOnUpdate_6;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnStepComplete
	bool ___hasOnStepComplete_7;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnComplete
	bool ___hasOnComplete_8;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnTweenCreated
	bool ___hasOnTweenCreated_9;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnRewind
	bool ___hasOnRewind_10;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onStart
	UnityEvent_t2581268647 * ___onStart_11;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onPlay
	UnityEvent_t2581268647 * ___onPlay_12;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onUpdate
	UnityEvent_t2581268647 * ___onUpdate_13;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onStepComplete
	UnityEvent_t2581268647 * ___onStepComplete_14;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onComplete
	UnityEvent_t2581268647 * ___onComplete_15;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onTweenCreated
	UnityEvent_t2581268647 * ___onTweenCreated_16;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onRewind
	UnityEvent_t2581268647 * ___onRewind_17;
	// DG.Tweening.Tween DG.Tweening.Core.ABSAnimationComponent::tween
	Tween_t2342918553 * ___tween_18;

public:
	inline static int32_t get_offset_of_updateType_2() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___updateType_2)); }
	inline int32_t get_updateType_2() const { return ___updateType_2; }
	inline int32_t* get_address_of_updateType_2() { return &___updateType_2; }
	inline void set_updateType_2(int32_t value)
	{
		___updateType_2 = value;
	}

	inline static int32_t get_offset_of_isSpeedBased_3() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___isSpeedBased_3)); }
	inline bool get_isSpeedBased_3() const { return ___isSpeedBased_3; }
	inline bool* get_address_of_isSpeedBased_3() { return &___isSpeedBased_3; }
	inline void set_isSpeedBased_3(bool value)
	{
		___isSpeedBased_3 = value;
	}

	inline static int32_t get_offset_of_hasOnStart_4() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnStart_4)); }
	inline bool get_hasOnStart_4() const { return ___hasOnStart_4; }
	inline bool* get_address_of_hasOnStart_4() { return &___hasOnStart_4; }
	inline void set_hasOnStart_4(bool value)
	{
		___hasOnStart_4 = value;
	}

	inline static int32_t get_offset_of_hasOnPlay_5() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnPlay_5)); }
	inline bool get_hasOnPlay_5() const { return ___hasOnPlay_5; }
	inline bool* get_address_of_hasOnPlay_5() { return &___hasOnPlay_5; }
	inline void set_hasOnPlay_5(bool value)
	{
		___hasOnPlay_5 = value;
	}

	inline static int32_t get_offset_of_hasOnUpdate_6() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnUpdate_6)); }
	inline bool get_hasOnUpdate_6() const { return ___hasOnUpdate_6; }
	inline bool* get_address_of_hasOnUpdate_6() { return &___hasOnUpdate_6; }
	inline void set_hasOnUpdate_6(bool value)
	{
		___hasOnUpdate_6 = value;
	}

	inline static int32_t get_offset_of_hasOnStepComplete_7() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnStepComplete_7)); }
	inline bool get_hasOnStepComplete_7() const { return ___hasOnStepComplete_7; }
	inline bool* get_address_of_hasOnStepComplete_7() { return &___hasOnStepComplete_7; }
	inline void set_hasOnStepComplete_7(bool value)
	{
		___hasOnStepComplete_7 = value;
	}

	inline static int32_t get_offset_of_hasOnComplete_8() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnComplete_8)); }
	inline bool get_hasOnComplete_8() const { return ___hasOnComplete_8; }
	inline bool* get_address_of_hasOnComplete_8() { return &___hasOnComplete_8; }
	inline void set_hasOnComplete_8(bool value)
	{
		___hasOnComplete_8 = value;
	}

	inline static int32_t get_offset_of_hasOnTweenCreated_9() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnTweenCreated_9)); }
	inline bool get_hasOnTweenCreated_9() const { return ___hasOnTweenCreated_9; }
	inline bool* get_address_of_hasOnTweenCreated_9() { return &___hasOnTweenCreated_9; }
	inline void set_hasOnTweenCreated_9(bool value)
	{
		___hasOnTweenCreated_9 = value;
	}

	inline static int32_t get_offset_of_hasOnRewind_10() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnRewind_10)); }
	inline bool get_hasOnRewind_10() const { return ___hasOnRewind_10; }
	inline bool* get_address_of_hasOnRewind_10() { return &___hasOnRewind_10; }
	inline void set_hasOnRewind_10(bool value)
	{
		___hasOnRewind_10 = value;
	}

	inline static int32_t get_offset_of_onStart_11() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onStart_11)); }
	inline UnityEvent_t2581268647 * get_onStart_11() const { return ___onStart_11; }
	inline UnityEvent_t2581268647 ** get_address_of_onStart_11() { return &___onStart_11; }
	inline void set_onStart_11(UnityEvent_t2581268647 * value)
	{
		___onStart_11 = value;
		Il2CppCodeGenWriteBarrier((&___onStart_11), value);
	}

	inline static int32_t get_offset_of_onPlay_12() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onPlay_12)); }
	inline UnityEvent_t2581268647 * get_onPlay_12() const { return ___onPlay_12; }
	inline UnityEvent_t2581268647 ** get_address_of_onPlay_12() { return &___onPlay_12; }
	inline void set_onPlay_12(UnityEvent_t2581268647 * value)
	{
		___onPlay_12 = value;
		Il2CppCodeGenWriteBarrier((&___onPlay_12), value);
	}

	inline static int32_t get_offset_of_onUpdate_13() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onUpdate_13)); }
	inline UnityEvent_t2581268647 * get_onUpdate_13() const { return ___onUpdate_13; }
	inline UnityEvent_t2581268647 ** get_address_of_onUpdate_13() { return &___onUpdate_13; }
	inline void set_onUpdate_13(UnityEvent_t2581268647 * value)
	{
		___onUpdate_13 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdate_13), value);
	}

	inline static int32_t get_offset_of_onStepComplete_14() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onStepComplete_14)); }
	inline UnityEvent_t2581268647 * get_onStepComplete_14() const { return ___onStepComplete_14; }
	inline UnityEvent_t2581268647 ** get_address_of_onStepComplete_14() { return &___onStepComplete_14; }
	inline void set_onStepComplete_14(UnityEvent_t2581268647 * value)
	{
		___onStepComplete_14 = value;
		Il2CppCodeGenWriteBarrier((&___onStepComplete_14), value);
	}

	inline static int32_t get_offset_of_onComplete_15() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onComplete_15)); }
	inline UnityEvent_t2581268647 * get_onComplete_15() const { return ___onComplete_15; }
	inline UnityEvent_t2581268647 ** get_address_of_onComplete_15() { return &___onComplete_15; }
	inline void set_onComplete_15(UnityEvent_t2581268647 * value)
	{
		___onComplete_15 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_15), value);
	}

	inline static int32_t get_offset_of_onTweenCreated_16() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onTweenCreated_16)); }
	inline UnityEvent_t2581268647 * get_onTweenCreated_16() const { return ___onTweenCreated_16; }
	inline UnityEvent_t2581268647 ** get_address_of_onTweenCreated_16() { return &___onTweenCreated_16; }
	inline void set_onTweenCreated_16(UnityEvent_t2581268647 * value)
	{
		___onTweenCreated_16 = value;
		Il2CppCodeGenWriteBarrier((&___onTweenCreated_16), value);
	}

	inline static int32_t get_offset_of_onRewind_17() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onRewind_17)); }
	inline UnityEvent_t2581268647 * get_onRewind_17() const { return ___onRewind_17; }
	inline UnityEvent_t2581268647 ** get_address_of_onRewind_17() { return &___onRewind_17; }
	inline void set_onRewind_17(UnityEvent_t2581268647 * value)
	{
		___onRewind_17 = value;
		Il2CppCodeGenWriteBarrier((&___onRewind_17), value);
	}

	inline static int32_t get_offset_of_tween_18() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___tween_18)); }
	inline Tween_t2342918553 * get_tween_18() const { return ___tween_18; }
	inline Tween_t2342918553 ** get_address_of_tween_18() { return &___tween_18; }
	inline void set_tween_18(Tween_t2342918553 * value)
	{
		___tween_18 = value;
		Il2CppCodeGenWriteBarrier((&___tween_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSANIMATIONCOMPONENT_T262169234_H
#ifndef DOTWEENPATH_T3192963685_H
#define DOTWEENPATH_T3192963685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenPath
struct  DOTweenPath_t3192963685  : public ABSAnimationComponent_t262169234
{
public:
	// System.Single DG.Tweening.DOTweenPath::delay
	float ___delay_19;
	// System.Single DG.Tweening.DOTweenPath::duration
	float ___duration_20;
	// DG.Tweening.Ease DG.Tweening.DOTweenPath::easeType
	int32_t ___easeType_21;
	// UnityEngine.AnimationCurve DG.Tweening.DOTweenPath::easeCurve
	AnimationCurve_t3046754366 * ___easeCurve_22;
	// System.Int32 DG.Tweening.DOTweenPath::loops
	int32_t ___loops_23;
	// System.String DG.Tweening.DOTweenPath::id
	String_t* ___id_24;
	// DG.Tweening.LoopType DG.Tweening.DOTweenPath::loopType
	int32_t ___loopType_25;
	// DG.Tweening.Plugins.Options.OrientType DG.Tweening.DOTweenPath::orientType
	int32_t ___orientType_26;
	// UnityEngine.Transform DG.Tweening.DOTweenPath::lookAtTransform
	Transform_t3600365921 * ___lookAtTransform_27;
	// UnityEngine.Vector3 DG.Tweening.DOTweenPath::lookAtPosition
	Vector3_t3722313464  ___lookAtPosition_28;
	// System.Single DG.Tweening.DOTweenPath::lookAhead
	float ___lookAhead_29;
	// System.Boolean DG.Tweening.DOTweenPath::autoPlay
	bool ___autoPlay_30;
	// System.Boolean DG.Tweening.DOTweenPath::autoKill
	bool ___autoKill_31;
	// System.Boolean DG.Tweening.DOTweenPath::relative
	bool ___relative_32;
	// System.Boolean DG.Tweening.DOTweenPath::isLocal
	bool ___isLocal_33;
	// System.Boolean DG.Tweening.DOTweenPath::isClosedPath
	bool ___isClosedPath_34;
	// System.Int32 DG.Tweening.DOTweenPath::pathResolution
	int32_t ___pathResolution_35;
	// DG.Tweening.PathMode DG.Tweening.DOTweenPath::pathMode
	int32_t ___pathMode_36;
	// DG.Tweening.AxisConstraint DG.Tweening.DOTweenPath::lockRotation
	int32_t ___lockRotation_37;
	// System.Boolean DG.Tweening.DOTweenPath::assignForwardAndUp
	bool ___assignForwardAndUp_38;
	// UnityEngine.Vector3 DG.Tweening.DOTweenPath::forwardDirection
	Vector3_t3722313464  ___forwardDirection_39;
	// UnityEngine.Vector3 DG.Tweening.DOTweenPath::upDirection
	Vector3_t3722313464  ___upDirection_40;
	// System.Boolean DG.Tweening.DOTweenPath::tweenRigidbody
	bool ___tweenRigidbody_41;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> DG.Tweening.DOTweenPath::wps
	List_1_t899420910 * ___wps_42;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> DG.Tweening.DOTweenPath::fullWps
	List_1_t899420910 * ___fullWps_43;
	// DG.Tweening.Plugins.Core.PathCore.Path DG.Tweening.DOTweenPath::path
	Path_t3614338981 * ___path_44;
	// DG.Tweening.DOTweenInspectorMode DG.Tweening.DOTweenPath::inspectorMode
	int32_t ___inspectorMode_45;
	// DG.Tweening.PathType DG.Tweening.DOTweenPath::pathType
	int32_t ___pathType_46;
	// DG.Tweening.HandlesType DG.Tweening.DOTweenPath::handlesType
	int32_t ___handlesType_47;
	// System.Boolean DG.Tweening.DOTweenPath::livePreview
	bool ___livePreview_48;
	// DG.Tweening.HandlesDrawMode DG.Tweening.DOTweenPath::handlesDrawMode
	int32_t ___handlesDrawMode_49;
	// System.Single DG.Tweening.DOTweenPath::perspectiveHandleSize
	float ___perspectiveHandleSize_50;
	// System.Boolean DG.Tweening.DOTweenPath::showIndexes
	bool ___showIndexes_51;
	// System.Boolean DG.Tweening.DOTweenPath::showWpLength
	bool ___showWpLength_52;
	// UnityEngine.Color DG.Tweening.DOTweenPath::pathColor
	Color_t2555686324  ___pathColor_53;
	// UnityEngine.Vector3 DG.Tweening.DOTweenPath::lastSrcPosition
	Vector3_t3722313464  ___lastSrcPosition_54;
	// System.Boolean DG.Tweening.DOTweenPath::wpsDropdown
	bool ___wpsDropdown_55;
	// System.Single DG.Tweening.DOTweenPath::dropToFloorOffset
	float ___dropToFloorOffset_56;

public:
	inline static int32_t get_offset_of_delay_19() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___delay_19)); }
	inline float get_delay_19() const { return ___delay_19; }
	inline float* get_address_of_delay_19() { return &___delay_19; }
	inline void set_delay_19(float value)
	{
		___delay_19 = value;
	}

	inline static int32_t get_offset_of_duration_20() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___duration_20)); }
	inline float get_duration_20() const { return ___duration_20; }
	inline float* get_address_of_duration_20() { return &___duration_20; }
	inline void set_duration_20(float value)
	{
		___duration_20 = value;
	}

	inline static int32_t get_offset_of_easeType_21() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___easeType_21)); }
	inline int32_t get_easeType_21() const { return ___easeType_21; }
	inline int32_t* get_address_of_easeType_21() { return &___easeType_21; }
	inline void set_easeType_21(int32_t value)
	{
		___easeType_21 = value;
	}

	inline static int32_t get_offset_of_easeCurve_22() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___easeCurve_22)); }
	inline AnimationCurve_t3046754366 * get_easeCurve_22() const { return ___easeCurve_22; }
	inline AnimationCurve_t3046754366 ** get_address_of_easeCurve_22() { return &___easeCurve_22; }
	inline void set_easeCurve_22(AnimationCurve_t3046754366 * value)
	{
		___easeCurve_22 = value;
		Il2CppCodeGenWriteBarrier((&___easeCurve_22), value);
	}

	inline static int32_t get_offset_of_loops_23() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___loops_23)); }
	inline int32_t get_loops_23() const { return ___loops_23; }
	inline int32_t* get_address_of_loops_23() { return &___loops_23; }
	inline void set_loops_23(int32_t value)
	{
		___loops_23 = value;
	}

	inline static int32_t get_offset_of_id_24() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___id_24)); }
	inline String_t* get_id_24() const { return ___id_24; }
	inline String_t** get_address_of_id_24() { return &___id_24; }
	inline void set_id_24(String_t* value)
	{
		___id_24 = value;
		Il2CppCodeGenWriteBarrier((&___id_24), value);
	}

	inline static int32_t get_offset_of_loopType_25() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___loopType_25)); }
	inline int32_t get_loopType_25() const { return ___loopType_25; }
	inline int32_t* get_address_of_loopType_25() { return &___loopType_25; }
	inline void set_loopType_25(int32_t value)
	{
		___loopType_25 = value;
	}

	inline static int32_t get_offset_of_orientType_26() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___orientType_26)); }
	inline int32_t get_orientType_26() const { return ___orientType_26; }
	inline int32_t* get_address_of_orientType_26() { return &___orientType_26; }
	inline void set_orientType_26(int32_t value)
	{
		___orientType_26 = value;
	}

	inline static int32_t get_offset_of_lookAtTransform_27() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___lookAtTransform_27)); }
	inline Transform_t3600365921 * get_lookAtTransform_27() const { return ___lookAtTransform_27; }
	inline Transform_t3600365921 ** get_address_of_lookAtTransform_27() { return &___lookAtTransform_27; }
	inline void set_lookAtTransform_27(Transform_t3600365921 * value)
	{
		___lookAtTransform_27 = value;
		Il2CppCodeGenWriteBarrier((&___lookAtTransform_27), value);
	}

	inline static int32_t get_offset_of_lookAtPosition_28() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___lookAtPosition_28)); }
	inline Vector3_t3722313464  get_lookAtPosition_28() const { return ___lookAtPosition_28; }
	inline Vector3_t3722313464 * get_address_of_lookAtPosition_28() { return &___lookAtPosition_28; }
	inline void set_lookAtPosition_28(Vector3_t3722313464  value)
	{
		___lookAtPosition_28 = value;
	}

	inline static int32_t get_offset_of_lookAhead_29() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___lookAhead_29)); }
	inline float get_lookAhead_29() const { return ___lookAhead_29; }
	inline float* get_address_of_lookAhead_29() { return &___lookAhead_29; }
	inline void set_lookAhead_29(float value)
	{
		___lookAhead_29 = value;
	}

	inline static int32_t get_offset_of_autoPlay_30() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___autoPlay_30)); }
	inline bool get_autoPlay_30() const { return ___autoPlay_30; }
	inline bool* get_address_of_autoPlay_30() { return &___autoPlay_30; }
	inline void set_autoPlay_30(bool value)
	{
		___autoPlay_30 = value;
	}

	inline static int32_t get_offset_of_autoKill_31() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___autoKill_31)); }
	inline bool get_autoKill_31() const { return ___autoKill_31; }
	inline bool* get_address_of_autoKill_31() { return &___autoKill_31; }
	inline void set_autoKill_31(bool value)
	{
		___autoKill_31 = value;
	}

	inline static int32_t get_offset_of_relative_32() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___relative_32)); }
	inline bool get_relative_32() const { return ___relative_32; }
	inline bool* get_address_of_relative_32() { return &___relative_32; }
	inline void set_relative_32(bool value)
	{
		___relative_32 = value;
	}

	inline static int32_t get_offset_of_isLocal_33() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___isLocal_33)); }
	inline bool get_isLocal_33() const { return ___isLocal_33; }
	inline bool* get_address_of_isLocal_33() { return &___isLocal_33; }
	inline void set_isLocal_33(bool value)
	{
		___isLocal_33 = value;
	}

	inline static int32_t get_offset_of_isClosedPath_34() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___isClosedPath_34)); }
	inline bool get_isClosedPath_34() const { return ___isClosedPath_34; }
	inline bool* get_address_of_isClosedPath_34() { return &___isClosedPath_34; }
	inline void set_isClosedPath_34(bool value)
	{
		___isClosedPath_34 = value;
	}

	inline static int32_t get_offset_of_pathResolution_35() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___pathResolution_35)); }
	inline int32_t get_pathResolution_35() const { return ___pathResolution_35; }
	inline int32_t* get_address_of_pathResolution_35() { return &___pathResolution_35; }
	inline void set_pathResolution_35(int32_t value)
	{
		___pathResolution_35 = value;
	}

	inline static int32_t get_offset_of_pathMode_36() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___pathMode_36)); }
	inline int32_t get_pathMode_36() const { return ___pathMode_36; }
	inline int32_t* get_address_of_pathMode_36() { return &___pathMode_36; }
	inline void set_pathMode_36(int32_t value)
	{
		___pathMode_36 = value;
	}

	inline static int32_t get_offset_of_lockRotation_37() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___lockRotation_37)); }
	inline int32_t get_lockRotation_37() const { return ___lockRotation_37; }
	inline int32_t* get_address_of_lockRotation_37() { return &___lockRotation_37; }
	inline void set_lockRotation_37(int32_t value)
	{
		___lockRotation_37 = value;
	}

	inline static int32_t get_offset_of_assignForwardAndUp_38() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___assignForwardAndUp_38)); }
	inline bool get_assignForwardAndUp_38() const { return ___assignForwardAndUp_38; }
	inline bool* get_address_of_assignForwardAndUp_38() { return &___assignForwardAndUp_38; }
	inline void set_assignForwardAndUp_38(bool value)
	{
		___assignForwardAndUp_38 = value;
	}

	inline static int32_t get_offset_of_forwardDirection_39() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___forwardDirection_39)); }
	inline Vector3_t3722313464  get_forwardDirection_39() const { return ___forwardDirection_39; }
	inline Vector3_t3722313464 * get_address_of_forwardDirection_39() { return &___forwardDirection_39; }
	inline void set_forwardDirection_39(Vector3_t3722313464  value)
	{
		___forwardDirection_39 = value;
	}

	inline static int32_t get_offset_of_upDirection_40() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___upDirection_40)); }
	inline Vector3_t3722313464  get_upDirection_40() const { return ___upDirection_40; }
	inline Vector3_t3722313464 * get_address_of_upDirection_40() { return &___upDirection_40; }
	inline void set_upDirection_40(Vector3_t3722313464  value)
	{
		___upDirection_40 = value;
	}

	inline static int32_t get_offset_of_tweenRigidbody_41() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___tweenRigidbody_41)); }
	inline bool get_tweenRigidbody_41() const { return ___tweenRigidbody_41; }
	inline bool* get_address_of_tweenRigidbody_41() { return &___tweenRigidbody_41; }
	inline void set_tweenRigidbody_41(bool value)
	{
		___tweenRigidbody_41 = value;
	}

	inline static int32_t get_offset_of_wps_42() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___wps_42)); }
	inline List_1_t899420910 * get_wps_42() const { return ___wps_42; }
	inline List_1_t899420910 ** get_address_of_wps_42() { return &___wps_42; }
	inline void set_wps_42(List_1_t899420910 * value)
	{
		___wps_42 = value;
		Il2CppCodeGenWriteBarrier((&___wps_42), value);
	}

	inline static int32_t get_offset_of_fullWps_43() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___fullWps_43)); }
	inline List_1_t899420910 * get_fullWps_43() const { return ___fullWps_43; }
	inline List_1_t899420910 ** get_address_of_fullWps_43() { return &___fullWps_43; }
	inline void set_fullWps_43(List_1_t899420910 * value)
	{
		___fullWps_43 = value;
		Il2CppCodeGenWriteBarrier((&___fullWps_43), value);
	}

	inline static int32_t get_offset_of_path_44() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___path_44)); }
	inline Path_t3614338981 * get_path_44() const { return ___path_44; }
	inline Path_t3614338981 ** get_address_of_path_44() { return &___path_44; }
	inline void set_path_44(Path_t3614338981 * value)
	{
		___path_44 = value;
		Il2CppCodeGenWriteBarrier((&___path_44), value);
	}

	inline static int32_t get_offset_of_inspectorMode_45() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___inspectorMode_45)); }
	inline int32_t get_inspectorMode_45() const { return ___inspectorMode_45; }
	inline int32_t* get_address_of_inspectorMode_45() { return &___inspectorMode_45; }
	inline void set_inspectorMode_45(int32_t value)
	{
		___inspectorMode_45 = value;
	}

	inline static int32_t get_offset_of_pathType_46() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___pathType_46)); }
	inline int32_t get_pathType_46() const { return ___pathType_46; }
	inline int32_t* get_address_of_pathType_46() { return &___pathType_46; }
	inline void set_pathType_46(int32_t value)
	{
		___pathType_46 = value;
	}

	inline static int32_t get_offset_of_handlesType_47() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___handlesType_47)); }
	inline int32_t get_handlesType_47() const { return ___handlesType_47; }
	inline int32_t* get_address_of_handlesType_47() { return &___handlesType_47; }
	inline void set_handlesType_47(int32_t value)
	{
		___handlesType_47 = value;
	}

	inline static int32_t get_offset_of_livePreview_48() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___livePreview_48)); }
	inline bool get_livePreview_48() const { return ___livePreview_48; }
	inline bool* get_address_of_livePreview_48() { return &___livePreview_48; }
	inline void set_livePreview_48(bool value)
	{
		___livePreview_48 = value;
	}

	inline static int32_t get_offset_of_handlesDrawMode_49() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___handlesDrawMode_49)); }
	inline int32_t get_handlesDrawMode_49() const { return ___handlesDrawMode_49; }
	inline int32_t* get_address_of_handlesDrawMode_49() { return &___handlesDrawMode_49; }
	inline void set_handlesDrawMode_49(int32_t value)
	{
		___handlesDrawMode_49 = value;
	}

	inline static int32_t get_offset_of_perspectiveHandleSize_50() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___perspectiveHandleSize_50)); }
	inline float get_perspectiveHandleSize_50() const { return ___perspectiveHandleSize_50; }
	inline float* get_address_of_perspectiveHandleSize_50() { return &___perspectiveHandleSize_50; }
	inline void set_perspectiveHandleSize_50(float value)
	{
		___perspectiveHandleSize_50 = value;
	}

	inline static int32_t get_offset_of_showIndexes_51() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___showIndexes_51)); }
	inline bool get_showIndexes_51() const { return ___showIndexes_51; }
	inline bool* get_address_of_showIndexes_51() { return &___showIndexes_51; }
	inline void set_showIndexes_51(bool value)
	{
		___showIndexes_51 = value;
	}

	inline static int32_t get_offset_of_showWpLength_52() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___showWpLength_52)); }
	inline bool get_showWpLength_52() const { return ___showWpLength_52; }
	inline bool* get_address_of_showWpLength_52() { return &___showWpLength_52; }
	inline void set_showWpLength_52(bool value)
	{
		___showWpLength_52 = value;
	}

	inline static int32_t get_offset_of_pathColor_53() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___pathColor_53)); }
	inline Color_t2555686324  get_pathColor_53() const { return ___pathColor_53; }
	inline Color_t2555686324 * get_address_of_pathColor_53() { return &___pathColor_53; }
	inline void set_pathColor_53(Color_t2555686324  value)
	{
		___pathColor_53 = value;
	}

	inline static int32_t get_offset_of_lastSrcPosition_54() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___lastSrcPosition_54)); }
	inline Vector3_t3722313464  get_lastSrcPosition_54() const { return ___lastSrcPosition_54; }
	inline Vector3_t3722313464 * get_address_of_lastSrcPosition_54() { return &___lastSrcPosition_54; }
	inline void set_lastSrcPosition_54(Vector3_t3722313464  value)
	{
		___lastSrcPosition_54 = value;
	}

	inline static int32_t get_offset_of_wpsDropdown_55() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___wpsDropdown_55)); }
	inline bool get_wpsDropdown_55() const { return ___wpsDropdown_55; }
	inline bool* get_address_of_wpsDropdown_55() { return &___wpsDropdown_55; }
	inline void set_wpsDropdown_55(bool value)
	{
		___wpsDropdown_55 = value;
	}

	inline static int32_t get_offset_of_dropToFloorOffset_56() { return static_cast<int32_t>(offsetof(DOTweenPath_t3192963685, ___dropToFloorOffset_56)); }
	inline float get_dropToFloorOffset_56() const { return ___dropToFloorOffset_56; }
	inline float* get_address_of_dropToFloorOffset_56() { return &___dropToFloorOffset_56; }
	inline void set_dropToFloorOffset_56(float value)
	{
		___dropToFloorOffset_56 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENPATH_T3192963685_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3000 = { sizeof (ShortcutExtensions43_t1788548476), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3001 = { sizeof (U3CU3Ec__DisplayClass2_0_t116211281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3001[1] = 
{
	U3CU3Ec__DisplayClass2_0_t116211281::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3002 = { sizeof (U3CU3Ec__DisplayClass3_0_t2845094636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3002[1] = 
{
	U3CU3Ec__DisplayClass3_0_t2845094636::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3003 = { sizeof (U3CU3Ec__DisplayClass5_0_t2038525582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3003[1] = 
{
	U3CU3Ec__DisplayClass5_0_t2038525582::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3004 = { sizeof (U3CU3Ec__DisplayClass8_0_t1635241055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3004[1] = 
{
	U3CU3Ec__DisplayClass8_0_t1635241055::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3005 = { sizeof (U3CModuleU3E_t692745555), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3006 = { sizeof (DOTweenUtils46_t543860410), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3007 = { sizeof (ShortcutExtensions46_t3362526588), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3008 = { sizeof (U3CU3Ec__DisplayClass0_0_t892042862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3008[1] = 
{
	U3CU3Ec__DisplayClass0_0_t892042862::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3009 = { sizeof (U3CU3Ec__DisplayClass3_0_t1295327389), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3009[1] = 
{
	U3CU3Ec__DisplayClass3_0_t1295327389::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3010 = { sizeof (U3CU3Ec__DisplayClass4_0_t2861411330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3010[1] = 
{
	U3CU3Ec__DisplayClass4_0_t2861411330::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3011 = { sizeof (U3CU3Ec__DisplayClass13_0_t2635598378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3011[1] = 
{
	U3CU3Ec__DisplayClass13_0_t2635598378::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3012 = { sizeof (U3CU3Ec__DisplayClass16_0_t1443957290), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3012[1] = 
{
	U3CU3Ec__DisplayClass16_0_t1443957290::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3013 = { sizeof (U3CU3Ec__DisplayClass22_0_t2245367183), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3013[1] = 
{
	U3CU3Ec__DisplayClass22_0_t2245367183::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3014 = { sizeof (U3CU3Ec__DisplayClass23_0_t4201682319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3014[1] = 
{
	U3CU3Ec__DisplayClass23_0_t4201682319::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3015 = { sizeof (U3CU3Ec__DisplayClass25_0_t289052047), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3015[1] = 
{
	U3CU3Ec__DisplayClass25_0_t289052047::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3016 = { sizeof (U3CU3Ec__DisplayClass31_0_t1090461940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3016[1] = 
{
	U3CU3Ec__DisplayClass31_0_t1090461940::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3017 = { sizeof (U3CU3Ec__DisplayClass32_0_t3811451124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3017[1] = 
{
	U3CU3Ec__DisplayClass32_0_t3811451124::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3018 = { sizeof (U3CU3Ec__DisplayClass33_0_t1472798964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3018[1] = 
{
	U3CU3Ec__DisplayClass33_0_t1472798964::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3019 = { sizeof (U3CModuleU3E_t692745556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3020 = { sizeof (DOTweenVisualManager_t1560353112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3020[5] = 
{
	DOTweenVisualManager_t1560353112::get_offset_of_preset_2(),
	DOTweenVisualManager_t1560353112::get_offset_of_onEnableBehaviour_3(),
	DOTweenVisualManager_t1560353112::get_offset_of_onDisableBehaviour_4(),
	DOTweenVisualManager_t1560353112::get_offset_of__requiresRestartFromSpawnPoint_5(),
	DOTweenVisualManager_t1560353112::get_offset_of__animComponent_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3021 = { sizeof (HandlesDrawMode_t2193450492)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3021[3] = 
{
	HandlesDrawMode_t2193450492::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3022 = { sizeof (HandlesType_t4074904290)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3022[3] = 
{
	HandlesType_t4074904290::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3023 = { sizeof (DOTweenInspectorMode_t2656909913)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3023[5] = 
{
	DOTweenInspectorMode_t2656909913::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3024 = { sizeof (DOTweenPath_t3192963685), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3024[38] = 
{
	DOTweenPath_t3192963685::get_offset_of_delay_19(),
	DOTweenPath_t3192963685::get_offset_of_duration_20(),
	DOTweenPath_t3192963685::get_offset_of_easeType_21(),
	DOTweenPath_t3192963685::get_offset_of_easeCurve_22(),
	DOTweenPath_t3192963685::get_offset_of_loops_23(),
	DOTweenPath_t3192963685::get_offset_of_id_24(),
	DOTweenPath_t3192963685::get_offset_of_loopType_25(),
	DOTweenPath_t3192963685::get_offset_of_orientType_26(),
	DOTweenPath_t3192963685::get_offset_of_lookAtTransform_27(),
	DOTweenPath_t3192963685::get_offset_of_lookAtPosition_28(),
	DOTweenPath_t3192963685::get_offset_of_lookAhead_29(),
	DOTweenPath_t3192963685::get_offset_of_autoPlay_30(),
	DOTweenPath_t3192963685::get_offset_of_autoKill_31(),
	DOTweenPath_t3192963685::get_offset_of_relative_32(),
	DOTweenPath_t3192963685::get_offset_of_isLocal_33(),
	DOTweenPath_t3192963685::get_offset_of_isClosedPath_34(),
	DOTweenPath_t3192963685::get_offset_of_pathResolution_35(),
	DOTweenPath_t3192963685::get_offset_of_pathMode_36(),
	DOTweenPath_t3192963685::get_offset_of_lockRotation_37(),
	DOTweenPath_t3192963685::get_offset_of_assignForwardAndUp_38(),
	DOTweenPath_t3192963685::get_offset_of_forwardDirection_39(),
	DOTweenPath_t3192963685::get_offset_of_upDirection_40(),
	DOTweenPath_t3192963685::get_offset_of_tweenRigidbody_41(),
	DOTweenPath_t3192963685::get_offset_of_wps_42(),
	DOTweenPath_t3192963685::get_offset_of_fullWps_43(),
	DOTweenPath_t3192963685::get_offset_of_path_44(),
	DOTweenPath_t3192963685::get_offset_of_inspectorMode_45(),
	DOTweenPath_t3192963685::get_offset_of_pathType_46(),
	DOTweenPath_t3192963685::get_offset_of_handlesType_47(),
	DOTweenPath_t3192963685::get_offset_of_livePreview_48(),
	DOTweenPath_t3192963685::get_offset_of_handlesDrawMode_49(),
	DOTweenPath_t3192963685::get_offset_of_perspectiveHandleSize_50(),
	DOTweenPath_t3192963685::get_offset_of_showIndexes_51(),
	DOTweenPath_t3192963685::get_offset_of_showWpLength_52(),
	DOTweenPath_t3192963685::get_offset_of_pathColor_53(),
	DOTweenPath_t3192963685::get_offset_of_lastSrcPosition_54(),
	DOTweenPath_t3192963685::get_offset_of_wpsDropdown_55(),
	DOTweenPath_t3192963685::get_offset_of_dropToFloorOffset_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3025 = { sizeof (ABSAnimationComponent_t262169234), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3025[17] = 
{
	ABSAnimationComponent_t262169234::get_offset_of_updateType_2(),
	ABSAnimationComponent_t262169234::get_offset_of_isSpeedBased_3(),
	ABSAnimationComponent_t262169234::get_offset_of_hasOnStart_4(),
	ABSAnimationComponent_t262169234::get_offset_of_hasOnPlay_5(),
	ABSAnimationComponent_t262169234::get_offset_of_hasOnUpdate_6(),
	ABSAnimationComponent_t262169234::get_offset_of_hasOnStepComplete_7(),
	ABSAnimationComponent_t262169234::get_offset_of_hasOnComplete_8(),
	ABSAnimationComponent_t262169234::get_offset_of_hasOnTweenCreated_9(),
	ABSAnimationComponent_t262169234::get_offset_of_hasOnRewind_10(),
	ABSAnimationComponent_t262169234::get_offset_of_onStart_11(),
	ABSAnimationComponent_t262169234::get_offset_of_onPlay_12(),
	ABSAnimationComponent_t262169234::get_offset_of_onUpdate_13(),
	ABSAnimationComponent_t262169234::get_offset_of_onStepComplete_14(),
	ABSAnimationComponent_t262169234::get_offset_of_onComplete_15(),
	ABSAnimationComponent_t262169234::get_offset_of_onTweenCreated_16(),
	ABSAnimationComponent_t262169234::get_offset_of_onRewind_17(),
	ABSAnimationComponent_t262169234::get_offset_of_tween_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3026 = { sizeof (DOTweenAnimationType_t2748799855)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3026[23] = 
{
	DOTweenAnimationType_t2748799855::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3027 = { sizeof (OnDisableBehaviour_t979035984)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3027[7] = 
{
	OnDisableBehaviour_t979035984::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3028 = { sizeof (OnEnableBehaviour_t3474514863)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3028[5] = 
{
	OnEnableBehaviour_t3474514863::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3029 = { sizeof (TargetType_t3479356996)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3029[17] = 
{
	TargetType_t3479356996::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3030 = { sizeof (VisualManagerPreset_t2677960456)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3030[3] = 
{
	VisualManagerPreset_t2677960456::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3031 = { sizeof (U3CModuleU3E_t692745557), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3032 = { sizeof (AddingNewEventArgs_t2974299087), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3033 = { sizeof (AddingNewEventHandler_t4128025860), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3034 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3035 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3036 = { sizeof (NotifyCollectionChangedAction_t2928948188)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3036[6] = 
{
	NotifyCollectionChangedAction_t2928948188::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3037 = { sizeof (NotifyCollectionChangedEventArgs_t1368105863), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3038 = { sizeof (NotifyCollectionChangedEventHandler_t2206564399), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3039 = { sizeof (PropertyChangingEventArgs_t2067745136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3039[1] = 
{
	PropertyChangingEventArgs_t2067745136::get_offset_of_U3CPropertyNameU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3040 = { sizeof (PropertyChangingEventHandler_t2830353497), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3041 = { sizeof (ConstructorHandling_t3189370124)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3041[3] = 
{
	ConstructorHandling_t3189370124::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3042 = { sizeof (DateFormatHandling_t1376167855)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3042[3] = 
{
	DateFormatHandling_t1376167855::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3043 = { sizeof (DateParseHandling_t2751701876)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3043[4] = 
{
	DateParseHandling_t2751701876::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3044 = { sizeof (DateTimeZoneHandling_t3002599730)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3044[5] = 
{
	DateTimeZoneHandling_t3002599730::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3045 = { sizeof (FloatFormatHandling_t562907821)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3045[4] = 
{
	FloatFormatHandling_t562907821::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3046 = { sizeof (FloatParseHandling_t1006965658)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3046[3] = 
{
	FloatParseHandling_t1006965658::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3047 = { sizeof (Formatting_t2192044929)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3047[3] = 
{
	Formatting_t2192044929::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3048 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3049 = { sizeof (JsonConstructorAttribute_t2226951669), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3050 = { sizeof (JsonDictionaryAttribute_t3890173510), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3051 = { sizeof (JsonException_t3720114400), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3052 = { sizeof (JsonExtensionDataAttribute_t1775849281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3052[2] = 
{
	JsonExtensionDataAttribute_t1775849281::get_offset_of_U3CWriteDataU3Ek__BackingField_0(),
	JsonExtensionDataAttribute_t1775849281::get_offset_of_U3CReadDataU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3053 = { sizeof (JsonContainerType_t3191599701)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3053[5] = 
{
	JsonContainerType_t3191599701::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3054 = { sizeof (JsonPosition_t2528027714)+ sizeof (RuntimeObject), sizeof(JsonPosition_t2528027714_marshaled_pinvoke), sizeof(JsonPosition_t2528027714_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3054[5] = 
{
	JsonPosition_t2528027714_StaticFields::get_offset_of_SpecialCharacters_0(),
	JsonPosition_t2528027714::get_offset_of_Type_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JsonPosition_t2528027714::get_offset_of_Position_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JsonPosition_t2528027714::get_offset_of_PropertyName_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JsonPosition_t2528027714::get_offset_of_HasIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3055 = { sizeof (JsonRequiredAttribute_t2327646188), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3056 = { sizeof (MetadataPropertyHandling_t1042169553)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3056[4] = 
{
	MetadataPropertyHandling_t1042169553::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3057 = { sizeof (StringEscapeHandling_t4077875565)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3057[4] = 
{
	StringEscapeHandling_t4077875565::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3058 = { sizeof (Required_t1591382370)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3058[5] = 
{
	Required_t1591382370::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3059 = { sizeof (PreserveReferencesHandling_t815988812)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3059[5] = 
{
	PreserveReferencesHandling_t815988812::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3060 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3061 = { sizeof (JsonArrayAttribute_t3155801748), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3062 = { sizeof (JsonContainerAttribute_t3874748064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3062[6] = 
{
	JsonContainerAttribute_t3874748064::get_offset_of_U3CItemConverterTypeU3Ek__BackingField_0(),
	JsonContainerAttribute_t3874748064::get_offset_of_U3CItemConverterParametersU3Ek__BackingField_1(),
	JsonContainerAttribute_t3874748064::get_offset_of__isReference_2(),
	JsonContainerAttribute_t3874748064::get_offset_of__itemIsReference_3(),
	JsonContainerAttribute_t3874748064::get_offset_of__itemReferenceLoopHandling_4(),
	JsonContainerAttribute_t3874748064::get_offset_of__itemTypeNameHandling_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3063 = { sizeof (DefaultValueHandling_t3240998650)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3063[5] = 
{
	DefaultValueHandling_t3240998650::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3064 = { sizeof (JsonConverterAttribute_t3132853130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3064[2] = 
{
	JsonConverterAttribute_t3132853130::get_offset_of__converterType_0(),
	JsonConverterAttribute_t3132853130::get_offset_of_U3CConverterParametersU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3065 = { sizeof (JsonObjectAttribute_t3277381142), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3065[2] = 
{
	JsonObjectAttribute_t3277381142::get_offset_of__memberSerialization_6(),
	JsonObjectAttribute_t3277381142::get_offset_of__itemRequired_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3066 = { sizeof (JsonSerializerSettings_t2139255314), -1, sizeof(JsonSerializerSettings_t2139255314_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3066[33] = 
{
	JsonSerializerSettings_t2139255314_StaticFields::get_offset_of_DefaultContext_0(),
	JsonSerializerSettings_t2139255314_StaticFields::get_offset_of_DefaultCulture_1(),
	JsonSerializerSettings_t2139255314::get_offset_of__formatting_2(),
	JsonSerializerSettings_t2139255314::get_offset_of__dateFormatHandling_3(),
	JsonSerializerSettings_t2139255314::get_offset_of__dateTimeZoneHandling_4(),
	JsonSerializerSettings_t2139255314::get_offset_of__dateParseHandling_5(),
	JsonSerializerSettings_t2139255314::get_offset_of__floatFormatHandling_6(),
	JsonSerializerSettings_t2139255314::get_offset_of__floatParseHandling_7(),
	JsonSerializerSettings_t2139255314::get_offset_of__stringEscapeHandling_8(),
	JsonSerializerSettings_t2139255314::get_offset_of__culture_9(),
	JsonSerializerSettings_t2139255314::get_offset_of__checkAdditionalContent_10(),
	JsonSerializerSettings_t2139255314::get_offset_of__maxDepth_11(),
	JsonSerializerSettings_t2139255314::get_offset_of__maxDepthSet_12(),
	JsonSerializerSettings_t2139255314::get_offset_of__dateFormatString_13(),
	JsonSerializerSettings_t2139255314::get_offset_of__dateFormatStringSet_14(),
	JsonSerializerSettings_t2139255314::get_offset_of__typeNameAssemblyFormat_15(),
	JsonSerializerSettings_t2139255314::get_offset_of__defaultValueHandling_16(),
	JsonSerializerSettings_t2139255314::get_offset_of__preserveReferencesHandling_17(),
	JsonSerializerSettings_t2139255314::get_offset_of__nullValueHandling_18(),
	JsonSerializerSettings_t2139255314::get_offset_of__objectCreationHandling_19(),
	JsonSerializerSettings_t2139255314::get_offset_of__missingMemberHandling_20(),
	JsonSerializerSettings_t2139255314::get_offset_of__referenceLoopHandling_21(),
	JsonSerializerSettings_t2139255314::get_offset_of__context_22(),
	JsonSerializerSettings_t2139255314::get_offset_of__constructorHandling_23(),
	JsonSerializerSettings_t2139255314::get_offset_of__typeNameHandling_24(),
	JsonSerializerSettings_t2139255314::get_offset_of__metadataPropertyHandling_25(),
	JsonSerializerSettings_t2139255314::get_offset_of_U3CConvertersU3Ek__BackingField_26(),
	JsonSerializerSettings_t2139255314::get_offset_of_U3CContractResolverU3Ek__BackingField_27(),
	JsonSerializerSettings_t2139255314::get_offset_of_U3CEqualityComparerU3Ek__BackingField_28(),
	JsonSerializerSettings_t2139255314::get_offset_of_U3CReferenceResolverProviderU3Ek__BackingField_29(),
	JsonSerializerSettings_t2139255314::get_offset_of_U3CTraceWriterU3Ek__BackingField_30(),
	JsonSerializerSettings_t2139255314::get_offset_of_U3CBinderU3Ek__BackingField_31(),
	JsonSerializerSettings_t2139255314::get_offset_of_U3CErrorU3Ek__BackingField_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3067 = { sizeof (U3CU3Ec__DisplayClass90_0_t3738200698), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3068 = { sizeof (MemberSerialization_t1095097135)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3068[4] = 
{
	MemberSerialization_t1095097135::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3069 = { sizeof (ObjectCreationHandling_t3160794531)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3069[4] = 
{
	ObjectCreationHandling_t3160794531::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3070 = { sizeof (ReadType_t340786580)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3070[10] = 
{
	ReadType_t340786580::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3071 = { sizeof (JsonTextReader_t262560521), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3071[11] = 
{
	JsonTextReader_t262560521::get_offset_of__reader_15(),
	JsonTextReader_t262560521::get_offset_of__chars_16(),
	JsonTextReader_t262560521::get_offset_of__charsUsed_17(),
	JsonTextReader_t262560521::get_offset_of__charPos_18(),
	JsonTextReader_t262560521::get_offset_of__lineStartPos_19(),
	JsonTextReader_t262560521::get_offset_of__lineNumber_20(),
	JsonTextReader_t262560521::get_offset_of__isEndOfFile_21(),
	JsonTextReader_t262560521::get_offset_of__stringBuffer_22(),
	JsonTextReader_t262560521::get_offset_of__stringReference_23(),
	JsonTextReader_t262560521::get_offset_of__arrayPool_24(),
	JsonTextReader_t262560521::get_offset_of_NameTable_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3072 = { sizeof (JsonPropertyAttribute_t2223277056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3072[14] = 
{
	JsonPropertyAttribute_t2223277056::get_offset_of__nullValueHandling_0(),
	JsonPropertyAttribute_t2223277056::get_offset_of__defaultValueHandling_1(),
	JsonPropertyAttribute_t2223277056::get_offset_of__referenceLoopHandling_2(),
	JsonPropertyAttribute_t2223277056::get_offset_of__objectCreationHandling_3(),
	JsonPropertyAttribute_t2223277056::get_offset_of__typeNameHandling_4(),
	JsonPropertyAttribute_t2223277056::get_offset_of__isReference_5(),
	JsonPropertyAttribute_t2223277056::get_offset_of__order_6(),
	JsonPropertyAttribute_t2223277056::get_offset_of__required_7(),
	JsonPropertyAttribute_t2223277056::get_offset_of__itemIsReference_8(),
	JsonPropertyAttribute_t2223277056::get_offset_of__itemReferenceLoopHandling_9(),
	JsonPropertyAttribute_t2223277056::get_offset_of__itemTypeNameHandling_10(),
	JsonPropertyAttribute_t2223277056::get_offset_of_U3CItemConverterTypeU3Ek__BackingField_11(),
	JsonPropertyAttribute_t2223277056::get_offset_of_U3CItemConverterParametersU3Ek__BackingField_12(),
	JsonPropertyAttribute_t2223277056::get_offset_of_U3CPropertyNameU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3073 = { sizeof (JsonIgnoreAttribute_t836745302), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3074 = { sizeof (JsonTextWriter_t1095349912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3074[10] = 
{
	JsonTextWriter_t1095349912::get_offset_of__writer_13(),
	JsonTextWriter_t1095349912::get_offset_of__base64Encoder_14(),
	JsonTextWriter_t1095349912::get_offset_of__indentChar_15(),
	JsonTextWriter_t1095349912::get_offset_of__indentation_16(),
	JsonTextWriter_t1095349912::get_offset_of__quoteChar_17(),
	JsonTextWriter_t1095349912::get_offset_of__quoteName_18(),
	JsonTextWriter_t1095349912::get_offset_of__charEscapeFlags_19(),
	JsonTextWriter_t1095349912::get_offset_of__writeBuffer_20(),
	JsonTextWriter_t1095349912::get_offset_of__arrayPool_21(),
	JsonTextWriter_t1095349912::get_offset_of__indentChars_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3075 = { sizeof (JsonWriterException_t1282940098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3075[1] = 
{
	JsonWriterException_t1282940098::get_offset_of_U3CPathU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3076 = { sizeof (JsonReaderException_t3553144781), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3076[3] = 
{
	JsonReaderException_t3553144781::get_offset_of_U3CLineNumberU3Ek__BackingField_11(),
	JsonReaderException_t3553144781::get_offset_of_U3CLinePositionU3Ek__BackingField_12(),
	JsonReaderException_t3553144781::get_offset_of_U3CPathU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3077 = { sizeof (JsonConverter_t1047106545), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3078 = { sizeof (JsonConverterCollection_t3675964822), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3079 = { sizeof (JsonReader_t2369136700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3079[15] = 
{
	JsonReader_t2369136700::get_offset_of__tokenType_0(),
	JsonReader_t2369136700::get_offset_of__value_1(),
	JsonReader_t2369136700::get_offset_of__quoteChar_2(),
	JsonReader_t2369136700::get_offset_of__currentState_3(),
	JsonReader_t2369136700::get_offset_of__currentPosition_4(),
	JsonReader_t2369136700::get_offset_of__culture_5(),
	JsonReader_t2369136700::get_offset_of__dateTimeZoneHandling_6(),
	JsonReader_t2369136700::get_offset_of__maxDepth_7(),
	JsonReader_t2369136700::get_offset_of__hasExceededMaxDepth_8(),
	JsonReader_t2369136700::get_offset_of__dateParseHandling_9(),
	JsonReader_t2369136700::get_offset_of__floatParseHandling_10(),
	JsonReader_t2369136700::get_offset_of__dateFormatString_11(),
	JsonReader_t2369136700::get_offset_of__stack_12(),
	JsonReader_t2369136700::get_offset_of_U3CCloseInputU3Ek__BackingField_13(),
	JsonReader_t2369136700::get_offset_of_U3CSupportMultipleContentU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3080 = { sizeof (State_t879723262)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3080[14] = 
{
	State_t879723262::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3081 = { sizeof (JsonConvert_t3077351166), -1, sizeof(JsonConvert_t3077351166_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3081[9] = 
{
	JsonConvert_t3077351166_StaticFields::get_offset_of_U3CDefaultSettingsU3Ek__BackingField_0(),
	JsonConvert_t3077351166_StaticFields::get_offset_of_True_1(),
	JsonConvert_t3077351166_StaticFields::get_offset_of_False_2(),
	JsonConvert_t3077351166_StaticFields::get_offset_of_Null_3(),
	JsonConvert_t3077351166_StaticFields::get_offset_of_Undefined_4(),
	JsonConvert_t3077351166_StaticFields::get_offset_of_PositiveInfinity_5(),
	JsonConvert_t3077351166_StaticFields::get_offset_of_NegativeInfinity_6(),
	JsonConvert_t3077351166_StaticFields::get_offset_of_NaN_7(),
	JsonConvert_t3077351166_StaticFields::get_offset_of_InitialSerializerSettings_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3082 = { sizeof (JsonSerializationException_t4152118729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3083 = { sizeof (JsonSerializer_t1424496335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3083[31] = 
{
	JsonSerializer_t1424496335::get_offset_of__typeNameHandling_0(),
	JsonSerializer_t1424496335::get_offset_of__typeNameAssemblyFormat_1(),
	JsonSerializer_t1424496335::get_offset_of__preserveReferencesHandling_2(),
	JsonSerializer_t1424496335::get_offset_of__referenceLoopHandling_3(),
	JsonSerializer_t1424496335::get_offset_of__missingMemberHandling_4(),
	JsonSerializer_t1424496335::get_offset_of__objectCreationHandling_5(),
	JsonSerializer_t1424496335::get_offset_of__nullValueHandling_6(),
	JsonSerializer_t1424496335::get_offset_of__defaultValueHandling_7(),
	JsonSerializer_t1424496335::get_offset_of__constructorHandling_8(),
	JsonSerializer_t1424496335::get_offset_of__metadataPropertyHandling_9(),
	JsonSerializer_t1424496335::get_offset_of__converters_10(),
	JsonSerializer_t1424496335::get_offset_of__contractResolver_11(),
	JsonSerializer_t1424496335::get_offset_of__traceWriter_12(),
	JsonSerializer_t1424496335::get_offset_of__equalityComparer_13(),
	JsonSerializer_t1424496335::get_offset_of__binder_14(),
	JsonSerializer_t1424496335::get_offset_of__context_15(),
	JsonSerializer_t1424496335::get_offset_of__referenceResolver_16(),
	JsonSerializer_t1424496335::get_offset_of__formatting_17(),
	JsonSerializer_t1424496335::get_offset_of__dateFormatHandling_18(),
	JsonSerializer_t1424496335::get_offset_of__dateTimeZoneHandling_19(),
	JsonSerializer_t1424496335::get_offset_of__dateParseHandling_20(),
	JsonSerializer_t1424496335::get_offset_of__floatFormatHandling_21(),
	JsonSerializer_t1424496335::get_offset_of__floatParseHandling_22(),
	JsonSerializer_t1424496335::get_offset_of__stringEscapeHandling_23(),
	JsonSerializer_t1424496335::get_offset_of__culture_24(),
	JsonSerializer_t1424496335::get_offset_of__maxDepth_25(),
	JsonSerializer_t1424496335::get_offset_of__maxDepthSet_26(),
	JsonSerializer_t1424496335::get_offset_of__checkAdditionalContent_27(),
	JsonSerializer_t1424496335::get_offset_of__dateFormatString_28(),
	JsonSerializer_t1424496335::get_offset_of__dateFormatStringSet_29(),
	JsonSerializer_t1424496335::get_offset_of_Error_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3084 = { sizeof (MissingMemberHandling_t4043300624)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3084[3] = 
{
	MissingMemberHandling_t4043300624::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3085 = { sizeof (NullValueHandling_t3402136445)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3085[3] = 
{
	NullValueHandling_t3402136445::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3086 = { sizeof (ReferenceLoopHandling_t1329130968)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3086[4] = 
{
	ReferenceLoopHandling_t1329130968::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3087 = { sizeof (TypeNameHandling_t3731987346)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3087[6] = 
{
	TypeNameHandling_t3731987346::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3088 = { sizeof (JsonToken_t1917433489)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3088[19] = 
{
	JsonToken_t1917433489::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3089 = { sizeof (JsonWriter_t1467272295), -1, sizeof(JsonWriter_t1467272295_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3089[13] = 
{
	JsonWriter_t1467272295_StaticFields::get_offset_of_StateArray_0(),
	JsonWriter_t1467272295_StaticFields::get_offset_of_StateArrayTempate_1(),
	JsonWriter_t1467272295::get_offset_of__stack_2(),
	JsonWriter_t1467272295::get_offset_of__currentPosition_3(),
	JsonWriter_t1467272295::get_offset_of__currentState_4(),
	JsonWriter_t1467272295::get_offset_of__formatting_5(),
	JsonWriter_t1467272295::get_offset_of_U3CCloseOutputU3Ek__BackingField_6(),
	JsonWriter_t1467272295::get_offset_of__dateFormatHandling_7(),
	JsonWriter_t1467272295::get_offset_of__dateTimeZoneHandling_8(),
	JsonWriter_t1467272295::get_offset_of__stringEscapeHandling_9(),
	JsonWriter_t1467272295::get_offset_of__floatFormatHandling_10(),
	JsonWriter_t1467272295::get_offset_of__dateFormatString_11(),
	JsonWriter_t1467272295::get_offset_of__culture_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3090 = { sizeof (State_t2595666649)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3090[11] = 
{
	State_t2595666649::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3091 = { sizeof (WriteState_t2626176463)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3091[8] = 
{
	WriteState_t2626176463::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3092 = { sizeof (ParserTimeZone_t1439545141)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3092[5] = 
{
	ParserTimeZone_t1439545141::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3093 = { sizeof (DateTimeParser_t3754458704)+ sizeof (RuntimeObject), sizeof(DateTimeParser_t3754458704_marshaled_pinvoke), sizeof(DateTimeParser_t3754458704_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3093[26] = 
{
	DateTimeParser_t3754458704::get_offset_of_Year_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t3754458704::get_offset_of_Month_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t3754458704::get_offset_of_Day_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t3754458704::get_offset_of_Hour_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t3754458704::get_offset_of_Minute_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t3754458704::get_offset_of_Second_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t3754458704::get_offset_of_Fraction_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t3754458704::get_offset_of_ZoneHour_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t3754458704::get_offset_of_ZoneMinute_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t3754458704::get_offset_of_Zone_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t3754458704::get_offset_of__text_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t3754458704::get_offset_of__end_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DateTimeParser_t3754458704_StaticFields::get_offset_of_Power10_12(),
	DateTimeParser_t3754458704_StaticFields::get_offset_of_Lzyyyy_13(),
	DateTimeParser_t3754458704_StaticFields::get_offset_of_Lzyyyy__14(),
	DateTimeParser_t3754458704_StaticFields::get_offset_of_Lzyyyy_MM_15(),
	DateTimeParser_t3754458704_StaticFields::get_offset_of_Lzyyyy_MM__16(),
	DateTimeParser_t3754458704_StaticFields::get_offset_of_Lzyyyy_MM_dd_17(),
	DateTimeParser_t3754458704_StaticFields::get_offset_of_Lzyyyy_MM_ddT_18(),
	DateTimeParser_t3754458704_StaticFields::get_offset_of_LzHH_19(),
	DateTimeParser_t3754458704_StaticFields::get_offset_of_LzHH__20(),
	DateTimeParser_t3754458704_StaticFields::get_offset_of_LzHH_mm_21(),
	DateTimeParser_t3754458704_StaticFields::get_offset_of_LzHH_mm__22(),
	DateTimeParser_t3754458704_StaticFields::get_offset_of_LzHH_mm_ss_23(),
	DateTimeParser_t3754458704_StaticFields::get_offset_of_Lz__24(),
	DateTimeParser_t3754458704_StaticFields::get_offset_of_Lz_zz_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3094 = { sizeof (Base64Encoder_t3724986751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3094[4] = 
{
	Base64Encoder_t3724986751::get_offset_of__charsLine_0(),
	Base64Encoder_t3724986751::get_offset_of__writer_1(),
	Base64Encoder_t3724986751::get_offset_of__leftOverBytes_2(),
	Base64Encoder_t3724986751::get_offset_of__leftOverBytesCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3095 = { sizeof (JsonTokenUtils_t2823043526), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3096 = { sizeof (PropertyNameTable_t4130830590), -1, sizeof(PropertyNameTable_t4130830590_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3096[4] = 
{
	PropertyNameTable_t4130830590_StaticFields::get_offset_of_HashCodeRandomizer_0(),
	PropertyNameTable_t4130830590::get_offset_of__count_1(),
	PropertyNameTable_t4130830590::get_offset_of__entries_2(),
	PropertyNameTable_t4130830590::get_offset_of__mask_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3097 = { sizeof (Entry_t2924091039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3097[3] = 
{
	Entry_t2924091039::get_offset_of_Value_0(),
	Entry_t2924091039::get_offset_of_HashCode_1(),
	Entry_t2924091039::get_offset_of_Next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3098 = { sizeof (ReflectionDelegateFactory_t2528576452), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3099 = { sizeof (LateBoundReflectionDelegateFactory_t925499913), -1, sizeof(LateBoundReflectionDelegateFactory_t925499913_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3099[1] = 
{
	LateBoundReflectionDelegateFactory_t925499913_StaticFields::get_offset_of__instance_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
