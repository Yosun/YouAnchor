﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// DG.Tweening.Tweener
struct Tweener_t436044680;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3235626157;
// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_t116211281;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Color>
struct DOGetter_1_t2298482528;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Color>
struct DOSetter_1_t3605794656;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct TweenerCore_3_t745532282;
// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_t2845094636;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_t2038525582;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>
struct DOGetter_1_t1899025727;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>
struct DOSetter_1_t3206337855;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t4006488229;
// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass8_0
struct U3CU3Ec__DisplayClass8_0_t1635241055;
// DG.Tweening.Core.DOGetter`1<System.Single>
struct DOGetter_1_t1140062978;
// DG.Tweening.Core.DOSetter`1<System.Single>
struct DOSetter_1_t2447375106;
// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct TweenerCore_3_t884556411;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// DG.Tweening.TweenCallback
struct TweenCallback_t3727756325;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// DG.Tweening.TweenCallback`1<System.Int32>
struct TweenCallback_1_t3009965658;
// DG.Tweening.EaseFunction
struct EaseFunction_t3531141372;
// System.Type
struct Type_t;
// DG.Tweening.Sequence
struct Sequence_t2050373119;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct ABSTweenPlugin_3_t420566061;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct ABSTweenPlugin_3_t281541932;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct ABSTweenPlugin_3_t3542497879;

extern RuntimeClass* U3CU3Ec__DisplayClass2_0_t116211281_il2cpp_TypeInfo_var;
extern RuntimeClass* DOGetter_1_t2298482528_il2cpp_TypeInfo_var;
extern RuntimeClass* DOSetter_1_t3605794656_il2cpp_TypeInfo_var;
extern RuntimeClass* DOTween_t2744875806_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__0_m3301202239_RuntimeMethod_var;
extern const RuntimeMethod* DOGetter_1__ctor_m1558284636_RuntimeMethod_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__1_m3918942505_RuntimeMethod_var;
extern const RuntimeMethod* DOSetter_1__ctor_m380718107_RuntimeMethod_var;
extern const RuntimeMethod* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t745532282_m2904780857_RuntimeMethod_var;
extern const uint32_t ShortcutExtensions43_DOColor_m2154457141_MetadataUsageId;
extern RuntimeClass* U3CU3Ec__DisplayClass3_0_t2845094636_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__0_m1573959735_RuntimeMethod_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__1_m2190249801_RuntimeMethod_var;
extern const RuntimeMethod* TweenSettingsExtensions_SetTarget_TisTweener_t436044680_m598017345_RuntimeMethod_var;
extern const uint32_t ShortcutExtensions43_DOFade_m836661547_MetadataUsageId;
extern RuntimeClass* U3CU3Ec__DisplayClass5_0_t2038525582_il2cpp_TypeInfo_var;
extern RuntimeClass* DOGetter_1_t1899025727_il2cpp_TypeInfo_var;
extern RuntimeClass* DOSetter_1_t3206337855_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass5_0_U3CDOMoveU3Eb__0_m2124110351_RuntimeMethod_var;
extern const RuntimeMethod* DOGetter_1__ctor_m3327751853_RuntimeMethod_var;
extern const RuntimeMethod* Rigidbody2D_MovePosition_m1934912072_RuntimeMethod_var;
extern const RuntimeMethod* DOSetter_1__ctor_m2986952954_RuntimeMethod_var;
extern const uint32_t ShortcutExtensions43_DOMove_m1054159185_MetadataUsageId;
extern RuntimeClass* U3CU3Ec__DisplayClass8_0_t1635241055_il2cpp_TypeInfo_var;
extern RuntimeClass* DOGetter_1_t1140062978_il2cpp_TypeInfo_var;
extern RuntimeClass* DOSetter_1_t2447375106_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass8_0_U3CDORotateU3Eb__0_m2165437160_RuntimeMethod_var;
extern const RuntimeMethod* DOGetter_1__ctor_m2335505072_RuntimeMethod_var;
extern const RuntimeMethod* Rigidbody2D_MoveRotation_m3032842781_RuntimeMethod_var;
extern const RuntimeMethod* DOSetter_1__ctor_m3423399374_RuntimeMethod_var;
extern const RuntimeMethod* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t884556411_m1318424630_RuntimeMethod_var;
extern const uint32_t ShortcutExtensions43_DORotate_m3965828009_MetadataUsageId;



#ifndef U3CMODULEU3E_T692745554_H
#define U3CMODULEU3E_T692745554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745554 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745554_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T2038525582_H
#define U3CU3EC__DISPLAYCLASS5_0_T2038525582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t2038525582  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.ShortcutExtensions43/<>c__DisplayClass5_0::target
	Rigidbody2D_t939494601 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t2038525582, ___target_0)); }
	inline Rigidbody2D_t939494601 * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_t939494601 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_t939494601 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T2038525582_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_T116211281_H
#define U3CU3EC__DISPLAYCLASS2_0_T116211281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t116211281  : public RuntimeObject
{
public:
	// UnityEngine.SpriteRenderer DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0::target
	SpriteRenderer_t3235626157 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t116211281, ___target_0)); }
	inline SpriteRenderer_t3235626157 * get_target_0() const { return ___target_0; }
	inline SpriteRenderer_t3235626157 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(SpriteRenderer_t3235626157 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_T116211281_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_T1635241055_H
#define U3CU3EC__DISPLAYCLASS8_0_T1635241055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_t1635241055  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.ShortcutExtensions43/<>c__DisplayClass8_0::target
	Rigidbody2D_t939494601 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t1635241055, ___target_0)); }
	inline Rigidbody2D_t939494601 * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_t939494601 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_t939494601 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_T1635241055_H
#ifndef SHORTCUTEXTENSIONS43_T1788548476_H
#define SHORTCUTEXTENSIONS43_T1788548476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions43
struct  ShortcutExtensions43_t1788548476  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHORTCUTEXTENSIONS43_T1788548476_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T2845094636_H
#define U3CU3EC__DISPLAYCLASS3_0_T2845094636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t2845094636  : public RuntimeObject
{
public:
	// UnityEngine.SpriteRenderer DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0::target
	SpriteRenderer_t3235626157 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t2845094636, ___target_0)); }
	inline SpriteRenderer_t3235626157 * get_target_0() const { return ___target_0; }
	inline SpriteRenderer_t3235626157 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(SpriteRenderer_t3235626157 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T2845094636_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef COLOROPTIONS_T1487297155_H
#define COLOROPTIONS_T1487297155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.ColorOptions
struct  ColorOptions_t1487297155 
{
public:
	// System.Boolean DG.Tweening.Plugins.Options.ColorOptions::alphaOnly
	bool ___alphaOnly_0;

public:
	inline static int32_t get_offset_of_alphaOnly_0() { return static_cast<int32_t>(offsetof(ColorOptions_t1487297155, ___alphaOnly_0)); }
	inline bool get_alphaOnly_0() const { return ___alphaOnly_0; }
	inline bool* get_address_of_alphaOnly_0() { return &___alphaOnly_0; }
	inline void set_alphaOnly_0(bool value)
	{
		___alphaOnly_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.ColorOptions
struct ColorOptions_t1487297155_marshaled_pinvoke
{
	int32_t ___alphaOnly_0;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.ColorOptions
struct ColorOptions_t1487297155_marshaled_com
{
	int32_t ___alphaOnly_0;
};
#endif // COLOROPTIONS_T1487297155_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef FLOATOPTIONS_T1203667100_H
#define FLOATOPTIONS_T1203667100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.FloatOptions
struct  FloatOptions_t1203667100 
{
public:
	// System.Boolean DG.Tweening.Plugins.Options.FloatOptions::snapping
	bool ___snapping_0;

public:
	inline static int32_t get_offset_of_snapping_0() { return static_cast<int32_t>(offsetof(FloatOptions_t1203667100, ___snapping_0)); }
	inline bool get_snapping_0() const { return ___snapping_0; }
	inline bool* get_address_of_snapping_0() { return &___snapping_0; }
	inline void set_snapping_0(bool value)
	{
		___snapping_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.FloatOptions
struct FloatOptions_t1203667100_marshaled_pinvoke
{
	int32_t ___snapping_0;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.FloatOptions
struct FloatOptions_t1203667100_marshaled_com
{
	int32_t ___snapping_0;
};
#endif // FLOATOPTIONS_T1203667100_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef TWEENTYPE_T1971673186_H
#define TWEENTYPE_T1971673186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.TweenType
struct  TweenType_t1971673186 
{
public:
	// System.Int32 DG.Tweening.TweenType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TweenType_t1971673186, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENTYPE_T1971673186_H
#ifndef UPDATETYPE_T3937729206_H
#define UPDATETYPE_T3937729206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.UpdateType
struct  UpdateType_t3937729206 
{
public:
	// System.Int32 DG.Tweening.UpdateType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdateType_t3937729206, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATETYPE_T3937729206_H
#ifndef LOOPTYPE_T3049802818_H
#define LOOPTYPE_T3049802818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LoopType
struct  LoopType_t3049802818 
{
public:
	// System.Int32 DG.Tweening.LoopType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoopType_t3049802818, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T3049802818_H
#ifndef AXISCONSTRAINT_T2771958344_H
#define AXISCONSTRAINT_T2771958344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.AxisConstraint
struct  AxisConstraint_t2771958344 
{
public:
	// System.Int32 DG.Tweening.AxisConstraint::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisConstraint_t2771958344, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCONSTRAINT_T2771958344_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef EASE_T4010715394_H
#define EASE_T4010715394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Ease
struct  Ease_t4010715394 
{
public:
	// System.Int32 DG.Tweening.Ease::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Ease_t4010715394, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASE_T4010715394_H
#ifndef SPECIALSTARTUPMODE_T1644068939_H
#define SPECIALSTARTUPMODE_T1644068939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Enums.SpecialStartupMode
struct  SpecialStartupMode_t1644068939 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.SpecialStartupMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SpecialStartupMode_t1644068939, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALSTARTUPMODE_T1644068939_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef VECTOROPTIONS_T1354903650_H
#define VECTOROPTIONS_T1354903650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.VectorOptions
struct  VectorOptions_t1354903650 
{
public:
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.VectorOptions::axisConstraint
	int32_t ___axisConstraint_0;
	// System.Boolean DG.Tweening.Plugins.Options.VectorOptions::snapping
	bool ___snapping_1;

public:
	inline static int32_t get_offset_of_axisConstraint_0() { return static_cast<int32_t>(offsetof(VectorOptions_t1354903650, ___axisConstraint_0)); }
	inline int32_t get_axisConstraint_0() const { return ___axisConstraint_0; }
	inline int32_t* get_address_of_axisConstraint_0() { return &___axisConstraint_0; }
	inline void set_axisConstraint_0(int32_t value)
	{
		___axisConstraint_0 = value;
	}

	inline static int32_t get_offset_of_snapping_1() { return static_cast<int32_t>(offsetof(VectorOptions_t1354903650, ___snapping_1)); }
	inline bool get_snapping_1() const { return ___snapping_1; }
	inline bool* get_address_of_snapping_1() { return &___snapping_1; }
	inline void set_snapping_1(bool value)
	{
		___snapping_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.VectorOptions
struct VectorOptions_t1354903650_marshaled_pinvoke
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.VectorOptions
struct VectorOptions_t1354903650_marshaled_com
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
};
#endif // VECTOROPTIONS_T1354903650_H
#ifndef ABSSEQUENTIABLE_T3376041011_H
#define ABSSEQUENTIABLE_T3376041011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.ABSSequentiable
struct  ABSSequentiable_t3376041011  : public RuntimeObject
{
public:
	// DG.Tweening.TweenType DG.Tweening.Core.ABSSequentiable::tweenType
	int32_t ___tweenType_0;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedPosition
	float ___sequencedPosition_1;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedEndPosition
	float ___sequencedEndPosition_2;
	// DG.Tweening.TweenCallback DG.Tweening.Core.ABSSequentiable::onStart
	TweenCallback_t3727756325 * ___onStart_3;

public:
	inline static int32_t get_offset_of_tweenType_0() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___tweenType_0)); }
	inline int32_t get_tweenType_0() const { return ___tweenType_0; }
	inline int32_t* get_address_of_tweenType_0() { return &___tweenType_0; }
	inline void set_tweenType_0(int32_t value)
	{
		___tweenType_0 = value;
	}

	inline static int32_t get_offset_of_sequencedPosition_1() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___sequencedPosition_1)); }
	inline float get_sequencedPosition_1() const { return ___sequencedPosition_1; }
	inline float* get_address_of_sequencedPosition_1() { return &___sequencedPosition_1; }
	inline void set_sequencedPosition_1(float value)
	{
		___sequencedPosition_1 = value;
	}

	inline static int32_t get_offset_of_sequencedEndPosition_2() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___sequencedEndPosition_2)); }
	inline float get_sequencedEndPosition_2() const { return ___sequencedEndPosition_2; }
	inline float* get_address_of_sequencedEndPosition_2() { return &___sequencedEndPosition_2; }
	inline void set_sequencedEndPosition_2(float value)
	{
		___sequencedEndPosition_2 = value;
	}

	inline static int32_t get_offset_of_onStart_3() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___onStart_3)); }
	inline TweenCallback_t3727756325 * get_onStart_3() const { return ___onStart_3; }
	inline TweenCallback_t3727756325 ** get_address_of_onStart_3() { return &___onStart_3; }
	inline void set_onStart_3(TweenCallback_t3727756325 * value)
	{
		___onStart_3 = value;
		Il2CppCodeGenWriteBarrier((&___onStart_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSSEQUENTIABLE_T3376041011_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef RIGIDBODY2D_T939494601_H
#define RIGIDBODY2D_T939494601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody2D
struct  Rigidbody2D_t939494601  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY2D_T939494601_H
#ifndef DOSETTER_1_T3605794656_H
#define DOSETTER_1_T3605794656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOSetter`1<UnityEngine.Color>
struct  DOSetter_1_t3605794656  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOSETTER_1_T3605794656_H
#ifndef DOGETTER_1_T2298482528_H
#define DOGETTER_1_T2298482528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOGetter`1<UnityEngine.Color>
struct  DOGetter_1_t2298482528  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOGETTER_1_T2298482528_H
#ifndef DOGETTER_1_T1899025727_H
#define DOGETTER_1_T1899025727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>
struct  DOGetter_1_t1899025727  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOGETTER_1_T1899025727_H
#ifndef DOSETTER_1_T3206337855_H
#define DOSETTER_1_T3206337855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>
struct  DOSetter_1_t3206337855  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOSETTER_1_T3206337855_H
#ifndef DOGETTER_1_T1140062978_H
#define DOGETTER_1_T1140062978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOGetter`1<System.Single>
struct  DOGetter_1_t1140062978  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOGETTER_1_T1140062978_H
#ifndef TWEEN_T2342918553_H
#define TWEEN_T2342918553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Tween
struct  Tween_t2342918553  : public ABSSequentiable_t3376041011
{
public:
	// System.Single DG.Tweening.Tween::timeScale
	float ___timeScale_4;
	// System.Boolean DG.Tweening.Tween::isBackwards
	bool ___isBackwards_5;
	// System.Object DG.Tweening.Tween::id
	RuntimeObject * ___id_6;
	// System.Object DG.Tweening.Tween::target
	RuntimeObject * ___target_7;
	// DG.Tweening.UpdateType DG.Tweening.Tween::updateType
	int32_t ___updateType_8;
	// System.Boolean DG.Tweening.Tween::isIndependentUpdate
	bool ___isIndependentUpdate_9;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPlay
	TweenCallback_t3727756325 * ___onPlay_10;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPause
	TweenCallback_t3727756325 * ___onPause_11;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onRewind
	TweenCallback_t3727756325 * ___onRewind_12;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onUpdate
	TweenCallback_t3727756325 * ___onUpdate_13;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onStepComplete
	TweenCallback_t3727756325 * ___onStepComplete_14;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onComplete
	TweenCallback_t3727756325 * ___onComplete_15;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onKill
	TweenCallback_t3727756325 * ___onKill_16;
	// DG.Tweening.TweenCallback`1<System.Int32> DG.Tweening.Tween::onWaypointChange
	TweenCallback_1_t3009965658 * ___onWaypointChange_17;
	// System.Boolean DG.Tweening.Tween::isFrom
	bool ___isFrom_18;
	// System.Boolean DG.Tweening.Tween::isBlendable
	bool ___isBlendable_19;
	// System.Boolean DG.Tweening.Tween::isRecyclable
	bool ___isRecyclable_20;
	// System.Boolean DG.Tweening.Tween::isSpeedBased
	bool ___isSpeedBased_21;
	// System.Boolean DG.Tweening.Tween::autoKill
	bool ___autoKill_22;
	// System.Single DG.Tweening.Tween::duration
	float ___duration_23;
	// System.Int32 DG.Tweening.Tween::loops
	int32_t ___loops_24;
	// DG.Tweening.LoopType DG.Tweening.Tween::loopType
	int32_t ___loopType_25;
	// System.Single DG.Tweening.Tween::delay
	float ___delay_26;
	// System.Boolean DG.Tweening.Tween::isRelative
	bool ___isRelative_27;
	// DG.Tweening.Ease DG.Tweening.Tween::easeType
	int32_t ___easeType_28;
	// DG.Tweening.EaseFunction DG.Tweening.Tween::customEase
	EaseFunction_t3531141372 * ___customEase_29;
	// System.Single DG.Tweening.Tween::easeOvershootOrAmplitude
	float ___easeOvershootOrAmplitude_30;
	// System.Single DG.Tweening.Tween::easePeriod
	float ___easePeriod_31;
	// System.Type DG.Tweening.Tween::typeofT1
	Type_t * ___typeofT1_32;
	// System.Type DG.Tweening.Tween::typeofT2
	Type_t * ___typeofT2_33;
	// System.Type DG.Tweening.Tween::typeofTPlugOptions
	Type_t * ___typeofTPlugOptions_34;
	// System.Boolean DG.Tweening.Tween::active
	bool ___active_35;
	// System.Boolean DG.Tweening.Tween::isSequenced
	bool ___isSequenced_36;
	// DG.Tweening.Sequence DG.Tweening.Tween::sequenceParent
	Sequence_t2050373119 * ___sequenceParent_37;
	// System.Int32 DG.Tweening.Tween::activeId
	int32_t ___activeId_38;
	// DG.Tweening.Core.Enums.SpecialStartupMode DG.Tweening.Tween::specialStartupMode
	int32_t ___specialStartupMode_39;
	// System.Boolean DG.Tweening.Tween::creationLocked
	bool ___creationLocked_40;
	// System.Boolean DG.Tweening.Tween::startupDone
	bool ___startupDone_41;
	// System.Boolean DG.Tweening.Tween::playedOnce
	bool ___playedOnce_42;
	// System.Single DG.Tweening.Tween::position
	float ___position_43;
	// System.Single DG.Tweening.Tween::fullDuration
	float ___fullDuration_44;
	// System.Int32 DG.Tweening.Tween::completedLoops
	int32_t ___completedLoops_45;
	// System.Boolean DG.Tweening.Tween::isPlaying
	bool ___isPlaying_46;
	// System.Boolean DG.Tweening.Tween::isComplete
	bool ___isComplete_47;
	// System.Single DG.Tweening.Tween::elapsedDelay
	float ___elapsedDelay_48;
	// System.Boolean DG.Tweening.Tween::delayComplete
	bool ___delayComplete_49;
	// System.Int32 DG.Tweening.Tween::miscInt
	int32_t ___miscInt_50;

public:
	inline static int32_t get_offset_of_timeScale_4() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___timeScale_4)); }
	inline float get_timeScale_4() const { return ___timeScale_4; }
	inline float* get_address_of_timeScale_4() { return &___timeScale_4; }
	inline void set_timeScale_4(float value)
	{
		___timeScale_4 = value;
	}

	inline static int32_t get_offset_of_isBackwards_5() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isBackwards_5)); }
	inline bool get_isBackwards_5() const { return ___isBackwards_5; }
	inline bool* get_address_of_isBackwards_5() { return &___isBackwards_5; }
	inline void set_isBackwards_5(bool value)
	{
		___isBackwards_5 = value;
	}

	inline static int32_t get_offset_of_id_6() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___id_6)); }
	inline RuntimeObject * get_id_6() const { return ___id_6; }
	inline RuntimeObject ** get_address_of_id_6() { return &___id_6; }
	inline void set_id_6(RuntimeObject * value)
	{
		___id_6 = value;
		Il2CppCodeGenWriteBarrier((&___id_6), value);
	}

	inline static int32_t get_offset_of_target_7() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___target_7)); }
	inline RuntimeObject * get_target_7() const { return ___target_7; }
	inline RuntimeObject ** get_address_of_target_7() { return &___target_7; }
	inline void set_target_7(RuntimeObject * value)
	{
		___target_7 = value;
		Il2CppCodeGenWriteBarrier((&___target_7), value);
	}

	inline static int32_t get_offset_of_updateType_8() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___updateType_8)); }
	inline int32_t get_updateType_8() const { return ___updateType_8; }
	inline int32_t* get_address_of_updateType_8() { return &___updateType_8; }
	inline void set_updateType_8(int32_t value)
	{
		___updateType_8 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_9() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isIndependentUpdate_9)); }
	inline bool get_isIndependentUpdate_9() const { return ___isIndependentUpdate_9; }
	inline bool* get_address_of_isIndependentUpdate_9() { return &___isIndependentUpdate_9; }
	inline void set_isIndependentUpdate_9(bool value)
	{
		___isIndependentUpdate_9 = value;
	}

	inline static int32_t get_offset_of_onPlay_10() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onPlay_10)); }
	inline TweenCallback_t3727756325 * get_onPlay_10() const { return ___onPlay_10; }
	inline TweenCallback_t3727756325 ** get_address_of_onPlay_10() { return &___onPlay_10; }
	inline void set_onPlay_10(TweenCallback_t3727756325 * value)
	{
		___onPlay_10 = value;
		Il2CppCodeGenWriteBarrier((&___onPlay_10), value);
	}

	inline static int32_t get_offset_of_onPause_11() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onPause_11)); }
	inline TweenCallback_t3727756325 * get_onPause_11() const { return ___onPause_11; }
	inline TweenCallback_t3727756325 ** get_address_of_onPause_11() { return &___onPause_11; }
	inline void set_onPause_11(TweenCallback_t3727756325 * value)
	{
		___onPause_11 = value;
		Il2CppCodeGenWriteBarrier((&___onPause_11), value);
	}

	inline static int32_t get_offset_of_onRewind_12() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onRewind_12)); }
	inline TweenCallback_t3727756325 * get_onRewind_12() const { return ___onRewind_12; }
	inline TweenCallback_t3727756325 ** get_address_of_onRewind_12() { return &___onRewind_12; }
	inline void set_onRewind_12(TweenCallback_t3727756325 * value)
	{
		___onRewind_12 = value;
		Il2CppCodeGenWriteBarrier((&___onRewind_12), value);
	}

	inline static int32_t get_offset_of_onUpdate_13() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onUpdate_13)); }
	inline TweenCallback_t3727756325 * get_onUpdate_13() const { return ___onUpdate_13; }
	inline TweenCallback_t3727756325 ** get_address_of_onUpdate_13() { return &___onUpdate_13; }
	inline void set_onUpdate_13(TweenCallback_t3727756325 * value)
	{
		___onUpdate_13 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdate_13), value);
	}

	inline static int32_t get_offset_of_onStepComplete_14() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onStepComplete_14)); }
	inline TweenCallback_t3727756325 * get_onStepComplete_14() const { return ___onStepComplete_14; }
	inline TweenCallback_t3727756325 ** get_address_of_onStepComplete_14() { return &___onStepComplete_14; }
	inline void set_onStepComplete_14(TweenCallback_t3727756325 * value)
	{
		___onStepComplete_14 = value;
		Il2CppCodeGenWriteBarrier((&___onStepComplete_14), value);
	}

	inline static int32_t get_offset_of_onComplete_15() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onComplete_15)); }
	inline TweenCallback_t3727756325 * get_onComplete_15() const { return ___onComplete_15; }
	inline TweenCallback_t3727756325 ** get_address_of_onComplete_15() { return &___onComplete_15; }
	inline void set_onComplete_15(TweenCallback_t3727756325 * value)
	{
		___onComplete_15 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_15), value);
	}

	inline static int32_t get_offset_of_onKill_16() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onKill_16)); }
	inline TweenCallback_t3727756325 * get_onKill_16() const { return ___onKill_16; }
	inline TweenCallback_t3727756325 ** get_address_of_onKill_16() { return &___onKill_16; }
	inline void set_onKill_16(TweenCallback_t3727756325 * value)
	{
		___onKill_16 = value;
		Il2CppCodeGenWriteBarrier((&___onKill_16), value);
	}

	inline static int32_t get_offset_of_onWaypointChange_17() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onWaypointChange_17)); }
	inline TweenCallback_1_t3009965658 * get_onWaypointChange_17() const { return ___onWaypointChange_17; }
	inline TweenCallback_1_t3009965658 ** get_address_of_onWaypointChange_17() { return &___onWaypointChange_17; }
	inline void set_onWaypointChange_17(TweenCallback_1_t3009965658 * value)
	{
		___onWaypointChange_17 = value;
		Il2CppCodeGenWriteBarrier((&___onWaypointChange_17), value);
	}

	inline static int32_t get_offset_of_isFrom_18() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isFrom_18)); }
	inline bool get_isFrom_18() const { return ___isFrom_18; }
	inline bool* get_address_of_isFrom_18() { return &___isFrom_18; }
	inline void set_isFrom_18(bool value)
	{
		___isFrom_18 = value;
	}

	inline static int32_t get_offset_of_isBlendable_19() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isBlendable_19)); }
	inline bool get_isBlendable_19() const { return ___isBlendable_19; }
	inline bool* get_address_of_isBlendable_19() { return &___isBlendable_19; }
	inline void set_isBlendable_19(bool value)
	{
		___isBlendable_19 = value;
	}

	inline static int32_t get_offset_of_isRecyclable_20() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isRecyclable_20)); }
	inline bool get_isRecyclable_20() const { return ___isRecyclable_20; }
	inline bool* get_address_of_isRecyclable_20() { return &___isRecyclable_20; }
	inline void set_isRecyclable_20(bool value)
	{
		___isRecyclable_20 = value;
	}

	inline static int32_t get_offset_of_isSpeedBased_21() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isSpeedBased_21)); }
	inline bool get_isSpeedBased_21() const { return ___isSpeedBased_21; }
	inline bool* get_address_of_isSpeedBased_21() { return &___isSpeedBased_21; }
	inline void set_isSpeedBased_21(bool value)
	{
		___isSpeedBased_21 = value;
	}

	inline static int32_t get_offset_of_autoKill_22() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___autoKill_22)); }
	inline bool get_autoKill_22() const { return ___autoKill_22; }
	inline bool* get_address_of_autoKill_22() { return &___autoKill_22; }
	inline void set_autoKill_22(bool value)
	{
		___autoKill_22 = value;
	}

	inline static int32_t get_offset_of_duration_23() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___duration_23)); }
	inline float get_duration_23() const { return ___duration_23; }
	inline float* get_address_of_duration_23() { return &___duration_23; }
	inline void set_duration_23(float value)
	{
		___duration_23 = value;
	}

	inline static int32_t get_offset_of_loops_24() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___loops_24)); }
	inline int32_t get_loops_24() const { return ___loops_24; }
	inline int32_t* get_address_of_loops_24() { return &___loops_24; }
	inline void set_loops_24(int32_t value)
	{
		___loops_24 = value;
	}

	inline static int32_t get_offset_of_loopType_25() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___loopType_25)); }
	inline int32_t get_loopType_25() const { return ___loopType_25; }
	inline int32_t* get_address_of_loopType_25() { return &___loopType_25; }
	inline void set_loopType_25(int32_t value)
	{
		___loopType_25 = value;
	}

	inline static int32_t get_offset_of_delay_26() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___delay_26)); }
	inline float get_delay_26() const { return ___delay_26; }
	inline float* get_address_of_delay_26() { return &___delay_26; }
	inline void set_delay_26(float value)
	{
		___delay_26 = value;
	}

	inline static int32_t get_offset_of_isRelative_27() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isRelative_27)); }
	inline bool get_isRelative_27() const { return ___isRelative_27; }
	inline bool* get_address_of_isRelative_27() { return &___isRelative_27; }
	inline void set_isRelative_27(bool value)
	{
		___isRelative_27 = value;
	}

	inline static int32_t get_offset_of_easeType_28() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___easeType_28)); }
	inline int32_t get_easeType_28() const { return ___easeType_28; }
	inline int32_t* get_address_of_easeType_28() { return &___easeType_28; }
	inline void set_easeType_28(int32_t value)
	{
		___easeType_28 = value;
	}

	inline static int32_t get_offset_of_customEase_29() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___customEase_29)); }
	inline EaseFunction_t3531141372 * get_customEase_29() const { return ___customEase_29; }
	inline EaseFunction_t3531141372 ** get_address_of_customEase_29() { return &___customEase_29; }
	inline void set_customEase_29(EaseFunction_t3531141372 * value)
	{
		___customEase_29 = value;
		Il2CppCodeGenWriteBarrier((&___customEase_29), value);
	}

	inline static int32_t get_offset_of_easeOvershootOrAmplitude_30() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___easeOvershootOrAmplitude_30)); }
	inline float get_easeOvershootOrAmplitude_30() const { return ___easeOvershootOrAmplitude_30; }
	inline float* get_address_of_easeOvershootOrAmplitude_30() { return &___easeOvershootOrAmplitude_30; }
	inline void set_easeOvershootOrAmplitude_30(float value)
	{
		___easeOvershootOrAmplitude_30 = value;
	}

	inline static int32_t get_offset_of_easePeriod_31() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___easePeriod_31)); }
	inline float get_easePeriod_31() const { return ___easePeriod_31; }
	inline float* get_address_of_easePeriod_31() { return &___easePeriod_31; }
	inline void set_easePeriod_31(float value)
	{
		___easePeriod_31 = value;
	}

	inline static int32_t get_offset_of_typeofT1_32() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___typeofT1_32)); }
	inline Type_t * get_typeofT1_32() const { return ___typeofT1_32; }
	inline Type_t ** get_address_of_typeofT1_32() { return &___typeofT1_32; }
	inline void set_typeofT1_32(Type_t * value)
	{
		___typeofT1_32 = value;
		Il2CppCodeGenWriteBarrier((&___typeofT1_32), value);
	}

	inline static int32_t get_offset_of_typeofT2_33() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___typeofT2_33)); }
	inline Type_t * get_typeofT2_33() const { return ___typeofT2_33; }
	inline Type_t ** get_address_of_typeofT2_33() { return &___typeofT2_33; }
	inline void set_typeofT2_33(Type_t * value)
	{
		___typeofT2_33 = value;
		Il2CppCodeGenWriteBarrier((&___typeofT2_33), value);
	}

	inline static int32_t get_offset_of_typeofTPlugOptions_34() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___typeofTPlugOptions_34)); }
	inline Type_t * get_typeofTPlugOptions_34() const { return ___typeofTPlugOptions_34; }
	inline Type_t ** get_address_of_typeofTPlugOptions_34() { return &___typeofTPlugOptions_34; }
	inline void set_typeofTPlugOptions_34(Type_t * value)
	{
		___typeofTPlugOptions_34 = value;
		Il2CppCodeGenWriteBarrier((&___typeofTPlugOptions_34), value);
	}

	inline static int32_t get_offset_of_active_35() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___active_35)); }
	inline bool get_active_35() const { return ___active_35; }
	inline bool* get_address_of_active_35() { return &___active_35; }
	inline void set_active_35(bool value)
	{
		___active_35 = value;
	}

	inline static int32_t get_offset_of_isSequenced_36() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isSequenced_36)); }
	inline bool get_isSequenced_36() const { return ___isSequenced_36; }
	inline bool* get_address_of_isSequenced_36() { return &___isSequenced_36; }
	inline void set_isSequenced_36(bool value)
	{
		___isSequenced_36 = value;
	}

	inline static int32_t get_offset_of_sequenceParent_37() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___sequenceParent_37)); }
	inline Sequence_t2050373119 * get_sequenceParent_37() const { return ___sequenceParent_37; }
	inline Sequence_t2050373119 ** get_address_of_sequenceParent_37() { return &___sequenceParent_37; }
	inline void set_sequenceParent_37(Sequence_t2050373119 * value)
	{
		___sequenceParent_37 = value;
		Il2CppCodeGenWriteBarrier((&___sequenceParent_37), value);
	}

	inline static int32_t get_offset_of_activeId_38() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___activeId_38)); }
	inline int32_t get_activeId_38() const { return ___activeId_38; }
	inline int32_t* get_address_of_activeId_38() { return &___activeId_38; }
	inline void set_activeId_38(int32_t value)
	{
		___activeId_38 = value;
	}

	inline static int32_t get_offset_of_specialStartupMode_39() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___specialStartupMode_39)); }
	inline int32_t get_specialStartupMode_39() const { return ___specialStartupMode_39; }
	inline int32_t* get_address_of_specialStartupMode_39() { return &___specialStartupMode_39; }
	inline void set_specialStartupMode_39(int32_t value)
	{
		___specialStartupMode_39 = value;
	}

	inline static int32_t get_offset_of_creationLocked_40() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___creationLocked_40)); }
	inline bool get_creationLocked_40() const { return ___creationLocked_40; }
	inline bool* get_address_of_creationLocked_40() { return &___creationLocked_40; }
	inline void set_creationLocked_40(bool value)
	{
		___creationLocked_40 = value;
	}

	inline static int32_t get_offset_of_startupDone_41() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___startupDone_41)); }
	inline bool get_startupDone_41() const { return ___startupDone_41; }
	inline bool* get_address_of_startupDone_41() { return &___startupDone_41; }
	inline void set_startupDone_41(bool value)
	{
		___startupDone_41 = value;
	}

	inline static int32_t get_offset_of_playedOnce_42() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___playedOnce_42)); }
	inline bool get_playedOnce_42() const { return ___playedOnce_42; }
	inline bool* get_address_of_playedOnce_42() { return &___playedOnce_42; }
	inline void set_playedOnce_42(bool value)
	{
		___playedOnce_42 = value;
	}

	inline static int32_t get_offset_of_position_43() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___position_43)); }
	inline float get_position_43() const { return ___position_43; }
	inline float* get_address_of_position_43() { return &___position_43; }
	inline void set_position_43(float value)
	{
		___position_43 = value;
	}

	inline static int32_t get_offset_of_fullDuration_44() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___fullDuration_44)); }
	inline float get_fullDuration_44() const { return ___fullDuration_44; }
	inline float* get_address_of_fullDuration_44() { return &___fullDuration_44; }
	inline void set_fullDuration_44(float value)
	{
		___fullDuration_44 = value;
	}

	inline static int32_t get_offset_of_completedLoops_45() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___completedLoops_45)); }
	inline int32_t get_completedLoops_45() const { return ___completedLoops_45; }
	inline int32_t* get_address_of_completedLoops_45() { return &___completedLoops_45; }
	inline void set_completedLoops_45(int32_t value)
	{
		___completedLoops_45 = value;
	}

	inline static int32_t get_offset_of_isPlaying_46() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isPlaying_46)); }
	inline bool get_isPlaying_46() const { return ___isPlaying_46; }
	inline bool* get_address_of_isPlaying_46() { return &___isPlaying_46; }
	inline void set_isPlaying_46(bool value)
	{
		___isPlaying_46 = value;
	}

	inline static int32_t get_offset_of_isComplete_47() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isComplete_47)); }
	inline bool get_isComplete_47() const { return ___isComplete_47; }
	inline bool* get_address_of_isComplete_47() { return &___isComplete_47; }
	inline void set_isComplete_47(bool value)
	{
		___isComplete_47 = value;
	}

	inline static int32_t get_offset_of_elapsedDelay_48() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___elapsedDelay_48)); }
	inline float get_elapsedDelay_48() const { return ___elapsedDelay_48; }
	inline float* get_address_of_elapsedDelay_48() { return &___elapsedDelay_48; }
	inline void set_elapsedDelay_48(float value)
	{
		___elapsedDelay_48 = value;
	}

	inline static int32_t get_offset_of_delayComplete_49() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___delayComplete_49)); }
	inline bool get_delayComplete_49() const { return ___delayComplete_49; }
	inline bool* get_address_of_delayComplete_49() { return &___delayComplete_49; }
	inline void set_delayComplete_49(bool value)
	{
		___delayComplete_49 = value;
	}

	inline static int32_t get_offset_of_miscInt_50() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___miscInt_50)); }
	inline int32_t get_miscInt_50() const { return ___miscInt_50; }
	inline int32_t* get_address_of_miscInt_50() { return &___miscInt_50; }
	inline void set_miscInt_50(int32_t value)
	{
		___miscInt_50 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEEN_T2342918553_H
#ifndef RENDERER_T2627027031_H
#define RENDERER_T2627027031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t2627027031  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T2627027031_H
#ifndef DOSETTER_1_T2447375106_H
#define DOSETTER_1_T2447375106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOSetter`1<System.Single>
struct  DOSetter_1_t2447375106  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOSETTER_1_T2447375106_H
#ifndef SPRITERENDERER_T3235626157_H
#define SPRITERENDERER_T3235626157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpriteRenderer
struct  SpriteRenderer_t3235626157  : public Renderer_t2627027031
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITERENDERER_T3235626157_H
#ifndef TWEENER_T436044680_H
#define TWEENER_T436044680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Tweener
struct  Tweener_t436044680  : public Tween_t2342918553
{
public:
	// System.Boolean DG.Tweening.Tweener::hasManuallySetStartValue
	bool ___hasManuallySetStartValue_51;
	// System.Boolean DG.Tweening.Tweener::isFromAllowed
	bool ___isFromAllowed_52;

public:
	inline static int32_t get_offset_of_hasManuallySetStartValue_51() { return static_cast<int32_t>(offsetof(Tweener_t436044680, ___hasManuallySetStartValue_51)); }
	inline bool get_hasManuallySetStartValue_51() const { return ___hasManuallySetStartValue_51; }
	inline bool* get_address_of_hasManuallySetStartValue_51() { return &___hasManuallySetStartValue_51; }
	inline void set_hasManuallySetStartValue_51(bool value)
	{
		___hasManuallySetStartValue_51 = value;
	}

	inline static int32_t get_offset_of_isFromAllowed_52() { return static_cast<int32_t>(offsetof(Tweener_t436044680, ___isFromAllowed_52)); }
	inline bool get_isFromAllowed_52() const { return ___isFromAllowed_52; }
	inline bool* get_address_of_isFromAllowed_52() { return &___isFromAllowed_52; }
	inline void set_isFromAllowed_52(bool value)
	{
		___isFromAllowed_52 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENER_T436044680_H
#ifndef TWEENERCORE_3_T884556411_H
#define TWEENERCORE_3_T884556411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct  TweenerCore_3_t884556411  : public Tweener_t436044680
{
public:
	// T2 DG.Tweening.Core.TweenerCore`3::startValue
	float ___startValue_53;
	// T2 DG.Tweening.Core.TweenerCore`3::endValue
	float ___endValue_54;
	// T2 DG.Tweening.Core.TweenerCore`3::changeValue
	float ___changeValue_55;
	// TPlugOptions DG.Tweening.Core.TweenerCore`3::plugOptions
	FloatOptions_t1203667100  ___plugOptions_56;
	// DG.Tweening.Core.DOGetter`1<T1> DG.Tweening.Core.TweenerCore`3::getter
	DOGetter_1_t1140062978 * ___getter_57;
	// DG.Tweening.Core.DOSetter`1<T1> DG.Tweening.Core.TweenerCore`3::setter
	DOSetter_1_t2447375106 * ___setter_58;
	// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions> DG.Tweening.Core.TweenerCore`3::tweenPlugin
	ABSTweenPlugin_3_t420566061 * ___tweenPlugin_59;

public:
	inline static int32_t get_offset_of_startValue_53() { return static_cast<int32_t>(offsetof(TweenerCore_3_t884556411, ___startValue_53)); }
	inline float get_startValue_53() const { return ___startValue_53; }
	inline float* get_address_of_startValue_53() { return &___startValue_53; }
	inline void set_startValue_53(float value)
	{
		___startValue_53 = value;
	}

	inline static int32_t get_offset_of_endValue_54() { return static_cast<int32_t>(offsetof(TweenerCore_3_t884556411, ___endValue_54)); }
	inline float get_endValue_54() const { return ___endValue_54; }
	inline float* get_address_of_endValue_54() { return &___endValue_54; }
	inline void set_endValue_54(float value)
	{
		___endValue_54 = value;
	}

	inline static int32_t get_offset_of_changeValue_55() { return static_cast<int32_t>(offsetof(TweenerCore_3_t884556411, ___changeValue_55)); }
	inline float get_changeValue_55() const { return ___changeValue_55; }
	inline float* get_address_of_changeValue_55() { return &___changeValue_55; }
	inline void set_changeValue_55(float value)
	{
		___changeValue_55 = value;
	}

	inline static int32_t get_offset_of_plugOptions_56() { return static_cast<int32_t>(offsetof(TweenerCore_3_t884556411, ___plugOptions_56)); }
	inline FloatOptions_t1203667100  get_plugOptions_56() const { return ___plugOptions_56; }
	inline FloatOptions_t1203667100 * get_address_of_plugOptions_56() { return &___plugOptions_56; }
	inline void set_plugOptions_56(FloatOptions_t1203667100  value)
	{
		___plugOptions_56 = value;
	}

	inline static int32_t get_offset_of_getter_57() { return static_cast<int32_t>(offsetof(TweenerCore_3_t884556411, ___getter_57)); }
	inline DOGetter_1_t1140062978 * get_getter_57() const { return ___getter_57; }
	inline DOGetter_1_t1140062978 ** get_address_of_getter_57() { return &___getter_57; }
	inline void set_getter_57(DOGetter_1_t1140062978 * value)
	{
		___getter_57 = value;
		Il2CppCodeGenWriteBarrier((&___getter_57), value);
	}

	inline static int32_t get_offset_of_setter_58() { return static_cast<int32_t>(offsetof(TweenerCore_3_t884556411, ___setter_58)); }
	inline DOSetter_1_t2447375106 * get_setter_58() const { return ___setter_58; }
	inline DOSetter_1_t2447375106 ** get_address_of_setter_58() { return &___setter_58; }
	inline void set_setter_58(DOSetter_1_t2447375106 * value)
	{
		___setter_58 = value;
		Il2CppCodeGenWriteBarrier((&___setter_58), value);
	}

	inline static int32_t get_offset_of_tweenPlugin_59() { return static_cast<int32_t>(offsetof(TweenerCore_3_t884556411, ___tweenPlugin_59)); }
	inline ABSTweenPlugin_3_t420566061 * get_tweenPlugin_59() const { return ___tweenPlugin_59; }
	inline ABSTweenPlugin_3_t420566061 ** get_address_of_tweenPlugin_59() { return &___tweenPlugin_59; }
	inline void set_tweenPlugin_59(ABSTweenPlugin_3_t420566061 * value)
	{
		___tweenPlugin_59 = value;
		Il2CppCodeGenWriteBarrier((&___tweenPlugin_59), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENERCORE_3_T884556411_H
#ifndef TWEENERCORE_3_T745532282_H
#define TWEENERCORE_3_T745532282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct  TweenerCore_3_t745532282  : public Tweener_t436044680
{
public:
	// T2 DG.Tweening.Core.TweenerCore`3::startValue
	Color_t2555686324  ___startValue_53;
	// T2 DG.Tweening.Core.TweenerCore`3::endValue
	Color_t2555686324  ___endValue_54;
	// T2 DG.Tweening.Core.TweenerCore`3::changeValue
	Color_t2555686324  ___changeValue_55;
	// TPlugOptions DG.Tweening.Core.TweenerCore`3::plugOptions
	ColorOptions_t1487297155  ___plugOptions_56;
	// DG.Tweening.Core.DOGetter`1<T1> DG.Tweening.Core.TweenerCore`3::getter
	DOGetter_1_t2298482528 * ___getter_57;
	// DG.Tweening.Core.DOSetter`1<T1> DG.Tweening.Core.TweenerCore`3::setter
	DOSetter_1_t3605794656 * ___setter_58;
	// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions> DG.Tweening.Core.TweenerCore`3::tweenPlugin
	ABSTweenPlugin_3_t281541932 * ___tweenPlugin_59;

public:
	inline static int32_t get_offset_of_startValue_53() { return static_cast<int32_t>(offsetof(TweenerCore_3_t745532282, ___startValue_53)); }
	inline Color_t2555686324  get_startValue_53() const { return ___startValue_53; }
	inline Color_t2555686324 * get_address_of_startValue_53() { return &___startValue_53; }
	inline void set_startValue_53(Color_t2555686324  value)
	{
		___startValue_53 = value;
	}

	inline static int32_t get_offset_of_endValue_54() { return static_cast<int32_t>(offsetof(TweenerCore_3_t745532282, ___endValue_54)); }
	inline Color_t2555686324  get_endValue_54() const { return ___endValue_54; }
	inline Color_t2555686324 * get_address_of_endValue_54() { return &___endValue_54; }
	inline void set_endValue_54(Color_t2555686324  value)
	{
		___endValue_54 = value;
	}

	inline static int32_t get_offset_of_changeValue_55() { return static_cast<int32_t>(offsetof(TweenerCore_3_t745532282, ___changeValue_55)); }
	inline Color_t2555686324  get_changeValue_55() const { return ___changeValue_55; }
	inline Color_t2555686324 * get_address_of_changeValue_55() { return &___changeValue_55; }
	inline void set_changeValue_55(Color_t2555686324  value)
	{
		___changeValue_55 = value;
	}

	inline static int32_t get_offset_of_plugOptions_56() { return static_cast<int32_t>(offsetof(TweenerCore_3_t745532282, ___plugOptions_56)); }
	inline ColorOptions_t1487297155  get_plugOptions_56() const { return ___plugOptions_56; }
	inline ColorOptions_t1487297155 * get_address_of_plugOptions_56() { return &___plugOptions_56; }
	inline void set_plugOptions_56(ColorOptions_t1487297155  value)
	{
		___plugOptions_56 = value;
	}

	inline static int32_t get_offset_of_getter_57() { return static_cast<int32_t>(offsetof(TweenerCore_3_t745532282, ___getter_57)); }
	inline DOGetter_1_t2298482528 * get_getter_57() const { return ___getter_57; }
	inline DOGetter_1_t2298482528 ** get_address_of_getter_57() { return &___getter_57; }
	inline void set_getter_57(DOGetter_1_t2298482528 * value)
	{
		___getter_57 = value;
		Il2CppCodeGenWriteBarrier((&___getter_57), value);
	}

	inline static int32_t get_offset_of_setter_58() { return static_cast<int32_t>(offsetof(TweenerCore_3_t745532282, ___setter_58)); }
	inline DOSetter_1_t3605794656 * get_setter_58() const { return ___setter_58; }
	inline DOSetter_1_t3605794656 ** get_address_of_setter_58() { return &___setter_58; }
	inline void set_setter_58(DOSetter_1_t3605794656 * value)
	{
		___setter_58 = value;
		Il2CppCodeGenWriteBarrier((&___setter_58), value);
	}

	inline static int32_t get_offset_of_tweenPlugin_59() { return static_cast<int32_t>(offsetof(TweenerCore_3_t745532282, ___tweenPlugin_59)); }
	inline ABSTweenPlugin_3_t281541932 * get_tweenPlugin_59() const { return ___tweenPlugin_59; }
	inline ABSTweenPlugin_3_t281541932 ** get_address_of_tweenPlugin_59() { return &___tweenPlugin_59; }
	inline void set_tweenPlugin_59(ABSTweenPlugin_3_t281541932 * value)
	{
		___tweenPlugin_59 = value;
		Il2CppCodeGenWriteBarrier((&___tweenPlugin_59), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENERCORE_3_T745532282_H
#ifndef TWEENERCORE_3_T4006488229_H
#define TWEENERCORE_3_T4006488229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct  TweenerCore_3_t4006488229  : public Tweener_t436044680
{
public:
	// T2 DG.Tweening.Core.TweenerCore`3::startValue
	Vector2_t2156229523  ___startValue_53;
	// T2 DG.Tweening.Core.TweenerCore`3::endValue
	Vector2_t2156229523  ___endValue_54;
	// T2 DG.Tweening.Core.TweenerCore`3::changeValue
	Vector2_t2156229523  ___changeValue_55;
	// TPlugOptions DG.Tweening.Core.TweenerCore`3::plugOptions
	VectorOptions_t1354903650  ___plugOptions_56;
	// DG.Tweening.Core.DOGetter`1<T1> DG.Tweening.Core.TweenerCore`3::getter
	DOGetter_1_t1899025727 * ___getter_57;
	// DG.Tweening.Core.DOSetter`1<T1> DG.Tweening.Core.TweenerCore`3::setter
	DOSetter_1_t3206337855 * ___setter_58;
	// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions> DG.Tweening.Core.TweenerCore`3::tweenPlugin
	ABSTweenPlugin_3_t3542497879 * ___tweenPlugin_59;

public:
	inline static int32_t get_offset_of_startValue_53() { return static_cast<int32_t>(offsetof(TweenerCore_3_t4006488229, ___startValue_53)); }
	inline Vector2_t2156229523  get_startValue_53() const { return ___startValue_53; }
	inline Vector2_t2156229523 * get_address_of_startValue_53() { return &___startValue_53; }
	inline void set_startValue_53(Vector2_t2156229523  value)
	{
		___startValue_53 = value;
	}

	inline static int32_t get_offset_of_endValue_54() { return static_cast<int32_t>(offsetof(TweenerCore_3_t4006488229, ___endValue_54)); }
	inline Vector2_t2156229523  get_endValue_54() const { return ___endValue_54; }
	inline Vector2_t2156229523 * get_address_of_endValue_54() { return &___endValue_54; }
	inline void set_endValue_54(Vector2_t2156229523  value)
	{
		___endValue_54 = value;
	}

	inline static int32_t get_offset_of_changeValue_55() { return static_cast<int32_t>(offsetof(TweenerCore_3_t4006488229, ___changeValue_55)); }
	inline Vector2_t2156229523  get_changeValue_55() const { return ___changeValue_55; }
	inline Vector2_t2156229523 * get_address_of_changeValue_55() { return &___changeValue_55; }
	inline void set_changeValue_55(Vector2_t2156229523  value)
	{
		___changeValue_55 = value;
	}

	inline static int32_t get_offset_of_plugOptions_56() { return static_cast<int32_t>(offsetof(TweenerCore_3_t4006488229, ___plugOptions_56)); }
	inline VectorOptions_t1354903650  get_plugOptions_56() const { return ___plugOptions_56; }
	inline VectorOptions_t1354903650 * get_address_of_plugOptions_56() { return &___plugOptions_56; }
	inline void set_plugOptions_56(VectorOptions_t1354903650  value)
	{
		___plugOptions_56 = value;
	}

	inline static int32_t get_offset_of_getter_57() { return static_cast<int32_t>(offsetof(TweenerCore_3_t4006488229, ___getter_57)); }
	inline DOGetter_1_t1899025727 * get_getter_57() const { return ___getter_57; }
	inline DOGetter_1_t1899025727 ** get_address_of_getter_57() { return &___getter_57; }
	inline void set_getter_57(DOGetter_1_t1899025727 * value)
	{
		___getter_57 = value;
		Il2CppCodeGenWriteBarrier((&___getter_57), value);
	}

	inline static int32_t get_offset_of_setter_58() { return static_cast<int32_t>(offsetof(TweenerCore_3_t4006488229, ___setter_58)); }
	inline DOSetter_1_t3206337855 * get_setter_58() const { return ___setter_58; }
	inline DOSetter_1_t3206337855 ** get_address_of_setter_58() { return &___setter_58; }
	inline void set_setter_58(DOSetter_1_t3206337855 * value)
	{
		___setter_58 = value;
		Il2CppCodeGenWriteBarrier((&___setter_58), value);
	}

	inline static int32_t get_offset_of_tweenPlugin_59() { return static_cast<int32_t>(offsetof(TweenerCore_3_t4006488229, ___tweenPlugin_59)); }
	inline ABSTweenPlugin_3_t3542497879 * get_tweenPlugin_59() const { return ___tweenPlugin_59; }
	inline ABSTweenPlugin_3_t3542497879 ** get_address_of_tweenPlugin_59() { return &___tweenPlugin_59; }
	inline void set_tweenPlugin_59(ABSTweenPlugin_3_t3542497879 * value)
	{
		___tweenPlugin_59 = value;
		Il2CppCodeGenWriteBarrier((&___tweenPlugin_59), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENERCORE_3_T4006488229_H


// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m1558284636_gshared (DOGetter_1_t2298482528 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m380718107_gshared (DOSetter_1_t3605794656 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<System.Object>(!!0,System.Object)
extern "C"  RuntimeObject * TweenSettingsExtensions_SetTarget_TisRuntimeObject_m179229169_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m3327751853_gshared (DOGetter_1_t1899025727 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2986952954_gshared (DOSetter_1_t3206337855 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOGetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2335505072_gshared (DOGetter_1_t1140062978 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m3423399374_gshared (DOSetter_1_t2447375106 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);

// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass2_0__ctor_m3114130936 (U3CU3Ec__DisplayClass2_0_t116211281 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
#define DOGetter_1__ctor_m1558284636(__this, p0, p1, method) ((  void (*) (DOGetter_1_t2298482528 *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOGetter_1__ctor_m1558284636_gshared)(__this, p0, p1, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
#define DOSetter_1__ctor_m380718107(__this, p0, p1, method) ((  void (*) (DOSetter_1_t3605794656 *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOSetter_1__ctor_m380718107_gshared)(__this, p0, p1, method)
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Color>,DG.Tweening.Core.DOSetter`1<UnityEngine.Color>,UnityEngine.Color,System.Single)
extern "C"  TweenerCore_3_t745532282 * DOTween_To_m3257840982 (RuntimeObject * __this /* static, unused */, DOGetter_1_t2298482528 * p0, DOSetter_1_t3605794656 * p1, Color_t2555686324  p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>>(!!0,System.Object)
#define TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t745532282_m2904780857(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t745532282 * (*) (RuntimeObject * /* static, unused */, TweenerCore_3_t745532282 *, RuntimeObject *, const RuntimeMethod*))TweenSettingsExtensions_SetTarget_TisRuntimeObject_m179229169_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass3_0__ctor_m3114131961 (U3CU3Ec__DisplayClass3_0_t2845094636 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.DOTween::ToAlpha(DG.Tweening.Core.DOGetter`1<UnityEngine.Color>,DG.Tweening.Core.DOSetter`1<UnityEngine.Color>,System.Single,System.Single)
extern "C"  Tweener_t436044680 * DOTween_ToAlpha_m1646959083 (RuntimeObject * __this /* static, unused */, DOGetter_1_t2298482528 * p0, DOSetter_1_t3605794656 * p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Tweener>(!!0,System.Object)
#define TweenSettingsExtensions_SetTarget_TisTweener_t436044680_m598017345(__this /* static, unused */, p0, p1, method) ((  Tweener_t436044680 * (*) (RuntimeObject * /* static, unused */, Tweener_t436044680 *, RuntimeObject *, const RuntimeMethod*))TweenSettingsExtensions_SetTarget_TisRuntimeObject_m179229169_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass5_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass5_0__ctor_m3114126035 (U3CU3Ec__DisplayClass5_0_t2038525582 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
#define DOGetter_1__ctor_m3327751853(__this, p0, p1, method) ((  void (*) (DOGetter_1_t1899025727 *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOGetter_1__ctor_m3327751853_gshared)(__this, p0, p1, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
#define DOSetter_1__ctor_m2986952954(__this, p0, p1, method) ((  void (*) (DOSetter_1_t3206337855 *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOSetter_1__ctor_m2986952954_gshared)(__this, p0, p1, method)
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>,UnityEngine.Vector2,System.Single)
extern "C"  TweenerCore_3_t4006488229 * DOTween_To_m3593210828 (RuntimeObject * __this /* static, unused */, DOGetter_1_t1899025727 * p0, DOSetter_1_t3206337855 * p1, Vector2_t2156229523  p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>,System.Boolean)
extern "C"  Tweener_t436044680 * TweenSettingsExtensions_SetOptions_m2879183578 (RuntimeObject * __this /* static, unused */, TweenerCore_3_t4006488229 * p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass8_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass8_0__ctor_m3114136806 (U3CU3Ec__DisplayClass8_0_t1635241055 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOGetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
#define DOGetter_1__ctor_m2335505072(__this, p0, p1, method) ((  void (*) (DOGetter_1_t1140062978 *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOGetter_1__ctor_m2335505072_gshared)(__this, p0, p1, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
#define DOSetter_1__ctor_m3423399374(__this, p0, p1, method) ((  void (*) (DOSetter_1_t2447375106 *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOSetter_1__ctor_m3423399374_gshared)(__this, p0, p1, method)
// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<System.Single>,DG.Tweening.Core.DOSetter`1<System.Single>,System.Single,System.Single)
extern "C"  TweenerCore_3_t884556411 * DOTween_To_m2106473129 (RuntimeObject * __this /* static, unused */, DOGetter_1_t1140062978 * p0, DOSetter_1_t2447375106 * p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>>(!!0,System.Object)
#define TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t884556411_m1318424630(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t884556411 * (*) (RuntimeObject * /* static, unused */, TweenerCore_3_t884556411 *, RuntimeObject *, const RuntimeMethod*))TweenSettingsExtensions_SetTarget_TisRuntimeObject_m179229169_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.SpriteRenderer::get_color()
extern "C"  Color_t2555686324  SpriteRenderer_get_color_m904197748 (SpriteRenderer_t3235626157 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::set_color(UnityEngine.Color)
extern "C"  void SpriteRenderer_set_color_m3056777566 (SpriteRenderer_t3235626157 * __this, Color_t2555686324  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_position()
extern "C"  Vector2_t2156229523  Rigidbody2D_get_position_m2575647076 (Rigidbody2D_t939494601 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rigidbody2D::get_rotation()
extern "C"  float Rigidbody2D_get_rotation_m1584227507 (Rigidbody2D_t939494601 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DOColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
extern "C"  Tweener_t436044680 * ShortcutExtensions43_DOColor_m2154457141 (RuntimeObject * __this /* static, unused */, SpriteRenderer_t3235626157 * ___target0, Color_t2555686324  ___endValue1, float ___duration2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions43_DOColor_m2154457141_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass2_0_t116211281 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass2_0_t116211281 * L_0 = (U3CU3Ec__DisplayClass2_0_t116211281 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass2_0_t116211281_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass2_0__ctor_m3114130936(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass2_0_t116211281 * L_1 = V_0;
		SpriteRenderer_t3235626157 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass2_0_t116211281 * L_3 = V_0;
		intptr_t L_4 = (intptr_t)U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__0_m3301202239_RuntimeMethod_var;
		DOGetter_1_t2298482528 * L_5 = (DOGetter_1_t2298482528 *)il2cpp_codegen_object_new(DOGetter_1_t2298482528_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m1558284636(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m1558284636_RuntimeMethod_var);
		U3CU3Ec__DisplayClass2_0_t116211281 * L_6 = V_0;
		intptr_t L_7 = (intptr_t)U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__1_m3918942505_RuntimeMethod_var;
		DOSetter_1_t3605794656 * L_8 = (DOSetter_1_t3605794656 *)il2cpp_codegen_object_new(DOSetter_1_t3605794656_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m380718107(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m380718107_RuntimeMethod_var);
		Color_t2555686324  L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2744875806_il2cpp_TypeInfo_var);
		TweenerCore_3_t745532282 * L_11 = DOTween_To_m3257840982(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t116211281 * L_12 = V_0;
		NullCheck(L_12);
		SpriteRenderer_t3235626157 * L_13 = L_12->get_target_0();
		TweenerCore_3_t745532282 * L_14 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t745532282_m2904780857(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t745532282_m2904780857_RuntimeMethod_var);
		return L_14;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DOFade(UnityEngine.SpriteRenderer,System.Single,System.Single)
extern "C"  Tweener_t436044680 * ShortcutExtensions43_DOFade_m836661547 (RuntimeObject * __this /* static, unused */, SpriteRenderer_t3235626157 * ___target0, float ___endValue1, float ___duration2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions43_DOFade_m836661547_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass3_0_t2845094636 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass3_0_t2845094636 * L_0 = (U3CU3Ec__DisplayClass3_0_t2845094636 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass3_0_t2845094636_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass3_0__ctor_m3114131961(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass3_0_t2845094636 * L_1 = V_0;
		SpriteRenderer_t3235626157 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass3_0_t2845094636 * L_3 = V_0;
		intptr_t L_4 = (intptr_t)U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__0_m1573959735_RuntimeMethod_var;
		DOGetter_1_t2298482528 * L_5 = (DOGetter_1_t2298482528 *)il2cpp_codegen_object_new(DOGetter_1_t2298482528_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m1558284636(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m1558284636_RuntimeMethod_var);
		U3CU3Ec__DisplayClass3_0_t2845094636 * L_6 = V_0;
		intptr_t L_7 = (intptr_t)U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__1_m2190249801_RuntimeMethod_var;
		DOSetter_1_t3605794656 * L_8 = (DOSetter_1_t3605794656 *)il2cpp_codegen_object_new(DOSetter_1_t3605794656_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m380718107(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m380718107_RuntimeMethod_var);
		float L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2744875806_il2cpp_TypeInfo_var);
		Tweener_t436044680 * L_11 = DOTween_ToAlpha_m1646959083(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass3_0_t2845094636 * L_12 = V_0;
		NullCheck(L_12);
		SpriteRenderer_t3235626157 * L_13 = L_12->get_target_0();
		Tweener_t436044680 * L_14 = TweenSettingsExtensions_SetTarget_TisTweener_t436044680_m598017345(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t436044680_m598017345_RuntimeMethod_var);
		return L_14;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DOMove(UnityEngine.Rigidbody2D,UnityEngine.Vector2,System.Single,System.Boolean)
extern "C"  Tweener_t436044680 * ShortcutExtensions43_DOMove_m1054159185 (RuntimeObject * __this /* static, unused */, Rigidbody2D_t939494601 * ___target0, Vector2_t2156229523  ___endValue1, float ___duration2, bool ___snapping3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions43_DOMove_m1054159185_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass5_0_t2038525582 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass5_0_t2038525582 * L_0 = (U3CU3Ec__DisplayClass5_0_t2038525582 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass5_0_t2038525582_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass5_0__ctor_m3114126035(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass5_0_t2038525582 * L_1 = V_0;
		Rigidbody2D_t939494601 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass5_0_t2038525582 * L_3 = V_0;
		intptr_t L_4 = (intptr_t)U3CU3Ec__DisplayClass5_0_U3CDOMoveU3Eb__0_m2124110351_RuntimeMethod_var;
		DOGetter_1_t1899025727 * L_5 = (DOGetter_1_t1899025727 *)il2cpp_codegen_object_new(DOGetter_1_t1899025727_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m3327751853(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m3327751853_RuntimeMethod_var);
		U3CU3Ec__DisplayClass5_0_t2038525582 * L_6 = V_0;
		NullCheck(L_6);
		Rigidbody2D_t939494601 * L_7 = L_6->get_target_0();
		intptr_t L_8 = (intptr_t)Rigidbody2D_MovePosition_m1934912072_RuntimeMethod_var;
		DOSetter_1_t3206337855 * L_9 = (DOSetter_1_t3206337855 *)il2cpp_codegen_object_new(DOSetter_1_t3206337855_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m2986952954(L_9, L_7, L_8, /*hidden argument*/DOSetter_1__ctor_m2986952954_RuntimeMethod_var);
		Vector2_t2156229523  L_10 = ___endValue1;
		float L_11 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2744875806_il2cpp_TypeInfo_var);
		TweenerCore_3_t4006488229 * L_12 = DOTween_To_m3593210828(NULL /*static, unused*/, L_5, L_9, L_10, L_11, /*hidden argument*/NULL);
		bool L_13 = ___snapping3;
		Tweener_t436044680 * L_14 = TweenSettingsExtensions_SetOptions_m2879183578(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass5_0_t2038525582 * L_15 = V_0;
		NullCheck(L_15);
		Rigidbody2D_t939494601 * L_16 = L_15->get_target_0();
		Tweener_t436044680 * L_17 = TweenSettingsExtensions_SetTarget_TisTweener_t436044680_m598017345(NULL /*static, unused*/, L_14, L_16, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t436044680_m598017345_RuntimeMethod_var);
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DORotate(UnityEngine.Rigidbody2D,System.Single,System.Single)
extern "C"  Tweener_t436044680 * ShortcutExtensions43_DORotate_m3965828009 (RuntimeObject * __this /* static, unused */, Rigidbody2D_t939494601 * ___target0, float ___endValue1, float ___duration2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions43_DORotate_m3965828009_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass8_0_t1635241055 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass8_0_t1635241055 * L_0 = (U3CU3Ec__DisplayClass8_0_t1635241055 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass8_0_t1635241055_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass8_0__ctor_m3114136806(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass8_0_t1635241055 * L_1 = V_0;
		Rigidbody2D_t939494601 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass8_0_t1635241055 * L_3 = V_0;
		intptr_t L_4 = (intptr_t)U3CU3Ec__DisplayClass8_0_U3CDORotateU3Eb__0_m2165437160_RuntimeMethod_var;
		DOGetter_1_t1140062978 * L_5 = (DOGetter_1_t1140062978 *)il2cpp_codegen_object_new(DOGetter_1_t1140062978_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m2335505072(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m2335505072_RuntimeMethod_var);
		U3CU3Ec__DisplayClass8_0_t1635241055 * L_6 = V_0;
		NullCheck(L_6);
		Rigidbody2D_t939494601 * L_7 = L_6->get_target_0();
		intptr_t L_8 = (intptr_t)Rigidbody2D_MoveRotation_m3032842781_RuntimeMethod_var;
		DOSetter_1_t2447375106 * L_9 = (DOSetter_1_t2447375106 *)il2cpp_codegen_object_new(DOSetter_1_t2447375106_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m3423399374(L_9, L_7, L_8, /*hidden argument*/DOSetter_1__ctor_m3423399374_RuntimeMethod_var);
		float L_10 = ___endValue1;
		float L_11 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2744875806_il2cpp_TypeInfo_var);
		TweenerCore_3_t884556411 * L_12 = DOTween_To_m2106473129(NULL /*static, unused*/, L_5, L_9, L_10, L_11, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass8_0_t1635241055 * L_13 = V_0;
		NullCheck(L_13);
		Rigidbody2D_t939494601 * L_14 = L_13->get_target_0();
		TweenerCore_3_t884556411 * L_15 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t884556411_m1318424630(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t884556411_m1318424630_RuntimeMethod_var);
		return L_15;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass2_0__ctor_m3114130936 (U3CU3Ec__DisplayClass2_0_t116211281 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0::<DOColor>b__0()
extern "C"  Color_t2555686324  U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__0_m3301202239 (U3CU3Ec__DisplayClass2_0_t116211281 * __this, const RuntimeMethod* method)
{
	{
		SpriteRenderer_t3235626157 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Color_t2555686324  L_1 = SpriteRenderer_get_color_m904197748(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0::<DOColor>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__1_m3918942505 (U3CU3Ec__DisplayClass2_0_t116211281 * __this, Color_t2555686324  ___x0, const RuntimeMethod* method)
{
	{
		SpriteRenderer_t3235626157 * L_0 = __this->get_target_0();
		Color_t2555686324  L_1 = ___x0;
		NullCheck(L_0);
		SpriteRenderer_set_color_m3056777566(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass3_0__ctor_m3114131961 (U3CU3Ec__DisplayClass3_0_t2845094636 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0::<DOFade>b__0()
extern "C"  Color_t2555686324  U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__0_m1573959735 (U3CU3Ec__DisplayClass3_0_t2845094636 * __this, const RuntimeMethod* method)
{
	{
		SpriteRenderer_t3235626157 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Color_t2555686324  L_1 = SpriteRenderer_get_color_m904197748(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0::<DOFade>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__1_m2190249801 (U3CU3Ec__DisplayClass3_0_t2845094636 * __this, Color_t2555686324  ___x0, const RuntimeMethod* method)
{
	{
		SpriteRenderer_t3235626157 * L_0 = __this->get_target_0();
		Color_t2555686324  L_1 = ___x0;
		NullCheck(L_0);
		SpriteRenderer_set_color_m3056777566(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass5_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass5_0__ctor_m3114126035 (U3CU3Ec__DisplayClass5_0_t2038525582 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions43/<>c__DisplayClass5_0::<DOMove>b__0()
extern "C"  Vector2_t2156229523  U3CU3Ec__DisplayClass5_0_U3CDOMoveU3Eb__0_m2124110351 (U3CU3Ec__DisplayClass5_0_t2038525582 * __this, const RuntimeMethod* method)
{
	{
		Rigidbody2D_t939494601 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector2_t2156229523  L_1 = Rigidbody2D_get_position_m2575647076(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass8_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass8_0__ctor_m3114136806 (U3CU3Ec__DisplayClass8_0_t1635241055 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single DG.Tweening.ShortcutExtensions43/<>c__DisplayClass8_0::<DORotate>b__0()
extern "C"  float U3CU3Ec__DisplayClass8_0_U3CDORotateU3Eb__0_m2165437160 (U3CU3Ec__DisplayClass8_0_t1635241055 * __this, const RuntimeMethod* method)
{
	{
		Rigidbody2D_t939494601 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		float L_1 = Rigidbody2D_get_rotation_m1584227507(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
