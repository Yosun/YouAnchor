﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Configuration.ConfigurationSectionCollection
struct ConfigurationSectionCollection_t2786897858;
// System.Configuration.ConfigurationSectionGroupCollection
struct ConfigurationSectionGroupCollection_t1151641153;
// System.Configuration.Configuration
struct Configuration_t2529364143;
// System.Configuration.SectionGroupInfo
struct SectionGroupInfo_t687896682;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Configuration.ConfigurationProperty>
struct List_1_t767969300;
// System.Configuration.Internal.IInternalConfigHost
struct IInternalConfigHost_t3684241368;
// System.Configuration.InternalConfigurationFactory
struct InternalConfigurationFactory_t2066639612;
// System.Configuration.Internal.IInternalConfigSystem
struct IInternalConfigSystem_t3983974894;
// System.Configuration.Internal.IInternalConfigRoot
struct IInternalConfigRoot_t1704923096;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Configuration.PropertyInformation
struct PropertyInformation_t4136807793;
// System.Configuration.ConfigurationElement
struct ConfigurationElement_t3318566633;
// System.Configuration.PropertyInformationCollection
struct PropertyInformationCollection_t3866802480;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationCollectionAttribute
struct ConfigurationCollectionAttribute_t3695308734;
// System.Configuration.ElementMap
struct ElementMap_t2160633803;
// System.Configuration.ConfigurationElementCollection
struct ConfigurationElementCollection_t446763386;
// System.Configuration.ElementInformation
struct ElementInformation_t2651568025;
// System.Configuration.ConfigurationLockCollection
struct ConfigurationLockCollection_t4066281341;
// System.Configuration.ConfigurationSectionGroup
struct ConfigurationSectionGroup_t4179402520;
// System.Configuration.ConfigurationLocationCollection
struct ConfigurationLocationCollection_t1538619295;
// System.Configuration.Internal.IConfigSystem
struct IConfigSystem_t2399753304;
// System.Type
struct Type_t;
// System.Xml.XmlReaderBinarySupport
struct XmlReaderBinarySupport_t1809665003;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t2186285234;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Specialized.NameObjectCollectionBase/_Item
struct _Item_t2272350267;
// System.Collections.IHashCodeProvider
struct IHashCodeProvider_t267601189;
// System.Collections.IComparer
struct IComparer_t1540313114;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
struct KeysCollection_t1318642398;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t1493878338;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Security.Cryptography.SymmetricAlgorithm
struct SymmetricAlgorithm_t4254223087;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Configuration.ConfigNameValueCollection
struct ConfigNameValueCollection_t1573003828;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.Configuration.ConfigInfoCollection
struct ConfigInfoCollection_t215413917;
// Mono.Xml2.XmlTextReader
struct XmlTextReader_t3455035481;
// System.Collections.Generic.Stack`1<System.String>
struct Stack_1_t2690840144;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Configuration.ConfigurationValidatorBase
struct ConfigurationValidatorBase_t888490966;
// System.Configuration.SectionInformation
struct SectionInformation_t2821611020;
// System.Configuration.IConfigurationSectionHandler
struct IConfigurationSectionHandler_t3614337894;
// System.TimeZoneInfo/AdjustmentRule[]
struct AdjustmentRuleU5BU5D_t1469475992;
// System.Configuration.ProtectedConfigurationProviderCollection
struct ProtectedConfigurationProviderCollection_t729952796;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Configuration.ConfigurationSection
struct ConfigurationSection_t3156163955;
// System.Configuration.ProtectedConfigurationProvider
struct ProtectedConfigurationProvider_t413290519;
// System.Configuration.ExeConfigurationFileMap
struct ExeConfigurationFileMap_t3751566101;
// System.ComponentModel.TypeConverter
struct TypeConverter_t2249118273;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745530_H
#define U3CMODULEU3E_T692745530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745530 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745530_H
#ifndef U3CMODULEU3E_T692745529_H
#define U3CMODULEU3E_T692745529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745529 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745529_H
#ifndef ENUMERABLE_T538148348_H
#define ENUMERABLE_T538148348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable
struct  Enumerable_t538148348  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERABLE_T538148348_H
#ifndef PROTECTEDCONFIGURATION_T25042150_H
#define PROTECTEDCONFIGURATION_T25042150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ProtectedConfiguration
struct  ProtectedConfiguration_t25042150  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTECTEDCONFIGURATION_T25042150_H
#ifndef CONFIGURATIONSECTIONGROUP_T4179402520_H
#define CONFIGURATIONSECTIONGROUP_T4179402520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationSectionGroup
struct  ConfigurationSectionGroup_t4179402520  : public RuntimeObject
{
public:
	// System.Configuration.ConfigurationSectionCollection System.Configuration.ConfigurationSectionGroup::sections
	ConfigurationSectionCollection_t2786897858 * ___sections_0;
	// System.Configuration.ConfigurationSectionGroupCollection System.Configuration.ConfigurationSectionGroup::groups
	ConfigurationSectionGroupCollection_t1151641153 * ___groups_1;
	// System.Configuration.Configuration System.Configuration.ConfigurationSectionGroup::config
	Configuration_t2529364143 * ___config_2;
	// System.Configuration.SectionGroupInfo System.Configuration.ConfigurationSectionGroup::group
	SectionGroupInfo_t687896682 * ___group_3;
	// System.Boolean System.Configuration.ConfigurationSectionGroup::initialized
	bool ___initialized_4;

public:
	inline static int32_t get_offset_of_sections_0() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroup_t4179402520, ___sections_0)); }
	inline ConfigurationSectionCollection_t2786897858 * get_sections_0() const { return ___sections_0; }
	inline ConfigurationSectionCollection_t2786897858 ** get_address_of_sections_0() { return &___sections_0; }
	inline void set_sections_0(ConfigurationSectionCollection_t2786897858 * value)
	{
		___sections_0 = value;
		Il2CppCodeGenWriteBarrier((&___sections_0), value);
	}

	inline static int32_t get_offset_of_groups_1() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroup_t4179402520, ___groups_1)); }
	inline ConfigurationSectionGroupCollection_t1151641153 * get_groups_1() const { return ___groups_1; }
	inline ConfigurationSectionGroupCollection_t1151641153 ** get_address_of_groups_1() { return &___groups_1; }
	inline void set_groups_1(ConfigurationSectionGroupCollection_t1151641153 * value)
	{
		___groups_1 = value;
		Il2CppCodeGenWriteBarrier((&___groups_1), value);
	}

	inline static int32_t get_offset_of_config_2() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroup_t4179402520, ___config_2)); }
	inline Configuration_t2529364143 * get_config_2() const { return ___config_2; }
	inline Configuration_t2529364143 ** get_address_of_config_2() { return &___config_2; }
	inline void set_config_2(Configuration_t2529364143 * value)
	{
		___config_2 = value;
		Il2CppCodeGenWriteBarrier((&___config_2), value);
	}

	inline static int32_t get_offset_of_group_3() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroup_t4179402520, ___group_3)); }
	inline SectionGroupInfo_t687896682 * get_group_3() const { return ___group_3; }
	inline SectionGroupInfo_t687896682 ** get_address_of_group_3() { return &___group_3; }
	inline void set_group_3(SectionGroupInfo_t687896682 * value)
	{
		___group_3 = value;
		Il2CppCodeGenWriteBarrier((&___group_3), value);
	}

	inline static int32_t get_offset_of_initialized_4() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroup_t4179402520, ___initialized_4)); }
	inline bool get_initialized_4() const { return ___initialized_4; }
	inline bool* get_address_of_initialized_4() { return &___initialized_4; }
	inline void set_initialized_4(bool value)
	{
		___initialized_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONSECTIONGROUP_T4179402520_H
#ifndef U3CGETENUMERATORU3EC__ITERATOR0_T2103655423_H
#define U3CGETENUMERATORU3EC__ITERATOR0_T2103655423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationSectionCollection/<GetEnumerator>c__Iterator0
struct  U3CGetEnumeratorU3Ec__Iterator0_t2103655423  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Configuration.ConfigurationSectionCollection/<GetEnumerator>c__Iterator0::<$s_32>__0
	RuntimeObject* ___U3CU24s_32U3E__0_0;
	// System.String System.Configuration.ConfigurationSectionCollection/<GetEnumerator>c__Iterator0::<key>__1
	String_t* ___U3CkeyU3E__1_1;
	// System.Int32 System.Configuration.ConfigurationSectionCollection/<GetEnumerator>c__Iterator0::$PC
	int32_t ___U24PC_2;
	// System.Object System.Configuration.ConfigurationSectionCollection/<GetEnumerator>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Configuration.ConfigurationSectionCollection System.Configuration.ConfigurationSectionCollection/<GetEnumerator>c__Iterator0::<>f__this
	ConfigurationSectionCollection_t2786897858 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CU24s_32U3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t2103655423, ___U3CU24s_32U3E__0_0)); }
	inline RuntimeObject* get_U3CU24s_32U3E__0_0() const { return ___U3CU24s_32U3E__0_0; }
	inline RuntimeObject** get_address_of_U3CU24s_32U3E__0_0() { return &___U3CU24s_32U3E__0_0; }
	inline void set_U3CU24s_32U3E__0_0(RuntimeObject* value)
	{
		___U3CU24s_32U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU24s_32U3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CkeyU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t2103655423, ___U3CkeyU3E__1_1)); }
	inline String_t* get_U3CkeyU3E__1_1() const { return ___U3CkeyU3E__1_1; }
	inline String_t** get_address_of_U3CkeyU3E__1_1() { return &___U3CkeyU3E__1_1; }
	inline void set_U3CkeyU3E__1_1(String_t* value)
	{
		___U3CkeyU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CkeyU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t2103655423, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t2103655423, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t2103655423, ___U3CU3Ef__this_4)); }
	inline ConfigurationSectionCollection_t2786897858 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline ConfigurationSectionCollection_t2786897858 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(ConfigurationSectionCollection_t2786897858 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3EC__ITERATOR0_T2103655423_H
#ifndef CONFIGURATIONPROPERTYCOLLECTION_T2852175726_H
#define CONFIGURATIONPROPERTYCOLLECTION_T2852175726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationPropertyCollection
struct  ConfigurationPropertyCollection_t2852175726  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Configuration.ConfigurationProperty> System.Configuration.ConfigurationPropertyCollection::collection
	List_1_t767969300 * ___collection_0;

public:
	inline static int32_t get_offset_of_collection_0() { return static_cast<int32_t>(offsetof(ConfigurationPropertyCollection_t2852175726, ___collection_0)); }
	inline List_1_t767969300 * get_collection_0() const { return ___collection_0; }
	inline List_1_t767969300 ** get_address_of_collection_0() { return &___collection_0; }
	inline void set_collection_0(List_1_t767969300 * value)
	{
		___collection_0 = value;
		Il2CppCodeGenWriteBarrier((&___collection_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONPROPERTYCOLLECTION_T2852175726_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef INTERNALCONFIGURATIONROOT_T3745416310_H
#define INTERNALCONFIGURATIONROOT_T3745416310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.InternalConfigurationRoot
struct  InternalConfigurationRoot_t3745416310  : public RuntimeObject
{
public:
	// System.Configuration.Internal.IInternalConfigHost System.Configuration.InternalConfigurationRoot::host
	RuntimeObject* ___host_0;
	// System.Boolean System.Configuration.InternalConfigurationRoot::isDesignTime
	bool ___isDesignTime_1;

public:
	inline static int32_t get_offset_of_host_0() { return static_cast<int32_t>(offsetof(InternalConfigurationRoot_t3745416310, ___host_0)); }
	inline RuntimeObject* get_host_0() const { return ___host_0; }
	inline RuntimeObject** get_address_of_host_0() { return &___host_0; }
	inline void set_host_0(RuntimeObject* value)
	{
		___host_0 = value;
		Il2CppCodeGenWriteBarrier((&___host_0), value);
	}

	inline static int32_t get_offset_of_isDesignTime_1() { return static_cast<int32_t>(offsetof(InternalConfigurationRoot_t3745416310, ___isDesignTime_1)); }
	inline bool get_isDesignTime_1() const { return ___isDesignTime_1; }
	inline bool* get_address_of_isDesignTime_1() { return &___isDesignTime_1; }
	inline void set_isDesignTime_1(bool value)
	{
		___isDesignTime_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALCONFIGURATIONROOT_T3745416310_H
#ifndef CONFIGURATIONMANAGER_T386529156_H
#define CONFIGURATIONMANAGER_T386529156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationManager
struct  ConfigurationManager_t386529156  : public RuntimeObject
{
public:

public:
};

struct ConfigurationManager_t386529156_StaticFields
{
public:
	// System.Configuration.InternalConfigurationFactory System.Configuration.ConfigurationManager::configFactory
	InternalConfigurationFactory_t2066639612 * ___configFactory_0;
	// System.Configuration.Internal.IInternalConfigSystem System.Configuration.ConfigurationManager::configSystem
	RuntimeObject* ___configSystem_1;
	// System.Object System.Configuration.ConfigurationManager::lockobj
	RuntimeObject * ___lockobj_2;

public:
	inline static int32_t get_offset_of_configFactory_0() { return static_cast<int32_t>(offsetof(ConfigurationManager_t386529156_StaticFields, ___configFactory_0)); }
	inline InternalConfigurationFactory_t2066639612 * get_configFactory_0() const { return ___configFactory_0; }
	inline InternalConfigurationFactory_t2066639612 ** get_address_of_configFactory_0() { return &___configFactory_0; }
	inline void set_configFactory_0(InternalConfigurationFactory_t2066639612 * value)
	{
		___configFactory_0 = value;
		Il2CppCodeGenWriteBarrier((&___configFactory_0), value);
	}

	inline static int32_t get_offset_of_configSystem_1() { return static_cast<int32_t>(offsetof(ConfigurationManager_t386529156_StaticFields, ___configSystem_1)); }
	inline RuntimeObject* get_configSystem_1() const { return ___configSystem_1; }
	inline RuntimeObject** get_address_of_configSystem_1() { return &___configSystem_1; }
	inline void set_configSystem_1(RuntimeObject* value)
	{
		___configSystem_1 = value;
		Il2CppCodeGenWriteBarrier((&___configSystem_1), value);
	}

	inline static int32_t get_offset_of_lockobj_2() { return static_cast<int32_t>(offsetof(ConfigurationManager_t386529156_StaticFields, ___lockobj_2)); }
	inline RuntimeObject * get_lockobj_2() const { return ___lockobj_2; }
	inline RuntimeObject ** get_address_of_lockobj_2() { return &___lockobj_2; }
	inline void set_lockobj_2(RuntimeObject * value)
	{
		___lockobj_2 = value;
		Il2CppCodeGenWriteBarrier((&___lockobj_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONMANAGER_T386529156_H
#ifndef CONFIGURATIONVALIDATORBASE_T888490966_H
#define CONFIGURATIONVALIDATORBASE_T888490966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationValidatorBase
struct  ConfigurationValidatorBase_t888490966  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONVALIDATORBASE_T888490966_H
#ifndef INTERNALCONFIGURATIONHOST_T969478808_H
#define INTERNALCONFIGURATIONHOST_T969478808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.InternalConfigurationHost
struct  InternalConfigurationHost_t969478808  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALCONFIGURATIONHOST_T969478808_H
#ifndef INTERNALCONFIGURATIONSYSTEM_T844406905_H
#define INTERNALCONFIGURATIONSYSTEM_T844406905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.InternalConfigurationSystem
struct  InternalConfigurationSystem_t844406905  : public RuntimeObject
{
public:
	// System.Configuration.Internal.IInternalConfigHost System.Configuration.InternalConfigurationSystem::host
	RuntimeObject* ___host_0;
	// System.Configuration.Internal.IInternalConfigRoot System.Configuration.InternalConfigurationSystem::root
	RuntimeObject* ___root_1;
	// System.Object[] System.Configuration.InternalConfigurationSystem::hostInitParams
	ObjectU5BU5D_t2843939325* ___hostInitParams_2;

public:
	inline static int32_t get_offset_of_host_0() { return static_cast<int32_t>(offsetof(InternalConfigurationSystem_t844406905, ___host_0)); }
	inline RuntimeObject* get_host_0() const { return ___host_0; }
	inline RuntimeObject** get_address_of_host_0() { return &___host_0; }
	inline void set_host_0(RuntimeObject* value)
	{
		___host_0 = value;
		Il2CppCodeGenWriteBarrier((&___host_0), value);
	}

	inline static int32_t get_offset_of_root_1() { return static_cast<int32_t>(offsetof(InternalConfigurationSystem_t844406905, ___root_1)); }
	inline RuntimeObject* get_root_1() const { return ___root_1; }
	inline RuntimeObject** get_address_of_root_1() { return &___root_1; }
	inline void set_root_1(RuntimeObject* value)
	{
		___root_1 = value;
		Il2CppCodeGenWriteBarrier((&___root_1), value);
	}

	inline static int32_t get_offset_of_hostInitParams_2() { return static_cast<int32_t>(offsetof(InternalConfigurationSystem_t844406905, ___hostInitParams_2)); }
	inline ObjectU5BU5D_t2843939325* get_hostInitParams_2() const { return ___hostInitParams_2; }
	inline ObjectU5BU5D_t2843939325** get_address_of_hostInitParams_2() { return &___hostInitParams_2; }
	inline void set_hostInitParams_2(ObjectU5BU5D_t2843939325* value)
	{
		___hostInitParams_2 = value;
		Il2CppCodeGenWriteBarrier((&___hostInitParams_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALCONFIGURATIONSYSTEM_T844406905_H
#ifndef INTERNALCONFIGURATIONFACTORY_T2066639612_H
#define INTERNALCONFIGURATIONFACTORY_T2066639612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.InternalConfigurationFactory
struct  InternalConfigurationFactory_t2066639612  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALCONFIGURATIONFACTORY_T2066639612_H
#ifndef CHECK_T192468399_H
#define CHECK_T192468399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Check
struct  Check_t192468399  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECK_T192468399_H
#ifndef ELEMENTINFORMATION_T2651568025_H
#define ELEMENTINFORMATION_T2651568025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ElementInformation
struct  ElementInformation_t2651568025  : public RuntimeObject
{
public:
	// System.Configuration.PropertyInformation System.Configuration.ElementInformation::propertyInfo
	PropertyInformation_t4136807793 * ___propertyInfo_0;
	// System.Configuration.ConfigurationElement System.Configuration.ElementInformation::owner
	ConfigurationElement_t3318566633 * ___owner_1;
	// System.Configuration.PropertyInformationCollection System.Configuration.ElementInformation::properties
	PropertyInformationCollection_t3866802480 * ___properties_2;

public:
	inline static int32_t get_offset_of_propertyInfo_0() { return static_cast<int32_t>(offsetof(ElementInformation_t2651568025, ___propertyInfo_0)); }
	inline PropertyInformation_t4136807793 * get_propertyInfo_0() const { return ___propertyInfo_0; }
	inline PropertyInformation_t4136807793 ** get_address_of_propertyInfo_0() { return &___propertyInfo_0; }
	inline void set_propertyInfo_0(PropertyInformation_t4136807793 * value)
	{
		___propertyInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___propertyInfo_0), value);
	}

	inline static int32_t get_offset_of_owner_1() { return static_cast<int32_t>(offsetof(ElementInformation_t2651568025, ___owner_1)); }
	inline ConfigurationElement_t3318566633 * get_owner_1() const { return ___owner_1; }
	inline ConfigurationElement_t3318566633 ** get_address_of_owner_1() { return &___owner_1; }
	inline void set_owner_1(ConfigurationElement_t3318566633 * value)
	{
		___owner_1 = value;
		Il2CppCodeGenWriteBarrier((&___owner_1), value);
	}

	inline static int32_t get_offset_of_properties_2() { return static_cast<int32_t>(offsetof(ElementInformation_t2651568025, ___properties_2)); }
	inline PropertyInformationCollection_t3866802480 * get_properties_2() const { return ___properties_2; }
	inline PropertyInformationCollection_t3866802480 ** get_address_of_properties_2() { return &___properties_2; }
	inline void set_properties_2(PropertyInformationCollection_t3866802480 * value)
	{
		___properties_2 = value;
		Il2CppCodeGenWriteBarrier((&___properties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTINFORMATION_T2651568025_H
#ifndef PROPERTYINFORMATIONENUMERATOR_T3780095919_H
#define PROPERTYINFORMATIONENUMERATOR_T3780095919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.PropertyInformationCollection/PropertyInformationEnumerator
struct  PropertyInformationEnumerator_t3780095919  : public RuntimeObject
{
public:
	// System.Configuration.PropertyInformationCollection System.Configuration.PropertyInformationCollection/PropertyInformationEnumerator::collection
	PropertyInformationCollection_t3866802480 * ___collection_0;
	// System.Int32 System.Configuration.PropertyInformationCollection/PropertyInformationEnumerator::position
	int32_t ___position_1;

public:
	inline static int32_t get_offset_of_collection_0() { return static_cast<int32_t>(offsetof(PropertyInformationEnumerator_t3780095919, ___collection_0)); }
	inline PropertyInformationCollection_t3866802480 * get_collection_0() const { return ___collection_0; }
	inline PropertyInformationCollection_t3866802480 ** get_address_of_collection_0() { return &___collection_0; }
	inline void set_collection_0(PropertyInformationCollection_t3866802480 * value)
	{
		___collection_0 = value;
		Il2CppCodeGenWriteBarrier((&___collection_0), value);
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(PropertyInformationEnumerator_t3780095919, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYINFORMATIONENUMERATOR_T3780095919_H
#ifndef U3CCREATERANGEITERATORU3EC__ITERATORD_T2730143742_H
#define U3CCREATERANGEITERATORU3EC__ITERATORD_T2730143742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/<CreateRangeIterator>c__IteratorD
struct  U3CCreateRangeIteratorU3Ec__IteratorD_t2730143742  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable/<CreateRangeIterator>c__IteratorD::start
	int32_t ___start_0;
	// System.Int32 System.Linq.Enumerable/<CreateRangeIterator>c__IteratorD::<i>__0
	int32_t ___U3CiU3E__0_1;
	// System.Int32 System.Linq.Enumerable/<CreateRangeIterator>c__IteratorD::upto
	int32_t ___upto_2;
	// System.Int32 System.Linq.Enumerable/<CreateRangeIterator>c__IteratorD::$PC
	int32_t ___U24PC_3;
	// System.Int32 System.Linq.Enumerable/<CreateRangeIterator>c__IteratorD::$current
	int32_t ___U24current_4;
	// System.Int32 System.Linq.Enumerable/<CreateRangeIterator>c__IteratorD::<$>start
	int32_t ___U3CU24U3Estart_5;
	// System.Int32 System.Linq.Enumerable/<CreateRangeIterator>c__IteratorD::<$>upto
	int32_t ___U3CU24U3Eupto_6;

public:
	inline static int32_t get_offset_of_start_0() { return static_cast<int32_t>(offsetof(U3CCreateRangeIteratorU3Ec__IteratorD_t2730143742, ___start_0)); }
	inline int32_t get_start_0() const { return ___start_0; }
	inline int32_t* get_address_of_start_0() { return &___start_0; }
	inline void set_start_0(int32_t value)
	{
		___start_0 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__0_1() { return static_cast<int32_t>(offsetof(U3CCreateRangeIteratorU3Ec__IteratorD_t2730143742, ___U3CiU3E__0_1)); }
	inline int32_t get_U3CiU3E__0_1() const { return ___U3CiU3E__0_1; }
	inline int32_t* get_address_of_U3CiU3E__0_1() { return &___U3CiU3E__0_1; }
	inline void set_U3CiU3E__0_1(int32_t value)
	{
		___U3CiU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_upto_2() { return static_cast<int32_t>(offsetof(U3CCreateRangeIteratorU3Ec__IteratorD_t2730143742, ___upto_2)); }
	inline int32_t get_upto_2() const { return ___upto_2; }
	inline int32_t* get_address_of_upto_2() { return &___upto_2; }
	inline void set_upto_2(int32_t value)
	{
		___upto_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCreateRangeIteratorU3Ec__IteratorD_t2730143742, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCreateRangeIteratorU3Ec__IteratorD_t2730143742, ___U24current_4)); }
	inline int32_t get_U24current_4() const { return ___U24current_4; }
	inline int32_t* get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(int32_t value)
	{
		___U24current_4 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Estart_5() { return static_cast<int32_t>(offsetof(U3CCreateRangeIteratorU3Ec__IteratorD_t2730143742, ___U3CU24U3Estart_5)); }
	inline int32_t get_U3CU24U3Estart_5() const { return ___U3CU24U3Estart_5; }
	inline int32_t* get_address_of_U3CU24U3Estart_5() { return &___U3CU24U3Estart_5; }
	inline void set_U3CU24U3Estart_5(int32_t value)
	{
		___U3CU24U3Estart_5 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Eupto_6() { return static_cast<int32_t>(offsetof(U3CCreateRangeIteratorU3Ec__IteratorD_t2730143742, ___U3CU24U3Eupto_6)); }
	inline int32_t get_U3CU24U3Eupto_6() const { return ___U3CU24U3Eupto_6; }
	inline int32_t* get_address_of_U3CU24U3Eupto_6() { return &___U3CU24U3Eupto_6; }
	inline void set_U3CU24U3Eupto_6(int32_t value)
	{
		___U3CU24U3Eupto_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATERANGEITERATORU3EC__ITERATORD_T2730143742_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef READONLYCOLLECTIONBASE_T1836743899_H
#define READONLYCOLLECTIONBASE_T1836743899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ReadOnlyCollectionBase
struct  ReadOnlyCollectionBase_t1836743899  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.ReadOnlyCollectionBase::list
	ArrayList_t2718874744 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(ReadOnlyCollectionBase_t1836743899, ___list_0)); }
	inline ArrayList_t2718874744 * get_list_0() const { return ___list_0; }
	inline ArrayList_t2718874744 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t2718874744 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYCOLLECTIONBASE_T1836743899_H
#ifndef KEYBUILDER_T2049230356_H
#define KEYBUILDER_T2049230356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.KeyBuilder
struct  KeyBuilder_t2049230356  : public RuntimeObject
{
public:

public:
};

struct KeyBuilder_t2049230356_StaticFields
{
public:
	// System.Security.Cryptography.RandomNumberGenerator Mono.Security.Cryptography.KeyBuilder::rng
	RandomNumberGenerator_t386037858 * ___rng_0;

public:
	inline static int32_t get_offset_of_rng_0() { return static_cast<int32_t>(offsetof(KeyBuilder_t2049230356_StaticFields, ___rng_0)); }
	inline RandomNumberGenerator_t386037858 * get_rng_0() const { return ___rng_0; }
	inline RandomNumberGenerator_t386037858 ** get_address_of_rng_0() { return &___rng_0; }
	inline void set_rng_0(RandomNumberGenerator_t386037858 * value)
	{
		___rng_0 = value;
		Il2CppCodeGenWriteBarrier((&___rng_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBUILDER_T2049230356_H
#ifndef ELEMENTMAP_T2160633803_H
#define ELEMENTMAP_T2160633803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ElementMap
struct  ElementMap_t2160633803  : public RuntimeObject
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.ElementMap::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_1;
	// System.Configuration.ConfigurationCollectionAttribute System.Configuration.ElementMap::collectionAttribute
	ConfigurationCollectionAttribute_t3695308734 * ___collectionAttribute_2;

public:
	inline static int32_t get_offset_of_properties_1() { return static_cast<int32_t>(offsetof(ElementMap_t2160633803, ___properties_1)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_1() const { return ___properties_1; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_1() { return &___properties_1; }
	inline void set_properties_1(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_1 = value;
		Il2CppCodeGenWriteBarrier((&___properties_1), value);
	}

	inline static int32_t get_offset_of_collectionAttribute_2() { return static_cast<int32_t>(offsetof(ElementMap_t2160633803, ___collectionAttribute_2)); }
	inline ConfigurationCollectionAttribute_t3695308734 * get_collectionAttribute_2() const { return ___collectionAttribute_2; }
	inline ConfigurationCollectionAttribute_t3695308734 ** get_address_of_collectionAttribute_2() { return &___collectionAttribute_2; }
	inline void set_collectionAttribute_2(ConfigurationCollectionAttribute_t3695308734 * value)
	{
		___collectionAttribute_2 = value;
		Il2CppCodeGenWriteBarrier((&___collectionAttribute_2), value);
	}
};

struct ElementMap_t2160633803_StaticFields
{
public:
	// System.Collections.Hashtable System.Configuration.ElementMap::elementMaps
	Hashtable_t1853889766 * ___elementMaps_0;

public:
	inline static int32_t get_offset_of_elementMaps_0() { return static_cast<int32_t>(offsetof(ElementMap_t2160633803_StaticFields, ___elementMaps_0)); }
	inline Hashtable_t1853889766 * get_elementMaps_0() const { return ___elementMaps_0; }
	inline Hashtable_t1853889766 ** get_address_of_elementMaps_0() { return &___elementMaps_0; }
	inline void set_elementMaps_0(Hashtable_t1853889766 * value)
	{
		___elementMaps_0 = value;
		Il2CppCodeGenWriteBarrier((&___elementMaps_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTMAP_T2160633803_H
#ifndef CONFIGURATIONELEMENT_T3318566633_H
#define CONFIGURATIONELEMENT_T3318566633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationElement
struct  ConfigurationElement_t3318566633  : public RuntimeObject
{
public:
	// System.String System.Configuration.ConfigurationElement::rawXml
	String_t* ___rawXml_0;
	// System.Boolean System.Configuration.ConfigurationElement::modified
	bool ___modified_1;
	// System.Configuration.ElementMap System.Configuration.ConfigurationElement::map
	ElementMap_t2160633803 * ___map_2;
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.ConfigurationElement::keyProps
	ConfigurationPropertyCollection_t2852175726 * ___keyProps_3;
	// System.Configuration.ConfigurationElementCollection System.Configuration.ConfigurationElement::defaultCollection
	ConfigurationElementCollection_t446763386 * ___defaultCollection_4;
	// System.Boolean System.Configuration.ConfigurationElement::readOnly
	bool ___readOnly_5;
	// System.Configuration.ElementInformation System.Configuration.ConfigurationElement::elementInfo
	ElementInformation_t2651568025 * ___elementInfo_6;
	// System.Configuration.Configuration System.Configuration.ConfigurationElement::_configuration
	Configuration_t2529364143 * ____configuration_7;
	// System.Configuration.ConfigurationLockCollection System.Configuration.ConfigurationElement::lockAllAttributesExcept
	ConfigurationLockCollection_t4066281341 * ___lockAllAttributesExcept_8;
	// System.Configuration.ConfigurationLockCollection System.Configuration.ConfigurationElement::lockAllElementsExcept
	ConfigurationLockCollection_t4066281341 * ___lockAllElementsExcept_9;
	// System.Configuration.ConfigurationLockCollection System.Configuration.ConfigurationElement::lockAttributes
	ConfigurationLockCollection_t4066281341 * ___lockAttributes_10;
	// System.Configuration.ConfigurationLockCollection System.Configuration.ConfigurationElement::lockElements
	ConfigurationLockCollection_t4066281341 * ___lockElements_11;
	// System.Boolean System.Configuration.ConfigurationElement::lockItem
	bool ___lockItem_12;

public:
	inline static int32_t get_offset_of_rawXml_0() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ___rawXml_0)); }
	inline String_t* get_rawXml_0() const { return ___rawXml_0; }
	inline String_t** get_address_of_rawXml_0() { return &___rawXml_0; }
	inline void set_rawXml_0(String_t* value)
	{
		___rawXml_0 = value;
		Il2CppCodeGenWriteBarrier((&___rawXml_0), value);
	}

	inline static int32_t get_offset_of_modified_1() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ___modified_1)); }
	inline bool get_modified_1() const { return ___modified_1; }
	inline bool* get_address_of_modified_1() { return &___modified_1; }
	inline void set_modified_1(bool value)
	{
		___modified_1 = value;
	}

	inline static int32_t get_offset_of_map_2() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ___map_2)); }
	inline ElementMap_t2160633803 * get_map_2() const { return ___map_2; }
	inline ElementMap_t2160633803 ** get_address_of_map_2() { return &___map_2; }
	inline void set_map_2(ElementMap_t2160633803 * value)
	{
		___map_2 = value;
		Il2CppCodeGenWriteBarrier((&___map_2), value);
	}

	inline static int32_t get_offset_of_keyProps_3() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ___keyProps_3)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_keyProps_3() const { return ___keyProps_3; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_keyProps_3() { return &___keyProps_3; }
	inline void set_keyProps_3(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___keyProps_3 = value;
		Il2CppCodeGenWriteBarrier((&___keyProps_3), value);
	}

	inline static int32_t get_offset_of_defaultCollection_4() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ___defaultCollection_4)); }
	inline ConfigurationElementCollection_t446763386 * get_defaultCollection_4() const { return ___defaultCollection_4; }
	inline ConfigurationElementCollection_t446763386 ** get_address_of_defaultCollection_4() { return &___defaultCollection_4; }
	inline void set_defaultCollection_4(ConfigurationElementCollection_t446763386 * value)
	{
		___defaultCollection_4 = value;
		Il2CppCodeGenWriteBarrier((&___defaultCollection_4), value);
	}

	inline static int32_t get_offset_of_readOnly_5() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ___readOnly_5)); }
	inline bool get_readOnly_5() const { return ___readOnly_5; }
	inline bool* get_address_of_readOnly_5() { return &___readOnly_5; }
	inline void set_readOnly_5(bool value)
	{
		___readOnly_5 = value;
	}

	inline static int32_t get_offset_of_elementInfo_6() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ___elementInfo_6)); }
	inline ElementInformation_t2651568025 * get_elementInfo_6() const { return ___elementInfo_6; }
	inline ElementInformation_t2651568025 ** get_address_of_elementInfo_6() { return &___elementInfo_6; }
	inline void set_elementInfo_6(ElementInformation_t2651568025 * value)
	{
		___elementInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___elementInfo_6), value);
	}

	inline static int32_t get_offset_of__configuration_7() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ____configuration_7)); }
	inline Configuration_t2529364143 * get__configuration_7() const { return ____configuration_7; }
	inline Configuration_t2529364143 ** get_address_of__configuration_7() { return &____configuration_7; }
	inline void set__configuration_7(Configuration_t2529364143 * value)
	{
		____configuration_7 = value;
		Il2CppCodeGenWriteBarrier((&____configuration_7), value);
	}

	inline static int32_t get_offset_of_lockAllAttributesExcept_8() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ___lockAllAttributesExcept_8)); }
	inline ConfigurationLockCollection_t4066281341 * get_lockAllAttributesExcept_8() const { return ___lockAllAttributesExcept_8; }
	inline ConfigurationLockCollection_t4066281341 ** get_address_of_lockAllAttributesExcept_8() { return &___lockAllAttributesExcept_8; }
	inline void set_lockAllAttributesExcept_8(ConfigurationLockCollection_t4066281341 * value)
	{
		___lockAllAttributesExcept_8 = value;
		Il2CppCodeGenWriteBarrier((&___lockAllAttributesExcept_8), value);
	}

	inline static int32_t get_offset_of_lockAllElementsExcept_9() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ___lockAllElementsExcept_9)); }
	inline ConfigurationLockCollection_t4066281341 * get_lockAllElementsExcept_9() const { return ___lockAllElementsExcept_9; }
	inline ConfigurationLockCollection_t4066281341 ** get_address_of_lockAllElementsExcept_9() { return &___lockAllElementsExcept_9; }
	inline void set_lockAllElementsExcept_9(ConfigurationLockCollection_t4066281341 * value)
	{
		___lockAllElementsExcept_9 = value;
		Il2CppCodeGenWriteBarrier((&___lockAllElementsExcept_9), value);
	}

	inline static int32_t get_offset_of_lockAttributes_10() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ___lockAttributes_10)); }
	inline ConfigurationLockCollection_t4066281341 * get_lockAttributes_10() const { return ___lockAttributes_10; }
	inline ConfigurationLockCollection_t4066281341 ** get_address_of_lockAttributes_10() { return &___lockAttributes_10; }
	inline void set_lockAttributes_10(ConfigurationLockCollection_t4066281341 * value)
	{
		___lockAttributes_10 = value;
		Il2CppCodeGenWriteBarrier((&___lockAttributes_10), value);
	}

	inline static int32_t get_offset_of_lockElements_11() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ___lockElements_11)); }
	inline ConfigurationLockCollection_t4066281341 * get_lockElements_11() const { return ___lockElements_11; }
	inline ConfigurationLockCollection_t4066281341 ** get_address_of_lockElements_11() { return &___lockElements_11; }
	inline void set_lockElements_11(ConfigurationLockCollection_t4066281341 * value)
	{
		___lockElements_11 = value;
		Il2CppCodeGenWriteBarrier((&___lockElements_11), value);
	}

	inline static int32_t get_offset_of_lockItem_12() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ___lockItem_12)); }
	inline bool get_lockItem_12() const { return ___lockItem_12; }
	inline bool* get_address_of_lockItem_12() { return &___lockItem_12; }
	inline void set_lockItem_12(bool value)
	{
		___lockItem_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONELEMENT_T3318566633_H
#ifndef LOCALE_T4128636110_H
#define LOCALE_T4128636110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Locale
struct  Locale_t4128636110  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALE_T4128636110_H
#ifndef CONFIGURATION_T2529364143_H
#define CONFIGURATION_T2529364143_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.Configuration
struct  Configuration_t2529364143  : public RuntimeObject
{
public:
	// System.Configuration.Configuration System.Configuration.Configuration::parent
	Configuration_t2529364143 * ___parent_0;
	// System.Collections.Hashtable System.Configuration.Configuration::elementData
	Hashtable_t1853889766 * ___elementData_1;
	// System.String System.Configuration.Configuration::streamName
	String_t* ___streamName_2;
	// System.Configuration.ConfigurationSectionGroup System.Configuration.Configuration::rootSectionGroup
	ConfigurationSectionGroup_t4179402520 * ___rootSectionGroup_3;
	// System.Configuration.ConfigurationLocationCollection System.Configuration.Configuration::locations
	ConfigurationLocationCollection_t1538619295 * ___locations_4;
	// System.Configuration.SectionGroupInfo System.Configuration.Configuration::rootGroup
	SectionGroupInfo_t687896682 * ___rootGroup_5;
	// System.Configuration.Internal.IConfigSystem System.Configuration.Configuration::system
	RuntimeObject* ___system_6;
	// System.Boolean System.Configuration.Configuration::hasFile
	bool ___hasFile_7;
	// System.String System.Configuration.Configuration::rootNamespace
	String_t* ___rootNamespace_8;
	// System.String System.Configuration.Configuration::configPath
	String_t* ___configPath_9;
	// System.String System.Configuration.Configuration::locationConfigPath
	String_t* ___locationConfigPath_10;
	// System.String System.Configuration.Configuration::locationSubPath
	String_t* ___locationSubPath_11;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(Configuration_t2529364143, ___parent_0)); }
	inline Configuration_t2529364143 * get_parent_0() const { return ___parent_0; }
	inline Configuration_t2529364143 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(Configuration_t2529364143 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_elementData_1() { return static_cast<int32_t>(offsetof(Configuration_t2529364143, ___elementData_1)); }
	inline Hashtable_t1853889766 * get_elementData_1() const { return ___elementData_1; }
	inline Hashtable_t1853889766 ** get_address_of_elementData_1() { return &___elementData_1; }
	inline void set_elementData_1(Hashtable_t1853889766 * value)
	{
		___elementData_1 = value;
		Il2CppCodeGenWriteBarrier((&___elementData_1), value);
	}

	inline static int32_t get_offset_of_streamName_2() { return static_cast<int32_t>(offsetof(Configuration_t2529364143, ___streamName_2)); }
	inline String_t* get_streamName_2() const { return ___streamName_2; }
	inline String_t** get_address_of_streamName_2() { return &___streamName_2; }
	inline void set_streamName_2(String_t* value)
	{
		___streamName_2 = value;
		Il2CppCodeGenWriteBarrier((&___streamName_2), value);
	}

	inline static int32_t get_offset_of_rootSectionGroup_3() { return static_cast<int32_t>(offsetof(Configuration_t2529364143, ___rootSectionGroup_3)); }
	inline ConfigurationSectionGroup_t4179402520 * get_rootSectionGroup_3() const { return ___rootSectionGroup_3; }
	inline ConfigurationSectionGroup_t4179402520 ** get_address_of_rootSectionGroup_3() { return &___rootSectionGroup_3; }
	inline void set_rootSectionGroup_3(ConfigurationSectionGroup_t4179402520 * value)
	{
		___rootSectionGroup_3 = value;
		Il2CppCodeGenWriteBarrier((&___rootSectionGroup_3), value);
	}

	inline static int32_t get_offset_of_locations_4() { return static_cast<int32_t>(offsetof(Configuration_t2529364143, ___locations_4)); }
	inline ConfigurationLocationCollection_t1538619295 * get_locations_4() const { return ___locations_4; }
	inline ConfigurationLocationCollection_t1538619295 ** get_address_of_locations_4() { return &___locations_4; }
	inline void set_locations_4(ConfigurationLocationCollection_t1538619295 * value)
	{
		___locations_4 = value;
		Il2CppCodeGenWriteBarrier((&___locations_4), value);
	}

	inline static int32_t get_offset_of_rootGroup_5() { return static_cast<int32_t>(offsetof(Configuration_t2529364143, ___rootGroup_5)); }
	inline SectionGroupInfo_t687896682 * get_rootGroup_5() const { return ___rootGroup_5; }
	inline SectionGroupInfo_t687896682 ** get_address_of_rootGroup_5() { return &___rootGroup_5; }
	inline void set_rootGroup_5(SectionGroupInfo_t687896682 * value)
	{
		___rootGroup_5 = value;
		Il2CppCodeGenWriteBarrier((&___rootGroup_5), value);
	}

	inline static int32_t get_offset_of_system_6() { return static_cast<int32_t>(offsetof(Configuration_t2529364143, ___system_6)); }
	inline RuntimeObject* get_system_6() const { return ___system_6; }
	inline RuntimeObject** get_address_of_system_6() { return &___system_6; }
	inline void set_system_6(RuntimeObject* value)
	{
		___system_6 = value;
		Il2CppCodeGenWriteBarrier((&___system_6), value);
	}

	inline static int32_t get_offset_of_hasFile_7() { return static_cast<int32_t>(offsetof(Configuration_t2529364143, ___hasFile_7)); }
	inline bool get_hasFile_7() const { return ___hasFile_7; }
	inline bool* get_address_of_hasFile_7() { return &___hasFile_7; }
	inline void set_hasFile_7(bool value)
	{
		___hasFile_7 = value;
	}

	inline static int32_t get_offset_of_rootNamespace_8() { return static_cast<int32_t>(offsetof(Configuration_t2529364143, ___rootNamespace_8)); }
	inline String_t* get_rootNamespace_8() const { return ___rootNamespace_8; }
	inline String_t** get_address_of_rootNamespace_8() { return &___rootNamespace_8; }
	inline void set_rootNamespace_8(String_t* value)
	{
		___rootNamespace_8 = value;
		Il2CppCodeGenWriteBarrier((&___rootNamespace_8), value);
	}

	inline static int32_t get_offset_of_configPath_9() { return static_cast<int32_t>(offsetof(Configuration_t2529364143, ___configPath_9)); }
	inline String_t* get_configPath_9() const { return ___configPath_9; }
	inline String_t** get_address_of_configPath_9() { return &___configPath_9; }
	inline void set_configPath_9(String_t* value)
	{
		___configPath_9 = value;
		Il2CppCodeGenWriteBarrier((&___configPath_9), value);
	}

	inline static int32_t get_offset_of_locationConfigPath_10() { return static_cast<int32_t>(offsetof(Configuration_t2529364143, ___locationConfigPath_10)); }
	inline String_t* get_locationConfigPath_10() const { return ___locationConfigPath_10; }
	inline String_t** get_address_of_locationConfigPath_10() { return &___locationConfigPath_10; }
	inline void set_locationConfigPath_10(String_t* value)
	{
		___locationConfigPath_10 = value;
		Il2CppCodeGenWriteBarrier((&___locationConfigPath_10), value);
	}

	inline static int32_t get_offset_of_locationSubPath_11() { return static_cast<int32_t>(offsetof(Configuration_t2529364143, ___locationSubPath_11)); }
	inline String_t* get_locationSubPath_11() const { return ___locationSubPath_11; }
	inline String_t** get_address_of_locationSubPath_11() { return &___locationSubPath_11; }
	inline void set_locationSubPath_11(String_t* value)
	{
		___locationSubPath_11 = value;
		Il2CppCodeGenWriteBarrier((&___locationSubPath_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATION_T2529364143_H
#ifndef CONFIGINFO_T2364721456_H
#define CONFIGINFO_T2364721456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigInfo
struct  ConfigInfo_t2364721456  : public RuntimeObject
{
public:
	// System.String System.Configuration.ConfigInfo::Name
	String_t* ___Name_0;
	// System.String System.Configuration.ConfigInfo::TypeName
	String_t* ___TypeName_1;
	// System.Type System.Configuration.ConfigInfo::Type
	Type_t * ___Type_2;
	// System.String System.Configuration.ConfigInfo::streamName
	String_t* ___streamName_3;
	// System.Configuration.ConfigInfo System.Configuration.ConfigInfo::Parent
	ConfigInfo_t2364721456 * ___Parent_4;
	// System.Configuration.Internal.IInternalConfigHost System.Configuration.ConfigInfo::ConfigHost
	RuntimeObject* ___ConfigHost_5;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(ConfigInfo_t2364721456, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_TypeName_1() { return static_cast<int32_t>(offsetof(ConfigInfo_t2364721456, ___TypeName_1)); }
	inline String_t* get_TypeName_1() const { return ___TypeName_1; }
	inline String_t** get_address_of_TypeName_1() { return &___TypeName_1; }
	inline void set_TypeName_1(String_t* value)
	{
		___TypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___TypeName_1), value);
	}

	inline static int32_t get_offset_of_Type_2() { return static_cast<int32_t>(offsetof(ConfigInfo_t2364721456, ___Type_2)); }
	inline Type_t * get_Type_2() const { return ___Type_2; }
	inline Type_t ** get_address_of_Type_2() { return &___Type_2; }
	inline void set_Type_2(Type_t * value)
	{
		___Type_2 = value;
		Il2CppCodeGenWriteBarrier((&___Type_2), value);
	}

	inline static int32_t get_offset_of_streamName_3() { return static_cast<int32_t>(offsetof(ConfigInfo_t2364721456, ___streamName_3)); }
	inline String_t* get_streamName_3() const { return ___streamName_3; }
	inline String_t** get_address_of_streamName_3() { return &___streamName_3; }
	inline void set_streamName_3(String_t* value)
	{
		___streamName_3 = value;
		Il2CppCodeGenWriteBarrier((&___streamName_3), value);
	}

	inline static int32_t get_offset_of_Parent_4() { return static_cast<int32_t>(offsetof(ConfigInfo_t2364721456, ___Parent_4)); }
	inline ConfigInfo_t2364721456 * get_Parent_4() const { return ___Parent_4; }
	inline ConfigInfo_t2364721456 ** get_address_of_Parent_4() { return &___Parent_4; }
	inline void set_Parent_4(ConfigInfo_t2364721456 * value)
	{
		___Parent_4 = value;
		Il2CppCodeGenWriteBarrier((&___Parent_4), value);
	}

	inline static int32_t get_offset_of_ConfigHost_5() { return static_cast<int32_t>(offsetof(ConfigInfo_t2364721456, ___ConfigHost_5)); }
	inline RuntimeObject* get_ConfigHost_5() const { return ___ConfigHost_5; }
	inline RuntimeObject** get_address_of_ConfigHost_5() { return &___ConfigHost_5; }
	inline void set_ConfigHost_5(RuntimeObject* value)
	{
		___ConfigHost_5 = value;
		Il2CppCodeGenWriteBarrier((&___ConfigHost_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGINFO_T2364721456_H
#ifndef XMLREADER_T3121518892_H
#define XMLREADER_T3121518892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReader
struct  XmlReader_t3121518892  : public RuntimeObject
{
public:
	// System.Xml.XmlReaderBinarySupport System.Xml.XmlReader::binary
	XmlReaderBinarySupport_t1809665003 * ___binary_0;
	// System.Xml.XmlReaderSettings System.Xml.XmlReader::settings
	XmlReaderSettings_t2186285234 * ___settings_1;

public:
	inline static int32_t get_offset_of_binary_0() { return static_cast<int32_t>(offsetof(XmlReader_t3121518892, ___binary_0)); }
	inline XmlReaderBinarySupport_t1809665003 * get_binary_0() const { return ___binary_0; }
	inline XmlReaderBinarySupport_t1809665003 ** get_address_of_binary_0() { return &___binary_0; }
	inline void set_binary_0(XmlReaderBinarySupport_t1809665003 * value)
	{
		___binary_0 = value;
		Il2CppCodeGenWriteBarrier((&___binary_0), value);
	}

	inline static int32_t get_offset_of_settings_1() { return static_cast<int32_t>(offsetof(XmlReader_t3121518892, ___settings_1)); }
	inline XmlReaderSettings_t2186285234 * get_settings_1() const { return ___settings_1; }
	inline XmlReaderSettings_t2186285234 ** get_address_of_settings_1() { return &___settings_1; }
	inline void set_settings_1(XmlReaderSettings_t2186285234 * value)
	{
		___settings_1 = value;
		Il2CppCodeGenWriteBarrier((&___settings_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADER_T3121518892_H
#ifndef CLIENTCONFIGURATIONSYSTEM_T3254701293_H
#define CLIENTCONFIGURATIONSYSTEM_T3254701293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ClientConfigurationSystem
struct  ClientConfigurationSystem_t3254701293  : public RuntimeObject
{
public:
	// System.Configuration.Configuration System.Configuration.ClientConfigurationSystem::cfg
	Configuration_t2529364143 * ___cfg_0;

public:
	inline static int32_t get_offset_of_cfg_0() { return static_cast<int32_t>(offsetof(ClientConfigurationSystem_t3254701293, ___cfg_0)); }
	inline Configuration_t2529364143 * get_cfg_0() const { return ___cfg_0; }
	inline Configuration_t2529364143 ** get_address_of_cfg_0() { return &___cfg_0; }
	inline void set_cfg_0(Configuration_t2529364143 * value)
	{
		___cfg_0 = value;
		Il2CppCodeGenWriteBarrier((&___cfg_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTCONFIGURATIONSYSTEM_T3254701293_H
#ifndef PROVIDERCOLLECTION_T3810574769_H
#define PROVIDERCOLLECTION_T3810574769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.Provider.ProviderCollection
struct  ProviderCollection_t3810574769  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Configuration.Provider.ProviderCollection::lookup
	Hashtable_t1853889766 * ___lookup_0;
	// System.Boolean System.Configuration.Provider.ProviderCollection::readOnly
	bool ___readOnly_1;
	// System.Collections.ArrayList System.Configuration.Provider.ProviderCollection::values
	ArrayList_t2718874744 * ___values_2;

public:
	inline static int32_t get_offset_of_lookup_0() { return static_cast<int32_t>(offsetof(ProviderCollection_t3810574769, ___lookup_0)); }
	inline Hashtable_t1853889766 * get_lookup_0() const { return ___lookup_0; }
	inline Hashtable_t1853889766 ** get_address_of_lookup_0() { return &___lookup_0; }
	inline void set_lookup_0(Hashtable_t1853889766 * value)
	{
		___lookup_0 = value;
		Il2CppCodeGenWriteBarrier((&___lookup_0), value);
	}

	inline static int32_t get_offset_of_readOnly_1() { return static_cast<int32_t>(offsetof(ProviderCollection_t3810574769, ___readOnly_1)); }
	inline bool get_readOnly_1() const { return ___readOnly_1; }
	inline bool* get_address_of_readOnly_1() { return &___readOnly_1; }
	inline void set_readOnly_1(bool value)
	{
		___readOnly_1 = value;
	}

	inline static int32_t get_offset_of_values_2() { return static_cast<int32_t>(offsetof(ProviderCollection_t3810574769, ___values_2)); }
	inline ArrayList_t2718874744 * get_values_2() const { return ___values_2; }
	inline ArrayList_t2718874744 ** get_address_of_values_2() { return &___values_2; }
	inline void set_values_2(ArrayList_t2718874744 * value)
	{
		___values_2 = value;
		Il2CppCodeGenWriteBarrier((&___values_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERCOLLECTION_T3810574769_H
#ifndef PROVIDERBASE_T2594774949_H
#define PROVIDERBASE_T2594774949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.Provider.ProviderBase
struct  ProviderBase_t2594774949  : public RuntimeObject
{
public:
	// System.Boolean System.Configuration.Provider.ProviderBase::alreadyInitialized
	bool ___alreadyInitialized_0;
	// System.String System.Configuration.Provider.ProviderBase::_description
	String_t* ____description_1;
	// System.String System.Configuration.Provider.ProviderBase::_name
	String_t* ____name_2;

public:
	inline static int32_t get_offset_of_alreadyInitialized_0() { return static_cast<int32_t>(offsetof(ProviderBase_t2594774949, ___alreadyInitialized_0)); }
	inline bool get_alreadyInitialized_0() const { return ___alreadyInitialized_0; }
	inline bool* get_address_of_alreadyInitialized_0() { return &___alreadyInitialized_0; }
	inline void set_alreadyInitialized_0(bool value)
	{
		___alreadyInitialized_0 = value;
	}

	inline static int32_t get_offset_of__description_1() { return static_cast<int32_t>(offsetof(ProviderBase_t2594774949, ____description_1)); }
	inline String_t* get__description_1() const { return ____description_1; }
	inline String_t** get_address_of__description_1() { return &____description_1; }
	inline void set__description_1(String_t* value)
	{
		____description_1 = value;
		Il2CppCodeGenWriteBarrier((&____description_1), value);
	}

	inline static int32_t get_offset_of__name_2() { return static_cast<int32_t>(offsetof(ProviderBase_t2594774949, ____name_2)); }
	inline String_t* get__name_2() const { return ____name_2; }
	inline String_t** get_address_of__name_2() { return &____name_2; }
	inline void set__name_2(String_t* value)
	{
		____name_2 = value;
		Il2CppCodeGenWriteBarrier((&____name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERBASE_T2594774949_H
#ifndef CONFIGURATIONLOCATION_T372136312_H
#define CONFIGURATIONLOCATION_T372136312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationLocation
struct  ConfigurationLocation_t372136312  : public RuntimeObject
{
public:
	// System.String System.Configuration.ConfigurationLocation::path
	String_t* ___path_1;
	// System.Configuration.Configuration System.Configuration.ConfigurationLocation::configuration
	Configuration_t2529364143 * ___configuration_2;
	// System.Configuration.Configuration System.Configuration.ConfigurationLocation::parent
	Configuration_t2529364143 * ___parent_3;
	// System.String System.Configuration.ConfigurationLocation::xmlContent
	String_t* ___xmlContent_4;
	// System.Boolean System.Configuration.ConfigurationLocation::parentResolved
	bool ___parentResolved_5;
	// System.Boolean System.Configuration.ConfigurationLocation::allowOverride
	bool ___allowOverride_6;

public:
	inline static int32_t get_offset_of_path_1() { return static_cast<int32_t>(offsetof(ConfigurationLocation_t372136312, ___path_1)); }
	inline String_t* get_path_1() const { return ___path_1; }
	inline String_t** get_address_of_path_1() { return &___path_1; }
	inline void set_path_1(String_t* value)
	{
		___path_1 = value;
		Il2CppCodeGenWriteBarrier((&___path_1), value);
	}

	inline static int32_t get_offset_of_configuration_2() { return static_cast<int32_t>(offsetof(ConfigurationLocation_t372136312, ___configuration_2)); }
	inline Configuration_t2529364143 * get_configuration_2() const { return ___configuration_2; }
	inline Configuration_t2529364143 ** get_address_of_configuration_2() { return &___configuration_2; }
	inline void set_configuration_2(Configuration_t2529364143 * value)
	{
		___configuration_2 = value;
		Il2CppCodeGenWriteBarrier((&___configuration_2), value);
	}

	inline static int32_t get_offset_of_parent_3() { return static_cast<int32_t>(offsetof(ConfigurationLocation_t372136312, ___parent_3)); }
	inline Configuration_t2529364143 * get_parent_3() const { return ___parent_3; }
	inline Configuration_t2529364143 ** get_address_of_parent_3() { return &___parent_3; }
	inline void set_parent_3(Configuration_t2529364143 * value)
	{
		___parent_3 = value;
		Il2CppCodeGenWriteBarrier((&___parent_3), value);
	}

	inline static int32_t get_offset_of_xmlContent_4() { return static_cast<int32_t>(offsetof(ConfigurationLocation_t372136312, ___xmlContent_4)); }
	inline String_t* get_xmlContent_4() const { return ___xmlContent_4; }
	inline String_t** get_address_of_xmlContent_4() { return &___xmlContent_4; }
	inline void set_xmlContent_4(String_t* value)
	{
		___xmlContent_4 = value;
		Il2CppCodeGenWriteBarrier((&___xmlContent_4), value);
	}

	inline static int32_t get_offset_of_parentResolved_5() { return static_cast<int32_t>(offsetof(ConfigurationLocation_t372136312, ___parentResolved_5)); }
	inline bool get_parentResolved_5() const { return ___parentResolved_5; }
	inline bool* get_address_of_parentResolved_5() { return &___parentResolved_5; }
	inline void set_parentResolved_5(bool value)
	{
		___parentResolved_5 = value;
	}

	inline static int32_t get_offset_of_allowOverride_6() { return static_cast<int32_t>(offsetof(ConfigurationLocation_t372136312, ___allowOverride_6)); }
	inline bool get_allowOverride_6() const { return ___allowOverride_6; }
	inline bool* get_address_of_allowOverride_6() { return &___allowOverride_6; }
	inline void set_allowOverride_6(bool value)
	{
		___allowOverride_6 = value;
	}
};

struct ConfigurationLocation_t372136312_StaticFields
{
public:
	// System.Char[] System.Configuration.ConfigurationLocation::pathTrimChars
	CharU5BU5D_t3528271667* ___pathTrimChars_0;

public:
	inline static int32_t get_offset_of_pathTrimChars_0() { return static_cast<int32_t>(offsetof(ConfigurationLocation_t372136312_StaticFields, ___pathTrimChars_0)); }
	inline CharU5BU5D_t3528271667* get_pathTrimChars_0() const { return ___pathTrimChars_0; }
	inline CharU5BU5D_t3528271667** get_address_of_pathTrimChars_0() { return &___pathTrimChars_0; }
	inline void set_pathTrimChars_0(CharU5BU5D_t3528271667* value)
	{
		___pathTrimChars_0 = value;
		Il2CppCodeGenWriteBarrier((&___pathTrimChars_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONLOCATION_T372136312_H
#ifndef NAMEOBJECTCOLLECTIONBASE_T2091847364_H
#define NAMEOBJECTCOLLECTIONBASE_T2091847364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameObjectCollectionBase
struct  NameObjectCollectionBase_t2091847364  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Collections.Specialized.NameObjectCollectionBase::m_ItemsContainer
	Hashtable_t1853889766 * ___m_ItemsContainer_0;
	// System.Collections.Specialized.NameObjectCollectionBase/_Item System.Collections.Specialized.NameObjectCollectionBase::m_NullKeyItem
	_Item_t2272350267 * ___m_NullKeyItem_1;
	// System.Collections.ArrayList System.Collections.Specialized.NameObjectCollectionBase::m_ItemsArray
	ArrayList_t2718874744 * ___m_ItemsArray_2;
	// System.Collections.IHashCodeProvider System.Collections.Specialized.NameObjectCollectionBase::m_hashprovider
	RuntimeObject* ___m_hashprovider_3;
	// System.Collections.IComparer System.Collections.Specialized.NameObjectCollectionBase::m_comparer
	RuntimeObject* ___m_comparer_4;
	// System.Int32 System.Collections.Specialized.NameObjectCollectionBase::m_defCapacity
	int32_t ___m_defCapacity_5;
	// System.Boolean System.Collections.Specialized.NameObjectCollectionBase::m_readonly
	bool ___m_readonly_6;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Specialized.NameObjectCollectionBase::infoCopy
	SerializationInfo_t950877179 * ___infoCopy_7;
	// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection System.Collections.Specialized.NameObjectCollectionBase::keyscoll
	KeysCollection_t1318642398 * ___keyscoll_8;
	// System.Collections.IEqualityComparer System.Collections.Specialized.NameObjectCollectionBase::equality_comparer
	RuntimeObject* ___equality_comparer_9;

public:
	inline static int32_t get_offset_of_m_ItemsContainer_0() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___m_ItemsContainer_0)); }
	inline Hashtable_t1853889766 * get_m_ItemsContainer_0() const { return ___m_ItemsContainer_0; }
	inline Hashtable_t1853889766 ** get_address_of_m_ItemsContainer_0() { return &___m_ItemsContainer_0; }
	inline void set_m_ItemsContainer_0(Hashtable_t1853889766 * value)
	{
		___m_ItemsContainer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemsContainer_0), value);
	}

	inline static int32_t get_offset_of_m_NullKeyItem_1() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___m_NullKeyItem_1)); }
	inline _Item_t2272350267 * get_m_NullKeyItem_1() const { return ___m_NullKeyItem_1; }
	inline _Item_t2272350267 ** get_address_of_m_NullKeyItem_1() { return &___m_NullKeyItem_1; }
	inline void set_m_NullKeyItem_1(_Item_t2272350267 * value)
	{
		___m_NullKeyItem_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_NullKeyItem_1), value);
	}

	inline static int32_t get_offset_of_m_ItemsArray_2() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___m_ItemsArray_2)); }
	inline ArrayList_t2718874744 * get_m_ItemsArray_2() const { return ___m_ItemsArray_2; }
	inline ArrayList_t2718874744 ** get_address_of_m_ItemsArray_2() { return &___m_ItemsArray_2; }
	inline void set_m_ItemsArray_2(ArrayList_t2718874744 * value)
	{
		___m_ItemsArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemsArray_2), value);
	}

	inline static int32_t get_offset_of_m_hashprovider_3() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___m_hashprovider_3)); }
	inline RuntimeObject* get_m_hashprovider_3() const { return ___m_hashprovider_3; }
	inline RuntimeObject** get_address_of_m_hashprovider_3() { return &___m_hashprovider_3; }
	inline void set_m_hashprovider_3(RuntimeObject* value)
	{
		___m_hashprovider_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_hashprovider_3), value);
	}

	inline static int32_t get_offset_of_m_comparer_4() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___m_comparer_4)); }
	inline RuntimeObject* get_m_comparer_4() const { return ___m_comparer_4; }
	inline RuntimeObject** get_address_of_m_comparer_4() { return &___m_comparer_4; }
	inline void set_m_comparer_4(RuntimeObject* value)
	{
		___m_comparer_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_comparer_4), value);
	}

	inline static int32_t get_offset_of_m_defCapacity_5() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___m_defCapacity_5)); }
	inline int32_t get_m_defCapacity_5() const { return ___m_defCapacity_5; }
	inline int32_t* get_address_of_m_defCapacity_5() { return &___m_defCapacity_5; }
	inline void set_m_defCapacity_5(int32_t value)
	{
		___m_defCapacity_5 = value;
	}

	inline static int32_t get_offset_of_m_readonly_6() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___m_readonly_6)); }
	inline bool get_m_readonly_6() const { return ___m_readonly_6; }
	inline bool* get_address_of_m_readonly_6() { return &___m_readonly_6; }
	inline void set_m_readonly_6(bool value)
	{
		___m_readonly_6 = value;
	}

	inline static int32_t get_offset_of_infoCopy_7() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___infoCopy_7)); }
	inline SerializationInfo_t950877179 * get_infoCopy_7() const { return ___infoCopy_7; }
	inline SerializationInfo_t950877179 ** get_address_of_infoCopy_7() { return &___infoCopy_7; }
	inline void set_infoCopy_7(SerializationInfo_t950877179 * value)
	{
		___infoCopy_7 = value;
		Il2CppCodeGenWriteBarrier((&___infoCopy_7), value);
	}

	inline static int32_t get_offset_of_keyscoll_8() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___keyscoll_8)); }
	inline KeysCollection_t1318642398 * get_keyscoll_8() const { return ___keyscoll_8; }
	inline KeysCollection_t1318642398 ** get_address_of_keyscoll_8() { return &___keyscoll_8; }
	inline void set_keyscoll_8(KeysCollection_t1318642398 * value)
	{
		___keyscoll_8 = value;
		Il2CppCodeGenWriteBarrier((&___keyscoll_8), value);
	}

	inline static int32_t get_offset_of_equality_comparer_9() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___equality_comparer_9)); }
	inline RuntimeObject* get_equality_comparer_9() const { return ___equality_comparer_9; }
	inline RuntimeObject** get_address_of_equality_comparer_9() { return &___equality_comparer_9; }
	inline void set_equality_comparer_9(RuntimeObject* value)
	{
		___equality_comparer_9 = value;
		Il2CppCodeGenWriteBarrier((&___equality_comparer_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEOBJECTCOLLECTIONBASE_T2091847364_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef SYMMETRICTRANSFORM_T3802591843_H
#define SYMMETRICTRANSFORM_T3802591843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.SymmetricTransform
struct  SymmetricTransform_t3802591843  : public RuntimeObject
{
public:
	// System.Security.Cryptography.SymmetricAlgorithm Mono.Security.Cryptography.SymmetricTransform::algo
	SymmetricAlgorithm_t4254223087 * ___algo_0;
	// System.Boolean Mono.Security.Cryptography.SymmetricTransform::encrypt
	bool ___encrypt_1;
	// System.Int32 Mono.Security.Cryptography.SymmetricTransform::BlockSizeByte
	int32_t ___BlockSizeByte_2;
	// System.Byte[] Mono.Security.Cryptography.SymmetricTransform::temp
	ByteU5BU5D_t4116647657* ___temp_3;
	// System.Byte[] Mono.Security.Cryptography.SymmetricTransform::temp2
	ByteU5BU5D_t4116647657* ___temp2_4;
	// System.Byte[] Mono.Security.Cryptography.SymmetricTransform::workBuff
	ByteU5BU5D_t4116647657* ___workBuff_5;
	// System.Byte[] Mono.Security.Cryptography.SymmetricTransform::workout
	ByteU5BU5D_t4116647657* ___workout_6;
	// System.Int32 Mono.Security.Cryptography.SymmetricTransform::FeedBackByte
	int32_t ___FeedBackByte_7;
	// System.Int32 Mono.Security.Cryptography.SymmetricTransform::FeedBackIter
	int32_t ___FeedBackIter_8;
	// System.Boolean Mono.Security.Cryptography.SymmetricTransform::m_disposed
	bool ___m_disposed_9;
	// System.Boolean Mono.Security.Cryptography.SymmetricTransform::lastBlock
	bool ___lastBlock_10;
	// System.Security.Cryptography.RandomNumberGenerator Mono.Security.Cryptography.SymmetricTransform::_rng
	RandomNumberGenerator_t386037858 * ____rng_11;

public:
	inline static int32_t get_offset_of_algo_0() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591843, ___algo_0)); }
	inline SymmetricAlgorithm_t4254223087 * get_algo_0() const { return ___algo_0; }
	inline SymmetricAlgorithm_t4254223087 ** get_address_of_algo_0() { return &___algo_0; }
	inline void set_algo_0(SymmetricAlgorithm_t4254223087 * value)
	{
		___algo_0 = value;
		Il2CppCodeGenWriteBarrier((&___algo_0), value);
	}

	inline static int32_t get_offset_of_encrypt_1() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591843, ___encrypt_1)); }
	inline bool get_encrypt_1() const { return ___encrypt_1; }
	inline bool* get_address_of_encrypt_1() { return &___encrypt_1; }
	inline void set_encrypt_1(bool value)
	{
		___encrypt_1 = value;
	}

	inline static int32_t get_offset_of_BlockSizeByte_2() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591843, ___BlockSizeByte_2)); }
	inline int32_t get_BlockSizeByte_2() const { return ___BlockSizeByte_2; }
	inline int32_t* get_address_of_BlockSizeByte_2() { return &___BlockSizeByte_2; }
	inline void set_BlockSizeByte_2(int32_t value)
	{
		___BlockSizeByte_2 = value;
	}

	inline static int32_t get_offset_of_temp_3() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591843, ___temp_3)); }
	inline ByteU5BU5D_t4116647657* get_temp_3() const { return ___temp_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_temp_3() { return &___temp_3; }
	inline void set_temp_3(ByteU5BU5D_t4116647657* value)
	{
		___temp_3 = value;
		Il2CppCodeGenWriteBarrier((&___temp_3), value);
	}

	inline static int32_t get_offset_of_temp2_4() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591843, ___temp2_4)); }
	inline ByteU5BU5D_t4116647657* get_temp2_4() const { return ___temp2_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_temp2_4() { return &___temp2_4; }
	inline void set_temp2_4(ByteU5BU5D_t4116647657* value)
	{
		___temp2_4 = value;
		Il2CppCodeGenWriteBarrier((&___temp2_4), value);
	}

	inline static int32_t get_offset_of_workBuff_5() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591843, ___workBuff_5)); }
	inline ByteU5BU5D_t4116647657* get_workBuff_5() const { return ___workBuff_5; }
	inline ByteU5BU5D_t4116647657** get_address_of_workBuff_5() { return &___workBuff_5; }
	inline void set_workBuff_5(ByteU5BU5D_t4116647657* value)
	{
		___workBuff_5 = value;
		Il2CppCodeGenWriteBarrier((&___workBuff_5), value);
	}

	inline static int32_t get_offset_of_workout_6() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591843, ___workout_6)); }
	inline ByteU5BU5D_t4116647657* get_workout_6() const { return ___workout_6; }
	inline ByteU5BU5D_t4116647657** get_address_of_workout_6() { return &___workout_6; }
	inline void set_workout_6(ByteU5BU5D_t4116647657* value)
	{
		___workout_6 = value;
		Il2CppCodeGenWriteBarrier((&___workout_6), value);
	}

	inline static int32_t get_offset_of_FeedBackByte_7() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591843, ___FeedBackByte_7)); }
	inline int32_t get_FeedBackByte_7() const { return ___FeedBackByte_7; }
	inline int32_t* get_address_of_FeedBackByte_7() { return &___FeedBackByte_7; }
	inline void set_FeedBackByte_7(int32_t value)
	{
		___FeedBackByte_7 = value;
	}

	inline static int32_t get_offset_of_FeedBackIter_8() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591843, ___FeedBackIter_8)); }
	inline int32_t get_FeedBackIter_8() const { return ___FeedBackIter_8; }
	inline int32_t* get_address_of_FeedBackIter_8() { return &___FeedBackIter_8; }
	inline void set_FeedBackIter_8(int32_t value)
	{
		___FeedBackIter_8 = value;
	}

	inline static int32_t get_offset_of_m_disposed_9() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591843, ___m_disposed_9)); }
	inline bool get_m_disposed_9() const { return ___m_disposed_9; }
	inline bool* get_address_of_m_disposed_9() { return &___m_disposed_9; }
	inline void set_m_disposed_9(bool value)
	{
		___m_disposed_9 = value;
	}

	inline static int32_t get_offset_of_lastBlock_10() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591843, ___lastBlock_10)); }
	inline bool get_lastBlock_10() const { return ___lastBlock_10; }
	inline bool* get_address_of_lastBlock_10() { return &___lastBlock_10; }
	inline void set_lastBlock_10(bool value)
	{
		___lastBlock_10 = value;
	}

	inline static int32_t get_offset_of__rng_11() { return static_cast<int32_t>(offsetof(SymmetricTransform_t3802591843, ____rng_11)); }
	inline RandomNumberGenerator_t386037858 * get__rng_11() const { return ____rng_11; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__rng_11() { return &____rng_11; }
	inline void set__rng_11(RandomNumberGenerator_t386037858 * value)
	{
		____rng_11 = value;
		Il2CppCodeGenWriteBarrier((&____rng_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMMETRICTRANSFORM_T3802591843_H
#ifndef CONFIGURATIONFILEMAP_T3010527243_H
#define CONFIGURATIONFILEMAP_T3010527243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationFileMap
struct  ConfigurationFileMap_t3010527243  : public RuntimeObject
{
public:
	// System.String System.Configuration.ConfigurationFileMap::machineConfigFilename
	String_t* ___machineConfigFilename_0;

public:
	inline static int32_t get_offset_of_machineConfigFilename_0() { return static_cast<int32_t>(offsetof(ConfigurationFileMap_t3010527243, ___machineConfigFilename_0)); }
	inline String_t* get_machineConfigFilename_0() const { return ___machineConfigFilename_0; }
	inline String_t** get_address_of_machineConfigFilename_0() { return &___machineConfigFilename_0; }
	inline void set_machineConfigFilename_0(String_t* value)
	{
		___machineConfigFilename_0 = value;
		Il2CppCodeGenWriteBarrier((&___machineConfigFilename_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONFILEMAP_T3010527243_H
#ifndef PROPERTYINFORMATIONCOLLECTION_T3866802480_H
#define PROPERTYINFORMATIONCOLLECTION_T3866802480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.PropertyInformationCollection
struct  PropertyInformationCollection_t3866802480  : public NameObjectCollectionBase_t2091847364
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYINFORMATIONCOLLECTION_T3866802480_H
#ifndef MONOTODOATTRIBUTE_T4131080584_H
#define MONOTODOATTRIBUTE_T4131080584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MonoTODOAttribute
struct  MonoTODOAttribute_t4131080584  : public Attribute_t861562559
{
public:
	// System.String System.MonoTODOAttribute::comment
	String_t* ___comment_0;

public:
	inline static int32_t get_offset_of_comment_0() { return static_cast<int32_t>(offsetof(MonoTODOAttribute_t4131080584, ___comment_0)); }
	inline String_t* get_comment_0() const { return ___comment_0; }
	inline String_t** get_address_of_comment_0() { return &___comment_0; }
	inline void set_comment_0(String_t* value)
	{
		___comment_0 = value;
		Il2CppCodeGenWriteBarrier((&___comment_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTODOATTRIBUTE_T4131080584_H
#ifndef CONFIGINFOCOLLECTION_T215413917_H
#define CONFIGINFOCOLLECTION_T215413917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigInfoCollection
struct  ConfigInfoCollection_t215413917  : public NameObjectCollectionBase_t2091847364
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGINFOCOLLECTION_T215413917_H
#ifndef INVALIDTIMEZONEEXCEPTION_T4066718130_H
#define INVALIDTIMEZONEEXCEPTION_T4066718130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidTimeZoneException
struct  InvalidTimeZoneException_t4066718130  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDTIMEZONEEXCEPTION_T4066718130_H
#ifndef PROTECTEDCONFIGURATIONPROVIDER_T413290519_H
#define PROTECTEDCONFIGURATIONPROVIDER_T413290519_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ProtectedConfigurationProvider
struct  ProtectedConfigurationProvider_t413290519  : public ProviderBase_t2594774949
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTECTEDCONFIGURATIONPROVIDER_T413290519_H
#ifndef TIMEZONENOTFOUNDEXCEPTION_T171073523_H
#define TIMEZONENOTFOUNDEXCEPTION_T171073523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeZoneNotFoundException
struct  TimeZoneNotFoundException_t171073523  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEZONENOTFOUNDEXCEPTION_T171073523_H
#ifndef PROTECTEDCONFIGURATIONPROVIDERCOLLECTION_T729952796_H
#define PROTECTEDCONFIGURATIONPROVIDERCOLLECTION_T729952796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ProtectedConfigurationProviderCollection
struct  ProtectedConfigurationProviderCollection_t729952796  : public ProviderCollection_t3810574769
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTECTEDCONFIGURATIONPROVIDERCOLLECTION_T729952796_H
#ifndef TIMETYPE_T2507513283_H
#define TIMETYPE_T2507513283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeZoneInfo/TimeType
struct  TimeType_t2507513283 
{
public:
	// System.Int32 System.TimeZoneInfo/TimeType::Offset
	int32_t ___Offset_0;
	// System.Boolean System.TimeZoneInfo/TimeType::IsDst
	bool ___IsDst_1;
	// System.String System.TimeZoneInfo/TimeType::Name
	String_t* ___Name_2;

public:
	inline static int32_t get_offset_of_Offset_0() { return static_cast<int32_t>(offsetof(TimeType_t2507513283, ___Offset_0)); }
	inline int32_t get_Offset_0() const { return ___Offset_0; }
	inline int32_t* get_address_of_Offset_0() { return &___Offset_0; }
	inline void set_Offset_0(int32_t value)
	{
		___Offset_0 = value;
	}

	inline static int32_t get_offset_of_IsDst_1() { return static_cast<int32_t>(offsetof(TimeType_t2507513283, ___IsDst_1)); }
	inline bool get_IsDst_1() const { return ___IsDst_1; }
	inline bool* get_address_of_IsDst_1() { return &___IsDst_1; }
	inline void set_IsDst_1(bool value)
	{
		___IsDst_1 = value;
	}

	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(TimeType_t2507513283, ___Name_2)); }
	inline String_t* get_Name_2() const { return ___Name_2; }
	inline String_t** get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(String_t* value)
	{
		___Name_2 = value;
		Il2CppCodeGenWriteBarrier((&___Name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.TimeZoneInfo/TimeType
struct TimeType_t2507513283_marshaled_pinvoke
{
	int32_t ___Offset_0;
	int32_t ___IsDst_1;
	char* ___Name_2;
};
// Native definition for COM marshalling of System.TimeZoneInfo/TimeType
struct TimeType_t2507513283_marshaled_com
{
	int32_t ___Offset_0;
	int32_t ___IsDst_1;
	Il2CppChar* ___Name_2;
};
#endif // TIMETYPE_T2507513283_H
#ifndef EXTENSIONATTRIBUTE_T1723066603_H
#define EXTENSIONATTRIBUTE_T1723066603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.ExtensionAttribute
struct  ExtensionAttribute_t1723066603  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONATTRIBUTE_T1723066603_H
#ifndef PROVIDERSETTINGS_T4011220768_H
#define PROVIDERSETTINGS_T4011220768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ProviderSettings
struct  ProviderSettings_t4011220768  : public ConfigurationElement_t3318566633
{
public:
	// System.Configuration.ConfigNameValueCollection System.Configuration.ProviderSettings::parameters
	ConfigNameValueCollection_t1573003828 * ___parameters_13;

public:
	inline static int32_t get_offset_of_parameters_13() { return static_cast<int32_t>(offsetof(ProviderSettings_t4011220768, ___parameters_13)); }
	inline ConfigNameValueCollection_t1573003828 * get_parameters_13() const { return ___parameters_13; }
	inline ConfigNameValueCollection_t1573003828 ** get_address_of_parameters_13() { return &___parameters_13; }
	inline void set_parameters_13(ConfigNameValueCollection_t1573003828 * value)
	{
		___parameters_13 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_13), value);
	}
};

struct ProviderSettings_t4011220768_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Configuration.ProviderSettings::nameProp
	ConfigurationProperty_t3590861854 * ___nameProp_14;
	// System.Configuration.ConfigurationProperty System.Configuration.ProviderSettings::typeProp
	ConfigurationProperty_t3590861854 * ___typeProp_15;
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.ProviderSettings::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_16;

public:
	inline static int32_t get_offset_of_nameProp_14() { return static_cast<int32_t>(offsetof(ProviderSettings_t4011220768_StaticFields, ___nameProp_14)); }
	inline ConfigurationProperty_t3590861854 * get_nameProp_14() const { return ___nameProp_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_nameProp_14() { return &___nameProp_14; }
	inline void set_nameProp_14(ConfigurationProperty_t3590861854 * value)
	{
		___nameProp_14 = value;
		Il2CppCodeGenWriteBarrier((&___nameProp_14), value);
	}

	inline static int32_t get_offset_of_typeProp_15() { return static_cast<int32_t>(offsetof(ProviderSettings_t4011220768_StaticFields, ___typeProp_15)); }
	inline ConfigurationProperty_t3590861854 * get_typeProp_15() const { return ___typeProp_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_typeProp_15() { return &___typeProp_15; }
	inline void set_typeProp_15(ConfigurationProperty_t3590861854 * value)
	{
		___typeProp_15 = value;
		Il2CppCodeGenWriteBarrier((&___typeProp_15), value);
	}

	inline static int32_t get_offset_of_properties_16() { return static_cast<int32_t>(offsetof(ProviderSettings_t4011220768_StaticFields, ___properties_16)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_16() const { return ___properties_16; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_16() { return &___properties_16; }
	inline void set_properties_16(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_16 = value;
		Il2CppCodeGenWriteBarrier((&___properties_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERSETTINGS_T4011220768_H
#ifndef SECTIONGROUPINFO_T687896682_H
#define SECTIONGROUPINFO_T687896682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.SectionGroupInfo
struct  SectionGroupInfo_t687896682  : public ConfigInfo_t2364721456
{
public:
	// System.Configuration.ConfigInfoCollection System.Configuration.SectionGroupInfo::sections
	ConfigInfoCollection_t215413917 * ___sections_6;
	// System.Configuration.ConfigInfoCollection System.Configuration.SectionGroupInfo::groups
	ConfigInfoCollection_t215413917 * ___groups_7;

public:
	inline static int32_t get_offset_of_sections_6() { return static_cast<int32_t>(offsetof(SectionGroupInfo_t687896682, ___sections_6)); }
	inline ConfigInfoCollection_t215413917 * get_sections_6() const { return ___sections_6; }
	inline ConfigInfoCollection_t215413917 ** get_address_of_sections_6() { return &___sections_6; }
	inline void set_sections_6(ConfigInfoCollection_t215413917 * value)
	{
		___sections_6 = value;
		Il2CppCodeGenWriteBarrier((&___sections_6), value);
	}

	inline static int32_t get_offset_of_groups_7() { return static_cast<int32_t>(offsetof(SectionGroupInfo_t687896682, ___groups_7)); }
	inline ConfigInfoCollection_t215413917 * get_groups_7() const { return ___groups_7; }
	inline ConfigInfoCollection_t215413917 ** get_address_of_groups_7() { return &___groups_7; }
	inline void set_groups_7(ConfigInfoCollection_t215413917 * value)
	{
		___groups_7 = value;
		Il2CppCodeGenWriteBarrier((&___groups_7), value);
	}
};

struct SectionGroupInfo_t687896682_StaticFields
{
public:
	// System.Configuration.ConfigInfoCollection System.Configuration.SectionGroupInfo::emptyList
	ConfigInfoCollection_t215413917 * ___emptyList_8;

public:
	inline static int32_t get_offset_of_emptyList_8() { return static_cast<int32_t>(offsetof(SectionGroupInfo_t687896682_StaticFields, ___emptyList_8)); }
	inline ConfigInfoCollection_t215413917 * get_emptyList_8() const { return ___emptyList_8; }
	inline ConfigInfoCollection_t215413917 ** get_address_of_emptyList_8() { return &___emptyList_8; }
	inline void set_emptyList_8(ConfigInfoCollection_t215413917 * value)
	{
		___emptyList_8 = value;
		Il2CppCodeGenWriteBarrier((&___emptyList_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECTIONGROUPINFO_T687896682_H
#ifndef MONOTODOATTRIBUTE_T4131080585_H
#define MONOTODOATTRIBUTE_T4131080585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MonoTODOAttribute
struct  MonoTODOAttribute_t4131080585  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTODOATTRIBUTE_T4131080585_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef CONFIGURATIONLOCATIONCOLLECTION_T1538619295_H
#define CONFIGURATIONLOCATIONCOLLECTION_T1538619295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationLocationCollection
struct  ConfigurationLocationCollection_t1538619295  : public ReadOnlyCollectionBase_t1836743899
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONLOCATIONCOLLECTION_T1538619295_H
#ifndef XMLTEXTREADER_T4233384356_H
#define XMLTEXTREADER_T4233384356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReader
struct  XmlTextReader_t4233384356  : public XmlReader_t3121518892
{
public:
	// System.Xml.XmlTextReader System.Xml.XmlTextReader::entity
	XmlTextReader_t4233384356 * ___entity_2;
	// Mono.Xml2.XmlTextReader System.Xml.XmlTextReader::source
	XmlTextReader_t3455035481 * ___source_3;
	// System.Boolean System.Xml.XmlTextReader::entityInsideAttribute
	bool ___entityInsideAttribute_4;
	// System.Boolean System.Xml.XmlTextReader::insideAttribute
	bool ___insideAttribute_5;
	// System.Collections.Generic.Stack`1<System.String> System.Xml.XmlTextReader::entityNameStack
	Stack_1_t2690840144 * ___entityNameStack_6;

public:
	inline static int32_t get_offset_of_entity_2() { return static_cast<int32_t>(offsetof(XmlTextReader_t4233384356, ___entity_2)); }
	inline XmlTextReader_t4233384356 * get_entity_2() const { return ___entity_2; }
	inline XmlTextReader_t4233384356 ** get_address_of_entity_2() { return &___entity_2; }
	inline void set_entity_2(XmlTextReader_t4233384356 * value)
	{
		___entity_2 = value;
		Il2CppCodeGenWriteBarrier((&___entity_2), value);
	}

	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(XmlTextReader_t4233384356, ___source_3)); }
	inline XmlTextReader_t3455035481 * get_source_3() const { return ___source_3; }
	inline XmlTextReader_t3455035481 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(XmlTextReader_t3455035481 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((&___source_3), value);
	}

	inline static int32_t get_offset_of_entityInsideAttribute_4() { return static_cast<int32_t>(offsetof(XmlTextReader_t4233384356, ___entityInsideAttribute_4)); }
	inline bool get_entityInsideAttribute_4() const { return ___entityInsideAttribute_4; }
	inline bool* get_address_of_entityInsideAttribute_4() { return &___entityInsideAttribute_4; }
	inline void set_entityInsideAttribute_4(bool value)
	{
		___entityInsideAttribute_4 = value;
	}

	inline static int32_t get_offset_of_insideAttribute_5() { return static_cast<int32_t>(offsetof(XmlTextReader_t4233384356, ___insideAttribute_5)); }
	inline bool get_insideAttribute_5() const { return ___insideAttribute_5; }
	inline bool* get_address_of_insideAttribute_5() { return &___insideAttribute_5; }
	inline void set_insideAttribute_5(bool value)
	{
		___insideAttribute_5 = value;
	}

	inline static int32_t get_offset_of_entityNameStack_6() { return static_cast<int32_t>(offsetof(XmlTextReader_t4233384356, ___entityNameStack_6)); }
	inline Stack_1_t2690840144 * get_entityNameStack_6() const { return ___entityNameStack_6; }
	inline Stack_1_t2690840144 ** get_address_of_entityNameStack_6() { return &___entityNameStack_6; }
	inline void set_entityNameStack_6(Stack_1_t2690840144 * value)
	{
		___entityNameStack_6 = value;
		Il2CppCodeGenWriteBarrier((&___entityNameStack_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTREADER_T4233384356_H
#ifndef CONFIGURATIONREMOVEELEMENT_T1327025031_H
#define CONFIGURATIONREMOVEELEMENT_T1327025031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationElementCollection/ConfigurationRemoveElement
struct  ConfigurationRemoveElement_t1327025031  : public ConfigurationElement_t3318566633
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.ConfigurationElementCollection/ConfigurationRemoveElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationElement System.Configuration.ConfigurationElementCollection/ConfigurationRemoveElement::_origElement
	ConfigurationElement_t3318566633 * ____origElement_14;
	// System.Configuration.ConfigurationElementCollection System.Configuration.ConfigurationElementCollection/ConfigurationRemoveElement::_origCollection
	ConfigurationElementCollection_t446763386 * ____origCollection_15;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(ConfigurationRemoveElement_t1327025031, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier((&___properties_13), value);
	}

	inline static int32_t get_offset_of__origElement_14() { return static_cast<int32_t>(offsetof(ConfigurationRemoveElement_t1327025031, ____origElement_14)); }
	inline ConfigurationElement_t3318566633 * get__origElement_14() const { return ____origElement_14; }
	inline ConfigurationElement_t3318566633 ** get_address_of__origElement_14() { return &____origElement_14; }
	inline void set__origElement_14(ConfigurationElement_t3318566633 * value)
	{
		____origElement_14 = value;
		Il2CppCodeGenWriteBarrier((&____origElement_14), value);
	}

	inline static int32_t get_offset_of__origCollection_15() { return static_cast<int32_t>(offsetof(ConfigurationRemoveElement_t1327025031, ____origCollection_15)); }
	inline ConfigurationElementCollection_t446763386 * get__origCollection_15() const { return ____origCollection_15; }
	inline ConfigurationElementCollection_t446763386 ** get_address_of__origCollection_15() { return &____origCollection_15; }
	inline void set__origCollection_15(ConfigurationElementCollection_t446763386 * value)
	{
		____origCollection_15 = value;
		Il2CppCodeGenWriteBarrier((&____origCollection_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONREMOVEELEMENT_T1327025031_H
#ifndef CONFIGURATIONELEMENTCOLLECTION_T446763386_H
#define CONFIGURATIONELEMENTCOLLECTION_T446763386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationElementCollection
struct  ConfigurationElementCollection_t446763386  : public ConfigurationElement_t3318566633
{
public:
	// System.Collections.ArrayList System.Configuration.ConfigurationElementCollection::list
	ArrayList_t2718874744 * ___list_13;
	// System.Collections.ArrayList System.Configuration.ConfigurationElementCollection::removed
	ArrayList_t2718874744 * ___removed_14;
	// System.Collections.ArrayList System.Configuration.ConfigurationElementCollection::inherited
	ArrayList_t2718874744 * ___inherited_15;
	// System.Boolean System.Configuration.ConfigurationElementCollection::emitClear
	bool ___emitClear_16;
	// System.Boolean System.Configuration.ConfigurationElementCollection::modified
	bool ___modified_17;
	// System.Collections.IComparer System.Configuration.ConfigurationElementCollection::comparer
	RuntimeObject* ___comparer_18;
	// System.Int32 System.Configuration.ConfigurationElementCollection::inheritedLimitIndex
	int32_t ___inheritedLimitIndex_19;
	// System.String System.Configuration.ConfigurationElementCollection::addElementName
	String_t* ___addElementName_20;
	// System.String System.Configuration.ConfigurationElementCollection::clearElementName
	String_t* ___clearElementName_21;
	// System.String System.Configuration.ConfigurationElementCollection::removeElementName
	String_t* ___removeElementName_22;

public:
	inline static int32_t get_offset_of_list_13() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t446763386, ___list_13)); }
	inline ArrayList_t2718874744 * get_list_13() const { return ___list_13; }
	inline ArrayList_t2718874744 ** get_address_of_list_13() { return &___list_13; }
	inline void set_list_13(ArrayList_t2718874744 * value)
	{
		___list_13 = value;
		Il2CppCodeGenWriteBarrier((&___list_13), value);
	}

	inline static int32_t get_offset_of_removed_14() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t446763386, ___removed_14)); }
	inline ArrayList_t2718874744 * get_removed_14() const { return ___removed_14; }
	inline ArrayList_t2718874744 ** get_address_of_removed_14() { return &___removed_14; }
	inline void set_removed_14(ArrayList_t2718874744 * value)
	{
		___removed_14 = value;
		Il2CppCodeGenWriteBarrier((&___removed_14), value);
	}

	inline static int32_t get_offset_of_inherited_15() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t446763386, ___inherited_15)); }
	inline ArrayList_t2718874744 * get_inherited_15() const { return ___inherited_15; }
	inline ArrayList_t2718874744 ** get_address_of_inherited_15() { return &___inherited_15; }
	inline void set_inherited_15(ArrayList_t2718874744 * value)
	{
		___inherited_15 = value;
		Il2CppCodeGenWriteBarrier((&___inherited_15), value);
	}

	inline static int32_t get_offset_of_emitClear_16() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t446763386, ___emitClear_16)); }
	inline bool get_emitClear_16() const { return ___emitClear_16; }
	inline bool* get_address_of_emitClear_16() { return &___emitClear_16; }
	inline void set_emitClear_16(bool value)
	{
		___emitClear_16 = value;
	}

	inline static int32_t get_offset_of_modified_17() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t446763386, ___modified_17)); }
	inline bool get_modified_17() const { return ___modified_17; }
	inline bool* get_address_of_modified_17() { return &___modified_17; }
	inline void set_modified_17(bool value)
	{
		___modified_17 = value;
	}

	inline static int32_t get_offset_of_comparer_18() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t446763386, ___comparer_18)); }
	inline RuntimeObject* get_comparer_18() const { return ___comparer_18; }
	inline RuntimeObject** get_address_of_comparer_18() { return &___comparer_18; }
	inline void set_comparer_18(RuntimeObject* value)
	{
		___comparer_18 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_18), value);
	}

	inline static int32_t get_offset_of_inheritedLimitIndex_19() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t446763386, ___inheritedLimitIndex_19)); }
	inline int32_t get_inheritedLimitIndex_19() const { return ___inheritedLimitIndex_19; }
	inline int32_t* get_address_of_inheritedLimitIndex_19() { return &___inheritedLimitIndex_19; }
	inline void set_inheritedLimitIndex_19(int32_t value)
	{
		___inheritedLimitIndex_19 = value;
	}

	inline static int32_t get_offset_of_addElementName_20() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t446763386, ___addElementName_20)); }
	inline String_t* get_addElementName_20() const { return ___addElementName_20; }
	inline String_t** get_address_of_addElementName_20() { return &___addElementName_20; }
	inline void set_addElementName_20(String_t* value)
	{
		___addElementName_20 = value;
		Il2CppCodeGenWriteBarrier((&___addElementName_20), value);
	}

	inline static int32_t get_offset_of_clearElementName_21() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t446763386, ___clearElementName_21)); }
	inline String_t* get_clearElementName_21() const { return ___clearElementName_21; }
	inline String_t** get_address_of_clearElementName_21() { return &___clearElementName_21; }
	inline void set_clearElementName_21(String_t* value)
	{
		___clearElementName_21 = value;
		Il2CppCodeGenWriteBarrier((&___clearElementName_21), value);
	}

	inline static int32_t get_offset_of_removeElementName_22() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t446763386, ___removeElementName_22)); }
	inline String_t* get_removeElementName_22() const { return ___removeElementName_22; }
	inline String_t** get_address_of_removeElementName_22() { return &___removeElementName_22; }
	inline void set_removeElementName_22(String_t* value)
	{
		___removeElementName_22 = value;
		Il2CppCodeGenWriteBarrier((&___removeElementName_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONELEMENTCOLLECTION_T446763386_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef NAMEVALUECOLLECTION_T407452768_H
#define NAMEVALUECOLLECTION_T407452768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameValueCollection
struct  NameValueCollection_t407452768  : public NameObjectCollectionBase_t2091847364
{
public:
	// System.String[] System.Collections.Specialized.NameValueCollection::cachedAllKeys
	StringU5BU5D_t1281789340* ___cachedAllKeys_10;
	// System.String[] System.Collections.Specialized.NameValueCollection::cachedAll
	StringU5BU5D_t1281789340* ___cachedAll_11;

public:
	inline static int32_t get_offset_of_cachedAllKeys_10() { return static_cast<int32_t>(offsetof(NameValueCollection_t407452768, ___cachedAllKeys_10)); }
	inline StringU5BU5D_t1281789340* get_cachedAllKeys_10() const { return ___cachedAllKeys_10; }
	inline StringU5BU5D_t1281789340** get_address_of_cachedAllKeys_10() { return &___cachedAllKeys_10; }
	inline void set_cachedAllKeys_10(StringU5BU5D_t1281789340* value)
	{
		___cachedAllKeys_10 = value;
		Il2CppCodeGenWriteBarrier((&___cachedAllKeys_10), value);
	}

	inline static int32_t get_offset_of_cachedAll_11() { return static_cast<int32_t>(offsetof(NameValueCollection_t407452768, ___cachedAll_11)); }
	inline StringU5BU5D_t1281789340* get_cachedAll_11() const { return ___cachedAll_11; }
	inline StringU5BU5D_t1281789340** get_address_of_cachedAll_11() { return &___cachedAll_11; }
	inline void set_cachedAll_11(StringU5BU5D_t1281789340* value)
	{
		___cachedAll_11 = value;
		Il2CppCodeGenWriteBarrier((&___cachedAll_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEVALUECOLLECTION_T407452768_H
#ifndef EXECONFIGURATIONFILEMAP_T3751566101_H
#define EXECONFIGURATIONFILEMAP_T3751566101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ExeConfigurationFileMap
struct  ExeConfigurationFileMap_t3751566101  : public ConfigurationFileMap_t3010527243
{
public:
	// System.String System.Configuration.ExeConfigurationFileMap::exeConfigFilename
	String_t* ___exeConfigFilename_1;
	// System.String System.Configuration.ExeConfigurationFileMap::localUserConfigFilename
	String_t* ___localUserConfigFilename_2;
	// System.String System.Configuration.ExeConfigurationFileMap::roamingUserConfigFilename
	String_t* ___roamingUserConfigFilename_3;

public:
	inline static int32_t get_offset_of_exeConfigFilename_1() { return static_cast<int32_t>(offsetof(ExeConfigurationFileMap_t3751566101, ___exeConfigFilename_1)); }
	inline String_t* get_exeConfigFilename_1() const { return ___exeConfigFilename_1; }
	inline String_t** get_address_of_exeConfigFilename_1() { return &___exeConfigFilename_1; }
	inline void set_exeConfigFilename_1(String_t* value)
	{
		___exeConfigFilename_1 = value;
		Il2CppCodeGenWriteBarrier((&___exeConfigFilename_1), value);
	}

	inline static int32_t get_offset_of_localUserConfigFilename_2() { return static_cast<int32_t>(offsetof(ExeConfigurationFileMap_t3751566101, ___localUserConfigFilename_2)); }
	inline String_t* get_localUserConfigFilename_2() const { return ___localUserConfigFilename_2; }
	inline String_t** get_address_of_localUserConfigFilename_2() { return &___localUserConfigFilename_2; }
	inline void set_localUserConfigFilename_2(String_t* value)
	{
		___localUserConfigFilename_2 = value;
		Il2CppCodeGenWriteBarrier((&___localUserConfigFilename_2), value);
	}

	inline static int32_t get_offset_of_roamingUserConfigFilename_3() { return static_cast<int32_t>(offsetof(ExeConfigurationFileMap_t3751566101, ___roamingUserConfigFilename_3)); }
	inline String_t* get_roamingUserConfigFilename_3() const { return ___roamingUserConfigFilename_3; }
	inline String_t** get_address_of_roamingUserConfigFilename_3() { return &___roamingUserConfigFilename_3; }
	inline void set_roamingUserConfigFilename_3(String_t* value)
	{
		___roamingUserConfigFilename_3 = value;
		Il2CppCodeGenWriteBarrier((&___roamingUserConfigFilename_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXECONFIGURATIONFILEMAP_T3751566101_H
#ifndef DEFAULTVALIDATOR_T1354184753_H
#define DEFAULTVALIDATOR_T1354184753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.DefaultValidator
struct  DefaultValidator_t1354184753  : public ConfigurationValidatorBase_t888490966
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTVALIDATOR_T1354184753_H
#ifndef CONFIGURATIONVALIDATORATTRIBUTE_T448955463_H
#define CONFIGURATIONVALIDATORATTRIBUTE_T448955463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationValidatorAttribute
struct  ConfigurationValidatorAttribute_t448955463  : public Attribute_t861562559
{
public:
	// System.Type System.Configuration.ConfigurationValidatorAttribute::validatorType
	Type_t * ___validatorType_0;
	// System.Configuration.ConfigurationValidatorBase System.Configuration.ConfigurationValidatorAttribute::instance
	ConfigurationValidatorBase_t888490966 * ___instance_1;

public:
	inline static int32_t get_offset_of_validatorType_0() { return static_cast<int32_t>(offsetof(ConfigurationValidatorAttribute_t448955463, ___validatorType_0)); }
	inline Type_t * get_validatorType_0() const { return ___validatorType_0; }
	inline Type_t ** get_address_of_validatorType_0() { return &___validatorType_0; }
	inline void set_validatorType_0(Type_t * value)
	{
		___validatorType_0 = value;
		Il2CppCodeGenWriteBarrier((&___validatorType_0), value);
	}

	inline static int32_t get_offset_of_instance_1() { return static_cast<int32_t>(offsetof(ConfigurationValidatorAttribute_t448955463, ___instance_1)); }
	inline ConfigurationValidatorBase_t888490966 * get_instance_1() const { return ___instance_1; }
	inline ConfigurationValidatorBase_t888490966 ** get_address_of_instance_1() { return &___instance_1; }
	inline void set_instance_1(ConfigurationValidatorBase_t888490966 * value)
	{
		___instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONVALIDATORATTRIBUTE_T448955463_H
#ifndef U24ARRAYTYPEU244_T1630999355_H
#define U24ARRAYTYPEU244_T1630999355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType$4
struct  U24ArrayTypeU244_t1630999355 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU244_t1630999355__padding[4];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU244_T1630999355_H
#ifndef CONFIGURATIONSECTION_T3156163955_H
#define CONFIGURATIONSECTION_T3156163955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationSection
struct  ConfigurationSection_t3156163955  : public ConfigurationElement_t3318566633
{
public:
	// System.Configuration.SectionInformation System.Configuration.ConfigurationSection::sectionInformation
	SectionInformation_t2821611020 * ___sectionInformation_13;
	// System.Configuration.IConfigurationSectionHandler System.Configuration.ConfigurationSection::section_handler
	RuntimeObject* ___section_handler_14;
	// System.String System.Configuration.ConfigurationSection::externalDataXml
	String_t* ___externalDataXml_15;
	// System.Object System.Configuration.ConfigurationSection::_configContext
	RuntimeObject * ____configContext_16;

public:
	inline static int32_t get_offset_of_sectionInformation_13() { return static_cast<int32_t>(offsetof(ConfigurationSection_t3156163955, ___sectionInformation_13)); }
	inline SectionInformation_t2821611020 * get_sectionInformation_13() const { return ___sectionInformation_13; }
	inline SectionInformation_t2821611020 ** get_address_of_sectionInformation_13() { return &___sectionInformation_13; }
	inline void set_sectionInformation_13(SectionInformation_t2821611020 * value)
	{
		___sectionInformation_13 = value;
		Il2CppCodeGenWriteBarrier((&___sectionInformation_13), value);
	}

	inline static int32_t get_offset_of_section_handler_14() { return static_cast<int32_t>(offsetof(ConfigurationSection_t3156163955, ___section_handler_14)); }
	inline RuntimeObject* get_section_handler_14() const { return ___section_handler_14; }
	inline RuntimeObject** get_address_of_section_handler_14() { return &___section_handler_14; }
	inline void set_section_handler_14(RuntimeObject* value)
	{
		___section_handler_14 = value;
		Il2CppCodeGenWriteBarrier((&___section_handler_14), value);
	}

	inline static int32_t get_offset_of_externalDataXml_15() { return static_cast<int32_t>(offsetof(ConfigurationSection_t3156163955, ___externalDataXml_15)); }
	inline String_t* get_externalDataXml_15() const { return ___externalDataXml_15; }
	inline String_t** get_address_of_externalDataXml_15() { return &___externalDataXml_15; }
	inline void set_externalDataXml_15(String_t* value)
	{
		___externalDataXml_15 = value;
		Il2CppCodeGenWriteBarrier((&___externalDataXml_15), value);
	}

	inline static int32_t get_offset_of__configContext_16() { return static_cast<int32_t>(offsetof(ConfigurationSection_t3156163955, ____configContext_16)); }
	inline RuntimeObject * get__configContext_16() const { return ____configContext_16; }
	inline RuntimeObject ** get_address_of__configContext_16() { return &____configContext_16; }
	inline void set__configContext_16(RuntimeObject * value)
	{
		____configContext_16 = value;
		Il2CppCodeGenWriteBarrier((&____configContext_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONSECTION_T3156163955_H
#ifndef CONFIGURATIONSECTIONCOLLECTION_T2786897858_H
#define CONFIGURATIONSECTIONCOLLECTION_T2786897858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationSectionCollection
struct  ConfigurationSectionCollection_t2786897858  : public NameObjectCollectionBase_t2091847364
{
public:
	// System.Configuration.SectionGroupInfo System.Configuration.ConfigurationSectionCollection::group
	SectionGroupInfo_t687896682 * ___group_10;
	// System.Configuration.Configuration System.Configuration.ConfigurationSectionCollection::config
	Configuration_t2529364143 * ___config_11;

public:
	inline static int32_t get_offset_of_group_10() { return static_cast<int32_t>(offsetof(ConfigurationSectionCollection_t2786897858, ___group_10)); }
	inline SectionGroupInfo_t687896682 * get_group_10() const { return ___group_10; }
	inline SectionGroupInfo_t687896682 ** get_address_of_group_10() { return &___group_10; }
	inline void set_group_10(SectionGroupInfo_t687896682 * value)
	{
		___group_10 = value;
		Il2CppCodeGenWriteBarrier((&___group_10), value);
	}

	inline static int32_t get_offset_of_config_11() { return static_cast<int32_t>(offsetof(ConfigurationSectionCollection_t2786897858, ___config_11)); }
	inline Configuration_t2529364143 * get_config_11() const { return ___config_11; }
	inline Configuration_t2529364143 ** get_address_of_config_11() { return &___config_11; }
	inline void set_config_11(Configuration_t2529364143 * value)
	{
		___config_11 = value;
		Il2CppCodeGenWriteBarrier((&___config_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONSECTIONCOLLECTION_T2786897858_H
#ifndef CONFIGURATIONSECTIONGROUPCOLLECTION_T1151641153_H
#define CONFIGURATIONSECTIONGROUPCOLLECTION_T1151641153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationSectionGroupCollection
struct  ConfigurationSectionGroupCollection_t1151641153  : public NameObjectCollectionBase_t2091847364
{
public:
	// System.Configuration.SectionGroupInfo System.Configuration.ConfigurationSectionGroupCollection::group
	SectionGroupInfo_t687896682 * ___group_10;
	// System.Configuration.Configuration System.Configuration.ConfigurationSectionGroupCollection::config
	Configuration_t2529364143 * ___config_11;

public:
	inline static int32_t get_offset_of_group_10() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroupCollection_t1151641153, ___group_10)); }
	inline SectionGroupInfo_t687896682 * get_group_10() const { return ___group_10; }
	inline SectionGroupInfo_t687896682 ** get_address_of_group_10() { return &___group_10; }
	inline void set_group_10(SectionGroupInfo_t687896682 * value)
	{
		___group_10 = value;
		Il2CppCodeGenWriteBarrier((&___group_10), value);
	}

	inline static int32_t get_offset_of_config_11() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroupCollection_t1151641153, ___config_11)); }
	inline Configuration_t2529364143 * get_config_11() const { return ___config_11; }
	inline Configuration_t2529364143 ** get_address_of_config_11() { return &___config_11; }
	inline void set_config_11(Configuration_t2529364143 * value)
	{
		___config_11 = value;
		Il2CppCodeGenWriteBarrier((&___config_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONSECTIONGROUPCOLLECTION_T1151641153_H
#ifndef CONFIGURATIONEXCEPTION_T3515317685_H
#define CONFIGURATIONEXCEPTION_T3515317685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationException
struct  ConfigurationException_t3515317685  : public SystemException_t176217640
{
public:
	// System.String System.Configuration.ConfigurationException::filename
	String_t* ___filename_11;
	// System.Int32 System.Configuration.ConfigurationException::line
	int32_t ___line_12;

public:
	inline static int32_t get_offset_of_filename_11() { return static_cast<int32_t>(offsetof(ConfigurationException_t3515317685, ___filename_11)); }
	inline String_t* get_filename_11() const { return ___filename_11; }
	inline String_t** get_address_of_filename_11() { return &___filename_11; }
	inline void set_filename_11(String_t* value)
	{
		___filename_11 = value;
		Il2CppCodeGenWriteBarrier((&___filename_11), value);
	}

	inline static int32_t get_offset_of_line_12() { return static_cast<int32_t>(offsetof(ConfigurationException_t3515317685, ___line_12)); }
	inline int32_t get_line_12() const { return ___line_12; }
	inline int32_t* get_address_of_line_12() { return &___line_12; }
	inline void set_line_12(int32_t value)
	{
		___line_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONEXCEPTION_T3515317685_H
#ifndef DAYOFWEEK_T3650621421_H
#define DAYOFWEEK_T3650621421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DayOfWeek
struct  DayOfWeek_t3650621421 
{
public:
	// System.Int32 System.DayOfWeek::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DayOfWeek_t3650621421, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DAYOFWEEK_T3650621421_H
#ifndef FALLBACK_T3495999270_H
#define FALLBACK_T3495999270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/Fallback
struct  Fallback_t3495999270 
{
public:
	// System.Int32 System.Linq.Enumerable/Fallback::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Fallback_t3495999270, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FALLBACK_T3495999270_H
#ifndef TIMEZONEINFO_T161405854_H
#define TIMEZONEINFO_T161405854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeZoneInfo
struct  TimeZoneInfo_t161405854  : public RuntimeObject
{
public:
	// System.TimeSpan System.TimeZoneInfo::baseUtcOffset
	TimeSpan_t881159249  ___baseUtcOffset_0;
	// System.String System.TimeZoneInfo::daylightDisplayName
	String_t* ___daylightDisplayName_1;
	// System.String System.TimeZoneInfo::displayName
	String_t* ___displayName_2;
	// System.String System.TimeZoneInfo::id
	String_t* ___id_3;
	// System.String System.TimeZoneInfo::standardDisplayName
	String_t* ___standardDisplayName_5;
	// System.Boolean System.TimeZoneInfo::disableDaylightSavingTime
	bool ___disableDaylightSavingTime_6;
	// System.TimeZoneInfo/AdjustmentRule[] System.TimeZoneInfo::adjustmentRules
	AdjustmentRuleU5BU5D_t1469475992* ___adjustmentRules_9;

public:
	inline static int32_t get_offset_of_baseUtcOffset_0() { return static_cast<int32_t>(offsetof(TimeZoneInfo_t161405854, ___baseUtcOffset_0)); }
	inline TimeSpan_t881159249  get_baseUtcOffset_0() const { return ___baseUtcOffset_0; }
	inline TimeSpan_t881159249 * get_address_of_baseUtcOffset_0() { return &___baseUtcOffset_0; }
	inline void set_baseUtcOffset_0(TimeSpan_t881159249  value)
	{
		___baseUtcOffset_0 = value;
	}

	inline static int32_t get_offset_of_daylightDisplayName_1() { return static_cast<int32_t>(offsetof(TimeZoneInfo_t161405854, ___daylightDisplayName_1)); }
	inline String_t* get_daylightDisplayName_1() const { return ___daylightDisplayName_1; }
	inline String_t** get_address_of_daylightDisplayName_1() { return &___daylightDisplayName_1; }
	inline void set_daylightDisplayName_1(String_t* value)
	{
		___daylightDisplayName_1 = value;
		Il2CppCodeGenWriteBarrier((&___daylightDisplayName_1), value);
	}

	inline static int32_t get_offset_of_displayName_2() { return static_cast<int32_t>(offsetof(TimeZoneInfo_t161405854, ___displayName_2)); }
	inline String_t* get_displayName_2() const { return ___displayName_2; }
	inline String_t** get_address_of_displayName_2() { return &___displayName_2; }
	inline void set_displayName_2(String_t* value)
	{
		___displayName_2 = value;
		Il2CppCodeGenWriteBarrier((&___displayName_2), value);
	}

	inline static int32_t get_offset_of_id_3() { return static_cast<int32_t>(offsetof(TimeZoneInfo_t161405854, ___id_3)); }
	inline String_t* get_id_3() const { return ___id_3; }
	inline String_t** get_address_of_id_3() { return &___id_3; }
	inline void set_id_3(String_t* value)
	{
		___id_3 = value;
		Il2CppCodeGenWriteBarrier((&___id_3), value);
	}

	inline static int32_t get_offset_of_standardDisplayName_5() { return static_cast<int32_t>(offsetof(TimeZoneInfo_t161405854, ___standardDisplayName_5)); }
	inline String_t* get_standardDisplayName_5() const { return ___standardDisplayName_5; }
	inline String_t** get_address_of_standardDisplayName_5() { return &___standardDisplayName_5; }
	inline void set_standardDisplayName_5(String_t* value)
	{
		___standardDisplayName_5 = value;
		Il2CppCodeGenWriteBarrier((&___standardDisplayName_5), value);
	}

	inline static int32_t get_offset_of_disableDaylightSavingTime_6() { return static_cast<int32_t>(offsetof(TimeZoneInfo_t161405854, ___disableDaylightSavingTime_6)); }
	inline bool get_disableDaylightSavingTime_6() const { return ___disableDaylightSavingTime_6; }
	inline bool* get_address_of_disableDaylightSavingTime_6() { return &___disableDaylightSavingTime_6; }
	inline void set_disableDaylightSavingTime_6(bool value)
	{
		___disableDaylightSavingTime_6 = value;
	}

	inline static int32_t get_offset_of_adjustmentRules_9() { return static_cast<int32_t>(offsetof(TimeZoneInfo_t161405854, ___adjustmentRules_9)); }
	inline AdjustmentRuleU5BU5D_t1469475992* get_adjustmentRules_9() const { return ___adjustmentRules_9; }
	inline AdjustmentRuleU5BU5D_t1469475992** get_address_of_adjustmentRules_9() { return &___adjustmentRules_9; }
	inline void set_adjustmentRules_9(AdjustmentRuleU5BU5D_t1469475992* value)
	{
		___adjustmentRules_9 = value;
		Il2CppCodeGenWriteBarrier((&___adjustmentRules_9), value);
	}
};

struct TimeZoneInfo_t161405854_StaticFields
{
public:
	// System.TimeZoneInfo System.TimeZoneInfo::local
	TimeZoneInfo_t161405854 * ___local_4;
	// System.TimeZoneInfo System.TimeZoneInfo::utc
	TimeZoneInfo_t161405854 * ___utc_7;
	// System.String System.TimeZoneInfo::timeZoneDirectory
	String_t* ___timeZoneDirectory_8;

public:
	inline static int32_t get_offset_of_local_4() { return static_cast<int32_t>(offsetof(TimeZoneInfo_t161405854_StaticFields, ___local_4)); }
	inline TimeZoneInfo_t161405854 * get_local_4() const { return ___local_4; }
	inline TimeZoneInfo_t161405854 ** get_address_of_local_4() { return &___local_4; }
	inline void set_local_4(TimeZoneInfo_t161405854 * value)
	{
		___local_4 = value;
		Il2CppCodeGenWriteBarrier((&___local_4), value);
	}

	inline static int32_t get_offset_of_utc_7() { return static_cast<int32_t>(offsetof(TimeZoneInfo_t161405854_StaticFields, ___utc_7)); }
	inline TimeZoneInfo_t161405854 * get_utc_7() const { return ___utc_7; }
	inline TimeZoneInfo_t161405854 ** get_address_of_utc_7() { return &___utc_7; }
	inline void set_utc_7(TimeZoneInfo_t161405854 * value)
	{
		___utc_7 = value;
		Il2CppCodeGenWriteBarrier((&___utc_7), value);
	}

	inline static int32_t get_offset_of_timeZoneDirectory_8() { return static_cast<int32_t>(offsetof(TimeZoneInfo_t161405854_StaticFields, ___timeZoneDirectory_8)); }
	inline String_t* get_timeZoneDirectory_8() const { return ___timeZoneDirectory_8; }
	inline String_t** get_address_of_timeZoneDirectory_8() { return &___timeZoneDirectory_8; }
	inline void set_timeZoneDirectory_8(String_t* value)
	{
		___timeZoneDirectory_8 = value;
		Il2CppCodeGenWriteBarrier((&___timeZoneDirectory_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEZONEINFO_T161405854_H
#ifndef DEFAULTSECTION_T752822653_H
#define DEFAULTSECTION_T752822653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.DefaultSection
struct  DefaultSection_t752822653  : public ConfigurationSection_t3156163955
{
public:

public:
};

struct DefaultSection_t752822653_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.DefaultSection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_17;

public:
	inline static int32_t get_offset_of_properties_17() { return static_cast<int32_t>(offsetof(DefaultSection_t752822653_StaticFields, ___properties_17)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_17() const { return ___properties_17; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_17() { return &___properties_17; }
	inline void set_properties_17(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_17 = value;
		Il2CppCodeGenWriteBarrier((&___properties_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTSECTION_T752822653_H
#ifndef CONFIGXMLTEXTREADER_T3683698114_H
#define CONFIGXMLTEXTREADER_T3683698114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConfigXmlTextReader
struct  ConfigXmlTextReader_t3683698114  : public XmlTextReader_t4233384356
{
public:
	// System.String ConfigXmlTextReader::fileName
	String_t* ___fileName_7;

public:
	inline static int32_t get_offset_of_fileName_7() { return static_cast<int32_t>(offsetof(ConfigXmlTextReader_t3683698114, ___fileName_7)); }
	inline String_t* get_fileName_7() const { return ___fileName_7; }
	inline String_t** get_address_of_fileName_7() { return &___fileName_7; }
	inline void set_fileName_7(String_t* value)
	{
		___fileName_7 = value;
		Il2CppCodeGenWriteBarrier((&___fileName_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGXMLTEXTREADER_T3683698114_H
#ifndef CONFIGURATIONUSERLEVEL_T2700976468_H
#define CONFIGURATIONUSERLEVEL_T2700976468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationUserLevel
struct  ConfigurationUserLevel_t2700976468 
{
public:
	// System.Int32 System.Configuration.ConfigurationUserLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConfigurationUserLevel_t2700976468, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONUSERLEVEL_T2700976468_H
#ifndef CONFIGURATIONSAVEMODE_T1605032206_H
#define CONFIGURATIONSAVEMODE_T1605032206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationSaveMode
struct  ConfigurationSaveMode_t1605032206 
{
public:
	// System.Int32 System.Configuration.ConfigurationSaveMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConfigurationSaveMode_t1605032206, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONSAVEMODE_T1605032206_H
#ifndef CONFIGURATIONPROPERTYOPTIONS_T1059028815_H
#define CONFIGURATIONPROPERTYOPTIONS_T1059028815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationPropertyOptions
struct  ConfigurationPropertyOptions_t1059028815 
{
public:
	// System.Int32 System.Configuration.ConfigurationPropertyOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConfigurationPropertyOptions_t1059028815, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONPROPERTYOPTIONS_T1059028815_H
#ifndef CONFIGURATIONLOCKTYPE_T161400359_H
#define CONFIGURATIONLOCKTYPE_T161400359_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationLockType
struct  ConfigurationLockType_t161400359 
{
public:
	// System.Int32 System.Configuration.ConfigurationLockType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConfigurationLockType_t161400359, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONLOCKTYPE_T161400359_H
#ifndef CONFIGURATIONELEMENTCOLLECTIONTYPE_T2560831360_H
#define CONFIGURATIONELEMENTCOLLECTIONTYPE_T2560831360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationElementCollectionType
struct  ConfigurationElementCollectionType_t2560831360 
{
public:
	// System.Int32 System.Configuration.ConfigurationElementCollectionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConfigurationElementCollectionType_t2560831360, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONELEMENTCOLLECTIONTYPE_T2560831360_H
#ifndef CONFIGURATIONALLOWEXEDEFINITION_T2804732269_H
#define CONFIGURATIONALLOWEXEDEFINITION_T2804732269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationAllowExeDefinition
struct  ConfigurationAllowExeDefinition_t2804732269 
{
public:
	// System.Int32 System.Configuration.ConfigurationAllowExeDefinition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConfigurationAllowExeDefinition_t2804732269, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONALLOWEXEDEFINITION_T2804732269_H
#ifndef CONFIGURATIONALLOWDEFINITION_T1879228320_H
#define CONFIGURATIONALLOWDEFINITION_T1879228320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationAllowDefinition
struct  ConfigurationAllowDefinition_t1879228320 
{
public:
	// System.Int32 System.Configuration.ConfigurationAllowDefinition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConfigurationAllowDefinition_t1879228320, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONALLOWDEFINITION_T1879228320_H
#ifndef CONFIGNAMEVALUECOLLECTION_T1573003828_H
#define CONFIGNAMEVALUECOLLECTION_T1573003828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigNameValueCollection
struct  ConfigNameValueCollection_t1573003828  : public NameValueCollection_t407452768
{
public:
	// System.Boolean System.Configuration.ConfigNameValueCollection::modified
	bool ___modified_12;

public:
	inline static int32_t get_offset_of_modified_12() { return static_cast<int32_t>(offsetof(ConfigNameValueCollection_t1573003828, ___modified_12)); }
	inline bool get_modified_12() const { return ___modified_12; }
	inline bool* get_address_of_modified_12() { return &___modified_12; }
	inline void set_modified_12(bool value)
	{
		___modified_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGNAMEVALUECOLLECTION_T1573003828_H
#ifndef IGNORESECTION_T2596619786_H
#define IGNORESECTION_T2596619786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.IgnoreSection
struct  IgnoreSection_t2596619786  : public ConfigurationSection_t3156163955
{
public:
	// System.String System.Configuration.IgnoreSection::xml
	String_t* ___xml_17;

public:
	inline static int32_t get_offset_of_xml_17() { return static_cast<int32_t>(offsetof(IgnoreSection_t2596619786, ___xml_17)); }
	inline String_t* get_xml_17() const { return ___xml_17; }
	inline String_t** get_address_of_xml_17() { return &___xml_17; }
	inline void set_xml_17(String_t* value)
	{
		___xml_17 = value;
		Il2CppCodeGenWriteBarrier((&___xml_17), value);
	}
};

struct IgnoreSection_t2596619786_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.IgnoreSection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_18;

public:
	inline static int32_t get_offset_of_properties_18() { return static_cast<int32_t>(offsetof(IgnoreSection_t2596619786_StaticFields, ___properties_18)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_18() const { return ___properties_18; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_18() { return &___properties_18; }
	inline void set_properties_18(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_18 = value;
		Il2CppCodeGenWriteBarrier((&___properties_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IGNORESECTION_T2596619786_H
#ifndef PROVIDERSETTINGSCOLLECTION_T3942935190_H
#define PROVIDERSETTINGSCOLLECTION_T3942935190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ProviderSettingsCollection
struct  ProviderSettingsCollection_t3942935190  : public ConfigurationElementCollection_t446763386
{
public:

public:
};

struct ProviderSettingsCollection_t3942935190_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.ProviderSettingsCollection::props
	ConfigurationPropertyCollection_t2852175726 * ___props_23;

public:
	inline static int32_t get_offset_of_props_23() { return static_cast<int32_t>(offsetof(ProviderSettingsCollection_t3942935190_StaticFields, ___props_23)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_props_23() const { return ___props_23; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_props_23() { return &___props_23; }
	inline void set_props_23(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___props_23 = value;
		Il2CppCodeGenWriteBarrier((&___props_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERSETTINGSCOLLECTION_T3942935190_H
#ifndef PROTECTEDCONFIGURATIONSECTION_T1525901209_H
#define PROTECTEDCONFIGURATIONSECTION_T1525901209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ProtectedConfigurationSection
struct  ProtectedConfigurationSection_t1525901209  : public ConfigurationSection_t3156163955
{
public:
	// System.Configuration.ProtectedConfigurationProviderCollection System.Configuration.ProtectedConfigurationSection::providers
	ProtectedConfigurationProviderCollection_t729952796 * ___providers_20;

public:
	inline static int32_t get_offset_of_providers_20() { return static_cast<int32_t>(offsetof(ProtectedConfigurationSection_t1525901209, ___providers_20)); }
	inline ProtectedConfigurationProviderCollection_t729952796 * get_providers_20() const { return ___providers_20; }
	inline ProtectedConfigurationProviderCollection_t729952796 ** get_address_of_providers_20() { return &___providers_20; }
	inline void set_providers_20(ProtectedConfigurationProviderCollection_t729952796 * value)
	{
		___providers_20 = value;
		Il2CppCodeGenWriteBarrier((&___providers_20), value);
	}
};

struct ProtectedConfigurationSection_t1525901209_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Configuration.ProtectedConfigurationSection::defaultProviderProp
	ConfigurationProperty_t3590861854 * ___defaultProviderProp_17;
	// System.Configuration.ConfigurationProperty System.Configuration.ProtectedConfigurationSection::providersProp
	ConfigurationProperty_t3590861854 * ___providersProp_18;
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.ProtectedConfigurationSection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_19;

public:
	inline static int32_t get_offset_of_defaultProviderProp_17() { return static_cast<int32_t>(offsetof(ProtectedConfigurationSection_t1525901209_StaticFields, ___defaultProviderProp_17)); }
	inline ConfigurationProperty_t3590861854 * get_defaultProviderProp_17() const { return ___defaultProviderProp_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_defaultProviderProp_17() { return &___defaultProviderProp_17; }
	inline void set_defaultProviderProp_17(ConfigurationProperty_t3590861854 * value)
	{
		___defaultProviderProp_17 = value;
		Il2CppCodeGenWriteBarrier((&___defaultProviderProp_17), value);
	}

	inline static int32_t get_offset_of_providersProp_18() { return static_cast<int32_t>(offsetof(ProtectedConfigurationSection_t1525901209_StaticFields, ___providersProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_providersProp_18() const { return ___providersProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_providersProp_18() { return &___providersProp_18; }
	inline void set_providersProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___providersProp_18 = value;
		Il2CppCodeGenWriteBarrier((&___providersProp_18), value);
	}

	inline static int32_t get_offset_of_properties_19() { return static_cast<int32_t>(offsetof(ProtectedConfigurationSection_t1525901209_StaticFields, ___properties_19)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_19() const { return ___properties_19; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_19() { return &___properties_19; }
	inline void set_properties_19(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_19 = value;
		Il2CppCodeGenWriteBarrier((&___properties_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTECTEDCONFIGURATIONSECTION_T1525901209_H
#ifndef PROPERTYVALUEORIGIN_T225613163_H
#define PROPERTYVALUEORIGIN_T225613163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.PropertyValueOrigin
struct  PropertyValueOrigin_t225613163 
{
public:
	// System.Int32 System.Configuration.PropertyValueOrigin::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PropertyValueOrigin_t225613163, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYVALUEORIGIN_T225613163_H
#ifndef MONOINTERNALNOTEATTRIBUTE_T2284022217_H
#define MONOINTERNALNOTEATTRIBUTE_T2284022217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MonoInternalNoteAttribute
struct  MonoInternalNoteAttribute_t2284022217  : public MonoTODOAttribute_t4131080584
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOINTERNALNOTEATTRIBUTE_T2284022217_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef CONFIGURATIONCOLLECTIONATTRIBUTE_T3695308734_H
#define CONFIGURATIONCOLLECTIONATTRIBUTE_T3695308734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationCollectionAttribute
struct  ConfigurationCollectionAttribute_t3695308734  : public Attribute_t861562559
{
public:
	// System.String System.Configuration.ConfigurationCollectionAttribute::addItemName
	String_t* ___addItemName_0;
	// System.String System.Configuration.ConfigurationCollectionAttribute::clearItemsName
	String_t* ___clearItemsName_1;
	// System.String System.Configuration.ConfigurationCollectionAttribute::removeItemName
	String_t* ___removeItemName_2;
	// System.Configuration.ConfigurationElementCollectionType System.Configuration.ConfigurationCollectionAttribute::collectionType
	int32_t ___collectionType_3;
	// System.Type System.Configuration.ConfigurationCollectionAttribute::itemType
	Type_t * ___itemType_4;

public:
	inline static int32_t get_offset_of_addItemName_0() { return static_cast<int32_t>(offsetof(ConfigurationCollectionAttribute_t3695308734, ___addItemName_0)); }
	inline String_t* get_addItemName_0() const { return ___addItemName_0; }
	inline String_t** get_address_of_addItemName_0() { return &___addItemName_0; }
	inline void set_addItemName_0(String_t* value)
	{
		___addItemName_0 = value;
		Il2CppCodeGenWriteBarrier((&___addItemName_0), value);
	}

	inline static int32_t get_offset_of_clearItemsName_1() { return static_cast<int32_t>(offsetof(ConfigurationCollectionAttribute_t3695308734, ___clearItemsName_1)); }
	inline String_t* get_clearItemsName_1() const { return ___clearItemsName_1; }
	inline String_t** get_address_of_clearItemsName_1() { return &___clearItemsName_1; }
	inline void set_clearItemsName_1(String_t* value)
	{
		___clearItemsName_1 = value;
		Il2CppCodeGenWriteBarrier((&___clearItemsName_1), value);
	}

	inline static int32_t get_offset_of_removeItemName_2() { return static_cast<int32_t>(offsetof(ConfigurationCollectionAttribute_t3695308734, ___removeItemName_2)); }
	inline String_t* get_removeItemName_2() const { return ___removeItemName_2; }
	inline String_t** get_address_of_removeItemName_2() { return &___removeItemName_2; }
	inline void set_removeItemName_2(String_t* value)
	{
		___removeItemName_2 = value;
		Il2CppCodeGenWriteBarrier((&___removeItemName_2), value);
	}

	inline static int32_t get_offset_of_collectionType_3() { return static_cast<int32_t>(offsetof(ConfigurationCollectionAttribute_t3695308734, ___collectionType_3)); }
	inline int32_t get_collectionType_3() const { return ___collectionType_3; }
	inline int32_t* get_address_of_collectionType_3() { return &___collectionType_3; }
	inline void set_collectionType_3(int32_t value)
	{
		___collectionType_3 = value;
	}

	inline static int32_t get_offset_of_itemType_4() { return static_cast<int32_t>(offsetof(ConfigurationCollectionAttribute_t3695308734, ___itemType_4)); }
	inline Type_t * get_itemType_4() const { return ___itemType_4; }
	inline Type_t ** get_address_of_itemType_4() { return &___itemType_4; }
	inline void set_itemType_4(Type_t * value)
	{
		___itemType_4 = value;
		Il2CppCodeGenWriteBarrier((&___itemType_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONCOLLECTIONATTRIBUTE_T3695308734_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_10)); }
	inline TimeSpan_t881159249  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t881159249 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t881159249  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_12)); }
	inline DateTime_t3738529785  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t3738529785  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_13)); }
	inline DateTime_t3738529785  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t3738529785 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t3738529785  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef SECTIONINFO_T640588401_H
#define SECTIONINFO_T640588401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.SectionInfo
struct  SectionInfo_t640588401  : public ConfigInfo_t2364721456
{
public:
	// System.Boolean System.Configuration.SectionInfo::allowLocation
	bool ___allowLocation_6;
	// System.Boolean System.Configuration.SectionInfo::requirePermission
	bool ___requirePermission_7;
	// System.Boolean System.Configuration.SectionInfo::restartOnExternalChanges
	bool ___restartOnExternalChanges_8;
	// System.Configuration.ConfigurationAllowDefinition System.Configuration.SectionInfo::allowDefinition
	int32_t ___allowDefinition_9;
	// System.Configuration.ConfigurationAllowExeDefinition System.Configuration.SectionInfo::allowExeDefinition
	int32_t ___allowExeDefinition_10;

public:
	inline static int32_t get_offset_of_allowLocation_6() { return static_cast<int32_t>(offsetof(SectionInfo_t640588401, ___allowLocation_6)); }
	inline bool get_allowLocation_6() const { return ___allowLocation_6; }
	inline bool* get_address_of_allowLocation_6() { return &___allowLocation_6; }
	inline void set_allowLocation_6(bool value)
	{
		___allowLocation_6 = value;
	}

	inline static int32_t get_offset_of_requirePermission_7() { return static_cast<int32_t>(offsetof(SectionInfo_t640588401, ___requirePermission_7)); }
	inline bool get_requirePermission_7() const { return ___requirePermission_7; }
	inline bool* get_address_of_requirePermission_7() { return &___requirePermission_7; }
	inline void set_requirePermission_7(bool value)
	{
		___requirePermission_7 = value;
	}

	inline static int32_t get_offset_of_restartOnExternalChanges_8() { return static_cast<int32_t>(offsetof(SectionInfo_t640588401, ___restartOnExternalChanges_8)); }
	inline bool get_restartOnExternalChanges_8() const { return ___restartOnExternalChanges_8; }
	inline bool* get_address_of_restartOnExternalChanges_8() { return &___restartOnExternalChanges_8; }
	inline void set_restartOnExternalChanges_8(bool value)
	{
		___restartOnExternalChanges_8 = value;
	}

	inline static int32_t get_offset_of_allowDefinition_9() { return static_cast<int32_t>(offsetof(SectionInfo_t640588401, ___allowDefinition_9)); }
	inline int32_t get_allowDefinition_9() const { return ___allowDefinition_9; }
	inline int32_t* get_address_of_allowDefinition_9() { return &___allowDefinition_9; }
	inline void set_allowDefinition_9(int32_t value)
	{
		___allowDefinition_9 = value;
	}

	inline static int32_t get_offset_of_allowExeDefinition_10() { return static_cast<int32_t>(offsetof(SectionInfo_t640588401, ___allowExeDefinition_10)); }
	inline int32_t get_allowExeDefinition_10() const { return ___allowExeDefinition_10; }
	inline int32_t* get_address_of_allowExeDefinition_10() { return &___allowExeDefinition_10; }
	inline void set_allowExeDefinition_10(int32_t value)
	{
		___allowExeDefinition_10 = value;
	}
};

struct SectionInfo_t640588401_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Configuration.SectionInfo::<>f__switch$map1
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map1_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_11() { return static_cast<int32_t>(offsetof(SectionInfo_t640588401_StaticFields, ___U3CU3Ef__switchU24map1_11)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map1_11() const { return ___U3CU3Ef__switchU24map1_11; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map1_11() { return &___U3CU3Ef__switchU24map1_11; }
	inline void set_U3CU3Ef__switchU24map1_11(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map1_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECTIONINFO_T640588401_H
#ifndef SECTIONINFORMATION_T2821611020_H
#define SECTIONINFORMATION_T2821611020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.SectionInformation
struct  SectionInformation_t2821611020  : public RuntimeObject
{
public:
	// System.Configuration.ConfigurationSection System.Configuration.SectionInformation::parent
	ConfigurationSection_t3156163955 * ___parent_0;
	// System.Configuration.ConfigurationAllowDefinition System.Configuration.SectionInformation::allow_definition
	int32_t ___allow_definition_1;
	// System.Configuration.ConfigurationAllowExeDefinition System.Configuration.SectionInformation::allow_exe_definition
	int32_t ___allow_exe_definition_2;
	// System.Boolean System.Configuration.SectionInformation::allow_location
	bool ___allow_location_3;
	// System.Boolean System.Configuration.SectionInformation::allow_override
	bool ___allow_override_4;
	// System.Boolean System.Configuration.SectionInformation::inherit_on_child_apps
	bool ___inherit_on_child_apps_5;
	// System.Boolean System.Configuration.SectionInformation::restart_on_external_changes
	bool ___restart_on_external_changes_6;
	// System.Boolean System.Configuration.SectionInformation::require_permission
	bool ___require_permission_7;
	// System.String System.Configuration.SectionInformation::config_source
	String_t* ___config_source_8;
	// System.String System.Configuration.SectionInformation::name
	String_t* ___name_9;
	// System.String System.Configuration.SectionInformation::raw_xml
	String_t* ___raw_xml_10;
	// System.Configuration.ProtectedConfigurationProvider System.Configuration.SectionInformation::protection_provider
	ProtectedConfigurationProvider_t413290519 * ___protection_provider_11;
	// System.String System.Configuration.SectionInformation::<ConfigFilePath>k__BackingField
	String_t* ___U3CConfigFilePathU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(SectionInformation_t2821611020, ___parent_0)); }
	inline ConfigurationSection_t3156163955 * get_parent_0() const { return ___parent_0; }
	inline ConfigurationSection_t3156163955 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(ConfigurationSection_t3156163955 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_allow_definition_1() { return static_cast<int32_t>(offsetof(SectionInformation_t2821611020, ___allow_definition_1)); }
	inline int32_t get_allow_definition_1() const { return ___allow_definition_1; }
	inline int32_t* get_address_of_allow_definition_1() { return &___allow_definition_1; }
	inline void set_allow_definition_1(int32_t value)
	{
		___allow_definition_1 = value;
	}

	inline static int32_t get_offset_of_allow_exe_definition_2() { return static_cast<int32_t>(offsetof(SectionInformation_t2821611020, ___allow_exe_definition_2)); }
	inline int32_t get_allow_exe_definition_2() const { return ___allow_exe_definition_2; }
	inline int32_t* get_address_of_allow_exe_definition_2() { return &___allow_exe_definition_2; }
	inline void set_allow_exe_definition_2(int32_t value)
	{
		___allow_exe_definition_2 = value;
	}

	inline static int32_t get_offset_of_allow_location_3() { return static_cast<int32_t>(offsetof(SectionInformation_t2821611020, ___allow_location_3)); }
	inline bool get_allow_location_3() const { return ___allow_location_3; }
	inline bool* get_address_of_allow_location_3() { return &___allow_location_3; }
	inline void set_allow_location_3(bool value)
	{
		___allow_location_3 = value;
	}

	inline static int32_t get_offset_of_allow_override_4() { return static_cast<int32_t>(offsetof(SectionInformation_t2821611020, ___allow_override_4)); }
	inline bool get_allow_override_4() const { return ___allow_override_4; }
	inline bool* get_address_of_allow_override_4() { return &___allow_override_4; }
	inline void set_allow_override_4(bool value)
	{
		___allow_override_4 = value;
	}

	inline static int32_t get_offset_of_inherit_on_child_apps_5() { return static_cast<int32_t>(offsetof(SectionInformation_t2821611020, ___inherit_on_child_apps_5)); }
	inline bool get_inherit_on_child_apps_5() const { return ___inherit_on_child_apps_5; }
	inline bool* get_address_of_inherit_on_child_apps_5() { return &___inherit_on_child_apps_5; }
	inline void set_inherit_on_child_apps_5(bool value)
	{
		___inherit_on_child_apps_5 = value;
	}

	inline static int32_t get_offset_of_restart_on_external_changes_6() { return static_cast<int32_t>(offsetof(SectionInformation_t2821611020, ___restart_on_external_changes_6)); }
	inline bool get_restart_on_external_changes_6() const { return ___restart_on_external_changes_6; }
	inline bool* get_address_of_restart_on_external_changes_6() { return &___restart_on_external_changes_6; }
	inline void set_restart_on_external_changes_6(bool value)
	{
		___restart_on_external_changes_6 = value;
	}

	inline static int32_t get_offset_of_require_permission_7() { return static_cast<int32_t>(offsetof(SectionInformation_t2821611020, ___require_permission_7)); }
	inline bool get_require_permission_7() const { return ___require_permission_7; }
	inline bool* get_address_of_require_permission_7() { return &___require_permission_7; }
	inline void set_require_permission_7(bool value)
	{
		___require_permission_7 = value;
	}

	inline static int32_t get_offset_of_config_source_8() { return static_cast<int32_t>(offsetof(SectionInformation_t2821611020, ___config_source_8)); }
	inline String_t* get_config_source_8() const { return ___config_source_8; }
	inline String_t** get_address_of_config_source_8() { return &___config_source_8; }
	inline void set_config_source_8(String_t* value)
	{
		___config_source_8 = value;
		Il2CppCodeGenWriteBarrier((&___config_source_8), value);
	}

	inline static int32_t get_offset_of_name_9() { return static_cast<int32_t>(offsetof(SectionInformation_t2821611020, ___name_9)); }
	inline String_t* get_name_9() const { return ___name_9; }
	inline String_t** get_address_of_name_9() { return &___name_9; }
	inline void set_name_9(String_t* value)
	{
		___name_9 = value;
		Il2CppCodeGenWriteBarrier((&___name_9), value);
	}

	inline static int32_t get_offset_of_raw_xml_10() { return static_cast<int32_t>(offsetof(SectionInformation_t2821611020, ___raw_xml_10)); }
	inline String_t* get_raw_xml_10() const { return ___raw_xml_10; }
	inline String_t** get_address_of_raw_xml_10() { return &___raw_xml_10; }
	inline void set_raw_xml_10(String_t* value)
	{
		___raw_xml_10 = value;
		Il2CppCodeGenWriteBarrier((&___raw_xml_10), value);
	}

	inline static int32_t get_offset_of_protection_provider_11() { return static_cast<int32_t>(offsetof(SectionInformation_t2821611020, ___protection_provider_11)); }
	inline ProtectedConfigurationProvider_t413290519 * get_protection_provider_11() const { return ___protection_provider_11; }
	inline ProtectedConfigurationProvider_t413290519 ** get_address_of_protection_provider_11() { return &___protection_provider_11; }
	inline void set_protection_provider_11(ProtectedConfigurationProvider_t413290519 * value)
	{
		___protection_provider_11 = value;
		Il2CppCodeGenWriteBarrier((&___protection_provider_11), value);
	}

	inline static int32_t get_offset_of_U3CConfigFilePathU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(SectionInformation_t2821611020, ___U3CConfigFilePathU3Ek__BackingField_12)); }
	inline String_t* get_U3CConfigFilePathU3Ek__BackingField_12() const { return ___U3CConfigFilePathU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CConfigFilePathU3Ek__BackingField_12() { return &___U3CConfigFilePathU3Ek__BackingField_12; }
	inline void set_U3CConfigFilePathU3Ek__BackingField_12(String_t* value)
	{
		___U3CConfigFilePathU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConfigFilePathU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECTIONINFORMATION_T2821611020_H
#ifndef CONFIGURATIONERRORSEXCEPTION_T66512296_H
#define CONFIGURATIONERRORSEXCEPTION_T66512296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationErrorsException
struct  ConfigurationErrorsException_t66512296  : public ConfigurationException_t3515317685
{
public:
	// System.String System.Configuration.ConfigurationErrorsException::filename
	String_t* ___filename_13;
	// System.Int32 System.Configuration.ConfigurationErrorsException::line
	int32_t ___line_14;

public:
	inline static int32_t get_offset_of_filename_13() { return static_cast<int32_t>(offsetof(ConfigurationErrorsException_t66512296, ___filename_13)); }
	inline String_t* get_filename_13() const { return ___filename_13; }
	inline String_t** get_address_of_filename_13() { return &___filename_13; }
	inline void set_filename_13(String_t* value)
	{
		___filename_13 = value;
		Il2CppCodeGenWriteBarrier((&___filename_13), value);
	}

	inline static int32_t get_offset_of_line_14() { return static_cast<int32_t>(offsetof(ConfigurationErrorsException_t66512296, ___line_14)); }
	inline int32_t get_line_14() const { return ___line_14; }
	inline int32_t* get_address_of_line_14() { return &___line_14; }
	inline void set_line_14(int32_t value)
	{
		___line_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONERRORSEXCEPTION_T66512296_H
#ifndef EXECONFIGURATIONHOST_T3079009552_H
#define EXECONFIGURATIONHOST_T3079009552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ExeConfigurationHost
struct  ExeConfigurationHost_t3079009552  : public InternalConfigurationHost_t969478808
{
public:
	// System.Configuration.ExeConfigurationFileMap System.Configuration.ExeConfigurationHost::map
	ExeConfigurationFileMap_t3751566101 * ___map_0;
	// System.Configuration.ConfigurationUserLevel System.Configuration.ExeConfigurationHost::level
	int32_t ___level_1;

public:
	inline static int32_t get_offset_of_map_0() { return static_cast<int32_t>(offsetof(ExeConfigurationHost_t3079009552, ___map_0)); }
	inline ExeConfigurationFileMap_t3751566101 * get_map_0() const { return ___map_0; }
	inline ExeConfigurationFileMap_t3751566101 ** get_address_of_map_0() { return &___map_0; }
	inline void set_map_0(ExeConfigurationFileMap_t3751566101 * value)
	{
		___map_0 = value;
		Il2CppCodeGenWriteBarrier((&___map_0), value);
	}

	inline static int32_t get_offset_of_level_1() { return static_cast<int32_t>(offsetof(ExeConfigurationHost_t3079009552, ___level_1)); }
	inline int32_t get_level_1() const { return ___level_1; }
	inline int32_t* get_address_of_level_1() { return &___level_1; }
	inline void set_level_1(int32_t value)
	{
		___level_1 = value;
	}
};

struct ExeConfigurationHost_t3079009552_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Configuration.ExeConfigurationHost::<>f__switch$map0
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map0_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_2() { return static_cast<int32_t>(offsetof(ExeConfigurationHost_t3079009552_StaticFields, ___U3CU3Ef__switchU24map0_2)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map0_2() const { return ___U3CU3Ef__switchU24map0_2; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map0_2() { return &___U3CU3Ef__switchU24map0_2; }
	inline void set_U3CU3Ef__switchU24map0_2(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXECONFIGURATIONHOST_T3079009552_H
#ifndef CONFIGURATIONPROPERTYATTRIBUTE_T2659780707_H
#define CONFIGURATIONPROPERTYATTRIBUTE_T2659780707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationPropertyAttribute
struct  ConfigurationPropertyAttribute_t2659780707  : public Attribute_t861562559
{
public:
	// System.String System.Configuration.ConfigurationPropertyAttribute::name
	String_t* ___name_0;
	// System.Object System.Configuration.ConfigurationPropertyAttribute::default_value
	RuntimeObject * ___default_value_1;
	// System.Configuration.ConfigurationPropertyOptions System.Configuration.ConfigurationPropertyAttribute::flags
	int32_t ___flags_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(ConfigurationPropertyAttribute_t2659780707, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_default_value_1() { return static_cast<int32_t>(offsetof(ConfigurationPropertyAttribute_t2659780707, ___default_value_1)); }
	inline RuntimeObject * get_default_value_1() const { return ___default_value_1; }
	inline RuntimeObject ** get_address_of_default_value_1() { return &___default_value_1; }
	inline void set_default_value_1(RuntimeObject * value)
	{
		___default_value_1 = value;
		Il2CppCodeGenWriteBarrier((&___default_value_1), value);
	}

	inline static int32_t get_offset_of_flags_2() { return static_cast<int32_t>(offsetof(ConfigurationPropertyAttribute_t2659780707, ___flags_2)); }
	inline int32_t get_flags_2() const { return ___flags_2; }
	inline int32_t* get_address_of_flags_2() { return &___flags_2; }
	inline void set_flags_2(int32_t value)
	{
		___flags_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONPROPERTYATTRIBUTE_T2659780707_H
#ifndef CONFIGURATIONPROPERTY_T3590861854_H
#define CONFIGURATIONPROPERTY_T3590861854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationProperty
struct  ConfigurationProperty_t3590861854  : public RuntimeObject
{
public:
	// System.String System.Configuration.ConfigurationProperty::name
	String_t* ___name_1;
	// System.Type System.Configuration.ConfigurationProperty::type
	Type_t * ___type_2;
	// System.Object System.Configuration.ConfigurationProperty::default_value
	RuntimeObject * ___default_value_3;
	// System.ComponentModel.TypeConverter System.Configuration.ConfigurationProperty::converter
	TypeConverter_t2249118273 * ___converter_4;
	// System.Configuration.ConfigurationValidatorBase System.Configuration.ConfigurationProperty::validation
	ConfigurationValidatorBase_t888490966 * ___validation_5;
	// System.Configuration.ConfigurationPropertyOptions System.Configuration.ConfigurationProperty::flags
	int32_t ___flags_6;
	// System.String System.Configuration.ConfigurationProperty::description
	String_t* ___description_7;
	// System.Configuration.ConfigurationCollectionAttribute System.Configuration.ConfigurationProperty::collectionAttribute
	ConfigurationCollectionAttribute_t3695308734 * ___collectionAttribute_8;

public:
	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(ConfigurationProperty_t3590861854, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(ConfigurationProperty_t3590861854, ___type_2)); }
	inline Type_t * get_type_2() const { return ___type_2; }
	inline Type_t ** get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(Type_t * value)
	{
		___type_2 = value;
		Il2CppCodeGenWriteBarrier((&___type_2), value);
	}

	inline static int32_t get_offset_of_default_value_3() { return static_cast<int32_t>(offsetof(ConfigurationProperty_t3590861854, ___default_value_3)); }
	inline RuntimeObject * get_default_value_3() const { return ___default_value_3; }
	inline RuntimeObject ** get_address_of_default_value_3() { return &___default_value_3; }
	inline void set_default_value_3(RuntimeObject * value)
	{
		___default_value_3 = value;
		Il2CppCodeGenWriteBarrier((&___default_value_3), value);
	}

	inline static int32_t get_offset_of_converter_4() { return static_cast<int32_t>(offsetof(ConfigurationProperty_t3590861854, ___converter_4)); }
	inline TypeConverter_t2249118273 * get_converter_4() const { return ___converter_4; }
	inline TypeConverter_t2249118273 ** get_address_of_converter_4() { return &___converter_4; }
	inline void set_converter_4(TypeConverter_t2249118273 * value)
	{
		___converter_4 = value;
		Il2CppCodeGenWriteBarrier((&___converter_4), value);
	}

	inline static int32_t get_offset_of_validation_5() { return static_cast<int32_t>(offsetof(ConfigurationProperty_t3590861854, ___validation_5)); }
	inline ConfigurationValidatorBase_t888490966 * get_validation_5() const { return ___validation_5; }
	inline ConfigurationValidatorBase_t888490966 ** get_address_of_validation_5() { return &___validation_5; }
	inline void set_validation_5(ConfigurationValidatorBase_t888490966 * value)
	{
		___validation_5 = value;
		Il2CppCodeGenWriteBarrier((&___validation_5), value);
	}

	inline static int32_t get_offset_of_flags_6() { return static_cast<int32_t>(offsetof(ConfigurationProperty_t3590861854, ___flags_6)); }
	inline int32_t get_flags_6() const { return ___flags_6; }
	inline int32_t* get_address_of_flags_6() { return &___flags_6; }
	inline void set_flags_6(int32_t value)
	{
		___flags_6 = value;
	}

	inline static int32_t get_offset_of_description_7() { return static_cast<int32_t>(offsetof(ConfigurationProperty_t3590861854, ___description_7)); }
	inline String_t* get_description_7() const { return ___description_7; }
	inline String_t** get_address_of_description_7() { return &___description_7; }
	inline void set_description_7(String_t* value)
	{
		___description_7 = value;
		Il2CppCodeGenWriteBarrier((&___description_7), value);
	}

	inline static int32_t get_offset_of_collectionAttribute_8() { return static_cast<int32_t>(offsetof(ConfigurationProperty_t3590861854, ___collectionAttribute_8)); }
	inline ConfigurationCollectionAttribute_t3695308734 * get_collectionAttribute_8() const { return ___collectionAttribute_8; }
	inline ConfigurationCollectionAttribute_t3695308734 ** get_address_of_collectionAttribute_8() { return &___collectionAttribute_8; }
	inline void set_collectionAttribute_8(ConfigurationCollectionAttribute_t3695308734 * value)
	{
		___collectionAttribute_8 = value;
		Il2CppCodeGenWriteBarrier((&___collectionAttribute_8), value);
	}
};

struct ConfigurationProperty_t3590861854_StaticFields
{
public:
	// System.Object System.Configuration.ConfigurationProperty::NoDefaultValue
	RuntimeObject * ___NoDefaultValue_0;

public:
	inline static int32_t get_offset_of_NoDefaultValue_0() { return static_cast<int32_t>(offsetof(ConfigurationProperty_t3590861854_StaticFields, ___NoDefaultValue_0)); }
	inline RuntimeObject * get_NoDefaultValue_0() const { return ___NoDefaultValue_0; }
	inline RuntimeObject ** get_address_of_NoDefaultValue_0() { return &___NoDefaultValue_0; }
	inline void set_NoDefaultValue_0(RuntimeObject * value)
	{
		___NoDefaultValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___NoDefaultValue_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONPROPERTY_T3590861854_H
#ifndef CONFIGURATIONLOCKCOLLECTION_T4066281341_H
#define CONFIGURATIONLOCKCOLLECTION_T4066281341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationLockCollection
struct  ConfigurationLockCollection_t4066281341  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Configuration.ConfigurationLockCollection::names
	ArrayList_t2718874744 * ___names_0;
	// System.Configuration.ConfigurationElement System.Configuration.ConfigurationLockCollection::element
	ConfigurationElement_t3318566633 * ___element_1;
	// System.Configuration.ConfigurationLockType System.Configuration.ConfigurationLockCollection::lockType
	int32_t ___lockType_2;
	// System.Boolean System.Configuration.ConfigurationLockCollection::is_modified
	bool ___is_modified_3;
	// System.Collections.Hashtable System.Configuration.ConfigurationLockCollection::valid_name_hash
	Hashtable_t1853889766 * ___valid_name_hash_4;
	// System.String System.Configuration.ConfigurationLockCollection::valid_names
	String_t* ___valid_names_5;

public:
	inline static int32_t get_offset_of_names_0() { return static_cast<int32_t>(offsetof(ConfigurationLockCollection_t4066281341, ___names_0)); }
	inline ArrayList_t2718874744 * get_names_0() const { return ___names_0; }
	inline ArrayList_t2718874744 ** get_address_of_names_0() { return &___names_0; }
	inline void set_names_0(ArrayList_t2718874744 * value)
	{
		___names_0 = value;
		Il2CppCodeGenWriteBarrier((&___names_0), value);
	}

	inline static int32_t get_offset_of_element_1() { return static_cast<int32_t>(offsetof(ConfigurationLockCollection_t4066281341, ___element_1)); }
	inline ConfigurationElement_t3318566633 * get_element_1() const { return ___element_1; }
	inline ConfigurationElement_t3318566633 ** get_address_of_element_1() { return &___element_1; }
	inline void set_element_1(ConfigurationElement_t3318566633 * value)
	{
		___element_1 = value;
		Il2CppCodeGenWriteBarrier((&___element_1), value);
	}

	inline static int32_t get_offset_of_lockType_2() { return static_cast<int32_t>(offsetof(ConfigurationLockCollection_t4066281341, ___lockType_2)); }
	inline int32_t get_lockType_2() const { return ___lockType_2; }
	inline int32_t* get_address_of_lockType_2() { return &___lockType_2; }
	inline void set_lockType_2(int32_t value)
	{
		___lockType_2 = value;
	}

	inline static int32_t get_offset_of_is_modified_3() { return static_cast<int32_t>(offsetof(ConfigurationLockCollection_t4066281341, ___is_modified_3)); }
	inline bool get_is_modified_3() const { return ___is_modified_3; }
	inline bool* get_address_of_is_modified_3() { return &___is_modified_3; }
	inline void set_is_modified_3(bool value)
	{
		___is_modified_3 = value;
	}

	inline static int32_t get_offset_of_valid_name_hash_4() { return static_cast<int32_t>(offsetof(ConfigurationLockCollection_t4066281341, ___valid_name_hash_4)); }
	inline Hashtable_t1853889766 * get_valid_name_hash_4() const { return ___valid_name_hash_4; }
	inline Hashtable_t1853889766 ** get_address_of_valid_name_hash_4() { return &___valid_name_hash_4; }
	inline void set_valid_name_hash_4(Hashtable_t1853889766 * value)
	{
		___valid_name_hash_4 = value;
		Il2CppCodeGenWriteBarrier((&___valid_name_hash_4), value);
	}

	inline static int32_t get_offset_of_valid_names_5() { return static_cast<int32_t>(offsetof(ConfigurationLockCollection_t4066281341, ___valid_names_5)); }
	inline String_t* get_valid_names_5() const { return ___valid_names_5; }
	inline String_t** get_address_of_valid_names_5() { return &___valid_names_5; }
	inline void set_valid_names_5(String_t* value)
	{
		___valid_names_5 = value;
		Il2CppCodeGenWriteBarrier((&___valid_names_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONLOCKCOLLECTION_T4066281341_H
#ifndef PROPERTYINFORMATION_T4136807793_H
#define PROPERTYINFORMATION_T4136807793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.PropertyInformation
struct  PropertyInformation_t4136807793  : public RuntimeObject
{
public:
	// System.Boolean System.Configuration.PropertyInformation::isModified
	bool ___isModified_0;
	// System.Object System.Configuration.PropertyInformation::val
	RuntimeObject * ___val_1;
	// System.Configuration.PropertyValueOrigin System.Configuration.PropertyInformation::origin
	int32_t ___origin_2;
	// System.Configuration.ConfigurationElement System.Configuration.PropertyInformation::owner
	ConfigurationElement_t3318566633 * ___owner_3;
	// System.Configuration.ConfigurationProperty System.Configuration.PropertyInformation::property
	ConfigurationProperty_t3590861854 * ___property_4;

public:
	inline static int32_t get_offset_of_isModified_0() { return static_cast<int32_t>(offsetof(PropertyInformation_t4136807793, ___isModified_0)); }
	inline bool get_isModified_0() const { return ___isModified_0; }
	inline bool* get_address_of_isModified_0() { return &___isModified_0; }
	inline void set_isModified_0(bool value)
	{
		___isModified_0 = value;
	}

	inline static int32_t get_offset_of_val_1() { return static_cast<int32_t>(offsetof(PropertyInformation_t4136807793, ___val_1)); }
	inline RuntimeObject * get_val_1() const { return ___val_1; }
	inline RuntimeObject ** get_address_of_val_1() { return &___val_1; }
	inline void set_val_1(RuntimeObject * value)
	{
		___val_1 = value;
		Il2CppCodeGenWriteBarrier((&___val_1), value);
	}

	inline static int32_t get_offset_of_origin_2() { return static_cast<int32_t>(offsetof(PropertyInformation_t4136807793, ___origin_2)); }
	inline int32_t get_origin_2() const { return ___origin_2; }
	inline int32_t* get_address_of_origin_2() { return &___origin_2; }
	inline void set_origin_2(int32_t value)
	{
		___origin_2 = value;
	}

	inline static int32_t get_offset_of_owner_3() { return static_cast<int32_t>(offsetof(PropertyInformation_t4136807793, ___owner_3)); }
	inline ConfigurationElement_t3318566633 * get_owner_3() const { return ___owner_3; }
	inline ConfigurationElement_t3318566633 ** get_address_of_owner_3() { return &___owner_3; }
	inline void set_owner_3(ConfigurationElement_t3318566633 * value)
	{
		___owner_3 = value;
		Il2CppCodeGenWriteBarrier((&___owner_3), value);
	}

	inline static int32_t get_offset_of_property_4() { return static_cast<int32_t>(offsetof(PropertyInformation_t4136807793, ___property_4)); }
	inline ConfigurationProperty_t3590861854 * get_property_4() const { return ___property_4; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_property_4() { return &___property_4; }
	inline void set_property_4(ConfigurationProperty_t3590861854 * value)
	{
		___property_4 = value;
		Il2CppCodeGenWriteBarrier((&___property_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYINFORMATION_T4136807793_H
#ifndef TRANSITIONTIME_T449921781_H
#define TRANSITIONTIME_T449921781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeZoneInfo/TransitionTime
struct  TransitionTime_t449921781 
{
public:
	// System.DateTime System.TimeZoneInfo/TransitionTime::timeOfDay
	DateTime_t3738529785  ___timeOfDay_0;
	// System.Int32 System.TimeZoneInfo/TransitionTime::month
	int32_t ___month_1;
	// System.Int32 System.TimeZoneInfo/TransitionTime::day
	int32_t ___day_2;
	// System.Int32 System.TimeZoneInfo/TransitionTime::week
	int32_t ___week_3;
	// System.DayOfWeek System.TimeZoneInfo/TransitionTime::dayOfWeek
	int32_t ___dayOfWeek_4;
	// System.Boolean System.TimeZoneInfo/TransitionTime::isFixedDateRule
	bool ___isFixedDateRule_5;

public:
	inline static int32_t get_offset_of_timeOfDay_0() { return static_cast<int32_t>(offsetof(TransitionTime_t449921781, ___timeOfDay_0)); }
	inline DateTime_t3738529785  get_timeOfDay_0() const { return ___timeOfDay_0; }
	inline DateTime_t3738529785 * get_address_of_timeOfDay_0() { return &___timeOfDay_0; }
	inline void set_timeOfDay_0(DateTime_t3738529785  value)
	{
		___timeOfDay_0 = value;
	}

	inline static int32_t get_offset_of_month_1() { return static_cast<int32_t>(offsetof(TransitionTime_t449921781, ___month_1)); }
	inline int32_t get_month_1() const { return ___month_1; }
	inline int32_t* get_address_of_month_1() { return &___month_1; }
	inline void set_month_1(int32_t value)
	{
		___month_1 = value;
	}

	inline static int32_t get_offset_of_day_2() { return static_cast<int32_t>(offsetof(TransitionTime_t449921781, ___day_2)); }
	inline int32_t get_day_2() const { return ___day_2; }
	inline int32_t* get_address_of_day_2() { return &___day_2; }
	inline void set_day_2(int32_t value)
	{
		___day_2 = value;
	}

	inline static int32_t get_offset_of_week_3() { return static_cast<int32_t>(offsetof(TransitionTime_t449921781, ___week_3)); }
	inline int32_t get_week_3() const { return ___week_3; }
	inline int32_t* get_address_of_week_3() { return &___week_3; }
	inline void set_week_3(int32_t value)
	{
		___week_3 = value;
	}

	inline static int32_t get_offset_of_dayOfWeek_4() { return static_cast<int32_t>(offsetof(TransitionTime_t449921781, ___dayOfWeek_4)); }
	inline int32_t get_dayOfWeek_4() const { return ___dayOfWeek_4; }
	inline int32_t* get_address_of_dayOfWeek_4() { return &___dayOfWeek_4; }
	inline void set_dayOfWeek_4(int32_t value)
	{
		___dayOfWeek_4 = value;
	}

	inline static int32_t get_offset_of_isFixedDateRule_5() { return static_cast<int32_t>(offsetof(TransitionTime_t449921781, ___isFixedDateRule_5)); }
	inline bool get_isFixedDateRule_5() const { return ___isFixedDateRule_5; }
	inline bool* get_address_of_isFixedDateRule_5() { return &___isFixedDateRule_5; }
	inline void set_isFixedDateRule_5(bool value)
	{
		___isFixedDateRule_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.TimeZoneInfo/TransitionTime
struct TransitionTime_t449921781_marshaled_pinvoke
{
	DateTime_t3738529785  ___timeOfDay_0;
	int32_t ___month_1;
	int32_t ___day_2;
	int32_t ___week_3;
	int32_t ___dayOfWeek_4;
	int32_t ___isFixedDateRule_5;
};
// Native definition for COM marshalling of System.TimeZoneInfo/TransitionTime
struct TransitionTime_t449921781_marshaled_com
{
	DateTime_t3738529785  ___timeOfDay_0;
	int32_t ___month_1;
	int32_t ___day_2;
	int32_t ___week_3;
	int32_t ___dayOfWeek_4;
	int32_t ___isFixedDateRule_5;
};
#endif // TRANSITIONTIME_T449921781_H
#ifndef ADJUSTMENTRULE_T74449909_H
#define ADJUSTMENTRULE_T74449909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeZoneInfo/AdjustmentRule
struct  AdjustmentRule_t74449909  : public RuntimeObject
{
public:
	// System.DateTime System.TimeZoneInfo/AdjustmentRule::dateEnd
	DateTime_t3738529785  ___dateEnd_0;
	// System.DateTime System.TimeZoneInfo/AdjustmentRule::dateStart
	DateTime_t3738529785  ___dateStart_1;
	// System.TimeSpan System.TimeZoneInfo/AdjustmentRule::daylightDelta
	TimeSpan_t881159249  ___daylightDelta_2;
	// System.TimeZoneInfo/TransitionTime System.TimeZoneInfo/AdjustmentRule::daylightTransitionEnd
	TransitionTime_t449921781  ___daylightTransitionEnd_3;
	// System.TimeZoneInfo/TransitionTime System.TimeZoneInfo/AdjustmentRule::daylightTransitionStart
	TransitionTime_t449921781  ___daylightTransitionStart_4;

public:
	inline static int32_t get_offset_of_dateEnd_0() { return static_cast<int32_t>(offsetof(AdjustmentRule_t74449909, ___dateEnd_0)); }
	inline DateTime_t3738529785  get_dateEnd_0() const { return ___dateEnd_0; }
	inline DateTime_t3738529785 * get_address_of_dateEnd_0() { return &___dateEnd_0; }
	inline void set_dateEnd_0(DateTime_t3738529785  value)
	{
		___dateEnd_0 = value;
	}

	inline static int32_t get_offset_of_dateStart_1() { return static_cast<int32_t>(offsetof(AdjustmentRule_t74449909, ___dateStart_1)); }
	inline DateTime_t3738529785  get_dateStart_1() const { return ___dateStart_1; }
	inline DateTime_t3738529785 * get_address_of_dateStart_1() { return &___dateStart_1; }
	inline void set_dateStart_1(DateTime_t3738529785  value)
	{
		___dateStart_1 = value;
	}

	inline static int32_t get_offset_of_daylightDelta_2() { return static_cast<int32_t>(offsetof(AdjustmentRule_t74449909, ___daylightDelta_2)); }
	inline TimeSpan_t881159249  get_daylightDelta_2() const { return ___daylightDelta_2; }
	inline TimeSpan_t881159249 * get_address_of_daylightDelta_2() { return &___daylightDelta_2; }
	inline void set_daylightDelta_2(TimeSpan_t881159249  value)
	{
		___daylightDelta_2 = value;
	}

	inline static int32_t get_offset_of_daylightTransitionEnd_3() { return static_cast<int32_t>(offsetof(AdjustmentRule_t74449909, ___daylightTransitionEnd_3)); }
	inline TransitionTime_t449921781  get_daylightTransitionEnd_3() const { return ___daylightTransitionEnd_3; }
	inline TransitionTime_t449921781 * get_address_of_daylightTransitionEnd_3() { return &___daylightTransitionEnd_3; }
	inline void set_daylightTransitionEnd_3(TransitionTime_t449921781  value)
	{
		___daylightTransitionEnd_3 = value;
	}

	inline static int32_t get_offset_of_daylightTransitionStart_4() { return static_cast<int32_t>(offsetof(AdjustmentRule_t74449909, ___daylightTransitionStart_4)); }
	inline TransitionTime_t449921781  get_daylightTransitionStart_4() const { return ___daylightTransitionStart_4; }
	inline TransitionTime_t449921781 * get_address_of_daylightTransitionStart_4() { return &___daylightTransitionStart_4; }
	inline void set_daylightTransitionStart_4(TransitionTime_t449921781  value)
	{
		___daylightTransitionStart_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADJUSTMENTRULE_T74449909_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (U24ArrayTypeU244_t1630999355)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU244_t1630999355 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (U3CModuleU3E_t692745529), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (ProviderBase_t2594774949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2008[3] = 
{
	ProviderBase_t2594774949::get_offset_of_alreadyInitialized_0(),
	ProviderBase_t2594774949::get_offset_of__description_1(),
	ProviderBase_t2594774949::get_offset_of__name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (ProviderCollection_t3810574769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2009[3] = 
{
	ProviderCollection_t3810574769::get_offset_of_lookup_0(),
	ProviderCollection_t3810574769::get_offset_of_readOnly_1(),
	ProviderCollection_t3810574769::get_offset_of_values_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (ClientConfigurationSystem_t3254701293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2010[1] = 
{
	ClientConfigurationSystem_t3254701293::get_offset_of_cfg_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (ConfigNameValueCollection_t1573003828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2011[1] = 
{
	ConfigNameValueCollection_t1573003828::get_offset_of_modified_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (ConfigInfo_t2364721456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2012[6] = 
{
	ConfigInfo_t2364721456::get_offset_of_Name_0(),
	ConfigInfo_t2364721456::get_offset_of_TypeName_1(),
	ConfigInfo_t2364721456::get_offset_of_Type_2(),
	ConfigInfo_t2364721456::get_offset_of_streamName_3(),
	ConfigInfo_t2364721456::get_offset_of_Parent_4(),
	ConfigInfo_t2364721456::get_offset_of_ConfigHost_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (Configuration_t2529364143), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2013[12] = 
{
	Configuration_t2529364143::get_offset_of_parent_0(),
	Configuration_t2529364143::get_offset_of_elementData_1(),
	Configuration_t2529364143::get_offset_of_streamName_2(),
	Configuration_t2529364143::get_offset_of_rootSectionGroup_3(),
	Configuration_t2529364143::get_offset_of_locations_4(),
	Configuration_t2529364143::get_offset_of_rootGroup_5(),
	Configuration_t2529364143::get_offset_of_system_6(),
	Configuration_t2529364143::get_offset_of_hasFile_7(),
	Configuration_t2529364143::get_offset_of_rootNamespace_8(),
	Configuration_t2529364143::get_offset_of_configPath_9(),
	Configuration_t2529364143::get_offset_of_locationConfigPath_10(),
	Configuration_t2529364143::get_offset_of_locationSubPath_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (ConfigurationAllowDefinition_t1879228320)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2014[5] = 
{
	ConfigurationAllowDefinition_t1879228320::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (ConfigurationAllowExeDefinition_t2804732269)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2015[5] = 
{
	ConfigurationAllowExeDefinition_t2804732269::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (ConfigurationCollectionAttribute_t3695308734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2016[5] = 
{
	ConfigurationCollectionAttribute_t3695308734::get_offset_of_addItemName_0(),
	ConfigurationCollectionAttribute_t3695308734::get_offset_of_clearItemsName_1(),
	ConfigurationCollectionAttribute_t3695308734::get_offset_of_removeItemName_2(),
	ConfigurationCollectionAttribute_t3695308734::get_offset_of_collectionType_3(),
	ConfigurationCollectionAttribute_t3695308734::get_offset_of_itemType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (ConfigurationElement_t3318566633), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2017[13] = 
{
	ConfigurationElement_t3318566633::get_offset_of_rawXml_0(),
	ConfigurationElement_t3318566633::get_offset_of_modified_1(),
	ConfigurationElement_t3318566633::get_offset_of_map_2(),
	ConfigurationElement_t3318566633::get_offset_of_keyProps_3(),
	ConfigurationElement_t3318566633::get_offset_of_defaultCollection_4(),
	ConfigurationElement_t3318566633::get_offset_of_readOnly_5(),
	ConfigurationElement_t3318566633::get_offset_of_elementInfo_6(),
	ConfigurationElement_t3318566633::get_offset_of__configuration_7(),
	ConfigurationElement_t3318566633::get_offset_of_lockAllAttributesExcept_8(),
	ConfigurationElement_t3318566633::get_offset_of_lockAllElementsExcept_9(),
	ConfigurationElement_t3318566633::get_offset_of_lockAttributes_10(),
	ConfigurationElement_t3318566633::get_offset_of_lockElements_11(),
	ConfigurationElement_t3318566633::get_offset_of_lockItem_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (ElementMap_t2160633803), -1, sizeof(ElementMap_t2160633803_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2018[3] = 
{
	ElementMap_t2160633803_StaticFields::get_offset_of_elementMaps_0(),
	ElementMap_t2160633803::get_offset_of_properties_1(),
	ElementMap_t2160633803::get_offset_of_collectionAttribute_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (ConfigurationElementCollection_t446763386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2019[10] = 
{
	ConfigurationElementCollection_t446763386::get_offset_of_list_13(),
	ConfigurationElementCollection_t446763386::get_offset_of_removed_14(),
	ConfigurationElementCollection_t446763386::get_offset_of_inherited_15(),
	ConfigurationElementCollection_t446763386::get_offset_of_emitClear_16(),
	ConfigurationElementCollection_t446763386::get_offset_of_modified_17(),
	ConfigurationElementCollection_t446763386::get_offset_of_comparer_18(),
	ConfigurationElementCollection_t446763386::get_offset_of_inheritedLimitIndex_19(),
	ConfigurationElementCollection_t446763386::get_offset_of_addElementName_20(),
	ConfigurationElementCollection_t446763386::get_offset_of_clearElementName_21(),
	ConfigurationElementCollection_t446763386::get_offset_of_removeElementName_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (ConfigurationRemoveElement_t1327025031), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2020[3] = 
{
	ConfigurationRemoveElement_t1327025031::get_offset_of_properties_13(),
	ConfigurationRemoveElement_t1327025031::get_offset_of__origElement_14(),
	ConfigurationRemoveElement_t1327025031::get_offset_of__origCollection_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (ConfigurationElementCollectionType_t2560831360)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2021[5] = 
{
	ConfigurationElementCollectionType_t2560831360::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (ConfigurationErrorsException_t66512296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2022[2] = 
{
	ConfigurationErrorsException_t66512296::get_offset_of_filename_13(),
	ConfigurationErrorsException_t66512296::get_offset_of_line_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (ConfigurationFileMap_t3010527243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2023[1] = 
{
	ConfigurationFileMap_t3010527243::get_offset_of_machineConfigFilename_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (ConfigurationLocation_t372136312), -1, sizeof(ConfigurationLocation_t372136312_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2024[7] = 
{
	ConfigurationLocation_t372136312_StaticFields::get_offset_of_pathTrimChars_0(),
	ConfigurationLocation_t372136312::get_offset_of_path_1(),
	ConfigurationLocation_t372136312::get_offset_of_configuration_2(),
	ConfigurationLocation_t372136312::get_offset_of_parent_3(),
	ConfigurationLocation_t372136312::get_offset_of_xmlContent_4(),
	ConfigurationLocation_t372136312::get_offset_of_parentResolved_5(),
	ConfigurationLocation_t372136312::get_offset_of_allowOverride_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (ConfigurationLocationCollection_t1538619295), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (ConfigurationLockType_t161400359)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2026[4] = 
{
	ConfigurationLockType_t161400359::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (ConfigurationLockCollection_t4066281341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2027[6] = 
{
	ConfigurationLockCollection_t4066281341::get_offset_of_names_0(),
	ConfigurationLockCollection_t4066281341::get_offset_of_element_1(),
	ConfigurationLockCollection_t4066281341::get_offset_of_lockType_2(),
	ConfigurationLockCollection_t4066281341::get_offset_of_is_modified_3(),
	ConfigurationLockCollection_t4066281341::get_offset_of_valid_name_hash_4(),
	ConfigurationLockCollection_t4066281341::get_offset_of_valid_names_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (ConfigurationManager_t386529156), -1, sizeof(ConfigurationManager_t386529156_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2028[3] = 
{
	ConfigurationManager_t386529156_StaticFields::get_offset_of_configFactory_0(),
	ConfigurationManager_t386529156_StaticFields::get_offset_of_configSystem_1(),
	ConfigurationManager_t386529156_StaticFields::get_offset_of_lockobj_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (ConfigurationProperty_t3590861854), -1, sizeof(ConfigurationProperty_t3590861854_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2029[9] = 
{
	ConfigurationProperty_t3590861854_StaticFields::get_offset_of_NoDefaultValue_0(),
	ConfigurationProperty_t3590861854::get_offset_of_name_1(),
	ConfigurationProperty_t3590861854::get_offset_of_type_2(),
	ConfigurationProperty_t3590861854::get_offset_of_default_value_3(),
	ConfigurationProperty_t3590861854::get_offset_of_converter_4(),
	ConfigurationProperty_t3590861854::get_offset_of_validation_5(),
	ConfigurationProperty_t3590861854::get_offset_of_flags_6(),
	ConfigurationProperty_t3590861854::get_offset_of_description_7(),
	ConfigurationProperty_t3590861854::get_offset_of_collectionAttribute_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (ConfigurationPropertyAttribute_t2659780707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2030[3] = 
{
	ConfigurationPropertyAttribute_t2659780707::get_offset_of_name_0(),
	ConfigurationPropertyAttribute_t2659780707::get_offset_of_default_value_1(),
	ConfigurationPropertyAttribute_t2659780707::get_offset_of_flags_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (ConfigurationPropertyCollection_t2852175726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2031[1] = 
{
	ConfigurationPropertyCollection_t2852175726::get_offset_of_collection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (ConfigurationPropertyOptions_t1059028815)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2032[5] = 
{
	ConfigurationPropertyOptions_t1059028815::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (ConfigurationSaveMode_t1605032206)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2033[4] = 
{
	ConfigurationSaveMode_t1605032206::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (ConfigurationSection_t3156163955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2034[4] = 
{
	ConfigurationSection_t3156163955::get_offset_of_sectionInformation_13(),
	ConfigurationSection_t3156163955::get_offset_of_section_handler_14(),
	ConfigurationSection_t3156163955::get_offset_of_externalDataXml_15(),
	ConfigurationSection_t3156163955::get_offset_of__configContext_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (ConfigurationSectionCollection_t2786897858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2035[2] = 
{
	ConfigurationSectionCollection_t2786897858::get_offset_of_group_10(),
	ConfigurationSectionCollection_t2786897858::get_offset_of_config_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (U3CGetEnumeratorU3Ec__Iterator0_t2103655423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2036[5] = 
{
	U3CGetEnumeratorU3Ec__Iterator0_t2103655423::get_offset_of_U3CU24s_32U3E__0_0(),
	U3CGetEnumeratorU3Ec__Iterator0_t2103655423::get_offset_of_U3CkeyU3E__1_1(),
	U3CGetEnumeratorU3Ec__Iterator0_t2103655423::get_offset_of_U24PC_2(),
	U3CGetEnumeratorU3Ec__Iterator0_t2103655423::get_offset_of_U24current_3(),
	U3CGetEnumeratorU3Ec__Iterator0_t2103655423::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (ConfigurationSectionGroup_t4179402520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2037[5] = 
{
	ConfigurationSectionGroup_t4179402520::get_offset_of_sections_0(),
	ConfigurationSectionGroup_t4179402520::get_offset_of_groups_1(),
	ConfigurationSectionGroup_t4179402520::get_offset_of_config_2(),
	ConfigurationSectionGroup_t4179402520::get_offset_of_group_3(),
	ConfigurationSectionGroup_t4179402520::get_offset_of_initialized_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (ConfigurationSectionGroupCollection_t1151641153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2038[2] = 
{
	ConfigurationSectionGroupCollection_t1151641153::get_offset_of_group_10(),
	ConfigurationSectionGroupCollection_t1151641153::get_offset_of_config_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (ConfigurationUserLevel_t2700976468)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2039[4] = 
{
	ConfigurationUserLevel_t2700976468::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (ConfigurationValidatorAttribute_t448955463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2040[2] = 
{
	ConfigurationValidatorAttribute_t448955463::get_offset_of_validatorType_0(),
	ConfigurationValidatorAttribute_t448955463::get_offset_of_instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (ConfigurationValidatorBase_t888490966), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (ConfigXmlTextReader_t3683698114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2042[1] = 
{
	ConfigXmlTextReader_t3683698114::get_offset_of_fileName_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (DefaultSection_t752822653), -1, sizeof(DefaultSection_t752822653_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2043[1] = 
{
	DefaultSection_t752822653_StaticFields::get_offset_of_properties_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (DefaultValidator_t1354184753), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (ElementInformation_t2651568025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2045[3] = 
{
	ElementInformation_t2651568025::get_offset_of_propertyInfo_0(),
	ElementInformation_t2651568025::get_offset_of_owner_1(),
	ElementInformation_t2651568025::get_offset_of_properties_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (ExeConfigurationFileMap_t3751566101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2046[3] = 
{
	ExeConfigurationFileMap_t3751566101::get_offset_of_exeConfigFilename_1(),
	ExeConfigurationFileMap_t3751566101::get_offset_of_localUserConfigFilename_2(),
	ExeConfigurationFileMap_t3751566101::get_offset_of_roamingUserConfigFilename_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (IgnoreSection_t2596619786), -1, sizeof(IgnoreSection_t2596619786_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2047[2] = 
{
	IgnoreSection_t2596619786::get_offset_of_xml_17(),
	IgnoreSection_t2596619786_StaticFields::get_offset_of_properties_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (InternalConfigurationFactory_t2066639612), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (InternalConfigurationSystem_t844406905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2049[3] = 
{
	InternalConfigurationSystem_t844406905::get_offset_of_host_0(),
	InternalConfigurationSystem_t844406905::get_offset_of_root_1(),
	InternalConfigurationSystem_t844406905::get_offset_of_hostInitParams_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (InternalConfigurationHost_t969478808), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (ExeConfigurationHost_t3079009552), -1, sizeof(ExeConfigurationHost_t3079009552_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2051[3] = 
{
	ExeConfigurationHost_t3079009552::get_offset_of_map_0(),
	ExeConfigurationHost_t3079009552::get_offset_of_level_1(),
	ExeConfigurationHost_t3079009552_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (InternalConfigurationRoot_t3745416310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2052[2] = 
{
	InternalConfigurationRoot_t3745416310::get_offset_of_host_0(),
	InternalConfigurationRoot_t3745416310::get_offset_of_isDesignTime_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (PropertyInformation_t4136807793), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2053[5] = 
{
	PropertyInformation_t4136807793::get_offset_of_isModified_0(),
	PropertyInformation_t4136807793::get_offset_of_val_1(),
	PropertyInformation_t4136807793::get_offset_of_origin_2(),
	PropertyInformation_t4136807793::get_offset_of_owner_3(),
	PropertyInformation_t4136807793::get_offset_of_property_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (PropertyInformationCollection_t3866802480), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (PropertyInformationEnumerator_t3780095919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2055[2] = 
{
	PropertyInformationEnumerator_t3780095919::get_offset_of_collection_0(),
	PropertyInformationEnumerator_t3780095919::get_offset_of_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (PropertyValueOrigin_t225613163)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2056[4] = 
{
	PropertyValueOrigin_t225613163::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (ProtectedConfiguration_t25042150), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (ProtectedConfigurationProvider_t413290519), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (ProtectedConfigurationProviderCollection_t729952796), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (ProtectedConfigurationSection_t1525901209), -1, sizeof(ProtectedConfigurationSection_t1525901209_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2060[4] = 
{
	ProtectedConfigurationSection_t1525901209_StaticFields::get_offset_of_defaultProviderProp_17(),
	ProtectedConfigurationSection_t1525901209_StaticFields::get_offset_of_providersProp_18(),
	ProtectedConfigurationSection_t1525901209_StaticFields::get_offset_of_properties_19(),
	ProtectedConfigurationSection_t1525901209::get_offset_of_providers_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (ProviderSettings_t4011220768), -1, sizeof(ProviderSettings_t4011220768_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2061[4] = 
{
	ProviderSettings_t4011220768::get_offset_of_parameters_13(),
	ProviderSettings_t4011220768_StaticFields::get_offset_of_nameProp_14(),
	ProviderSettings_t4011220768_StaticFields::get_offset_of_typeProp_15(),
	ProviderSettings_t4011220768_StaticFields::get_offset_of_properties_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (ProviderSettingsCollection_t3942935190), -1, sizeof(ProviderSettingsCollection_t3942935190_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2062[1] = 
{
	ProviderSettingsCollection_t3942935190_StaticFields::get_offset_of_props_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (SectionInfo_t640588401), -1, sizeof(SectionInfo_t640588401_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2063[6] = 
{
	SectionInfo_t640588401::get_offset_of_allowLocation_6(),
	SectionInfo_t640588401::get_offset_of_requirePermission_7(),
	SectionInfo_t640588401::get_offset_of_restartOnExternalChanges_8(),
	SectionInfo_t640588401::get_offset_of_allowDefinition_9(),
	SectionInfo_t640588401::get_offset_of_allowExeDefinition_10(),
	SectionInfo_t640588401_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (SectionGroupInfo_t687896682), -1, sizeof(SectionGroupInfo_t687896682_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2064[3] = 
{
	SectionGroupInfo_t687896682::get_offset_of_sections_6(),
	SectionGroupInfo_t687896682::get_offset_of_groups_7(),
	SectionGroupInfo_t687896682_StaticFields::get_offset_of_emptyList_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (ConfigInfoCollection_t215413917), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (SectionInformation_t2821611020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2066[13] = 
{
	SectionInformation_t2821611020::get_offset_of_parent_0(),
	SectionInformation_t2821611020::get_offset_of_allow_definition_1(),
	SectionInformation_t2821611020::get_offset_of_allow_exe_definition_2(),
	SectionInformation_t2821611020::get_offset_of_allow_location_3(),
	SectionInformation_t2821611020::get_offset_of_allow_override_4(),
	SectionInformation_t2821611020::get_offset_of_inherit_on_child_apps_5(),
	SectionInformation_t2821611020::get_offset_of_restart_on_external_changes_6(),
	SectionInformation_t2821611020::get_offset_of_require_permission_7(),
	SectionInformation_t2821611020::get_offset_of_config_source_8(),
	SectionInformation_t2821611020::get_offset_of_name_9(),
	SectionInformation_t2821611020::get_offset_of_raw_xml_10(),
	SectionInformation_t2821611020::get_offset_of_protection_provider_11(),
	SectionInformation_t2821611020::get_offset_of_U3CConfigFilePathU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (MonoTODOAttribute_t4131080584), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2067[1] = 
{
	MonoTODOAttribute_t4131080584::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (MonoInternalNoteAttribute_t2284022217), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (U3CModuleU3E_t692745530), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (ExtensionAttribute_t1723066603), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (Locale_t4128636110), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (MonoTODOAttribute_t4131080585), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (KeyBuilder_t2049230356), -1, sizeof(KeyBuilder_t2049230356_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2073[1] = 
{
	KeyBuilder_t2049230356_StaticFields::get_offset_of_rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (SymmetricTransform_t3802591843), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2074[12] = 
{
	SymmetricTransform_t3802591843::get_offset_of_algo_0(),
	SymmetricTransform_t3802591843::get_offset_of_encrypt_1(),
	SymmetricTransform_t3802591843::get_offset_of_BlockSizeByte_2(),
	SymmetricTransform_t3802591843::get_offset_of_temp_3(),
	SymmetricTransform_t3802591843::get_offset_of_temp2_4(),
	SymmetricTransform_t3802591843::get_offset_of_workBuff_5(),
	SymmetricTransform_t3802591843::get_offset_of_workout_6(),
	SymmetricTransform_t3802591843::get_offset_of_FeedBackByte_7(),
	SymmetricTransform_t3802591843::get_offset_of_FeedBackIter_8(),
	SymmetricTransform_t3802591843::get_offset_of_m_disposed_9(),
	SymmetricTransform_t3802591843::get_offset_of_lastBlock_10(),
	SymmetricTransform_t3802591843::get_offset_of__rng_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (InvalidTimeZoneException_t4066718130), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (TimeZoneInfo_t161405854), -1, sizeof(TimeZoneInfo_t161405854_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2076[10] = 
{
	TimeZoneInfo_t161405854::get_offset_of_baseUtcOffset_0(),
	TimeZoneInfo_t161405854::get_offset_of_daylightDisplayName_1(),
	TimeZoneInfo_t161405854::get_offset_of_displayName_2(),
	TimeZoneInfo_t161405854::get_offset_of_id_3(),
	TimeZoneInfo_t161405854_StaticFields::get_offset_of_local_4(),
	TimeZoneInfo_t161405854::get_offset_of_standardDisplayName_5(),
	TimeZoneInfo_t161405854::get_offset_of_disableDaylightSavingTime_6(),
	TimeZoneInfo_t161405854_StaticFields::get_offset_of_utc_7(),
	TimeZoneInfo_t161405854_StaticFields::get_offset_of_timeZoneDirectory_8(),
	TimeZoneInfo_t161405854::get_offset_of_adjustmentRules_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (AdjustmentRule_t74449909), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2077[5] = 
{
	AdjustmentRule_t74449909::get_offset_of_dateEnd_0(),
	AdjustmentRule_t74449909::get_offset_of_dateStart_1(),
	AdjustmentRule_t74449909::get_offset_of_daylightDelta_2(),
	AdjustmentRule_t74449909::get_offset_of_daylightTransitionEnd_3(),
	AdjustmentRule_t74449909::get_offset_of_daylightTransitionStart_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (TimeType_t2507513283)+ sizeof (RuntimeObject), sizeof(TimeType_t2507513283_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2078[3] = 
{
	TimeType_t2507513283::get_offset_of_Offset_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TimeType_t2507513283::get_offset_of_IsDst_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TimeType_t2507513283::get_offset_of_Name_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (TransitionTime_t449921781)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2079[6] = 
{
	TransitionTime_t449921781::get_offset_of_timeOfDay_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransitionTime_t449921781::get_offset_of_month_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransitionTime_t449921781::get_offset_of_day_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransitionTime_t449921781::get_offset_of_week_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransitionTime_t449921781::get_offset_of_dayOfWeek_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransitionTime_t449921781::get_offset_of_isFixedDateRule_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (TimeZoneNotFoundException_t171073523), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (Check_t192468399), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (Enumerable_t538148348), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (Fallback_t3495999270)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2083[3] = 
{
	Fallback_t3495999270::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2084[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2085[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2086[18] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (U3CCreateRangeIteratorU3Ec__IteratorD_t2730143742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2087[7] = 
{
	U3CCreateRangeIteratorU3Ec__IteratorD_t2730143742::get_offset_of_start_0(),
	U3CCreateRangeIteratorU3Ec__IteratorD_t2730143742::get_offset_of_U3CiU3E__0_1(),
	U3CCreateRangeIteratorU3Ec__IteratorD_t2730143742::get_offset_of_upto_2(),
	U3CCreateRangeIteratorU3Ec__IteratorD_t2730143742::get_offset_of_U24PC_3(),
	U3CCreateRangeIteratorU3Ec__IteratorD_t2730143742::get_offset_of_U24current_4(),
	U3CCreateRangeIteratorU3Ec__IteratorD_t2730143742::get_offset_of_U3CU24U3Estart_5(),
	U3CCreateRangeIteratorU3Ec__IteratorD_t2730143742::get_offset_of_U3CU24U3Eupto_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2088[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2089[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2090[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2091[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2092[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2093[13] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2094[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2095[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2098[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2099[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
