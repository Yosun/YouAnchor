﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.String
struct String_t;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// CFX_AutoDestructShuriken
struct CFX_AutoDestructShuriken_t4057240314;
// UnityEngine.WWW
struct WWW_t3688466362;
// BlendShapeReader
struct BlendShapeReader_t1673803692;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UnityEngine.Camera
struct Camera_t4157153871;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Collections.Generic.Dictionary`2<System.String,System.Single>
struct Dictionary_2_t1182523073;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Material
struct Material_t340375123;
// System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Single>>
struct List_1_t2654597815;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t2869341516;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<UnityEngine.Quaternion>
struct List_1_t3774003073;
// BlendShapeReader/ProcessEachBlendShapeUpdate
struct ProcessEachBlendShapeUpdate_t611652369;
// BlendShapeReader/ProcessEachBlendShapeUpdateBasic
struct ProcessEachBlendShapeUpdateBasic_t1021678196;
// UIInputWait
struct UIInputWait_t3530943742;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// DG.Tweening.Tween
struct Tween_t2342918553;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// Avataaars/PartsAndFolderNames[]
struct PartsAndFolderNamesU5BU5D_t1814257234;
// Avataaars/AttachmentPointMapper[]
struct AttachmentPointMapperU5BU5D_t162241041;
// Avataaars/AvataaarPart[]
struct AvataaarPartU5BU5D_t11382455;
// Avataaars/EmotionsDefinable[]
struct EmotionsDefinableU5BU5D_t1185687067;
// System.Collections.Generic.Dictionary`2<Avataaars/AttachmentPoints,System.Collections.Generic.List`1<Avataaars/AvataaarPart>>
struct Dictionary_2_t2729386452;
// System.Collections.Generic.Dictionary`2<Avataaars/AttachmentPoints,UnityEngine.Transform>
struct Dictionary_2_t2442083389;
// System.Collections.Generic.Dictionary`2<Avataaars/Emotions,System.Collections.Generic.List`1<Avataaars/AvataaarPart>>
struct Dictionary_2_t4055422985;
// System.Collections.Generic.Dictionary`2<UnityEngine.Transform,System.Collections.Generic.List`1<UnityEngine.Sprite>>
struct Dictionary_2_t1063436421;
// System.Collections.Generic.Dictionary`2<UnityEngine.Transform,System.Collections.Generic.List`1<Avataaars/PartsAndFolderNames>>
struct Dictionary_2_t3688575236;
// System.String[]
struct StringU5BU5D_t1281789340;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface
struct UnityARSessionNativeInterface_t3929719369;
// PuppeteerByFace/ProcessEachBlendShapeUpdate
struct ProcessEachBlendShapeUpdate_t1319766324;
// UIToggleSet[]
struct UIToggleSetU5BU5D_t1159468339;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Texture3D
struct Texture3D_t1884131049;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UIInputWait/StringProcessor
struct StringProcessor_t3163369541;
// Colorful.Glitch/InterferenceSettings
struct InterferenceSettings_t2118005662;
// Colorful.Glitch/TearingSettings
struct TearingSettings_t1139179787;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Gradient
struct Gradient_t3067099924;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t934056436;
// UnityEngine.Component
struct Component_t1923634451;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef DOTWEENANIMATIONEXTENSIONS_T980608231_H
#define DOTWEENANIMATIONEXTENSIONS_T980608231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenAnimationExtensions
struct  DOTweenAnimationExtensions_t980608231  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENANIMATIONEXTENSIONS_T980608231_H
#ifndef PARSE_T3361751035_H
#define PARSE_T3361751035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Parse
struct  Parse_t3361751035  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSE_T3361751035_H
#ifndef UITOGGLESET_T1796645174_H
#define UITOGGLESET_T1796645174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIToggleSet
struct  UIToggleSet_t1796645174  : public RuntimeObject
{
public:
	// UnityEngine.Sprite UIToggleSet::sprite1
	Sprite_t280657092 * ___sprite1_0;
	// UnityEngine.Sprite UIToggleSet::sprite2
	Sprite_t280657092 * ___sprite2_1;
	// UnityEngine.UI.Image UIToggleSet::img
	Image_t2670269651 * ___img_2;

public:
	inline static int32_t get_offset_of_sprite1_0() { return static_cast<int32_t>(offsetof(UIToggleSet_t1796645174, ___sprite1_0)); }
	inline Sprite_t280657092 * get_sprite1_0() const { return ___sprite1_0; }
	inline Sprite_t280657092 ** get_address_of_sprite1_0() { return &___sprite1_0; }
	inline void set_sprite1_0(Sprite_t280657092 * value)
	{
		___sprite1_0 = value;
		Il2CppCodeGenWriteBarrier((&___sprite1_0), value);
	}

	inline static int32_t get_offset_of_sprite2_1() { return static_cast<int32_t>(offsetof(UIToggleSet_t1796645174, ___sprite2_1)); }
	inline Sprite_t280657092 * get_sprite2_1() const { return ___sprite2_1; }
	inline Sprite_t280657092 ** get_address_of_sprite2_1() { return &___sprite2_1; }
	inline void set_sprite2_1(Sprite_t280657092 * value)
	{
		___sprite2_1 = value;
		Il2CppCodeGenWriteBarrier((&___sprite2_1), value);
	}

	inline static int32_t get_offset_of_img_2() { return static_cast<int32_t>(offsetof(UIToggleSet_t1796645174, ___img_2)); }
	inline Image_t2670269651 * get_img_2() const { return ___img_2; }
	inline Image_t2670269651 ** get_address_of_img_2() { return &___img_2; }
	inline void set_img_2(Image_t2670269651 * value)
	{
		___img_2 = value;
		Il2CppCodeGenWriteBarrier((&___img_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITOGGLESET_T1796645174_H
#ifndef INTERFERENCESETTINGS_T2118005662_H
#define INTERFERENCESETTINGS_T2118005662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Glitch/InterferenceSettings
struct  InterferenceSettings_t2118005662  : public RuntimeObject
{
public:
	// System.Single Colorful.Glitch/InterferenceSettings::Speed
	float ___Speed_0;
	// System.Single Colorful.Glitch/InterferenceSettings::Density
	float ___Density_1;
	// System.Single Colorful.Glitch/InterferenceSettings::MaxDisplacement
	float ___MaxDisplacement_2;

public:
	inline static int32_t get_offset_of_Speed_0() { return static_cast<int32_t>(offsetof(InterferenceSettings_t2118005662, ___Speed_0)); }
	inline float get_Speed_0() const { return ___Speed_0; }
	inline float* get_address_of_Speed_0() { return &___Speed_0; }
	inline void set_Speed_0(float value)
	{
		___Speed_0 = value;
	}

	inline static int32_t get_offset_of_Density_1() { return static_cast<int32_t>(offsetof(InterferenceSettings_t2118005662, ___Density_1)); }
	inline float get_Density_1() const { return ___Density_1; }
	inline float* get_address_of_Density_1() { return &___Density_1; }
	inline void set_Density_1(float value)
	{
		___Density_1 = value;
	}

	inline static int32_t get_offset_of_MaxDisplacement_2() { return static_cast<int32_t>(offsetof(InterferenceSettings_t2118005662, ___MaxDisplacement_2)); }
	inline float get_MaxDisplacement_2() const { return ___MaxDisplacement_2; }
	inline float* get_address_of_MaxDisplacement_2() { return &___MaxDisplacement_2; }
	inline void set_MaxDisplacement_2(float value)
	{
		___MaxDisplacement_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERFERENCESETTINGS_T2118005662_H
#ifndef TEARINGSETTINGS_T1139179787_H
#define TEARINGSETTINGS_T1139179787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Glitch/TearingSettings
struct  TearingSettings_t1139179787  : public RuntimeObject
{
public:
	// System.Single Colorful.Glitch/TearingSettings::Speed
	float ___Speed_0;
	// System.Single Colorful.Glitch/TearingSettings::Intensity
	float ___Intensity_1;
	// System.Single Colorful.Glitch/TearingSettings::MaxDisplacement
	float ___MaxDisplacement_2;
	// System.Boolean Colorful.Glitch/TearingSettings::AllowFlipping
	bool ___AllowFlipping_3;
	// System.Boolean Colorful.Glitch/TearingSettings::YuvColorBleeding
	bool ___YuvColorBleeding_4;
	// System.Single Colorful.Glitch/TearingSettings::YuvOffset
	float ___YuvOffset_5;

public:
	inline static int32_t get_offset_of_Speed_0() { return static_cast<int32_t>(offsetof(TearingSettings_t1139179787, ___Speed_0)); }
	inline float get_Speed_0() const { return ___Speed_0; }
	inline float* get_address_of_Speed_0() { return &___Speed_0; }
	inline void set_Speed_0(float value)
	{
		___Speed_0 = value;
	}

	inline static int32_t get_offset_of_Intensity_1() { return static_cast<int32_t>(offsetof(TearingSettings_t1139179787, ___Intensity_1)); }
	inline float get_Intensity_1() const { return ___Intensity_1; }
	inline float* get_address_of_Intensity_1() { return &___Intensity_1; }
	inline void set_Intensity_1(float value)
	{
		___Intensity_1 = value;
	}

	inline static int32_t get_offset_of_MaxDisplacement_2() { return static_cast<int32_t>(offsetof(TearingSettings_t1139179787, ___MaxDisplacement_2)); }
	inline float get_MaxDisplacement_2() const { return ___MaxDisplacement_2; }
	inline float* get_address_of_MaxDisplacement_2() { return &___MaxDisplacement_2; }
	inline void set_MaxDisplacement_2(float value)
	{
		___MaxDisplacement_2 = value;
	}

	inline static int32_t get_offset_of_AllowFlipping_3() { return static_cast<int32_t>(offsetof(TearingSettings_t1139179787, ___AllowFlipping_3)); }
	inline bool get_AllowFlipping_3() const { return ___AllowFlipping_3; }
	inline bool* get_address_of_AllowFlipping_3() { return &___AllowFlipping_3; }
	inline void set_AllowFlipping_3(bool value)
	{
		___AllowFlipping_3 = value;
	}

	inline static int32_t get_offset_of_YuvColorBleeding_4() { return static_cast<int32_t>(offsetof(TearingSettings_t1139179787, ___YuvColorBleeding_4)); }
	inline bool get_YuvColorBleeding_4() const { return ___YuvColorBleeding_4; }
	inline bool* get_address_of_YuvColorBleeding_4() { return &___YuvColorBleeding_4; }
	inline void set_YuvColorBleeding_4(bool value)
	{
		___YuvColorBleeding_4 = value;
	}

	inline static int32_t get_offset_of_YuvOffset_5() { return static_cast<int32_t>(offsetof(TearingSettings_t1139179787, ___YuvOffset_5)); }
	inline float get_YuvOffset_5() const { return ___YuvOffset_5; }
	inline float* get_address_of_YuvOffset_5() { return &___YuvOffset_5; }
	inline void set_YuvOffset_5(float value)
	{
		___YuvOffset_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEARINGSETTINGS_T1139179787_H
#ifndef UI2_T953001989_H
#define UI2_T953001989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI2
struct  UI2_t953001989  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UI2_T953001989_H
#ifndef PARTSANDFOLDERNAMES_T2905795907_H
#define PARTSANDFOLDERNAMES_T2905795907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Avataaars/PartsAndFolderNames
struct  PartsAndFolderNames_t2905795907  : public RuntimeObject
{
public:
	// UnityEngine.Transform Avataaars/PartsAndFolderNames::attachmentTransform
	Transform_t3600365921 * ___attachmentTransform_0;
	// System.String Avataaars/PartsAndFolderNames::generalClassifier
	String_t* ___generalClassifier_1;
	// System.String Avataaars/PartsAndFolderNames::subClassifier
	String_t* ___subClassifier_2;
	// System.String Avataaars/PartsAndFolderNames::folderName
	String_t* ___folderName_3;
	// UnityEngine.Color[] Avataaars/PartsAndFolderNames::colors
	ColorU5BU5D_t941916413* ___colors_4;

public:
	inline static int32_t get_offset_of_attachmentTransform_0() { return static_cast<int32_t>(offsetof(PartsAndFolderNames_t2905795907, ___attachmentTransform_0)); }
	inline Transform_t3600365921 * get_attachmentTransform_0() const { return ___attachmentTransform_0; }
	inline Transform_t3600365921 ** get_address_of_attachmentTransform_0() { return &___attachmentTransform_0; }
	inline void set_attachmentTransform_0(Transform_t3600365921 * value)
	{
		___attachmentTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentTransform_0), value);
	}

	inline static int32_t get_offset_of_generalClassifier_1() { return static_cast<int32_t>(offsetof(PartsAndFolderNames_t2905795907, ___generalClassifier_1)); }
	inline String_t* get_generalClassifier_1() const { return ___generalClassifier_1; }
	inline String_t** get_address_of_generalClassifier_1() { return &___generalClassifier_1; }
	inline void set_generalClassifier_1(String_t* value)
	{
		___generalClassifier_1 = value;
		Il2CppCodeGenWriteBarrier((&___generalClassifier_1), value);
	}

	inline static int32_t get_offset_of_subClassifier_2() { return static_cast<int32_t>(offsetof(PartsAndFolderNames_t2905795907, ___subClassifier_2)); }
	inline String_t* get_subClassifier_2() const { return ___subClassifier_2; }
	inline String_t** get_address_of_subClassifier_2() { return &___subClassifier_2; }
	inline void set_subClassifier_2(String_t* value)
	{
		___subClassifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___subClassifier_2), value);
	}

	inline static int32_t get_offset_of_folderName_3() { return static_cast<int32_t>(offsetof(PartsAndFolderNames_t2905795907, ___folderName_3)); }
	inline String_t* get_folderName_3() const { return ___folderName_3; }
	inline String_t** get_address_of_folderName_3() { return &___folderName_3; }
	inline void set_folderName_3(String_t* value)
	{
		___folderName_3 = value;
		Il2CppCodeGenWriteBarrier((&___folderName_3), value);
	}

	inline static int32_t get_offset_of_colors_4() { return static_cast<int32_t>(offsetof(PartsAndFolderNames_t2905795907, ___colors_4)); }
	inline ColorU5BU5D_t941916413* get_colors_4() const { return ___colors_4; }
	inline ColorU5BU5D_t941916413** get_address_of_colors_4() { return &___colors_4; }
	inline void set_colors_4(ColorU5BU5D_t941916413* value)
	{
		___colors_4 = value;
		Il2CppCodeGenWriteBarrier((&___colors_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTSANDFOLDERNAMES_T2905795907_H
#ifndef U3CCHECKIFALIVEU3EC__ITERATOR0_T2402465905_H
#define U3CCHECKIFALIVEU3EC__ITERATOR0_T2402465905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_AutoDestructShuriken/<CheckIfAlive>c__Iterator0
struct  U3CCheckIfAliveU3Ec__Iterator0_t2402465905  : public RuntimeObject
{
public:
	// CFX_AutoDestructShuriken CFX_AutoDestructShuriken/<CheckIfAlive>c__Iterator0::$this
	CFX_AutoDestructShuriken_t4057240314 * ___U24this_0;
	// System.Object CFX_AutoDestructShuriken/<CheckIfAlive>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean CFX_AutoDestructShuriken/<CheckIfAlive>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 CFX_AutoDestructShuriken/<CheckIfAlive>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCheckIfAliveU3Ec__Iterator0_t2402465905, ___U24this_0)); }
	inline CFX_AutoDestructShuriken_t4057240314 * get_U24this_0() const { return ___U24this_0; }
	inline CFX_AutoDestructShuriken_t4057240314 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(CFX_AutoDestructShuriken_t4057240314 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCheckIfAliveU3Ec__Iterator0_t2402465905, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCheckIfAliveU3Ec__Iterator0_t2402465905, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCheckIfAliveU3Ec__Iterator0_t2402465905, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKIFALIVEU3EC__ITERATOR0_T2402465905_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CACTUALLYLOADBLENDSHAPEU3EC__ITERATOR0_T3550601061_H
#define U3CACTUALLYLOADBLENDSHAPEU3EC__ITERATOR0_T3550601061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlendShapeReader/<ActuallyLoadBlendShape>c__Iterator0
struct  U3CActuallyLoadBlendShapeU3Ec__Iterator0_t3550601061  : public RuntimeObject
{
public:
	// System.String BlendShapeReader/<ActuallyLoadBlendShape>c__Iterator0::url
	String_t* ___url_0;
	// UnityEngine.WWW BlendShapeReader/<ActuallyLoadBlendShape>c__Iterator0::<w>__0
	WWW_t3688466362 * ___U3CwU3E__0_1;
	// BlendShapeReader BlendShapeReader/<ActuallyLoadBlendShape>c__Iterator0::$this
	BlendShapeReader_t1673803692 * ___U24this_2;
	// System.Object BlendShapeReader/<ActuallyLoadBlendShape>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean BlendShapeReader/<ActuallyLoadBlendShape>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 BlendShapeReader/<ActuallyLoadBlendShape>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CActuallyLoadBlendShapeU3Ec__Iterator0_t3550601061, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_U3CwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CActuallyLoadBlendShapeU3Ec__Iterator0_t3550601061, ___U3CwU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CwU3E__0_1() const { return ___U3CwU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CwU3E__0_1() { return &___U3CwU3E__0_1; }
	inline void set_U3CwU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CActuallyLoadBlendShapeU3Ec__Iterator0_t3550601061, ___U24this_2)); }
	inline BlendShapeReader_t1673803692 * get_U24this_2() const { return ___U24this_2; }
	inline BlendShapeReader_t1673803692 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(BlendShapeReader_t1673803692 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CActuallyLoadBlendShapeU3Ec__Iterator0_t3550601061, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CActuallyLoadBlendShapeU3Ec__Iterator0_t3550601061, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CActuallyLoadBlendShapeU3Ec__Iterator0_t3550601061, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CACTUALLYLOADBLENDSHAPEU3EC__ITERATOR0_T3550601061_H
#ifndef BLENDSHAPEHELPER_T3964592397_H
#define BLENDSHAPEHELPER_T3964592397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlendShapeHelper
struct  BlendShapeHelper_t3964592397  : public RuntimeObject
{
public:
	// System.Single BlendShapeHelper::lastval
	float ___lastval_0;

public:
	inline static int32_t get_offset_of_lastval_0() { return static_cast<int32_t>(offsetof(BlendShapeHelper_t3964592397, ___lastval_0)); }
	inline float get_lastval_0() const { return ___lastval_0; }
	inline float* get_address_of_lastval_0() { return &___lastval_0; }
	inline void set_lastval_0(float value)
	{
		___lastval_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDSHAPEHELPER_T3964592397_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef COLORMODE_T2454795589_H
#define COLORMODE_T2454795589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Noise/ColorMode
struct  ColorMode_t2454795589 
{
public:
	// System.Int32 Colorful.Noise/ColorMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorMode_t2454795589, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORMODE_T2454795589_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef PRESET_T400928058_H
#define PRESET_T400928058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.LoFiPalette/Preset
struct  Preset_t400928058 
{
public:
	// System.Int32 Colorful.LoFiPalette/Preset::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Preset_t400928058, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESET_T400928058_H
#ifndef SIZEMODE_T3498286277_H
#define SIZEMODE_T3498286277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Pixelate/SizeMode
struct  SizeMode_t3498286277 
{
public:
	// System.Int32 Colorful.Pixelate/SizeMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SizeMode_t3498286277, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIZEMODE_T3498286277_H
#ifndef ALGORITHM_T3898688573_H
#define ALGORITHM_T3898688573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Wiggle/Algorithm
struct  Algorithm_t3898688573 
{
public:
	// System.Int32 Colorful.Wiggle/Algorithm::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Algorithm_t3898688573, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALGORITHM_T3898688573_H
#ifndef BALANCEMODE_T686342893_H
#define BALANCEMODE_T686342893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.WhiteBalance/BalanceMode
struct  BalanceMode_t686342893 
{
public:
	// System.Int32 Colorful.WhiteBalance/BalanceMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BalanceMode_t686342893, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALANCEMODE_T686342893_H
#ifndef INSTRAGRAMFILTER_T685827072_H
#define INSTRAGRAMFILTER_T685827072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Vintage/InstragramFilter
struct  InstragramFilter_t685827072 
{
public:
	// System.Int32 Colorful.Vintage/InstragramFilter::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InstragramFilter_t685827072, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTRAGRAMFILTER_T685827072_H
#ifndef MATHF2_T1422403535_H
#define MATHF2_T1422403535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mathf2
struct  Mathf2_t1422403535  : public RuntimeObject
{
public:

public:
};

struct Mathf2_t1422403535_StaticFields
{
public:
	// UnityEngine.Camera Mathf2::cam
	Camera_t4157153871 * ___cam_0;
	// UnityEngine.Vector3 Mathf2::FarFarAway
	Vector3_t3722313464  ___FarFarAway_1;

public:
	inline static int32_t get_offset_of_cam_0() { return static_cast<int32_t>(offsetof(Mathf2_t1422403535_StaticFields, ___cam_0)); }
	inline Camera_t4157153871 * get_cam_0() const { return ___cam_0; }
	inline Camera_t4157153871 ** get_address_of_cam_0() { return &___cam_0; }
	inline void set_cam_0(Camera_t4157153871 * value)
	{
		___cam_0 = value;
		Il2CppCodeGenWriteBarrier((&___cam_0), value);
	}

	inline static int32_t get_offset_of_FarFarAway_1() { return static_cast<int32_t>(offsetof(Mathf2_t1422403535_StaticFields, ___FarFarAway_1)); }
	inline Vector3_t3722313464  get_FarFarAway_1() const { return ___FarFarAway_1; }
	inline Vector3_t3722313464 * get_address_of_FarFarAway_1() { return &___FarFarAway_1; }
	inline void set_FarFarAway_1(Vector3_t3722313464  value)
	{
		___FarFarAway_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHF2_T1422403535_H
#ifndef COLORMODE_T650858100_H
#define COLORMODE_T650858100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Strokes/ColorMode
struct  ColorMode_t650858100 
{
public:
	// System.Int32 Colorful.Strokes/ColorMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorMode_t650858100, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORMODE_T650858100_H
#ifndef ALGORITHM_T3262665680_H
#define ALGORITHM_T3262665680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Sharpen/Algorithm
struct  Algorithm_t3262665680 
{
public:
	// System.Int32 Colorful.Sharpen/Algorithm::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Algorithm_t3262665680, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALGORITHM_T3262665680_H
#ifndef COLORMODE_T2800918933_H
#define COLORMODE_T2800918933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.ShadowsMidtonesHighlights/ColorMode
struct  ColorMode_t2800918933 
{
public:
	// System.Int32 Colorful.ShadowsMidtonesHighlights/ColorMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorMode_t2800918933, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORMODE_T2800918933_H
#ifndef EMOTIONS_T827797779_H
#define EMOTIONS_T827797779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Avataaars/Emotions
struct  Emotions_t827797779 
{
public:
	// System.Int32 Avataaars/Emotions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Emotions_t827797779, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMOTIONS_T827797779_H
#ifndef ATTACHMENTPOINTS_T1814348004_H
#define ATTACHMENTPOINTS_T1814348004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Avataaars/AttachmentPoints
struct  AttachmentPoints_t1814348004 
{
public:
	// System.Int32 Avataaars/AttachmentPoints::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AttachmentPoints_t1814348004, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENTPOINTS_T1814348004_H
#ifndef QUALITYPRESET_T1040903510_H
#define QUALITYPRESET_T1040903510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.RadialBlur/QualityPreset
struct  QualityPreset_t1040903510 
{
public:
	// System.Int32 Colorful.RadialBlur/QualityPreset::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(QualityPreset_t1040903510, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUALITYPRESET_T1040903510_H
#ifndef TARGETTYPE_T3479356996_H
#define TARGETTYPE_T3479356996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.TargetType
struct  TargetType_t3479356996 
{
public:
	// System.Int32 DG.Tweening.Core.TargetType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TargetType_t3479356996, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TARGETTYPE_T3479356996_H
#ifndef EASE_T4010715394_H
#define EASE_T4010715394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Ease
struct  Ease_t4010715394 
{
public:
	// System.Int32 DG.Tweening.Ease::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Ease_t4010715394, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASE_T4010715394_H
#ifndef LOOPTYPE_T3049802818_H
#define LOOPTYPE_T3049802818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LoopType
struct  LoopType_t3049802818 
{
public:
	// System.Int32 DG.Tweening.LoopType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoopType_t3049802818, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T3049802818_H
#ifndef DOTWEENANIMATIONTYPE_T2748799855_H
#define DOTWEENANIMATIONTYPE_T2748799855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenAnimationType
struct  DOTweenAnimationType_t2748799855 
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenAnimationType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DOTweenAnimationType_t2748799855, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENANIMATIONTYPE_T2748799855_H
#ifndef GLITCHINGMODE_T3305168335_H
#define GLITCHINGMODE_T3305168335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Glitch/GlitchingMode
struct  GlitchingMode_t3305168335 
{
public:
	// System.Int32 Colorful.Glitch/GlitchingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GlitchingMode_t3305168335, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLITCHINGMODE_T3305168335_H
#ifndef COLORMODE_T803725769_H
#define COLORMODE_T803725769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Levels/ColorMode
struct  ColorMode_t803725769 
{
public:
	// System.Int32 Colorful.Levels/ColorMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorMode_t803725769, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORMODE_T803725769_H
#ifndef ROTATEMODE_T2548570174_H
#define ROTATEMODE_T2548570174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.RotateMode
struct  RotateMode_t2548570174 
{
public:
	// System.Int32 DG.Tweening.RotateMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RotateMode_t2548570174, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEMODE_T2548570174_H
#ifndef CHANNEL_T1767287641_H
#define CHANNEL_T1767287641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.ChannelSwapper/Channel
struct  Channel_t1767287641 
{
public:
	// System.Int32 Colorful.ChannelSwapper/Channel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Channel_t1767287641, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNEL_T1767287641_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef UPDATETYPE_T3937729206_H
#define UPDATETYPE_T3937729206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.UpdateType
struct  UpdateType_t3937729206 
{
public:
	// System.Int32 DG.Tweening.UpdateType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdateType_t3937729206, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATETYPE_T3937729206_H
#ifndef QUALITYPRESET_T119956193_H
#define QUALITYPRESET_T119956193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.DirectionalBlur/QualityPreset
struct  QualityPreset_t119956193 
{
public:
	// System.Int32 Colorful.DirectionalBlur/QualityPreset::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(QualityPreset_t119956193, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUALITYPRESET_T119956193_H
#ifndef SCRAMBLEMODE_T1285273342_H
#define SCRAMBLEMODE_T1285273342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ScrambleMode
struct  ScrambleMode_t1285273342 
{
public:
	// System.Int32 DG.Tweening.ScrambleMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScrambleMode_t1285273342, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRAMBLEMODE_T1285273342_H
#ifndef COLORMODE_T3494539450_H
#define COLORMODE_T3494539450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.FastVignette/ColorMode
struct  ColorMode_t3494539450 
{
public:
	// System.Int32 Colorful.FastVignette/ColorMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorMode_t3494539450, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORMODE_T3494539450_H
#ifndef SIZEMODE_T1428983497_H
#define SIZEMODE_T1428983497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Led/SizeMode
struct  SizeMode_t1428983497 
{
public:
	// System.Int32 Colorful.Led/SizeMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SizeMode_t1428983497, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIZEMODE_T1428983497_H
#ifndef QUALITYPRESET_T618172025_H
#define QUALITYPRESET_T618172025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.LensDistortionBlur/QualityPreset
struct  QualityPreset_t618172025 
{
public:
	// System.Int32 Colorful.LensDistortionBlur/QualityPreset::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(QualityPreset_t618172025, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUALITYPRESET_T618172025_H
#ifndef ATTACHMENTPOINTMAPPER_T2251653936_H
#define ATTACHMENTPOINTMAPPER_T2251653936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Avataaars/AttachmentPointMapper
struct  AttachmentPointMapper_t2251653936  : public RuntimeObject
{
public:
	// Avataaars/AttachmentPoints Avataaars/AttachmentPointMapper::attachmentPoints
	int32_t ___attachmentPoints_0;
	// UnityEngine.Transform Avataaars/AttachmentPointMapper::tParent
	Transform_t3600365921 * ___tParent_1;

public:
	inline static int32_t get_offset_of_attachmentPoints_0() { return static_cast<int32_t>(offsetof(AttachmentPointMapper_t2251653936, ___attachmentPoints_0)); }
	inline int32_t get_attachmentPoints_0() const { return ___attachmentPoints_0; }
	inline int32_t* get_address_of_attachmentPoints_0() { return &___attachmentPoints_0; }
	inline void set_attachmentPoints_0(int32_t value)
	{
		___attachmentPoints_0 = value;
	}

	inline static int32_t get_offset_of_tParent_1() { return static_cast<int32_t>(offsetof(AttachmentPointMapper_t2251653936, ___tParent_1)); }
	inline Transform_t3600365921 * get_tParent_1() const { return ___tParent_1; }
	inline Transform_t3600365921 ** get_address_of_tParent_1() { return &___tParent_1; }
	inline void set_tParent_1(Transform_t3600365921 * value)
	{
		___tParent_1 = value;
		Il2CppCodeGenWriteBarrier((&___tParent_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENTPOINTMAPPER_T2251653936_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef AVATAAARPART_T2415594242_H
#define AVATAAARPART_T2415594242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Avataaars/AvataaarPart
struct  AvataaarPart_t2415594242  : public RuntimeObject
{
public:
	// System.String Avataaars/AvataaarPart::name
	String_t* ___name_0;
	// UnityEngine.Sprite Avataaars/AvataaarPart::sprite
	Sprite_t280657092 * ___sprite_1;
	// UnityEngine.Vector3 Avataaars/AvataaarPart::anchorPos
	Vector3_t3722313464  ___anchorPos_2;
	// System.Boolean Avataaars/AvataaarPart::uniqueSprite
	bool ___uniqueSprite_3;
	// System.Single Avataaars/AvataaarPart::baseMultiplier
	float ___baseMultiplier_4;
	// System.Boolean Avataaars/AvataaarPart::emotionallyDependent
	bool ___emotionallyDependent_5;
	// Avataaars/Emotions Avataaars/AvataaarPart::emotions
	int32_t ___emotions_6;
	// Avataaars/AttachmentPoints Avataaars/AvataaarPart::attachmentPoint
	int32_t ___attachmentPoint_7;
	// System.Boolean Avataaars/AvataaarPart::greaterThan
	bool ___greaterThan_8;
	// System.Single Avataaars/AvataaarPart::threshhold
	float ___threshhold_9;
	// System.String Avataaars/AvataaarPart::appleKey
	String_t* ___appleKey_10;
	// System.Boolean Avataaars/AvataaarPart::useThreshhold
	bool ___useThreshhold_11;
	// UnityEngine.Vector2 Avataaars/AvataaarPart::yScaleMinMax
	Vector2_t2156229523  ___yScaleMinMax_12;
	// System.Int32 Avataaars/AvataaarPart::xMult
	int32_t ___xMult_13;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(AvataaarPart_t2415594242, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_sprite_1() { return static_cast<int32_t>(offsetof(AvataaarPart_t2415594242, ___sprite_1)); }
	inline Sprite_t280657092 * get_sprite_1() const { return ___sprite_1; }
	inline Sprite_t280657092 ** get_address_of_sprite_1() { return &___sprite_1; }
	inline void set_sprite_1(Sprite_t280657092 * value)
	{
		___sprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___sprite_1), value);
	}

	inline static int32_t get_offset_of_anchorPos_2() { return static_cast<int32_t>(offsetof(AvataaarPart_t2415594242, ___anchorPos_2)); }
	inline Vector3_t3722313464  get_anchorPos_2() const { return ___anchorPos_2; }
	inline Vector3_t3722313464 * get_address_of_anchorPos_2() { return &___anchorPos_2; }
	inline void set_anchorPos_2(Vector3_t3722313464  value)
	{
		___anchorPos_2 = value;
	}

	inline static int32_t get_offset_of_uniqueSprite_3() { return static_cast<int32_t>(offsetof(AvataaarPart_t2415594242, ___uniqueSprite_3)); }
	inline bool get_uniqueSprite_3() const { return ___uniqueSprite_3; }
	inline bool* get_address_of_uniqueSprite_3() { return &___uniqueSprite_3; }
	inline void set_uniqueSprite_3(bool value)
	{
		___uniqueSprite_3 = value;
	}

	inline static int32_t get_offset_of_baseMultiplier_4() { return static_cast<int32_t>(offsetof(AvataaarPart_t2415594242, ___baseMultiplier_4)); }
	inline float get_baseMultiplier_4() const { return ___baseMultiplier_4; }
	inline float* get_address_of_baseMultiplier_4() { return &___baseMultiplier_4; }
	inline void set_baseMultiplier_4(float value)
	{
		___baseMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_emotionallyDependent_5() { return static_cast<int32_t>(offsetof(AvataaarPart_t2415594242, ___emotionallyDependent_5)); }
	inline bool get_emotionallyDependent_5() const { return ___emotionallyDependent_5; }
	inline bool* get_address_of_emotionallyDependent_5() { return &___emotionallyDependent_5; }
	inline void set_emotionallyDependent_5(bool value)
	{
		___emotionallyDependent_5 = value;
	}

	inline static int32_t get_offset_of_emotions_6() { return static_cast<int32_t>(offsetof(AvataaarPart_t2415594242, ___emotions_6)); }
	inline int32_t get_emotions_6() const { return ___emotions_6; }
	inline int32_t* get_address_of_emotions_6() { return &___emotions_6; }
	inline void set_emotions_6(int32_t value)
	{
		___emotions_6 = value;
	}

	inline static int32_t get_offset_of_attachmentPoint_7() { return static_cast<int32_t>(offsetof(AvataaarPart_t2415594242, ___attachmentPoint_7)); }
	inline int32_t get_attachmentPoint_7() const { return ___attachmentPoint_7; }
	inline int32_t* get_address_of_attachmentPoint_7() { return &___attachmentPoint_7; }
	inline void set_attachmentPoint_7(int32_t value)
	{
		___attachmentPoint_7 = value;
	}

	inline static int32_t get_offset_of_greaterThan_8() { return static_cast<int32_t>(offsetof(AvataaarPart_t2415594242, ___greaterThan_8)); }
	inline bool get_greaterThan_8() const { return ___greaterThan_8; }
	inline bool* get_address_of_greaterThan_8() { return &___greaterThan_8; }
	inline void set_greaterThan_8(bool value)
	{
		___greaterThan_8 = value;
	}

	inline static int32_t get_offset_of_threshhold_9() { return static_cast<int32_t>(offsetof(AvataaarPart_t2415594242, ___threshhold_9)); }
	inline float get_threshhold_9() const { return ___threshhold_9; }
	inline float* get_address_of_threshhold_9() { return &___threshhold_9; }
	inline void set_threshhold_9(float value)
	{
		___threshhold_9 = value;
	}

	inline static int32_t get_offset_of_appleKey_10() { return static_cast<int32_t>(offsetof(AvataaarPart_t2415594242, ___appleKey_10)); }
	inline String_t* get_appleKey_10() const { return ___appleKey_10; }
	inline String_t** get_address_of_appleKey_10() { return &___appleKey_10; }
	inline void set_appleKey_10(String_t* value)
	{
		___appleKey_10 = value;
		Il2CppCodeGenWriteBarrier((&___appleKey_10), value);
	}

	inline static int32_t get_offset_of_useThreshhold_11() { return static_cast<int32_t>(offsetof(AvataaarPart_t2415594242, ___useThreshhold_11)); }
	inline bool get_useThreshhold_11() const { return ___useThreshhold_11; }
	inline bool* get_address_of_useThreshhold_11() { return &___useThreshhold_11; }
	inline void set_useThreshhold_11(bool value)
	{
		___useThreshhold_11 = value;
	}

	inline static int32_t get_offset_of_yScaleMinMax_12() { return static_cast<int32_t>(offsetof(AvataaarPart_t2415594242, ___yScaleMinMax_12)); }
	inline Vector2_t2156229523  get_yScaleMinMax_12() const { return ___yScaleMinMax_12; }
	inline Vector2_t2156229523 * get_address_of_yScaleMinMax_12() { return &___yScaleMinMax_12; }
	inline void set_yScaleMinMax_12(Vector2_t2156229523  value)
	{
		___yScaleMinMax_12 = value;
	}

	inline static int32_t get_offset_of_xMult_13() { return static_cast<int32_t>(offsetof(AvataaarPart_t2415594242, ___xMult_13)); }
	inline int32_t get_xMult_13() const { return ___xMult_13; }
	inline int32_t* get_address_of_xMult_13() { return &___xMult_13; }
	inline void set_xMult_13(int32_t value)
	{
		___xMult_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AVATAAARPART_T2415594242_H
#ifndef EMOTIONSDEFINABLE_T547898478_H
#define EMOTIONSDEFINABLE_T547898478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Avataaars/EmotionsDefinable
struct  EmotionsDefinable_t547898478  : public RuntimeObject
{
public:
	// System.String Avataaars/EmotionsDefinable::appleKey
	String_t* ___appleKey_0;
	// System.Single Avataaars/EmotionsDefinable::threshhold
	float ___threshhold_1;
	// Avataaars/Emotions Avataaars/EmotionsDefinable::emotion
	int32_t ___emotion_2;

public:
	inline static int32_t get_offset_of_appleKey_0() { return static_cast<int32_t>(offsetof(EmotionsDefinable_t547898478, ___appleKey_0)); }
	inline String_t* get_appleKey_0() const { return ___appleKey_0; }
	inline String_t** get_address_of_appleKey_0() { return &___appleKey_0; }
	inline void set_appleKey_0(String_t* value)
	{
		___appleKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___appleKey_0), value);
	}

	inline static int32_t get_offset_of_threshhold_1() { return static_cast<int32_t>(offsetof(EmotionsDefinable_t547898478, ___threshhold_1)); }
	inline float get_threshhold_1() const { return ___threshhold_1; }
	inline float* get_address_of_threshhold_1() { return &___threshhold_1; }
	inline void set_threshhold_1(float value)
	{
		___threshhold_1 = value;
	}

	inline static int32_t get_offset_of_emotion_2() { return static_cast<int32_t>(offsetof(EmotionsDefinable_t547898478, ___emotion_2)); }
	inline int32_t get_emotion_2() const { return ___emotion_2; }
	inline int32_t* get_address_of_emotion_2() { return &___emotion_2; }
	inline void set_emotion_2(int32_t value)
	{
		___emotion_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMOTIONSDEFINABLE_T547898478_H
#ifndef STRINGPROCESSOR_T3163369541_H
#define STRINGPROCESSOR_T3163369541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIInputWait/StringProcessor
struct  StringProcessor_t3163369541  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGPROCESSOR_T3163369541_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef PROCESSEACHBLENDSHAPEUPDATEBASIC_T1021678196_H
#define PROCESSEACHBLENDSHAPEUPDATEBASIC_T1021678196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlendShapeReader/ProcessEachBlendShapeUpdateBasic
struct  ProcessEachBlendShapeUpdateBasic_t1021678196  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROCESSEACHBLENDSHAPEUPDATEBASIC_T1021678196_H
#ifndef PROCESSEACHBLENDSHAPEUPDATE_T611652369_H
#define PROCESSEACHBLENDSHAPEUPDATE_T611652369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlendShapeReader/ProcessEachBlendShapeUpdate
struct  ProcessEachBlendShapeUpdate_t611652369  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROCESSEACHBLENDSHAPEUPDATE_T611652369_H
#ifndef PROCESSEACHBLENDSHAPEUPDATE_T1319766324_H
#define PROCESSEACHBLENDSHAPEUPDATE_T1319766324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuppeteerByFace/ProcessEachBlendShapeUpdate
struct  ProcessEachBlendShapeUpdate_t1319766324  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROCESSEACHBLENDSHAPEUPDATE_T1319766324_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef BASEEFFECT_T1187847871_H
#define BASEEFFECT_T1187847871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.BaseEffect
struct  BaseEffect_t1187847871  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Shader Colorful.BaseEffect::Shader
	Shader_t4151988712 * ___Shader_2;
	// UnityEngine.Material Colorful.BaseEffect::m_Material
	Material_t340375123 * ___m_Material_3;

public:
	inline static int32_t get_offset_of_Shader_2() { return static_cast<int32_t>(offsetof(BaseEffect_t1187847871, ___Shader_2)); }
	inline Shader_t4151988712 * get_Shader_2() const { return ___Shader_2; }
	inline Shader_t4151988712 ** get_address_of_Shader_2() { return &___Shader_2; }
	inline void set_Shader_2(Shader_t4151988712 * value)
	{
		___Shader_2 = value;
		Il2CppCodeGenWriteBarrier((&___Shader_2), value);
	}

	inline static int32_t get_offset_of_m_Material_3() { return static_cast<int32_t>(offsetof(BaseEffect_t1187847871, ___m_Material_3)); }
	inline Material_t340375123 * get_m_Material_3() const { return ___m_Material_3; }
	inline Material_t340375123 ** get_address_of_m_Material_3() { return &___m_Material_3; }
	inline void set_m_Material_3(Material_t340375123 * value)
	{
		___m_Material_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEEFFECT_T1187847871_H
#ifndef CFX_AUTODESTRUCTWHENNOCHILDREN_T1135562420_H
#define CFX_AUTODESTRUCTWHENNOCHILDREN_T1135562420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_AutodestructWhenNoChildren
struct  CFX_AutodestructWhenNoChildren_t1135562420  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFX_AUTODESTRUCTWHENNOCHILDREN_T1135562420_H
#ifndef BLENDSHAPEREADER_T1673803692_H
#define BLENDSHAPEREADER_T1673803692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BlendShapeReader
struct  BlendShapeReader_t1673803692  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Quaternion> BlendShapeReader::camRot
	List_1_t3774003073 * ___camRot_6;
	// System.Boolean BlendShapeReader::playing
	bool ___playing_7;
	// System.Single BlendShapeReader::time
	float ___time_8;
	// System.Single BlendShapeReader::nextTime
	float ___nextTime_9;
	// System.Int32 BlendShapeReader::current
	int32_t ___current_10;
	// System.String BlendShapeReader::teststringurl
	String_t* ___teststringurl_11;
	// System.Boolean BlendShapeReader::useDelegate
	bool ___useDelegate_12;
	// UIInputWait BlendShapeReader::inputwait
	UIInputWait_t3530943742 * ___inputwait_15;
	// UnityEngine.Transform BlendShapeReader::tHead
	Transform_t3600365921 * ___tHead_16;

public:
	inline static int32_t get_offset_of_camRot_6() { return static_cast<int32_t>(offsetof(BlendShapeReader_t1673803692, ___camRot_6)); }
	inline List_1_t3774003073 * get_camRot_6() const { return ___camRot_6; }
	inline List_1_t3774003073 ** get_address_of_camRot_6() { return &___camRot_6; }
	inline void set_camRot_6(List_1_t3774003073 * value)
	{
		___camRot_6 = value;
		Il2CppCodeGenWriteBarrier((&___camRot_6), value);
	}

	inline static int32_t get_offset_of_playing_7() { return static_cast<int32_t>(offsetof(BlendShapeReader_t1673803692, ___playing_7)); }
	inline bool get_playing_7() const { return ___playing_7; }
	inline bool* get_address_of_playing_7() { return &___playing_7; }
	inline void set_playing_7(bool value)
	{
		___playing_7 = value;
	}

	inline static int32_t get_offset_of_time_8() { return static_cast<int32_t>(offsetof(BlendShapeReader_t1673803692, ___time_8)); }
	inline float get_time_8() const { return ___time_8; }
	inline float* get_address_of_time_8() { return &___time_8; }
	inline void set_time_8(float value)
	{
		___time_8 = value;
	}

	inline static int32_t get_offset_of_nextTime_9() { return static_cast<int32_t>(offsetof(BlendShapeReader_t1673803692, ___nextTime_9)); }
	inline float get_nextTime_9() const { return ___nextTime_9; }
	inline float* get_address_of_nextTime_9() { return &___nextTime_9; }
	inline void set_nextTime_9(float value)
	{
		___nextTime_9 = value;
	}

	inline static int32_t get_offset_of_current_10() { return static_cast<int32_t>(offsetof(BlendShapeReader_t1673803692, ___current_10)); }
	inline int32_t get_current_10() const { return ___current_10; }
	inline int32_t* get_address_of_current_10() { return &___current_10; }
	inline void set_current_10(int32_t value)
	{
		___current_10 = value;
	}

	inline static int32_t get_offset_of_teststringurl_11() { return static_cast<int32_t>(offsetof(BlendShapeReader_t1673803692, ___teststringurl_11)); }
	inline String_t* get_teststringurl_11() const { return ___teststringurl_11; }
	inline String_t** get_address_of_teststringurl_11() { return &___teststringurl_11; }
	inline void set_teststringurl_11(String_t* value)
	{
		___teststringurl_11 = value;
		Il2CppCodeGenWriteBarrier((&___teststringurl_11), value);
	}

	inline static int32_t get_offset_of_useDelegate_12() { return static_cast<int32_t>(offsetof(BlendShapeReader_t1673803692, ___useDelegate_12)); }
	inline bool get_useDelegate_12() const { return ___useDelegate_12; }
	inline bool* get_address_of_useDelegate_12() { return &___useDelegate_12; }
	inline void set_useDelegate_12(bool value)
	{
		___useDelegate_12 = value;
	}

	inline static int32_t get_offset_of_inputwait_15() { return static_cast<int32_t>(offsetof(BlendShapeReader_t1673803692, ___inputwait_15)); }
	inline UIInputWait_t3530943742 * get_inputwait_15() const { return ___inputwait_15; }
	inline UIInputWait_t3530943742 ** get_address_of_inputwait_15() { return &___inputwait_15; }
	inline void set_inputwait_15(UIInputWait_t3530943742 * value)
	{
		___inputwait_15 = value;
		Il2CppCodeGenWriteBarrier((&___inputwait_15), value);
	}

	inline static int32_t get_offset_of_tHead_16() { return static_cast<int32_t>(offsetof(BlendShapeReader_t1673803692, ___tHead_16)); }
	inline Transform_t3600365921 * get_tHead_16() const { return ___tHead_16; }
	inline Transform_t3600365921 ** get_address_of_tHead_16() { return &___tHead_16; }
	inline void set_tHead_16(Transform_t3600365921 * value)
	{
		___tHead_16 = value;
		Il2CppCodeGenWriteBarrier((&___tHead_16), value);
	}
};

struct BlendShapeReader_t1673803692_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Single>> BlendShapeReader::listBlendShapes
	List_1_t2654597815 * ___listBlendShapes_2;
	// System.Collections.Generic.List`1<System.Single> BlendShapeReader::listTimes
	List_1_t2869341516 * ___listTimes_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> BlendShapeReader::listPos
	List_1_t899420910 * ___listPos_4;
	// System.Collections.Generic.List`1<UnityEngine.Quaternion> BlendShapeReader::listRot
	List_1_t3774003073 * ___listRot_5;
	// BlendShapeReader/ProcessEachBlendShapeUpdate BlendShapeReader::SubscribeEachBlendShapeUpdate
	ProcessEachBlendShapeUpdate_t611652369 * ___SubscribeEachBlendShapeUpdate_13;
	// BlendShapeReader/ProcessEachBlendShapeUpdateBasic BlendShapeReader::SubscribeEachBlendShapeUpdateBasic
	ProcessEachBlendShapeUpdateBasic_t1021678196 * ___SubscribeEachBlendShapeUpdateBasic_14;

public:
	inline static int32_t get_offset_of_listBlendShapes_2() { return static_cast<int32_t>(offsetof(BlendShapeReader_t1673803692_StaticFields, ___listBlendShapes_2)); }
	inline List_1_t2654597815 * get_listBlendShapes_2() const { return ___listBlendShapes_2; }
	inline List_1_t2654597815 ** get_address_of_listBlendShapes_2() { return &___listBlendShapes_2; }
	inline void set_listBlendShapes_2(List_1_t2654597815 * value)
	{
		___listBlendShapes_2 = value;
		Il2CppCodeGenWriteBarrier((&___listBlendShapes_2), value);
	}

	inline static int32_t get_offset_of_listTimes_3() { return static_cast<int32_t>(offsetof(BlendShapeReader_t1673803692_StaticFields, ___listTimes_3)); }
	inline List_1_t2869341516 * get_listTimes_3() const { return ___listTimes_3; }
	inline List_1_t2869341516 ** get_address_of_listTimes_3() { return &___listTimes_3; }
	inline void set_listTimes_3(List_1_t2869341516 * value)
	{
		___listTimes_3 = value;
		Il2CppCodeGenWriteBarrier((&___listTimes_3), value);
	}

	inline static int32_t get_offset_of_listPos_4() { return static_cast<int32_t>(offsetof(BlendShapeReader_t1673803692_StaticFields, ___listPos_4)); }
	inline List_1_t899420910 * get_listPos_4() const { return ___listPos_4; }
	inline List_1_t899420910 ** get_address_of_listPos_4() { return &___listPos_4; }
	inline void set_listPos_4(List_1_t899420910 * value)
	{
		___listPos_4 = value;
		Il2CppCodeGenWriteBarrier((&___listPos_4), value);
	}

	inline static int32_t get_offset_of_listRot_5() { return static_cast<int32_t>(offsetof(BlendShapeReader_t1673803692_StaticFields, ___listRot_5)); }
	inline List_1_t3774003073 * get_listRot_5() const { return ___listRot_5; }
	inline List_1_t3774003073 ** get_address_of_listRot_5() { return &___listRot_5; }
	inline void set_listRot_5(List_1_t3774003073 * value)
	{
		___listRot_5 = value;
		Il2CppCodeGenWriteBarrier((&___listRot_5), value);
	}

	inline static int32_t get_offset_of_SubscribeEachBlendShapeUpdate_13() { return static_cast<int32_t>(offsetof(BlendShapeReader_t1673803692_StaticFields, ___SubscribeEachBlendShapeUpdate_13)); }
	inline ProcessEachBlendShapeUpdate_t611652369 * get_SubscribeEachBlendShapeUpdate_13() const { return ___SubscribeEachBlendShapeUpdate_13; }
	inline ProcessEachBlendShapeUpdate_t611652369 ** get_address_of_SubscribeEachBlendShapeUpdate_13() { return &___SubscribeEachBlendShapeUpdate_13; }
	inline void set_SubscribeEachBlendShapeUpdate_13(ProcessEachBlendShapeUpdate_t611652369 * value)
	{
		___SubscribeEachBlendShapeUpdate_13 = value;
		Il2CppCodeGenWriteBarrier((&___SubscribeEachBlendShapeUpdate_13), value);
	}

	inline static int32_t get_offset_of_SubscribeEachBlendShapeUpdateBasic_14() { return static_cast<int32_t>(offsetof(BlendShapeReader_t1673803692_StaticFields, ___SubscribeEachBlendShapeUpdateBasic_14)); }
	inline ProcessEachBlendShapeUpdateBasic_t1021678196 * get_SubscribeEachBlendShapeUpdateBasic_14() const { return ___SubscribeEachBlendShapeUpdateBasic_14; }
	inline ProcessEachBlendShapeUpdateBasic_t1021678196 ** get_address_of_SubscribeEachBlendShapeUpdateBasic_14() { return &___SubscribeEachBlendShapeUpdateBasic_14; }
	inline void set_SubscribeEachBlendShapeUpdateBasic_14(ProcessEachBlendShapeUpdateBasic_t1021678196 * value)
	{
		___SubscribeEachBlendShapeUpdateBasic_14 = value;
		Il2CppCodeGenWriteBarrier((&___SubscribeEachBlendShapeUpdateBasic_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDSHAPEREADER_T1673803692_H
#ifndef ABSANIMATIONCOMPONENT_T262169234_H
#define ABSANIMATIONCOMPONENT_T262169234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.ABSAnimationComponent
struct  ABSAnimationComponent_t262169234  : public MonoBehaviour_t3962482529
{
public:
	// DG.Tweening.UpdateType DG.Tweening.Core.ABSAnimationComponent::updateType
	int32_t ___updateType_2;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::isSpeedBased
	bool ___isSpeedBased_3;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnStart
	bool ___hasOnStart_4;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnPlay
	bool ___hasOnPlay_5;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnUpdate
	bool ___hasOnUpdate_6;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnStepComplete
	bool ___hasOnStepComplete_7;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnComplete
	bool ___hasOnComplete_8;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnTweenCreated
	bool ___hasOnTweenCreated_9;
	// System.Boolean DG.Tweening.Core.ABSAnimationComponent::hasOnRewind
	bool ___hasOnRewind_10;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onStart
	UnityEvent_t2581268647 * ___onStart_11;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onPlay
	UnityEvent_t2581268647 * ___onPlay_12;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onUpdate
	UnityEvent_t2581268647 * ___onUpdate_13;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onStepComplete
	UnityEvent_t2581268647 * ___onStepComplete_14;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onComplete
	UnityEvent_t2581268647 * ___onComplete_15;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onTweenCreated
	UnityEvent_t2581268647 * ___onTweenCreated_16;
	// UnityEngine.Events.UnityEvent DG.Tweening.Core.ABSAnimationComponent::onRewind
	UnityEvent_t2581268647 * ___onRewind_17;
	// DG.Tweening.Tween DG.Tweening.Core.ABSAnimationComponent::tween
	Tween_t2342918553 * ___tween_18;

public:
	inline static int32_t get_offset_of_updateType_2() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___updateType_2)); }
	inline int32_t get_updateType_2() const { return ___updateType_2; }
	inline int32_t* get_address_of_updateType_2() { return &___updateType_2; }
	inline void set_updateType_2(int32_t value)
	{
		___updateType_2 = value;
	}

	inline static int32_t get_offset_of_isSpeedBased_3() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___isSpeedBased_3)); }
	inline bool get_isSpeedBased_3() const { return ___isSpeedBased_3; }
	inline bool* get_address_of_isSpeedBased_3() { return &___isSpeedBased_3; }
	inline void set_isSpeedBased_3(bool value)
	{
		___isSpeedBased_3 = value;
	}

	inline static int32_t get_offset_of_hasOnStart_4() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnStart_4)); }
	inline bool get_hasOnStart_4() const { return ___hasOnStart_4; }
	inline bool* get_address_of_hasOnStart_4() { return &___hasOnStart_4; }
	inline void set_hasOnStart_4(bool value)
	{
		___hasOnStart_4 = value;
	}

	inline static int32_t get_offset_of_hasOnPlay_5() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnPlay_5)); }
	inline bool get_hasOnPlay_5() const { return ___hasOnPlay_5; }
	inline bool* get_address_of_hasOnPlay_5() { return &___hasOnPlay_5; }
	inline void set_hasOnPlay_5(bool value)
	{
		___hasOnPlay_5 = value;
	}

	inline static int32_t get_offset_of_hasOnUpdate_6() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnUpdate_6)); }
	inline bool get_hasOnUpdate_6() const { return ___hasOnUpdate_6; }
	inline bool* get_address_of_hasOnUpdate_6() { return &___hasOnUpdate_6; }
	inline void set_hasOnUpdate_6(bool value)
	{
		___hasOnUpdate_6 = value;
	}

	inline static int32_t get_offset_of_hasOnStepComplete_7() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnStepComplete_7)); }
	inline bool get_hasOnStepComplete_7() const { return ___hasOnStepComplete_7; }
	inline bool* get_address_of_hasOnStepComplete_7() { return &___hasOnStepComplete_7; }
	inline void set_hasOnStepComplete_7(bool value)
	{
		___hasOnStepComplete_7 = value;
	}

	inline static int32_t get_offset_of_hasOnComplete_8() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnComplete_8)); }
	inline bool get_hasOnComplete_8() const { return ___hasOnComplete_8; }
	inline bool* get_address_of_hasOnComplete_8() { return &___hasOnComplete_8; }
	inline void set_hasOnComplete_8(bool value)
	{
		___hasOnComplete_8 = value;
	}

	inline static int32_t get_offset_of_hasOnTweenCreated_9() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnTweenCreated_9)); }
	inline bool get_hasOnTweenCreated_9() const { return ___hasOnTweenCreated_9; }
	inline bool* get_address_of_hasOnTweenCreated_9() { return &___hasOnTweenCreated_9; }
	inline void set_hasOnTweenCreated_9(bool value)
	{
		___hasOnTweenCreated_9 = value;
	}

	inline static int32_t get_offset_of_hasOnRewind_10() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___hasOnRewind_10)); }
	inline bool get_hasOnRewind_10() const { return ___hasOnRewind_10; }
	inline bool* get_address_of_hasOnRewind_10() { return &___hasOnRewind_10; }
	inline void set_hasOnRewind_10(bool value)
	{
		___hasOnRewind_10 = value;
	}

	inline static int32_t get_offset_of_onStart_11() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onStart_11)); }
	inline UnityEvent_t2581268647 * get_onStart_11() const { return ___onStart_11; }
	inline UnityEvent_t2581268647 ** get_address_of_onStart_11() { return &___onStart_11; }
	inline void set_onStart_11(UnityEvent_t2581268647 * value)
	{
		___onStart_11 = value;
		Il2CppCodeGenWriteBarrier((&___onStart_11), value);
	}

	inline static int32_t get_offset_of_onPlay_12() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onPlay_12)); }
	inline UnityEvent_t2581268647 * get_onPlay_12() const { return ___onPlay_12; }
	inline UnityEvent_t2581268647 ** get_address_of_onPlay_12() { return &___onPlay_12; }
	inline void set_onPlay_12(UnityEvent_t2581268647 * value)
	{
		___onPlay_12 = value;
		Il2CppCodeGenWriteBarrier((&___onPlay_12), value);
	}

	inline static int32_t get_offset_of_onUpdate_13() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onUpdate_13)); }
	inline UnityEvent_t2581268647 * get_onUpdate_13() const { return ___onUpdate_13; }
	inline UnityEvent_t2581268647 ** get_address_of_onUpdate_13() { return &___onUpdate_13; }
	inline void set_onUpdate_13(UnityEvent_t2581268647 * value)
	{
		___onUpdate_13 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdate_13), value);
	}

	inline static int32_t get_offset_of_onStepComplete_14() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onStepComplete_14)); }
	inline UnityEvent_t2581268647 * get_onStepComplete_14() const { return ___onStepComplete_14; }
	inline UnityEvent_t2581268647 ** get_address_of_onStepComplete_14() { return &___onStepComplete_14; }
	inline void set_onStepComplete_14(UnityEvent_t2581268647 * value)
	{
		___onStepComplete_14 = value;
		Il2CppCodeGenWriteBarrier((&___onStepComplete_14), value);
	}

	inline static int32_t get_offset_of_onComplete_15() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onComplete_15)); }
	inline UnityEvent_t2581268647 * get_onComplete_15() const { return ___onComplete_15; }
	inline UnityEvent_t2581268647 ** get_address_of_onComplete_15() { return &___onComplete_15; }
	inline void set_onComplete_15(UnityEvent_t2581268647 * value)
	{
		___onComplete_15 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_15), value);
	}

	inline static int32_t get_offset_of_onTweenCreated_16() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onTweenCreated_16)); }
	inline UnityEvent_t2581268647 * get_onTweenCreated_16() const { return ___onTweenCreated_16; }
	inline UnityEvent_t2581268647 ** get_address_of_onTweenCreated_16() { return &___onTweenCreated_16; }
	inline void set_onTweenCreated_16(UnityEvent_t2581268647 * value)
	{
		___onTweenCreated_16 = value;
		Il2CppCodeGenWriteBarrier((&___onTweenCreated_16), value);
	}

	inline static int32_t get_offset_of_onRewind_17() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___onRewind_17)); }
	inline UnityEvent_t2581268647 * get_onRewind_17() const { return ___onRewind_17; }
	inline UnityEvent_t2581268647 ** get_address_of_onRewind_17() { return &___onRewind_17; }
	inline void set_onRewind_17(UnityEvent_t2581268647 * value)
	{
		___onRewind_17 = value;
		Il2CppCodeGenWriteBarrier((&___onRewind_17), value);
	}

	inline static int32_t get_offset_of_tween_18() { return static_cast<int32_t>(offsetof(ABSAnimationComponent_t262169234, ___tween_18)); }
	inline Tween_t2342918553 * get_tween_18() const { return ___tween_18; }
	inline Tween_t2342918553 ** get_address_of_tween_18() { return &___tween_18; }
	inline void set_tween_18(Tween_t2342918553 * value)
	{
		___tween_18 = value;
		Il2CppCodeGenWriteBarrier((&___tween_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSANIMATIONCOMPONENT_T262169234_H
#ifndef FACECUSTOMIZER_T3426199831_H
#define FACECUSTOMIZER_T3426199831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FaceCustomizer
struct  FaceCustomizer_t3426199831  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject FaceCustomizer::goUI_Customizable
	GameObject_t1113636619 * ___goUI_Customizable_2;
	// UnityEngine.Transform FaceCustomizer::tSpriteButton_PartContainer
	Transform_t3600365921 * ___tSpriteButton_PartContainer_3;
	// UnityEngine.GameObject FaceCustomizer::goPrefabButton
	GameObject_t1113636619 * ___goPrefabButton_4;
	// System.Single FaceCustomizer::buttonWidth
	float ___buttonWidth_5;
	// UnityEngine.Transform FaceCustomizer::tStartLoad
	Transform_t3600365921 * ___tStartLoad_7;
	// UnityEngine.Transform FaceCustomizer::tSpriteButton_ColorContainer
	Transform_t3600365921 * ___tSpriteButton_ColorContainer_8;
	// UnityEngine.GameObject FaceCustomizer::goPrefabButtonColor
	GameObject_t1113636619 * ___goPrefabButtonColor_9;

public:
	inline static int32_t get_offset_of_goUI_Customizable_2() { return static_cast<int32_t>(offsetof(FaceCustomizer_t3426199831, ___goUI_Customizable_2)); }
	inline GameObject_t1113636619 * get_goUI_Customizable_2() const { return ___goUI_Customizable_2; }
	inline GameObject_t1113636619 ** get_address_of_goUI_Customizable_2() { return &___goUI_Customizable_2; }
	inline void set_goUI_Customizable_2(GameObject_t1113636619 * value)
	{
		___goUI_Customizable_2 = value;
		Il2CppCodeGenWriteBarrier((&___goUI_Customizable_2), value);
	}

	inline static int32_t get_offset_of_tSpriteButton_PartContainer_3() { return static_cast<int32_t>(offsetof(FaceCustomizer_t3426199831, ___tSpriteButton_PartContainer_3)); }
	inline Transform_t3600365921 * get_tSpriteButton_PartContainer_3() const { return ___tSpriteButton_PartContainer_3; }
	inline Transform_t3600365921 ** get_address_of_tSpriteButton_PartContainer_3() { return &___tSpriteButton_PartContainer_3; }
	inline void set_tSpriteButton_PartContainer_3(Transform_t3600365921 * value)
	{
		___tSpriteButton_PartContainer_3 = value;
		Il2CppCodeGenWriteBarrier((&___tSpriteButton_PartContainer_3), value);
	}

	inline static int32_t get_offset_of_goPrefabButton_4() { return static_cast<int32_t>(offsetof(FaceCustomizer_t3426199831, ___goPrefabButton_4)); }
	inline GameObject_t1113636619 * get_goPrefabButton_4() const { return ___goPrefabButton_4; }
	inline GameObject_t1113636619 ** get_address_of_goPrefabButton_4() { return &___goPrefabButton_4; }
	inline void set_goPrefabButton_4(GameObject_t1113636619 * value)
	{
		___goPrefabButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___goPrefabButton_4), value);
	}

	inline static int32_t get_offset_of_buttonWidth_5() { return static_cast<int32_t>(offsetof(FaceCustomizer_t3426199831, ___buttonWidth_5)); }
	inline float get_buttonWidth_5() const { return ___buttonWidth_5; }
	inline float* get_address_of_buttonWidth_5() { return &___buttonWidth_5; }
	inline void set_buttonWidth_5(float value)
	{
		___buttonWidth_5 = value;
	}

	inline static int32_t get_offset_of_tStartLoad_7() { return static_cast<int32_t>(offsetof(FaceCustomizer_t3426199831, ___tStartLoad_7)); }
	inline Transform_t3600365921 * get_tStartLoad_7() const { return ___tStartLoad_7; }
	inline Transform_t3600365921 ** get_address_of_tStartLoad_7() { return &___tStartLoad_7; }
	inline void set_tStartLoad_7(Transform_t3600365921 * value)
	{
		___tStartLoad_7 = value;
		Il2CppCodeGenWriteBarrier((&___tStartLoad_7), value);
	}

	inline static int32_t get_offset_of_tSpriteButton_ColorContainer_8() { return static_cast<int32_t>(offsetof(FaceCustomizer_t3426199831, ___tSpriteButton_ColorContainer_8)); }
	inline Transform_t3600365921 * get_tSpriteButton_ColorContainer_8() const { return ___tSpriteButton_ColorContainer_8; }
	inline Transform_t3600365921 ** get_address_of_tSpriteButton_ColorContainer_8() { return &___tSpriteButton_ColorContainer_8; }
	inline void set_tSpriteButton_ColorContainer_8(Transform_t3600365921 * value)
	{
		___tSpriteButton_ColorContainer_8 = value;
		Il2CppCodeGenWriteBarrier((&___tSpriteButton_ColorContainer_8), value);
	}

	inline static int32_t get_offset_of_goPrefabButtonColor_9() { return static_cast<int32_t>(offsetof(FaceCustomizer_t3426199831, ___goPrefabButtonColor_9)); }
	inline GameObject_t1113636619 * get_goPrefabButtonColor_9() const { return ___goPrefabButtonColor_9; }
	inline GameObject_t1113636619 ** get_address_of_goPrefabButtonColor_9() { return &___goPrefabButtonColor_9; }
	inline void set_goPrefabButtonColor_9(GameObject_t1113636619 * value)
	{
		___goPrefabButtonColor_9 = value;
		Il2CppCodeGenWriteBarrier((&___goPrefabButtonColor_9), value);
	}
};

struct FaceCustomizer_t3426199831_StaticFields
{
public:
	// UnityEngine.Transform FaceCustomizer::currentlyEditing
	Transform_t3600365921 * ___currentlyEditing_6;

public:
	inline static int32_t get_offset_of_currentlyEditing_6() { return static_cast<int32_t>(offsetof(FaceCustomizer_t3426199831_StaticFields, ___currentlyEditing_6)); }
	inline Transform_t3600365921 * get_currentlyEditing_6() const { return ___currentlyEditing_6; }
	inline Transform_t3600365921 ** get_address_of_currentlyEditing_6() { return &___currentlyEditing_6; }
	inline void set_currentlyEditing_6(Transform_t3600365921 * value)
	{
		___currentlyEditing_6 = value;
		Il2CppCodeGenWriteBarrier((&___currentlyEditing_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACECUSTOMIZER_T3426199831_H
#ifndef AVATAAARS_T109144723_H
#define AVATAAARS_T109144723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Avataaars
struct  Avataaars_t109144723  : public MonoBehaviour_t3962482529
{
public:
	// System.String Avataaars::folderName
	String_t* ___folderName_2;
	// Avataaars/PartsAndFolderNames[] Avataaars::partsAndFolderNames
	PartsAndFolderNamesU5BU5D_t1814257234* ___partsAndFolderNames_3;
	// Avataaars/AttachmentPointMapper[] Avataaars::attachmentPointObjs
	AttachmentPointMapperU5BU5D_t162241041* ___attachmentPointObjs_4;
	// Avataaars/AvataaarPart[] Avataaars::crucialParts
	AvataaarPartU5BU5D_t11382455* ___crucialParts_5;
	// Avataaars/EmotionsDefinable[] Avataaars::emotionsDefinable
	EmotionsDefinableU5BU5D_t1185687067* ___emotionsDefinable_6;
	// Avataaars/Emotions Avataaars::currentEmotion
	int32_t ___currentEmotion_12;
	// UnityEngine.Transform Avataaars::tHead
	Transform_t3600365921 * ___tHead_13;

public:
	inline static int32_t get_offset_of_folderName_2() { return static_cast<int32_t>(offsetof(Avataaars_t109144723, ___folderName_2)); }
	inline String_t* get_folderName_2() const { return ___folderName_2; }
	inline String_t** get_address_of_folderName_2() { return &___folderName_2; }
	inline void set_folderName_2(String_t* value)
	{
		___folderName_2 = value;
		Il2CppCodeGenWriteBarrier((&___folderName_2), value);
	}

	inline static int32_t get_offset_of_partsAndFolderNames_3() { return static_cast<int32_t>(offsetof(Avataaars_t109144723, ___partsAndFolderNames_3)); }
	inline PartsAndFolderNamesU5BU5D_t1814257234* get_partsAndFolderNames_3() const { return ___partsAndFolderNames_3; }
	inline PartsAndFolderNamesU5BU5D_t1814257234** get_address_of_partsAndFolderNames_3() { return &___partsAndFolderNames_3; }
	inline void set_partsAndFolderNames_3(PartsAndFolderNamesU5BU5D_t1814257234* value)
	{
		___partsAndFolderNames_3 = value;
		Il2CppCodeGenWriteBarrier((&___partsAndFolderNames_3), value);
	}

	inline static int32_t get_offset_of_attachmentPointObjs_4() { return static_cast<int32_t>(offsetof(Avataaars_t109144723, ___attachmentPointObjs_4)); }
	inline AttachmentPointMapperU5BU5D_t162241041* get_attachmentPointObjs_4() const { return ___attachmentPointObjs_4; }
	inline AttachmentPointMapperU5BU5D_t162241041** get_address_of_attachmentPointObjs_4() { return &___attachmentPointObjs_4; }
	inline void set_attachmentPointObjs_4(AttachmentPointMapperU5BU5D_t162241041* value)
	{
		___attachmentPointObjs_4 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentPointObjs_4), value);
	}

	inline static int32_t get_offset_of_crucialParts_5() { return static_cast<int32_t>(offsetof(Avataaars_t109144723, ___crucialParts_5)); }
	inline AvataaarPartU5BU5D_t11382455* get_crucialParts_5() const { return ___crucialParts_5; }
	inline AvataaarPartU5BU5D_t11382455** get_address_of_crucialParts_5() { return &___crucialParts_5; }
	inline void set_crucialParts_5(AvataaarPartU5BU5D_t11382455* value)
	{
		___crucialParts_5 = value;
		Il2CppCodeGenWriteBarrier((&___crucialParts_5), value);
	}

	inline static int32_t get_offset_of_emotionsDefinable_6() { return static_cast<int32_t>(offsetof(Avataaars_t109144723, ___emotionsDefinable_6)); }
	inline EmotionsDefinableU5BU5D_t1185687067* get_emotionsDefinable_6() const { return ___emotionsDefinable_6; }
	inline EmotionsDefinableU5BU5D_t1185687067** get_address_of_emotionsDefinable_6() { return &___emotionsDefinable_6; }
	inline void set_emotionsDefinable_6(EmotionsDefinableU5BU5D_t1185687067* value)
	{
		___emotionsDefinable_6 = value;
		Il2CppCodeGenWriteBarrier((&___emotionsDefinable_6), value);
	}

	inline static int32_t get_offset_of_currentEmotion_12() { return static_cast<int32_t>(offsetof(Avataaars_t109144723, ___currentEmotion_12)); }
	inline int32_t get_currentEmotion_12() const { return ___currentEmotion_12; }
	inline int32_t* get_address_of_currentEmotion_12() { return &___currentEmotion_12; }
	inline void set_currentEmotion_12(int32_t value)
	{
		___currentEmotion_12 = value;
	}

	inline static int32_t get_offset_of_tHead_13() { return static_cast<int32_t>(offsetof(Avataaars_t109144723, ___tHead_13)); }
	inline Transform_t3600365921 * get_tHead_13() const { return ___tHead_13; }
	inline Transform_t3600365921 ** get_address_of_tHead_13() { return &___tHead_13; }
	inline void set_tHead_13(Transform_t3600365921 * value)
	{
		___tHead_13 = value;
		Il2CppCodeGenWriteBarrier((&___tHead_13), value);
	}
};

struct Avataaars_t109144723_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<Avataaars/AttachmentPoints,System.Collections.Generic.List`1<Avataaars/AvataaarPart>> Avataaars::dicAtach2AvataaarPart
	Dictionary_2_t2729386452 * ___dicAtach2AvataaarPart_7;
	// System.Collections.Generic.Dictionary`2<Avataaars/AttachmentPoints,UnityEngine.Transform> Avataaars::dicAttachmentPoint2Transform
	Dictionary_2_t2442083389 * ___dicAttachmentPoint2Transform_8;
	// System.Collections.Generic.Dictionary`2<Avataaars/Emotions,System.Collections.Generic.List`1<Avataaars/AvataaarPart>> Avataaars::dicEmo2Part
	Dictionary_2_t4055422985 * ___dicEmo2Part_9;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Transform,System.Collections.Generic.List`1<UnityEngine.Sprite>> Avataaars::dicTransform2Sprite
	Dictionary_2_t1063436421 * ___dicTransform2Sprite_10;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Transform,System.Collections.Generic.List`1<Avataaars/PartsAndFolderNames>> Avataaars::dicTransform2PartsAndFolderNames
	Dictionary_2_t3688575236 * ___dicTransform2PartsAndFolderNames_11;
	// System.String[] Avataaars::appleKeys
	StringU5BU5D_t1281789340* ___appleKeys_14;

public:
	inline static int32_t get_offset_of_dicAtach2AvataaarPart_7() { return static_cast<int32_t>(offsetof(Avataaars_t109144723_StaticFields, ___dicAtach2AvataaarPart_7)); }
	inline Dictionary_2_t2729386452 * get_dicAtach2AvataaarPart_7() const { return ___dicAtach2AvataaarPart_7; }
	inline Dictionary_2_t2729386452 ** get_address_of_dicAtach2AvataaarPart_7() { return &___dicAtach2AvataaarPart_7; }
	inline void set_dicAtach2AvataaarPart_7(Dictionary_2_t2729386452 * value)
	{
		___dicAtach2AvataaarPart_7 = value;
		Il2CppCodeGenWriteBarrier((&___dicAtach2AvataaarPart_7), value);
	}

	inline static int32_t get_offset_of_dicAttachmentPoint2Transform_8() { return static_cast<int32_t>(offsetof(Avataaars_t109144723_StaticFields, ___dicAttachmentPoint2Transform_8)); }
	inline Dictionary_2_t2442083389 * get_dicAttachmentPoint2Transform_8() const { return ___dicAttachmentPoint2Transform_8; }
	inline Dictionary_2_t2442083389 ** get_address_of_dicAttachmentPoint2Transform_8() { return &___dicAttachmentPoint2Transform_8; }
	inline void set_dicAttachmentPoint2Transform_8(Dictionary_2_t2442083389 * value)
	{
		___dicAttachmentPoint2Transform_8 = value;
		Il2CppCodeGenWriteBarrier((&___dicAttachmentPoint2Transform_8), value);
	}

	inline static int32_t get_offset_of_dicEmo2Part_9() { return static_cast<int32_t>(offsetof(Avataaars_t109144723_StaticFields, ___dicEmo2Part_9)); }
	inline Dictionary_2_t4055422985 * get_dicEmo2Part_9() const { return ___dicEmo2Part_9; }
	inline Dictionary_2_t4055422985 ** get_address_of_dicEmo2Part_9() { return &___dicEmo2Part_9; }
	inline void set_dicEmo2Part_9(Dictionary_2_t4055422985 * value)
	{
		___dicEmo2Part_9 = value;
		Il2CppCodeGenWriteBarrier((&___dicEmo2Part_9), value);
	}

	inline static int32_t get_offset_of_dicTransform2Sprite_10() { return static_cast<int32_t>(offsetof(Avataaars_t109144723_StaticFields, ___dicTransform2Sprite_10)); }
	inline Dictionary_2_t1063436421 * get_dicTransform2Sprite_10() const { return ___dicTransform2Sprite_10; }
	inline Dictionary_2_t1063436421 ** get_address_of_dicTransform2Sprite_10() { return &___dicTransform2Sprite_10; }
	inline void set_dicTransform2Sprite_10(Dictionary_2_t1063436421 * value)
	{
		___dicTransform2Sprite_10 = value;
		Il2CppCodeGenWriteBarrier((&___dicTransform2Sprite_10), value);
	}

	inline static int32_t get_offset_of_dicTransform2PartsAndFolderNames_11() { return static_cast<int32_t>(offsetof(Avataaars_t109144723_StaticFields, ___dicTransform2PartsAndFolderNames_11)); }
	inline Dictionary_2_t3688575236 * get_dicTransform2PartsAndFolderNames_11() const { return ___dicTransform2PartsAndFolderNames_11; }
	inline Dictionary_2_t3688575236 ** get_address_of_dicTransform2PartsAndFolderNames_11() { return &___dicTransform2PartsAndFolderNames_11; }
	inline void set_dicTransform2PartsAndFolderNames_11(Dictionary_2_t3688575236 * value)
	{
		___dicTransform2PartsAndFolderNames_11 = value;
		Il2CppCodeGenWriteBarrier((&___dicTransform2PartsAndFolderNames_11), value);
	}

	inline static int32_t get_offset_of_appleKeys_14() { return static_cast<int32_t>(offsetof(Avataaars_t109144723_StaticFields, ___appleKeys_14)); }
	inline StringU5BU5D_t1281789340* get_appleKeys_14() const { return ___appleKeys_14; }
	inline StringU5BU5D_t1281789340** get_address_of_appleKeys_14() { return &___appleKeys_14; }
	inline void set_appleKeys_14(StringU5BU5D_t1281789340* value)
	{
		___appleKeys_14 = value;
		Il2CppCodeGenWriteBarrier((&___appleKeys_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AVATAAARS_T109144723_H
#ifndef HISTOGRAM_T2046042414_H
#define HISTOGRAM_T2046042414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Histogram
struct  Histogram_t2046042414  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HISTOGRAM_T2046042414_H
#ifndef PUPPETEERBYFACE_T3068079294_H
#define PUPPETEERBYFACE_T3068079294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuppeteerByFace
struct  PuppeteerByFace_t3068079294  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface PuppeteerByFace::m_session
	UnityARSessionNativeInterface_t3929719369 * ___m_session_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> PuppeteerByFace::currentBlendShapes
	Dictionary_2_t1182523073 * ___currentBlendShapes_3;
	// UnityEngine.Transform PuppeteerByFace::tHypotheticalFace
	Transform_t3600365921 * ___tHypotheticalFace_4;

public:
	inline static int32_t get_offset_of_m_session_2() { return static_cast<int32_t>(offsetof(PuppeteerByFace_t3068079294, ___m_session_2)); }
	inline UnityARSessionNativeInterface_t3929719369 * get_m_session_2() const { return ___m_session_2; }
	inline UnityARSessionNativeInterface_t3929719369 ** get_address_of_m_session_2() { return &___m_session_2; }
	inline void set_m_session_2(UnityARSessionNativeInterface_t3929719369 * value)
	{
		___m_session_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_session_2), value);
	}

	inline static int32_t get_offset_of_currentBlendShapes_3() { return static_cast<int32_t>(offsetof(PuppeteerByFace_t3068079294, ___currentBlendShapes_3)); }
	inline Dictionary_2_t1182523073 * get_currentBlendShapes_3() const { return ___currentBlendShapes_3; }
	inline Dictionary_2_t1182523073 ** get_address_of_currentBlendShapes_3() { return &___currentBlendShapes_3; }
	inline void set_currentBlendShapes_3(Dictionary_2_t1182523073 * value)
	{
		___currentBlendShapes_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentBlendShapes_3), value);
	}

	inline static int32_t get_offset_of_tHypotheticalFace_4() { return static_cast<int32_t>(offsetof(PuppeteerByFace_t3068079294, ___tHypotheticalFace_4)); }
	inline Transform_t3600365921 * get_tHypotheticalFace_4() const { return ___tHypotheticalFace_4; }
	inline Transform_t3600365921 ** get_address_of_tHypotheticalFace_4() { return &___tHypotheticalFace_4; }
	inline void set_tHypotheticalFace_4(Transform_t3600365921 * value)
	{
		___tHypotheticalFace_4 = value;
		Il2CppCodeGenWriteBarrier((&___tHypotheticalFace_4), value);
	}
};

struct PuppeteerByFace_t3068079294_StaticFields
{
public:
	// PuppeteerByFace/ProcessEachBlendShapeUpdate PuppeteerByFace::SubscribeEachBlendShapeUpdate
	ProcessEachBlendShapeUpdate_t1319766324 * ___SubscribeEachBlendShapeUpdate_5;
	// System.Int32 PuppeteerByFace::totalFaces
	int32_t ___totalFaces_6;

public:
	inline static int32_t get_offset_of_SubscribeEachBlendShapeUpdate_5() { return static_cast<int32_t>(offsetof(PuppeteerByFace_t3068079294_StaticFields, ___SubscribeEachBlendShapeUpdate_5)); }
	inline ProcessEachBlendShapeUpdate_t1319766324 * get_SubscribeEachBlendShapeUpdate_5() const { return ___SubscribeEachBlendShapeUpdate_5; }
	inline ProcessEachBlendShapeUpdate_t1319766324 ** get_address_of_SubscribeEachBlendShapeUpdate_5() { return &___SubscribeEachBlendShapeUpdate_5; }
	inline void set_SubscribeEachBlendShapeUpdate_5(ProcessEachBlendShapeUpdate_t1319766324 * value)
	{
		___SubscribeEachBlendShapeUpdate_5 = value;
		Il2CppCodeGenWriteBarrier((&___SubscribeEachBlendShapeUpdate_5), value);
	}

	inline static int32_t get_offset_of_totalFaces_6() { return static_cast<int32_t>(offsetof(PuppeteerByFace_t3068079294_StaticFields, ___totalFaces_6)); }
	inline int32_t get_totalFaces_6() const { return ___totalFaces_6; }
	inline int32_t* get_address_of_totalFaces_6() { return &___totalFaces_6; }
	inline void set_totalFaces_6(int32_t value)
	{
		___totalFaces_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUPPETEERBYFACE_T3068079294_H
#ifndef UITOGGLE_T4192126258_H
#define UITOGGLE_T4192126258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIToggle
struct  UIToggle_t4192126258  : public MonoBehaviour_t3962482529
{
public:
	// UIToggleSet[] UIToggle::uitogglesets
	UIToggleSetU5BU5D_t1159468339* ___uitogglesets_2;

public:
	inline static int32_t get_offset_of_uitogglesets_2() { return static_cast<int32_t>(offsetof(UIToggle_t4192126258, ___uitogglesets_2)); }
	inline UIToggleSetU5BU5D_t1159468339* get_uitogglesets_2() const { return ___uitogglesets_2; }
	inline UIToggleSetU5BU5D_t1159468339** get_address_of_uitogglesets_2() { return &___uitogglesets_2; }
	inline void set_uitogglesets_2(UIToggleSetU5BU5D_t1159468339* value)
	{
		___uitogglesets_2 = value;
		Il2CppCodeGenWriteBarrier((&___uitogglesets_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UITOGGLE_T4192126258_H
#ifndef CFX_AUTODESTRUCTSHURIKEN_T4057240314_H
#define CFX_AUTODESTRUCTSHURIKEN_T4057240314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_AutoDestructShuriken
struct  CFX_AutoDestructShuriken_t4057240314  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean CFX_AutoDestructShuriken::OnlyDeactivate
	bool ___OnlyDeactivate_2;

public:
	inline static int32_t get_offset_of_OnlyDeactivate_2() { return static_cast<int32_t>(offsetof(CFX_AutoDestructShuriken_t4057240314, ___OnlyDeactivate_2)); }
	inline bool get_OnlyDeactivate_2() const { return ___OnlyDeactivate_2; }
	inline bool* get_address_of_OnlyDeactivate_2() { return &___OnlyDeactivate_2; }
	inline void set_OnlyDeactivate_2(bool value)
	{
		___OnlyDeactivate_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFX_AUTODESTRUCTSHURIKEN_T4057240314_H
#ifndef LOOKUPFILTER3D_T3361124730_H
#define LOOKUPFILTER3D_T3361124730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.LookupFilter3D
struct  LookupFilter3D_t3361124730  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Texture2D Colorful.LookupFilter3D::LookupTexture
	Texture2D_t3840446185 * ___LookupTexture_2;
	// System.Single Colorful.LookupFilter3D::Amount
	float ___Amount_3;
	// System.Boolean Colorful.LookupFilter3D::ForceCompatibility
	bool ___ForceCompatibility_4;
	// UnityEngine.Texture3D Colorful.LookupFilter3D::m_Lut3D
	Texture3D_t1884131049 * ___m_Lut3D_5;
	// System.String Colorful.LookupFilter3D::m_BaseTextureName
	String_t* ___m_BaseTextureName_6;
	// System.Boolean Colorful.LookupFilter3D::m_Use2DLut
	bool ___m_Use2DLut_7;
	// UnityEngine.Shader Colorful.LookupFilter3D::Shader2D
	Shader_t4151988712 * ___Shader2D_8;
	// UnityEngine.Shader Colorful.LookupFilter3D::Shader3D
	Shader_t4151988712 * ___Shader3D_9;
	// UnityEngine.Material Colorful.LookupFilter3D::m_Material2D
	Material_t340375123 * ___m_Material2D_10;
	// UnityEngine.Material Colorful.LookupFilter3D::m_Material3D
	Material_t340375123 * ___m_Material3D_11;

public:
	inline static int32_t get_offset_of_LookupTexture_2() { return static_cast<int32_t>(offsetof(LookupFilter3D_t3361124730, ___LookupTexture_2)); }
	inline Texture2D_t3840446185 * get_LookupTexture_2() const { return ___LookupTexture_2; }
	inline Texture2D_t3840446185 ** get_address_of_LookupTexture_2() { return &___LookupTexture_2; }
	inline void set_LookupTexture_2(Texture2D_t3840446185 * value)
	{
		___LookupTexture_2 = value;
		Il2CppCodeGenWriteBarrier((&___LookupTexture_2), value);
	}

	inline static int32_t get_offset_of_Amount_3() { return static_cast<int32_t>(offsetof(LookupFilter3D_t3361124730, ___Amount_3)); }
	inline float get_Amount_3() const { return ___Amount_3; }
	inline float* get_address_of_Amount_3() { return &___Amount_3; }
	inline void set_Amount_3(float value)
	{
		___Amount_3 = value;
	}

	inline static int32_t get_offset_of_ForceCompatibility_4() { return static_cast<int32_t>(offsetof(LookupFilter3D_t3361124730, ___ForceCompatibility_4)); }
	inline bool get_ForceCompatibility_4() const { return ___ForceCompatibility_4; }
	inline bool* get_address_of_ForceCompatibility_4() { return &___ForceCompatibility_4; }
	inline void set_ForceCompatibility_4(bool value)
	{
		___ForceCompatibility_4 = value;
	}

	inline static int32_t get_offset_of_m_Lut3D_5() { return static_cast<int32_t>(offsetof(LookupFilter3D_t3361124730, ___m_Lut3D_5)); }
	inline Texture3D_t1884131049 * get_m_Lut3D_5() const { return ___m_Lut3D_5; }
	inline Texture3D_t1884131049 ** get_address_of_m_Lut3D_5() { return &___m_Lut3D_5; }
	inline void set_m_Lut3D_5(Texture3D_t1884131049 * value)
	{
		___m_Lut3D_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Lut3D_5), value);
	}

	inline static int32_t get_offset_of_m_BaseTextureName_6() { return static_cast<int32_t>(offsetof(LookupFilter3D_t3361124730, ___m_BaseTextureName_6)); }
	inline String_t* get_m_BaseTextureName_6() const { return ___m_BaseTextureName_6; }
	inline String_t** get_address_of_m_BaseTextureName_6() { return &___m_BaseTextureName_6; }
	inline void set_m_BaseTextureName_6(String_t* value)
	{
		___m_BaseTextureName_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_BaseTextureName_6), value);
	}

	inline static int32_t get_offset_of_m_Use2DLut_7() { return static_cast<int32_t>(offsetof(LookupFilter3D_t3361124730, ___m_Use2DLut_7)); }
	inline bool get_m_Use2DLut_7() const { return ___m_Use2DLut_7; }
	inline bool* get_address_of_m_Use2DLut_7() { return &___m_Use2DLut_7; }
	inline void set_m_Use2DLut_7(bool value)
	{
		___m_Use2DLut_7 = value;
	}

	inline static int32_t get_offset_of_Shader2D_8() { return static_cast<int32_t>(offsetof(LookupFilter3D_t3361124730, ___Shader2D_8)); }
	inline Shader_t4151988712 * get_Shader2D_8() const { return ___Shader2D_8; }
	inline Shader_t4151988712 ** get_address_of_Shader2D_8() { return &___Shader2D_8; }
	inline void set_Shader2D_8(Shader_t4151988712 * value)
	{
		___Shader2D_8 = value;
		Il2CppCodeGenWriteBarrier((&___Shader2D_8), value);
	}

	inline static int32_t get_offset_of_Shader3D_9() { return static_cast<int32_t>(offsetof(LookupFilter3D_t3361124730, ___Shader3D_9)); }
	inline Shader_t4151988712 * get_Shader3D_9() const { return ___Shader3D_9; }
	inline Shader_t4151988712 ** get_address_of_Shader3D_9() { return &___Shader3D_9; }
	inline void set_Shader3D_9(Shader_t4151988712 * value)
	{
		___Shader3D_9 = value;
		Il2CppCodeGenWriteBarrier((&___Shader3D_9), value);
	}

	inline static int32_t get_offset_of_m_Material2D_10() { return static_cast<int32_t>(offsetof(LookupFilter3D_t3361124730, ___m_Material2D_10)); }
	inline Material_t340375123 * get_m_Material2D_10() const { return ___m_Material2D_10; }
	inline Material_t340375123 ** get_address_of_m_Material2D_10() { return &___m_Material2D_10; }
	inline void set_m_Material2D_10(Material_t340375123 * value)
	{
		___m_Material2D_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material2D_10), value);
	}

	inline static int32_t get_offset_of_m_Material3D_11() { return static_cast<int32_t>(offsetof(LookupFilter3D_t3361124730, ___m_Material3D_11)); }
	inline Material_t340375123 * get_m_Material3D_11() const { return ___m_Material3D_11; }
	inline Material_t340375123 ** get_address_of_m_Material3D_11() { return &___m_Material3D_11; }
	inline void set_m_Material3D_11(Material_t340375123 * value)
	{
		___m_Material3D_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material3D_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOKUPFILTER3D_T3361124730_H
#ifndef UIINPUTWAIT_T3530943742_H
#define UIINPUTWAIT_T3530943742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIInputWait
struct  UIInputWait_t3530943742  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField UIInputWait::inputfield
	InputField_t3762917431 * ___inputfield_2;
	// UnityEngine.Sprite UIInputWait::spriteWait
	Sprite_t280657092 * ___spriteWait_3;
	// UnityEngine.UI.Button UIInputWait::ButtonToDisable
	Button_t4055032469 * ___ButtonToDisable_4;
	// UnityEngine.Sprite UIInputWait::button_lastSprite
	Sprite_t280657092 * ___button_lastSprite_5;
	// UnityEngine.GameObject UIInputWait::goWait
	GameObject_t1113636619 * ___goWait_6;
	// UIInputWait/StringProcessor UIInputWait::SubscribeWhenStringReceived
	StringProcessor_t3163369541 * ___SubscribeWhenStringReceived_7;
	// System.String UIInputWait::lastSet
	String_t* ___lastSet_8;

public:
	inline static int32_t get_offset_of_inputfield_2() { return static_cast<int32_t>(offsetof(UIInputWait_t3530943742, ___inputfield_2)); }
	inline InputField_t3762917431 * get_inputfield_2() const { return ___inputfield_2; }
	inline InputField_t3762917431 ** get_address_of_inputfield_2() { return &___inputfield_2; }
	inline void set_inputfield_2(InputField_t3762917431 * value)
	{
		___inputfield_2 = value;
		Il2CppCodeGenWriteBarrier((&___inputfield_2), value);
	}

	inline static int32_t get_offset_of_spriteWait_3() { return static_cast<int32_t>(offsetof(UIInputWait_t3530943742, ___spriteWait_3)); }
	inline Sprite_t280657092 * get_spriteWait_3() const { return ___spriteWait_3; }
	inline Sprite_t280657092 ** get_address_of_spriteWait_3() { return &___spriteWait_3; }
	inline void set_spriteWait_3(Sprite_t280657092 * value)
	{
		___spriteWait_3 = value;
		Il2CppCodeGenWriteBarrier((&___spriteWait_3), value);
	}

	inline static int32_t get_offset_of_ButtonToDisable_4() { return static_cast<int32_t>(offsetof(UIInputWait_t3530943742, ___ButtonToDisable_4)); }
	inline Button_t4055032469 * get_ButtonToDisable_4() const { return ___ButtonToDisable_4; }
	inline Button_t4055032469 ** get_address_of_ButtonToDisable_4() { return &___ButtonToDisable_4; }
	inline void set_ButtonToDisable_4(Button_t4055032469 * value)
	{
		___ButtonToDisable_4 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonToDisable_4), value);
	}

	inline static int32_t get_offset_of_button_lastSprite_5() { return static_cast<int32_t>(offsetof(UIInputWait_t3530943742, ___button_lastSprite_5)); }
	inline Sprite_t280657092 * get_button_lastSprite_5() const { return ___button_lastSprite_5; }
	inline Sprite_t280657092 ** get_address_of_button_lastSprite_5() { return &___button_lastSprite_5; }
	inline void set_button_lastSprite_5(Sprite_t280657092 * value)
	{
		___button_lastSprite_5 = value;
		Il2CppCodeGenWriteBarrier((&___button_lastSprite_5), value);
	}

	inline static int32_t get_offset_of_goWait_6() { return static_cast<int32_t>(offsetof(UIInputWait_t3530943742, ___goWait_6)); }
	inline GameObject_t1113636619 * get_goWait_6() const { return ___goWait_6; }
	inline GameObject_t1113636619 ** get_address_of_goWait_6() { return &___goWait_6; }
	inline void set_goWait_6(GameObject_t1113636619 * value)
	{
		___goWait_6 = value;
		Il2CppCodeGenWriteBarrier((&___goWait_6), value);
	}

	inline static int32_t get_offset_of_SubscribeWhenStringReceived_7() { return static_cast<int32_t>(offsetof(UIInputWait_t3530943742, ___SubscribeWhenStringReceived_7)); }
	inline StringProcessor_t3163369541 * get_SubscribeWhenStringReceived_7() const { return ___SubscribeWhenStringReceived_7; }
	inline StringProcessor_t3163369541 ** get_address_of_SubscribeWhenStringReceived_7() { return &___SubscribeWhenStringReceived_7; }
	inline void set_SubscribeWhenStringReceived_7(StringProcessor_t3163369541 * value)
	{
		___SubscribeWhenStringReceived_7 = value;
		Il2CppCodeGenWriteBarrier((&___SubscribeWhenStringReceived_7), value);
	}

	inline static int32_t get_offset_of_lastSet_8() { return static_cast<int32_t>(offsetof(UIInputWait_t3530943742, ___lastSet_8)); }
	inline String_t* get_lastSet_8() const { return ___lastSet_8; }
	inline String_t** get_address_of_lastSet_8() { return &___lastSet_8; }
	inline void set_lastSet_8(String_t* value)
	{
		___lastSet_8 = value;
		Il2CppCodeGenWriteBarrier((&___lastSet_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIINPUTWAIT_T3530943742_H
#ifndef CFX_INSPECTORHELP_T4185620777_H
#define CFX_INSPECTORHELP_T4185620777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_InspectorHelp
struct  CFX_InspectorHelp_t4185620777  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean CFX_InspectorHelp::Locked
	bool ___Locked_2;
	// System.String CFX_InspectorHelp::Title
	String_t* ___Title_3;
	// System.String CFX_InspectorHelp::HelpText
	String_t* ___HelpText_4;
	// System.Int32 CFX_InspectorHelp::MsgType
	int32_t ___MsgType_5;

public:
	inline static int32_t get_offset_of_Locked_2() { return static_cast<int32_t>(offsetof(CFX_InspectorHelp_t4185620777, ___Locked_2)); }
	inline bool get_Locked_2() const { return ___Locked_2; }
	inline bool* get_address_of_Locked_2() { return &___Locked_2; }
	inline void set_Locked_2(bool value)
	{
		___Locked_2 = value;
	}

	inline static int32_t get_offset_of_Title_3() { return static_cast<int32_t>(offsetof(CFX_InspectorHelp_t4185620777, ___Title_3)); }
	inline String_t* get_Title_3() const { return ___Title_3; }
	inline String_t** get_address_of_Title_3() { return &___Title_3; }
	inline void set_Title_3(String_t* value)
	{
		___Title_3 = value;
		Il2CppCodeGenWriteBarrier((&___Title_3), value);
	}

	inline static int32_t get_offset_of_HelpText_4() { return static_cast<int32_t>(offsetof(CFX_InspectorHelp_t4185620777, ___HelpText_4)); }
	inline String_t* get_HelpText_4() const { return ___HelpText_4; }
	inline String_t** get_address_of_HelpText_4() { return &___HelpText_4; }
	inline void set_HelpText_4(String_t* value)
	{
		___HelpText_4 = value;
		Il2CppCodeGenWriteBarrier((&___HelpText_4), value);
	}

	inline static int32_t get_offset_of_MsgType_5() { return static_cast<int32_t>(offsetof(CFX_InspectorHelp_t4185620777, ___MsgType_5)); }
	inline int32_t get_MsgType_5() const { return ___MsgType_5; }
	inline int32_t* get_address_of_MsgType_5() { return &___MsgType_5; }
	inline void set_MsgType_5(int32_t value)
	{
		___MsgType_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFX_INSPECTORHELP_T4185620777_H
#ifndef GLITCH_T3656535212_H
#define GLITCH_T3656535212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Glitch
struct  Glitch_t3656535212  : public BaseEffect_t1187847871
{
public:
	// System.Boolean Colorful.Glitch::RandomActivation
	bool ___RandomActivation_4;
	// UnityEngine.Vector2 Colorful.Glitch::RandomEvery
	Vector2_t2156229523  ___RandomEvery_5;
	// UnityEngine.Vector2 Colorful.Glitch::RandomDuration
	Vector2_t2156229523  ___RandomDuration_6;
	// Colorful.Glitch/GlitchingMode Colorful.Glitch::Mode
	int32_t ___Mode_7;
	// Colorful.Glitch/InterferenceSettings Colorful.Glitch::SettingsInterferences
	InterferenceSettings_t2118005662 * ___SettingsInterferences_8;
	// Colorful.Glitch/TearingSettings Colorful.Glitch::SettingsTearing
	TearingSettings_t1139179787 * ___SettingsTearing_9;
	// System.Boolean Colorful.Glitch::m_Activated
	bool ___m_Activated_10;
	// System.Single Colorful.Glitch::m_EveryTimer
	float ___m_EveryTimer_11;
	// System.Single Colorful.Glitch::m_EveryTimerEnd
	float ___m_EveryTimerEnd_12;
	// System.Single Colorful.Glitch::m_DurationTimer
	float ___m_DurationTimer_13;
	// System.Single Colorful.Glitch::m_DurationTimerEnd
	float ___m_DurationTimerEnd_14;

public:
	inline static int32_t get_offset_of_RandomActivation_4() { return static_cast<int32_t>(offsetof(Glitch_t3656535212, ___RandomActivation_4)); }
	inline bool get_RandomActivation_4() const { return ___RandomActivation_4; }
	inline bool* get_address_of_RandomActivation_4() { return &___RandomActivation_4; }
	inline void set_RandomActivation_4(bool value)
	{
		___RandomActivation_4 = value;
	}

	inline static int32_t get_offset_of_RandomEvery_5() { return static_cast<int32_t>(offsetof(Glitch_t3656535212, ___RandomEvery_5)); }
	inline Vector2_t2156229523  get_RandomEvery_5() const { return ___RandomEvery_5; }
	inline Vector2_t2156229523 * get_address_of_RandomEvery_5() { return &___RandomEvery_5; }
	inline void set_RandomEvery_5(Vector2_t2156229523  value)
	{
		___RandomEvery_5 = value;
	}

	inline static int32_t get_offset_of_RandomDuration_6() { return static_cast<int32_t>(offsetof(Glitch_t3656535212, ___RandomDuration_6)); }
	inline Vector2_t2156229523  get_RandomDuration_6() const { return ___RandomDuration_6; }
	inline Vector2_t2156229523 * get_address_of_RandomDuration_6() { return &___RandomDuration_6; }
	inline void set_RandomDuration_6(Vector2_t2156229523  value)
	{
		___RandomDuration_6 = value;
	}

	inline static int32_t get_offset_of_Mode_7() { return static_cast<int32_t>(offsetof(Glitch_t3656535212, ___Mode_7)); }
	inline int32_t get_Mode_7() const { return ___Mode_7; }
	inline int32_t* get_address_of_Mode_7() { return &___Mode_7; }
	inline void set_Mode_7(int32_t value)
	{
		___Mode_7 = value;
	}

	inline static int32_t get_offset_of_SettingsInterferences_8() { return static_cast<int32_t>(offsetof(Glitch_t3656535212, ___SettingsInterferences_8)); }
	inline InterferenceSettings_t2118005662 * get_SettingsInterferences_8() const { return ___SettingsInterferences_8; }
	inline InterferenceSettings_t2118005662 ** get_address_of_SettingsInterferences_8() { return &___SettingsInterferences_8; }
	inline void set_SettingsInterferences_8(InterferenceSettings_t2118005662 * value)
	{
		___SettingsInterferences_8 = value;
		Il2CppCodeGenWriteBarrier((&___SettingsInterferences_8), value);
	}

	inline static int32_t get_offset_of_SettingsTearing_9() { return static_cast<int32_t>(offsetof(Glitch_t3656535212, ___SettingsTearing_9)); }
	inline TearingSettings_t1139179787 * get_SettingsTearing_9() const { return ___SettingsTearing_9; }
	inline TearingSettings_t1139179787 ** get_address_of_SettingsTearing_9() { return &___SettingsTearing_9; }
	inline void set_SettingsTearing_9(TearingSettings_t1139179787 * value)
	{
		___SettingsTearing_9 = value;
		Il2CppCodeGenWriteBarrier((&___SettingsTearing_9), value);
	}

	inline static int32_t get_offset_of_m_Activated_10() { return static_cast<int32_t>(offsetof(Glitch_t3656535212, ___m_Activated_10)); }
	inline bool get_m_Activated_10() const { return ___m_Activated_10; }
	inline bool* get_address_of_m_Activated_10() { return &___m_Activated_10; }
	inline void set_m_Activated_10(bool value)
	{
		___m_Activated_10 = value;
	}

	inline static int32_t get_offset_of_m_EveryTimer_11() { return static_cast<int32_t>(offsetof(Glitch_t3656535212, ___m_EveryTimer_11)); }
	inline float get_m_EveryTimer_11() const { return ___m_EveryTimer_11; }
	inline float* get_address_of_m_EveryTimer_11() { return &___m_EveryTimer_11; }
	inline void set_m_EveryTimer_11(float value)
	{
		___m_EveryTimer_11 = value;
	}

	inline static int32_t get_offset_of_m_EveryTimerEnd_12() { return static_cast<int32_t>(offsetof(Glitch_t3656535212, ___m_EveryTimerEnd_12)); }
	inline float get_m_EveryTimerEnd_12() const { return ___m_EveryTimerEnd_12; }
	inline float* get_address_of_m_EveryTimerEnd_12() { return &___m_EveryTimerEnd_12; }
	inline void set_m_EveryTimerEnd_12(float value)
	{
		___m_EveryTimerEnd_12 = value;
	}

	inline static int32_t get_offset_of_m_DurationTimer_13() { return static_cast<int32_t>(offsetof(Glitch_t3656535212, ___m_DurationTimer_13)); }
	inline float get_m_DurationTimer_13() const { return ___m_DurationTimer_13; }
	inline float* get_address_of_m_DurationTimer_13() { return &___m_DurationTimer_13; }
	inline void set_m_DurationTimer_13(float value)
	{
		___m_DurationTimer_13 = value;
	}

	inline static int32_t get_offset_of_m_DurationTimerEnd_14() { return static_cast<int32_t>(offsetof(Glitch_t3656535212, ___m_DurationTimerEnd_14)); }
	inline float get_m_DurationTimerEnd_14() const { return ___m_DurationTimerEnd_14; }
	inline float* get_address_of_m_DurationTimerEnd_14() { return &___m_DurationTimerEnd_14; }
	inline void set_m_DurationTimerEnd_14(float value)
	{
		___m_DurationTimerEnd_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLITCH_T3656535212_H
#ifndef GRADIENTRAMP_T513234030_H
#define GRADIENTRAMP_T513234030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.GradientRamp
struct  GradientRamp_t513234030  : public BaseEffect_t1187847871
{
public:
	// UnityEngine.Texture Colorful.GradientRamp::RampTexture
	Texture_t3661962703 * ___RampTexture_4;
	// System.Single Colorful.GradientRamp::Amount
	float ___Amount_5;

public:
	inline static int32_t get_offset_of_RampTexture_4() { return static_cast<int32_t>(offsetof(GradientRamp_t513234030, ___RampTexture_4)); }
	inline Texture_t3661962703 * get_RampTexture_4() const { return ___RampTexture_4; }
	inline Texture_t3661962703 ** get_address_of_RampTexture_4() { return &___RampTexture_4; }
	inline void set_RampTexture_4(Texture_t3661962703 * value)
	{
		___RampTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___RampTexture_4), value);
	}

	inline static int32_t get_offset_of_Amount_5() { return static_cast<int32_t>(offsetof(GradientRamp_t513234030, ___Amount_5)); }
	inline float get_Amount_5() const { return ___Amount_5; }
	inline float* get_address_of_Amount_5() { return &___Amount_5; }
	inline void set_Amount_5(float value)
	{
		___Amount_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADIENTRAMP_T513234030_H
#ifndef GRADIENTRAMPDYNAMIC_T2043771246_H
#define GRADIENTRAMPDYNAMIC_T2043771246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.GradientRampDynamic
struct  GradientRampDynamic_t2043771246  : public BaseEffect_t1187847871
{
public:
	// UnityEngine.Gradient Colorful.GradientRampDynamic::Ramp
	Gradient_t3067099924 * ___Ramp_4;
	// System.Single Colorful.GradientRampDynamic::Amount
	float ___Amount_5;
	// UnityEngine.Texture2D Colorful.GradientRampDynamic::m_RampTexture
	Texture2D_t3840446185 * ___m_RampTexture_6;

public:
	inline static int32_t get_offset_of_Ramp_4() { return static_cast<int32_t>(offsetof(GradientRampDynamic_t2043771246, ___Ramp_4)); }
	inline Gradient_t3067099924 * get_Ramp_4() const { return ___Ramp_4; }
	inline Gradient_t3067099924 ** get_address_of_Ramp_4() { return &___Ramp_4; }
	inline void set_Ramp_4(Gradient_t3067099924 * value)
	{
		___Ramp_4 = value;
		Il2CppCodeGenWriteBarrier((&___Ramp_4), value);
	}

	inline static int32_t get_offset_of_Amount_5() { return static_cast<int32_t>(offsetof(GradientRampDynamic_t2043771246, ___Amount_5)); }
	inline float get_Amount_5() const { return ___Amount_5; }
	inline float* get_address_of_Amount_5() { return &___Amount_5; }
	inline void set_Amount_5(float value)
	{
		___Amount_5 = value;
	}

	inline static int32_t get_offset_of_m_RampTexture_6() { return static_cast<int32_t>(offsetof(GradientRampDynamic_t2043771246, ___m_RampTexture_6)); }
	inline Texture2D_t3840446185 * get_m_RampTexture_6() const { return ___m_RampTexture_6; }
	inline Texture2D_t3840446185 ** get_address_of_m_RampTexture_6() { return &___m_RampTexture_6; }
	inline void set_m_RampTexture_6(Texture2D_t3840446185 * value)
	{
		___m_RampTexture_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_RampTexture_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRADIENTRAMPDYNAMIC_T2043771246_H
#ifndef GRAINYBLUR_T745788970_H
#define GRAINYBLUR_T745788970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.GrainyBlur
struct  GrainyBlur_t745788970  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.GrainyBlur::Radius
	float ___Radius_4;
	// System.Int32 Colorful.GrainyBlur::Samples
	int32_t ___Samples_5;

public:
	inline static int32_t get_offset_of_Radius_4() { return static_cast<int32_t>(offsetof(GrainyBlur_t745788970, ___Radius_4)); }
	inline float get_Radius_4() const { return ___Radius_4; }
	inline float* get_address_of_Radius_4() { return &___Radius_4; }
	inline void set_Radius_4(float value)
	{
		___Radius_4 = value;
	}

	inline static int32_t get_offset_of_Samples_5() { return static_cast<int32_t>(offsetof(GrainyBlur_t745788970, ___Samples_5)); }
	inline int32_t get_Samples_5() const { return ___Samples_5; }
	inline int32_t* get_address_of_Samples_5() { return &___Samples_5; }
	inline void set_Samples_5(int32_t value)
	{
		___Samples_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAINYBLUR_T745788970_H
#ifndef GRAYSCALE_T3716926545_H
#define GRAYSCALE_T3716926545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Grayscale
struct  Grayscale_t3716926545  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.Grayscale::RedLuminance
	float ___RedLuminance_4;
	// System.Single Colorful.Grayscale::GreenLuminance
	float ___GreenLuminance_5;
	// System.Single Colorful.Grayscale::BlueLuminance
	float ___BlueLuminance_6;
	// System.Single Colorful.Grayscale::Amount
	float ___Amount_7;

public:
	inline static int32_t get_offset_of_RedLuminance_4() { return static_cast<int32_t>(offsetof(Grayscale_t3716926545, ___RedLuminance_4)); }
	inline float get_RedLuminance_4() const { return ___RedLuminance_4; }
	inline float* get_address_of_RedLuminance_4() { return &___RedLuminance_4; }
	inline void set_RedLuminance_4(float value)
	{
		___RedLuminance_4 = value;
	}

	inline static int32_t get_offset_of_GreenLuminance_5() { return static_cast<int32_t>(offsetof(Grayscale_t3716926545, ___GreenLuminance_5)); }
	inline float get_GreenLuminance_5() const { return ___GreenLuminance_5; }
	inline float* get_address_of_GreenLuminance_5() { return &___GreenLuminance_5; }
	inline void set_GreenLuminance_5(float value)
	{
		___GreenLuminance_5 = value;
	}

	inline static int32_t get_offset_of_BlueLuminance_6() { return static_cast<int32_t>(offsetof(Grayscale_t3716926545, ___BlueLuminance_6)); }
	inline float get_BlueLuminance_6() const { return ___BlueLuminance_6; }
	inline float* get_address_of_BlueLuminance_6() { return &___BlueLuminance_6; }
	inline void set_BlueLuminance_6(float value)
	{
		___BlueLuminance_6 = value;
	}

	inline static int32_t get_offset_of_Amount_7() { return static_cast<int32_t>(offsetof(Grayscale_t3716926545, ___Amount_7)); }
	inline float get_Amount_7() const { return ___Amount_7; }
	inline float* get_address_of_Amount_7() { return &___Amount_7; }
	inline void set_Amount_7(float value)
	{
		___Amount_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAYSCALE_T3716926545_H
#ifndef HALFTONE_T1918305288_H
#define HALFTONE_T1918305288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Halftone
struct  Halftone_t1918305288  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.Halftone::Scale
	float ___Scale_4;
	// System.Single Colorful.Halftone::DotSize
	float ___DotSize_5;
	// System.Single Colorful.Halftone::Angle
	float ___Angle_6;
	// System.Single Colorful.Halftone::Smoothness
	float ___Smoothness_7;
	// UnityEngine.Vector2 Colorful.Halftone::Center
	Vector2_t2156229523  ___Center_8;
	// System.Boolean Colorful.Halftone::Desaturate
	bool ___Desaturate_9;

public:
	inline static int32_t get_offset_of_Scale_4() { return static_cast<int32_t>(offsetof(Halftone_t1918305288, ___Scale_4)); }
	inline float get_Scale_4() const { return ___Scale_4; }
	inline float* get_address_of_Scale_4() { return &___Scale_4; }
	inline void set_Scale_4(float value)
	{
		___Scale_4 = value;
	}

	inline static int32_t get_offset_of_DotSize_5() { return static_cast<int32_t>(offsetof(Halftone_t1918305288, ___DotSize_5)); }
	inline float get_DotSize_5() const { return ___DotSize_5; }
	inline float* get_address_of_DotSize_5() { return &___DotSize_5; }
	inline void set_DotSize_5(float value)
	{
		___DotSize_5 = value;
	}

	inline static int32_t get_offset_of_Angle_6() { return static_cast<int32_t>(offsetof(Halftone_t1918305288, ___Angle_6)); }
	inline float get_Angle_6() const { return ___Angle_6; }
	inline float* get_address_of_Angle_6() { return &___Angle_6; }
	inline void set_Angle_6(float value)
	{
		___Angle_6 = value;
	}

	inline static int32_t get_offset_of_Smoothness_7() { return static_cast<int32_t>(offsetof(Halftone_t1918305288, ___Smoothness_7)); }
	inline float get_Smoothness_7() const { return ___Smoothness_7; }
	inline float* get_address_of_Smoothness_7() { return &___Smoothness_7; }
	inline void set_Smoothness_7(float value)
	{
		___Smoothness_7 = value;
	}

	inline static int32_t get_offset_of_Center_8() { return static_cast<int32_t>(offsetof(Halftone_t1918305288, ___Center_8)); }
	inline Vector2_t2156229523  get_Center_8() const { return ___Center_8; }
	inline Vector2_t2156229523 * get_address_of_Center_8() { return &___Center_8; }
	inline void set_Center_8(Vector2_t2156229523  value)
	{
		___Center_8 = value;
	}

	inline static int32_t get_offset_of_Desaturate_9() { return static_cast<int32_t>(offsetof(Halftone_t1918305288, ___Desaturate_9)); }
	inline bool get_Desaturate_9() const { return ___Desaturate_9; }
	inline bool* get_address_of_Desaturate_9() { return &___Desaturate_9; }
	inline void set_Desaturate_9(bool value)
	{
		___Desaturate_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HALFTONE_T1918305288_H
#ifndef LETTERBOX_T3911859827_H
#define LETTERBOX_T3911859827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Letterbox
struct  Letterbox_t3911859827  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.Letterbox::Aspect
	float ___Aspect_4;
	// UnityEngine.Color Colorful.Letterbox::FillColor
	Color_t2555686324  ___FillColor_5;

public:
	inline static int32_t get_offset_of_Aspect_4() { return static_cast<int32_t>(offsetof(Letterbox_t3911859827, ___Aspect_4)); }
	inline float get_Aspect_4() const { return ___Aspect_4; }
	inline float* get_address_of_Aspect_4() { return &___Aspect_4; }
	inline void set_Aspect_4(float value)
	{
		___Aspect_4 = value;
	}

	inline static int32_t get_offset_of_FillColor_5() { return static_cast<int32_t>(offsetof(Letterbox_t3911859827, ___FillColor_5)); }
	inline Color_t2555686324  get_FillColor_5() const { return ___FillColor_5; }
	inline Color_t2555686324 * get_address_of_FillColor_5() { return &___FillColor_5; }
	inline void set_FillColor_5(Color_t2555686324  value)
	{
		___FillColor_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LETTERBOX_T3911859827_H
#ifndef LENSDISTORTIONBLUR_T2959671745_H
#define LENSDISTORTIONBLUR_T2959671745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.LensDistortionBlur
struct  LensDistortionBlur_t2959671745  : public BaseEffect_t1187847871
{
public:
	// Colorful.LensDistortionBlur/QualityPreset Colorful.LensDistortionBlur::Quality
	int32_t ___Quality_4;
	// System.Int32 Colorful.LensDistortionBlur::Samples
	int32_t ___Samples_5;
	// System.Single Colorful.LensDistortionBlur::Distortion
	float ___Distortion_6;
	// System.Single Colorful.LensDistortionBlur::CubicDistortion
	float ___CubicDistortion_7;
	// System.Single Colorful.LensDistortionBlur::Scale
	float ___Scale_8;

public:
	inline static int32_t get_offset_of_Quality_4() { return static_cast<int32_t>(offsetof(LensDistortionBlur_t2959671745, ___Quality_4)); }
	inline int32_t get_Quality_4() const { return ___Quality_4; }
	inline int32_t* get_address_of_Quality_4() { return &___Quality_4; }
	inline void set_Quality_4(int32_t value)
	{
		___Quality_4 = value;
	}

	inline static int32_t get_offset_of_Samples_5() { return static_cast<int32_t>(offsetof(LensDistortionBlur_t2959671745, ___Samples_5)); }
	inline int32_t get_Samples_5() const { return ___Samples_5; }
	inline int32_t* get_address_of_Samples_5() { return &___Samples_5; }
	inline void set_Samples_5(int32_t value)
	{
		___Samples_5 = value;
	}

	inline static int32_t get_offset_of_Distortion_6() { return static_cast<int32_t>(offsetof(LensDistortionBlur_t2959671745, ___Distortion_6)); }
	inline float get_Distortion_6() const { return ___Distortion_6; }
	inline float* get_address_of_Distortion_6() { return &___Distortion_6; }
	inline void set_Distortion_6(float value)
	{
		___Distortion_6 = value;
	}

	inline static int32_t get_offset_of_CubicDistortion_7() { return static_cast<int32_t>(offsetof(LensDistortionBlur_t2959671745, ___CubicDistortion_7)); }
	inline float get_CubicDistortion_7() const { return ___CubicDistortion_7; }
	inline float* get_address_of_CubicDistortion_7() { return &___CubicDistortion_7; }
	inline void set_CubicDistortion_7(float value)
	{
		___CubicDistortion_7 = value;
	}

	inline static int32_t get_offset_of_Scale_8() { return static_cast<int32_t>(offsetof(LensDistortionBlur_t2959671745, ___Scale_8)); }
	inline float get_Scale_8() const { return ___Scale_8; }
	inline float* get_address_of_Scale_8() { return &___Scale_8; }
	inline void set_Scale_8(float value)
	{
		___Scale_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LENSDISTORTIONBLUR_T2959671745_H
#ifndef LED_T3330730803_H
#define LED_T3330730803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Led
struct  Led_t3330730803  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.Led::Scale
	float ___Scale_4;
	// System.Single Colorful.Led::Brightness
	float ___Brightness_5;
	// System.Single Colorful.Led::Shape
	float ___Shape_6;
	// System.Boolean Colorful.Led::AutomaticRatio
	bool ___AutomaticRatio_7;
	// System.Single Colorful.Led::Ratio
	float ___Ratio_8;
	// Colorful.Led/SizeMode Colorful.Led::Mode
	int32_t ___Mode_9;

public:
	inline static int32_t get_offset_of_Scale_4() { return static_cast<int32_t>(offsetof(Led_t3330730803, ___Scale_4)); }
	inline float get_Scale_4() const { return ___Scale_4; }
	inline float* get_address_of_Scale_4() { return &___Scale_4; }
	inline void set_Scale_4(float value)
	{
		___Scale_4 = value;
	}

	inline static int32_t get_offset_of_Brightness_5() { return static_cast<int32_t>(offsetof(Led_t3330730803, ___Brightness_5)); }
	inline float get_Brightness_5() const { return ___Brightness_5; }
	inline float* get_address_of_Brightness_5() { return &___Brightness_5; }
	inline void set_Brightness_5(float value)
	{
		___Brightness_5 = value;
	}

	inline static int32_t get_offset_of_Shape_6() { return static_cast<int32_t>(offsetof(Led_t3330730803, ___Shape_6)); }
	inline float get_Shape_6() const { return ___Shape_6; }
	inline float* get_address_of_Shape_6() { return &___Shape_6; }
	inline void set_Shape_6(float value)
	{
		___Shape_6 = value;
	}

	inline static int32_t get_offset_of_AutomaticRatio_7() { return static_cast<int32_t>(offsetof(Led_t3330730803, ___AutomaticRatio_7)); }
	inline bool get_AutomaticRatio_7() const { return ___AutomaticRatio_7; }
	inline bool* get_address_of_AutomaticRatio_7() { return &___AutomaticRatio_7; }
	inline void set_AutomaticRatio_7(bool value)
	{
		___AutomaticRatio_7 = value;
	}

	inline static int32_t get_offset_of_Ratio_8() { return static_cast<int32_t>(offsetof(Led_t3330730803, ___Ratio_8)); }
	inline float get_Ratio_8() const { return ___Ratio_8; }
	inline float* get_address_of_Ratio_8() { return &___Ratio_8; }
	inline void set_Ratio_8(float value)
	{
		___Ratio_8 = value;
	}

	inline static int32_t get_offset_of_Mode_9() { return static_cast<int32_t>(offsetof(Led_t3330730803, ___Mode_9)); }
	inline int32_t get_Mode_9() const { return ___Mode_9; }
	inline int32_t* get_address_of_Mode_9() { return &___Mode_9; }
	inline void set_Mode_9(int32_t value)
	{
		___Mode_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LED_T3330730803_H
#ifndef KUWAHARA_T240745109_H
#define KUWAHARA_T240745109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Kuwahara
struct  Kuwahara_t240745109  : public BaseEffect_t1187847871
{
public:
	// System.Int32 Colorful.Kuwahara::Radius
	int32_t ___Radius_4;

public:
	inline static int32_t get_offset_of_Radius_4() { return static_cast<int32_t>(offsetof(Kuwahara_t240745109, ___Radius_4)); }
	inline int32_t get_Radius_4() const { return ___Radius_4; }
	inline int32_t* get_address_of_Radius_4() { return &___Radius_4; }
	inline void set_Radius_4(int32_t value)
	{
		___Radius_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KUWAHARA_T240745109_H
#ifndef HUESATURATIONVALUE_T2444304774_H
#define HUESATURATIONVALUE_T2444304774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.HueSaturationValue
struct  HueSaturationValue_t2444304774  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.HueSaturationValue::MasterHue
	float ___MasterHue_4;
	// System.Single Colorful.HueSaturationValue::MasterSaturation
	float ___MasterSaturation_5;
	// System.Single Colorful.HueSaturationValue::MasterValue
	float ___MasterValue_6;
	// System.Single Colorful.HueSaturationValue::RedsHue
	float ___RedsHue_7;
	// System.Single Colorful.HueSaturationValue::RedsSaturation
	float ___RedsSaturation_8;
	// System.Single Colorful.HueSaturationValue::RedsValue
	float ___RedsValue_9;
	// System.Single Colorful.HueSaturationValue::YellowsHue
	float ___YellowsHue_10;
	// System.Single Colorful.HueSaturationValue::YellowsSaturation
	float ___YellowsSaturation_11;
	// System.Single Colorful.HueSaturationValue::YellowsValue
	float ___YellowsValue_12;
	// System.Single Colorful.HueSaturationValue::GreensHue
	float ___GreensHue_13;
	// System.Single Colorful.HueSaturationValue::GreensSaturation
	float ___GreensSaturation_14;
	// System.Single Colorful.HueSaturationValue::GreensValue
	float ___GreensValue_15;
	// System.Single Colorful.HueSaturationValue::CyansHue
	float ___CyansHue_16;
	// System.Single Colorful.HueSaturationValue::CyansSaturation
	float ___CyansSaturation_17;
	// System.Single Colorful.HueSaturationValue::CyansValue
	float ___CyansValue_18;
	// System.Single Colorful.HueSaturationValue::BluesHue
	float ___BluesHue_19;
	// System.Single Colorful.HueSaturationValue::BluesSaturation
	float ___BluesSaturation_20;
	// System.Single Colorful.HueSaturationValue::BluesValue
	float ___BluesValue_21;
	// System.Single Colorful.HueSaturationValue::MagentasHue
	float ___MagentasHue_22;
	// System.Single Colorful.HueSaturationValue::MagentasSaturation
	float ___MagentasSaturation_23;
	// System.Single Colorful.HueSaturationValue::MagentasValue
	float ___MagentasValue_24;
	// System.Boolean Colorful.HueSaturationValue::AdvancedMode
	bool ___AdvancedMode_25;

public:
	inline static int32_t get_offset_of_MasterHue_4() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___MasterHue_4)); }
	inline float get_MasterHue_4() const { return ___MasterHue_4; }
	inline float* get_address_of_MasterHue_4() { return &___MasterHue_4; }
	inline void set_MasterHue_4(float value)
	{
		___MasterHue_4 = value;
	}

	inline static int32_t get_offset_of_MasterSaturation_5() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___MasterSaturation_5)); }
	inline float get_MasterSaturation_5() const { return ___MasterSaturation_5; }
	inline float* get_address_of_MasterSaturation_5() { return &___MasterSaturation_5; }
	inline void set_MasterSaturation_5(float value)
	{
		___MasterSaturation_5 = value;
	}

	inline static int32_t get_offset_of_MasterValue_6() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___MasterValue_6)); }
	inline float get_MasterValue_6() const { return ___MasterValue_6; }
	inline float* get_address_of_MasterValue_6() { return &___MasterValue_6; }
	inline void set_MasterValue_6(float value)
	{
		___MasterValue_6 = value;
	}

	inline static int32_t get_offset_of_RedsHue_7() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___RedsHue_7)); }
	inline float get_RedsHue_7() const { return ___RedsHue_7; }
	inline float* get_address_of_RedsHue_7() { return &___RedsHue_7; }
	inline void set_RedsHue_7(float value)
	{
		___RedsHue_7 = value;
	}

	inline static int32_t get_offset_of_RedsSaturation_8() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___RedsSaturation_8)); }
	inline float get_RedsSaturation_8() const { return ___RedsSaturation_8; }
	inline float* get_address_of_RedsSaturation_8() { return &___RedsSaturation_8; }
	inline void set_RedsSaturation_8(float value)
	{
		___RedsSaturation_8 = value;
	}

	inline static int32_t get_offset_of_RedsValue_9() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___RedsValue_9)); }
	inline float get_RedsValue_9() const { return ___RedsValue_9; }
	inline float* get_address_of_RedsValue_9() { return &___RedsValue_9; }
	inline void set_RedsValue_9(float value)
	{
		___RedsValue_9 = value;
	}

	inline static int32_t get_offset_of_YellowsHue_10() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___YellowsHue_10)); }
	inline float get_YellowsHue_10() const { return ___YellowsHue_10; }
	inline float* get_address_of_YellowsHue_10() { return &___YellowsHue_10; }
	inline void set_YellowsHue_10(float value)
	{
		___YellowsHue_10 = value;
	}

	inline static int32_t get_offset_of_YellowsSaturation_11() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___YellowsSaturation_11)); }
	inline float get_YellowsSaturation_11() const { return ___YellowsSaturation_11; }
	inline float* get_address_of_YellowsSaturation_11() { return &___YellowsSaturation_11; }
	inline void set_YellowsSaturation_11(float value)
	{
		___YellowsSaturation_11 = value;
	}

	inline static int32_t get_offset_of_YellowsValue_12() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___YellowsValue_12)); }
	inline float get_YellowsValue_12() const { return ___YellowsValue_12; }
	inline float* get_address_of_YellowsValue_12() { return &___YellowsValue_12; }
	inline void set_YellowsValue_12(float value)
	{
		___YellowsValue_12 = value;
	}

	inline static int32_t get_offset_of_GreensHue_13() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___GreensHue_13)); }
	inline float get_GreensHue_13() const { return ___GreensHue_13; }
	inline float* get_address_of_GreensHue_13() { return &___GreensHue_13; }
	inline void set_GreensHue_13(float value)
	{
		___GreensHue_13 = value;
	}

	inline static int32_t get_offset_of_GreensSaturation_14() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___GreensSaturation_14)); }
	inline float get_GreensSaturation_14() const { return ___GreensSaturation_14; }
	inline float* get_address_of_GreensSaturation_14() { return &___GreensSaturation_14; }
	inline void set_GreensSaturation_14(float value)
	{
		___GreensSaturation_14 = value;
	}

	inline static int32_t get_offset_of_GreensValue_15() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___GreensValue_15)); }
	inline float get_GreensValue_15() const { return ___GreensValue_15; }
	inline float* get_address_of_GreensValue_15() { return &___GreensValue_15; }
	inline void set_GreensValue_15(float value)
	{
		___GreensValue_15 = value;
	}

	inline static int32_t get_offset_of_CyansHue_16() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___CyansHue_16)); }
	inline float get_CyansHue_16() const { return ___CyansHue_16; }
	inline float* get_address_of_CyansHue_16() { return &___CyansHue_16; }
	inline void set_CyansHue_16(float value)
	{
		___CyansHue_16 = value;
	}

	inline static int32_t get_offset_of_CyansSaturation_17() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___CyansSaturation_17)); }
	inline float get_CyansSaturation_17() const { return ___CyansSaturation_17; }
	inline float* get_address_of_CyansSaturation_17() { return &___CyansSaturation_17; }
	inline void set_CyansSaturation_17(float value)
	{
		___CyansSaturation_17 = value;
	}

	inline static int32_t get_offset_of_CyansValue_18() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___CyansValue_18)); }
	inline float get_CyansValue_18() const { return ___CyansValue_18; }
	inline float* get_address_of_CyansValue_18() { return &___CyansValue_18; }
	inline void set_CyansValue_18(float value)
	{
		___CyansValue_18 = value;
	}

	inline static int32_t get_offset_of_BluesHue_19() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___BluesHue_19)); }
	inline float get_BluesHue_19() const { return ___BluesHue_19; }
	inline float* get_address_of_BluesHue_19() { return &___BluesHue_19; }
	inline void set_BluesHue_19(float value)
	{
		___BluesHue_19 = value;
	}

	inline static int32_t get_offset_of_BluesSaturation_20() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___BluesSaturation_20)); }
	inline float get_BluesSaturation_20() const { return ___BluesSaturation_20; }
	inline float* get_address_of_BluesSaturation_20() { return &___BluesSaturation_20; }
	inline void set_BluesSaturation_20(float value)
	{
		___BluesSaturation_20 = value;
	}

	inline static int32_t get_offset_of_BluesValue_21() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___BluesValue_21)); }
	inline float get_BluesValue_21() const { return ___BluesValue_21; }
	inline float* get_address_of_BluesValue_21() { return &___BluesValue_21; }
	inline void set_BluesValue_21(float value)
	{
		___BluesValue_21 = value;
	}

	inline static int32_t get_offset_of_MagentasHue_22() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___MagentasHue_22)); }
	inline float get_MagentasHue_22() const { return ___MagentasHue_22; }
	inline float* get_address_of_MagentasHue_22() { return &___MagentasHue_22; }
	inline void set_MagentasHue_22(float value)
	{
		___MagentasHue_22 = value;
	}

	inline static int32_t get_offset_of_MagentasSaturation_23() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___MagentasSaturation_23)); }
	inline float get_MagentasSaturation_23() const { return ___MagentasSaturation_23; }
	inline float* get_address_of_MagentasSaturation_23() { return &___MagentasSaturation_23; }
	inline void set_MagentasSaturation_23(float value)
	{
		___MagentasSaturation_23 = value;
	}

	inline static int32_t get_offset_of_MagentasValue_24() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___MagentasValue_24)); }
	inline float get_MagentasValue_24() const { return ___MagentasValue_24; }
	inline float* get_address_of_MagentasValue_24() { return &___MagentasValue_24; }
	inline void set_MagentasValue_24(float value)
	{
		___MagentasValue_24 = value;
	}

	inline static int32_t get_offset_of_AdvancedMode_25() { return static_cast<int32_t>(offsetof(HueSaturationValue_t2444304774, ___AdvancedMode_25)); }
	inline bool get_AdvancedMode_25() const { return ___AdvancedMode_25; }
	inline bool* get_address_of_AdvancedMode_25() { return &___AdvancedMode_25; }
	inline void set_AdvancedMode_25(bool value)
	{
		___AdvancedMode_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HUESATURATIONVALUE_T2444304774_H
#ifndef HUEFOCUS_T3289091382_H
#define HUEFOCUS_T3289091382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.HueFocus
struct  HueFocus_t3289091382  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.HueFocus::Hue
	float ___Hue_4;
	// System.Single Colorful.HueFocus::Range
	float ___Range_5;
	// System.Single Colorful.HueFocus::Boost
	float ___Boost_6;
	// System.Single Colorful.HueFocus::Amount
	float ___Amount_7;

public:
	inline static int32_t get_offset_of_Hue_4() { return static_cast<int32_t>(offsetof(HueFocus_t3289091382, ___Hue_4)); }
	inline float get_Hue_4() const { return ___Hue_4; }
	inline float* get_address_of_Hue_4() { return &___Hue_4; }
	inline void set_Hue_4(float value)
	{
		___Hue_4 = value;
	}

	inline static int32_t get_offset_of_Range_5() { return static_cast<int32_t>(offsetof(HueFocus_t3289091382, ___Range_5)); }
	inline float get_Range_5() const { return ___Range_5; }
	inline float* get_address_of_Range_5() { return &___Range_5; }
	inline void set_Range_5(float value)
	{
		___Range_5 = value;
	}

	inline static int32_t get_offset_of_Boost_6() { return static_cast<int32_t>(offsetof(HueFocus_t3289091382, ___Boost_6)); }
	inline float get_Boost_6() const { return ___Boost_6; }
	inline float* get_address_of_Boost_6() { return &___Boost_6; }
	inline void set_Boost_6(float value)
	{
		___Boost_6 = value;
	}

	inline static int32_t get_offset_of_Amount_7() { return static_cast<int32_t>(offsetof(HueFocus_t3289091382, ___Amount_7)); }
	inline float get_Amount_7() const { return ___Amount_7; }
	inline float* get_address_of_Amount_7() { return &___Amount_7; }
	inline void set_Amount_7(float value)
	{
		___Amount_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HUEFOCUS_T3289091382_H
#ifndef GAUSSIANBLUR_T3820976243_H
#define GAUSSIANBLUR_T3820976243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.GaussianBlur
struct  GaussianBlur_t3820976243  : public BaseEffect_t1187847871
{
public:
	// System.Int32 Colorful.GaussianBlur::Passes
	int32_t ___Passes_4;
	// System.Single Colorful.GaussianBlur::Downscaling
	float ___Downscaling_5;
	// System.Single Colorful.GaussianBlur::Amount
	float ___Amount_6;

public:
	inline static int32_t get_offset_of_Passes_4() { return static_cast<int32_t>(offsetof(GaussianBlur_t3820976243, ___Passes_4)); }
	inline int32_t get_Passes_4() const { return ___Passes_4; }
	inline int32_t* get_address_of_Passes_4() { return &___Passes_4; }
	inline void set_Passes_4(int32_t value)
	{
		___Passes_4 = value;
	}

	inline static int32_t get_offset_of_Downscaling_5() { return static_cast<int32_t>(offsetof(GaussianBlur_t3820976243, ___Downscaling_5)); }
	inline float get_Downscaling_5() const { return ___Downscaling_5; }
	inline float* get_address_of_Downscaling_5() { return &___Downscaling_5; }
	inline void set_Downscaling_5(float value)
	{
		___Downscaling_5 = value;
	}

	inline static int32_t get_offset_of_Amount_6() { return static_cast<int32_t>(offsetof(GaussianBlur_t3820976243, ___Amount_6)); }
	inline float get_Amount_6() const { return ___Amount_6; }
	inline float* get_address_of_Amount_6() { return &___Amount_6; }
	inline void set_Amount_6(float value)
	{
		___Amount_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAUSSIANBLUR_T3820976243_H
#ifndef CHROMATICABERRATION_T2529873745_H
#define CHROMATICABERRATION_T2529873745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.ChromaticAberration
struct  ChromaticAberration_t2529873745  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.ChromaticAberration::RedRefraction
	float ___RedRefraction_4;
	// System.Single Colorful.ChromaticAberration::GreenRefraction
	float ___GreenRefraction_5;
	// System.Single Colorful.ChromaticAberration::BlueRefraction
	float ___BlueRefraction_6;
	// System.Boolean Colorful.ChromaticAberration::PreserveAlpha
	bool ___PreserveAlpha_7;

public:
	inline static int32_t get_offset_of_RedRefraction_4() { return static_cast<int32_t>(offsetof(ChromaticAberration_t2529873745, ___RedRefraction_4)); }
	inline float get_RedRefraction_4() const { return ___RedRefraction_4; }
	inline float* get_address_of_RedRefraction_4() { return &___RedRefraction_4; }
	inline void set_RedRefraction_4(float value)
	{
		___RedRefraction_4 = value;
	}

	inline static int32_t get_offset_of_GreenRefraction_5() { return static_cast<int32_t>(offsetof(ChromaticAberration_t2529873745, ___GreenRefraction_5)); }
	inline float get_GreenRefraction_5() const { return ___GreenRefraction_5; }
	inline float* get_address_of_GreenRefraction_5() { return &___GreenRefraction_5; }
	inline void set_GreenRefraction_5(float value)
	{
		___GreenRefraction_5 = value;
	}

	inline static int32_t get_offset_of_BlueRefraction_6() { return static_cast<int32_t>(offsetof(ChromaticAberration_t2529873745, ___BlueRefraction_6)); }
	inline float get_BlueRefraction_6() const { return ___BlueRefraction_6; }
	inline float* get_address_of_BlueRefraction_6() { return &___BlueRefraction_6; }
	inline void set_BlueRefraction_6(float value)
	{
		___BlueRefraction_6 = value;
	}

	inline static int32_t get_offset_of_PreserveAlpha_7() { return static_cast<int32_t>(offsetof(ChromaticAberration_t2529873745, ___PreserveAlpha_7)); }
	inline bool get_PreserveAlpha_7() const { return ___PreserveAlpha_7; }
	inline bool* get_address_of_PreserveAlpha_7() { return &___PreserveAlpha_7; }
	inline void set_PreserveAlpha_7(bool value)
	{
		___PreserveAlpha_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHROMATICABERRATION_T2529873745_H
#ifndef COMICBOOK_T2243422766_H
#define COMICBOOK_T2243422766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.ComicBook
struct  ComicBook_t2243422766  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.ComicBook::StripAngle
	float ___StripAngle_4;
	// System.Single Colorful.ComicBook::StripDensity
	float ___StripDensity_5;
	// System.Single Colorful.ComicBook::StripThickness
	float ___StripThickness_6;
	// UnityEngine.Vector2 Colorful.ComicBook::StripLimits
	Vector2_t2156229523  ___StripLimits_7;
	// UnityEngine.Color Colorful.ComicBook::StripInnerColor
	Color_t2555686324  ___StripInnerColor_8;
	// UnityEngine.Color Colorful.ComicBook::StripOuterColor
	Color_t2555686324  ___StripOuterColor_9;
	// UnityEngine.Color Colorful.ComicBook::FillColor
	Color_t2555686324  ___FillColor_10;
	// UnityEngine.Color Colorful.ComicBook::BackgroundColor
	Color_t2555686324  ___BackgroundColor_11;
	// System.Boolean Colorful.ComicBook::EdgeDetection
	bool ___EdgeDetection_12;
	// System.Single Colorful.ComicBook::EdgeThreshold
	float ___EdgeThreshold_13;
	// UnityEngine.Color Colorful.ComicBook::EdgeColor
	Color_t2555686324  ___EdgeColor_14;
	// System.Single Colorful.ComicBook::Amount
	float ___Amount_15;

public:
	inline static int32_t get_offset_of_StripAngle_4() { return static_cast<int32_t>(offsetof(ComicBook_t2243422766, ___StripAngle_4)); }
	inline float get_StripAngle_4() const { return ___StripAngle_4; }
	inline float* get_address_of_StripAngle_4() { return &___StripAngle_4; }
	inline void set_StripAngle_4(float value)
	{
		___StripAngle_4 = value;
	}

	inline static int32_t get_offset_of_StripDensity_5() { return static_cast<int32_t>(offsetof(ComicBook_t2243422766, ___StripDensity_5)); }
	inline float get_StripDensity_5() const { return ___StripDensity_5; }
	inline float* get_address_of_StripDensity_5() { return &___StripDensity_5; }
	inline void set_StripDensity_5(float value)
	{
		___StripDensity_5 = value;
	}

	inline static int32_t get_offset_of_StripThickness_6() { return static_cast<int32_t>(offsetof(ComicBook_t2243422766, ___StripThickness_6)); }
	inline float get_StripThickness_6() const { return ___StripThickness_6; }
	inline float* get_address_of_StripThickness_6() { return &___StripThickness_6; }
	inline void set_StripThickness_6(float value)
	{
		___StripThickness_6 = value;
	}

	inline static int32_t get_offset_of_StripLimits_7() { return static_cast<int32_t>(offsetof(ComicBook_t2243422766, ___StripLimits_7)); }
	inline Vector2_t2156229523  get_StripLimits_7() const { return ___StripLimits_7; }
	inline Vector2_t2156229523 * get_address_of_StripLimits_7() { return &___StripLimits_7; }
	inline void set_StripLimits_7(Vector2_t2156229523  value)
	{
		___StripLimits_7 = value;
	}

	inline static int32_t get_offset_of_StripInnerColor_8() { return static_cast<int32_t>(offsetof(ComicBook_t2243422766, ___StripInnerColor_8)); }
	inline Color_t2555686324  get_StripInnerColor_8() const { return ___StripInnerColor_8; }
	inline Color_t2555686324 * get_address_of_StripInnerColor_8() { return &___StripInnerColor_8; }
	inline void set_StripInnerColor_8(Color_t2555686324  value)
	{
		___StripInnerColor_8 = value;
	}

	inline static int32_t get_offset_of_StripOuterColor_9() { return static_cast<int32_t>(offsetof(ComicBook_t2243422766, ___StripOuterColor_9)); }
	inline Color_t2555686324  get_StripOuterColor_9() const { return ___StripOuterColor_9; }
	inline Color_t2555686324 * get_address_of_StripOuterColor_9() { return &___StripOuterColor_9; }
	inline void set_StripOuterColor_9(Color_t2555686324  value)
	{
		___StripOuterColor_9 = value;
	}

	inline static int32_t get_offset_of_FillColor_10() { return static_cast<int32_t>(offsetof(ComicBook_t2243422766, ___FillColor_10)); }
	inline Color_t2555686324  get_FillColor_10() const { return ___FillColor_10; }
	inline Color_t2555686324 * get_address_of_FillColor_10() { return &___FillColor_10; }
	inline void set_FillColor_10(Color_t2555686324  value)
	{
		___FillColor_10 = value;
	}

	inline static int32_t get_offset_of_BackgroundColor_11() { return static_cast<int32_t>(offsetof(ComicBook_t2243422766, ___BackgroundColor_11)); }
	inline Color_t2555686324  get_BackgroundColor_11() const { return ___BackgroundColor_11; }
	inline Color_t2555686324 * get_address_of_BackgroundColor_11() { return &___BackgroundColor_11; }
	inline void set_BackgroundColor_11(Color_t2555686324  value)
	{
		___BackgroundColor_11 = value;
	}

	inline static int32_t get_offset_of_EdgeDetection_12() { return static_cast<int32_t>(offsetof(ComicBook_t2243422766, ___EdgeDetection_12)); }
	inline bool get_EdgeDetection_12() const { return ___EdgeDetection_12; }
	inline bool* get_address_of_EdgeDetection_12() { return &___EdgeDetection_12; }
	inline void set_EdgeDetection_12(bool value)
	{
		___EdgeDetection_12 = value;
	}

	inline static int32_t get_offset_of_EdgeThreshold_13() { return static_cast<int32_t>(offsetof(ComicBook_t2243422766, ___EdgeThreshold_13)); }
	inline float get_EdgeThreshold_13() const { return ___EdgeThreshold_13; }
	inline float* get_address_of_EdgeThreshold_13() { return &___EdgeThreshold_13; }
	inline void set_EdgeThreshold_13(float value)
	{
		___EdgeThreshold_13 = value;
	}

	inline static int32_t get_offset_of_EdgeColor_14() { return static_cast<int32_t>(offsetof(ComicBook_t2243422766, ___EdgeColor_14)); }
	inline Color_t2555686324  get_EdgeColor_14() const { return ___EdgeColor_14; }
	inline Color_t2555686324 * get_address_of_EdgeColor_14() { return &___EdgeColor_14; }
	inline void set_EdgeColor_14(Color_t2555686324  value)
	{
		___EdgeColor_14 = value;
	}

	inline static int32_t get_offset_of_Amount_15() { return static_cast<int32_t>(offsetof(ComicBook_t2243422766, ___Amount_15)); }
	inline float get_Amount_15() const { return ___Amount_15; }
	inline float* get_address_of_Amount_15() { return &___Amount_15; }
	inline void set_Amount_15(float value)
	{
		___Amount_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMICBOOK_T2243422766_H
#ifndef CONTRASTGAIN_T1677431729_H
#define CONTRASTGAIN_T1677431729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.ContrastGain
struct  ContrastGain_t1677431729  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.ContrastGain::Gain
	float ___Gain_4;

public:
	inline static int32_t get_offset_of_Gain_4() { return static_cast<int32_t>(offsetof(ContrastGain_t1677431729, ___Gain_4)); }
	inline float get_Gain_4() const { return ___Gain_4; }
	inline float* get_address_of_Gain_4() { return &___Gain_4; }
	inline void set_Gain_4(float value)
	{
		___Gain_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTRASTGAIN_T1677431729_H
#ifndef CONTRASTVIGNETTE_T2834033980_H
#define CONTRASTVIGNETTE_T2834033980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.ContrastVignette
struct  ContrastVignette_t2834033980  : public BaseEffect_t1187847871
{
public:
	// UnityEngine.Vector2 Colorful.ContrastVignette::Center
	Vector2_t2156229523  ___Center_4;
	// System.Single Colorful.ContrastVignette::Sharpness
	float ___Sharpness_5;
	// System.Single Colorful.ContrastVignette::Darkness
	float ___Darkness_6;
	// System.Single Colorful.ContrastVignette::Contrast
	float ___Contrast_7;
	// UnityEngine.Vector3 Colorful.ContrastVignette::ContrastCoeff
	Vector3_t3722313464  ___ContrastCoeff_8;
	// System.Single Colorful.ContrastVignette::EdgeBlending
	float ___EdgeBlending_9;

public:
	inline static int32_t get_offset_of_Center_4() { return static_cast<int32_t>(offsetof(ContrastVignette_t2834033980, ___Center_4)); }
	inline Vector2_t2156229523  get_Center_4() const { return ___Center_4; }
	inline Vector2_t2156229523 * get_address_of_Center_4() { return &___Center_4; }
	inline void set_Center_4(Vector2_t2156229523  value)
	{
		___Center_4 = value;
	}

	inline static int32_t get_offset_of_Sharpness_5() { return static_cast<int32_t>(offsetof(ContrastVignette_t2834033980, ___Sharpness_5)); }
	inline float get_Sharpness_5() const { return ___Sharpness_5; }
	inline float* get_address_of_Sharpness_5() { return &___Sharpness_5; }
	inline void set_Sharpness_5(float value)
	{
		___Sharpness_5 = value;
	}

	inline static int32_t get_offset_of_Darkness_6() { return static_cast<int32_t>(offsetof(ContrastVignette_t2834033980, ___Darkness_6)); }
	inline float get_Darkness_6() const { return ___Darkness_6; }
	inline float* get_address_of_Darkness_6() { return &___Darkness_6; }
	inline void set_Darkness_6(float value)
	{
		___Darkness_6 = value;
	}

	inline static int32_t get_offset_of_Contrast_7() { return static_cast<int32_t>(offsetof(ContrastVignette_t2834033980, ___Contrast_7)); }
	inline float get_Contrast_7() const { return ___Contrast_7; }
	inline float* get_address_of_Contrast_7() { return &___Contrast_7; }
	inline void set_Contrast_7(float value)
	{
		___Contrast_7 = value;
	}

	inline static int32_t get_offset_of_ContrastCoeff_8() { return static_cast<int32_t>(offsetof(ContrastVignette_t2834033980, ___ContrastCoeff_8)); }
	inline Vector3_t3722313464  get_ContrastCoeff_8() const { return ___ContrastCoeff_8; }
	inline Vector3_t3722313464 * get_address_of_ContrastCoeff_8() { return &___ContrastCoeff_8; }
	inline void set_ContrastCoeff_8(Vector3_t3722313464  value)
	{
		___ContrastCoeff_8 = value;
	}

	inline static int32_t get_offset_of_EdgeBlending_9() { return static_cast<int32_t>(offsetof(ContrastVignette_t2834033980, ___EdgeBlending_9)); }
	inline float get_EdgeBlending_9() const { return ___EdgeBlending_9; }
	inline float* get_address_of_EdgeBlending_9() { return &___EdgeBlending_9; }
	inline void set_EdgeBlending_9(float value)
	{
		___EdgeBlending_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTRASTVIGNETTE_T2834033980_H
#ifndef CONVOLUTION3X3_T1353237119_H
#define CONVOLUTION3X3_T1353237119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Convolution3x3
struct  Convolution3x3_t1353237119  : public BaseEffect_t1187847871
{
public:
	// UnityEngine.Vector3 Colorful.Convolution3x3::KernelTop
	Vector3_t3722313464  ___KernelTop_4;
	// UnityEngine.Vector3 Colorful.Convolution3x3::KernelMiddle
	Vector3_t3722313464  ___KernelMiddle_5;
	// UnityEngine.Vector3 Colorful.Convolution3x3::KernelBottom
	Vector3_t3722313464  ___KernelBottom_6;
	// System.Single Colorful.Convolution3x3::Divisor
	float ___Divisor_7;
	// System.Single Colorful.Convolution3x3::Amount
	float ___Amount_8;

public:
	inline static int32_t get_offset_of_KernelTop_4() { return static_cast<int32_t>(offsetof(Convolution3x3_t1353237119, ___KernelTop_4)); }
	inline Vector3_t3722313464  get_KernelTop_4() const { return ___KernelTop_4; }
	inline Vector3_t3722313464 * get_address_of_KernelTop_4() { return &___KernelTop_4; }
	inline void set_KernelTop_4(Vector3_t3722313464  value)
	{
		___KernelTop_4 = value;
	}

	inline static int32_t get_offset_of_KernelMiddle_5() { return static_cast<int32_t>(offsetof(Convolution3x3_t1353237119, ___KernelMiddle_5)); }
	inline Vector3_t3722313464  get_KernelMiddle_5() const { return ___KernelMiddle_5; }
	inline Vector3_t3722313464 * get_address_of_KernelMiddle_5() { return &___KernelMiddle_5; }
	inline void set_KernelMiddle_5(Vector3_t3722313464  value)
	{
		___KernelMiddle_5 = value;
	}

	inline static int32_t get_offset_of_KernelBottom_6() { return static_cast<int32_t>(offsetof(Convolution3x3_t1353237119, ___KernelBottom_6)); }
	inline Vector3_t3722313464  get_KernelBottom_6() const { return ___KernelBottom_6; }
	inline Vector3_t3722313464 * get_address_of_KernelBottom_6() { return &___KernelBottom_6; }
	inline void set_KernelBottom_6(Vector3_t3722313464  value)
	{
		___KernelBottom_6 = value;
	}

	inline static int32_t get_offset_of_Divisor_7() { return static_cast<int32_t>(offsetof(Convolution3x3_t1353237119, ___Divisor_7)); }
	inline float get_Divisor_7() const { return ___Divisor_7; }
	inline float* get_address_of_Divisor_7() { return &___Divisor_7; }
	inline void set_Divisor_7(float value)
	{
		___Divisor_7 = value;
	}

	inline static int32_t get_offset_of_Amount_8() { return static_cast<int32_t>(offsetof(Convolution3x3_t1353237119, ___Amount_8)); }
	inline float get_Amount_8() const { return ___Amount_8; }
	inline float* get_address_of_Amount_8() { return &___Amount_8; }
	inline void set_Amount_8(float value)
	{
		___Amount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVOLUTION3X3_T1353237119_H
#ifndef CROSSSTITCH_T1042481064_H
#define CROSSSTITCH_T1042481064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.CrossStitch
struct  CrossStitch_t1042481064  : public BaseEffect_t1187847871
{
public:
	// System.Int32 Colorful.CrossStitch::Size
	int32_t ___Size_4;
	// System.Single Colorful.CrossStitch::Brightness
	float ___Brightness_5;
	// System.Boolean Colorful.CrossStitch::Invert
	bool ___Invert_6;
	// System.Boolean Colorful.CrossStitch::Pixelize
	bool ___Pixelize_7;

public:
	inline static int32_t get_offset_of_Size_4() { return static_cast<int32_t>(offsetof(CrossStitch_t1042481064, ___Size_4)); }
	inline int32_t get_Size_4() const { return ___Size_4; }
	inline int32_t* get_address_of_Size_4() { return &___Size_4; }
	inline void set_Size_4(int32_t value)
	{
		___Size_4 = value;
	}

	inline static int32_t get_offset_of_Brightness_5() { return static_cast<int32_t>(offsetof(CrossStitch_t1042481064, ___Brightness_5)); }
	inline float get_Brightness_5() const { return ___Brightness_5; }
	inline float* get_address_of_Brightness_5() { return &___Brightness_5; }
	inline void set_Brightness_5(float value)
	{
		___Brightness_5 = value;
	}

	inline static int32_t get_offset_of_Invert_6() { return static_cast<int32_t>(offsetof(CrossStitch_t1042481064, ___Invert_6)); }
	inline bool get_Invert_6() const { return ___Invert_6; }
	inline bool* get_address_of_Invert_6() { return &___Invert_6; }
	inline void set_Invert_6(bool value)
	{
		___Invert_6 = value;
	}

	inline static int32_t get_offset_of_Pixelize_7() { return static_cast<int32_t>(offsetof(CrossStitch_t1042481064, ___Pixelize_7)); }
	inline bool get_Pixelize_7() const { return ___Pixelize_7; }
	inline bool* get_address_of_Pixelize_7() { return &___Pixelize_7; }
	inline void set_Pixelize_7(bool value)
	{
		___Pixelize_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CROSSSTITCH_T1042481064_H
#ifndef FROST_T3645176307_H
#define FROST_T3645176307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Frost
struct  Frost_t3645176307  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.Frost::Scale
	float ___Scale_4;
	// System.Single Colorful.Frost::Sharpness
	float ___Sharpness_5;
	// System.Single Colorful.Frost::Darkness
	float ___Darkness_6;
	// System.Boolean Colorful.Frost::EnableVignette
	bool ___EnableVignette_7;

public:
	inline static int32_t get_offset_of_Scale_4() { return static_cast<int32_t>(offsetof(Frost_t3645176307, ___Scale_4)); }
	inline float get_Scale_4() const { return ___Scale_4; }
	inline float* get_address_of_Scale_4() { return &___Scale_4; }
	inline void set_Scale_4(float value)
	{
		___Scale_4 = value;
	}

	inline static int32_t get_offset_of_Sharpness_5() { return static_cast<int32_t>(offsetof(Frost_t3645176307, ___Sharpness_5)); }
	inline float get_Sharpness_5() const { return ___Sharpness_5; }
	inline float* get_address_of_Sharpness_5() { return &___Sharpness_5; }
	inline void set_Sharpness_5(float value)
	{
		___Sharpness_5 = value;
	}

	inline static int32_t get_offset_of_Darkness_6() { return static_cast<int32_t>(offsetof(Frost_t3645176307, ___Darkness_6)); }
	inline float get_Darkness_6() const { return ___Darkness_6; }
	inline float* get_address_of_Darkness_6() { return &___Darkness_6; }
	inline void set_Darkness_6(float value)
	{
		___Darkness_6 = value;
	}

	inline static int32_t get_offset_of_EnableVignette_7() { return static_cast<int32_t>(offsetof(Frost_t3645176307, ___EnableVignette_7)); }
	inline bool get_EnableVignette_7() const { return ___EnableVignette_7; }
	inline bool* get_address_of_EnableVignette_7() { return &___EnableVignette_7; }
	inline void set_EnableVignette_7(bool value)
	{
		___EnableVignette_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FROST_T3645176307_H
#ifndef FASTVIGNETTE_T1694713166_H
#define FASTVIGNETTE_T1694713166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.FastVignette
struct  FastVignette_t1694713166  : public BaseEffect_t1187847871
{
public:
	// Colorful.FastVignette/ColorMode Colorful.FastVignette::Mode
	int32_t ___Mode_4;
	// UnityEngine.Color Colorful.FastVignette::Color
	Color_t2555686324  ___Color_5;
	// UnityEngine.Vector2 Colorful.FastVignette::Center
	Vector2_t2156229523  ___Center_6;
	// System.Single Colorful.FastVignette::Sharpness
	float ___Sharpness_7;
	// System.Single Colorful.FastVignette::Darkness
	float ___Darkness_8;

public:
	inline static int32_t get_offset_of_Mode_4() { return static_cast<int32_t>(offsetof(FastVignette_t1694713166, ___Mode_4)); }
	inline int32_t get_Mode_4() const { return ___Mode_4; }
	inline int32_t* get_address_of_Mode_4() { return &___Mode_4; }
	inline void set_Mode_4(int32_t value)
	{
		___Mode_4 = value;
	}

	inline static int32_t get_offset_of_Color_5() { return static_cast<int32_t>(offsetof(FastVignette_t1694713166, ___Color_5)); }
	inline Color_t2555686324  get_Color_5() const { return ___Color_5; }
	inline Color_t2555686324 * get_address_of_Color_5() { return &___Color_5; }
	inline void set_Color_5(Color_t2555686324  value)
	{
		___Color_5 = value;
	}

	inline static int32_t get_offset_of_Center_6() { return static_cast<int32_t>(offsetof(FastVignette_t1694713166, ___Center_6)); }
	inline Vector2_t2156229523  get_Center_6() const { return ___Center_6; }
	inline Vector2_t2156229523 * get_address_of_Center_6() { return &___Center_6; }
	inline void set_Center_6(Vector2_t2156229523  value)
	{
		___Center_6 = value;
	}

	inline static int32_t get_offset_of_Sharpness_7() { return static_cast<int32_t>(offsetof(FastVignette_t1694713166, ___Sharpness_7)); }
	inline float get_Sharpness_7() const { return ___Sharpness_7; }
	inline float* get_address_of_Sharpness_7() { return &___Sharpness_7; }
	inline void set_Sharpness_7(float value)
	{
		___Sharpness_7 = value;
	}

	inline static int32_t get_offset_of_Darkness_8() { return static_cast<int32_t>(offsetof(FastVignette_t1694713166, ___Darkness_8)); }
	inline float get_Darkness_8() const { return ___Darkness_8; }
	inline float* get_address_of_Darkness_8() { return &___Darkness_8; }
	inline void set_Darkness_8(float value)
	{
		___Darkness_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FASTVIGNETTE_T1694713166_H
#ifndef DYNAMICLOOKUP_T4028714612_H
#define DYNAMICLOOKUP_T4028714612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.DynamicLookup
struct  DynamicLookup_t4028714612  : public BaseEffect_t1187847871
{
public:
	// UnityEngine.Color Colorful.DynamicLookup::White
	Color_t2555686324  ___White_4;
	// UnityEngine.Color Colorful.DynamicLookup::Black
	Color_t2555686324  ___Black_5;
	// UnityEngine.Color Colorful.DynamicLookup::Red
	Color_t2555686324  ___Red_6;
	// UnityEngine.Color Colorful.DynamicLookup::Green
	Color_t2555686324  ___Green_7;
	// UnityEngine.Color Colorful.DynamicLookup::Blue
	Color_t2555686324  ___Blue_8;
	// UnityEngine.Color Colorful.DynamicLookup::Yellow
	Color_t2555686324  ___Yellow_9;
	// UnityEngine.Color Colorful.DynamicLookup::Magenta
	Color_t2555686324  ___Magenta_10;
	// UnityEngine.Color Colorful.DynamicLookup::Cyan
	Color_t2555686324  ___Cyan_11;
	// System.Single Colorful.DynamicLookup::Amount
	float ___Amount_12;

public:
	inline static int32_t get_offset_of_White_4() { return static_cast<int32_t>(offsetof(DynamicLookup_t4028714612, ___White_4)); }
	inline Color_t2555686324  get_White_4() const { return ___White_4; }
	inline Color_t2555686324 * get_address_of_White_4() { return &___White_4; }
	inline void set_White_4(Color_t2555686324  value)
	{
		___White_4 = value;
	}

	inline static int32_t get_offset_of_Black_5() { return static_cast<int32_t>(offsetof(DynamicLookup_t4028714612, ___Black_5)); }
	inline Color_t2555686324  get_Black_5() const { return ___Black_5; }
	inline Color_t2555686324 * get_address_of_Black_5() { return &___Black_5; }
	inline void set_Black_5(Color_t2555686324  value)
	{
		___Black_5 = value;
	}

	inline static int32_t get_offset_of_Red_6() { return static_cast<int32_t>(offsetof(DynamicLookup_t4028714612, ___Red_6)); }
	inline Color_t2555686324  get_Red_6() const { return ___Red_6; }
	inline Color_t2555686324 * get_address_of_Red_6() { return &___Red_6; }
	inline void set_Red_6(Color_t2555686324  value)
	{
		___Red_6 = value;
	}

	inline static int32_t get_offset_of_Green_7() { return static_cast<int32_t>(offsetof(DynamicLookup_t4028714612, ___Green_7)); }
	inline Color_t2555686324  get_Green_7() const { return ___Green_7; }
	inline Color_t2555686324 * get_address_of_Green_7() { return &___Green_7; }
	inline void set_Green_7(Color_t2555686324  value)
	{
		___Green_7 = value;
	}

	inline static int32_t get_offset_of_Blue_8() { return static_cast<int32_t>(offsetof(DynamicLookup_t4028714612, ___Blue_8)); }
	inline Color_t2555686324  get_Blue_8() const { return ___Blue_8; }
	inline Color_t2555686324 * get_address_of_Blue_8() { return &___Blue_8; }
	inline void set_Blue_8(Color_t2555686324  value)
	{
		___Blue_8 = value;
	}

	inline static int32_t get_offset_of_Yellow_9() { return static_cast<int32_t>(offsetof(DynamicLookup_t4028714612, ___Yellow_9)); }
	inline Color_t2555686324  get_Yellow_9() const { return ___Yellow_9; }
	inline Color_t2555686324 * get_address_of_Yellow_9() { return &___Yellow_9; }
	inline void set_Yellow_9(Color_t2555686324  value)
	{
		___Yellow_9 = value;
	}

	inline static int32_t get_offset_of_Magenta_10() { return static_cast<int32_t>(offsetof(DynamicLookup_t4028714612, ___Magenta_10)); }
	inline Color_t2555686324  get_Magenta_10() const { return ___Magenta_10; }
	inline Color_t2555686324 * get_address_of_Magenta_10() { return &___Magenta_10; }
	inline void set_Magenta_10(Color_t2555686324  value)
	{
		___Magenta_10 = value;
	}

	inline static int32_t get_offset_of_Cyan_11() { return static_cast<int32_t>(offsetof(DynamicLookup_t4028714612, ___Cyan_11)); }
	inline Color_t2555686324  get_Cyan_11() const { return ___Cyan_11; }
	inline Color_t2555686324 * get_address_of_Cyan_11() { return &___Cyan_11; }
	inline void set_Cyan_11(Color_t2555686324  value)
	{
		___Cyan_11 = value;
	}

	inline static int32_t get_offset_of_Amount_12() { return static_cast<int32_t>(offsetof(DynamicLookup_t4028714612, ___Amount_12)); }
	inline float get_Amount_12() const { return ___Amount_12; }
	inline float* get_address_of_Amount_12() { return &___Amount_12; }
	inline void set_Amount_12(float value)
	{
		___Amount_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICLOOKUP_T4028714612_H
#ifndef DOUBLEVISION_T3179333181_H
#define DOUBLEVISION_T3179333181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.DoubleVision
struct  DoubleVision_t3179333181  : public BaseEffect_t1187847871
{
public:
	// UnityEngine.Vector2 Colorful.DoubleVision::Displace
	Vector2_t2156229523  ___Displace_4;
	// System.Single Colorful.DoubleVision::Amount
	float ___Amount_5;

public:
	inline static int32_t get_offset_of_Displace_4() { return static_cast<int32_t>(offsetof(DoubleVision_t3179333181, ___Displace_4)); }
	inline Vector2_t2156229523  get_Displace_4() const { return ___Displace_4; }
	inline Vector2_t2156229523 * get_address_of_Displace_4() { return &___Displace_4; }
	inline void set_Displace_4(Vector2_t2156229523  value)
	{
		___Displace_4 = value;
	}

	inline static int32_t get_offset_of_Amount_5() { return static_cast<int32_t>(offsetof(DoubleVision_t3179333181, ___Amount_5)); }
	inline float get_Amount_5() const { return ___Amount_5; }
	inline float* get_address_of_Amount_5() { return &___Amount_5; }
	inline void set_Amount_5(float value)
	{
		___Amount_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLEVISION_T3179333181_H
#ifndef DITHERING_T358002770_H
#define DITHERING_T358002770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Dithering
struct  Dithering_t358002770  : public BaseEffect_t1187847871
{
public:
	// System.Boolean Colorful.Dithering::ShowOriginal
	bool ___ShowOriginal_4;
	// System.Boolean Colorful.Dithering::ConvertToGrayscale
	bool ___ConvertToGrayscale_5;
	// System.Single Colorful.Dithering::RedLuminance
	float ___RedLuminance_6;
	// System.Single Colorful.Dithering::GreenLuminance
	float ___GreenLuminance_7;
	// System.Single Colorful.Dithering::BlueLuminance
	float ___BlueLuminance_8;
	// System.Single Colorful.Dithering::Amount
	float ___Amount_9;
	// UnityEngine.Texture2D Colorful.Dithering::m_DitherPattern
	Texture2D_t3840446185 * ___m_DitherPattern_10;

public:
	inline static int32_t get_offset_of_ShowOriginal_4() { return static_cast<int32_t>(offsetof(Dithering_t358002770, ___ShowOriginal_4)); }
	inline bool get_ShowOriginal_4() const { return ___ShowOriginal_4; }
	inline bool* get_address_of_ShowOriginal_4() { return &___ShowOriginal_4; }
	inline void set_ShowOriginal_4(bool value)
	{
		___ShowOriginal_4 = value;
	}

	inline static int32_t get_offset_of_ConvertToGrayscale_5() { return static_cast<int32_t>(offsetof(Dithering_t358002770, ___ConvertToGrayscale_5)); }
	inline bool get_ConvertToGrayscale_5() const { return ___ConvertToGrayscale_5; }
	inline bool* get_address_of_ConvertToGrayscale_5() { return &___ConvertToGrayscale_5; }
	inline void set_ConvertToGrayscale_5(bool value)
	{
		___ConvertToGrayscale_5 = value;
	}

	inline static int32_t get_offset_of_RedLuminance_6() { return static_cast<int32_t>(offsetof(Dithering_t358002770, ___RedLuminance_6)); }
	inline float get_RedLuminance_6() const { return ___RedLuminance_6; }
	inline float* get_address_of_RedLuminance_6() { return &___RedLuminance_6; }
	inline void set_RedLuminance_6(float value)
	{
		___RedLuminance_6 = value;
	}

	inline static int32_t get_offset_of_GreenLuminance_7() { return static_cast<int32_t>(offsetof(Dithering_t358002770, ___GreenLuminance_7)); }
	inline float get_GreenLuminance_7() const { return ___GreenLuminance_7; }
	inline float* get_address_of_GreenLuminance_7() { return &___GreenLuminance_7; }
	inline void set_GreenLuminance_7(float value)
	{
		___GreenLuminance_7 = value;
	}

	inline static int32_t get_offset_of_BlueLuminance_8() { return static_cast<int32_t>(offsetof(Dithering_t358002770, ___BlueLuminance_8)); }
	inline float get_BlueLuminance_8() const { return ___BlueLuminance_8; }
	inline float* get_address_of_BlueLuminance_8() { return &___BlueLuminance_8; }
	inline void set_BlueLuminance_8(float value)
	{
		___BlueLuminance_8 = value;
	}

	inline static int32_t get_offset_of_Amount_9() { return static_cast<int32_t>(offsetof(Dithering_t358002770, ___Amount_9)); }
	inline float get_Amount_9() const { return ___Amount_9; }
	inline float* get_address_of_Amount_9() { return &___Amount_9; }
	inline void set_Amount_9(float value)
	{
		___Amount_9 = value;
	}

	inline static int32_t get_offset_of_m_DitherPattern_10() { return static_cast<int32_t>(offsetof(Dithering_t358002770, ___m_DitherPattern_10)); }
	inline Texture2D_t3840446185 * get_m_DitherPattern_10() const { return ___m_DitherPattern_10; }
	inline Texture2D_t3840446185 ** get_address_of_m_DitherPattern_10() { return &___m_DitherPattern_10; }
	inline void set_m_DitherPattern_10(Texture2D_t3840446185 * value)
	{
		___m_DitherPattern_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_DitherPattern_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DITHERING_T358002770_H
#ifndef DIRECTIONALBLUR_T13325309_H
#define DIRECTIONALBLUR_T13325309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.DirectionalBlur
struct  DirectionalBlur_t13325309  : public BaseEffect_t1187847871
{
public:
	// Colorful.DirectionalBlur/QualityPreset Colorful.DirectionalBlur::Quality
	int32_t ___Quality_4;
	// System.Int32 Colorful.DirectionalBlur::Samples
	int32_t ___Samples_5;
	// System.Single Colorful.DirectionalBlur::Strength
	float ___Strength_6;
	// System.Single Colorful.DirectionalBlur::Angle
	float ___Angle_7;

public:
	inline static int32_t get_offset_of_Quality_4() { return static_cast<int32_t>(offsetof(DirectionalBlur_t13325309, ___Quality_4)); }
	inline int32_t get_Quality_4() const { return ___Quality_4; }
	inline int32_t* get_address_of_Quality_4() { return &___Quality_4; }
	inline void set_Quality_4(int32_t value)
	{
		___Quality_4 = value;
	}

	inline static int32_t get_offset_of_Samples_5() { return static_cast<int32_t>(offsetof(DirectionalBlur_t13325309, ___Samples_5)); }
	inline int32_t get_Samples_5() const { return ___Samples_5; }
	inline int32_t* get_address_of_Samples_5() { return &___Samples_5; }
	inline void set_Samples_5(int32_t value)
	{
		___Samples_5 = value;
	}

	inline static int32_t get_offset_of_Strength_6() { return static_cast<int32_t>(offsetof(DirectionalBlur_t13325309, ___Strength_6)); }
	inline float get_Strength_6() const { return ___Strength_6; }
	inline float* get_address_of_Strength_6() { return &___Strength_6; }
	inline void set_Strength_6(float value)
	{
		___Strength_6 = value;
	}

	inline static int32_t get_offset_of_Angle_7() { return static_cast<int32_t>(offsetof(DirectionalBlur_t13325309, ___Angle_7)); }
	inline float get_Angle_7() const { return ___Angle_7; }
	inline float* get_address_of_Angle_7() { return &___Angle_7; }
	inline void set_Angle_7(float value)
	{
		___Angle_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTIONALBLUR_T13325309_H
#ifndef LEVELS_T1894673157_H
#define LEVELS_T1894673157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Levels
struct  Levels_t1894673157  : public BaseEffect_t1187847871
{
public:
	// Colorful.Levels/ColorMode Colorful.Levels::Mode
	int32_t ___Mode_4;
	// UnityEngine.Vector3 Colorful.Levels::InputL
	Vector3_t3722313464  ___InputL_5;
	// UnityEngine.Vector3 Colorful.Levels::InputR
	Vector3_t3722313464  ___InputR_6;
	// UnityEngine.Vector3 Colorful.Levels::InputG
	Vector3_t3722313464  ___InputG_7;
	// UnityEngine.Vector3 Colorful.Levels::InputB
	Vector3_t3722313464  ___InputB_8;
	// UnityEngine.Vector2 Colorful.Levels::OutputL
	Vector2_t2156229523  ___OutputL_9;
	// UnityEngine.Vector2 Colorful.Levels::OutputR
	Vector2_t2156229523  ___OutputR_10;
	// UnityEngine.Vector2 Colorful.Levels::OutputG
	Vector2_t2156229523  ___OutputG_11;
	// UnityEngine.Vector2 Colorful.Levels::OutputB
	Vector2_t2156229523  ___OutputB_12;

public:
	inline static int32_t get_offset_of_Mode_4() { return static_cast<int32_t>(offsetof(Levels_t1894673157, ___Mode_4)); }
	inline int32_t get_Mode_4() const { return ___Mode_4; }
	inline int32_t* get_address_of_Mode_4() { return &___Mode_4; }
	inline void set_Mode_4(int32_t value)
	{
		___Mode_4 = value;
	}

	inline static int32_t get_offset_of_InputL_5() { return static_cast<int32_t>(offsetof(Levels_t1894673157, ___InputL_5)); }
	inline Vector3_t3722313464  get_InputL_5() const { return ___InputL_5; }
	inline Vector3_t3722313464 * get_address_of_InputL_5() { return &___InputL_5; }
	inline void set_InputL_5(Vector3_t3722313464  value)
	{
		___InputL_5 = value;
	}

	inline static int32_t get_offset_of_InputR_6() { return static_cast<int32_t>(offsetof(Levels_t1894673157, ___InputR_6)); }
	inline Vector3_t3722313464  get_InputR_6() const { return ___InputR_6; }
	inline Vector3_t3722313464 * get_address_of_InputR_6() { return &___InputR_6; }
	inline void set_InputR_6(Vector3_t3722313464  value)
	{
		___InputR_6 = value;
	}

	inline static int32_t get_offset_of_InputG_7() { return static_cast<int32_t>(offsetof(Levels_t1894673157, ___InputG_7)); }
	inline Vector3_t3722313464  get_InputG_7() const { return ___InputG_7; }
	inline Vector3_t3722313464 * get_address_of_InputG_7() { return &___InputG_7; }
	inline void set_InputG_7(Vector3_t3722313464  value)
	{
		___InputG_7 = value;
	}

	inline static int32_t get_offset_of_InputB_8() { return static_cast<int32_t>(offsetof(Levels_t1894673157, ___InputB_8)); }
	inline Vector3_t3722313464  get_InputB_8() const { return ___InputB_8; }
	inline Vector3_t3722313464 * get_address_of_InputB_8() { return &___InputB_8; }
	inline void set_InputB_8(Vector3_t3722313464  value)
	{
		___InputB_8 = value;
	}

	inline static int32_t get_offset_of_OutputL_9() { return static_cast<int32_t>(offsetof(Levels_t1894673157, ___OutputL_9)); }
	inline Vector2_t2156229523  get_OutputL_9() const { return ___OutputL_9; }
	inline Vector2_t2156229523 * get_address_of_OutputL_9() { return &___OutputL_9; }
	inline void set_OutputL_9(Vector2_t2156229523  value)
	{
		___OutputL_9 = value;
	}

	inline static int32_t get_offset_of_OutputR_10() { return static_cast<int32_t>(offsetof(Levels_t1894673157, ___OutputR_10)); }
	inline Vector2_t2156229523  get_OutputR_10() const { return ___OutputR_10; }
	inline Vector2_t2156229523 * get_address_of_OutputR_10() { return &___OutputR_10; }
	inline void set_OutputR_10(Vector2_t2156229523  value)
	{
		___OutputR_10 = value;
	}

	inline static int32_t get_offset_of_OutputG_11() { return static_cast<int32_t>(offsetof(Levels_t1894673157, ___OutputG_11)); }
	inline Vector2_t2156229523  get_OutputG_11() const { return ___OutputG_11; }
	inline Vector2_t2156229523 * get_address_of_OutputG_11() { return &___OutputG_11; }
	inline void set_OutputG_11(Vector2_t2156229523  value)
	{
		___OutputG_11 = value;
	}

	inline static int32_t get_offset_of_OutputB_12() { return static_cast<int32_t>(offsetof(Levels_t1894673157, ___OutputB_12)); }
	inline Vector2_t2156229523  get_OutputB_12() const { return ___OutputB_12; }
	inline Vector2_t2156229523 * get_address_of_OutputB_12() { return &___OutputB_12; }
	inline void set_OutputB_12(Vector2_t2156229523  value)
	{
		___OutputB_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELS_T1894673157_H
#ifndef SMARTSATURATION_T3817639239_H
#define SMARTSATURATION_T3817639239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.SmartSaturation
struct  SmartSaturation_t3817639239  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.SmartSaturation::Boost
	float ___Boost_4;
	// UnityEngine.AnimationCurve Colorful.SmartSaturation::Curve
	AnimationCurve_t3046754366 * ___Curve_5;
	// UnityEngine.Texture2D Colorful.SmartSaturation::_CurveTexture
	Texture2D_t3840446185 * ____CurveTexture_6;

public:
	inline static int32_t get_offset_of_Boost_4() { return static_cast<int32_t>(offsetof(SmartSaturation_t3817639239, ___Boost_4)); }
	inline float get_Boost_4() const { return ___Boost_4; }
	inline float* get_address_of_Boost_4() { return &___Boost_4; }
	inline void set_Boost_4(float value)
	{
		___Boost_4 = value;
	}

	inline static int32_t get_offset_of_Curve_5() { return static_cast<int32_t>(offsetof(SmartSaturation_t3817639239, ___Curve_5)); }
	inline AnimationCurve_t3046754366 * get_Curve_5() const { return ___Curve_5; }
	inline AnimationCurve_t3046754366 ** get_address_of_Curve_5() { return &___Curve_5; }
	inline void set_Curve_5(AnimationCurve_t3046754366 * value)
	{
		___Curve_5 = value;
		Il2CppCodeGenWriteBarrier((&___Curve_5), value);
	}

	inline static int32_t get_offset_of__CurveTexture_6() { return static_cast<int32_t>(offsetof(SmartSaturation_t3817639239, ____CurveTexture_6)); }
	inline Texture2D_t3840446185 * get__CurveTexture_6() const { return ____CurveTexture_6; }
	inline Texture2D_t3840446185 ** get_address_of__CurveTexture_6() { return &____CurveTexture_6; }
	inline void set__CurveTexture_6(Texture2D_t3840446185 * value)
	{
		____CurveTexture_6 = value;
		Il2CppCodeGenWriteBarrier((&____CurveTexture_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTSATURATION_T3817639239_H
#ifndef STROKES_T892401832_H
#define STROKES_T892401832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Strokes
struct  Strokes_t892401832  : public BaseEffect_t1187847871
{
public:
	// Colorful.Strokes/ColorMode Colorful.Strokes::Mode
	int32_t ___Mode_4;
	// System.Single Colorful.Strokes::Amplitude
	float ___Amplitude_5;
	// System.Single Colorful.Strokes::Frequency
	float ___Frequency_6;
	// System.Single Colorful.Strokes::Scaling
	float ___Scaling_7;
	// System.Single Colorful.Strokes::MaxThickness
	float ___MaxThickness_8;
	// System.Single Colorful.Strokes::Threshold
	float ___Threshold_9;
	// System.Single Colorful.Strokes::Harshness
	float ___Harshness_10;
	// System.Single Colorful.Strokes::RedLuminance
	float ___RedLuminance_11;
	// System.Single Colorful.Strokes::GreenLuminance
	float ___GreenLuminance_12;
	// System.Single Colorful.Strokes::BlueLuminance
	float ___BlueLuminance_13;

public:
	inline static int32_t get_offset_of_Mode_4() { return static_cast<int32_t>(offsetof(Strokes_t892401832, ___Mode_4)); }
	inline int32_t get_Mode_4() const { return ___Mode_4; }
	inline int32_t* get_address_of_Mode_4() { return &___Mode_4; }
	inline void set_Mode_4(int32_t value)
	{
		___Mode_4 = value;
	}

	inline static int32_t get_offset_of_Amplitude_5() { return static_cast<int32_t>(offsetof(Strokes_t892401832, ___Amplitude_5)); }
	inline float get_Amplitude_5() const { return ___Amplitude_5; }
	inline float* get_address_of_Amplitude_5() { return &___Amplitude_5; }
	inline void set_Amplitude_5(float value)
	{
		___Amplitude_5 = value;
	}

	inline static int32_t get_offset_of_Frequency_6() { return static_cast<int32_t>(offsetof(Strokes_t892401832, ___Frequency_6)); }
	inline float get_Frequency_6() const { return ___Frequency_6; }
	inline float* get_address_of_Frequency_6() { return &___Frequency_6; }
	inline void set_Frequency_6(float value)
	{
		___Frequency_6 = value;
	}

	inline static int32_t get_offset_of_Scaling_7() { return static_cast<int32_t>(offsetof(Strokes_t892401832, ___Scaling_7)); }
	inline float get_Scaling_7() const { return ___Scaling_7; }
	inline float* get_address_of_Scaling_7() { return &___Scaling_7; }
	inline void set_Scaling_7(float value)
	{
		___Scaling_7 = value;
	}

	inline static int32_t get_offset_of_MaxThickness_8() { return static_cast<int32_t>(offsetof(Strokes_t892401832, ___MaxThickness_8)); }
	inline float get_MaxThickness_8() const { return ___MaxThickness_8; }
	inline float* get_address_of_MaxThickness_8() { return &___MaxThickness_8; }
	inline void set_MaxThickness_8(float value)
	{
		___MaxThickness_8 = value;
	}

	inline static int32_t get_offset_of_Threshold_9() { return static_cast<int32_t>(offsetof(Strokes_t892401832, ___Threshold_9)); }
	inline float get_Threshold_9() const { return ___Threshold_9; }
	inline float* get_address_of_Threshold_9() { return &___Threshold_9; }
	inline void set_Threshold_9(float value)
	{
		___Threshold_9 = value;
	}

	inline static int32_t get_offset_of_Harshness_10() { return static_cast<int32_t>(offsetof(Strokes_t892401832, ___Harshness_10)); }
	inline float get_Harshness_10() const { return ___Harshness_10; }
	inline float* get_address_of_Harshness_10() { return &___Harshness_10; }
	inline void set_Harshness_10(float value)
	{
		___Harshness_10 = value;
	}

	inline static int32_t get_offset_of_RedLuminance_11() { return static_cast<int32_t>(offsetof(Strokes_t892401832, ___RedLuminance_11)); }
	inline float get_RedLuminance_11() const { return ___RedLuminance_11; }
	inline float* get_address_of_RedLuminance_11() { return &___RedLuminance_11; }
	inline void set_RedLuminance_11(float value)
	{
		___RedLuminance_11 = value;
	}

	inline static int32_t get_offset_of_GreenLuminance_12() { return static_cast<int32_t>(offsetof(Strokes_t892401832, ___GreenLuminance_12)); }
	inline float get_GreenLuminance_12() const { return ___GreenLuminance_12; }
	inline float* get_address_of_GreenLuminance_12() { return &___GreenLuminance_12; }
	inline void set_GreenLuminance_12(float value)
	{
		___GreenLuminance_12 = value;
	}

	inline static int32_t get_offset_of_BlueLuminance_13() { return static_cast<int32_t>(offsetof(Strokes_t892401832, ___BlueLuminance_13)); }
	inline float get_BlueLuminance_13() const { return ___BlueLuminance_13; }
	inline float* get_address_of_BlueLuminance_13() { return &___BlueLuminance_13; }
	inline void set_BlueLuminance_13(float value)
	{
		___BlueLuminance_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STROKES_T892401832_H
#ifndef TECHNICOLOR_T2196134954_H
#define TECHNICOLOR_T2196134954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Technicolor
struct  Technicolor_t2196134954  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.Technicolor::Exposure
	float ___Exposure_4;
	// UnityEngine.Vector3 Colorful.Technicolor::Balance
	Vector3_t3722313464  ___Balance_5;
	// System.Single Colorful.Technicolor::Amount
	float ___Amount_6;

public:
	inline static int32_t get_offset_of_Exposure_4() { return static_cast<int32_t>(offsetof(Technicolor_t2196134954, ___Exposure_4)); }
	inline float get_Exposure_4() const { return ___Exposure_4; }
	inline float* get_address_of_Exposure_4() { return &___Exposure_4; }
	inline void set_Exposure_4(float value)
	{
		___Exposure_4 = value;
	}

	inline static int32_t get_offset_of_Balance_5() { return static_cast<int32_t>(offsetof(Technicolor_t2196134954, ___Balance_5)); }
	inline Vector3_t3722313464  get_Balance_5() const { return ___Balance_5; }
	inline Vector3_t3722313464 * get_address_of_Balance_5() { return &___Balance_5; }
	inline void set_Balance_5(Vector3_t3722313464  value)
	{
		___Balance_5 = value;
	}

	inline static int32_t get_offset_of_Amount_6() { return static_cast<int32_t>(offsetof(Technicolor_t2196134954, ___Amount_6)); }
	inline float get_Amount_6() const { return ___Amount_6; }
	inline float* get_address_of_Amount_6() { return &___Amount_6; }
	inline void set_Amount_6(float value)
	{
		___Amount_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TECHNICOLOR_T2196134954_H
#ifndef THRESHOLD_T1192544618_H
#define THRESHOLD_T1192544618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Threshold
struct  Threshold_t1192544618  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.Threshold::Value
	float ___Value_4;
	// System.Single Colorful.Threshold::NoiseRange
	float ___NoiseRange_5;
	// System.Boolean Colorful.Threshold::UseNoise
	bool ___UseNoise_6;

public:
	inline static int32_t get_offset_of_Value_4() { return static_cast<int32_t>(offsetof(Threshold_t1192544618, ___Value_4)); }
	inline float get_Value_4() const { return ___Value_4; }
	inline float* get_address_of_Value_4() { return &___Value_4; }
	inline void set_Value_4(float value)
	{
		___Value_4 = value;
	}

	inline static int32_t get_offset_of_NoiseRange_5() { return static_cast<int32_t>(offsetof(Threshold_t1192544618, ___NoiseRange_5)); }
	inline float get_NoiseRange_5() const { return ___NoiseRange_5; }
	inline float* get_address_of_NoiseRange_5() { return &___NoiseRange_5; }
	inline void set_NoiseRange_5(float value)
	{
		___NoiseRange_5 = value;
	}

	inline static int32_t get_offset_of_UseNoise_6() { return static_cast<int32_t>(offsetof(Threshold_t1192544618, ___UseNoise_6)); }
	inline bool get_UseNoise_6() const { return ___UseNoise_6; }
	inline bool* get_address_of_UseNoise_6() { return &___UseNoise_6; }
	inline void set_UseNoise_6(bool value)
	{
		___UseNoise_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THRESHOLD_T1192544618_H
#ifndef CHANNELSWAPPER_T1718792779_H
#define CHANNELSWAPPER_T1718792779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.ChannelSwapper
struct  ChannelSwapper_t1718792779  : public BaseEffect_t1187847871
{
public:
	// Colorful.ChannelSwapper/Channel Colorful.ChannelSwapper::RedSource
	int32_t ___RedSource_4;
	// Colorful.ChannelSwapper/Channel Colorful.ChannelSwapper::GreenSource
	int32_t ___GreenSource_5;
	// Colorful.ChannelSwapper/Channel Colorful.ChannelSwapper::BlueSource
	int32_t ___BlueSource_6;

public:
	inline static int32_t get_offset_of_RedSource_4() { return static_cast<int32_t>(offsetof(ChannelSwapper_t1718792779, ___RedSource_4)); }
	inline int32_t get_RedSource_4() const { return ___RedSource_4; }
	inline int32_t* get_address_of_RedSource_4() { return &___RedSource_4; }
	inline void set_RedSource_4(int32_t value)
	{
		___RedSource_4 = value;
	}

	inline static int32_t get_offset_of_GreenSource_5() { return static_cast<int32_t>(offsetof(ChannelSwapper_t1718792779, ___GreenSource_5)); }
	inline int32_t get_GreenSource_5() const { return ___GreenSource_5; }
	inline int32_t* get_address_of_GreenSource_5() { return &___GreenSource_5; }
	inline void set_GreenSource_5(int32_t value)
	{
		___GreenSource_5 = value;
	}

	inline static int32_t get_offset_of_BlueSource_6() { return static_cast<int32_t>(offsetof(ChannelSwapper_t1718792779, ___BlueSource_6)); }
	inline int32_t get_BlueSource_6() const { return ___BlueSource_6; }
	inline int32_t* get_address_of_BlueSource_6() { return &___BlueSource_6; }
	inline void set_BlueSource_6(int32_t value)
	{
		___BlueSource_6 = value;
	}
};

struct ChannelSwapper_t1718792779_StaticFields
{
public:
	// UnityEngine.Vector4[] Colorful.ChannelSwapper::m_Channels
	Vector4U5BU5D_t934056436* ___m_Channels_7;

public:
	inline static int32_t get_offset_of_m_Channels_7() { return static_cast<int32_t>(offsetof(ChannelSwapper_t1718792779_StaticFields, ___m_Channels_7)); }
	inline Vector4U5BU5D_t934056436* get_m_Channels_7() const { return ___m_Channels_7; }
	inline Vector4U5BU5D_t934056436** get_address_of_m_Channels_7() { return &___m_Channels_7; }
	inline void set_m_Channels_7(Vector4U5BU5D_t934056436* value)
	{
		___m_Channels_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Channels_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNELSWAPPER_T1718792779_H
#ifndef VIBRANCE_T2988503215_H
#define VIBRANCE_T2988503215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Vibrance
struct  Vibrance_t2988503215  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.Vibrance::Amount
	float ___Amount_4;
	// System.Single Colorful.Vibrance::RedChannel
	float ___RedChannel_5;
	// System.Single Colorful.Vibrance::GreenChannel
	float ___GreenChannel_6;
	// System.Single Colorful.Vibrance::BlueChannel
	float ___BlueChannel_7;
	// System.Boolean Colorful.Vibrance::AdvancedMode
	bool ___AdvancedMode_8;

public:
	inline static int32_t get_offset_of_Amount_4() { return static_cast<int32_t>(offsetof(Vibrance_t2988503215, ___Amount_4)); }
	inline float get_Amount_4() const { return ___Amount_4; }
	inline float* get_address_of_Amount_4() { return &___Amount_4; }
	inline void set_Amount_4(float value)
	{
		___Amount_4 = value;
	}

	inline static int32_t get_offset_of_RedChannel_5() { return static_cast<int32_t>(offsetof(Vibrance_t2988503215, ___RedChannel_5)); }
	inline float get_RedChannel_5() const { return ___RedChannel_5; }
	inline float* get_address_of_RedChannel_5() { return &___RedChannel_5; }
	inline void set_RedChannel_5(float value)
	{
		___RedChannel_5 = value;
	}

	inline static int32_t get_offset_of_GreenChannel_6() { return static_cast<int32_t>(offsetof(Vibrance_t2988503215, ___GreenChannel_6)); }
	inline float get_GreenChannel_6() const { return ___GreenChannel_6; }
	inline float* get_address_of_GreenChannel_6() { return &___GreenChannel_6; }
	inline void set_GreenChannel_6(float value)
	{
		___GreenChannel_6 = value;
	}

	inline static int32_t get_offset_of_BlueChannel_7() { return static_cast<int32_t>(offsetof(Vibrance_t2988503215, ___BlueChannel_7)); }
	inline float get_BlueChannel_7() const { return ___BlueChannel_7; }
	inline float* get_address_of_BlueChannel_7() { return &___BlueChannel_7; }
	inline void set_BlueChannel_7(float value)
	{
		___BlueChannel_7 = value;
	}

	inline static int32_t get_offset_of_AdvancedMode_8() { return static_cast<int32_t>(offsetof(Vibrance_t2988503215, ___AdvancedMode_8)); }
	inline bool get_AdvancedMode_8() const { return ___AdvancedMode_8; }
	inline bool* get_address_of_AdvancedMode_8() { return &___AdvancedMode_8; }
	inline void set_AdvancedMode_8(bool value)
	{
		___AdvancedMode_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIBRANCE_T2988503215_H
#ifndef VINTAGEFAST_T3040349303_H
#define VINTAGEFAST_T3040349303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.VintageFast
struct  VintageFast_t3040349303  : public LookupFilter3D_t3361124730
{
public:
	// Colorful.Vintage/InstragramFilter Colorful.VintageFast::Filter
	int32_t ___Filter_12;
	// Colorful.Vintage/InstragramFilter Colorful.VintageFast::m_CurrentFilter
	int32_t ___m_CurrentFilter_13;

public:
	inline static int32_t get_offset_of_Filter_12() { return static_cast<int32_t>(offsetof(VintageFast_t3040349303, ___Filter_12)); }
	inline int32_t get_Filter_12() const { return ___Filter_12; }
	inline int32_t* get_address_of_Filter_12() { return &___Filter_12; }
	inline void set_Filter_12(int32_t value)
	{
		___Filter_12 = value;
	}

	inline static int32_t get_offset_of_m_CurrentFilter_13() { return static_cast<int32_t>(offsetof(VintageFast_t3040349303, ___m_CurrentFilter_13)); }
	inline int32_t get_m_CurrentFilter_13() const { return ___m_CurrentFilter_13; }
	inline int32_t* get_address_of_m_CurrentFilter_13() { return &___m_CurrentFilter_13; }
	inline void set_m_CurrentFilter_13(int32_t value)
	{
		___m_CurrentFilter_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VINTAGEFAST_T3040349303_H
#ifndef WAVEDISTORTION_T3073420756_H
#define WAVEDISTORTION_T3073420756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.WaveDistortion
struct  WaveDistortion_t3073420756  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.WaveDistortion::Amplitude
	float ___Amplitude_4;
	// System.Single Colorful.WaveDistortion::Waves
	float ___Waves_5;
	// System.Single Colorful.WaveDistortion::ColorGlitch
	float ___ColorGlitch_6;
	// System.Single Colorful.WaveDistortion::Phase
	float ___Phase_7;

public:
	inline static int32_t get_offset_of_Amplitude_4() { return static_cast<int32_t>(offsetof(WaveDistortion_t3073420756, ___Amplitude_4)); }
	inline float get_Amplitude_4() const { return ___Amplitude_4; }
	inline float* get_address_of_Amplitude_4() { return &___Amplitude_4; }
	inline void set_Amplitude_4(float value)
	{
		___Amplitude_4 = value;
	}

	inline static int32_t get_offset_of_Waves_5() { return static_cast<int32_t>(offsetof(WaveDistortion_t3073420756, ___Waves_5)); }
	inline float get_Waves_5() const { return ___Waves_5; }
	inline float* get_address_of_Waves_5() { return &___Waves_5; }
	inline void set_Waves_5(float value)
	{
		___Waves_5 = value;
	}

	inline static int32_t get_offset_of_ColorGlitch_6() { return static_cast<int32_t>(offsetof(WaveDistortion_t3073420756, ___ColorGlitch_6)); }
	inline float get_ColorGlitch_6() const { return ___ColorGlitch_6; }
	inline float* get_address_of_ColorGlitch_6() { return &___ColorGlitch_6; }
	inline void set_ColorGlitch_6(float value)
	{
		___ColorGlitch_6 = value;
	}

	inline static int32_t get_offset_of_Phase_7() { return static_cast<int32_t>(offsetof(WaveDistortion_t3073420756, ___Phase_7)); }
	inline float get_Phase_7() const { return ___Phase_7; }
	inline float* get_address_of_Phase_7() { return &___Phase_7; }
	inline void set_Phase_7(float value)
	{
		___Phase_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAVEDISTORTION_T3073420756_H
#ifndef WHITEBALANCE_T1550391260_H
#define WHITEBALANCE_T1550391260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.WhiteBalance
struct  WhiteBalance_t1550391260  : public BaseEffect_t1187847871
{
public:
	// UnityEngine.Color Colorful.WhiteBalance::White
	Color_t2555686324  ___White_4;
	// Colorful.WhiteBalance/BalanceMode Colorful.WhiteBalance::Mode
	int32_t ___Mode_5;

public:
	inline static int32_t get_offset_of_White_4() { return static_cast<int32_t>(offsetof(WhiteBalance_t1550391260, ___White_4)); }
	inline Color_t2555686324  get_White_4() const { return ___White_4; }
	inline Color_t2555686324 * get_address_of_White_4() { return &___White_4; }
	inline void set_White_4(Color_t2555686324  value)
	{
		___White_4 = value;
	}

	inline static int32_t get_offset_of_Mode_5() { return static_cast<int32_t>(offsetof(WhiteBalance_t1550391260, ___Mode_5)); }
	inline int32_t get_Mode_5() const { return ___Mode_5; }
	inline int32_t* get_address_of_Mode_5() { return &___Mode_5; }
	inline void set_Mode_5(int32_t value)
	{
		___Mode_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHITEBALANCE_T1550391260_H
#ifndef WIGGLE_T4291210129_H
#define WIGGLE_T4291210129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Wiggle
struct  Wiggle_t4291210129  : public BaseEffect_t1187847871
{
public:
	// Colorful.Wiggle/Algorithm Colorful.Wiggle::Mode
	int32_t ___Mode_4;
	// System.Single Colorful.Wiggle::Timer
	float ___Timer_5;
	// System.Single Colorful.Wiggle::Speed
	float ___Speed_6;
	// System.Single Colorful.Wiggle::Frequency
	float ___Frequency_7;
	// System.Single Colorful.Wiggle::Amplitude
	float ___Amplitude_8;
	// System.Boolean Colorful.Wiggle::AutomaticTimer
	bool ___AutomaticTimer_9;

public:
	inline static int32_t get_offset_of_Mode_4() { return static_cast<int32_t>(offsetof(Wiggle_t4291210129, ___Mode_4)); }
	inline int32_t get_Mode_4() const { return ___Mode_4; }
	inline int32_t* get_address_of_Mode_4() { return &___Mode_4; }
	inline void set_Mode_4(int32_t value)
	{
		___Mode_4 = value;
	}

	inline static int32_t get_offset_of_Timer_5() { return static_cast<int32_t>(offsetof(Wiggle_t4291210129, ___Timer_5)); }
	inline float get_Timer_5() const { return ___Timer_5; }
	inline float* get_address_of_Timer_5() { return &___Timer_5; }
	inline void set_Timer_5(float value)
	{
		___Timer_5 = value;
	}

	inline static int32_t get_offset_of_Speed_6() { return static_cast<int32_t>(offsetof(Wiggle_t4291210129, ___Speed_6)); }
	inline float get_Speed_6() const { return ___Speed_6; }
	inline float* get_address_of_Speed_6() { return &___Speed_6; }
	inline void set_Speed_6(float value)
	{
		___Speed_6 = value;
	}

	inline static int32_t get_offset_of_Frequency_7() { return static_cast<int32_t>(offsetof(Wiggle_t4291210129, ___Frequency_7)); }
	inline float get_Frequency_7() const { return ___Frequency_7; }
	inline float* get_address_of_Frequency_7() { return &___Frequency_7; }
	inline void set_Frequency_7(float value)
	{
		___Frequency_7 = value;
	}

	inline static int32_t get_offset_of_Amplitude_8() { return static_cast<int32_t>(offsetof(Wiggle_t4291210129, ___Amplitude_8)); }
	inline float get_Amplitude_8() const { return ___Amplitude_8; }
	inline float* get_address_of_Amplitude_8() { return &___Amplitude_8; }
	inline void set_Amplitude_8(float value)
	{
		___Amplitude_8 = value;
	}

	inline static int32_t get_offset_of_AutomaticTimer_9() { return static_cast<int32_t>(offsetof(Wiggle_t4291210129, ___AutomaticTimer_9)); }
	inline bool get_AutomaticTimer_9() const { return ___AutomaticTimer_9; }
	inline bool* get_address_of_AutomaticTimer_9() { return &___AutomaticTimer_9; }
	inline void set_AutomaticTimer_9(bool value)
	{
		___AutomaticTimer_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIGGLE_T4291210129_H
#ifndef DOTWEENANIMATION_T3459117967_H
#define DOTWEENANIMATION_T3459117967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenAnimation
struct  DOTweenAnimation_t3459117967  : public ABSAnimationComponent_t262169234
{
public:
	// System.Single DG.Tweening.DOTweenAnimation::delay
	float ___delay_19;
	// System.Single DG.Tweening.DOTweenAnimation::duration
	float ___duration_20;
	// DG.Tweening.Ease DG.Tweening.DOTweenAnimation::easeType
	int32_t ___easeType_21;
	// UnityEngine.AnimationCurve DG.Tweening.DOTweenAnimation::easeCurve
	AnimationCurve_t3046754366 * ___easeCurve_22;
	// DG.Tweening.LoopType DG.Tweening.DOTweenAnimation::loopType
	int32_t ___loopType_23;
	// System.Int32 DG.Tweening.DOTweenAnimation::loops
	int32_t ___loops_24;
	// System.String DG.Tweening.DOTweenAnimation::id
	String_t* ___id_25;
	// System.Boolean DG.Tweening.DOTweenAnimation::isRelative
	bool ___isRelative_26;
	// System.Boolean DG.Tweening.DOTweenAnimation::isFrom
	bool ___isFrom_27;
	// System.Boolean DG.Tweening.DOTweenAnimation::isIndependentUpdate
	bool ___isIndependentUpdate_28;
	// System.Boolean DG.Tweening.DOTweenAnimation::autoKill
	bool ___autoKill_29;
	// System.Boolean DG.Tweening.DOTweenAnimation::isActive
	bool ___isActive_30;
	// System.Boolean DG.Tweening.DOTweenAnimation::isValid
	bool ___isValid_31;
	// UnityEngine.Component DG.Tweening.DOTweenAnimation::target
	Component_t1923634451 * ___target_32;
	// DG.Tweening.Core.DOTweenAnimationType DG.Tweening.DOTweenAnimation::animationType
	int32_t ___animationType_33;
	// DG.Tweening.Core.TargetType DG.Tweening.DOTweenAnimation::targetType
	int32_t ___targetType_34;
	// DG.Tweening.Core.TargetType DG.Tweening.DOTweenAnimation::forcedTargetType
	int32_t ___forcedTargetType_35;
	// System.Boolean DG.Tweening.DOTweenAnimation::autoPlay
	bool ___autoPlay_36;
	// System.Boolean DG.Tweening.DOTweenAnimation::useTargetAsV3
	bool ___useTargetAsV3_37;
	// System.Single DG.Tweening.DOTweenAnimation::endValueFloat
	float ___endValueFloat_38;
	// UnityEngine.Vector3 DG.Tweening.DOTweenAnimation::endValueV3
	Vector3_t3722313464  ___endValueV3_39;
	// UnityEngine.Vector2 DG.Tweening.DOTweenAnimation::endValueV2
	Vector2_t2156229523  ___endValueV2_40;
	// UnityEngine.Color DG.Tweening.DOTweenAnimation::endValueColor
	Color_t2555686324  ___endValueColor_41;
	// System.String DG.Tweening.DOTweenAnimation::endValueString
	String_t* ___endValueString_42;
	// UnityEngine.Rect DG.Tweening.DOTweenAnimation::endValueRect
	Rect_t2360479859  ___endValueRect_43;
	// UnityEngine.Transform DG.Tweening.DOTweenAnimation::endValueTransform
	Transform_t3600365921 * ___endValueTransform_44;
	// System.Boolean DG.Tweening.DOTweenAnimation::optionalBool0
	bool ___optionalBool0_45;
	// System.Single DG.Tweening.DOTweenAnimation::optionalFloat0
	float ___optionalFloat0_46;
	// System.Int32 DG.Tweening.DOTweenAnimation::optionalInt0
	int32_t ___optionalInt0_47;
	// DG.Tweening.RotateMode DG.Tweening.DOTweenAnimation::optionalRotationMode
	int32_t ___optionalRotationMode_48;
	// DG.Tweening.ScrambleMode DG.Tweening.DOTweenAnimation::optionalScrambleMode
	int32_t ___optionalScrambleMode_49;
	// System.String DG.Tweening.DOTweenAnimation::optionalString
	String_t* ___optionalString_50;
	// System.Boolean DG.Tweening.DOTweenAnimation::_tweenCreated
	bool ____tweenCreated_51;
	// System.Int32 DG.Tweening.DOTweenAnimation::_playCount
	int32_t ____playCount_52;

public:
	inline static int32_t get_offset_of_delay_19() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___delay_19)); }
	inline float get_delay_19() const { return ___delay_19; }
	inline float* get_address_of_delay_19() { return &___delay_19; }
	inline void set_delay_19(float value)
	{
		___delay_19 = value;
	}

	inline static int32_t get_offset_of_duration_20() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___duration_20)); }
	inline float get_duration_20() const { return ___duration_20; }
	inline float* get_address_of_duration_20() { return &___duration_20; }
	inline void set_duration_20(float value)
	{
		___duration_20 = value;
	}

	inline static int32_t get_offset_of_easeType_21() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___easeType_21)); }
	inline int32_t get_easeType_21() const { return ___easeType_21; }
	inline int32_t* get_address_of_easeType_21() { return &___easeType_21; }
	inline void set_easeType_21(int32_t value)
	{
		___easeType_21 = value;
	}

	inline static int32_t get_offset_of_easeCurve_22() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___easeCurve_22)); }
	inline AnimationCurve_t3046754366 * get_easeCurve_22() const { return ___easeCurve_22; }
	inline AnimationCurve_t3046754366 ** get_address_of_easeCurve_22() { return &___easeCurve_22; }
	inline void set_easeCurve_22(AnimationCurve_t3046754366 * value)
	{
		___easeCurve_22 = value;
		Il2CppCodeGenWriteBarrier((&___easeCurve_22), value);
	}

	inline static int32_t get_offset_of_loopType_23() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___loopType_23)); }
	inline int32_t get_loopType_23() const { return ___loopType_23; }
	inline int32_t* get_address_of_loopType_23() { return &___loopType_23; }
	inline void set_loopType_23(int32_t value)
	{
		___loopType_23 = value;
	}

	inline static int32_t get_offset_of_loops_24() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___loops_24)); }
	inline int32_t get_loops_24() const { return ___loops_24; }
	inline int32_t* get_address_of_loops_24() { return &___loops_24; }
	inline void set_loops_24(int32_t value)
	{
		___loops_24 = value;
	}

	inline static int32_t get_offset_of_id_25() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___id_25)); }
	inline String_t* get_id_25() const { return ___id_25; }
	inline String_t** get_address_of_id_25() { return &___id_25; }
	inline void set_id_25(String_t* value)
	{
		___id_25 = value;
		Il2CppCodeGenWriteBarrier((&___id_25), value);
	}

	inline static int32_t get_offset_of_isRelative_26() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___isRelative_26)); }
	inline bool get_isRelative_26() const { return ___isRelative_26; }
	inline bool* get_address_of_isRelative_26() { return &___isRelative_26; }
	inline void set_isRelative_26(bool value)
	{
		___isRelative_26 = value;
	}

	inline static int32_t get_offset_of_isFrom_27() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___isFrom_27)); }
	inline bool get_isFrom_27() const { return ___isFrom_27; }
	inline bool* get_address_of_isFrom_27() { return &___isFrom_27; }
	inline void set_isFrom_27(bool value)
	{
		___isFrom_27 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_28() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___isIndependentUpdate_28)); }
	inline bool get_isIndependentUpdate_28() const { return ___isIndependentUpdate_28; }
	inline bool* get_address_of_isIndependentUpdate_28() { return &___isIndependentUpdate_28; }
	inline void set_isIndependentUpdate_28(bool value)
	{
		___isIndependentUpdate_28 = value;
	}

	inline static int32_t get_offset_of_autoKill_29() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___autoKill_29)); }
	inline bool get_autoKill_29() const { return ___autoKill_29; }
	inline bool* get_address_of_autoKill_29() { return &___autoKill_29; }
	inline void set_autoKill_29(bool value)
	{
		___autoKill_29 = value;
	}

	inline static int32_t get_offset_of_isActive_30() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___isActive_30)); }
	inline bool get_isActive_30() const { return ___isActive_30; }
	inline bool* get_address_of_isActive_30() { return &___isActive_30; }
	inline void set_isActive_30(bool value)
	{
		___isActive_30 = value;
	}

	inline static int32_t get_offset_of_isValid_31() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___isValid_31)); }
	inline bool get_isValid_31() const { return ___isValid_31; }
	inline bool* get_address_of_isValid_31() { return &___isValid_31; }
	inline void set_isValid_31(bool value)
	{
		___isValid_31 = value;
	}

	inline static int32_t get_offset_of_target_32() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___target_32)); }
	inline Component_t1923634451 * get_target_32() const { return ___target_32; }
	inline Component_t1923634451 ** get_address_of_target_32() { return &___target_32; }
	inline void set_target_32(Component_t1923634451 * value)
	{
		___target_32 = value;
		Il2CppCodeGenWriteBarrier((&___target_32), value);
	}

	inline static int32_t get_offset_of_animationType_33() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___animationType_33)); }
	inline int32_t get_animationType_33() const { return ___animationType_33; }
	inline int32_t* get_address_of_animationType_33() { return &___animationType_33; }
	inline void set_animationType_33(int32_t value)
	{
		___animationType_33 = value;
	}

	inline static int32_t get_offset_of_targetType_34() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___targetType_34)); }
	inline int32_t get_targetType_34() const { return ___targetType_34; }
	inline int32_t* get_address_of_targetType_34() { return &___targetType_34; }
	inline void set_targetType_34(int32_t value)
	{
		___targetType_34 = value;
	}

	inline static int32_t get_offset_of_forcedTargetType_35() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___forcedTargetType_35)); }
	inline int32_t get_forcedTargetType_35() const { return ___forcedTargetType_35; }
	inline int32_t* get_address_of_forcedTargetType_35() { return &___forcedTargetType_35; }
	inline void set_forcedTargetType_35(int32_t value)
	{
		___forcedTargetType_35 = value;
	}

	inline static int32_t get_offset_of_autoPlay_36() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___autoPlay_36)); }
	inline bool get_autoPlay_36() const { return ___autoPlay_36; }
	inline bool* get_address_of_autoPlay_36() { return &___autoPlay_36; }
	inline void set_autoPlay_36(bool value)
	{
		___autoPlay_36 = value;
	}

	inline static int32_t get_offset_of_useTargetAsV3_37() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___useTargetAsV3_37)); }
	inline bool get_useTargetAsV3_37() const { return ___useTargetAsV3_37; }
	inline bool* get_address_of_useTargetAsV3_37() { return &___useTargetAsV3_37; }
	inline void set_useTargetAsV3_37(bool value)
	{
		___useTargetAsV3_37 = value;
	}

	inline static int32_t get_offset_of_endValueFloat_38() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___endValueFloat_38)); }
	inline float get_endValueFloat_38() const { return ___endValueFloat_38; }
	inline float* get_address_of_endValueFloat_38() { return &___endValueFloat_38; }
	inline void set_endValueFloat_38(float value)
	{
		___endValueFloat_38 = value;
	}

	inline static int32_t get_offset_of_endValueV3_39() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___endValueV3_39)); }
	inline Vector3_t3722313464  get_endValueV3_39() const { return ___endValueV3_39; }
	inline Vector3_t3722313464 * get_address_of_endValueV3_39() { return &___endValueV3_39; }
	inline void set_endValueV3_39(Vector3_t3722313464  value)
	{
		___endValueV3_39 = value;
	}

	inline static int32_t get_offset_of_endValueV2_40() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___endValueV2_40)); }
	inline Vector2_t2156229523  get_endValueV2_40() const { return ___endValueV2_40; }
	inline Vector2_t2156229523 * get_address_of_endValueV2_40() { return &___endValueV2_40; }
	inline void set_endValueV2_40(Vector2_t2156229523  value)
	{
		___endValueV2_40 = value;
	}

	inline static int32_t get_offset_of_endValueColor_41() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___endValueColor_41)); }
	inline Color_t2555686324  get_endValueColor_41() const { return ___endValueColor_41; }
	inline Color_t2555686324 * get_address_of_endValueColor_41() { return &___endValueColor_41; }
	inline void set_endValueColor_41(Color_t2555686324  value)
	{
		___endValueColor_41 = value;
	}

	inline static int32_t get_offset_of_endValueString_42() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___endValueString_42)); }
	inline String_t* get_endValueString_42() const { return ___endValueString_42; }
	inline String_t** get_address_of_endValueString_42() { return &___endValueString_42; }
	inline void set_endValueString_42(String_t* value)
	{
		___endValueString_42 = value;
		Il2CppCodeGenWriteBarrier((&___endValueString_42), value);
	}

	inline static int32_t get_offset_of_endValueRect_43() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___endValueRect_43)); }
	inline Rect_t2360479859  get_endValueRect_43() const { return ___endValueRect_43; }
	inline Rect_t2360479859 * get_address_of_endValueRect_43() { return &___endValueRect_43; }
	inline void set_endValueRect_43(Rect_t2360479859  value)
	{
		___endValueRect_43 = value;
	}

	inline static int32_t get_offset_of_endValueTransform_44() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___endValueTransform_44)); }
	inline Transform_t3600365921 * get_endValueTransform_44() const { return ___endValueTransform_44; }
	inline Transform_t3600365921 ** get_address_of_endValueTransform_44() { return &___endValueTransform_44; }
	inline void set_endValueTransform_44(Transform_t3600365921 * value)
	{
		___endValueTransform_44 = value;
		Il2CppCodeGenWriteBarrier((&___endValueTransform_44), value);
	}

	inline static int32_t get_offset_of_optionalBool0_45() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___optionalBool0_45)); }
	inline bool get_optionalBool0_45() const { return ___optionalBool0_45; }
	inline bool* get_address_of_optionalBool0_45() { return &___optionalBool0_45; }
	inline void set_optionalBool0_45(bool value)
	{
		___optionalBool0_45 = value;
	}

	inline static int32_t get_offset_of_optionalFloat0_46() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___optionalFloat0_46)); }
	inline float get_optionalFloat0_46() const { return ___optionalFloat0_46; }
	inline float* get_address_of_optionalFloat0_46() { return &___optionalFloat0_46; }
	inline void set_optionalFloat0_46(float value)
	{
		___optionalFloat0_46 = value;
	}

	inline static int32_t get_offset_of_optionalInt0_47() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___optionalInt0_47)); }
	inline int32_t get_optionalInt0_47() const { return ___optionalInt0_47; }
	inline int32_t* get_address_of_optionalInt0_47() { return &___optionalInt0_47; }
	inline void set_optionalInt0_47(int32_t value)
	{
		___optionalInt0_47 = value;
	}

	inline static int32_t get_offset_of_optionalRotationMode_48() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___optionalRotationMode_48)); }
	inline int32_t get_optionalRotationMode_48() const { return ___optionalRotationMode_48; }
	inline int32_t* get_address_of_optionalRotationMode_48() { return &___optionalRotationMode_48; }
	inline void set_optionalRotationMode_48(int32_t value)
	{
		___optionalRotationMode_48 = value;
	}

	inline static int32_t get_offset_of_optionalScrambleMode_49() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___optionalScrambleMode_49)); }
	inline int32_t get_optionalScrambleMode_49() const { return ___optionalScrambleMode_49; }
	inline int32_t* get_address_of_optionalScrambleMode_49() { return &___optionalScrambleMode_49; }
	inline void set_optionalScrambleMode_49(int32_t value)
	{
		___optionalScrambleMode_49 = value;
	}

	inline static int32_t get_offset_of_optionalString_50() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ___optionalString_50)); }
	inline String_t* get_optionalString_50() const { return ___optionalString_50; }
	inline String_t** get_address_of_optionalString_50() { return &___optionalString_50; }
	inline void set_optionalString_50(String_t* value)
	{
		___optionalString_50 = value;
		Il2CppCodeGenWriteBarrier((&___optionalString_50), value);
	}

	inline static int32_t get_offset_of__tweenCreated_51() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ____tweenCreated_51)); }
	inline bool get__tweenCreated_51() const { return ____tweenCreated_51; }
	inline bool* get_address_of__tweenCreated_51() { return &____tweenCreated_51; }
	inline void set__tweenCreated_51(bool value)
	{
		____tweenCreated_51 = value;
	}

	inline static int32_t get_offset_of__playCount_52() { return static_cast<int32_t>(offsetof(DOTweenAnimation_t3459117967, ____playCount_52)); }
	inline int32_t get__playCount_52() const { return ____playCount_52; }
	inline int32_t* get_address_of__playCount_52() { return &____playCount_52; }
	inline void set__playCount_52(int32_t value)
	{
		____playCount_52 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENANIMATION_T3459117967_H
#ifndef SHARPEN_T1993388953_H
#define SHARPEN_T1993388953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Sharpen
struct  Sharpen_t1993388953  : public BaseEffect_t1187847871
{
public:
	// Colorful.Sharpen/Algorithm Colorful.Sharpen::Mode
	int32_t ___Mode_4;
	// System.Single Colorful.Sharpen::Strength
	float ___Strength_5;
	// System.Single Colorful.Sharpen::Clamp
	float ___Clamp_6;

public:
	inline static int32_t get_offset_of_Mode_4() { return static_cast<int32_t>(offsetof(Sharpen_t1993388953, ___Mode_4)); }
	inline int32_t get_Mode_4() const { return ___Mode_4; }
	inline int32_t* get_address_of_Mode_4() { return &___Mode_4; }
	inline void set_Mode_4(int32_t value)
	{
		___Mode_4 = value;
	}

	inline static int32_t get_offset_of_Strength_5() { return static_cast<int32_t>(offsetof(Sharpen_t1993388953, ___Strength_5)); }
	inline float get_Strength_5() const { return ___Strength_5; }
	inline float* get_address_of_Strength_5() { return &___Strength_5; }
	inline void set_Strength_5(float value)
	{
		___Strength_5 = value;
	}

	inline static int32_t get_offset_of_Clamp_6() { return static_cast<int32_t>(offsetof(Sharpen_t1993388953, ___Clamp_6)); }
	inline float get_Clamp_6() const { return ___Clamp_6; }
	inline float* get_address_of_Clamp_6() { return &___Clamp_6; }
	inline void set_Clamp_6(float value)
	{
		___Clamp_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHARPEN_T1993388953_H
#ifndef LOFIPALETTE_T3780803221_H
#define LOFIPALETTE_T3780803221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.LoFiPalette
struct  LoFiPalette_t3780803221  : public LookupFilter3D_t3361124730
{
public:
	// Colorful.LoFiPalette/Preset Colorful.LoFiPalette::Palette
	int32_t ___Palette_12;
	// System.Boolean Colorful.LoFiPalette::Pixelize
	bool ___Pixelize_13;
	// System.Single Colorful.LoFiPalette::PixelSize
	float ___PixelSize_14;
	// Colorful.LoFiPalette/Preset Colorful.LoFiPalette::m_CurrentPreset
	int32_t ___m_CurrentPreset_15;

public:
	inline static int32_t get_offset_of_Palette_12() { return static_cast<int32_t>(offsetof(LoFiPalette_t3780803221, ___Palette_12)); }
	inline int32_t get_Palette_12() const { return ___Palette_12; }
	inline int32_t* get_address_of_Palette_12() { return &___Palette_12; }
	inline void set_Palette_12(int32_t value)
	{
		___Palette_12 = value;
	}

	inline static int32_t get_offset_of_Pixelize_13() { return static_cast<int32_t>(offsetof(LoFiPalette_t3780803221, ___Pixelize_13)); }
	inline bool get_Pixelize_13() const { return ___Pixelize_13; }
	inline bool* get_address_of_Pixelize_13() { return &___Pixelize_13; }
	inline void set_Pixelize_13(bool value)
	{
		___Pixelize_13 = value;
	}

	inline static int32_t get_offset_of_PixelSize_14() { return static_cast<int32_t>(offsetof(LoFiPalette_t3780803221, ___PixelSize_14)); }
	inline float get_PixelSize_14() const { return ___PixelSize_14; }
	inline float* get_address_of_PixelSize_14() { return &___PixelSize_14; }
	inline void set_PixelSize_14(float value)
	{
		___PixelSize_14 = value;
	}

	inline static int32_t get_offset_of_m_CurrentPreset_15() { return static_cast<int32_t>(offsetof(LoFiPalette_t3780803221, ___m_CurrentPreset_15)); }
	inline int32_t get_m_CurrentPreset_15() const { return ___m_CurrentPreset_15; }
	inline int32_t* get_address_of_m_CurrentPreset_15() { return &___m_CurrentPreset_15; }
	inline void set_m_CurrentPreset_15(int32_t value)
	{
		___m_CurrentPreset_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOFIPALETTE_T3780803221_H
#ifndef LOOKUPFILTER_T4073759679_H
#define LOOKUPFILTER_T4073759679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.LookupFilter
struct  LookupFilter_t4073759679  : public BaseEffect_t1187847871
{
public:
	// UnityEngine.Texture Colorful.LookupFilter::LookupTexture
	Texture_t3661962703 * ___LookupTexture_4;
	// System.Single Colorful.LookupFilter::Amount
	float ___Amount_5;

public:
	inline static int32_t get_offset_of_LookupTexture_4() { return static_cast<int32_t>(offsetof(LookupFilter_t4073759679, ___LookupTexture_4)); }
	inline Texture_t3661962703 * get_LookupTexture_4() const { return ___LookupTexture_4; }
	inline Texture_t3661962703 ** get_address_of_LookupTexture_4() { return &___LookupTexture_4; }
	inline void set_LookupTexture_4(Texture_t3661962703 * value)
	{
		___LookupTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___LookupTexture_4), value);
	}

	inline static int32_t get_offset_of_Amount_5() { return static_cast<int32_t>(offsetof(LookupFilter_t4073759679, ___Amount_5)); }
	inline float get_Amount_5() const { return ___Amount_5; }
	inline float* get_address_of_Amount_5() { return &___Amount_5; }
	inline void set_Amount_5(float value)
	{
		___Amount_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOKUPFILTER_T4073759679_H
#ifndef NEGATIVE_T2741149473_H
#define NEGATIVE_T2741149473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Negative
struct  Negative_t2741149473  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.Negative::Amount
	float ___Amount_4;

public:
	inline static int32_t get_offset_of_Amount_4() { return static_cast<int32_t>(offsetof(Negative_t2741149473, ___Amount_4)); }
	inline float get_Amount_4() const { return ___Amount_4; }
	inline float* get_address_of_Amount_4() { return &___Amount_4; }
	inline void set_Amount_4(float value)
	{
		___Amount_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEGATIVE_T2741149473_H
#ifndef NOISE_T3271901434_H
#define NOISE_T3271901434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Noise
struct  Noise_t3271901434  : public BaseEffect_t1187847871
{
public:
	// Colorful.Noise/ColorMode Colorful.Noise::Mode
	int32_t ___Mode_4;
	// System.Boolean Colorful.Noise::Animate
	bool ___Animate_5;
	// System.Single Colorful.Noise::Seed
	float ___Seed_6;
	// System.Single Colorful.Noise::Strength
	float ___Strength_7;
	// System.Single Colorful.Noise::LumContribution
	float ___LumContribution_8;

public:
	inline static int32_t get_offset_of_Mode_4() { return static_cast<int32_t>(offsetof(Noise_t3271901434, ___Mode_4)); }
	inline int32_t get_Mode_4() const { return ___Mode_4; }
	inline int32_t* get_address_of_Mode_4() { return &___Mode_4; }
	inline void set_Mode_4(int32_t value)
	{
		___Mode_4 = value;
	}

	inline static int32_t get_offset_of_Animate_5() { return static_cast<int32_t>(offsetof(Noise_t3271901434, ___Animate_5)); }
	inline bool get_Animate_5() const { return ___Animate_5; }
	inline bool* get_address_of_Animate_5() { return &___Animate_5; }
	inline void set_Animate_5(bool value)
	{
		___Animate_5 = value;
	}

	inline static int32_t get_offset_of_Seed_6() { return static_cast<int32_t>(offsetof(Noise_t3271901434, ___Seed_6)); }
	inline float get_Seed_6() const { return ___Seed_6; }
	inline float* get_address_of_Seed_6() { return &___Seed_6; }
	inline void set_Seed_6(float value)
	{
		___Seed_6 = value;
	}

	inline static int32_t get_offset_of_Strength_7() { return static_cast<int32_t>(offsetof(Noise_t3271901434, ___Strength_7)); }
	inline float get_Strength_7() const { return ___Strength_7; }
	inline float* get_address_of_Strength_7() { return &___Strength_7; }
	inline void set_Strength_7(float value)
	{
		___Strength_7 = value;
	}

	inline static int32_t get_offset_of_LumContribution_8() { return static_cast<int32_t>(offsetof(Noise_t3271901434, ___LumContribution_8)); }
	inline float get_LumContribution_8() const { return ___LumContribution_8; }
	inline float* get_address_of_LumContribution_8() { return &___LumContribution_8; }
	inline void set_LumContribution_8(float value)
	{
		___LumContribution_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOISE_T3271901434_H
#ifndef PHOTOFILTER_T50036371_H
#define PHOTOFILTER_T50036371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.PhotoFilter
struct  PhotoFilter_t50036371  : public BaseEffect_t1187847871
{
public:
	// UnityEngine.Color Colorful.PhotoFilter::Color
	Color_t2555686324  ___Color_4;
	// System.Single Colorful.PhotoFilter::Density
	float ___Density_5;

public:
	inline static int32_t get_offset_of_Color_4() { return static_cast<int32_t>(offsetof(PhotoFilter_t50036371, ___Color_4)); }
	inline Color_t2555686324  get_Color_4() const { return ___Color_4; }
	inline Color_t2555686324 * get_address_of_Color_4() { return &___Color_4; }
	inline void set_Color_4(Color_t2555686324  value)
	{
		___Color_4 = value;
	}

	inline static int32_t get_offset_of_Density_5() { return static_cast<int32_t>(offsetof(PhotoFilter_t50036371, ___Density_5)); }
	inline float get_Density_5() const { return ___Density_5; }
	inline float* get_address_of_Density_5() { return &___Density_5; }
	inline void set_Density_5(float value)
	{
		___Density_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTOFILTER_T50036371_H
#ifndef PIXELATE_T1523178201_H
#define PIXELATE_T1523178201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Pixelate
struct  Pixelate_t1523178201  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.Pixelate::Scale
	float ___Scale_4;
	// System.Boolean Colorful.Pixelate::AutomaticRatio
	bool ___AutomaticRatio_5;
	// System.Single Colorful.Pixelate::Ratio
	float ___Ratio_6;
	// Colorful.Pixelate/SizeMode Colorful.Pixelate::Mode
	int32_t ___Mode_7;

public:
	inline static int32_t get_offset_of_Scale_4() { return static_cast<int32_t>(offsetof(Pixelate_t1523178201, ___Scale_4)); }
	inline float get_Scale_4() const { return ___Scale_4; }
	inline float* get_address_of_Scale_4() { return &___Scale_4; }
	inline void set_Scale_4(float value)
	{
		___Scale_4 = value;
	}

	inline static int32_t get_offset_of_AutomaticRatio_5() { return static_cast<int32_t>(offsetof(Pixelate_t1523178201, ___AutomaticRatio_5)); }
	inline bool get_AutomaticRatio_5() const { return ___AutomaticRatio_5; }
	inline bool* get_address_of_AutomaticRatio_5() { return &___AutomaticRatio_5; }
	inline void set_AutomaticRatio_5(bool value)
	{
		___AutomaticRatio_5 = value;
	}

	inline static int32_t get_offset_of_Ratio_6() { return static_cast<int32_t>(offsetof(Pixelate_t1523178201, ___Ratio_6)); }
	inline float get_Ratio_6() const { return ___Ratio_6; }
	inline float* get_address_of_Ratio_6() { return &___Ratio_6; }
	inline void set_Ratio_6(float value)
	{
		___Ratio_6 = value;
	}

	inline static int32_t get_offset_of_Mode_7() { return static_cast<int32_t>(offsetof(Pixelate_t1523178201, ___Mode_7)); }
	inline int32_t get_Mode_7() const { return ___Mode_7; }
	inline int32_t* get_address_of_Mode_7() { return &___Mode_7; }
	inline void set_Mode_7(int32_t value)
	{
		___Mode_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIXELATE_T1523178201_H
#ifndef SHADOWSMIDTONESHIGHLIGHTS_T2681697010_H
#define SHADOWSMIDTONESHIGHLIGHTS_T2681697010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.ShadowsMidtonesHighlights
struct  ShadowsMidtonesHighlights_t2681697010  : public BaseEffect_t1187847871
{
public:
	// Colorful.ShadowsMidtonesHighlights/ColorMode Colorful.ShadowsMidtonesHighlights::Mode
	int32_t ___Mode_4;
	// UnityEngine.Color Colorful.ShadowsMidtonesHighlights::Shadows
	Color_t2555686324  ___Shadows_5;
	// UnityEngine.Color Colorful.ShadowsMidtonesHighlights::Midtones
	Color_t2555686324  ___Midtones_6;
	// UnityEngine.Color Colorful.ShadowsMidtonesHighlights::Highlights
	Color_t2555686324  ___Highlights_7;
	// System.Single Colorful.ShadowsMidtonesHighlights::Amount
	float ___Amount_8;

public:
	inline static int32_t get_offset_of_Mode_4() { return static_cast<int32_t>(offsetof(ShadowsMidtonesHighlights_t2681697010, ___Mode_4)); }
	inline int32_t get_Mode_4() const { return ___Mode_4; }
	inline int32_t* get_address_of_Mode_4() { return &___Mode_4; }
	inline void set_Mode_4(int32_t value)
	{
		___Mode_4 = value;
	}

	inline static int32_t get_offset_of_Shadows_5() { return static_cast<int32_t>(offsetof(ShadowsMidtonesHighlights_t2681697010, ___Shadows_5)); }
	inline Color_t2555686324  get_Shadows_5() const { return ___Shadows_5; }
	inline Color_t2555686324 * get_address_of_Shadows_5() { return &___Shadows_5; }
	inline void set_Shadows_5(Color_t2555686324  value)
	{
		___Shadows_5 = value;
	}

	inline static int32_t get_offset_of_Midtones_6() { return static_cast<int32_t>(offsetof(ShadowsMidtonesHighlights_t2681697010, ___Midtones_6)); }
	inline Color_t2555686324  get_Midtones_6() const { return ___Midtones_6; }
	inline Color_t2555686324 * get_address_of_Midtones_6() { return &___Midtones_6; }
	inline void set_Midtones_6(Color_t2555686324  value)
	{
		___Midtones_6 = value;
	}

	inline static int32_t get_offset_of_Highlights_7() { return static_cast<int32_t>(offsetof(ShadowsMidtonesHighlights_t2681697010, ___Highlights_7)); }
	inline Color_t2555686324  get_Highlights_7() const { return ___Highlights_7; }
	inline Color_t2555686324 * get_address_of_Highlights_7() { return &___Highlights_7; }
	inline void set_Highlights_7(Color_t2555686324  value)
	{
		___Highlights_7 = value;
	}

	inline static int32_t get_offset_of_Amount_8() { return static_cast<int32_t>(offsetof(ShadowsMidtonesHighlights_t2681697010, ___Amount_8)); }
	inline float get_Amount_8() const { return ___Amount_8; }
	inline float* get_address_of_Amount_8() { return &___Amount_8; }
	inline void set_Amount_8(float value)
	{
		___Amount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOWSMIDTONESHIGHLIGHTS_T2681697010_H
#ifndef SCURVECONTRAST_T1916604434_H
#define SCURVECONTRAST_T1916604434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.SCurveContrast
struct  SCurveContrast_t1916604434  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.SCurveContrast::RedSteepness
	float ___RedSteepness_4;
	// System.Single Colorful.SCurveContrast::RedGamma
	float ___RedGamma_5;
	// System.Single Colorful.SCurveContrast::GreenSteepness
	float ___GreenSteepness_6;
	// System.Single Colorful.SCurveContrast::GreenGamma
	float ___GreenGamma_7;
	// System.Single Colorful.SCurveContrast::BlueSteepness
	float ___BlueSteepness_8;
	// System.Single Colorful.SCurveContrast::BlueGamma
	float ___BlueGamma_9;

public:
	inline static int32_t get_offset_of_RedSteepness_4() { return static_cast<int32_t>(offsetof(SCurveContrast_t1916604434, ___RedSteepness_4)); }
	inline float get_RedSteepness_4() const { return ___RedSteepness_4; }
	inline float* get_address_of_RedSteepness_4() { return &___RedSteepness_4; }
	inline void set_RedSteepness_4(float value)
	{
		___RedSteepness_4 = value;
	}

	inline static int32_t get_offset_of_RedGamma_5() { return static_cast<int32_t>(offsetof(SCurveContrast_t1916604434, ___RedGamma_5)); }
	inline float get_RedGamma_5() const { return ___RedGamma_5; }
	inline float* get_address_of_RedGamma_5() { return &___RedGamma_5; }
	inline void set_RedGamma_5(float value)
	{
		___RedGamma_5 = value;
	}

	inline static int32_t get_offset_of_GreenSteepness_6() { return static_cast<int32_t>(offsetof(SCurveContrast_t1916604434, ___GreenSteepness_6)); }
	inline float get_GreenSteepness_6() const { return ___GreenSteepness_6; }
	inline float* get_address_of_GreenSteepness_6() { return &___GreenSteepness_6; }
	inline void set_GreenSteepness_6(float value)
	{
		___GreenSteepness_6 = value;
	}

	inline static int32_t get_offset_of_GreenGamma_7() { return static_cast<int32_t>(offsetof(SCurveContrast_t1916604434, ___GreenGamma_7)); }
	inline float get_GreenGamma_7() const { return ___GreenGamma_7; }
	inline float* get_address_of_GreenGamma_7() { return &___GreenGamma_7; }
	inline void set_GreenGamma_7(float value)
	{
		___GreenGamma_7 = value;
	}

	inline static int32_t get_offset_of_BlueSteepness_8() { return static_cast<int32_t>(offsetof(SCurveContrast_t1916604434, ___BlueSteepness_8)); }
	inline float get_BlueSteepness_8() const { return ___BlueSteepness_8; }
	inline float* get_address_of_BlueSteepness_8() { return &___BlueSteepness_8; }
	inline void set_BlueSteepness_8(float value)
	{
		___BlueSteepness_8 = value;
	}

	inline static int32_t get_offset_of_BlueGamma_9() { return static_cast<int32_t>(offsetof(SCurveContrast_t1916604434, ___BlueGamma_9)); }
	inline float get_BlueGamma_9() const { return ___BlueGamma_9; }
	inline float* get_address_of_BlueGamma_9() { return &___BlueGamma_9; }
	inline void set_BlueGamma_9(float value)
	{
		___BlueGamma_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCURVECONTRAST_T1916604434_H
#ifndef RGBSPLIT_T656033929_H
#define RGBSPLIT_T656033929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.RGBSplit
struct  RGBSplit_t656033929  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.RGBSplit::Amount
	float ___Amount_4;
	// System.Single Colorful.RGBSplit::Angle
	float ___Angle_5;

public:
	inline static int32_t get_offset_of_Amount_4() { return static_cast<int32_t>(offsetof(RGBSplit_t656033929, ___Amount_4)); }
	inline float get_Amount_4() const { return ___Amount_4; }
	inline float* get_address_of_Amount_4() { return &___Amount_4; }
	inline void set_Amount_4(float value)
	{
		___Amount_4 = value;
	}

	inline static int32_t get_offset_of_Angle_5() { return static_cast<int32_t>(offsetof(RGBSplit_t656033929, ___Angle_5)); }
	inline float get_Angle_5() const { return ___Angle_5; }
	inline float* get_address_of_Angle_5() { return &___Angle_5; }
	inline void set_Angle_5(float value)
	{
		___Angle_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RGBSPLIT_T656033929_H
#ifndef RADIALBLUR_T147563976_H
#define RADIALBLUR_T147563976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.RadialBlur
struct  RadialBlur_t147563976  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.RadialBlur::Strength
	float ___Strength_4;
	// System.Int32 Colorful.RadialBlur::Samples
	int32_t ___Samples_5;
	// UnityEngine.Vector2 Colorful.RadialBlur::Center
	Vector2_t2156229523  ___Center_6;
	// Colorful.RadialBlur/QualityPreset Colorful.RadialBlur::Quality
	int32_t ___Quality_7;
	// System.Single Colorful.RadialBlur::Sharpness
	float ___Sharpness_8;
	// System.Single Colorful.RadialBlur::Darkness
	float ___Darkness_9;
	// System.Boolean Colorful.RadialBlur::EnableVignette
	bool ___EnableVignette_10;

public:
	inline static int32_t get_offset_of_Strength_4() { return static_cast<int32_t>(offsetof(RadialBlur_t147563976, ___Strength_4)); }
	inline float get_Strength_4() const { return ___Strength_4; }
	inline float* get_address_of_Strength_4() { return &___Strength_4; }
	inline void set_Strength_4(float value)
	{
		___Strength_4 = value;
	}

	inline static int32_t get_offset_of_Samples_5() { return static_cast<int32_t>(offsetof(RadialBlur_t147563976, ___Samples_5)); }
	inline int32_t get_Samples_5() const { return ___Samples_5; }
	inline int32_t* get_address_of_Samples_5() { return &___Samples_5; }
	inline void set_Samples_5(int32_t value)
	{
		___Samples_5 = value;
	}

	inline static int32_t get_offset_of_Center_6() { return static_cast<int32_t>(offsetof(RadialBlur_t147563976, ___Center_6)); }
	inline Vector2_t2156229523  get_Center_6() const { return ___Center_6; }
	inline Vector2_t2156229523 * get_address_of_Center_6() { return &___Center_6; }
	inline void set_Center_6(Vector2_t2156229523  value)
	{
		___Center_6 = value;
	}

	inline static int32_t get_offset_of_Quality_7() { return static_cast<int32_t>(offsetof(RadialBlur_t147563976, ___Quality_7)); }
	inline int32_t get_Quality_7() const { return ___Quality_7; }
	inline int32_t* get_address_of_Quality_7() { return &___Quality_7; }
	inline void set_Quality_7(int32_t value)
	{
		___Quality_7 = value;
	}

	inline static int32_t get_offset_of_Sharpness_8() { return static_cast<int32_t>(offsetof(RadialBlur_t147563976, ___Sharpness_8)); }
	inline float get_Sharpness_8() const { return ___Sharpness_8; }
	inline float* get_address_of_Sharpness_8() { return &___Sharpness_8; }
	inline void set_Sharpness_8(float value)
	{
		___Sharpness_8 = value;
	}

	inline static int32_t get_offset_of_Darkness_9() { return static_cast<int32_t>(offsetof(RadialBlur_t147563976, ___Darkness_9)); }
	inline float get_Darkness_9() const { return ___Darkness_9; }
	inline float* get_address_of_Darkness_9() { return &___Darkness_9; }
	inline void set_Darkness_9(float value)
	{
		___Darkness_9 = value;
	}

	inline static int32_t get_offset_of_EnableVignette_10() { return static_cast<int32_t>(offsetof(RadialBlur_t147563976, ___EnableVignette_10)); }
	inline bool get_EnableVignette_10() const { return ___EnableVignette_10; }
	inline bool* get_address_of_EnableVignette_10() { return &___EnableVignette_10; }
	inline void set_EnableVignette_10(bool value)
	{
		___EnableVignette_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RADIALBLUR_T147563976_H
#ifndef POSTERIZE_T3175588324_H
#define POSTERIZE_T3175588324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Posterize
struct  Posterize_t3175588324  : public BaseEffect_t1187847871
{
public:
	// System.Int32 Colorful.Posterize::Levels
	int32_t ___Levels_4;
	// System.Single Colorful.Posterize::Amount
	float ___Amount_5;
	// System.Boolean Colorful.Posterize::LuminosityOnly
	bool ___LuminosityOnly_6;

public:
	inline static int32_t get_offset_of_Levels_4() { return static_cast<int32_t>(offsetof(Posterize_t3175588324, ___Levels_4)); }
	inline int32_t get_Levels_4() const { return ___Levels_4; }
	inline int32_t* get_address_of_Levels_4() { return &___Levels_4; }
	inline void set_Levels_4(int32_t value)
	{
		___Levels_4 = value;
	}

	inline static int32_t get_offset_of_Amount_5() { return static_cast<int32_t>(offsetof(Posterize_t3175588324, ___Amount_5)); }
	inline float get_Amount_5() const { return ___Amount_5; }
	inline float* get_address_of_Amount_5() { return &___Amount_5; }
	inline void set_Amount_5(float value)
	{
		___Amount_5 = value;
	}

	inline static int32_t get_offset_of_LuminosityOnly_6() { return static_cast<int32_t>(offsetof(Posterize_t3175588324, ___LuminosityOnly_6)); }
	inline bool get_LuminosityOnly_6() const { return ___LuminosityOnly_6; }
	inline bool* get_address_of_LuminosityOnly_6() { return &___LuminosityOnly_6; }
	inline void set_LuminosityOnly_6(bool value)
	{
		___LuminosityOnly_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTERIZE_T3175588324_H
#ifndef PIXELMATRIX_T927513071_H
#define PIXELMATRIX_T927513071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.PixelMatrix
struct  PixelMatrix_t927513071  : public BaseEffect_t1187847871
{
public:
	// System.Int32 Colorful.PixelMatrix::Size
	int32_t ___Size_4;
	// System.Single Colorful.PixelMatrix::Brightness
	float ___Brightness_5;
	// System.Boolean Colorful.PixelMatrix::BlackBorder
	bool ___BlackBorder_6;

public:
	inline static int32_t get_offset_of_Size_4() { return static_cast<int32_t>(offsetof(PixelMatrix_t927513071, ___Size_4)); }
	inline int32_t get_Size_4() const { return ___Size_4; }
	inline int32_t* get_address_of_Size_4() { return &___Size_4; }
	inline void set_Size_4(int32_t value)
	{
		___Size_4 = value;
	}

	inline static int32_t get_offset_of_Brightness_5() { return static_cast<int32_t>(offsetof(PixelMatrix_t927513071, ___Brightness_5)); }
	inline float get_Brightness_5() const { return ___Brightness_5; }
	inline float* get_address_of_Brightness_5() { return &___Brightness_5; }
	inline void set_Brightness_5(float value)
	{
		___Brightness_5 = value;
	}

	inline static int32_t get_offset_of_BlackBorder_6() { return static_cast<int32_t>(offsetof(PixelMatrix_t927513071, ___BlackBorder_6)); }
	inline bool get_BlackBorder_6() const { return ___BlackBorder_6; }
	inline bool* get_address_of_BlackBorder_6() { return &___BlackBorder_6; }
	inline void set_BlackBorder_6(bool value)
	{
		___BlackBorder_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIXELMATRIX_T927513071_H
#ifndef TVVIGNETTE_T3643404851_H
#define TVVIGNETTE_T3643404851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.TVVignette
struct  TVVignette_t3643404851  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.TVVignette::Size
	float ___Size_4;
	// System.Single Colorful.TVVignette::Offset
	float ___Offset_5;

public:
	inline static int32_t get_offset_of_Size_4() { return static_cast<int32_t>(offsetof(TVVignette_t3643404851, ___Size_4)); }
	inline float get_Size_4() const { return ___Size_4; }
	inline float* get_address_of_Size_4() { return &___Size_4; }
	inline void set_Size_4(float value)
	{
		___Size_4 = value;
	}

	inline static int32_t get_offset_of_Offset_5() { return static_cast<int32_t>(offsetof(TVVignette_t3643404851, ___Offset_5)); }
	inline float get_Offset_5() const { return ___Offset_5; }
	inline float* get_address_of_Offset_5() { return &___Offset_5; }
	inline void set_Offset_5(float value)
	{
		___Offset_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TVVIGNETTE_T3643404851_H
#ifndef VINTAGE_T2551956078_H
#define VINTAGE_T2551956078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Vintage
struct  Vintage_t2551956078  : public LookupFilter_t4073759679
{
public:
	// Colorful.Vintage/InstragramFilter Colorful.Vintage::Filter
	int32_t ___Filter_6;
	// Colorful.Vintage/InstragramFilter Colorful.Vintage::m_CurrentFilter
	int32_t ___m_CurrentFilter_7;

public:
	inline static int32_t get_offset_of_Filter_6() { return static_cast<int32_t>(offsetof(Vintage_t2551956078, ___Filter_6)); }
	inline int32_t get_Filter_6() const { return ___Filter_6; }
	inline int32_t* get_address_of_Filter_6() { return &___Filter_6; }
	inline void set_Filter_6(int32_t value)
	{
		___Filter_6 = value;
	}

	inline static int32_t get_offset_of_m_CurrentFilter_7() { return static_cast<int32_t>(offsetof(Vintage_t2551956078, ___m_CurrentFilter_7)); }
	inline int32_t get_m_CurrentFilter_7() const { return ___m_CurrentFilter_7; }
	inline int32_t* get_address_of_m_CurrentFilter_7() { return &___m_CurrentFilter_7; }
	inline void set_m_CurrentFilter_7(int32_t value)
	{
		___m_CurrentFilter_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VINTAGE_T2551956078_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3400 = { sizeof (ChannelSwapper_t1718792779), -1, sizeof(ChannelSwapper_t1718792779_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3400[4] = 
{
	ChannelSwapper_t1718792779::get_offset_of_RedSource_4(),
	ChannelSwapper_t1718792779::get_offset_of_GreenSource_5(),
	ChannelSwapper_t1718792779::get_offset_of_BlueSource_6(),
	ChannelSwapper_t1718792779_StaticFields::get_offset_of_m_Channels_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3401 = { sizeof (Channel_t1767287641)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3401[4] = 
{
	Channel_t1767287641::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3402 = { sizeof (ChromaticAberration_t2529873745), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3402[4] = 
{
	ChromaticAberration_t2529873745::get_offset_of_RedRefraction_4(),
	ChromaticAberration_t2529873745::get_offset_of_GreenRefraction_5(),
	ChromaticAberration_t2529873745::get_offset_of_BlueRefraction_6(),
	ChromaticAberration_t2529873745::get_offset_of_PreserveAlpha_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3403 = { sizeof (ComicBook_t2243422766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3403[12] = 
{
	ComicBook_t2243422766::get_offset_of_StripAngle_4(),
	ComicBook_t2243422766::get_offset_of_StripDensity_5(),
	ComicBook_t2243422766::get_offset_of_StripThickness_6(),
	ComicBook_t2243422766::get_offset_of_StripLimits_7(),
	ComicBook_t2243422766::get_offset_of_StripInnerColor_8(),
	ComicBook_t2243422766::get_offset_of_StripOuterColor_9(),
	ComicBook_t2243422766::get_offset_of_FillColor_10(),
	ComicBook_t2243422766::get_offset_of_BackgroundColor_11(),
	ComicBook_t2243422766::get_offset_of_EdgeDetection_12(),
	ComicBook_t2243422766::get_offset_of_EdgeThreshold_13(),
	ComicBook_t2243422766::get_offset_of_EdgeColor_14(),
	ComicBook_t2243422766::get_offset_of_Amount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3404 = { sizeof (ContrastGain_t1677431729), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3404[1] = 
{
	ContrastGain_t1677431729::get_offset_of_Gain_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3405 = { sizeof (ContrastVignette_t2834033980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3405[6] = 
{
	ContrastVignette_t2834033980::get_offset_of_Center_4(),
	ContrastVignette_t2834033980::get_offset_of_Sharpness_5(),
	ContrastVignette_t2834033980::get_offset_of_Darkness_6(),
	ContrastVignette_t2834033980::get_offset_of_Contrast_7(),
	ContrastVignette_t2834033980::get_offset_of_ContrastCoeff_8(),
	ContrastVignette_t2834033980::get_offset_of_EdgeBlending_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3406 = { sizeof (Convolution3x3_t1353237119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3406[5] = 
{
	Convolution3x3_t1353237119::get_offset_of_KernelTop_4(),
	Convolution3x3_t1353237119::get_offset_of_KernelMiddle_5(),
	Convolution3x3_t1353237119::get_offset_of_KernelBottom_6(),
	Convolution3x3_t1353237119::get_offset_of_Divisor_7(),
	Convolution3x3_t1353237119::get_offset_of_Amount_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3407 = { sizeof (CrossStitch_t1042481064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3407[4] = 
{
	CrossStitch_t1042481064::get_offset_of_Size_4(),
	CrossStitch_t1042481064::get_offset_of_Brightness_5(),
	CrossStitch_t1042481064::get_offset_of_Invert_6(),
	CrossStitch_t1042481064::get_offset_of_Pixelize_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3408 = { sizeof (DirectionalBlur_t13325309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3408[4] = 
{
	DirectionalBlur_t13325309::get_offset_of_Quality_4(),
	DirectionalBlur_t13325309::get_offset_of_Samples_5(),
	DirectionalBlur_t13325309::get_offset_of_Strength_6(),
	DirectionalBlur_t13325309::get_offset_of_Angle_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3409 = { sizeof (QualityPreset_t119956193)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3409[5] = 
{
	QualityPreset_t119956193::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3410 = { sizeof (Dithering_t358002770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3410[7] = 
{
	Dithering_t358002770::get_offset_of_ShowOriginal_4(),
	Dithering_t358002770::get_offset_of_ConvertToGrayscale_5(),
	Dithering_t358002770::get_offset_of_RedLuminance_6(),
	Dithering_t358002770::get_offset_of_GreenLuminance_7(),
	Dithering_t358002770::get_offset_of_BlueLuminance_8(),
	Dithering_t358002770::get_offset_of_Amount_9(),
	Dithering_t358002770::get_offset_of_m_DitherPattern_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3411 = { sizeof (DoubleVision_t3179333181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3411[2] = 
{
	DoubleVision_t3179333181::get_offset_of_Displace_4(),
	DoubleVision_t3179333181::get_offset_of_Amount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3412 = { sizeof (DynamicLookup_t4028714612), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3412[9] = 
{
	DynamicLookup_t4028714612::get_offset_of_White_4(),
	DynamicLookup_t4028714612::get_offset_of_Black_5(),
	DynamicLookup_t4028714612::get_offset_of_Red_6(),
	DynamicLookup_t4028714612::get_offset_of_Green_7(),
	DynamicLookup_t4028714612::get_offset_of_Blue_8(),
	DynamicLookup_t4028714612::get_offset_of_Yellow_9(),
	DynamicLookup_t4028714612::get_offset_of_Magenta_10(),
	DynamicLookup_t4028714612::get_offset_of_Cyan_11(),
	DynamicLookup_t4028714612::get_offset_of_Amount_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3413 = { sizeof (FastVignette_t1694713166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3413[5] = 
{
	FastVignette_t1694713166::get_offset_of_Mode_4(),
	FastVignette_t1694713166::get_offset_of_Color_5(),
	FastVignette_t1694713166::get_offset_of_Center_6(),
	FastVignette_t1694713166::get_offset_of_Sharpness_7(),
	FastVignette_t1694713166::get_offset_of_Darkness_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3414 = { sizeof (ColorMode_t3494539450)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3414[4] = 
{
	ColorMode_t3494539450::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3415 = { sizeof (Frost_t3645176307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3415[4] = 
{
	Frost_t3645176307::get_offset_of_Scale_4(),
	Frost_t3645176307::get_offset_of_Sharpness_5(),
	Frost_t3645176307::get_offset_of_Darkness_6(),
	Frost_t3645176307::get_offset_of_EnableVignette_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3416 = { sizeof (GaussianBlur_t3820976243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3416[3] = 
{
	GaussianBlur_t3820976243::get_offset_of_Passes_4(),
	GaussianBlur_t3820976243::get_offset_of_Downscaling_5(),
	GaussianBlur_t3820976243::get_offset_of_Amount_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3417 = { sizeof (Glitch_t3656535212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3417[11] = 
{
	Glitch_t3656535212::get_offset_of_RandomActivation_4(),
	Glitch_t3656535212::get_offset_of_RandomEvery_5(),
	Glitch_t3656535212::get_offset_of_RandomDuration_6(),
	Glitch_t3656535212::get_offset_of_Mode_7(),
	Glitch_t3656535212::get_offset_of_SettingsInterferences_8(),
	Glitch_t3656535212::get_offset_of_SettingsTearing_9(),
	Glitch_t3656535212::get_offset_of_m_Activated_10(),
	Glitch_t3656535212::get_offset_of_m_EveryTimer_11(),
	Glitch_t3656535212::get_offset_of_m_EveryTimerEnd_12(),
	Glitch_t3656535212::get_offset_of_m_DurationTimer_13(),
	Glitch_t3656535212::get_offset_of_m_DurationTimerEnd_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3418 = { sizeof (GlitchingMode_t3305168335)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3418[4] = 
{
	GlitchingMode_t3305168335::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3419 = { sizeof (InterferenceSettings_t2118005662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3419[3] = 
{
	InterferenceSettings_t2118005662::get_offset_of_Speed_0(),
	InterferenceSettings_t2118005662::get_offset_of_Density_1(),
	InterferenceSettings_t2118005662::get_offset_of_MaxDisplacement_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3420 = { sizeof (TearingSettings_t1139179787), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3420[6] = 
{
	TearingSettings_t1139179787::get_offset_of_Speed_0(),
	TearingSettings_t1139179787::get_offset_of_Intensity_1(),
	TearingSettings_t1139179787::get_offset_of_MaxDisplacement_2(),
	TearingSettings_t1139179787::get_offset_of_AllowFlipping_3(),
	TearingSettings_t1139179787::get_offset_of_YuvColorBleeding_4(),
	TearingSettings_t1139179787::get_offset_of_YuvOffset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3421 = { sizeof (GradientRamp_t513234030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3421[2] = 
{
	GradientRamp_t513234030::get_offset_of_RampTexture_4(),
	GradientRamp_t513234030::get_offset_of_Amount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3422 = { sizeof (GradientRampDynamic_t2043771246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3422[3] = 
{
	GradientRampDynamic_t2043771246::get_offset_of_Ramp_4(),
	GradientRampDynamic_t2043771246::get_offset_of_Amount_5(),
	GradientRampDynamic_t2043771246::get_offset_of_m_RampTexture_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3423 = { sizeof (GrainyBlur_t745788970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3423[2] = 
{
	GrainyBlur_t745788970::get_offset_of_Radius_4(),
	GrainyBlur_t745788970::get_offset_of_Samples_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3424 = { sizeof (Grayscale_t3716926545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3424[4] = 
{
	Grayscale_t3716926545::get_offset_of_RedLuminance_4(),
	Grayscale_t3716926545::get_offset_of_GreenLuminance_5(),
	Grayscale_t3716926545::get_offset_of_BlueLuminance_6(),
	Grayscale_t3716926545::get_offset_of_Amount_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3425 = { sizeof (Halftone_t1918305288), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3425[6] = 
{
	Halftone_t1918305288::get_offset_of_Scale_4(),
	Halftone_t1918305288::get_offset_of_DotSize_5(),
	Halftone_t1918305288::get_offset_of_Angle_6(),
	Halftone_t1918305288::get_offset_of_Smoothness_7(),
	Halftone_t1918305288::get_offset_of_Center_8(),
	Halftone_t1918305288::get_offset_of_Desaturate_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3426 = { sizeof (Histogram_t2046042414), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3427 = { sizeof (HueFocus_t3289091382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3427[4] = 
{
	HueFocus_t3289091382::get_offset_of_Hue_4(),
	HueFocus_t3289091382::get_offset_of_Range_5(),
	HueFocus_t3289091382::get_offset_of_Boost_6(),
	HueFocus_t3289091382::get_offset_of_Amount_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3428 = { sizeof (HueSaturationValue_t2444304774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3428[22] = 
{
	HueSaturationValue_t2444304774::get_offset_of_MasterHue_4(),
	HueSaturationValue_t2444304774::get_offset_of_MasterSaturation_5(),
	HueSaturationValue_t2444304774::get_offset_of_MasterValue_6(),
	HueSaturationValue_t2444304774::get_offset_of_RedsHue_7(),
	HueSaturationValue_t2444304774::get_offset_of_RedsSaturation_8(),
	HueSaturationValue_t2444304774::get_offset_of_RedsValue_9(),
	HueSaturationValue_t2444304774::get_offset_of_YellowsHue_10(),
	HueSaturationValue_t2444304774::get_offset_of_YellowsSaturation_11(),
	HueSaturationValue_t2444304774::get_offset_of_YellowsValue_12(),
	HueSaturationValue_t2444304774::get_offset_of_GreensHue_13(),
	HueSaturationValue_t2444304774::get_offset_of_GreensSaturation_14(),
	HueSaturationValue_t2444304774::get_offset_of_GreensValue_15(),
	HueSaturationValue_t2444304774::get_offset_of_CyansHue_16(),
	HueSaturationValue_t2444304774::get_offset_of_CyansSaturation_17(),
	HueSaturationValue_t2444304774::get_offset_of_CyansValue_18(),
	HueSaturationValue_t2444304774::get_offset_of_BluesHue_19(),
	HueSaturationValue_t2444304774::get_offset_of_BluesSaturation_20(),
	HueSaturationValue_t2444304774::get_offset_of_BluesValue_21(),
	HueSaturationValue_t2444304774::get_offset_of_MagentasHue_22(),
	HueSaturationValue_t2444304774::get_offset_of_MagentasSaturation_23(),
	HueSaturationValue_t2444304774::get_offset_of_MagentasValue_24(),
	HueSaturationValue_t2444304774::get_offset_of_AdvancedMode_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3429 = { sizeof (Kuwahara_t240745109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3429[1] = 
{
	Kuwahara_t240745109::get_offset_of_Radius_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3430 = { sizeof (Led_t3330730803), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3430[6] = 
{
	Led_t3330730803::get_offset_of_Scale_4(),
	Led_t3330730803::get_offset_of_Brightness_5(),
	Led_t3330730803::get_offset_of_Shape_6(),
	Led_t3330730803::get_offset_of_AutomaticRatio_7(),
	Led_t3330730803::get_offset_of_Ratio_8(),
	Led_t3330730803::get_offset_of_Mode_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3431 = { sizeof (SizeMode_t1428983497)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3431[3] = 
{
	SizeMode_t1428983497::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3432 = { sizeof (LensDistortionBlur_t2959671745), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3432[5] = 
{
	LensDistortionBlur_t2959671745::get_offset_of_Quality_4(),
	LensDistortionBlur_t2959671745::get_offset_of_Samples_5(),
	LensDistortionBlur_t2959671745::get_offset_of_Distortion_6(),
	LensDistortionBlur_t2959671745::get_offset_of_CubicDistortion_7(),
	LensDistortionBlur_t2959671745::get_offset_of_Scale_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3433 = { sizeof (QualityPreset_t618172025)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3433[5] = 
{
	QualityPreset_t618172025::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3434 = { sizeof (Letterbox_t3911859827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3434[2] = 
{
	Letterbox_t3911859827::get_offset_of_Aspect_4(),
	Letterbox_t3911859827::get_offset_of_FillColor_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3435 = { sizeof (Levels_t1894673157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3435[9] = 
{
	Levels_t1894673157::get_offset_of_Mode_4(),
	Levels_t1894673157::get_offset_of_InputL_5(),
	Levels_t1894673157::get_offset_of_InputR_6(),
	Levels_t1894673157::get_offset_of_InputG_7(),
	Levels_t1894673157::get_offset_of_InputB_8(),
	Levels_t1894673157::get_offset_of_OutputL_9(),
	Levels_t1894673157::get_offset_of_OutputR_10(),
	Levels_t1894673157::get_offset_of_OutputG_11(),
	Levels_t1894673157::get_offset_of_OutputB_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3436 = { sizeof (ColorMode_t803725769)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3436[3] = 
{
	ColorMode_t803725769::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3437 = { sizeof (LoFiPalette_t3780803221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3437[4] = 
{
	LoFiPalette_t3780803221::get_offset_of_Palette_12(),
	LoFiPalette_t3780803221::get_offset_of_Pixelize_13(),
	LoFiPalette_t3780803221::get_offset_of_PixelSize_14(),
	LoFiPalette_t3780803221::get_offset_of_m_CurrentPreset_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3438 = { sizeof (Preset_t400928058)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3438[33] = 
{
	Preset_t400928058::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3439 = { sizeof (LookupFilter_t4073759679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3439[2] = 
{
	LookupFilter_t4073759679::get_offset_of_LookupTexture_4(),
	LookupFilter_t4073759679::get_offset_of_Amount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3440 = { sizeof (LookupFilter3D_t3361124730), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3440[10] = 
{
	LookupFilter3D_t3361124730::get_offset_of_LookupTexture_2(),
	LookupFilter3D_t3361124730::get_offset_of_Amount_3(),
	LookupFilter3D_t3361124730::get_offset_of_ForceCompatibility_4(),
	LookupFilter3D_t3361124730::get_offset_of_m_Lut3D_5(),
	LookupFilter3D_t3361124730::get_offset_of_m_BaseTextureName_6(),
	LookupFilter3D_t3361124730::get_offset_of_m_Use2DLut_7(),
	LookupFilter3D_t3361124730::get_offset_of_Shader2D_8(),
	LookupFilter3D_t3361124730::get_offset_of_Shader3D_9(),
	LookupFilter3D_t3361124730::get_offset_of_m_Material2D_10(),
	LookupFilter3D_t3361124730::get_offset_of_m_Material3D_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3441 = { sizeof (Negative_t2741149473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3441[1] = 
{
	Negative_t2741149473::get_offset_of_Amount_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3442 = { sizeof (Noise_t3271901434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3442[5] = 
{
	Noise_t3271901434::get_offset_of_Mode_4(),
	Noise_t3271901434::get_offset_of_Animate_5(),
	Noise_t3271901434::get_offset_of_Seed_6(),
	Noise_t3271901434::get_offset_of_Strength_7(),
	Noise_t3271901434::get_offset_of_LumContribution_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3443 = { sizeof (ColorMode_t2454795589)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3443[3] = 
{
	ColorMode_t2454795589::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3444 = { sizeof (PhotoFilter_t50036371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3444[2] = 
{
	PhotoFilter_t50036371::get_offset_of_Color_4(),
	PhotoFilter_t50036371::get_offset_of_Density_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3445 = { sizeof (Pixelate_t1523178201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3445[4] = 
{
	Pixelate_t1523178201::get_offset_of_Scale_4(),
	Pixelate_t1523178201::get_offset_of_AutomaticRatio_5(),
	Pixelate_t1523178201::get_offset_of_Ratio_6(),
	Pixelate_t1523178201::get_offset_of_Mode_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3446 = { sizeof (SizeMode_t3498286277)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3446[3] = 
{
	SizeMode_t3498286277::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3447 = { sizeof (PixelMatrix_t927513071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3447[3] = 
{
	PixelMatrix_t927513071::get_offset_of_Size_4(),
	PixelMatrix_t927513071::get_offset_of_Brightness_5(),
	PixelMatrix_t927513071::get_offset_of_BlackBorder_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3448 = { sizeof (Posterize_t3175588324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3448[3] = 
{
	Posterize_t3175588324::get_offset_of_Levels_4(),
	Posterize_t3175588324::get_offset_of_Amount_5(),
	Posterize_t3175588324::get_offset_of_LuminosityOnly_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3449 = { sizeof (RadialBlur_t147563976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3449[7] = 
{
	RadialBlur_t147563976::get_offset_of_Strength_4(),
	RadialBlur_t147563976::get_offset_of_Samples_5(),
	RadialBlur_t147563976::get_offset_of_Center_6(),
	RadialBlur_t147563976::get_offset_of_Quality_7(),
	RadialBlur_t147563976::get_offset_of_Sharpness_8(),
	RadialBlur_t147563976::get_offset_of_Darkness_9(),
	RadialBlur_t147563976::get_offset_of_EnableVignette_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3450 = { sizeof (QualityPreset_t1040903510)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3450[5] = 
{
	QualityPreset_t1040903510::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3451 = { sizeof (RGBSplit_t656033929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3451[2] = 
{
	RGBSplit_t656033929::get_offset_of_Amount_4(),
	RGBSplit_t656033929::get_offset_of_Angle_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3452 = { sizeof (SCurveContrast_t1916604434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3452[6] = 
{
	SCurveContrast_t1916604434::get_offset_of_RedSteepness_4(),
	SCurveContrast_t1916604434::get_offset_of_RedGamma_5(),
	SCurveContrast_t1916604434::get_offset_of_GreenSteepness_6(),
	SCurveContrast_t1916604434::get_offset_of_GreenGamma_7(),
	SCurveContrast_t1916604434::get_offset_of_BlueSteepness_8(),
	SCurveContrast_t1916604434::get_offset_of_BlueGamma_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3453 = { sizeof (ShadowsMidtonesHighlights_t2681697010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3453[5] = 
{
	ShadowsMidtonesHighlights_t2681697010::get_offset_of_Mode_4(),
	ShadowsMidtonesHighlights_t2681697010::get_offset_of_Shadows_5(),
	ShadowsMidtonesHighlights_t2681697010::get_offset_of_Midtones_6(),
	ShadowsMidtonesHighlights_t2681697010::get_offset_of_Highlights_7(),
	ShadowsMidtonesHighlights_t2681697010::get_offset_of_Amount_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3454 = { sizeof (ColorMode_t2800918933)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3454[3] = 
{
	ColorMode_t2800918933::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3455 = { sizeof (Sharpen_t1993388953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3455[3] = 
{
	Sharpen_t1993388953::get_offset_of_Mode_4(),
	Sharpen_t1993388953::get_offset_of_Strength_5(),
	Sharpen_t1993388953::get_offset_of_Clamp_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3456 = { sizeof (Algorithm_t3262665680)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3456[3] = 
{
	Algorithm_t3262665680::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3457 = { sizeof (SmartSaturation_t3817639239), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3457[3] = 
{
	SmartSaturation_t3817639239::get_offset_of_Boost_4(),
	SmartSaturation_t3817639239::get_offset_of_Curve_5(),
	SmartSaturation_t3817639239::get_offset_of__CurveTexture_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3458 = { sizeof (Strokes_t892401832), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3458[10] = 
{
	Strokes_t892401832::get_offset_of_Mode_4(),
	Strokes_t892401832::get_offset_of_Amplitude_5(),
	Strokes_t892401832::get_offset_of_Frequency_6(),
	Strokes_t892401832::get_offset_of_Scaling_7(),
	Strokes_t892401832::get_offset_of_MaxThickness_8(),
	Strokes_t892401832::get_offset_of_Threshold_9(),
	Strokes_t892401832::get_offset_of_Harshness_10(),
	Strokes_t892401832::get_offset_of_RedLuminance_11(),
	Strokes_t892401832::get_offset_of_GreenLuminance_12(),
	Strokes_t892401832::get_offset_of_BlueLuminance_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3459 = { sizeof (ColorMode_t650858100)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3459[7] = 
{
	ColorMode_t650858100::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3460 = { sizeof (Technicolor_t2196134954), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3460[3] = 
{
	Technicolor_t2196134954::get_offset_of_Exposure_4(),
	Technicolor_t2196134954::get_offset_of_Balance_5(),
	Technicolor_t2196134954::get_offset_of_Amount_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3461 = { sizeof (Threshold_t1192544618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3461[3] = 
{
	Threshold_t1192544618::get_offset_of_Value_4(),
	Threshold_t1192544618::get_offset_of_NoiseRange_5(),
	Threshold_t1192544618::get_offset_of_UseNoise_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3462 = { sizeof (TVVignette_t3643404851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3462[2] = 
{
	TVVignette_t3643404851::get_offset_of_Size_4(),
	TVVignette_t3643404851::get_offset_of_Offset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3463 = { sizeof (Vibrance_t2988503215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3463[5] = 
{
	Vibrance_t2988503215::get_offset_of_Amount_4(),
	Vibrance_t2988503215::get_offset_of_RedChannel_5(),
	Vibrance_t2988503215::get_offset_of_GreenChannel_6(),
	Vibrance_t2988503215::get_offset_of_BlueChannel_7(),
	Vibrance_t2988503215::get_offset_of_AdvancedMode_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3464 = { sizeof (Vintage_t2551956078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3464[2] = 
{
	Vintage_t2551956078::get_offset_of_Filter_6(),
	Vintage_t2551956078::get_offset_of_m_CurrentFilter_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3465 = { sizeof (InstragramFilter_t685827072)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3465[29] = 
{
	InstragramFilter_t685827072::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3466 = { sizeof (VintageFast_t3040349303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3466[2] = 
{
	VintageFast_t3040349303::get_offset_of_Filter_12(),
	VintageFast_t3040349303::get_offset_of_m_CurrentFilter_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3467 = { sizeof (WaveDistortion_t3073420756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3467[4] = 
{
	WaveDistortion_t3073420756::get_offset_of_Amplitude_4(),
	WaveDistortion_t3073420756::get_offset_of_Waves_5(),
	WaveDistortion_t3073420756::get_offset_of_ColorGlitch_6(),
	WaveDistortion_t3073420756::get_offset_of_Phase_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3468 = { sizeof (WhiteBalance_t1550391260), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3468[2] = 
{
	WhiteBalance_t1550391260::get_offset_of_White_4(),
	WhiteBalance_t1550391260::get_offset_of_Mode_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3469 = { sizeof (BalanceMode_t686342893)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3469[3] = 
{
	BalanceMode_t686342893::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3470 = { sizeof (Wiggle_t4291210129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3470[6] = 
{
	Wiggle_t4291210129::get_offset_of_Mode_4(),
	Wiggle_t4291210129::get_offset_of_Timer_5(),
	Wiggle_t4291210129::get_offset_of_Speed_6(),
	Wiggle_t4291210129::get_offset_of_Frequency_7(),
	Wiggle_t4291210129::get_offset_of_Amplitude_8(),
	Wiggle_t4291210129::get_offset_of_AutomaticTimer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3471 = { sizeof (Algorithm_t3898688573)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3471[3] = 
{
	Algorithm_t3898688573::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3472 = { sizeof (DOTweenAnimation_t3459117967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3472[34] = 
{
	DOTweenAnimation_t3459117967::get_offset_of_delay_19(),
	DOTweenAnimation_t3459117967::get_offset_of_duration_20(),
	DOTweenAnimation_t3459117967::get_offset_of_easeType_21(),
	DOTweenAnimation_t3459117967::get_offset_of_easeCurve_22(),
	DOTweenAnimation_t3459117967::get_offset_of_loopType_23(),
	DOTweenAnimation_t3459117967::get_offset_of_loops_24(),
	DOTweenAnimation_t3459117967::get_offset_of_id_25(),
	DOTweenAnimation_t3459117967::get_offset_of_isRelative_26(),
	DOTweenAnimation_t3459117967::get_offset_of_isFrom_27(),
	DOTweenAnimation_t3459117967::get_offset_of_isIndependentUpdate_28(),
	DOTweenAnimation_t3459117967::get_offset_of_autoKill_29(),
	DOTweenAnimation_t3459117967::get_offset_of_isActive_30(),
	DOTweenAnimation_t3459117967::get_offset_of_isValid_31(),
	DOTweenAnimation_t3459117967::get_offset_of_target_32(),
	DOTweenAnimation_t3459117967::get_offset_of_animationType_33(),
	DOTweenAnimation_t3459117967::get_offset_of_targetType_34(),
	DOTweenAnimation_t3459117967::get_offset_of_forcedTargetType_35(),
	DOTweenAnimation_t3459117967::get_offset_of_autoPlay_36(),
	DOTweenAnimation_t3459117967::get_offset_of_useTargetAsV3_37(),
	DOTweenAnimation_t3459117967::get_offset_of_endValueFloat_38(),
	DOTweenAnimation_t3459117967::get_offset_of_endValueV3_39(),
	DOTweenAnimation_t3459117967::get_offset_of_endValueV2_40(),
	DOTweenAnimation_t3459117967::get_offset_of_endValueColor_41(),
	DOTweenAnimation_t3459117967::get_offset_of_endValueString_42(),
	DOTweenAnimation_t3459117967::get_offset_of_endValueRect_43(),
	DOTweenAnimation_t3459117967::get_offset_of_endValueTransform_44(),
	DOTweenAnimation_t3459117967::get_offset_of_optionalBool0_45(),
	DOTweenAnimation_t3459117967::get_offset_of_optionalFloat0_46(),
	DOTweenAnimation_t3459117967::get_offset_of_optionalInt0_47(),
	DOTweenAnimation_t3459117967::get_offset_of_optionalRotationMode_48(),
	DOTweenAnimation_t3459117967::get_offset_of_optionalScrambleMode_49(),
	DOTweenAnimation_t3459117967::get_offset_of_optionalString_50(),
	DOTweenAnimation_t3459117967::get_offset_of__tweenCreated_51(),
	DOTweenAnimation_t3459117967::get_offset_of__playCount_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3473 = { sizeof (DOTweenAnimationExtensions_t980608231), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3474 = { sizeof (BlendShapeHelper_t3964592397), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3474[1] = 
{
	BlendShapeHelper_t3964592397::get_offset_of_lastval_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3475 = { sizeof (BlendShapeReader_t1673803692), -1, sizeof(BlendShapeReader_t1673803692_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3475[15] = 
{
	BlendShapeReader_t1673803692_StaticFields::get_offset_of_listBlendShapes_2(),
	BlendShapeReader_t1673803692_StaticFields::get_offset_of_listTimes_3(),
	BlendShapeReader_t1673803692_StaticFields::get_offset_of_listPos_4(),
	BlendShapeReader_t1673803692_StaticFields::get_offset_of_listRot_5(),
	BlendShapeReader_t1673803692::get_offset_of_camRot_6(),
	BlendShapeReader_t1673803692::get_offset_of_playing_7(),
	BlendShapeReader_t1673803692::get_offset_of_time_8(),
	BlendShapeReader_t1673803692::get_offset_of_nextTime_9(),
	BlendShapeReader_t1673803692::get_offset_of_current_10(),
	BlendShapeReader_t1673803692::get_offset_of_teststringurl_11(),
	BlendShapeReader_t1673803692::get_offset_of_useDelegate_12(),
	BlendShapeReader_t1673803692_StaticFields::get_offset_of_SubscribeEachBlendShapeUpdate_13(),
	BlendShapeReader_t1673803692_StaticFields::get_offset_of_SubscribeEachBlendShapeUpdateBasic_14(),
	BlendShapeReader_t1673803692::get_offset_of_inputwait_15(),
	BlendShapeReader_t1673803692::get_offset_of_tHead_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3476 = { sizeof (ProcessEachBlendShapeUpdate_t611652369), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3477 = { sizeof (ProcessEachBlendShapeUpdateBasic_t1021678196), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3478 = { sizeof (U3CActuallyLoadBlendShapeU3Ec__Iterator0_t3550601061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3478[6] = 
{
	U3CActuallyLoadBlendShapeU3Ec__Iterator0_t3550601061::get_offset_of_url_0(),
	U3CActuallyLoadBlendShapeU3Ec__Iterator0_t3550601061::get_offset_of_U3CwU3E__0_1(),
	U3CActuallyLoadBlendShapeU3Ec__Iterator0_t3550601061::get_offset_of_U24this_2(),
	U3CActuallyLoadBlendShapeU3Ec__Iterator0_t3550601061::get_offset_of_U24current_3(),
	U3CActuallyLoadBlendShapeU3Ec__Iterator0_t3550601061::get_offset_of_U24disposing_4(),
	U3CActuallyLoadBlendShapeU3Ec__Iterator0_t3550601061::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3479 = { sizeof (Mathf2_t1422403535), -1, sizeof(Mathf2_t1422403535_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3479[2] = 
{
	Mathf2_t1422403535_StaticFields::get_offset_of_cam_0(),
	Mathf2_t1422403535_StaticFields::get_offset_of_FarFarAway_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3480 = { sizeof (Parse_t3361751035), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3481 = { sizeof (UIInputWait_t3530943742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3481[7] = 
{
	UIInputWait_t3530943742::get_offset_of_inputfield_2(),
	UIInputWait_t3530943742::get_offset_of_spriteWait_3(),
	UIInputWait_t3530943742::get_offset_of_ButtonToDisable_4(),
	UIInputWait_t3530943742::get_offset_of_button_lastSprite_5(),
	UIInputWait_t3530943742::get_offset_of_goWait_6(),
	UIInputWait_t3530943742::get_offset_of_SubscribeWhenStringReceived_7(),
	UIInputWait_t3530943742::get_offset_of_lastSet_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3482 = { sizeof (StringProcessor_t3163369541), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3483 = { sizeof (UIToggleSet_t1796645174), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3483[3] = 
{
	UIToggleSet_t1796645174::get_offset_of_sprite1_0(),
	UIToggleSet_t1796645174::get_offset_of_sprite2_1(),
	UIToggleSet_t1796645174::get_offset_of_img_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3484 = { sizeof (UIToggle_t4192126258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3484[1] = 
{
	UIToggle_t4192126258::get_offset_of_uitogglesets_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3485 = { sizeof (PuppeteerByFace_t3068079294), -1, sizeof(PuppeteerByFace_t3068079294_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3485[5] = 
{
	PuppeteerByFace_t3068079294::get_offset_of_m_session_2(),
	PuppeteerByFace_t3068079294::get_offset_of_currentBlendShapes_3(),
	PuppeteerByFace_t3068079294::get_offset_of_tHypotheticalFace_4(),
	PuppeteerByFace_t3068079294_StaticFields::get_offset_of_SubscribeEachBlendShapeUpdate_5(),
	PuppeteerByFace_t3068079294_StaticFields::get_offset_of_totalFaces_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3486 = { sizeof (ProcessEachBlendShapeUpdate_t1319766324), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3487 = { sizeof (Avataaars_t109144723), -1, sizeof(Avataaars_t109144723_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3487[13] = 
{
	Avataaars_t109144723::get_offset_of_folderName_2(),
	Avataaars_t109144723::get_offset_of_partsAndFolderNames_3(),
	Avataaars_t109144723::get_offset_of_attachmentPointObjs_4(),
	Avataaars_t109144723::get_offset_of_crucialParts_5(),
	Avataaars_t109144723::get_offset_of_emotionsDefinable_6(),
	Avataaars_t109144723_StaticFields::get_offset_of_dicAtach2AvataaarPart_7(),
	Avataaars_t109144723_StaticFields::get_offset_of_dicAttachmentPoint2Transform_8(),
	Avataaars_t109144723_StaticFields::get_offset_of_dicEmo2Part_9(),
	Avataaars_t109144723_StaticFields::get_offset_of_dicTransform2Sprite_10(),
	Avataaars_t109144723_StaticFields::get_offset_of_dicTransform2PartsAndFolderNames_11(),
	Avataaars_t109144723::get_offset_of_currentEmotion_12(),
	Avataaars_t109144723::get_offset_of_tHead_13(),
	Avataaars_t109144723_StaticFields::get_offset_of_appleKeys_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3488 = { sizeof (Emotions_t827797779)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3488[4] = 
{
	Emotions_t827797779::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3489 = { sizeof (AttachmentPoints_t1814348004)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3489[23] = 
{
	AttachmentPoints_t1814348004::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3490 = { sizeof (PartsAndFolderNames_t2905795907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3490[5] = 
{
	PartsAndFolderNames_t2905795907::get_offset_of_attachmentTransform_0(),
	PartsAndFolderNames_t2905795907::get_offset_of_generalClassifier_1(),
	PartsAndFolderNames_t2905795907::get_offset_of_subClassifier_2(),
	PartsAndFolderNames_t2905795907::get_offset_of_folderName_3(),
	PartsAndFolderNames_t2905795907::get_offset_of_colors_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3491 = { sizeof (AttachmentPointMapper_t2251653936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3491[2] = 
{
	AttachmentPointMapper_t2251653936::get_offset_of_attachmentPoints_0(),
	AttachmentPointMapper_t2251653936::get_offset_of_tParent_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3492 = { sizeof (AvataaarPart_t2415594242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3492[14] = 
{
	AvataaarPart_t2415594242::get_offset_of_name_0(),
	AvataaarPart_t2415594242::get_offset_of_sprite_1(),
	AvataaarPart_t2415594242::get_offset_of_anchorPos_2(),
	AvataaarPart_t2415594242::get_offset_of_uniqueSprite_3(),
	AvataaarPart_t2415594242::get_offset_of_baseMultiplier_4(),
	AvataaarPart_t2415594242::get_offset_of_emotionallyDependent_5(),
	AvataaarPart_t2415594242::get_offset_of_emotions_6(),
	AvataaarPart_t2415594242::get_offset_of_attachmentPoint_7(),
	AvataaarPart_t2415594242::get_offset_of_greaterThan_8(),
	AvataaarPart_t2415594242::get_offset_of_threshhold_9(),
	AvataaarPart_t2415594242::get_offset_of_appleKey_10(),
	AvataaarPart_t2415594242::get_offset_of_useThreshhold_11(),
	AvataaarPart_t2415594242::get_offset_of_yScaleMinMax_12(),
	AvataaarPart_t2415594242::get_offset_of_xMult_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3493 = { sizeof (EmotionsDefinable_t547898478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3493[3] = 
{
	EmotionsDefinable_t547898478::get_offset_of_appleKey_0(),
	EmotionsDefinable_t547898478::get_offset_of_threshhold_1(),
	EmotionsDefinable_t547898478::get_offset_of_emotion_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3494 = { sizeof (FaceCustomizer_t3426199831), -1, sizeof(FaceCustomizer_t3426199831_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3494[8] = 
{
	FaceCustomizer_t3426199831::get_offset_of_goUI_Customizable_2(),
	FaceCustomizer_t3426199831::get_offset_of_tSpriteButton_PartContainer_3(),
	FaceCustomizer_t3426199831::get_offset_of_goPrefabButton_4(),
	FaceCustomizer_t3426199831::get_offset_of_buttonWidth_5(),
	FaceCustomizer_t3426199831_StaticFields::get_offset_of_currentlyEditing_6(),
	FaceCustomizer_t3426199831::get_offset_of_tStartLoad_7(),
	FaceCustomizer_t3426199831::get_offset_of_tSpriteButton_ColorContainer_8(),
	FaceCustomizer_t3426199831::get_offset_of_goPrefabButtonColor_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3495 = { sizeof (UI2_t953001989), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3496 = { sizeof (CFX_AutoDestructShuriken_t4057240314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3496[1] = 
{
	CFX_AutoDestructShuriken_t4057240314::get_offset_of_OnlyDeactivate_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3497 = { sizeof (U3CCheckIfAliveU3Ec__Iterator0_t2402465905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3497[4] = 
{
	U3CCheckIfAliveU3Ec__Iterator0_t2402465905::get_offset_of_U24this_0(),
	U3CCheckIfAliveU3Ec__Iterator0_t2402465905::get_offset_of_U24current_1(),
	U3CCheckIfAliveU3Ec__Iterator0_t2402465905::get_offset_of_U24disposing_2(),
	U3CCheckIfAliveU3Ec__Iterator0_t2402465905::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3498 = { sizeof (CFX_AutodestructWhenNoChildren_t1135562420), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3499 = { sizeof (CFX_InspectorHelp_t4185620777), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3499[4] = 
{
	CFX_InspectorHelp_t4185620777::get_offset_of_Locked_2(),
	CFX_InspectorHelp_t4185620777::get_offset_of_Title_3(),
	CFX_InspectorHelp_t4185620777::get_offset_of_HelpText_4(),
	CFX_InspectorHelp_t4185620777::get_offset_of_MsgType_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
