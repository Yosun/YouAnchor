﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken>
struct IList_1_t2853859030;
// Newtonsoft.Json.Linq.JToken
struct JToken_t1038539247;
// Newtonsoft.Json.Linq.JContainer
struct JContainer_t1023664833;
// Newtonsoft.Json.Linq.JTokenType[]
struct JTokenTypeU5BU5D_t1887505675;
// System.String
struct String_t;
// System.Attribute[]
struct AttributeU5BU5D_t1575011174;
// System.ComponentModel.AttributeCollection
struct AttributeCollection_t4221220734;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerator_1_t1471109715;
// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode>
struct List_1_t2041514553;
// System.Xml.Linq.XObject
struct XObject_t1119084474;
// System.Func`2<System.String,System.String>
struct Func_2_t3947292210;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerable_1_t18392136;
// Newtonsoft.Json.Linq.JsonPath.QueryFilter
struct QueryFilter_t1584032927;
// Newtonsoft.Json.Linq.JsonPath.ScanFilter
struct ScanFilter_t1566003751;
// Newtonsoft.Json.Linq.JProperty
struct JProperty_t3803048347;
// Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter
struct ArraySliceFilter_t662461599;
// Newtonsoft.Json.Linq.JArray
struct JArray_t2963978544;
// Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter
struct ArrayIndexFilter_t2739218971;
// Newtonsoft.Json.Linq.JProperty/JPropertyList
struct JPropertyList_t1362261004;
// System.Type
struct Type_t;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Func`2<System.Object,System.Type>
struct Func_2_t1850968970;
// System.Func`1<System.Object>
struct Func_1_t2509852811;
// Newtonsoft.Json.Serialization.ErrorContext
struct ErrorContext_t3532118581;
// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object>
struct BidirectionalDictionary_2_t2019708942;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t1424496335;
// Newtonsoft.Json.Serialization.ITraceWriter
struct ITraceWriter_t2623073143;
// Newtonsoft.Json.Serialization.JsonSerializerProxy
struct JsonSerializerProxy_t657065764;
// System.Xml.XmlNode
struct XmlNode_t3767805227;
// Newtonsoft.Json.Serialization.JsonProperty
struct JsonProperty_t1405154176;
// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,System.String>
struct Func_2_t705144599;
// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonProperty>
struct Func_2_t262848086;
// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>
struct Func_2_t1348288522;
// Newtonsoft.Json.Utilities.ReflectionObject
struct ReflectionObject_t701100009;
// Newtonsoft.Json.Linq.JsonPath.QueryExpression
struct QueryExpression_t3171385195;
// System.Char[]
struct CharU5BU5D_t3528271667;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.ReflectionObject>
struct ThreadSafeStore_2_t4079379169;
// System.Xml.Linq.XDocumentType
struct XDocumentType_t1853592271;
// System.Xml.Linq.XDeclaration
struct XDeclaration_t2907650823;
// System.Xml.XmlDocumentType
struct XmlDocumentType_t4112370061;
// System.Xml.XmlDocument
struct XmlDocument_t2837193595;
// System.Xml.XmlElement
struct XmlElement_t561603118;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Xml.XmlDeclaration
struct XmlDeclaration_t679870411;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.ComponentModel.ListChangedEventHandler
struct ListChangedEventHandler_t1703970447;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3954782707;
// System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Linq.JToken>
struct Dictionary_2_t823795546;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter
struct FieldMultipleFilter_t3452751328;
// Newtonsoft.Json.Linq.JObject
struct JObject_t2059125928;
// Newtonsoft.Json.Linq.JsonPath.FieldFilter
struct FieldFilter_t3654920868;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>>
struct IEnumerator_1_t3654038181;
// Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter
struct ArrayMultipleIndexFilter_t2913121614;
// Newtonsoft.Json.Linq.JPropertyKeyedCollection
struct JPropertyKeyedCollection_t1521123921;
// System.ComponentModel.PropertyChangedEventHandler
struct PropertyChangedEventHandler_t3836340606;
// System.ComponentModel.PropertyChangingEventHandler
struct PropertyChangingEventHandler_t2830353497;
// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JToken>
struct List_1_t2510613989;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Func`2<System.Object[],Newtonsoft.Json.JsonConverter>>
struct ThreadSafeStore_2_t1780285614;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Type>
struct ThreadSafeStore_2_t1567256624;
// System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback>
struct List_1_t3780931481;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback>
struct IList_1_t4124176522;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationErrorCallback>
struct IList_1_t1853545566;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t1047106545;
// System.Globalization.CultureInfo
struct CultureInfo_t4157843068;
// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>
struct List_1_t4000102456;
// Newtonsoft.Json.JsonWriter/State[][]
struct StateU5BU5DU5BU5D_t3190318925;
// Newtonsoft.Json.JsonConverterCollection
struct JsonConverterCollection_t3675964822;
// Newtonsoft.Json.Serialization.IContractResolver
struct IContractResolver_t4193385603;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t1493878338;
// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_t274213469;
// Newtonsoft.Json.Serialization.IReferenceResolver
struct IReferenceResolver_t2564061104;
// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>
struct EventHandler_1_t2935836205;
// System.Collections.Generic.Dictionary`2<System.Type,Newtonsoft.Json.ReadType>
struct Dictionary_2_t2785133644;
// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JsonPath.QueryExpression>
struct List_1_t348492641;
// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JsonPath.PathFilter>
struct List_1_t1040076091;
// Newtonsoft.Json.Linq.JValue
struct JValue_t1448862567;
// Newtonsoft.Json.Serialization.JsonSerializerInternalReader
struct JsonSerializerInternalReader_t927352408;
// Newtonsoft.Json.Serialization.JsonSerializerInternalWriter
struct JsonSerializerInternalWriter_t3588727337;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef COLLECTION_1_T4277862461_H
#define COLLECTION_1_T4277862461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JToken>
struct  Collection_1_t4277862461  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1::list
	RuntimeObject* ___list_0;
	// System.Object System.Collections.ObjectModel.Collection`1::syncRoot
	RuntimeObject * ___syncRoot_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Collection_1_t4277862461, ___list_0)); }
	inline RuntimeObject* get_list_0() const { return ___list_0; }
	inline RuntimeObject** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(RuntimeObject* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_syncRoot_1() { return static_cast<int32_t>(offsetof(Collection_1_t4277862461, ___syncRoot_1)); }
	inline RuntimeObject * get_syncRoot_1() const { return ___syncRoot_1; }
	inline RuntimeObject ** get_address_of_syncRoot_1() { return &___syncRoot_1; }
	inline void set_syncRoot_1(RuntimeObject * value)
	{
		___syncRoot_1 = value;
		Il2CppCodeGenWriteBarrier((&___syncRoot_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTION_1_T4277862461_H
#ifndef U3CAFTERSELFU3ED__42_T883970034_H
#define U3CAFTERSELFU3ED__42_T883970034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JToken/<AfterSelf>d__42
struct  U3CAfterSelfU3Ed__42_t883970034  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JToken/<AfterSelf>d__42::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken/<AfterSelf>d__42::<>2__current
	JToken_t1038539247 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JToken/<AfterSelf>d__42::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken/<AfterSelf>d__42::<>4__this
	JToken_t1038539247 * ___U3CU3E4__this_3;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken/<AfterSelf>d__42::<o>5__1
	JToken_t1038539247 * ___U3CoU3E5__1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAfterSelfU3Ed__42_t883970034, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAfterSelfU3Ed__42_t883970034, ___U3CU3E2__current_1)); }
	inline JToken_t1038539247 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_t1038539247 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_t1038539247 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CAfterSelfU3Ed__42_t883970034, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CAfterSelfU3Ed__42_t883970034, ___U3CU3E4__this_3)); }
	inline JToken_t1038539247 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline JToken_t1038539247 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(JToken_t1038539247 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CoU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CAfterSelfU3Ed__42_t883970034, ___U3CoU3E5__1_4)); }
	inline JToken_t1038539247 * get_U3CoU3E5__1_4() const { return ___U3CoU3E5__1_4; }
	inline JToken_t1038539247 ** get_address_of_U3CoU3E5__1_4() { return &___U3CoU3E5__1_4; }
	inline void set_U3CoU3E5__1_4(JToken_t1038539247 * value)
	{
		___U3CoU3E5__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoU3E5__1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAFTERSELFU3ED__42_T883970034_H
#ifndef U3CGETANCESTORSU3ED__41_T2485884922_H
#define U3CGETANCESTORSU3ED__41_T2485884922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41
struct  U3CGetAncestorsU3Ed__41_t2485884922  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41::<>2__current
	JToken_t1038539247 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Boolean Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41::self
	bool ___self_3;
	// System.Boolean Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41::<>3__self
	bool ___U3CU3E3__self_4;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41::<>4__this
	JToken_t1038539247 * ___U3CU3E4__this_5;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken/<GetAncestors>d__41::<current>5__1
	JToken_t1038539247 * ___U3CcurrentU3E5__1_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_t2485884922, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_t2485884922, ___U3CU3E2__current_1)); }
	inline JToken_t1038539247 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_t1038539247 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_t1038539247 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_t2485884922, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_self_3() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_t2485884922, ___self_3)); }
	inline bool get_self_3() const { return ___self_3; }
	inline bool* get_address_of_self_3() { return &___self_3; }
	inline void set_self_3(bool value)
	{
		___self_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__self_4() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_t2485884922, ___U3CU3E3__self_4)); }
	inline bool get_U3CU3E3__self_4() const { return ___U3CU3E3__self_4; }
	inline bool* get_address_of_U3CU3E3__self_4() { return &___U3CU3E3__self_4; }
	inline void set_U3CU3E3__self_4(bool value)
	{
		___U3CU3E3__self_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_t2485884922, ___U3CU3E4__this_5)); }
	inline JToken_t1038539247 * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline JToken_t1038539247 ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(JToken_t1038539247 * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CcurrentU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CGetAncestorsU3Ed__41_t2485884922, ___U3CcurrentU3E5__1_6)); }
	inline JToken_t1038539247 * get_U3CcurrentU3E5__1_6() const { return ___U3CcurrentU3E5__1_6; }
	inline JToken_t1038539247 ** get_address_of_U3CcurrentU3E5__1_6() { return &___U3CcurrentU3E5__1_6; }
	inline void set_U3CcurrentU3E5__1_6(JToken_t1038539247 * value)
	{
		___U3CcurrentU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrentU3E5__1_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETANCESTORSU3ED__41_T2485884922_H
#ifndef LINEINFOANNOTATION_T2730592426_H
#define LINEINFOANNOTATION_T2730592426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JToken/LineInfoAnnotation
struct  LineInfoAnnotation_t2730592426  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JToken/LineInfoAnnotation::LineNumber
	int32_t ___LineNumber_0;
	// System.Int32 Newtonsoft.Json.Linq.JToken/LineInfoAnnotation::LinePosition
	int32_t ___LinePosition_1;

public:
	inline static int32_t get_offset_of_LineNumber_0() { return static_cast<int32_t>(offsetof(LineInfoAnnotation_t2730592426, ___LineNumber_0)); }
	inline int32_t get_LineNumber_0() const { return ___LineNumber_0; }
	inline int32_t* get_address_of_LineNumber_0() { return &___LineNumber_0; }
	inline void set_LineNumber_0(int32_t value)
	{
		___LineNumber_0 = value;
	}

	inline static int32_t get_offset_of_LinePosition_1() { return static_cast<int32_t>(offsetof(LineInfoAnnotation_t2730592426, ___LinePosition_1)); }
	inline int32_t get_LinePosition_1() const { return ___LinePosition_1; }
	inline int32_t* get_address_of_LinePosition_1() { return &___LinePosition_1; }
	inline void set_LinePosition_1(int32_t value)
	{
		___LinePosition_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEINFOANNOTATION_T2730592426_H
#ifndef JTOKEN_T1038539247_H
#define JTOKEN_T1038539247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JToken
struct  JToken_t1038539247  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JToken::_parent
	JContainer_t1023664833 * ____parent_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::_previous
	JToken_t1038539247 * ____previous_1;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::_next
	JToken_t1038539247 * ____next_2;
	// System.Object Newtonsoft.Json.Linq.JToken::_annotations
	RuntimeObject * ____annotations_3;

public:
	inline static int32_t get_offset_of__parent_0() { return static_cast<int32_t>(offsetof(JToken_t1038539247, ____parent_0)); }
	inline JContainer_t1023664833 * get__parent_0() const { return ____parent_0; }
	inline JContainer_t1023664833 ** get_address_of__parent_0() { return &____parent_0; }
	inline void set__parent_0(JContainer_t1023664833 * value)
	{
		____parent_0 = value;
		Il2CppCodeGenWriteBarrier((&____parent_0), value);
	}

	inline static int32_t get_offset_of__previous_1() { return static_cast<int32_t>(offsetof(JToken_t1038539247, ____previous_1)); }
	inline JToken_t1038539247 * get__previous_1() const { return ____previous_1; }
	inline JToken_t1038539247 ** get_address_of__previous_1() { return &____previous_1; }
	inline void set__previous_1(JToken_t1038539247 * value)
	{
		____previous_1 = value;
		Il2CppCodeGenWriteBarrier((&____previous_1), value);
	}

	inline static int32_t get_offset_of__next_2() { return static_cast<int32_t>(offsetof(JToken_t1038539247, ____next_2)); }
	inline JToken_t1038539247 * get__next_2() const { return ____next_2; }
	inline JToken_t1038539247 ** get_address_of__next_2() { return &____next_2; }
	inline void set__next_2(JToken_t1038539247 * value)
	{
		____next_2 = value;
		Il2CppCodeGenWriteBarrier((&____next_2), value);
	}

	inline static int32_t get_offset_of__annotations_3() { return static_cast<int32_t>(offsetof(JToken_t1038539247, ____annotations_3)); }
	inline RuntimeObject * get__annotations_3() const { return ____annotations_3; }
	inline RuntimeObject ** get_address_of__annotations_3() { return &____annotations_3; }
	inline void set__annotations_3(RuntimeObject * value)
	{
		____annotations_3 = value;
		Il2CppCodeGenWriteBarrier((&____annotations_3), value);
	}
};

struct JToken_t1038539247_StaticFields
{
public:
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::BooleanTypes
	JTokenTypeU5BU5D_t1887505675* ___BooleanTypes_4;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::NumberTypes
	JTokenTypeU5BU5D_t1887505675* ___NumberTypes_5;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::StringTypes
	JTokenTypeU5BU5D_t1887505675* ___StringTypes_6;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::GuidTypes
	JTokenTypeU5BU5D_t1887505675* ___GuidTypes_7;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::TimeSpanTypes
	JTokenTypeU5BU5D_t1887505675* ___TimeSpanTypes_8;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::UriTypes
	JTokenTypeU5BU5D_t1887505675* ___UriTypes_9;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::CharTypes
	JTokenTypeU5BU5D_t1887505675* ___CharTypes_10;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::DateTimeTypes
	JTokenTypeU5BU5D_t1887505675* ___DateTimeTypes_11;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::BytesTypes
	JTokenTypeU5BU5D_t1887505675* ___BytesTypes_12;

public:
	inline static int32_t get_offset_of_BooleanTypes_4() { return static_cast<int32_t>(offsetof(JToken_t1038539247_StaticFields, ___BooleanTypes_4)); }
	inline JTokenTypeU5BU5D_t1887505675* get_BooleanTypes_4() const { return ___BooleanTypes_4; }
	inline JTokenTypeU5BU5D_t1887505675** get_address_of_BooleanTypes_4() { return &___BooleanTypes_4; }
	inline void set_BooleanTypes_4(JTokenTypeU5BU5D_t1887505675* value)
	{
		___BooleanTypes_4 = value;
		Il2CppCodeGenWriteBarrier((&___BooleanTypes_4), value);
	}

	inline static int32_t get_offset_of_NumberTypes_5() { return static_cast<int32_t>(offsetof(JToken_t1038539247_StaticFields, ___NumberTypes_5)); }
	inline JTokenTypeU5BU5D_t1887505675* get_NumberTypes_5() const { return ___NumberTypes_5; }
	inline JTokenTypeU5BU5D_t1887505675** get_address_of_NumberTypes_5() { return &___NumberTypes_5; }
	inline void set_NumberTypes_5(JTokenTypeU5BU5D_t1887505675* value)
	{
		___NumberTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___NumberTypes_5), value);
	}

	inline static int32_t get_offset_of_StringTypes_6() { return static_cast<int32_t>(offsetof(JToken_t1038539247_StaticFields, ___StringTypes_6)); }
	inline JTokenTypeU5BU5D_t1887505675* get_StringTypes_6() const { return ___StringTypes_6; }
	inline JTokenTypeU5BU5D_t1887505675** get_address_of_StringTypes_6() { return &___StringTypes_6; }
	inline void set_StringTypes_6(JTokenTypeU5BU5D_t1887505675* value)
	{
		___StringTypes_6 = value;
		Il2CppCodeGenWriteBarrier((&___StringTypes_6), value);
	}

	inline static int32_t get_offset_of_GuidTypes_7() { return static_cast<int32_t>(offsetof(JToken_t1038539247_StaticFields, ___GuidTypes_7)); }
	inline JTokenTypeU5BU5D_t1887505675* get_GuidTypes_7() const { return ___GuidTypes_7; }
	inline JTokenTypeU5BU5D_t1887505675** get_address_of_GuidTypes_7() { return &___GuidTypes_7; }
	inline void set_GuidTypes_7(JTokenTypeU5BU5D_t1887505675* value)
	{
		___GuidTypes_7 = value;
		Il2CppCodeGenWriteBarrier((&___GuidTypes_7), value);
	}

	inline static int32_t get_offset_of_TimeSpanTypes_8() { return static_cast<int32_t>(offsetof(JToken_t1038539247_StaticFields, ___TimeSpanTypes_8)); }
	inline JTokenTypeU5BU5D_t1887505675* get_TimeSpanTypes_8() const { return ___TimeSpanTypes_8; }
	inline JTokenTypeU5BU5D_t1887505675** get_address_of_TimeSpanTypes_8() { return &___TimeSpanTypes_8; }
	inline void set_TimeSpanTypes_8(JTokenTypeU5BU5D_t1887505675* value)
	{
		___TimeSpanTypes_8 = value;
		Il2CppCodeGenWriteBarrier((&___TimeSpanTypes_8), value);
	}

	inline static int32_t get_offset_of_UriTypes_9() { return static_cast<int32_t>(offsetof(JToken_t1038539247_StaticFields, ___UriTypes_9)); }
	inline JTokenTypeU5BU5D_t1887505675* get_UriTypes_9() const { return ___UriTypes_9; }
	inline JTokenTypeU5BU5D_t1887505675** get_address_of_UriTypes_9() { return &___UriTypes_9; }
	inline void set_UriTypes_9(JTokenTypeU5BU5D_t1887505675* value)
	{
		___UriTypes_9 = value;
		Il2CppCodeGenWriteBarrier((&___UriTypes_9), value);
	}

	inline static int32_t get_offset_of_CharTypes_10() { return static_cast<int32_t>(offsetof(JToken_t1038539247_StaticFields, ___CharTypes_10)); }
	inline JTokenTypeU5BU5D_t1887505675* get_CharTypes_10() const { return ___CharTypes_10; }
	inline JTokenTypeU5BU5D_t1887505675** get_address_of_CharTypes_10() { return &___CharTypes_10; }
	inline void set_CharTypes_10(JTokenTypeU5BU5D_t1887505675* value)
	{
		___CharTypes_10 = value;
		Il2CppCodeGenWriteBarrier((&___CharTypes_10), value);
	}

	inline static int32_t get_offset_of_DateTimeTypes_11() { return static_cast<int32_t>(offsetof(JToken_t1038539247_StaticFields, ___DateTimeTypes_11)); }
	inline JTokenTypeU5BU5D_t1887505675* get_DateTimeTypes_11() const { return ___DateTimeTypes_11; }
	inline JTokenTypeU5BU5D_t1887505675** get_address_of_DateTimeTypes_11() { return &___DateTimeTypes_11; }
	inline void set_DateTimeTypes_11(JTokenTypeU5BU5D_t1887505675* value)
	{
		___DateTimeTypes_11 = value;
		Il2CppCodeGenWriteBarrier((&___DateTimeTypes_11), value);
	}

	inline static int32_t get_offset_of_BytesTypes_12() { return static_cast<int32_t>(offsetof(JToken_t1038539247_StaticFields, ___BytesTypes_12)); }
	inline JTokenTypeU5BU5D_t1887505675* get_BytesTypes_12() const { return ___BytesTypes_12; }
	inline JTokenTypeU5BU5D_t1887505675** get_address_of_BytesTypes_12() { return &___BytesTypes_12; }
	inline void set_BytesTypes_12(JTokenTypeU5BU5D_t1887505675* value)
	{
		___BytesTypes_12 = value;
		Il2CppCodeGenWriteBarrier((&___BytesTypes_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKEN_T1038539247_H
#ifndef JSONCONVERTER_T1047106545_H
#define JSONCONVERTER_T1047106545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonConverter
struct  JsonConverter_t1047106545  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONVERTER_T1047106545_H
#ifndef MEMBERDESCRIPTOR_T3815403747_H
#define MEMBERDESCRIPTOR_T3815403747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.MemberDescriptor
struct  MemberDescriptor_t3815403747  : public RuntimeObject
{
public:
	// System.String System.ComponentModel.MemberDescriptor::name
	String_t* ___name_0;
	// System.Attribute[] System.ComponentModel.MemberDescriptor::attrs
	AttributeU5BU5D_t1575011174* ___attrs_1;
	// System.ComponentModel.AttributeCollection System.ComponentModel.MemberDescriptor::attrCollection
	AttributeCollection_t4221220734 * ___attrCollection_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(MemberDescriptor_t3815403747, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_attrs_1() { return static_cast<int32_t>(offsetof(MemberDescriptor_t3815403747, ___attrs_1)); }
	inline AttributeU5BU5D_t1575011174* get_attrs_1() const { return ___attrs_1; }
	inline AttributeU5BU5D_t1575011174** get_address_of_attrs_1() { return &___attrs_1; }
	inline void set_attrs_1(AttributeU5BU5D_t1575011174* value)
	{
		___attrs_1 = value;
		Il2CppCodeGenWriteBarrier((&___attrs_1), value);
	}

	inline static int32_t get_offset_of_attrCollection_2() { return static_cast<int32_t>(offsetof(MemberDescriptor_t3815403747, ___attrCollection_2)); }
	inline AttributeCollection_t4221220734 * get_attrCollection_2() const { return ___attrCollection_2; }
	inline AttributeCollection_t4221220734 ** get_address_of_attrCollection_2() { return &___attrCollection_2; }
	inline void set_attrCollection_2(AttributeCollection_t4221220734 * value)
	{
		___attrCollection_2 = value;
		Il2CppCodeGenWriteBarrier((&___attrCollection_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERDESCRIPTOR_T3815403747_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CU3EC_T3140449829_H
#define U3CU3EC_T3140449829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JObject/<>c
struct  U3CU3Ec_t3140449829  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t3140449829_StaticFields
{
public:
	// Newtonsoft.Json.Linq.JObject/<>c Newtonsoft.Json.Linq.JObject/<>c::<>9
	U3CU3Ec_t3140449829 * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3140449829_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3140449829 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3140449829 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3140449829 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T3140449829_H
#ifndef U3CGETDESCENDANTSU3ED__29_T2717699431_H
#define U3CGETDESCENDANTSU3ED__29_T2717699431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JContainer/<GetDescendants>d__29
struct  U3CGetDescendantsU3Ed__29_t2717699431  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JContainer/<GetDescendants>d__29::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer/<GetDescendants>d__29::<>2__current
	JToken_t1038539247 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JContainer/<GetDescendants>d__29::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Boolean Newtonsoft.Json.Linq.JContainer/<GetDescendants>d__29::self
	bool ___self_3;
	// System.Boolean Newtonsoft.Json.Linq.JContainer/<GetDescendants>d__29::<>3__self
	bool ___U3CU3E3__self_4;
	// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JContainer/<GetDescendants>d__29::<>4__this
	JContainer_t1023664833 * ___U3CU3E4__this_5;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JContainer/<GetDescendants>d__29::<>s__1
	RuntimeObject* ___U3CU3Es__1_6;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer/<GetDescendants>d__29::<o>5__2
	JToken_t1038539247 * ___U3CoU3E5__2_7;
	// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JContainer/<GetDescendants>d__29::<c>5__3
	JContainer_t1023664833 * ___U3CcU3E5__3_8;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JContainer/<GetDescendants>d__29::<>s__4
	RuntimeObject* ___U3CU3Es__4_9;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer/<GetDescendants>d__29::<d>5__5
	JToken_t1038539247 * ___U3CdU3E5__5_10;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__29_t2717699431, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__29_t2717699431, ___U3CU3E2__current_1)); }
	inline JToken_t1038539247 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_t1038539247 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_t1038539247 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__29_t2717699431, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_self_3() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__29_t2717699431, ___self_3)); }
	inline bool get_self_3() const { return ___self_3; }
	inline bool* get_address_of_self_3() { return &___self_3; }
	inline void set_self_3(bool value)
	{
		___self_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__self_4() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__29_t2717699431, ___U3CU3E3__self_4)); }
	inline bool get_U3CU3E3__self_4() const { return ___U3CU3E3__self_4; }
	inline bool* get_address_of_U3CU3E3__self_4() { return &___U3CU3E3__self_4; }
	inline void set_U3CU3E3__self_4(bool value)
	{
		___U3CU3E3__self_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__29_t2717699431, ___U3CU3E4__this_5)); }
	inline JContainer_t1023664833 * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline JContainer_t1023664833 ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(JContainer_t1023664833 * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Es__1_6() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__29_t2717699431, ___U3CU3Es__1_6)); }
	inline RuntimeObject* get_U3CU3Es__1_6() const { return ___U3CU3Es__1_6; }
	inline RuntimeObject** get_address_of_U3CU3Es__1_6() { return &___U3CU3Es__1_6; }
	inline void set_U3CU3Es__1_6(RuntimeObject* value)
	{
		___U3CU3Es__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Es__1_6), value);
	}

	inline static int32_t get_offset_of_U3CoU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__29_t2717699431, ___U3CoU3E5__2_7)); }
	inline JToken_t1038539247 * get_U3CoU3E5__2_7() const { return ___U3CoU3E5__2_7; }
	inline JToken_t1038539247 ** get_address_of_U3CoU3E5__2_7() { return &___U3CoU3E5__2_7; }
	inline void set_U3CoU3E5__2_7(JToken_t1038539247 * value)
	{
		___U3CoU3E5__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoU3E5__2_7), value);
	}

	inline static int32_t get_offset_of_U3CcU3E5__3_8() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__29_t2717699431, ___U3CcU3E5__3_8)); }
	inline JContainer_t1023664833 * get_U3CcU3E5__3_8() const { return ___U3CcU3E5__3_8; }
	inline JContainer_t1023664833 ** get_address_of_U3CcU3E5__3_8() { return &___U3CcU3E5__3_8; }
	inline void set_U3CcU3E5__3_8(JContainer_t1023664833 * value)
	{
		___U3CcU3E5__3_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcU3E5__3_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Es__4_9() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__29_t2717699431, ___U3CU3Es__4_9)); }
	inline RuntimeObject* get_U3CU3Es__4_9() const { return ___U3CU3Es__4_9; }
	inline RuntimeObject** get_address_of_U3CU3Es__4_9() { return &___U3CU3Es__4_9; }
	inline void set_U3CU3Es__4_9(RuntimeObject* value)
	{
		___U3CU3Es__4_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Es__4_9), value);
	}

	inline static int32_t get_offset_of_U3CdU3E5__5_10() { return static_cast<int32_t>(offsetof(U3CGetDescendantsU3Ed__29_t2717699431, ___U3CdU3E5__5_10)); }
	inline JToken_t1038539247 * get_U3CdU3E5__5_10() const { return ___U3CdU3E5__5_10; }
	inline JToken_t1038539247 ** get_address_of_U3CdU3E5__5_10() { return &___U3CdU3E5__5_10; }
	inline void set_U3CdU3E5__5_10(JToken_t1038539247 * value)
	{
		___U3CdU3E5__5_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdU3E5__5_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETDESCENDANTSU3ED__29_T2717699431_H
#ifndef U3CBEFORESELFU3ED__43_T2375137862_H
#define U3CBEFORESELFU3ED__43_T2375137862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JToken/<BeforeSelf>d__43
struct  U3CBeforeSelfU3Ed__43_t2375137862  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JToken/<BeforeSelf>d__43::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken/<BeforeSelf>d__43::<>2__current
	JToken_t1038539247 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JToken/<BeforeSelf>d__43::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken/<BeforeSelf>d__43::<>4__this
	JToken_t1038539247 * ___U3CU3E4__this_3;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken/<BeforeSelf>d__43::<o>5__1
	JToken_t1038539247 * ___U3CoU3E5__1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CBeforeSelfU3Ed__43_t2375137862, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CBeforeSelfU3Ed__43_t2375137862, ___U3CU3E2__current_1)); }
	inline JToken_t1038539247 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_t1038539247 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_t1038539247 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CBeforeSelfU3Ed__43_t2375137862, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CBeforeSelfU3Ed__43_t2375137862, ___U3CU3E4__this_3)); }
	inline JToken_t1038539247 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline JToken_t1038539247 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(JToken_t1038539247 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CoU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CBeforeSelfU3Ed__43_t2375137862, ___U3CoU3E5__1_4)); }
	inline JToken_t1038539247 * get_U3CoU3E5__1_4() const { return ___U3CoU3E5__1_4; }
	inline JToken_t1038539247 ** get_address_of_U3CoU3E5__1_4() { return &___U3CoU3E5__1_4; }
	inline void set_U3CoU3E5__1_4(JToken_t1038539247 * value)
	{
		___U3CoU3E5__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoU3E5__1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBEFORESELFU3ED__43_T2375137862_H
#ifndef XOBJECTWRAPPER_T2266598274_H
#define XOBJECTWRAPPER_T2266598274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XObjectWrapper
struct  XObjectWrapper_t2266598274  : public RuntimeObject
{
public:
	// System.Xml.Linq.XObject Newtonsoft.Json.Converters.XObjectWrapper::_xmlObject
	XObject_t1119084474 * ____xmlObject_1;

public:
	inline static int32_t get_offset_of__xmlObject_1() { return static_cast<int32_t>(offsetof(XObjectWrapper_t2266598274, ____xmlObject_1)); }
	inline XObject_t1119084474 * get__xmlObject_1() const { return ____xmlObject_1; }
	inline XObject_t1119084474 ** get_address_of__xmlObject_1() { return &____xmlObject_1; }
	inline void set__xmlObject_1(XObject_t1119084474 * value)
	{
		____xmlObject_1 = value;
		Il2CppCodeGenWriteBarrier((&____xmlObject_1), value);
	}
};

struct XObjectWrapper_t2266598274_StaticFields
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XObjectWrapper::EmptyChildNodes
	List_1_t2041514553 * ___EmptyChildNodes_0;

public:
	inline static int32_t get_offset_of_EmptyChildNodes_0() { return static_cast<int32_t>(offsetof(XObjectWrapper_t2266598274_StaticFields, ___EmptyChildNodes_0)); }
	inline List_1_t2041514553 * get_EmptyChildNodes_0() const { return ___EmptyChildNodes_0; }
	inline List_1_t2041514553 ** get_address_of_EmptyChildNodes_0() { return &___EmptyChildNodes_0; }
	inline void set_EmptyChildNodes_0(List_1_t2041514553 * value)
	{
		___EmptyChildNodes_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyChildNodes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XOBJECTWRAPPER_T2266598274_H
#ifndef PATHFILTER_T3862968645_H
#define PATHFILTER_T3862968645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.PathFilter
struct  PathFilter_t3862968645  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHFILTER_T3862968645_H
#ifndef JPATH_T4294290739_H
#define JPATH_T4294290739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.JPath
struct  JPath_t4294290739  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPATH_T4294290739_H
#ifndef U3CU3EC_T266907836_H
#define U3CU3EC_T266907836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter/<>c
struct  U3CU3Ec_t266907836  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t266907836_StaticFields
{
public:
	// Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter/<>c Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter/<>c::<>9
	U3CU3Ec_t266907836 * ___U3CU3E9_0;
	// System.Func`2<System.String,System.String> Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter/<>c::<>9__4_0
	Func_2_t3947292210 * ___U3CU3E9__4_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t266907836_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t266907836 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t266907836 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t266907836 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t266907836_StaticFields, ___U3CU3E9__4_0_1)); }
	inline Func_2_t3947292210 * get_U3CU3E9__4_0_1() const { return ___U3CU3E9__4_0_1; }
	inline Func_2_t3947292210 ** get_address_of_U3CU3E9__4_0_1() { return &___U3CU3E9__4_0_1; }
	inline void set_U3CU3E9__4_0_1(Func_2_t3947292210 * value)
	{
		___U3CU3E9__4_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T266907836_H
#ifndef U3CEXECUTEFILTERU3ED__4_T2672090193_H
#define U3CEXECUTEFILTERU3ED__4_T2672090193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.QueryFilter/<ExecuteFilter>d__4
struct  U3CExecuteFilterU3Ed__4_t2672090193  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.QueryFilter/<ExecuteFilter>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.QueryFilter/<ExecuteFilter>d__4::<>2__current
	JToken_t1038539247 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.QueryFilter/<ExecuteFilter>d__4::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.QueryFilter/<ExecuteFilter>d__4::current
	RuntimeObject* ___current_3;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.QueryFilter/<ExecuteFilter>d__4::<>3__current
	RuntimeObject* ___U3CU3E3__current_4;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.QueryFilter/<ExecuteFilter>d__4::errorWhenNoMatch
	bool ___errorWhenNoMatch_5;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.QueryFilter/<ExecuteFilter>d__4::<>3__errorWhenNoMatch
	bool ___U3CU3E3__errorWhenNoMatch_6;
	// Newtonsoft.Json.Linq.JsonPath.QueryFilter Newtonsoft.Json.Linq.JsonPath.QueryFilter/<ExecuteFilter>d__4::<>4__this
	QueryFilter_t1584032927 * ___U3CU3E4__this_7;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.QueryFilter/<ExecuteFilter>d__4::<>s__1
	RuntimeObject* ___U3CU3Es__1_8;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.QueryFilter/<ExecuteFilter>d__4::<t>5__2
	JToken_t1038539247 * ___U3CtU3E5__2_9;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.QueryFilter/<ExecuteFilter>d__4::<>s__3
	RuntimeObject* ___U3CU3Es__3_10;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.QueryFilter/<ExecuteFilter>d__4::<v>5__4
	JToken_t1038539247 * ___U3CvU3E5__4_11;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2672090193, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2672090193, ___U3CU3E2__current_1)); }
	inline JToken_t1038539247 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_t1038539247 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_t1038539247 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2672090193, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2672090193, ___current_3)); }
	inline RuntimeObject* get_current_3() const { return ___current_3; }
	inline RuntimeObject** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__current_4() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2672090193, ___U3CU3E3__current_4)); }
	inline RuntimeObject* get_U3CU3E3__current_4() const { return ___U3CU3E3__current_4; }
	inline RuntimeObject** get_address_of_U3CU3E3__current_4() { return &___U3CU3E3__current_4; }
	inline void set_U3CU3E3__current_4(RuntimeObject* value)
	{
		___U3CU3E3__current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__current_4), value);
	}

	inline static int32_t get_offset_of_errorWhenNoMatch_5() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2672090193, ___errorWhenNoMatch_5)); }
	inline bool get_errorWhenNoMatch_5() const { return ___errorWhenNoMatch_5; }
	inline bool* get_address_of_errorWhenNoMatch_5() { return &___errorWhenNoMatch_5; }
	inline void set_errorWhenNoMatch_5(bool value)
	{
		___errorWhenNoMatch_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__errorWhenNoMatch_6() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2672090193, ___U3CU3E3__errorWhenNoMatch_6)); }
	inline bool get_U3CU3E3__errorWhenNoMatch_6() const { return ___U3CU3E3__errorWhenNoMatch_6; }
	inline bool* get_address_of_U3CU3E3__errorWhenNoMatch_6() { return &___U3CU3E3__errorWhenNoMatch_6; }
	inline void set_U3CU3E3__errorWhenNoMatch_6(bool value)
	{
		___U3CU3E3__errorWhenNoMatch_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_7() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2672090193, ___U3CU3E4__this_7)); }
	inline QueryFilter_t1584032927 * get_U3CU3E4__this_7() const { return ___U3CU3E4__this_7; }
	inline QueryFilter_t1584032927 ** get_address_of_U3CU3E4__this_7() { return &___U3CU3E4__this_7; }
	inline void set_U3CU3E4__this_7(QueryFilter_t1584032927 * value)
	{
		___U3CU3E4__this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Es__1_8() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2672090193, ___U3CU3Es__1_8)); }
	inline RuntimeObject* get_U3CU3Es__1_8() const { return ___U3CU3Es__1_8; }
	inline RuntimeObject** get_address_of_U3CU3Es__1_8() { return &___U3CU3Es__1_8; }
	inline void set_U3CU3Es__1_8(RuntimeObject* value)
	{
		___U3CU3Es__1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Es__1_8), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__2_9() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2672090193, ___U3CtU3E5__2_9)); }
	inline JToken_t1038539247 * get_U3CtU3E5__2_9() const { return ___U3CtU3E5__2_9; }
	inline JToken_t1038539247 ** get_address_of_U3CtU3E5__2_9() { return &___U3CtU3E5__2_9; }
	inline void set_U3CtU3E5__2_9(JToken_t1038539247 * value)
	{
		___U3CtU3E5__2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtU3E5__2_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Es__3_10() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2672090193, ___U3CU3Es__3_10)); }
	inline RuntimeObject* get_U3CU3Es__3_10() const { return ___U3CU3Es__3_10; }
	inline RuntimeObject** get_address_of_U3CU3Es__3_10() { return &___U3CU3Es__3_10; }
	inline void set_U3CU3Es__3_10(RuntimeObject* value)
	{
		___U3CU3Es__3_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Es__3_10), value);
	}

	inline static int32_t get_offset_of_U3CvU3E5__4_11() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2672090193, ___U3CvU3E5__4_11)); }
	inline JToken_t1038539247 * get_U3CvU3E5__4_11() const { return ___U3CvU3E5__4_11; }
	inline JToken_t1038539247 ** get_address_of_U3CvU3E5__4_11() { return &___U3CvU3E5__4_11; }
	inline void set_U3CvU3E5__4_11(JToken_t1038539247 * value)
	{
		___U3CvU3E5__4_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvU3E5__4_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEFILTERU3ED__4_T2672090193_H
#ifndef U3CEXECUTEFILTERU3ED__4_T3421602345_H
#define U3CEXECUTEFILTERU3ED__4_T3421602345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.ScanFilter/<ExecuteFilter>d__4
struct  U3CExecuteFilterU3Ed__4_t3421602345  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ScanFilter/<ExecuteFilter>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ScanFilter/<ExecuteFilter>d__4::<>2__current
	JToken_t1038539247 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ScanFilter/<ExecuteFilter>d__4::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ScanFilter/<ExecuteFilter>d__4::current
	RuntimeObject* ___current_3;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ScanFilter/<ExecuteFilter>d__4::<>3__current
	RuntimeObject* ___U3CU3E3__current_4;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.ScanFilter/<ExecuteFilter>d__4::errorWhenNoMatch
	bool ___errorWhenNoMatch_5;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.ScanFilter/<ExecuteFilter>d__4::<>3__errorWhenNoMatch
	bool ___U3CU3E3__errorWhenNoMatch_6;
	// Newtonsoft.Json.Linq.JsonPath.ScanFilter Newtonsoft.Json.Linq.JsonPath.ScanFilter/<ExecuteFilter>d__4::<>4__this
	ScanFilter_t1566003751 * ___U3CU3E4__this_7;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ScanFilter/<ExecuteFilter>d__4::<>s__1
	RuntimeObject* ___U3CU3Es__1_8;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ScanFilter/<ExecuteFilter>d__4::<root>5__2
	JToken_t1038539247 * ___U3CrootU3E5__2_9;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ScanFilter/<ExecuteFilter>d__4::<value>5__3
	JToken_t1038539247 * ___U3CvalueU3E5__3_10;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ScanFilter/<ExecuteFilter>d__4::<container>5__4
	JToken_t1038539247 * ___U3CcontainerU3E5__4_11;
	// Newtonsoft.Json.Linq.JProperty Newtonsoft.Json.Linq.JsonPath.ScanFilter/<ExecuteFilter>d__4::<e>5__5
	JProperty_t3803048347 * ___U3CeU3E5__5_12;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t3421602345, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t3421602345, ___U3CU3E2__current_1)); }
	inline JToken_t1038539247 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_t1038539247 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_t1038539247 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t3421602345, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t3421602345, ___current_3)); }
	inline RuntimeObject* get_current_3() const { return ___current_3; }
	inline RuntimeObject** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__current_4() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t3421602345, ___U3CU3E3__current_4)); }
	inline RuntimeObject* get_U3CU3E3__current_4() const { return ___U3CU3E3__current_4; }
	inline RuntimeObject** get_address_of_U3CU3E3__current_4() { return &___U3CU3E3__current_4; }
	inline void set_U3CU3E3__current_4(RuntimeObject* value)
	{
		___U3CU3E3__current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__current_4), value);
	}

	inline static int32_t get_offset_of_errorWhenNoMatch_5() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t3421602345, ___errorWhenNoMatch_5)); }
	inline bool get_errorWhenNoMatch_5() const { return ___errorWhenNoMatch_5; }
	inline bool* get_address_of_errorWhenNoMatch_5() { return &___errorWhenNoMatch_5; }
	inline void set_errorWhenNoMatch_5(bool value)
	{
		___errorWhenNoMatch_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__errorWhenNoMatch_6() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t3421602345, ___U3CU3E3__errorWhenNoMatch_6)); }
	inline bool get_U3CU3E3__errorWhenNoMatch_6() const { return ___U3CU3E3__errorWhenNoMatch_6; }
	inline bool* get_address_of_U3CU3E3__errorWhenNoMatch_6() { return &___U3CU3E3__errorWhenNoMatch_6; }
	inline void set_U3CU3E3__errorWhenNoMatch_6(bool value)
	{
		___U3CU3E3__errorWhenNoMatch_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_7() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t3421602345, ___U3CU3E4__this_7)); }
	inline ScanFilter_t1566003751 * get_U3CU3E4__this_7() const { return ___U3CU3E4__this_7; }
	inline ScanFilter_t1566003751 ** get_address_of_U3CU3E4__this_7() { return &___U3CU3E4__this_7; }
	inline void set_U3CU3E4__this_7(ScanFilter_t1566003751 * value)
	{
		___U3CU3E4__this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Es__1_8() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t3421602345, ___U3CU3Es__1_8)); }
	inline RuntimeObject* get_U3CU3Es__1_8() const { return ___U3CU3Es__1_8; }
	inline RuntimeObject** get_address_of_U3CU3Es__1_8() { return &___U3CU3Es__1_8; }
	inline void set_U3CU3Es__1_8(RuntimeObject* value)
	{
		___U3CU3Es__1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Es__1_8), value);
	}

	inline static int32_t get_offset_of_U3CrootU3E5__2_9() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t3421602345, ___U3CrootU3E5__2_9)); }
	inline JToken_t1038539247 * get_U3CrootU3E5__2_9() const { return ___U3CrootU3E5__2_9; }
	inline JToken_t1038539247 ** get_address_of_U3CrootU3E5__2_9() { return &___U3CrootU3E5__2_9; }
	inline void set_U3CrootU3E5__2_9(JToken_t1038539247 * value)
	{
		___U3CrootU3E5__2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrootU3E5__2_9), value);
	}

	inline static int32_t get_offset_of_U3CvalueU3E5__3_10() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t3421602345, ___U3CvalueU3E5__3_10)); }
	inline JToken_t1038539247 * get_U3CvalueU3E5__3_10() const { return ___U3CvalueU3E5__3_10; }
	inline JToken_t1038539247 ** get_address_of_U3CvalueU3E5__3_10() { return &___U3CvalueU3E5__3_10; }
	inline void set_U3CvalueU3E5__3_10(JToken_t1038539247 * value)
	{
		___U3CvalueU3E5__3_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvalueU3E5__3_10), value);
	}

	inline static int32_t get_offset_of_U3CcontainerU3E5__4_11() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t3421602345, ___U3CcontainerU3E5__4_11)); }
	inline JToken_t1038539247 * get_U3CcontainerU3E5__4_11() const { return ___U3CcontainerU3E5__4_11; }
	inline JToken_t1038539247 ** get_address_of_U3CcontainerU3E5__4_11() { return &___U3CcontainerU3E5__4_11; }
	inline void set_U3CcontainerU3E5__4_11(JToken_t1038539247 * value)
	{
		___U3CcontainerU3E5__4_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontainerU3E5__4_11), value);
	}

	inline static int32_t get_offset_of_U3CeU3E5__5_12() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t3421602345, ___U3CeU3E5__5_12)); }
	inline JProperty_t3803048347 * get_U3CeU3E5__5_12() const { return ___U3CeU3E5__5_12; }
	inline JProperty_t3803048347 ** get_address_of_U3CeU3E5__5_12() { return &___U3CeU3E5__5_12; }
	inline void set_U3CeU3E5__5_12(JProperty_t3803048347 * value)
	{
		___U3CeU3E5__5_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CeU3E5__5_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEFILTERU3ED__4_T3421602345_H
#ifndef U3CEXECUTEFILTERU3ED__12_T222072922_H
#define U3CEXECUTEFILTERU3ED__12_T222072922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter/<ExecuteFilter>d__12
struct  U3CExecuteFilterU3Ed__12_t222072922  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter/<ExecuteFilter>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter/<ExecuteFilter>d__12::<>2__current
	JToken_t1038539247 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter/<ExecuteFilter>d__12::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter/<ExecuteFilter>d__12::current
	RuntimeObject* ___current_3;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter/<ExecuteFilter>d__12::<>3__current
	RuntimeObject* ___U3CU3E3__current_4;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter/<ExecuteFilter>d__12::errorWhenNoMatch
	bool ___errorWhenNoMatch_5;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter/<ExecuteFilter>d__12::<>3__errorWhenNoMatch
	bool ___U3CU3E3__errorWhenNoMatch_6;
	// Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter/<ExecuteFilter>d__12::<>4__this
	ArraySliceFilter_t662461599 * ___U3CU3E4__this_7;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter/<ExecuteFilter>d__12::<>s__1
	RuntimeObject* ___U3CU3Es__1_8;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter/<ExecuteFilter>d__12::<t>5__2
	JToken_t1038539247 * ___U3CtU3E5__2_9;
	// Newtonsoft.Json.Linq.JArray Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter/<ExecuteFilter>d__12::<a>5__3
	JArray_t2963978544 * ___U3CaU3E5__3_10;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter/<ExecuteFilter>d__12::<stepCount>5__4
	int32_t ___U3CstepCountU3E5__4_11;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter/<ExecuteFilter>d__12::<startIndex>5__5
	int32_t ___U3CstartIndexU3E5__5_12;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter/<ExecuteFilter>d__12::<stopIndex>5__6
	int32_t ___U3CstopIndexU3E5__6_13;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter/<ExecuteFilter>d__12::<positiveStep>5__7
	bool ___U3CpositiveStepU3E5__7_14;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter/<ExecuteFilter>d__12::<i>5__8
	int32_t ___U3CiU3E5__8_15;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_t222072922, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_t222072922, ___U3CU3E2__current_1)); }
	inline JToken_t1038539247 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_t1038539247 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_t1038539247 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_t222072922, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_t222072922, ___current_3)); }
	inline RuntimeObject* get_current_3() const { return ___current_3; }
	inline RuntimeObject** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__current_4() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_t222072922, ___U3CU3E3__current_4)); }
	inline RuntimeObject* get_U3CU3E3__current_4() const { return ___U3CU3E3__current_4; }
	inline RuntimeObject** get_address_of_U3CU3E3__current_4() { return &___U3CU3E3__current_4; }
	inline void set_U3CU3E3__current_4(RuntimeObject* value)
	{
		___U3CU3E3__current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__current_4), value);
	}

	inline static int32_t get_offset_of_errorWhenNoMatch_5() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_t222072922, ___errorWhenNoMatch_5)); }
	inline bool get_errorWhenNoMatch_5() const { return ___errorWhenNoMatch_5; }
	inline bool* get_address_of_errorWhenNoMatch_5() { return &___errorWhenNoMatch_5; }
	inline void set_errorWhenNoMatch_5(bool value)
	{
		___errorWhenNoMatch_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__errorWhenNoMatch_6() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_t222072922, ___U3CU3E3__errorWhenNoMatch_6)); }
	inline bool get_U3CU3E3__errorWhenNoMatch_6() const { return ___U3CU3E3__errorWhenNoMatch_6; }
	inline bool* get_address_of_U3CU3E3__errorWhenNoMatch_6() { return &___U3CU3E3__errorWhenNoMatch_6; }
	inline void set_U3CU3E3__errorWhenNoMatch_6(bool value)
	{
		___U3CU3E3__errorWhenNoMatch_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_7() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_t222072922, ___U3CU3E4__this_7)); }
	inline ArraySliceFilter_t662461599 * get_U3CU3E4__this_7() const { return ___U3CU3E4__this_7; }
	inline ArraySliceFilter_t662461599 ** get_address_of_U3CU3E4__this_7() { return &___U3CU3E4__this_7; }
	inline void set_U3CU3E4__this_7(ArraySliceFilter_t662461599 * value)
	{
		___U3CU3E4__this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Es__1_8() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_t222072922, ___U3CU3Es__1_8)); }
	inline RuntimeObject* get_U3CU3Es__1_8() const { return ___U3CU3Es__1_8; }
	inline RuntimeObject** get_address_of_U3CU3Es__1_8() { return &___U3CU3Es__1_8; }
	inline void set_U3CU3Es__1_8(RuntimeObject* value)
	{
		___U3CU3Es__1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Es__1_8), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__2_9() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_t222072922, ___U3CtU3E5__2_9)); }
	inline JToken_t1038539247 * get_U3CtU3E5__2_9() const { return ___U3CtU3E5__2_9; }
	inline JToken_t1038539247 ** get_address_of_U3CtU3E5__2_9() { return &___U3CtU3E5__2_9; }
	inline void set_U3CtU3E5__2_9(JToken_t1038539247 * value)
	{
		___U3CtU3E5__2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtU3E5__2_9), value);
	}

	inline static int32_t get_offset_of_U3CaU3E5__3_10() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_t222072922, ___U3CaU3E5__3_10)); }
	inline JArray_t2963978544 * get_U3CaU3E5__3_10() const { return ___U3CaU3E5__3_10; }
	inline JArray_t2963978544 ** get_address_of_U3CaU3E5__3_10() { return &___U3CaU3E5__3_10; }
	inline void set_U3CaU3E5__3_10(JArray_t2963978544 * value)
	{
		___U3CaU3E5__3_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CaU3E5__3_10), value);
	}

	inline static int32_t get_offset_of_U3CstepCountU3E5__4_11() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_t222072922, ___U3CstepCountU3E5__4_11)); }
	inline int32_t get_U3CstepCountU3E5__4_11() const { return ___U3CstepCountU3E5__4_11; }
	inline int32_t* get_address_of_U3CstepCountU3E5__4_11() { return &___U3CstepCountU3E5__4_11; }
	inline void set_U3CstepCountU3E5__4_11(int32_t value)
	{
		___U3CstepCountU3E5__4_11 = value;
	}

	inline static int32_t get_offset_of_U3CstartIndexU3E5__5_12() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_t222072922, ___U3CstartIndexU3E5__5_12)); }
	inline int32_t get_U3CstartIndexU3E5__5_12() const { return ___U3CstartIndexU3E5__5_12; }
	inline int32_t* get_address_of_U3CstartIndexU3E5__5_12() { return &___U3CstartIndexU3E5__5_12; }
	inline void set_U3CstartIndexU3E5__5_12(int32_t value)
	{
		___U3CstartIndexU3E5__5_12 = value;
	}

	inline static int32_t get_offset_of_U3CstopIndexU3E5__6_13() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_t222072922, ___U3CstopIndexU3E5__6_13)); }
	inline int32_t get_U3CstopIndexU3E5__6_13() const { return ___U3CstopIndexU3E5__6_13; }
	inline int32_t* get_address_of_U3CstopIndexU3E5__6_13() { return &___U3CstopIndexU3E5__6_13; }
	inline void set_U3CstopIndexU3E5__6_13(int32_t value)
	{
		___U3CstopIndexU3E5__6_13 = value;
	}

	inline static int32_t get_offset_of_U3CpositiveStepU3E5__7_14() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_t222072922, ___U3CpositiveStepU3E5__7_14)); }
	inline bool get_U3CpositiveStepU3E5__7_14() const { return ___U3CpositiveStepU3E5__7_14; }
	inline bool* get_address_of_U3CpositiveStepU3E5__7_14() { return &___U3CpositiveStepU3E5__7_14; }
	inline void set_U3CpositiveStepU3E5__7_14(bool value)
	{
		___U3CpositiveStepU3E5__7_14 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__8_15() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__12_t222072922, ___U3CiU3E5__8_15)); }
	inline int32_t get_U3CiU3E5__8_15() const { return ___U3CiU3E5__8_15; }
	inline int32_t* get_address_of_U3CiU3E5__8_15() { return &___U3CiU3E5__8_15; }
	inline void set_U3CiU3E5__8_15(int32_t value)
	{
		___U3CiU3E5__8_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEFILTERU3ED__12_T222072922_H
#ifndef U3CEXECUTEFILTERU3ED__4_T508957393_H
#define U3CEXECUTEFILTERU3ED__4_T508957393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter/<ExecuteFilter>d__4
struct  U3CExecuteFilterU3Ed__4_t508957393  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter/<ExecuteFilter>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter/<ExecuteFilter>d__4::<>2__current
	JToken_t1038539247 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter/<ExecuteFilter>d__4::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter/<ExecuteFilter>d__4::current
	RuntimeObject* ___current_3;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter/<ExecuteFilter>d__4::<>3__current
	RuntimeObject* ___U3CU3E3__current_4;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter/<ExecuteFilter>d__4::errorWhenNoMatch
	bool ___errorWhenNoMatch_5;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter/<ExecuteFilter>d__4::<>3__errorWhenNoMatch
	bool ___U3CU3E3__errorWhenNoMatch_6;
	// Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter/<ExecuteFilter>d__4::<>4__this
	ArrayIndexFilter_t2739218971 * ___U3CU3E4__this_7;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter/<ExecuteFilter>d__4::<>s__1
	RuntimeObject* ___U3CU3Es__1_8;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter/<ExecuteFilter>d__4::<t>5__2
	JToken_t1038539247 * ___U3CtU3E5__2_9;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter/<ExecuteFilter>d__4::<v>5__3
	JToken_t1038539247 * ___U3CvU3E5__3_10;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter/<ExecuteFilter>d__4::<>s__4
	RuntimeObject* ___U3CU3Es__4_11;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter/<ExecuteFilter>d__4::<v>5__5
	JToken_t1038539247 * ___U3CvU3E5__5_12;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t508957393, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t508957393, ___U3CU3E2__current_1)); }
	inline JToken_t1038539247 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_t1038539247 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_t1038539247 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t508957393, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t508957393, ___current_3)); }
	inline RuntimeObject* get_current_3() const { return ___current_3; }
	inline RuntimeObject** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__current_4() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t508957393, ___U3CU3E3__current_4)); }
	inline RuntimeObject* get_U3CU3E3__current_4() const { return ___U3CU3E3__current_4; }
	inline RuntimeObject** get_address_of_U3CU3E3__current_4() { return &___U3CU3E3__current_4; }
	inline void set_U3CU3E3__current_4(RuntimeObject* value)
	{
		___U3CU3E3__current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__current_4), value);
	}

	inline static int32_t get_offset_of_errorWhenNoMatch_5() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t508957393, ___errorWhenNoMatch_5)); }
	inline bool get_errorWhenNoMatch_5() const { return ___errorWhenNoMatch_5; }
	inline bool* get_address_of_errorWhenNoMatch_5() { return &___errorWhenNoMatch_5; }
	inline void set_errorWhenNoMatch_5(bool value)
	{
		___errorWhenNoMatch_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__errorWhenNoMatch_6() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t508957393, ___U3CU3E3__errorWhenNoMatch_6)); }
	inline bool get_U3CU3E3__errorWhenNoMatch_6() const { return ___U3CU3E3__errorWhenNoMatch_6; }
	inline bool* get_address_of_U3CU3E3__errorWhenNoMatch_6() { return &___U3CU3E3__errorWhenNoMatch_6; }
	inline void set_U3CU3E3__errorWhenNoMatch_6(bool value)
	{
		___U3CU3E3__errorWhenNoMatch_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_7() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t508957393, ___U3CU3E4__this_7)); }
	inline ArrayIndexFilter_t2739218971 * get_U3CU3E4__this_7() const { return ___U3CU3E4__this_7; }
	inline ArrayIndexFilter_t2739218971 ** get_address_of_U3CU3E4__this_7() { return &___U3CU3E4__this_7; }
	inline void set_U3CU3E4__this_7(ArrayIndexFilter_t2739218971 * value)
	{
		___U3CU3E4__this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Es__1_8() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t508957393, ___U3CU3Es__1_8)); }
	inline RuntimeObject* get_U3CU3Es__1_8() const { return ___U3CU3Es__1_8; }
	inline RuntimeObject** get_address_of_U3CU3Es__1_8() { return &___U3CU3Es__1_8; }
	inline void set_U3CU3Es__1_8(RuntimeObject* value)
	{
		___U3CU3Es__1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Es__1_8), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__2_9() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t508957393, ___U3CtU3E5__2_9)); }
	inline JToken_t1038539247 * get_U3CtU3E5__2_9() const { return ___U3CtU3E5__2_9; }
	inline JToken_t1038539247 ** get_address_of_U3CtU3E5__2_9() { return &___U3CtU3E5__2_9; }
	inline void set_U3CtU3E5__2_9(JToken_t1038539247 * value)
	{
		___U3CtU3E5__2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtU3E5__2_9), value);
	}

	inline static int32_t get_offset_of_U3CvU3E5__3_10() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t508957393, ___U3CvU3E5__3_10)); }
	inline JToken_t1038539247 * get_U3CvU3E5__3_10() const { return ___U3CvU3E5__3_10; }
	inline JToken_t1038539247 ** get_address_of_U3CvU3E5__3_10() { return &___U3CvU3E5__3_10; }
	inline void set_U3CvU3E5__3_10(JToken_t1038539247 * value)
	{
		___U3CvU3E5__3_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvU3E5__3_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Es__4_11() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t508957393, ___U3CU3Es__4_11)); }
	inline RuntimeObject* get_U3CU3Es__4_11() const { return ___U3CU3Es__4_11; }
	inline RuntimeObject** get_address_of_U3CU3Es__4_11() { return &___U3CU3Es__4_11; }
	inline void set_U3CU3Es__4_11(RuntimeObject* value)
	{
		___U3CU3Es__4_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Es__4_11), value);
	}

	inline static int32_t get_offset_of_U3CvU3E5__5_12() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t508957393, ___U3CvU3E5__5_12)); }
	inline JToken_t1038539247 * get_U3CvU3E5__5_12() const { return ___U3CvU3E5__5_12; }
	inline JToken_t1038539247 ** get_address_of_U3CvU3E5__5_12() { return &___U3CvU3E5__5_12; }
	inline void set_U3CvU3E5__5_12(JToken_t1038539247 * value)
	{
		___U3CvU3E5__5_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvU3E5__5_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEFILTERU3ED__4_T508957393_H
#ifndef U3CGETENUMERATORU3ED__1_T1232179978_H
#define U3CGETENUMERATORU3ED__1_T1232179978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JProperty/JPropertyList/<GetEnumerator>d__1
struct  U3CGetEnumeratorU3Ed__1_t1232179978  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JProperty/JPropertyList/<GetEnumerator>d__1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty/JPropertyList/<GetEnumerator>d__1::<>2__current
	JToken_t1038539247 * ___U3CU3E2__current_1;
	// Newtonsoft.Json.Linq.JProperty/JPropertyList Newtonsoft.Json.Linq.JProperty/JPropertyList/<GetEnumerator>d__1::<>4__this
	JPropertyList_t1362261004 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__1_t1232179978, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__1_t1232179978, ___U3CU3E2__current_1)); }
	inline JToken_t1038539247 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_t1038539247 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_t1038539247 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__1_t1232179978, ___U3CU3E4__this_2)); }
	inline JPropertyList_t1362261004 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline JPropertyList_t1362261004 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(JPropertyList_t1362261004 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__1_T1232179978_H
#ifndef JPROPERTYLIST_T1362261004_H
#define JPROPERTYLIST_T1362261004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JProperty/JPropertyList
struct  JPropertyList_t1362261004  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty/JPropertyList::_token
	JToken_t1038539247 * ____token_0;

public:
	inline static int32_t get_offset_of__token_0() { return static_cast<int32_t>(offsetof(JPropertyList_t1362261004, ____token_0)); }
	inline JToken_t1038539247 * get__token_0() const { return ____token_0; }
	inline JToken_t1038539247 ** get_address_of__token_0() { return &____token_0; }
	inline void set__token_0(JToken_t1038539247 * value)
	{
		____token_0 = value;
		Il2CppCodeGenWriteBarrier((&____token_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPROPERTYLIST_T1362261004_H
#ifndef U3CANNOTATIONSU3ED__172_T3494334042_H
#define U3CANNOTATIONSU3ED__172_T3494334042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JToken/<Annotations>d__172
struct  U3CAnnotationsU3Ed__172_t3494334042  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JToken/<Annotations>d__172::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Newtonsoft.Json.Linq.JToken/<Annotations>d__172::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JToken/<Annotations>d__172::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Type Newtonsoft.Json.Linq.JToken/<Annotations>d__172::type
	Type_t * ___type_3;
	// System.Type Newtonsoft.Json.Linq.JToken/<Annotations>d__172::<>3__type
	Type_t * ___U3CU3E3__type_4;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken/<Annotations>d__172::<>4__this
	JToken_t1038539247 * ___U3CU3E4__this_5;
	// System.Object[] Newtonsoft.Json.Linq.JToken/<Annotations>d__172::<annotations>5__1
	ObjectU5BU5D_t2843939325* ___U3CannotationsU3E5__1_6;
	// System.Int32 Newtonsoft.Json.Linq.JToken/<Annotations>d__172::<i>5__2
	int32_t ___U3CiU3E5__2_7;
	// System.Object Newtonsoft.Json.Linq.JToken/<Annotations>d__172::<o>5__3
	RuntimeObject * ___U3CoU3E5__3_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAnnotationsU3Ed__172_t3494334042, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAnnotationsU3Ed__172_t3494334042, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CAnnotationsU3Ed__172_t3494334042, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(U3CAnnotationsU3Ed__172_t3494334042, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__type_4() { return static_cast<int32_t>(offsetof(U3CAnnotationsU3Ed__172_t3494334042, ___U3CU3E3__type_4)); }
	inline Type_t * get_U3CU3E3__type_4() const { return ___U3CU3E3__type_4; }
	inline Type_t ** get_address_of_U3CU3E3__type_4() { return &___U3CU3E3__type_4; }
	inline void set_U3CU3E3__type_4(Type_t * value)
	{
		___U3CU3E3__type_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__type_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CAnnotationsU3Ed__172_t3494334042, ___U3CU3E4__this_5)); }
	inline JToken_t1038539247 * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline JToken_t1038539247 ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(JToken_t1038539247 * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CannotationsU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CAnnotationsU3Ed__172_t3494334042, ___U3CannotationsU3E5__1_6)); }
	inline ObjectU5BU5D_t2843939325* get_U3CannotationsU3E5__1_6() const { return ___U3CannotationsU3E5__1_6; }
	inline ObjectU5BU5D_t2843939325** get_address_of_U3CannotationsU3E5__1_6() { return &___U3CannotationsU3E5__1_6; }
	inline void set_U3CannotationsU3E5__1_6(ObjectU5BU5D_t2843939325* value)
	{
		___U3CannotationsU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CannotationsU3E5__1_6), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CAnnotationsU3Ed__172_t3494334042, ___U3CiU3E5__2_7)); }
	inline int32_t get_U3CiU3E5__2_7() const { return ___U3CiU3E5__2_7; }
	inline int32_t* get_address_of_U3CiU3E5__2_7() { return &___U3CiU3E5__2_7; }
	inline void set_U3CiU3E5__2_7(int32_t value)
	{
		___U3CiU3E5__2_7 = value;
	}

	inline static int32_t get_offset_of_U3CoU3E5__3_8() { return static_cast<int32_t>(offsetof(U3CAnnotationsU3Ed__172_t3494334042, ___U3CoU3E5__3_8)); }
	inline RuntimeObject * get_U3CoU3E5__3_8() const { return ___U3CoU3E5__3_8; }
	inline RuntimeObject ** get_address_of_U3CoU3E5__3_8() { return &___U3CoU3E5__3_8; }
	inline void set_U3CoU3E5__3_8(RuntimeObject * value)
	{
		___U3CoU3E5__3_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoU3E5__3_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANNOTATIONSU3ED__172_T3494334042_H
#ifndef REFLECTIONVALUEPROVIDER_T2127813129_H
#define REFLECTIONVALUEPROVIDER_T2127813129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.ReflectionValueProvider
struct  ReflectionValueProvider_t2127813129  : public RuntimeObject
{
public:
	// System.Reflection.MemberInfo Newtonsoft.Json.Serialization.ReflectionValueProvider::_memberInfo
	MemberInfo_t * ____memberInfo_0;

public:
	inline static int32_t get_offset_of__memberInfo_0() { return static_cast<int32_t>(offsetof(ReflectionValueProvider_t2127813129, ____memberInfo_0)); }
	inline MemberInfo_t * get__memberInfo_0() const { return ____memberInfo_0; }
	inline MemberInfo_t ** get_address_of__memberInfo_0() { return &____memberInfo_0; }
	inline void set__memberInfo_0(MemberInfo_t * value)
	{
		____memberInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____memberInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONVALUEPROVIDER_T2127813129_H
#ifndef U3CU3EC_T751952102_H
#define U3CU3EC_T751952102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonTypeReflector/<>c
struct  U3CU3Ec_t751952102  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t751952102_StaticFields
{
public:
	// Newtonsoft.Json.Serialization.JsonTypeReflector/<>c Newtonsoft.Json.Serialization.JsonTypeReflector/<>c::<>9
	U3CU3Ec_t751952102 * ___U3CU3E9_0;
	// System.Func`2<System.Object,System.Type> Newtonsoft.Json.Serialization.JsonTypeReflector/<>c::<>9__18_1
	Func_2_t1850968970 * ___U3CU3E9__18_1_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t751952102_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t751952102 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t751952102 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t751952102 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__18_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t751952102_StaticFields, ___U3CU3E9__18_1_1)); }
	inline Func_2_t1850968970 * get_U3CU3E9__18_1_1() const { return ___U3CU3E9__18_1_1; }
	inline Func_2_t1850968970 ** get_address_of_U3CU3E9__18_1_1() { return &___U3CU3E9__18_1_1; }
	inline void set_U3CU3E9__18_1_1(Func_2_t1850968970 * value)
	{
		___U3CU3E9__18_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__18_1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T751952102_H
#ifndef U3CU3EC__DISPLAYCLASS18_0_T667614246_H
#define U3CU3EC__DISPLAYCLASS18_0_T667614246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonTypeReflector/<>c__DisplayClass18_0
struct  U3CU3Ec__DisplayClass18_0_t667614246  : public RuntimeObject
{
public:
	// System.Type Newtonsoft.Json.Serialization.JsonTypeReflector/<>c__DisplayClass18_0::converterType
	Type_t * ___converterType_0;
	// System.Func`1<System.Object> Newtonsoft.Json.Serialization.JsonTypeReflector/<>c__DisplayClass18_0::defaultConstructor
	Func_1_t2509852811 * ___defaultConstructor_1;

public:
	inline static int32_t get_offset_of_converterType_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t667614246, ___converterType_0)); }
	inline Type_t * get_converterType_0() const { return ___converterType_0; }
	inline Type_t ** get_address_of_converterType_0() { return &___converterType_0; }
	inline void set_converterType_0(Type_t * value)
	{
		___converterType_0 = value;
		Il2CppCodeGenWriteBarrier((&___converterType_0), value);
	}

	inline static int32_t get_offset_of_defaultConstructor_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t667614246, ___defaultConstructor_1)); }
	inline Func_1_t2509852811 * get_defaultConstructor_1() const { return ___defaultConstructor_1; }
	inline Func_1_t2509852811 ** get_address_of_defaultConstructor_1() { return &___defaultConstructor_1; }
	inline void set_defaultConstructor_1(Func_1_t2509852811 * value)
	{
		___defaultConstructor_1 = value;
		Il2CppCodeGenWriteBarrier((&___defaultConstructor_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS18_0_T667614246_H
#ifndef JSONSERIALIZERINTERNALBASE_T286011480_H
#define JSONSERIALIZERINTERNALBASE_T286011480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalBase
struct  JsonSerializerInternalBase_t286011480  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.ErrorContext Newtonsoft.Json.Serialization.JsonSerializerInternalBase::_currentErrorContext
	ErrorContext_t3532118581 * ____currentErrorContext_0;
	// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalBase::_mappings
	BidirectionalDictionary_2_t2019708942 * ____mappings_1;
	// Newtonsoft.Json.JsonSerializer Newtonsoft.Json.Serialization.JsonSerializerInternalBase::Serializer
	JsonSerializer_t1424496335 * ___Serializer_2;
	// Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.Serialization.JsonSerializerInternalBase::TraceWriter
	RuntimeObject* ___TraceWriter_3;
	// Newtonsoft.Json.Serialization.JsonSerializerProxy Newtonsoft.Json.Serialization.JsonSerializerInternalBase::InternalSerializer
	JsonSerializerProxy_t657065764 * ___InternalSerializer_4;

public:
	inline static int32_t get_offset_of__currentErrorContext_0() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t286011480, ____currentErrorContext_0)); }
	inline ErrorContext_t3532118581 * get__currentErrorContext_0() const { return ____currentErrorContext_0; }
	inline ErrorContext_t3532118581 ** get_address_of__currentErrorContext_0() { return &____currentErrorContext_0; }
	inline void set__currentErrorContext_0(ErrorContext_t3532118581 * value)
	{
		____currentErrorContext_0 = value;
		Il2CppCodeGenWriteBarrier((&____currentErrorContext_0), value);
	}

	inline static int32_t get_offset_of__mappings_1() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t286011480, ____mappings_1)); }
	inline BidirectionalDictionary_2_t2019708942 * get__mappings_1() const { return ____mappings_1; }
	inline BidirectionalDictionary_2_t2019708942 ** get_address_of__mappings_1() { return &____mappings_1; }
	inline void set__mappings_1(BidirectionalDictionary_2_t2019708942 * value)
	{
		____mappings_1 = value;
		Il2CppCodeGenWriteBarrier((&____mappings_1), value);
	}

	inline static int32_t get_offset_of_Serializer_2() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t286011480, ___Serializer_2)); }
	inline JsonSerializer_t1424496335 * get_Serializer_2() const { return ___Serializer_2; }
	inline JsonSerializer_t1424496335 ** get_address_of_Serializer_2() { return &___Serializer_2; }
	inline void set_Serializer_2(JsonSerializer_t1424496335 * value)
	{
		___Serializer_2 = value;
		Il2CppCodeGenWriteBarrier((&___Serializer_2), value);
	}

	inline static int32_t get_offset_of_TraceWriter_3() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t286011480, ___TraceWriter_3)); }
	inline RuntimeObject* get_TraceWriter_3() const { return ___TraceWriter_3; }
	inline RuntimeObject** get_address_of_TraceWriter_3() { return &___TraceWriter_3; }
	inline void set_TraceWriter_3(RuntimeObject* value)
	{
		___TraceWriter_3 = value;
		Il2CppCodeGenWriteBarrier((&___TraceWriter_3), value);
	}

	inline static int32_t get_offset_of_InternalSerializer_4() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t286011480, ___InternalSerializer_4)); }
	inline JsonSerializerProxy_t657065764 * get_InternalSerializer_4() const { return ___InternalSerializer_4; }
	inline JsonSerializerProxy_t657065764 ** get_address_of_InternalSerializer_4() { return &___InternalSerializer_4; }
	inline void set_InternalSerializer_4(JsonSerializerProxy_t657065764 * value)
	{
		___InternalSerializer_4 = value;
		Il2CppCodeGenWriteBarrier((&___InternalSerializer_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZERINTERNALBASE_T286011480_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef JSONMERGESETTINGS_T3341555814_H
#define JSONMERGESETTINGS_T3341555814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonMergeSettings
struct  JsonMergeSettings_t3341555814  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONMERGESETTINGS_T3341555814_H
#ifndef XMLNODEWRAPPER_T3016097339_H
#define XMLNODEWRAPPER_T3016097339_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlNodeWrapper
struct  XmlNodeWrapper_t3016097339  : public RuntimeObject
{
public:
	// System.Xml.XmlNode Newtonsoft.Json.Converters.XmlNodeWrapper::_node
	XmlNode_t3767805227 * ____node_0;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XmlNodeWrapper::_childNodes
	List_1_t2041514553 * ____childNodes_1;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XmlNodeWrapper::_attributes
	List_1_t2041514553 * ____attributes_2;

public:
	inline static int32_t get_offset_of__node_0() { return static_cast<int32_t>(offsetof(XmlNodeWrapper_t3016097339, ____node_0)); }
	inline XmlNode_t3767805227 * get__node_0() const { return ____node_0; }
	inline XmlNode_t3767805227 ** get_address_of__node_0() { return &____node_0; }
	inline void set__node_0(XmlNode_t3767805227 * value)
	{
		____node_0 = value;
		Il2CppCodeGenWriteBarrier((&____node_0), value);
	}

	inline static int32_t get_offset_of__childNodes_1() { return static_cast<int32_t>(offsetof(XmlNodeWrapper_t3016097339, ____childNodes_1)); }
	inline List_1_t2041514553 * get__childNodes_1() const { return ____childNodes_1; }
	inline List_1_t2041514553 ** get_address_of__childNodes_1() { return &____childNodes_1; }
	inline void set__childNodes_1(List_1_t2041514553 * value)
	{
		____childNodes_1 = value;
		Il2CppCodeGenWriteBarrier((&____childNodes_1), value);
	}

	inline static int32_t get_offset_of__attributes_2() { return static_cast<int32_t>(offsetof(XmlNodeWrapper_t3016097339, ____attributes_2)); }
	inline List_1_t2041514553 * get__attributes_2() const { return ____attributes_2; }
	inline List_1_t2041514553 ** get_address_of__attributes_2() { return &____attributes_2; }
	inline void set__attributes_2(List_1_t2041514553 * value)
	{
		____attributes_2 = value;
		Il2CppCodeGenWriteBarrier((&____attributes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODEWRAPPER_T3016097339_H
#ifndef U3CU3EC__DISPLAYCLASS36_0_T358886780_H
#define U3CU3EC__DISPLAYCLASS36_0_T358886780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c__DisplayClass36_0
struct  U3CU3Ec__DisplayClass36_0_t358886780  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c__DisplayClass36_0::property
	JsonProperty_t1405154176 * ___property_0;

public:
	inline static int32_t get_offset_of_property_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_t358886780, ___property_0)); }
	inline JsonProperty_t1405154176 * get_property_0() const { return ___property_0; }
	inline JsonProperty_t1405154176 ** get_address_of_property_0() { return &___property_0; }
	inline void set_property_0(JsonProperty_t1405154176 * value)
	{
		___property_0 = value;
		Il2CppCodeGenWriteBarrier((&___property_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS36_0_T358886780_H
#ifndef U3CU3EC_T1720245494_H
#define U3CU3EC_T1720245494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.Extensions/<>c
struct  U3CU3Ec_t1720245494  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1720245494_StaticFields
{
public:
	// Newtonsoft.Json.Linq.Extensions/<>c Newtonsoft.Json.Linq.Extensions/<>c::<>9
	U3CU3Ec_t1720245494 * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1720245494_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1720245494 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1720245494 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1720245494 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1720245494_H
#ifndef U3CU3EC_T4156374721_H
#define U3CU3EC_T4156374721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c
struct  U3CU3Ec_t4156374721  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t4156374721_StaticFields
{
public:
	// Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c::<>9
	U3CU3Ec_t4156374721 * ___U3CU3E9_0;
	// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,System.String> Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c::<>9__36_0
	Func_2_t705144599 * ___U3CU3E9__36_0_1;
	// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,System.String> Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c::<>9__36_2
	Func_2_t705144599 * ___U3CU3E9__36_2_2;
	// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonProperty> Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c::<>9__41_0
	Func_2_t262848086 * ___U3CU3E9__41_0_3;
	// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence> Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c::<>9__41_1
	Func_2_t1348288522 * ___U3CU3E9__41_1_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4156374721_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t4156374721 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t4156374721 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t4156374721 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__36_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4156374721_StaticFields, ___U3CU3E9__36_0_1)); }
	inline Func_2_t705144599 * get_U3CU3E9__36_0_1() const { return ___U3CU3E9__36_0_1; }
	inline Func_2_t705144599 ** get_address_of_U3CU3E9__36_0_1() { return &___U3CU3E9__36_0_1; }
	inline void set_U3CU3E9__36_0_1(Func_2_t705144599 * value)
	{
		___U3CU3E9__36_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__36_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__36_2_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4156374721_StaticFields, ___U3CU3E9__36_2_2)); }
	inline Func_2_t705144599 * get_U3CU3E9__36_2_2() const { return ___U3CU3E9__36_2_2; }
	inline Func_2_t705144599 ** get_address_of_U3CU3E9__36_2_2() { return &___U3CU3E9__36_2_2; }
	inline void set_U3CU3E9__36_2_2(Func_2_t705144599 * value)
	{
		___U3CU3E9__36_2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__36_2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__41_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4156374721_StaticFields, ___U3CU3E9__41_0_3)); }
	inline Func_2_t262848086 * get_U3CU3E9__41_0_3() const { return ___U3CU3E9__41_0_3; }
	inline Func_2_t262848086 ** get_address_of_U3CU3E9__41_0_3() { return &___U3CU3E9__41_0_3; }
	inline void set_U3CU3E9__41_0_3(Func_2_t262848086 * value)
	{
		___U3CU3E9__41_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__41_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__41_1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4156374721_StaticFields, ___U3CU3E9__41_1_4)); }
	inline Func_2_t1348288522 * get_U3CU3E9__41_1_4() const { return ___U3CU3E9__41_1_4; }
	inline Func_2_t1348288522 ** get_address_of_U3CU3E9__41_1_4() { return &___U3CU3E9__41_1_4; }
	inline void set_U3CU3E9__41_1_4(Func_2_t1348288522 * value)
	{
		___U3CU3E9__41_1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__41_1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T4156374721_H
#ifndef EXTENSIONS_T2945510795_H
#define EXTENSIONS_T2945510795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.Extensions
struct  Extensions_t2945510795  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONS_T2945510795_H
#ifndef JTOKENEQUALITYCOMPARER_T2051491032_H
#define JTOKENEQUALITYCOMPARER_T2051491032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JTokenEqualityComparer
struct  JTokenEqualityComparer_t2051491032  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKENEQUALITYCOMPARER_T2051491032_H
#ifndef PROPERTYDESCRIPTOR_T3244362832_H
#define PROPERTYDESCRIPTOR_T3244362832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.PropertyDescriptor
struct  PropertyDescriptor_t3244362832  : public MemberDescriptor_t3815403747
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYDESCRIPTOR_T3244362832_H
#ifndef HASHSETCONVERTER_T587720113_H
#define HASHSETCONVERTER_T587720113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.HashSetConverter
struct  HashSetConverter_t587720113  : public JsonConverter_t1047106545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHSETCONVERTER_T587720113_H
#ifndef DATETIMECONVERTERBASE_T4233217353_H
#define DATETIMECONVERTERBASE_T4233217353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.DateTimeConverterBase
struct  DateTimeConverterBase_t4233217353  : public JsonConverter_t1047106545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMECONVERTERBASE_T4233217353_H
#ifndef BINARYCONVERTER_T779619639_H
#define BINARYCONVERTER_T779619639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.BinaryConverter
struct  BinaryConverter_t779619639  : public JsonConverter_t1047106545
{
public:
	// Newtonsoft.Json.Utilities.ReflectionObject Newtonsoft.Json.Converters.BinaryConverter::_reflectionObject
	ReflectionObject_t701100009 * ____reflectionObject_0;

public:
	inline static int32_t get_offset_of__reflectionObject_0() { return static_cast<int32_t>(offsetof(BinaryConverter_t779619639, ____reflectionObject_0)); }
	inline ReflectionObject_t701100009 * get__reflectionObject_0() const { return ____reflectionObject_0; }
	inline ReflectionObject_t701100009 ** get_address_of__reflectionObject_0() { return &____reflectionObject_0; }
	inline void set__reflectionObject_0(ReflectionObject_t701100009 * value)
	{
		____reflectionObject_0 = value;
		Il2CppCodeGenWriteBarrier((&____reflectionObject_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYCONVERTER_T779619639_H
#ifndef QUERYFILTER_T1584032927_H
#define QUERYFILTER_T1584032927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.QueryFilter
struct  QueryFilter_t1584032927  : public PathFilter_t3862968645
{
public:
	// Newtonsoft.Json.Linq.JsonPath.QueryExpression Newtonsoft.Json.Linq.JsonPath.QueryFilter::<Expression>k__BackingField
	QueryExpression_t3171385195 * ___U3CExpressionU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CExpressionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(QueryFilter_t1584032927, ___U3CExpressionU3Ek__BackingField_0)); }
	inline QueryExpression_t3171385195 * get_U3CExpressionU3Ek__BackingField_0() const { return ___U3CExpressionU3Ek__BackingField_0; }
	inline QueryExpression_t3171385195 ** get_address_of_U3CExpressionU3Ek__BackingField_0() { return &___U3CExpressionU3Ek__BackingField_0; }
	inline void set_U3CExpressionU3Ek__BackingField_0(QueryExpression_t3171385195 * value)
	{
		___U3CExpressionU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExpressionU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYFILTER_T1584032927_H
#ifndef NULLABLE_1_T1819850047_H
#define NULLABLE_1_T1819850047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t1819850047 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1819850047, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1819850047, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1819850047_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef SCANFILTER_T1566003751_H
#define SCANFILTER_T1566003751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.ScanFilter
struct  ScanFilter_t1566003751  : public PathFilter_t3862968645
{
public:
	// System.String Newtonsoft.Json.Linq.JsonPath.ScanFilter::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ScanFilter_t1566003751, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCANFILTER_T1566003751_H
#ifndef KEYVALUEPAIRCONVERTER_T2108458033_H
#define KEYVALUEPAIRCONVERTER_T2108458033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.KeyValuePairConverter
struct  KeyValuePairConverter_t2108458033  : public JsonConverter_t1047106545
{
public:

public:
};

struct KeyValuePairConverter_t2108458033_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.ReflectionObject> Newtonsoft.Json.Converters.KeyValuePairConverter::ReflectionObjectPerType
	ThreadSafeStore_2_t4079379169 * ___ReflectionObjectPerType_0;

public:
	inline static int32_t get_offset_of_ReflectionObjectPerType_0() { return static_cast<int32_t>(offsetof(KeyValuePairConverter_t2108458033_StaticFields, ___ReflectionObjectPerType_0)); }
	inline ThreadSafeStore_2_t4079379169 * get_ReflectionObjectPerType_0() const { return ___ReflectionObjectPerType_0; }
	inline ThreadSafeStore_2_t4079379169 ** get_address_of_ReflectionObjectPerType_0() { return &___ReflectionObjectPerType_0; }
	inline void set_ReflectionObjectPerType_0(ThreadSafeStore_2_t4079379169 * value)
	{
		___ReflectionObjectPerType_0 = value;
		Il2CppCodeGenWriteBarrier((&___ReflectionObjectPerType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIRCONVERTER_T2108458033_H
#ifndef XDOCUMENTTYPEWRAPPER_T2526962505_H
#define XDOCUMENTTYPEWRAPPER_T2526962505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XDocumentTypeWrapper
struct  XDocumentTypeWrapper_t2526962505  : public XObjectWrapper_t2266598274
{
public:
	// System.Xml.Linq.XDocumentType Newtonsoft.Json.Converters.XDocumentTypeWrapper::_documentType
	XDocumentType_t1853592271 * ____documentType_2;

public:
	inline static int32_t get_offset_of__documentType_2() { return static_cast<int32_t>(offsetof(XDocumentTypeWrapper_t2526962505, ____documentType_2)); }
	inline XDocumentType_t1853592271 * get__documentType_2() const { return ____documentType_2; }
	inline XDocumentType_t1853592271 ** get_address_of__documentType_2() { return &____documentType_2; }
	inline void set__documentType_2(XDocumentType_t1853592271 * value)
	{
		____documentType_2 = value;
		Il2CppCodeGenWriteBarrier((&____documentType_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDOCUMENTTYPEWRAPPER_T2526962505_H
#ifndef XDECLARATIONWRAPPER_T1986085046_H
#define XDECLARATIONWRAPPER_T1986085046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XDeclarationWrapper
struct  XDeclarationWrapper_t1986085046  : public XObjectWrapper_t2266598274
{
public:
	// System.Xml.Linq.XDeclaration Newtonsoft.Json.Converters.XDeclarationWrapper::<Declaration>k__BackingField
	XDeclaration_t2907650823 * ___U3CDeclarationU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CDeclarationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(XDeclarationWrapper_t1986085046, ___U3CDeclarationU3Ek__BackingField_2)); }
	inline XDeclaration_t2907650823 * get_U3CDeclarationU3Ek__BackingField_2() const { return ___U3CDeclarationU3Ek__BackingField_2; }
	inline XDeclaration_t2907650823 ** get_address_of_U3CDeclarationU3Ek__BackingField_2() { return &___U3CDeclarationU3Ek__BackingField_2; }
	inline void set_U3CDeclarationU3Ek__BackingField_2(XDeclaration_t2907650823 * value)
	{
		___U3CDeclarationU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeclarationU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDECLARATIONWRAPPER_T1986085046_H
#ifndef VERSIONCONVERTER_T4153846029_H
#define VERSIONCONVERTER_T4153846029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.VersionConverter
struct  VersionConverter_t4153846029  : public JsonConverter_t1047106545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERSIONCONVERTER_T4153846029_H
#ifndef XMLDOCUMENTTYPEWRAPPER_T2345418247_H
#define XMLDOCUMENTTYPEWRAPPER_T2345418247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlDocumentTypeWrapper
struct  XmlDocumentTypeWrapper_t2345418247  : public XmlNodeWrapper_t3016097339
{
public:
	// System.Xml.XmlDocumentType Newtonsoft.Json.Converters.XmlDocumentTypeWrapper::_documentType
	XmlDocumentType_t4112370061 * ____documentType_3;

public:
	inline static int32_t get_offset_of__documentType_3() { return static_cast<int32_t>(offsetof(XmlDocumentTypeWrapper_t2345418247, ____documentType_3)); }
	inline XmlDocumentType_t4112370061 * get__documentType_3() const { return ____documentType_3; }
	inline XmlDocumentType_t4112370061 ** get_address_of__documentType_3() { return &____documentType_3; }
	inline void set__documentType_3(XmlDocumentType_t4112370061 * value)
	{
		____documentType_3 = value;
		Il2CppCodeGenWriteBarrier((&____documentType_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENTTYPEWRAPPER_T2345418247_H
#ifndef XMLDOCUMENTWRAPPER_T3575962533_H
#define XMLDOCUMENTWRAPPER_T3575962533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlDocumentWrapper
struct  XmlDocumentWrapper_t3575962533  : public XmlNodeWrapper_t3016097339
{
public:
	// System.Xml.XmlDocument Newtonsoft.Json.Converters.XmlDocumentWrapper::_document
	XmlDocument_t2837193595 * ____document_3;

public:
	inline static int32_t get_offset_of__document_3() { return static_cast<int32_t>(offsetof(XmlDocumentWrapper_t3575962533, ____document_3)); }
	inline XmlDocument_t2837193595 * get__document_3() const { return ____document_3; }
	inline XmlDocument_t2837193595 ** get_address_of__document_3() { return &____document_3; }
	inline void set__document_3(XmlDocument_t2837193595 * value)
	{
		____document_3 = value;
		Il2CppCodeGenWriteBarrier((&____document_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENTWRAPPER_T3575962533_H
#ifndef XMLELEMENTWRAPPER_T3185492177_H
#define XMLELEMENTWRAPPER_T3185492177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlElementWrapper
struct  XmlElementWrapper_t3185492177  : public XmlNodeWrapper_t3016097339
{
public:
	// System.Xml.XmlElement Newtonsoft.Json.Converters.XmlElementWrapper::_element
	XmlElement_t561603118 * ____element_3;

public:
	inline static int32_t get_offset_of__element_3() { return static_cast<int32_t>(offsetof(XmlElementWrapper_t3185492177, ____element_3)); }
	inline XmlElement_t561603118 * get__element_3() const { return ____element_3; }
	inline XmlElement_t561603118 ** get_address_of__element_3() { return &____element_3; }
	inline void set__element_3(XmlElement_t561603118 * value)
	{
		____element_3 = value;
		Il2CppCodeGenWriteBarrier((&____element_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLELEMENTWRAPPER_T3185492177_H
#ifndef ENUMERATOR_T2017297076_H
#define ENUMERATOR_T2017297076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Int32>
struct  Enumerator_t2017297076 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t128053199 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2017297076, ___l_0)); }
	inline List_1_t128053199 * get_l_0() const { return ___l_0; }
	inline List_1_t128053199 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t128053199 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2017297076, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2017297076, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2017297076, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2017297076_H
#ifndef BSONOBJECTIDCONVERTER_T1190033754_H
#define BSONOBJECTIDCONVERTER_T1190033754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.BsonObjectIdConverter
struct  BsonObjectIdConverter_t1190033754  : public JsonConverter_t1047106545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONOBJECTIDCONVERTER_T1190033754_H
#ifndef STRINGENUMCONVERTER_T3708940030_H
#define STRINGENUMCONVERTER_T3708940030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.StringEnumConverter
struct  StringEnumConverter_t3708940030  : public JsonConverter_t1047106545
{
public:
	// System.Boolean Newtonsoft.Json.Converters.StringEnumConverter::<CamelCaseText>k__BackingField
	bool ___U3CCamelCaseTextU3Ek__BackingField_0;
	// System.Boolean Newtonsoft.Json.Converters.StringEnumConverter::<AllowIntegerValues>k__BackingField
	bool ___U3CAllowIntegerValuesU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CCamelCaseTextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StringEnumConverter_t3708940030, ___U3CCamelCaseTextU3Ek__BackingField_0)); }
	inline bool get_U3CCamelCaseTextU3Ek__BackingField_0() const { return ___U3CCamelCaseTextU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CCamelCaseTextU3Ek__BackingField_0() { return &___U3CCamelCaseTextU3Ek__BackingField_0; }
	inline void set_U3CCamelCaseTextU3Ek__BackingField_0(bool value)
	{
		___U3CCamelCaseTextU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CAllowIntegerValuesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(StringEnumConverter_t3708940030, ___U3CAllowIntegerValuesU3Ek__BackingField_1)); }
	inline bool get_U3CAllowIntegerValuesU3Ek__BackingField_1() const { return ___U3CAllowIntegerValuesU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CAllowIntegerValuesU3Ek__BackingField_1() { return &___U3CAllowIntegerValuesU3Ek__BackingField_1; }
	inline void set_U3CAllowIntegerValuesU3Ek__BackingField_1(bool value)
	{
		___U3CAllowIntegerValuesU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGENUMCONVERTER_T3708940030_H
#ifndef XMLDECLARATIONWRAPPER_T2867104133_H
#define XMLDECLARATIONWRAPPER_T2867104133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlDeclarationWrapper
struct  XmlDeclarationWrapper_t2867104133  : public XmlNodeWrapper_t3016097339
{
public:
	// System.Xml.XmlDeclaration Newtonsoft.Json.Converters.XmlDeclarationWrapper::_declaration
	XmlDeclaration_t679870411 * ____declaration_3;

public:
	inline static int32_t get_offset_of__declaration_3() { return static_cast<int32_t>(offsetof(XmlDeclarationWrapper_t2867104133, ____declaration_3)); }
	inline XmlDeclaration_t679870411 * get__declaration_3() const { return ____declaration_3; }
	inline XmlDeclaration_t679870411 ** get_address_of__declaration_3() { return &____declaration_3; }
	inline void set__declaration_3(XmlDeclaration_t679870411 * value)
	{
		____declaration_3 = value;
		Il2CppCodeGenWriteBarrier((&____declaration_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDECLARATIONWRAPPER_T2867104133_H
#ifndef KEYVALUEPAIR_2_T3221467713_H
#define KEYVALUEPAIR_2_T3221467713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>
struct  KeyValuePair_2_t3221467713 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	JToken_t1038539247 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3221467713, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3221467713, ___value_1)); }
	inline JToken_t1038539247 * get_value_1() const { return ___value_1; }
	inline JToken_t1038539247 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(JToken_t1038539247 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3221467713_H
#ifndef VECTORCONVERTER_T1294738057_H
#define VECTORCONVERTER_T1294738057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.VectorConverter
struct  VectorConverter_t1294738057  : public JsonConverter_t1047106545
{
public:
	// System.Boolean Newtonsoft.Json.Converters.VectorConverter::<EnableVector2>k__BackingField
	bool ___U3CEnableVector2U3Ek__BackingField_3;
	// System.Boolean Newtonsoft.Json.Converters.VectorConverter::<EnableVector3>k__BackingField
	bool ___U3CEnableVector3U3Ek__BackingField_4;
	// System.Boolean Newtonsoft.Json.Converters.VectorConverter::<EnableVector4>k__BackingField
	bool ___U3CEnableVector4U3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CEnableVector2U3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(VectorConverter_t1294738057, ___U3CEnableVector2U3Ek__BackingField_3)); }
	inline bool get_U3CEnableVector2U3Ek__BackingField_3() const { return ___U3CEnableVector2U3Ek__BackingField_3; }
	inline bool* get_address_of_U3CEnableVector2U3Ek__BackingField_3() { return &___U3CEnableVector2U3Ek__BackingField_3; }
	inline void set_U3CEnableVector2U3Ek__BackingField_3(bool value)
	{
		___U3CEnableVector2U3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CEnableVector3U3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(VectorConverter_t1294738057, ___U3CEnableVector3U3Ek__BackingField_4)); }
	inline bool get_U3CEnableVector3U3Ek__BackingField_4() const { return ___U3CEnableVector3U3Ek__BackingField_4; }
	inline bool* get_address_of_U3CEnableVector3U3Ek__BackingField_4() { return &___U3CEnableVector3U3Ek__BackingField_4; }
	inline void set_U3CEnableVector3U3Ek__BackingField_4(bool value)
	{
		___U3CEnableVector3U3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CEnableVector4U3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(VectorConverter_t1294738057, ___U3CEnableVector4U3Ek__BackingField_5)); }
	inline bool get_U3CEnableVector4U3Ek__BackingField_5() const { return ___U3CEnableVector4U3Ek__BackingField_5; }
	inline bool* get_address_of_U3CEnableVector4U3Ek__BackingField_5() { return &___U3CEnableVector4U3Ek__BackingField_5; }
	inline void set_U3CEnableVector4U3Ek__BackingField_5(bool value)
	{
		___U3CEnableVector4U3Ek__BackingField_5 = value;
	}
};

struct VectorConverter_t1294738057_StaticFields
{
public:
	// System.Type Newtonsoft.Json.Converters.VectorConverter::V2
	Type_t * ___V2_0;
	// System.Type Newtonsoft.Json.Converters.VectorConverter::V3
	Type_t * ___V3_1;
	// System.Type Newtonsoft.Json.Converters.VectorConverter::V4
	Type_t * ___V4_2;

public:
	inline static int32_t get_offset_of_V2_0() { return static_cast<int32_t>(offsetof(VectorConverter_t1294738057_StaticFields, ___V2_0)); }
	inline Type_t * get_V2_0() const { return ___V2_0; }
	inline Type_t ** get_address_of_V2_0() { return &___V2_0; }
	inline void set_V2_0(Type_t * value)
	{
		___V2_0 = value;
		Il2CppCodeGenWriteBarrier((&___V2_0), value);
	}

	inline static int32_t get_offset_of_V3_1() { return static_cast<int32_t>(offsetof(VectorConverter_t1294738057_StaticFields, ___V3_1)); }
	inline Type_t * get_V3_1() const { return ___V3_1; }
	inline Type_t ** get_address_of_V3_1() { return &___V3_1; }
	inline void set_V3_1(Type_t * value)
	{
		___V3_1 = value;
		Il2CppCodeGenWriteBarrier((&___V3_1), value);
	}

	inline static int32_t get_offset_of_V4_2() { return static_cast<int32_t>(offsetof(VectorConverter_t1294738057_StaticFields, ___V4_2)); }
	inline Type_t * get_V4_2() const { return ___V4_2; }
	inline Type_t ** get_address_of_V4_2() { return &___V4_2; }
	inline void set_V4_2(Type_t * value)
	{
		___V4_2 = value;
		Il2CppCodeGenWriteBarrier((&___V4_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORCONVERTER_T1294738057_H
#ifndef NULLABLE_1_T378540539_H
#define NULLABLE_1_T378540539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t378540539 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T378540539_H
#ifndef ENUMERATOR_T913802012_H
#define ENUMERATOR_T913802012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.String>
struct  Enumerator_t913802012 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t3319525431 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	String_t* ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t913802012, ___l_0)); }
	inline List_1_t3319525431 * get_l_0() const { return ___l_0; }
	inline List_1_t3319525431 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t3319525431 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t913802012, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t913802012, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t913802012, ___current_3)); }
	inline String_t* get_current_3() const { return ___current_3; }
	inline String_t** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(String_t* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T913802012_H
#ifndef REGEXCONVERTER_T2517515405_H
#define REGEXCONVERTER_T2517515405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.RegexConverter
struct  RegexConverter_t2517515405  : public JsonConverter_t1047106545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXCONVERTER_T2517515405_H
#ifndef ARRAYMULTIPLEINDEXFILTER_T2913121614_H
#define ARRAYMULTIPLEINDEXFILTER_T2913121614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter
struct  ArrayMultipleIndexFilter_t2913121614  : public PathFilter_t3862968645
{
public:
	// System.Collections.Generic.List`1<System.Int32> Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter::<Indexes>k__BackingField
	List_1_t128053199 * ___U3CIndexesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CIndexesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ArrayMultipleIndexFilter_t2913121614, ___U3CIndexesU3Ek__BackingField_0)); }
	inline List_1_t128053199 * get_U3CIndexesU3Ek__BackingField_0() const { return ___U3CIndexesU3Ek__BackingField_0; }
	inline List_1_t128053199 ** get_address_of_U3CIndexesU3Ek__BackingField_0() { return &___U3CIndexesU3Ek__BackingField_0; }
	inline void set_U3CIndexesU3Ek__BackingField_0(List_1_t128053199 * value)
	{
		___U3CIndexesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIndexesU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYMULTIPLEINDEXFILTER_T2913121614_H
#ifndef JCONTAINER_T1023664833_H
#define JCONTAINER_T1023664833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JContainer
struct  JContainer_t1023664833  : public JToken_t1038539247
{
public:
	// System.ComponentModel.ListChangedEventHandler Newtonsoft.Json.Linq.JContainer::_listChanged
	ListChangedEventHandler_t1703970447 * ____listChanged_13;
	// System.Object Newtonsoft.Json.Linq.JContainer::_syncRoot
	RuntimeObject * ____syncRoot_14;
	// System.Boolean Newtonsoft.Json.Linq.JContainer::_busy
	bool ____busy_15;

public:
	inline static int32_t get_offset_of__listChanged_13() { return static_cast<int32_t>(offsetof(JContainer_t1023664833, ____listChanged_13)); }
	inline ListChangedEventHandler_t1703970447 * get__listChanged_13() const { return ____listChanged_13; }
	inline ListChangedEventHandler_t1703970447 ** get_address_of__listChanged_13() { return &____listChanged_13; }
	inline void set__listChanged_13(ListChangedEventHandler_t1703970447 * value)
	{
		____listChanged_13 = value;
		Il2CppCodeGenWriteBarrier((&____listChanged_13), value);
	}

	inline static int32_t get_offset_of__syncRoot_14() { return static_cast<int32_t>(offsetof(JContainer_t1023664833, ____syncRoot_14)); }
	inline RuntimeObject * get__syncRoot_14() const { return ____syncRoot_14; }
	inline RuntimeObject ** get_address_of__syncRoot_14() { return &____syncRoot_14; }
	inline void set__syncRoot_14(RuntimeObject * value)
	{
		____syncRoot_14 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_14), value);
	}

	inline static int32_t get_offset_of__busy_15() { return static_cast<int32_t>(offsetof(JContainer_t1023664833, ____busy_15)); }
	inline bool get__busy_15() const { return ____busy_15; }
	inline bool* get_address_of__busy_15() { return &____busy_15; }
	inline void set__busy_15(bool value)
	{
		____busy_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JCONTAINER_T1023664833_H
#ifndef JPROPERTYKEYEDCOLLECTION_T1521123921_H
#define JPROPERTYKEYEDCOLLECTION_T1521123921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JPropertyKeyedCollection
struct  JPropertyKeyedCollection_t1521123921  : public Collection_1_t4277862461
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JPropertyKeyedCollection::_dictionary
	Dictionary_2_t823795546 * ____dictionary_3;

public:
	inline static int32_t get_offset_of__dictionary_3() { return static_cast<int32_t>(offsetof(JPropertyKeyedCollection_t1521123921, ____dictionary_3)); }
	inline Dictionary_2_t823795546 * get__dictionary_3() const { return ____dictionary_3; }
	inline Dictionary_2_t823795546 ** get_address_of__dictionary_3() { return &____dictionary_3; }
	inline void set__dictionary_3(Dictionary_2_t823795546 * value)
	{
		____dictionary_3 = value;
		Il2CppCodeGenWriteBarrier((&____dictionary_3), value);
	}
};

struct JPropertyKeyedCollection_t1521123921_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<System.String> Newtonsoft.Json.Linq.JPropertyKeyedCollection::Comparer
	RuntimeObject* ___Comparer_2;

public:
	inline static int32_t get_offset_of_Comparer_2() { return static_cast<int32_t>(offsetof(JPropertyKeyedCollection_t1521123921_StaticFields, ___Comparer_2)); }
	inline RuntimeObject* get_Comparer_2() const { return ___Comparer_2; }
	inline RuntimeObject** get_address_of_Comparer_2() { return &___Comparer_2; }
	inline void set_Comparer_2(RuntimeObject* value)
	{
		___Comparer_2 = value;
		Il2CppCodeGenWriteBarrier((&___Comparer_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPROPERTYKEYEDCOLLECTION_T1521123921_H
#ifndef ONERRORATTRIBUTE_T1580718334_H
#define ONERRORATTRIBUTE_T1580718334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.OnErrorAttribute
struct  OnErrorAttribute_t1580718334  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONERRORATTRIBUTE_T1580718334_H
#ifndef JSONSERIALIZERINTERNALWRITER_T3588727337_H
#define JSONSERIALIZERINTERNALWRITER_T3588727337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalWriter
struct  JsonSerializerInternalWriter_t3588727337  : public JsonSerializerInternalBase_t286011480
{
public:
	// System.Type Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::_rootType
	Type_t * ____rootType_5;
	// System.Int32 Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::_rootLevel
	int32_t ____rootLevel_6;
	// System.Collections.Generic.List`1<System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::_serializeStack
	List_1_t257213610 * ____serializeStack_7;

public:
	inline static int32_t get_offset_of__rootType_5() { return static_cast<int32_t>(offsetof(JsonSerializerInternalWriter_t3588727337, ____rootType_5)); }
	inline Type_t * get__rootType_5() const { return ____rootType_5; }
	inline Type_t ** get_address_of__rootType_5() { return &____rootType_5; }
	inline void set__rootType_5(Type_t * value)
	{
		____rootType_5 = value;
		Il2CppCodeGenWriteBarrier((&____rootType_5), value);
	}

	inline static int32_t get_offset_of__rootLevel_6() { return static_cast<int32_t>(offsetof(JsonSerializerInternalWriter_t3588727337, ____rootLevel_6)); }
	inline int32_t get__rootLevel_6() const { return ____rootLevel_6; }
	inline int32_t* get_address_of__rootLevel_6() { return &____rootLevel_6; }
	inline void set__rootLevel_6(int32_t value)
	{
		____rootLevel_6 = value;
	}

	inline static int32_t get_offset_of__serializeStack_7() { return static_cast<int32_t>(offsetof(JsonSerializerInternalWriter_t3588727337, ____serializeStack_7)); }
	inline List_1_t257213610 * get__serializeStack_7() const { return ____serializeStack_7; }
	inline List_1_t257213610 ** get_address_of__serializeStack_7() { return &____serializeStack_7; }
	inline void set__serializeStack_7(List_1_t257213610 * value)
	{
		____serializeStack_7 = value;
		Il2CppCodeGenWriteBarrier((&____serializeStack_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZERINTERNALWRITER_T3588727337_H
#ifndef FIELDMULTIPLEFILTER_T3452751328_H
#define FIELDMULTIPLEFILTER_T3452751328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter
struct  FieldMultipleFilter_t3452751328  : public PathFilter_t3862968645
{
public:
	// System.Collections.Generic.List`1<System.String> Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter::<Names>k__BackingField
	List_1_t3319525431 * ___U3CNamesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNamesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FieldMultipleFilter_t3452751328, ___U3CNamesU3Ek__BackingField_0)); }
	inline List_1_t3319525431 * get_U3CNamesU3Ek__BackingField_0() const { return ___U3CNamesU3Ek__BackingField_0; }
	inline List_1_t3319525431 ** get_address_of_U3CNamesU3Ek__BackingField_0() { return &___U3CNamesU3Ek__BackingField_0; }
	inline void set_U3CNamesU3Ek__BackingField_0(List_1_t3319525431 * value)
	{
		___U3CNamesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNamesU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDMULTIPLEFILTER_T3452751328_H
#ifndef FIELDFILTER_T3654920868_H
#define FIELDFILTER_T3654920868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.FieldFilter
struct  FieldFilter_t3654920868  : public PathFilter_t3862968645
{
public:
	// System.String Newtonsoft.Json.Linq.JsonPath.FieldFilter::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FieldFilter_t3654920868, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDFILTER_T3654920868_H
#ifndef DATEFORMATHANDLING_T1376167855_H
#define DATEFORMATHANDLING_T1376167855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateFormatHandling
struct  DateFormatHandling_t1376167855 
{
public:
	// System.Int32 Newtonsoft.Json.DateFormatHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateFormatHandling_t1376167855, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEFORMATHANDLING_T1376167855_H
#ifndef FORMATTERASSEMBLYSTYLE_T868039825_H
#define FORMATTERASSEMBLYSTYLE_T868039825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
struct  FormatterAssemblyStyle_t868039825 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.FormatterAssemblyStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FormatterAssemblyStyle_t868039825, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTERASSEMBLYSTYLE_T868039825_H
#ifndef FORMATTING_T2192044929_H
#define FORMATTING_T2192044929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Formatting
struct  Formatting_t2192044929 
{
public:
	// System.Int32 Newtonsoft.Json.Formatting::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Formatting_t2192044929, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTING_T2192044929_H
#ifndef REFERENCELOOPHANDLING_T1329130968_H
#define REFERENCELOOPHANDLING_T1329130968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ReferenceLoopHandling
struct  ReferenceLoopHandling_t1329130968 
{
public:
	// System.Int32 Newtonsoft.Json.ReferenceLoopHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReferenceLoopHandling_t1329130968, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCELOOPHANDLING_T1329130968_H
#ifndef STATE_T2595666649_H
#define STATE_T2595666649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter/State
struct  State_t2595666649 
{
public:
	// System.Int32 Newtonsoft.Json.JsonWriter/State::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(State_t2595666649, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T2595666649_H
#ifndef STRINGESCAPEHANDLING_T4077875565_H
#define STRINGESCAPEHANDLING_T4077875565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.StringEscapeHandling
struct  StringEscapeHandling_t4077875565 
{
public:
	// System.Int32 Newtonsoft.Json.StringEscapeHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StringEscapeHandling_t4077875565, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGESCAPEHANDLING_T4077875565_H
#ifndef PROPERTYPRESENCE_T2490594612_H
#define PROPERTYPRESENCE_T2490594612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence
struct  PropertyPresence_t2490594612 
{
public:
	// System.Int32 Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PropertyPresence_t2490594612, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYPRESENCE_T2490594612_H
#ifndef JSONCONTRACTTYPE_T386662320_H
#define JSONCONTRACTTYPE_T386662320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonContractType
struct  JsonContractType_t386662320 
{
public:
	// System.Int32 Newtonsoft.Json.Serialization.JsonContractType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JsonContractType_t386662320, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTRACTTYPE_T386662320_H
#ifndef READTYPE_T340786580_H
#define READTYPE_T340786580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ReadType
struct  ReadType_t340786580 
{
public:
	// System.Int32 Newtonsoft.Json.ReadType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReadType_t340786580, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READTYPE_T340786580_H
#ifndef STREAMINGCONTEXTSTATES_T3580100459_H
#define STREAMINGCONTEXTSTATES_T3580100459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContextStates
struct  StreamingContextStates_t3580100459 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StreamingContextStates_t3580100459, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGCONTEXTSTATES_T3580100459_H
#ifndef TYPENAMEHANDLING_T3731987346_H
#define TYPENAMEHANDLING_T3731987346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.TypeNameHandling
struct  TypeNameHandling_t3731987346 
{
public:
	// System.Int32 Newtonsoft.Json.TypeNameHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TypeNameHandling_t3731987346, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPENAMEHANDLING_T3731987346_H
#ifndef FLOATFORMATHANDLING_T562907821_H
#define FLOATFORMATHANDLING_T562907821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.FloatFormatHandling
struct  FloatFormatHandling_t562907821 
{
public:
	// System.Int32 Newtonsoft.Json.FloatFormatHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FloatFormatHandling_t562907821, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATFORMATHANDLING_T562907821_H
#ifndef FLOATPARSEHANDLING_T1006965658_H
#define FLOATPARSEHANDLING_T1006965658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.FloatParseHandling
struct  FloatParseHandling_t1006965658 
{
public:
	// System.Int32 Newtonsoft.Json.FloatParseHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FloatParseHandling_t1006965658, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATPARSEHANDLING_T1006965658_H
#ifndef MISSINGMEMBERHANDLING_T4043300624_H
#define MISSINGMEMBERHANDLING_T4043300624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MissingMemberHandling
struct  MissingMemberHandling_t4043300624 
{
public:
	// System.Int32 Newtonsoft.Json.MissingMemberHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MissingMemberHandling_t4043300624, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSINGMEMBERHANDLING_T4043300624_H
#ifndef OBJECTCREATIONHANDLING_T3160794531_H
#define OBJECTCREATIONHANDLING_T3160794531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ObjectCreationHandling
struct  ObjectCreationHandling_t3160794531 
{
public:
	// System.Int32 Newtonsoft.Json.ObjectCreationHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ObjectCreationHandling_t3160794531, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCREATIONHANDLING_T3160794531_H
#ifndef NULLVALUEHANDLING_T3402136445_H
#define NULLVALUEHANDLING_T3402136445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.NullValueHandling
struct  NullValueHandling_t3402136445 
{
public:
	// System.Int32 Newtonsoft.Json.NullValueHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NullValueHandling_t3402136445, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLVALUEHANDLING_T3402136445_H
#ifndef DATETIMESTYLES_T840957420_H
#define DATETIMESTYLES_T840957420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.DateTimeStyles
struct  DateTimeStyles_t840957420 
{
public:
	// System.Int32 System.Globalization.DateTimeStyles::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeStyles_t840957420, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMESTYLES_T840957420_H
#ifndef DEFAULTVALUEHANDLING_T3240998650_H
#define DEFAULTVALUEHANDLING_T3240998650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DefaultValueHandling
struct  DefaultValueHandling_t3240998650 
{
public:
	// System.Int32 Newtonsoft.Json.DefaultValueHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DefaultValueHandling_t3240998650, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTVALUEHANDLING_T3240998650_H
#ifndef CONSTRUCTORHANDLING_T3189370124_H
#define CONSTRUCTORHANDLING_T3189370124_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ConstructorHandling
struct  ConstructorHandling_t3189370124 
{
public:
	// System.Int32 Newtonsoft.Json.ConstructorHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConstructorHandling_t3189370124, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRUCTORHANDLING_T3189370124_H
#ifndef DATEPARSEHANDLING_T2751701876_H
#define DATEPARSEHANDLING_T2751701876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateParseHandling
struct  DateParseHandling_t2751701876 
{
public:
	// System.Int32 Newtonsoft.Json.DateParseHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateParseHandling_t2751701876, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEPARSEHANDLING_T2751701876_H
#ifndef DATETIMEZONEHANDLING_T3002599730_H
#define DATETIMEZONEHANDLING_T3002599730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateTimeZoneHandling
struct  DateTimeZoneHandling_t3002599730 
{
public:
	// System.Int32 Newtonsoft.Json.DateTimeZoneHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeZoneHandling_t3002599730, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEZONEHANDLING_T3002599730_H
#ifndef STATE_T879723262_H
#define STATE_T879723262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonReader/State
struct  State_t879723262 
{
public:
	// System.Int32 Newtonsoft.Json.JsonReader/State::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(State_t879723262, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T879723262_H
#ifndef PRESERVEREFERENCESHANDLING_T815988812_H
#define PRESERVEREFERENCESHANDLING_T815988812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.PreserveReferencesHandling
struct  PreserveReferencesHandling_t815988812 
{
public:
	// System.Int32 Newtonsoft.Json.PreserveReferencesHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PreserveReferencesHandling_t815988812, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESERVEREFERENCESHANDLING_T815988812_H
#ifndef PRIMITIVETYPECODE_T798949904_H
#define PRIMITIVETYPECODE_T798949904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.PrimitiveTypeCode
struct  PrimitiveTypeCode_t798949904 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.PrimitiveTypeCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PrimitiveTypeCode_t798949904, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMITIVETYPECODE_T798949904_H
#ifndef METADATAPROPERTYHANDLING_T1042169553_H
#define METADATAPROPERTYHANDLING_T1042169553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MetadataPropertyHandling
struct  MetadataPropertyHandling_t1042169553 
{
public:
	// System.Int32 Newtonsoft.Json.MetadataPropertyHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MetadataPropertyHandling_t1042169553, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METADATAPROPERTYHANDLING_T1042169553_H
#ifndef JSONTOKEN_T1917433489_H
#define JSONTOKEN_T1917433489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonToken
struct  JsonToken_t1917433489 
{
public:
	// System.Int32 Newtonsoft.Json.JsonToken::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JsonToken_t1917433489, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTOKEN_T1917433489_H
#ifndef U3CEXECUTEFILTERU3ED__4_T32591816_H
#define U3CEXECUTEFILTERU3ED__4_T32591816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter/<ExecuteFilter>d__4
struct  U3CExecuteFilterU3Ed__4_t32591816  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter/<ExecuteFilter>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter/<ExecuteFilter>d__4::<>2__current
	JToken_t1038539247 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter/<ExecuteFilter>d__4::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter/<ExecuteFilter>d__4::current
	RuntimeObject* ___current_3;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter/<ExecuteFilter>d__4::<>3__current
	RuntimeObject* ___U3CU3E3__current_4;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter/<ExecuteFilter>d__4::errorWhenNoMatch
	bool ___errorWhenNoMatch_5;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter/<ExecuteFilter>d__4::<>3__errorWhenNoMatch
	bool ___U3CU3E3__errorWhenNoMatch_6;
	// Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter/<ExecuteFilter>d__4::<>4__this
	FieldMultipleFilter_t3452751328 * ___U3CU3E4__this_7;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter/<ExecuteFilter>d__4::<>s__1
	RuntimeObject* ___U3CU3Es__1_8;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter/<ExecuteFilter>d__4::<t>5__2
	JToken_t1038539247 * ___U3CtU3E5__2_9;
	// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter/<ExecuteFilter>d__4::<o>5__3
	JObject_t2059125928 * ___U3CoU3E5__3_10;
	// System.Collections.Generic.List`1/Enumerator<System.String> Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter/<ExecuteFilter>d__4::<>s__4
	Enumerator_t913802012  ___U3CU3Es__4_11;
	// System.String Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter/<ExecuteFilter>d__4::<name>5__5
	String_t* ___U3CnameU3E5__5_12;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.FieldMultipleFilter/<ExecuteFilter>d__4::<v>5__6
	JToken_t1038539247 * ___U3CvU3E5__6_13;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t32591816, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t32591816, ___U3CU3E2__current_1)); }
	inline JToken_t1038539247 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_t1038539247 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_t1038539247 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t32591816, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t32591816, ___current_3)); }
	inline RuntimeObject* get_current_3() const { return ___current_3; }
	inline RuntimeObject** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__current_4() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t32591816, ___U3CU3E3__current_4)); }
	inline RuntimeObject* get_U3CU3E3__current_4() const { return ___U3CU3E3__current_4; }
	inline RuntimeObject** get_address_of_U3CU3E3__current_4() { return &___U3CU3E3__current_4; }
	inline void set_U3CU3E3__current_4(RuntimeObject* value)
	{
		___U3CU3E3__current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__current_4), value);
	}

	inline static int32_t get_offset_of_errorWhenNoMatch_5() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t32591816, ___errorWhenNoMatch_5)); }
	inline bool get_errorWhenNoMatch_5() const { return ___errorWhenNoMatch_5; }
	inline bool* get_address_of_errorWhenNoMatch_5() { return &___errorWhenNoMatch_5; }
	inline void set_errorWhenNoMatch_5(bool value)
	{
		___errorWhenNoMatch_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__errorWhenNoMatch_6() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t32591816, ___U3CU3E3__errorWhenNoMatch_6)); }
	inline bool get_U3CU3E3__errorWhenNoMatch_6() const { return ___U3CU3E3__errorWhenNoMatch_6; }
	inline bool* get_address_of_U3CU3E3__errorWhenNoMatch_6() { return &___U3CU3E3__errorWhenNoMatch_6; }
	inline void set_U3CU3E3__errorWhenNoMatch_6(bool value)
	{
		___U3CU3E3__errorWhenNoMatch_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_7() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t32591816, ___U3CU3E4__this_7)); }
	inline FieldMultipleFilter_t3452751328 * get_U3CU3E4__this_7() const { return ___U3CU3E4__this_7; }
	inline FieldMultipleFilter_t3452751328 ** get_address_of_U3CU3E4__this_7() { return &___U3CU3E4__this_7; }
	inline void set_U3CU3E4__this_7(FieldMultipleFilter_t3452751328 * value)
	{
		___U3CU3E4__this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Es__1_8() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t32591816, ___U3CU3Es__1_8)); }
	inline RuntimeObject* get_U3CU3Es__1_8() const { return ___U3CU3Es__1_8; }
	inline RuntimeObject** get_address_of_U3CU3Es__1_8() { return &___U3CU3Es__1_8; }
	inline void set_U3CU3Es__1_8(RuntimeObject* value)
	{
		___U3CU3Es__1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Es__1_8), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__2_9() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t32591816, ___U3CtU3E5__2_9)); }
	inline JToken_t1038539247 * get_U3CtU3E5__2_9() const { return ___U3CtU3E5__2_9; }
	inline JToken_t1038539247 ** get_address_of_U3CtU3E5__2_9() { return &___U3CtU3E5__2_9; }
	inline void set_U3CtU3E5__2_9(JToken_t1038539247 * value)
	{
		___U3CtU3E5__2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtU3E5__2_9), value);
	}

	inline static int32_t get_offset_of_U3CoU3E5__3_10() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t32591816, ___U3CoU3E5__3_10)); }
	inline JObject_t2059125928 * get_U3CoU3E5__3_10() const { return ___U3CoU3E5__3_10; }
	inline JObject_t2059125928 ** get_address_of_U3CoU3E5__3_10() { return &___U3CoU3E5__3_10; }
	inline void set_U3CoU3E5__3_10(JObject_t2059125928 * value)
	{
		___U3CoU3E5__3_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoU3E5__3_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Es__4_11() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t32591816, ___U3CU3Es__4_11)); }
	inline Enumerator_t913802012  get_U3CU3Es__4_11() const { return ___U3CU3Es__4_11; }
	inline Enumerator_t913802012 * get_address_of_U3CU3Es__4_11() { return &___U3CU3Es__4_11; }
	inline void set_U3CU3Es__4_11(Enumerator_t913802012  value)
	{
		___U3CU3Es__4_11 = value;
	}

	inline static int32_t get_offset_of_U3CnameU3E5__5_12() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t32591816, ___U3CnameU3E5__5_12)); }
	inline String_t* get_U3CnameU3E5__5_12() const { return ___U3CnameU3E5__5_12; }
	inline String_t** get_address_of_U3CnameU3E5__5_12() { return &___U3CnameU3E5__5_12; }
	inline void set_U3CnameU3E5__5_12(String_t* value)
	{
		___U3CnameU3E5__5_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3E5__5_12), value);
	}

	inline static int32_t get_offset_of_U3CvU3E5__6_13() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t32591816, ___U3CvU3E5__6_13)); }
	inline JToken_t1038539247 * get_U3CvU3E5__6_13() const { return ___U3CvU3E5__6_13; }
	inline JToken_t1038539247 ** get_address_of_U3CvU3E5__6_13() { return &___U3CvU3E5__6_13; }
	inline void set_U3CvU3E5__6_13(JToken_t1038539247 * value)
	{
		___U3CvU3E5__6_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvU3E5__6_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEFILTERU3ED__4_T32591816_H
#ifndef U3CEXECUTEFILTERU3ED__4_T2686066276_H
#define U3CEXECUTEFILTERU3ED__4_T2686066276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.FieldFilter/<ExecuteFilter>d__4
struct  U3CExecuteFilterU3Ed__4_t2686066276  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.FieldFilter/<ExecuteFilter>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.FieldFilter/<ExecuteFilter>d__4::<>2__current
	JToken_t1038539247 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.FieldFilter/<ExecuteFilter>d__4::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.FieldFilter/<ExecuteFilter>d__4::current
	RuntimeObject* ___current_3;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.FieldFilter/<ExecuteFilter>d__4::<>3__current
	RuntimeObject* ___U3CU3E3__current_4;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.FieldFilter/<ExecuteFilter>d__4::errorWhenNoMatch
	bool ___errorWhenNoMatch_5;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.FieldFilter/<ExecuteFilter>d__4::<>3__errorWhenNoMatch
	bool ___U3CU3E3__errorWhenNoMatch_6;
	// Newtonsoft.Json.Linq.JsonPath.FieldFilter Newtonsoft.Json.Linq.JsonPath.FieldFilter/<ExecuteFilter>d__4::<>4__this
	FieldFilter_t3654920868 * ___U3CU3E4__this_7;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.FieldFilter/<ExecuteFilter>d__4::<>s__1
	RuntimeObject* ___U3CU3Es__1_8;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.FieldFilter/<ExecuteFilter>d__4::<t>5__2
	JToken_t1038539247 * ___U3CtU3E5__2_9;
	// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JsonPath.FieldFilter/<ExecuteFilter>d__4::<o>5__3
	JObject_t2059125928 * ___U3CoU3E5__3_10;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.FieldFilter/<ExecuteFilter>d__4::<v>5__4
	JToken_t1038539247 * ___U3CvU3E5__4_11;
	// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>> Newtonsoft.Json.Linq.JsonPath.FieldFilter/<ExecuteFilter>d__4::<>s__5
	RuntimeObject* ___U3CU3Es__5_12;
	// System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.FieldFilter/<ExecuteFilter>d__4::<p>5__6
	KeyValuePair_2_t3221467713  ___U3CpU3E5__6_13;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2686066276, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2686066276, ___U3CU3E2__current_1)); }
	inline JToken_t1038539247 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_t1038539247 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_t1038539247 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2686066276, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2686066276, ___current_3)); }
	inline RuntimeObject* get_current_3() const { return ___current_3; }
	inline RuntimeObject** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__current_4() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2686066276, ___U3CU3E3__current_4)); }
	inline RuntimeObject* get_U3CU3E3__current_4() const { return ___U3CU3E3__current_4; }
	inline RuntimeObject** get_address_of_U3CU3E3__current_4() { return &___U3CU3E3__current_4; }
	inline void set_U3CU3E3__current_4(RuntimeObject* value)
	{
		___U3CU3E3__current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__current_4), value);
	}

	inline static int32_t get_offset_of_errorWhenNoMatch_5() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2686066276, ___errorWhenNoMatch_5)); }
	inline bool get_errorWhenNoMatch_5() const { return ___errorWhenNoMatch_5; }
	inline bool* get_address_of_errorWhenNoMatch_5() { return &___errorWhenNoMatch_5; }
	inline void set_errorWhenNoMatch_5(bool value)
	{
		___errorWhenNoMatch_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__errorWhenNoMatch_6() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2686066276, ___U3CU3E3__errorWhenNoMatch_6)); }
	inline bool get_U3CU3E3__errorWhenNoMatch_6() const { return ___U3CU3E3__errorWhenNoMatch_6; }
	inline bool* get_address_of_U3CU3E3__errorWhenNoMatch_6() { return &___U3CU3E3__errorWhenNoMatch_6; }
	inline void set_U3CU3E3__errorWhenNoMatch_6(bool value)
	{
		___U3CU3E3__errorWhenNoMatch_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_7() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2686066276, ___U3CU3E4__this_7)); }
	inline FieldFilter_t3654920868 * get_U3CU3E4__this_7() const { return ___U3CU3E4__this_7; }
	inline FieldFilter_t3654920868 ** get_address_of_U3CU3E4__this_7() { return &___U3CU3E4__this_7; }
	inline void set_U3CU3E4__this_7(FieldFilter_t3654920868 * value)
	{
		___U3CU3E4__this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Es__1_8() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2686066276, ___U3CU3Es__1_8)); }
	inline RuntimeObject* get_U3CU3Es__1_8() const { return ___U3CU3Es__1_8; }
	inline RuntimeObject** get_address_of_U3CU3Es__1_8() { return &___U3CU3Es__1_8; }
	inline void set_U3CU3Es__1_8(RuntimeObject* value)
	{
		___U3CU3Es__1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Es__1_8), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__2_9() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2686066276, ___U3CtU3E5__2_9)); }
	inline JToken_t1038539247 * get_U3CtU3E5__2_9() const { return ___U3CtU3E5__2_9; }
	inline JToken_t1038539247 ** get_address_of_U3CtU3E5__2_9() { return &___U3CtU3E5__2_9; }
	inline void set_U3CtU3E5__2_9(JToken_t1038539247 * value)
	{
		___U3CtU3E5__2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtU3E5__2_9), value);
	}

	inline static int32_t get_offset_of_U3CoU3E5__3_10() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2686066276, ___U3CoU3E5__3_10)); }
	inline JObject_t2059125928 * get_U3CoU3E5__3_10() const { return ___U3CoU3E5__3_10; }
	inline JObject_t2059125928 ** get_address_of_U3CoU3E5__3_10() { return &___U3CoU3E5__3_10; }
	inline void set_U3CoU3E5__3_10(JObject_t2059125928 * value)
	{
		___U3CoU3E5__3_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoU3E5__3_10), value);
	}

	inline static int32_t get_offset_of_U3CvU3E5__4_11() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2686066276, ___U3CvU3E5__4_11)); }
	inline JToken_t1038539247 * get_U3CvU3E5__4_11() const { return ___U3CvU3E5__4_11; }
	inline JToken_t1038539247 ** get_address_of_U3CvU3E5__4_11() { return &___U3CvU3E5__4_11; }
	inline void set_U3CvU3E5__4_11(JToken_t1038539247 * value)
	{
		___U3CvU3E5__4_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvU3E5__4_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Es__5_12() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2686066276, ___U3CU3Es__5_12)); }
	inline RuntimeObject* get_U3CU3Es__5_12() const { return ___U3CU3Es__5_12; }
	inline RuntimeObject** get_address_of_U3CU3Es__5_12() { return &___U3CU3Es__5_12; }
	inline void set_U3CU3Es__5_12(RuntimeObject* value)
	{
		___U3CU3Es__5_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Es__5_12), value);
	}

	inline static int32_t get_offset_of_U3CpU3E5__6_13() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t2686066276, ___U3CpU3E5__6_13)); }
	inline KeyValuePair_2_t3221467713  get_U3CpU3E5__6_13() const { return ___U3CpU3E5__6_13; }
	inline KeyValuePair_2_t3221467713 * get_address_of_U3CpU3E5__6_13() { return &___U3CpU3E5__6_13; }
	inline void set_U3CpU3E5__6_13(KeyValuePair_2_t3221467713  value)
	{
		___U3CpU3E5__6_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEFILTERU3ED__4_T2686066276_H
#ifndef ARRAYSLICEFILTER_T662461599_H
#define ARRAYSLICEFILTER_T662461599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter
struct  ArraySliceFilter_t662461599  : public PathFilter_t3862968645
{
public:
	// System.Nullable`1<System.Int32> Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter::<Start>k__BackingField
	Nullable_1_t378540539  ___U3CStartU3Ek__BackingField_0;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter::<End>k__BackingField
	Nullable_1_t378540539  ___U3CEndU3Ek__BackingField_1;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.Linq.JsonPath.ArraySliceFilter::<Step>k__BackingField
	Nullable_1_t378540539  ___U3CStepU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CStartU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ArraySliceFilter_t662461599, ___U3CStartU3Ek__BackingField_0)); }
	inline Nullable_1_t378540539  get_U3CStartU3Ek__BackingField_0() const { return ___U3CStartU3Ek__BackingField_0; }
	inline Nullable_1_t378540539 * get_address_of_U3CStartU3Ek__BackingField_0() { return &___U3CStartU3Ek__BackingField_0; }
	inline void set_U3CStartU3Ek__BackingField_0(Nullable_1_t378540539  value)
	{
		___U3CStartU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CEndU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ArraySliceFilter_t662461599, ___U3CEndU3Ek__BackingField_1)); }
	inline Nullable_1_t378540539  get_U3CEndU3Ek__BackingField_1() const { return ___U3CEndU3Ek__BackingField_1; }
	inline Nullable_1_t378540539 * get_address_of_U3CEndU3Ek__BackingField_1() { return &___U3CEndU3Ek__BackingField_1; }
	inline void set_U3CEndU3Ek__BackingField_1(Nullable_1_t378540539  value)
	{
		___U3CEndU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CStepU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ArraySliceFilter_t662461599, ___U3CStepU3Ek__BackingField_2)); }
	inline Nullable_1_t378540539  get_U3CStepU3Ek__BackingField_2() const { return ___U3CStepU3Ek__BackingField_2; }
	inline Nullable_1_t378540539 * get_address_of_U3CStepU3Ek__BackingField_2() { return &___U3CStepU3Ek__BackingField_2; }
	inline void set_U3CStepU3Ek__BackingField_2(Nullable_1_t378540539  value)
	{
		___U3CStepU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYSLICEFILTER_T662461599_H
#ifndef U3CEXECUTEFILTERU3ED__4_T1113823742_H
#define U3CEXECUTEFILTERU3ED__4_T1113823742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter/<ExecuteFilter>d__4
struct  U3CExecuteFilterU3Ed__4_t1113823742  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter/<ExecuteFilter>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter/<ExecuteFilter>d__4::<>2__current
	JToken_t1038539247 * ___U3CU3E2__current_1;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter/<ExecuteFilter>d__4::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter/<ExecuteFilter>d__4::current
	RuntimeObject* ___current_3;
	// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter/<ExecuteFilter>d__4::<>3__current
	RuntimeObject* ___U3CU3E3__current_4;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter/<ExecuteFilter>d__4::errorWhenNoMatch
	bool ___errorWhenNoMatch_5;
	// System.Boolean Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter/<ExecuteFilter>d__4::<>3__errorWhenNoMatch
	bool ___U3CU3E3__errorWhenNoMatch_6;
	// Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter/<ExecuteFilter>d__4::<>4__this
	ArrayMultipleIndexFilter_t2913121614 * ___U3CU3E4__this_7;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter/<ExecuteFilter>d__4::<>s__1
	RuntimeObject* ___U3CU3Es__1_8;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter/<ExecuteFilter>d__4::<t>5__2
	JToken_t1038539247 * ___U3CtU3E5__2_9;
	// System.Collections.Generic.List`1/Enumerator<System.Int32> Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter/<ExecuteFilter>d__4::<>s__3
	Enumerator_t2017297076  ___U3CU3Es__3_10;
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter/<ExecuteFilter>d__4::<i>5__4
	int32_t ___U3CiU3E5__4_11;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JsonPath.ArrayMultipleIndexFilter/<ExecuteFilter>d__4::<v>5__5
	JToken_t1038539247 * ___U3CvU3E5__5_12;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1113823742, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1113823742, ___U3CU3E2__current_1)); }
	inline JToken_t1038539247 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_t1038539247 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_t1038539247 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1113823742, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1113823742, ___current_3)); }
	inline RuntimeObject* get_current_3() const { return ___current_3; }
	inline RuntimeObject** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__current_4() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1113823742, ___U3CU3E3__current_4)); }
	inline RuntimeObject* get_U3CU3E3__current_4() const { return ___U3CU3E3__current_4; }
	inline RuntimeObject** get_address_of_U3CU3E3__current_4() { return &___U3CU3E3__current_4; }
	inline void set_U3CU3E3__current_4(RuntimeObject* value)
	{
		___U3CU3E3__current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__current_4), value);
	}

	inline static int32_t get_offset_of_errorWhenNoMatch_5() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1113823742, ___errorWhenNoMatch_5)); }
	inline bool get_errorWhenNoMatch_5() const { return ___errorWhenNoMatch_5; }
	inline bool* get_address_of_errorWhenNoMatch_5() { return &___errorWhenNoMatch_5; }
	inline void set_errorWhenNoMatch_5(bool value)
	{
		___errorWhenNoMatch_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__errorWhenNoMatch_6() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1113823742, ___U3CU3E3__errorWhenNoMatch_6)); }
	inline bool get_U3CU3E3__errorWhenNoMatch_6() const { return ___U3CU3E3__errorWhenNoMatch_6; }
	inline bool* get_address_of_U3CU3E3__errorWhenNoMatch_6() { return &___U3CU3E3__errorWhenNoMatch_6; }
	inline void set_U3CU3E3__errorWhenNoMatch_6(bool value)
	{
		___U3CU3E3__errorWhenNoMatch_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_7() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1113823742, ___U3CU3E4__this_7)); }
	inline ArrayMultipleIndexFilter_t2913121614 * get_U3CU3E4__this_7() const { return ___U3CU3E4__this_7; }
	inline ArrayMultipleIndexFilter_t2913121614 ** get_address_of_U3CU3E4__this_7() { return &___U3CU3E4__this_7; }
	inline void set_U3CU3E4__this_7(ArrayMultipleIndexFilter_t2913121614 * value)
	{
		___U3CU3E4__this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Es__1_8() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1113823742, ___U3CU3Es__1_8)); }
	inline RuntimeObject* get_U3CU3Es__1_8() const { return ___U3CU3Es__1_8; }
	inline RuntimeObject** get_address_of_U3CU3Es__1_8() { return &___U3CU3Es__1_8; }
	inline void set_U3CU3Es__1_8(RuntimeObject* value)
	{
		___U3CU3Es__1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Es__1_8), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__2_9() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1113823742, ___U3CtU3E5__2_9)); }
	inline JToken_t1038539247 * get_U3CtU3E5__2_9() const { return ___U3CtU3E5__2_9; }
	inline JToken_t1038539247 ** get_address_of_U3CtU3E5__2_9() { return &___U3CtU3E5__2_9; }
	inline void set_U3CtU3E5__2_9(JToken_t1038539247 * value)
	{
		___U3CtU3E5__2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtU3E5__2_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Es__3_10() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1113823742, ___U3CU3Es__3_10)); }
	inline Enumerator_t2017297076  get_U3CU3Es__3_10() const { return ___U3CU3Es__3_10; }
	inline Enumerator_t2017297076 * get_address_of_U3CU3Es__3_10() { return &___U3CU3Es__3_10; }
	inline void set_U3CU3Es__3_10(Enumerator_t2017297076  value)
	{
		___U3CU3Es__3_10 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__4_11() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1113823742, ___U3CiU3E5__4_11)); }
	inline int32_t get_U3CiU3E5__4_11() const { return ___U3CiU3E5__4_11; }
	inline int32_t* get_address_of_U3CiU3E5__4_11() { return &___U3CiU3E5__4_11; }
	inline void set_U3CiU3E5__4_11(int32_t value)
	{
		___U3CiU3E5__4_11 = value;
	}

	inline static int32_t get_offset_of_U3CvU3E5__5_12() { return static_cast<int32_t>(offsetof(U3CExecuteFilterU3Ed__4_t1113823742, ___U3CvU3E5__5_12)); }
	inline JToken_t1038539247 * get_U3CvU3E5__5_12() const { return ___U3CvU3E5__5_12; }
	inline JToken_t1038539247 ** get_address_of_U3CvU3E5__5_12() { return &___U3CvU3E5__5_12; }
	inline void set_U3CvU3E5__5_12(JToken_t1038539247 * value)
	{
		___U3CvU3E5__5_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvU3E5__5_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEFILTERU3ED__4_T1113823742_H
#ifndef ARRAYINDEXFILTER_T2739218971_H
#define ARRAYINDEXFILTER_T2739218971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter
struct  ArrayIndexFilter_t2739218971  : public PathFilter_t3862968645
{
public:
	// System.Nullable`1<System.Int32> Newtonsoft.Json.Linq.JsonPath.ArrayIndexFilter::<Index>k__BackingField
	Nullable_1_t378540539  ___U3CIndexU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CIndexU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ArrayIndexFilter_t2739218971, ___U3CIndexU3Ek__BackingField_0)); }
	inline Nullable_1_t378540539  get_U3CIndexU3Ek__BackingField_0() const { return ___U3CIndexU3Ek__BackingField_0; }
	inline Nullable_1_t378540539 * get_address_of_U3CIndexU3Ek__BackingField_0() { return &___U3CIndexU3Ek__BackingField_0; }
	inline void set_U3CIndexU3Ek__BackingField_0(Nullable_1_t378540539  value)
	{
		___U3CIndexU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYINDEXFILTER_T2739218971_H
#ifndef JOBJECT_T2059125928_H
#define JOBJECT_T2059125928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JObject
struct  JObject_t2059125928  : public JContainer_t1023664833
{
public:
	// Newtonsoft.Json.Linq.JPropertyKeyedCollection Newtonsoft.Json.Linq.JObject::_properties
	JPropertyKeyedCollection_t1521123921 * ____properties_16;
	// System.ComponentModel.PropertyChangedEventHandler Newtonsoft.Json.Linq.JObject::PropertyChanged
	PropertyChangedEventHandler_t3836340606 * ___PropertyChanged_17;
	// System.ComponentModel.PropertyChangingEventHandler Newtonsoft.Json.Linq.JObject::PropertyChanging
	PropertyChangingEventHandler_t2830353497 * ___PropertyChanging_18;

public:
	inline static int32_t get_offset_of__properties_16() { return static_cast<int32_t>(offsetof(JObject_t2059125928, ____properties_16)); }
	inline JPropertyKeyedCollection_t1521123921 * get__properties_16() const { return ____properties_16; }
	inline JPropertyKeyedCollection_t1521123921 ** get_address_of__properties_16() { return &____properties_16; }
	inline void set__properties_16(JPropertyKeyedCollection_t1521123921 * value)
	{
		____properties_16 = value;
		Il2CppCodeGenWriteBarrier((&____properties_16), value);
	}

	inline static int32_t get_offset_of_PropertyChanged_17() { return static_cast<int32_t>(offsetof(JObject_t2059125928, ___PropertyChanged_17)); }
	inline PropertyChangedEventHandler_t3836340606 * get_PropertyChanged_17() const { return ___PropertyChanged_17; }
	inline PropertyChangedEventHandler_t3836340606 ** get_address_of_PropertyChanged_17() { return &___PropertyChanged_17; }
	inline void set_PropertyChanged_17(PropertyChangedEventHandler_t3836340606 * value)
	{
		___PropertyChanged_17 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyChanged_17), value);
	}

	inline static int32_t get_offset_of_PropertyChanging_18() { return static_cast<int32_t>(offsetof(JObject_t2059125928, ___PropertyChanging_18)); }
	inline PropertyChangingEventHandler_t2830353497 * get_PropertyChanging_18() const { return ___PropertyChanging_18; }
	inline PropertyChangingEventHandler_t2830353497 ** get_address_of_PropertyChanging_18() { return &___PropertyChanging_18; }
	inline void set_PropertyChanging_18(PropertyChangingEventHandler_t2830353497 * value)
	{
		___PropertyChanging_18 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyChanging_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOBJECT_T2059125928_H
#ifndef JTOKENTYPE_T3022361662_H
#define JTOKENTYPE_T3022361662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JTokenType
struct  JTokenType_t3022361662 
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JTokenType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JTokenType_t3022361662, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKENTYPE_T3022361662_H
#ifndef JPROPERTY_T3803048347_H
#define JPROPERTY_T3803048347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JProperty
struct  JProperty_t3803048347  : public JContainer_t1023664833
{
public:
	// Newtonsoft.Json.Linq.JProperty/JPropertyList Newtonsoft.Json.Linq.JProperty::_content
	JPropertyList_t1362261004 * ____content_16;
	// System.String Newtonsoft.Json.Linq.JProperty::_name
	String_t* ____name_17;

public:
	inline static int32_t get_offset_of__content_16() { return static_cast<int32_t>(offsetof(JProperty_t3803048347, ____content_16)); }
	inline JPropertyList_t1362261004 * get__content_16() const { return ____content_16; }
	inline JPropertyList_t1362261004 ** get_address_of__content_16() { return &____content_16; }
	inline void set__content_16(JPropertyList_t1362261004 * value)
	{
		____content_16 = value;
		Il2CppCodeGenWriteBarrier((&____content_16), value);
	}

	inline static int32_t get_offset_of__name_17() { return static_cast<int32_t>(offsetof(JProperty_t3803048347, ____name_17)); }
	inline String_t* get__name_17() const { return ____name_17; }
	inline String_t** get_address_of__name_17() { return &____name_17; }
	inline void set__name_17(String_t* value)
	{
		____name_17 = value;
		Il2CppCodeGenWriteBarrier((&____name_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPROPERTY_T3803048347_H
#ifndef U3CGETENUMERATORU3ED__58_T2686635841_H
#define U3CGETENUMERATORU3ED__58_T2686635841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JObject/<GetEnumerator>d__58
struct  U3CGetEnumeratorU3Ed__58_t2686635841  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JObject/<GetEnumerator>d__58::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject/<GetEnumerator>d__58::<>2__current
	KeyValuePair_2_t3221467713  ___U3CU3E2__current_1;
	// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JObject/<GetEnumerator>d__58::<>4__this
	JObject_t2059125928 * ___U3CU3E4__this_2;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject/<GetEnumerator>d__58::<>s__1
	RuntimeObject* ___U3CU3Es__1_3;
	// Newtonsoft.Json.Linq.JProperty Newtonsoft.Json.Linq.JObject/<GetEnumerator>d__58::<property>5__2
	JProperty_t3803048347 * ___U3CpropertyU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__58_t2686635841, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__58_t2686635841, ___U3CU3E2__current_1)); }
	inline KeyValuePair_2_t3221467713  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline KeyValuePair_2_t3221467713 * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(KeyValuePair_2_t3221467713  value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__58_t2686635841, ___U3CU3E4__this_2)); }
	inline JObject_t2059125928 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline JObject_t2059125928 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(JObject_t2059125928 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Es__1_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__58_t2686635841, ___U3CU3Es__1_3)); }
	inline RuntimeObject* get_U3CU3Es__1_3() const { return ___U3CU3Es__1_3; }
	inline RuntimeObject** get_address_of_U3CU3Es__1_3() { return &___U3CU3Es__1_3; }
	inline void set_U3CU3Es__1_3(RuntimeObject* value)
	{
		___U3CU3Es__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Es__1_3), value);
	}

	inline static int32_t get_offset_of_U3CpropertyU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__58_t2686635841, ___U3CpropertyU3E5__2_4)); }
	inline JProperty_t3803048347 * get_U3CpropertyU3E5__2_4() const { return ___U3CpropertyU3E5__2_4; }
	inline JProperty_t3803048347 ** get_address_of_U3CpropertyU3E5__2_4() { return &___U3CpropertyU3E5__2_4; }
	inline void set_U3CpropertyU3E5__2_4(JProperty_t3803048347 * value)
	{
		___U3CpropertyU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpropertyU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__58_T2686635841_H
#ifndef JARRAY_T2963978544_H
#define JARRAY_T2963978544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JArray
struct  JArray_t2963978544  : public JContainer_t1023664833
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JArray::_values
	List_1_t2510613989 * ____values_16;

public:
	inline static int32_t get_offset_of__values_16() { return static_cast<int32_t>(offsetof(JArray_t2963978544, ____values_16)); }
	inline List_1_t2510613989 * get__values_16() const { return ____values_16; }
	inline List_1_t2510613989 ** get_address_of__values_16() { return &____values_16; }
	inline void set__values_16(List_1_t2510613989 * value)
	{
		____values_16 = value;
		Il2CppCodeGenWriteBarrier((&____values_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JARRAY_T2963978544_H
#ifndef QUERYOPERATOR_T2966169422_H
#define QUERYOPERATOR_T2966169422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.QueryOperator
struct  QueryOperator_t2966169422 
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JsonPath.QueryOperator::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(QueryOperator_t2966169422, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYOPERATOR_T2966169422_H
#ifndef JSONTYPEREFLECTOR_T526591219_H
#define JSONTYPEREFLECTOR_T526591219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonTypeReflector
struct  JsonTypeReflector_t526591219  : public RuntimeObject
{
public:

public:
};

struct JsonTypeReflector_t526591219_StaticFields
{
public:
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonTypeReflector::_fullyTrusted
	Nullable_1_t1819850047  ____fullyTrusted_0;
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Func`2<System.Object[],Newtonsoft.Json.JsonConverter>> Newtonsoft.Json.Serialization.JsonTypeReflector::JsonConverterCreatorCache
	ThreadSafeStore_2_t1780285614 * ___JsonConverterCreatorCache_1;
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Type> Newtonsoft.Json.Serialization.JsonTypeReflector::AssociatedMetadataTypesCache
	ThreadSafeStore_2_t1567256624 * ___AssociatedMetadataTypesCache_2;
	// Newtonsoft.Json.Utilities.ReflectionObject Newtonsoft.Json.Serialization.JsonTypeReflector::_metadataTypeAttributeReflectionObject
	ReflectionObject_t701100009 * ____metadataTypeAttributeReflectionObject_3;

public:
	inline static int32_t get_offset_of__fullyTrusted_0() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t526591219_StaticFields, ____fullyTrusted_0)); }
	inline Nullable_1_t1819850047  get__fullyTrusted_0() const { return ____fullyTrusted_0; }
	inline Nullable_1_t1819850047 * get_address_of__fullyTrusted_0() { return &____fullyTrusted_0; }
	inline void set__fullyTrusted_0(Nullable_1_t1819850047  value)
	{
		____fullyTrusted_0 = value;
	}

	inline static int32_t get_offset_of_JsonConverterCreatorCache_1() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t526591219_StaticFields, ___JsonConverterCreatorCache_1)); }
	inline ThreadSafeStore_2_t1780285614 * get_JsonConverterCreatorCache_1() const { return ___JsonConverterCreatorCache_1; }
	inline ThreadSafeStore_2_t1780285614 ** get_address_of_JsonConverterCreatorCache_1() { return &___JsonConverterCreatorCache_1; }
	inline void set_JsonConverterCreatorCache_1(ThreadSafeStore_2_t1780285614 * value)
	{
		___JsonConverterCreatorCache_1 = value;
		Il2CppCodeGenWriteBarrier((&___JsonConverterCreatorCache_1), value);
	}

	inline static int32_t get_offset_of_AssociatedMetadataTypesCache_2() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t526591219_StaticFields, ___AssociatedMetadataTypesCache_2)); }
	inline ThreadSafeStore_2_t1567256624 * get_AssociatedMetadataTypesCache_2() const { return ___AssociatedMetadataTypesCache_2; }
	inline ThreadSafeStore_2_t1567256624 ** get_address_of_AssociatedMetadataTypesCache_2() { return &___AssociatedMetadataTypesCache_2; }
	inline void set_AssociatedMetadataTypesCache_2(ThreadSafeStore_2_t1567256624 * value)
	{
		___AssociatedMetadataTypesCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___AssociatedMetadataTypesCache_2), value);
	}

	inline static int32_t get_offset_of__metadataTypeAttributeReflectionObject_3() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t526591219_StaticFields, ____metadataTypeAttributeReflectionObject_3)); }
	inline ReflectionObject_t701100009 * get__metadataTypeAttributeReflectionObject_3() const { return ____metadataTypeAttributeReflectionObject_3; }
	inline ReflectionObject_t701100009 ** get_address_of__metadataTypeAttributeReflectionObject_3() { return &____metadataTypeAttributeReflectionObject_3; }
	inline void set__metadataTypeAttributeReflectionObject_3(ReflectionObject_t701100009 * value)
	{
		____metadataTypeAttributeReflectionObject_3 = value;
		Il2CppCodeGenWriteBarrier((&____metadataTypeAttributeReflectionObject_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTYPEREFLECTOR_T526591219_H
#ifndef COMMENTHANDLING_T2931237307_H
#define COMMENTHANDLING_T2931237307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.CommentHandling
struct  CommentHandling_t2931237307 
{
public:
	// System.Int32 Newtonsoft.Json.Linq.CommentHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CommentHandling_t2931237307, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMENTHANDLING_T2931237307_H
#ifndef LINEINFOHANDLING_T588682358_H
#define LINEINFOHANDLING_T588682358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.LineInfoHandling
struct  LineInfoHandling_t588682358 
{
public:
	// System.Int32 Newtonsoft.Json.Linq.LineInfoHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LineInfoHandling_t588682358, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEINFOHANDLING_T588682358_H
#ifndef JPROPERTYDESCRIPTOR_T1462014386_H
#define JPROPERTYDESCRIPTOR_T1462014386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JPropertyDescriptor
struct  JPropertyDescriptor_t1462014386  : public PropertyDescriptor_t3244362832
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPROPERTYDESCRIPTOR_T1462014386_H
#ifndef JAVASCRIPTDATETIMECONVERTER_T3307376016_H
#define JAVASCRIPTDATETIMECONVERTER_T3307376016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.JavaScriptDateTimeConverter
struct  JavaScriptDateTimeConverter_t3307376016  : public DateTimeConverterBase_t4233217353
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JAVASCRIPTDATETIMECONVERTER_T3307376016_H
#ifndef JCONSTRUCTOR_T3659803310_H
#define JCONSTRUCTOR_T3659803310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JConstructor
struct  JConstructor_t3659803310  : public JContainer_t1023664833
{
public:
	// System.String Newtonsoft.Json.Linq.JConstructor::_name
	String_t* ____name_16;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JConstructor::_values
	List_1_t2510613989 * ____values_17;

public:
	inline static int32_t get_offset_of__name_16() { return static_cast<int32_t>(offsetof(JConstructor_t3659803310, ____name_16)); }
	inline String_t* get__name_16() const { return ____name_16; }
	inline String_t** get_address_of__name_16() { return &____name_16; }
	inline void set__name_16(String_t* value)
	{
		____name_16 = value;
		Il2CppCodeGenWriteBarrier((&____name_16), value);
	}

	inline static int32_t get_offset_of__values_17() { return static_cast<int32_t>(offsetof(JConstructor_t3659803310, ____values_17)); }
	inline List_1_t2510613989 * get__values_17() const { return ____values_17; }
	inline List_1_t2510613989 ** get_address_of__values_17() { return &____values_17; }
	inline void set__values_17(List_1_t2510613989 * value)
	{
		____values_17 = value;
		Il2CppCodeGenWriteBarrier((&____values_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JCONSTRUCTOR_T3659803310_H
#ifndef MERGENULLVALUEHANDLING_T2760645705_H
#define MERGENULLVALUEHANDLING_T2760645705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.MergeNullValueHandling
struct  MergeNullValueHandling_t2760645705 
{
public:
	// System.Int32 Newtonsoft.Json.Linq.MergeNullValueHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MergeNullValueHandling_t2760645705, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MERGENULLVALUEHANDLING_T2760645705_H
#ifndef JSONCONTAINERTYPE_T3191599701_H
#define JSONCONTAINERTYPE_T3191599701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonContainerType
struct  JsonContainerType_t3191599701 
{
public:
	// System.Int32 Newtonsoft.Json.JsonContainerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JsonContainerType_t3191599701, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTAINERTYPE_T3191599701_H
#ifndef MERGEARRAYHANDLING_T691603146_H
#define MERGEARRAYHANDLING_T691603146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.MergeArrayHandling
struct  MergeArrayHandling_t691603146 
{
public:
	// System.Int32 Newtonsoft.Json.Linq.MergeArrayHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MergeArrayHandling_t691603146, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MERGEARRAYHANDLING_T691603146_H
#ifndef NULLABLE_1_T1505470351_H
#define NULLABLE_1_T1505470351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.StringEscapeHandling>
struct  Nullable_1_t1505470351 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1505470351, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1505470351, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1505470351_H
#ifndef NULLABLE_1_T2729527740_H
#define NULLABLE_1_T2729527740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.FloatParseHandling>
struct  Nullable_1_t2729527740 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2729527740, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2729527740, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2729527740_H
#ifndef JSONCONTRACT_T3715061318_H
#define JSONCONTRACT_T3715061318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonContract
struct  JsonContract_t3715061318  : public RuntimeObject
{
public:
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsNullable
	bool ___IsNullable_0;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsConvertable
	bool ___IsConvertable_1;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsEnum
	bool ___IsEnum_2;
	// System.Type Newtonsoft.Json.Serialization.JsonContract::NonNullableUnderlyingType
	Type_t * ___NonNullableUnderlyingType_3;
	// Newtonsoft.Json.ReadType Newtonsoft.Json.Serialization.JsonContract::InternalReadType
	int32_t ___InternalReadType_4;
	// Newtonsoft.Json.Serialization.JsonContractType Newtonsoft.Json.Serialization.JsonContract::ContractType
	int32_t ___ContractType_5;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsReadOnlyOrFixedSize
	bool ___IsReadOnlyOrFixedSize_6;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsSealed
	bool ___IsSealed_7;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsInstantiable
	bool ___IsInstantiable_8;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::_onDeserializedCallbacks
	List_1_t3780931481 * ____onDeserializedCallbacks_9;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::_onDeserializingCallbacks
	RuntimeObject* ____onDeserializingCallbacks_10;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::_onSerializedCallbacks
	RuntimeObject* ____onSerializedCallbacks_11;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::_onSerializingCallbacks
	RuntimeObject* ____onSerializingCallbacks_12;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationErrorCallback> Newtonsoft.Json.Serialization.JsonContract::_onErrorCallbacks
	RuntimeObject* ____onErrorCallbacks_13;
	// System.Type Newtonsoft.Json.Serialization.JsonContract::_createdType
	Type_t * ____createdType_14;
	// System.Type Newtonsoft.Json.Serialization.JsonContract::<UnderlyingType>k__BackingField
	Type_t * ___U3CUnderlyingTypeU3Ek__BackingField_15;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonContract::<IsReference>k__BackingField
	Nullable_1_t1819850047  ___U3CIsReferenceU3Ek__BackingField_16;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContract::<Converter>k__BackingField
	JsonConverter_t1047106545 * ___U3CConverterU3Ek__BackingField_17;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContract::<InternalConverter>k__BackingField
	JsonConverter_t1047106545 * ___U3CInternalConverterU3Ek__BackingField_18;
	// System.Func`1<System.Object> Newtonsoft.Json.Serialization.JsonContract::<DefaultCreator>k__BackingField
	Func_1_t2509852811 * ___U3CDefaultCreatorU3Ek__BackingField_19;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::<DefaultCreatorNonPublic>k__BackingField
	bool ___U3CDefaultCreatorNonPublicU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_IsNullable_0() { return static_cast<int32_t>(offsetof(JsonContract_t3715061318, ___IsNullable_0)); }
	inline bool get_IsNullable_0() const { return ___IsNullable_0; }
	inline bool* get_address_of_IsNullable_0() { return &___IsNullable_0; }
	inline void set_IsNullable_0(bool value)
	{
		___IsNullable_0 = value;
	}

	inline static int32_t get_offset_of_IsConvertable_1() { return static_cast<int32_t>(offsetof(JsonContract_t3715061318, ___IsConvertable_1)); }
	inline bool get_IsConvertable_1() const { return ___IsConvertable_1; }
	inline bool* get_address_of_IsConvertable_1() { return &___IsConvertable_1; }
	inline void set_IsConvertable_1(bool value)
	{
		___IsConvertable_1 = value;
	}

	inline static int32_t get_offset_of_IsEnum_2() { return static_cast<int32_t>(offsetof(JsonContract_t3715061318, ___IsEnum_2)); }
	inline bool get_IsEnum_2() const { return ___IsEnum_2; }
	inline bool* get_address_of_IsEnum_2() { return &___IsEnum_2; }
	inline void set_IsEnum_2(bool value)
	{
		___IsEnum_2 = value;
	}

	inline static int32_t get_offset_of_NonNullableUnderlyingType_3() { return static_cast<int32_t>(offsetof(JsonContract_t3715061318, ___NonNullableUnderlyingType_3)); }
	inline Type_t * get_NonNullableUnderlyingType_3() const { return ___NonNullableUnderlyingType_3; }
	inline Type_t ** get_address_of_NonNullableUnderlyingType_3() { return &___NonNullableUnderlyingType_3; }
	inline void set_NonNullableUnderlyingType_3(Type_t * value)
	{
		___NonNullableUnderlyingType_3 = value;
		Il2CppCodeGenWriteBarrier((&___NonNullableUnderlyingType_3), value);
	}

	inline static int32_t get_offset_of_InternalReadType_4() { return static_cast<int32_t>(offsetof(JsonContract_t3715061318, ___InternalReadType_4)); }
	inline int32_t get_InternalReadType_4() const { return ___InternalReadType_4; }
	inline int32_t* get_address_of_InternalReadType_4() { return &___InternalReadType_4; }
	inline void set_InternalReadType_4(int32_t value)
	{
		___InternalReadType_4 = value;
	}

	inline static int32_t get_offset_of_ContractType_5() { return static_cast<int32_t>(offsetof(JsonContract_t3715061318, ___ContractType_5)); }
	inline int32_t get_ContractType_5() const { return ___ContractType_5; }
	inline int32_t* get_address_of_ContractType_5() { return &___ContractType_5; }
	inline void set_ContractType_5(int32_t value)
	{
		___ContractType_5 = value;
	}

	inline static int32_t get_offset_of_IsReadOnlyOrFixedSize_6() { return static_cast<int32_t>(offsetof(JsonContract_t3715061318, ___IsReadOnlyOrFixedSize_6)); }
	inline bool get_IsReadOnlyOrFixedSize_6() const { return ___IsReadOnlyOrFixedSize_6; }
	inline bool* get_address_of_IsReadOnlyOrFixedSize_6() { return &___IsReadOnlyOrFixedSize_6; }
	inline void set_IsReadOnlyOrFixedSize_6(bool value)
	{
		___IsReadOnlyOrFixedSize_6 = value;
	}

	inline static int32_t get_offset_of_IsSealed_7() { return static_cast<int32_t>(offsetof(JsonContract_t3715061318, ___IsSealed_7)); }
	inline bool get_IsSealed_7() const { return ___IsSealed_7; }
	inline bool* get_address_of_IsSealed_7() { return &___IsSealed_7; }
	inline void set_IsSealed_7(bool value)
	{
		___IsSealed_7 = value;
	}

	inline static int32_t get_offset_of_IsInstantiable_8() { return static_cast<int32_t>(offsetof(JsonContract_t3715061318, ___IsInstantiable_8)); }
	inline bool get_IsInstantiable_8() const { return ___IsInstantiable_8; }
	inline bool* get_address_of_IsInstantiable_8() { return &___IsInstantiable_8; }
	inline void set_IsInstantiable_8(bool value)
	{
		___IsInstantiable_8 = value;
	}

	inline static int32_t get_offset_of__onDeserializedCallbacks_9() { return static_cast<int32_t>(offsetof(JsonContract_t3715061318, ____onDeserializedCallbacks_9)); }
	inline List_1_t3780931481 * get__onDeserializedCallbacks_9() const { return ____onDeserializedCallbacks_9; }
	inline List_1_t3780931481 ** get_address_of__onDeserializedCallbacks_9() { return &____onDeserializedCallbacks_9; }
	inline void set__onDeserializedCallbacks_9(List_1_t3780931481 * value)
	{
		____onDeserializedCallbacks_9 = value;
		Il2CppCodeGenWriteBarrier((&____onDeserializedCallbacks_9), value);
	}

	inline static int32_t get_offset_of__onDeserializingCallbacks_10() { return static_cast<int32_t>(offsetof(JsonContract_t3715061318, ____onDeserializingCallbacks_10)); }
	inline RuntimeObject* get__onDeserializingCallbacks_10() const { return ____onDeserializingCallbacks_10; }
	inline RuntimeObject** get_address_of__onDeserializingCallbacks_10() { return &____onDeserializingCallbacks_10; }
	inline void set__onDeserializingCallbacks_10(RuntimeObject* value)
	{
		____onDeserializingCallbacks_10 = value;
		Il2CppCodeGenWriteBarrier((&____onDeserializingCallbacks_10), value);
	}

	inline static int32_t get_offset_of__onSerializedCallbacks_11() { return static_cast<int32_t>(offsetof(JsonContract_t3715061318, ____onSerializedCallbacks_11)); }
	inline RuntimeObject* get__onSerializedCallbacks_11() const { return ____onSerializedCallbacks_11; }
	inline RuntimeObject** get_address_of__onSerializedCallbacks_11() { return &____onSerializedCallbacks_11; }
	inline void set__onSerializedCallbacks_11(RuntimeObject* value)
	{
		____onSerializedCallbacks_11 = value;
		Il2CppCodeGenWriteBarrier((&____onSerializedCallbacks_11), value);
	}

	inline static int32_t get_offset_of__onSerializingCallbacks_12() { return static_cast<int32_t>(offsetof(JsonContract_t3715061318, ____onSerializingCallbacks_12)); }
	inline RuntimeObject* get__onSerializingCallbacks_12() const { return ____onSerializingCallbacks_12; }
	inline RuntimeObject** get_address_of__onSerializingCallbacks_12() { return &____onSerializingCallbacks_12; }
	inline void set__onSerializingCallbacks_12(RuntimeObject* value)
	{
		____onSerializingCallbacks_12 = value;
		Il2CppCodeGenWriteBarrier((&____onSerializingCallbacks_12), value);
	}

	inline static int32_t get_offset_of__onErrorCallbacks_13() { return static_cast<int32_t>(offsetof(JsonContract_t3715061318, ____onErrorCallbacks_13)); }
	inline RuntimeObject* get__onErrorCallbacks_13() const { return ____onErrorCallbacks_13; }
	inline RuntimeObject** get_address_of__onErrorCallbacks_13() { return &____onErrorCallbacks_13; }
	inline void set__onErrorCallbacks_13(RuntimeObject* value)
	{
		____onErrorCallbacks_13 = value;
		Il2CppCodeGenWriteBarrier((&____onErrorCallbacks_13), value);
	}

	inline static int32_t get_offset_of__createdType_14() { return static_cast<int32_t>(offsetof(JsonContract_t3715061318, ____createdType_14)); }
	inline Type_t * get__createdType_14() const { return ____createdType_14; }
	inline Type_t ** get_address_of__createdType_14() { return &____createdType_14; }
	inline void set__createdType_14(Type_t * value)
	{
		____createdType_14 = value;
		Il2CppCodeGenWriteBarrier((&____createdType_14), value);
	}

	inline static int32_t get_offset_of_U3CUnderlyingTypeU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(JsonContract_t3715061318, ___U3CUnderlyingTypeU3Ek__BackingField_15)); }
	inline Type_t * get_U3CUnderlyingTypeU3Ek__BackingField_15() const { return ___U3CUnderlyingTypeU3Ek__BackingField_15; }
	inline Type_t ** get_address_of_U3CUnderlyingTypeU3Ek__BackingField_15() { return &___U3CUnderlyingTypeU3Ek__BackingField_15; }
	inline void set_U3CUnderlyingTypeU3Ek__BackingField_15(Type_t * value)
	{
		___U3CUnderlyingTypeU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnderlyingTypeU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CIsReferenceU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(JsonContract_t3715061318, ___U3CIsReferenceU3Ek__BackingField_16)); }
	inline Nullable_1_t1819850047  get_U3CIsReferenceU3Ek__BackingField_16() const { return ___U3CIsReferenceU3Ek__BackingField_16; }
	inline Nullable_1_t1819850047 * get_address_of_U3CIsReferenceU3Ek__BackingField_16() { return &___U3CIsReferenceU3Ek__BackingField_16; }
	inline void set_U3CIsReferenceU3Ek__BackingField_16(Nullable_1_t1819850047  value)
	{
		___U3CIsReferenceU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CConverterU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(JsonContract_t3715061318, ___U3CConverterU3Ek__BackingField_17)); }
	inline JsonConverter_t1047106545 * get_U3CConverterU3Ek__BackingField_17() const { return ___U3CConverterU3Ek__BackingField_17; }
	inline JsonConverter_t1047106545 ** get_address_of_U3CConverterU3Ek__BackingField_17() { return &___U3CConverterU3Ek__BackingField_17; }
	inline void set_U3CConverterU3Ek__BackingField_17(JsonConverter_t1047106545 * value)
	{
		___U3CConverterU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConverterU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CInternalConverterU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(JsonContract_t3715061318, ___U3CInternalConverterU3Ek__BackingField_18)); }
	inline JsonConverter_t1047106545 * get_U3CInternalConverterU3Ek__BackingField_18() const { return ___U3CInternalConverterU3Ek__BackingField_18; }
	inline JsonConverter_t1047106545 ** get_address_of_U3CInternalConverterU3Ek__BackingField_18() { return &___U3CInternalConverterU3Ek__BackingField_18; }
	inline void set_U3CInternalConverterU3Ek__BackingField_18(JsonConverter_t1047106545 * value)
	{
		___U3CInternalConverterU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInternalConverterU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_U3CDefaultCreatorU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(JsonContract_t3715061318, ___U3CDefaultCreatorU3Ek__BackingField_19)); }
	inline Func_1_t2509852811 * get_U3CDefaultCreatorU3Ek__BackingField_19() const { return ___U3CDefaultCreatorU3Ek__BackingField_19; }
	inline Func_1_t2509852811 ** get_address_of_U3CDefaultCreatorU3Ek__BackingField_19() { return &___U3CDefaultCreatorU3Ek__BackingField_19; }
	inline void set_U3CDefaultCreatorU3Ek__BackingField_19(Func_1_t2509852811 * value)
	{
		___U3CDefaultCreatorU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultCreatorU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_U3CDefaultCreatorNonPublicU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(JsonContract_t3715061318, ___U3CDefaultCreatorNonPublicU3Ek__BackingField_20)); }
	inline bool get_U3CDefaultCreatorNonPublicU3Ek__BackingField_20() const { return ___U3CDefaultCreatorNonPublicU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CDefaultCreatorNonPublicU3Ek__BackingField_20() { return &___U3CDefaultCreatorNonPublicU3Ek__BackingField_20; }
	inline void set_U3CDefaultCreatorNonPublicU3Ek__BackingField_20(bool value)
	{
		___U3CDefaultCreatorNonPublicU3Ek__BackingField_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTRACT_T3715061318_H
#ifndef JSONLOADSETTINGS_T878621609_H
#define JSONLOADSETTINGS_T878621609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonLoadSettings
struct  JsonLoadSettings_t878621609  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Linq.CommentHandling Newtonsoft.Json.Linq.JsonLoadSettings::_commentHandling
	int32_t ____commentHandling_0;
	// Newtonsoft.Json.Linq.LineInfoHandling Newtonsoft.Json.Linq.JsonLoadSettings::_lineInfoHandling
	int32_t ____lineInfoHandling_1;

public:
	inline static int32_t get_offset_of__commentHandling_0() { return static_cast<int32_t>(offsetof(JsonLoadSettings_t878621609, ____commentHandling_0)); }
	inline int32_t get__commentHandling_0() const { return ____commentHandling_0; }
	inline int32_t* get_address_of__commentHandling_0() { return &____commentHandling_0; }
	inline void set__commentHandling_0(int32_t value)
	{
		____commentHandling_0 = value;
	}

	inline static int32_t get_offset_of__lineInfoHandling_1() { return static_cast<int32_t>(offsetof(JsonLoadSettings_t878621609, ____lineInfoHandling_1)); }
	inline int32_t get__lineInfoHandling_1() const { return ____lineInfoHandling_1; }
	inline int32_t* get_address_of__lineInfoHandling_1() { return &____lineInfoHandling_1; }
	inline void set__lineInfoHandling_1(int32_t value)
	{
		____lineInfoHandling_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONLOADSETTINGS_T878621609_H
#ifndef JSONPOSITION_T2528027714_H
#define JSONPOSITION_T2528027714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonPosition
struct  JsonPosition_t2528027714 
{
public:
	// Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonPosition::Type
	int32_t ___Type_1;
	// System.Int32 Newtonsoft.Json.JsonPosition::Position
	int32_t ___Position_2;
	// System.String Newtonsoft.Json.JsonPosition::PropertyName
	String_t* ___PropertyName_3;
	// System.Boolean Newtonsoft.Json.JsonPosition::HasIndex
	bool ___HasIndex_4;

public:
	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(JsonPosition_t2528027714, ___Type_1)); }
	inline int32_t get_Type_1() const { return ___Type_1; }
	inline int32_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(int32_t value)
	{
		___Type_1 = value;
	}

	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(JsonPosition_t2528027714, ___Position_2)); }
	inline int32_t get_Position_2() const { return ___Position_2; }
	inline int32_t* get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(int32_t value)
	{
		___Position_2 = value;
	}

	inline static int32_t get_offset_of_PropertyName_3() { return static_cast<int32_t>(offsetof(JsonPosition_t2528027714, ___PropertyName_3)); }
	inline String_t* get_PropertyName_3() const { return ___PropertyName_3; }
	inline String_t** get_address_of_PropertyName_3() { return &___PropertyName_3; }
	inline void set_PropertyName_3(String_t* value)
	{
		___PropertyName_3 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyName_3), value);
	}

	inline static int32_t get_offset_of_HasIndex_4() { return static_cast<int32_t>(offsetof(JsonPosition_t2528027714, ___HasIndex_4)); }
	inline bool get_HasIndex_4() const { return ___HasIndex_4; }
	inline bool* get_address_of_HasIndex_4() { return &___HasIndex_4; }
	inline void set_HasIndex_4(bool value)
	{
		___HasIndex_4 = value;
	}
};

struct JsonPosition_t2528027714_StaticFields
{
public:
	// System.Char[] Newtonsoft.Json.JsonPosition::SpecialCharacters
	CharU5BU5D_t3528271667* ___SpecialCharacters_0;

public:
	inline static int32_t get_offset_of_SpecialCharacters_0() { return static_cast<int32_t>(offsetof(JsonPosition_t2528027714_StaticFields, ___SpecialCharacters_0)); }
	inline CharU5BU5D_t3528271667* get_SpecialCharacters_0() const { return ___SpecialCharacters_0; }
	inline CharU5BU5D_t3528271667** get_address_of_SpecialCharacters_0() { return &___SpecialCharacters_0; }
	inline void set_SpecialCharacters_0(CharU5BU5D_t3528271667* value)
	{
		___SpecialCharacters_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpecialCharacters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_t2528027714_marshaled_pinvoke
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	char* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
// Native definition for COM marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_t2528027714_marshaled_com
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	Il2CppChar* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
#endif // JSONPOSITION_T2528027714_H
#ifndef NULLABLE_1_T2285469903_H
#define NULLABLE_1_T2285469903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.FloatFormatHandling>
struct  Nullable_1_t2285469903 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2285469903, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2285469903, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2285469903_H
#ifndef NULLABLE_1_T4213156694_H
#define NULLABLE_1_T4213156694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>
struct  Nullable_1_t4213156694 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t4213156694, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t4213156694, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T4213156694_H
#ifndef QUERYEXPRESSION_T3171385195_H
#define QUERYEXPRESSION_T3171385195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.QueryExpression
struct  QueryExpression_t3171385195  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Linq.JsonPath.QueryOperator Newtonsoft.Json.Linq.JsonPath.QueryExpression::<Operator>k__BackingField
	int32_t ___U3COperatorU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3COperatorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(QueryExpression_t3171385195, ___U3COperatorU3Ek__BackingField_0)); }
	inline int32_t get_U3COperatorU3Ek__BackingField_0() const { return ___U3COperatorU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3COperatorU3Ek__BackingField_0() { return &___U3COperatorU3Ek__BackingField_0; }
	inline void set_U3COperatorU3Ek__BackingField_0(int32_t value)
	{
		___U3COperatorU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYEXPRESSION_T3171385195_H
#ifndef JVALUE_T1448862567_H
#define JVALUE_T1448862567_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JValue
struct  JValue_t1448862567  : public JToken_t1038539247
{
public:
	// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JValue::_valueType
	int32_t ____valueType_13;
	// System.Object Newtonsoft.Json.Linq.JValue::_value
	RuntimeObject * ____value_14;

public:
	inline static int32_t get_offset_of__valueType_13() { return static_cast<int32_t>(offsetof(JValue_t1448862567, ____valueType_13)); }
	inline int32_t get__valueType_13() const { return ____valueType_13; }
	inline int32_t* get_address_of__valueType_13() { return &____valueType_13; }
	inline void set__valueType_13(int32_t value)
	{
		____valueType_13 = value;
	}

	inline static int32_t get_offset_of__value_14() { return static_cast<int32_t>(offsetof(JValue_t1448862567, ____value_14)); }
	inline RuntimeObject * get__value_14() const { return ____value_14; }
	inline RuntimeObject ** get_address_of__value_14() { return &____value_14; }
	inline void set__value_14(RuntimeObject * value)
	{
		____value_14 = value;
		Il2CppCodeGenWriteBarrier((&____value_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JVALUE_T1448862567_H
#ifndef STREAMINGCONTEXT_T3711869237_H
#define STREAMINGCONTEXT_T3711869237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContext
struct  StreamingContext_t3711869237 
{
public:
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::state
	int32_t ___state_0;
	// System.Object System.Runtime.Serialization.StreamingContext::additional
	RuntimeObject * ___additional_1;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(StreamingContext_t3711869237, ___state_0)); }
	inline int32_t get_state_0() const { return ___state_0; }
	inline int32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(int32_t value)
	{
		___state_0 = value;
	}

	inline static int32_t get_offset_of_additional_1() { return static_cast<int32_t>(offsetof(StreamingContext_t3711869237, ___additional_1)); }
	inline RuntimeObject * get_additional_1() const { return ___additional_1; }
	inline RuntimeObject ** get_address_of_additional_1() { return &___additional_1; }
	inline void set_additional_1(RuntimeObject * value)
	{
		___additional_1 = value;
		Il2CppCodeGenWriteBarrier((&___additional_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t3711869237_marshaled_pinvoke
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t3711869237_marshaled_com
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
#endif // STREAMINGCONTEXT_T3711869237_H
#ifndef NULLABLE_1_T430194516_H
#define NULLABLE_1_T430194516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling>
struct  Nullable_1_t430194516 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t430194516, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t430194516, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T430194516_H
#ifndef NULLABLE_1_T179296662_H
#define NULLABLE_1_T179296662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DateParseHandling>
struct  Nullable_1_t179296662 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t179296662, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t179296662, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T179296662_H
#ifndef ISODATETIMECONVERTER_T3020114287_H
#define ISODATETIMECONVERTER_T3020114287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.IsoDateTimeConverter
struct  IsoDateTimeConverter_t3020114287  : public DateTimeConverterBase_t4233217353
{
public:
	// System.Globalization.DateTimeStyles Newtonsoft.Json.Converters.IsoDateTimeConverter::_dateTimeStyles
	int32_t ____dateTimeStyles_0;
	// System.String Newtonsoft.Json.Converters.IsoDateTimeConverter::_dateTimeFormat
	String_t* ____dateTimeFormat_1;
	// System.Globalization.CultureInfo Newtonsoft.Json.Converters.IsoDateTimeConverter::_culture
	CultureInfo_t4157843068 * ____culture_2;

public:
	inline static int32_t get_offset_of__dateTimeStyles_0() { return static_cast<int32_t>(offsetof(IsoDateTimeConverter_t3020114287, ____dateTimeStyles_0)); }
	inline int32_t get__dateTimeStyles_0() const { return ____dateTimeStyles_0; }
	inline int32_t* get_address_of__dateTimeStyles_0() { return &____dateTimeStyles_0; }
	inline void set__dateTimeStyles_0(int32_t value)
	{
		____dateTimeStyles_0 = value;
	}

	inline static int32_t get_offset_of__dateTimeFormat_1() { return static_cast<int32_t>(offsetof(IsoDateTimeConverter_t3020114287, ____dateTimeFormat_1)); }
	inline String_t* get__dateTimeFormat_1() const { return ____dateTimeFormat_1; }
	inline String_t** get_address_of__dateTimeFormat_1() { return &____dateTimeFormat_1; }
	inline void set__dateTimeFormat_1(String_t* value)
	{
		____dateTimeFormat_1 = value;
		Il2CppCodeGenWriteBarrier((&____dateTimeFormat_1), value);
	}

	inline static int32_t get_offset_of__culture_2() { return static_cast<int32_t>(offsetof(IsoDateTimeConverter_t3020114287, ____culture_2)); }
	inline CultureInfo_t4157843068 * get__culture_2() const { return ____culture_2; }
	inline CultureInfo_t4157843068 ** get_address_of__culture_2() { return &____culture_2; }
	inline void set__culture_2(CultureInfo_t4157843068 * value)
	{
		____culture_2 = value;
		Il2CppCodeGenWriteBarrier((&____culture_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ISODATETIMECONVERTER_T3020114287_H
#ifndef NULLABLE_1_T3098729937_H
#define NULLABLE_1_T3098729937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DateFormatHandling>
struct  Nullable_1_t3098729937 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3098729937, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3098729937, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3098729937_H
#ifndef NULLABLE_1_T3914607011_H
#define NULLABLE_1_T3914607011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.Formatting>
struct  Nullable_1_t3914607011 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3914607011, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3914607011, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3914607011_H
#ifndef JSONREADER_T2369136700_H
#define JSONREADER_T2369136700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonReader
struct  JsonReader_t2369136700  : public RuntimeObject
{
public:
	// Newtonsoft.Json.JsonToken Newtonsoft.Json.JsonReader::_tokenType
	int32_t ____tokenType_0;
	// System.Object Newtonsoft.Json.JsonReader::_value
	RuntimeObject * ____value_1;
	// System.Char Newtonsoft.Json.JsonReader::_quoteChar
	Il2CppChar ____quoteChar_2;
	// Newtonsoft.Json.JsonReader/State Newtonsoft.Json.JsonReader::_currentState
	int32_t ____currentState_3;
	// Newtonsoft.Json.JsonPosition Newtonsoft.Json.JsonReader::_currentPosition
	JsonPosition_t2528027714  ____currentPosition_4;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonReader::_culture
	CultureInfo_t4157843068 * ____culture_5;
	// Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonReader::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_6;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonReader::_maxDepth
	Nullable_1_t378540539  ____maxDepth_7;
	// System.Boolean Newtonsoft.Json.JsonReader::_hasExceededMaxDepth
	bool ____hasExceededMaxDepth_8;
	// Newtonsoft.Json.DateParseHandling Newtonsoft.Json.JsonReader::_dateParseHandling
	int32_t ____dateParseHandling_9;
	// Newtonsoft.Json.FloatParseHandling Newtonsoft.Json.JsonReader::_floatParseHandling
	int32_t ____floatParseHandling_10;
	// System.String Newtonsoft.Json.JsonReader::_dateFormatString
	String_t* ____dateFormatString_11;
	// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition> Newtonsoft.Json.JsonReader::_stack
	List_1_t4000102456 * ____stack_12;
	// System.Boolean Newtonsoft.Json.JsonReader::<CloseInput>k__BackingField
	bool ___U3CCloseInputU3Ek__BackingField_13;
	// System.Boolean Newtonsoft.Json.JsonReader::<SupportMultipleContent>k__BackingField
	bool ___U3CSupportMultipleContentU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of__tokenType_0() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____tokenType_0)); }
	inline int32_t get__tokenType_0() const { return ____tokenType_0; }
	inline int32_t* get_address_of__tokenType_0() { return &____tokenType_0; }
	inline void set__tokenType_0(int32_t value)
	{
		____tokenType_0 = value;
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____value_1)); }
	inline RuntimeObject * get__value_1() const { return ____value_1; }
	inline RuntimeObject ** get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(RuntimeObject * value)
	{
		____value_1 = value;
		Il2CppCodeGenWriteBarrier((&____value_1), value);
	}

	inline static int32_t get_offset_of__quoteChar_2() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____quoteChar_2)); }
	inline Il2CppChar get__quoteChar_2() const { return ____quoteChar_2; }
	inline Il2CppChar* get_address_of__quoteChar_2() { return &____quoteChar_2; }
	inline void set__quoteChar_2(Il2CppChar value)
	{
		____quoteChar_2 = value;
	}

	inline static int32_t get_offset_of__currentState_3() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____currentState_3)); }
	inline int32_t get__currentState_3() const { return ____currentState_3; }
	inline int32_t* get_address_of__currentState_3() { return &____currentState_3; }
	inline void set__currentState_3(int32_t value)
	{
		____currentState_3 = value;
	}

	inline static int32_t get_offset_of__currentPosition_4() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____currentPosition_4)); }
	inline JsonPosition_t2528027714  get__currentPosition_4() const { return ____currentPosition_4; }
	inline JsonPosition_t2528027714 * get_address_of__currentPosition_4() { return &____currentPosition_4; }
	inline void set__currentPosition_4(JsonPosition_t2528027714  value)
	{
		____currentPosition_4 = value;
	}

	inline static int32_t get_offset_of__culture_5() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____culture_5)); }
	inline CultureInfo_t4157843068 * get__culture_5() const { return ____culture_5; }
	inline CultureInfo_t4157843068 ** get_address_of__culture_5() { return &____culture_5; }
	inline void set__culture_5(CultureInfo_t4157843068 * value)
	{
		____culture_5 = value;
		Il2CppCodeGenWriteBarrier((&____culture_5), value);
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_6() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____dateTimeZoneHandling_6)); }
	inline int32_t get__dateTimeZoneHandling_6() const { return ____dateTimeZoneHandling_6; }
	inline int32_t* get_address_of__dateTimeZoneHandling_6() { return &____dateTimeZoneHandling_6; }
	inline void set__dateTimeZoneHandling_6(int32_t value)
	{
		____dateTimeZoneHandling_6 = value;
	}

	inline static int32_t get_offset_of__maxDepth_7() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____maxDepth_7)); }
	inline Nullable_1_t378540539  get__maxDepth_7() const { return ____maxDepth_7; }
	inline Nullable_1_t378540539 * get_address_of__maxDepth_7() { return &____maxDepth_7; }
	inline void set__maxDepth_7(Nullable_1_t378540539  value)
	{
		____maxDepth_7 = value;
	}

	inline static int32_t get_offset_of__hasExceededMaxDepth_8() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____hasExceededMaxDepth_8)); }
	inline bool get__hasExceededMaxDepth_8() const { return ____hasExceededMaxDepth_8; }
	inline bool* get_address_of__hasExceededMaxDepth_8() { return &____hasExceededMaxDepth_8; }
	inline void set__hasExceededMaxDepth_8(bool value)
	{
		____hasExceededMaxDepth_8 = value;
	}

	inline static int32_t get_offset_of__dateParseHandling_9() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____dateParseHandling_9)); }
	inline int32_t get__dateParseHandling_9() const { return ____dateParseHandling_9; }
	inline int32_t* get_address_of__dateParseHandling_9() { return &____dateParseHandling_9; }
	inline void set__dateParseHandling_9(int32_t value)
	{
		____dateParseHandling_9 = value;
	}

	inline static int32_t get_offset_of__floatParseHandling_10() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____floatParseHandling_10)); }
	inline int32_t get__floatParseHandling_10() const { return ____floatParseHandling_10; }
	inline int32_t* get_address_of__floatParseHandling_10() { return &____floatParseHandling_10; }
	inline void set__floatParseHandling_10(int32_t value)
	{
		____floatParseHandling_10 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_11() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____dateFormatString_11)); }
	inline String_t* get__dateFormatString_11() const { return ____dateFormatString_11; }
	inline String_t** get_address_of__dateFormatString_11() { return &____dateFormatString_11; }
	inline void set__dateFormatString_11(String_t* value)
	{
		____dateFormatString_11 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_11), value);
	}

	inline static int32_t get_offset_of__stack_12() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____stack_12)); }
	inline List_1_t4000102456 * get__stack_12() const { return ____stack_12; }
	inline List_1_t4000102456 ** get_address_of__stack_12() { return &____stack_12; }
	inline void set__stack_12(List_1_t4000102456 * value)
	{
		____stack_12 = value;
		Il2CppCodeGenWriteBarrier((&____stack_12), value);
	}

	inline static int32_t get_offset_of_U3CCloseInputU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ___U3CCloseInputU3Ek__BackingField_13)); }
	inline bool get_U3CCloseInputU3Ek__BackingField_13() const { return ___U3CCloseInputU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CCloseInputU3Ek__BackingField_13() { return &___U3CCloseInputU3Ek__BackingField_13; }
	inline void set_U3CCloseInputU3Ek__BackingField_13(bool value)
	{
		___U3CCloseInputU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CSupportMultipleContentU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ___U3CSupportMultipleContentU3Ek__BackingField_14)); }
	inline bool get_U3CSupportMultipleContentU3Ek__BackingField_14() const { return ___U3CSupportMultipleContentU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CSupportMultipleContentU3Ek__BackingField_14() { return &___U3CSupportMultipleContentU3Ek__BackingField_14; }
	inline void set_U3CSupportMultipleContentU3Ek__BackingField_14(bool value)
	{
		___U3CSupportMultipleContentU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONREADER_T2369136700_H
#ifndef JSONWRITER_T1467272295_H
#define JSONWRITER_T1467272295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter
struct  JsonWriter_t1467272295  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition> Newtonsoft.Json.JsonWriter::_stack
	List_1_t4000102456 * ____stack_2;
	// Newtonsoft.Json.JsonPosition Newtonsoft.Json.JsonWriter::_currentPosition
	JsonPosition_t2528027714  ____currentPosition_3;
	// Newtonsoft.Json.JsonWriter/State Newtonsoft.Json.JsonWriter::_currentState
	int32_t ____currentState_4;
	// Newtonsoft.Json.Formatting Newtonsoft.Json.JsonWriter::_formatting
	int32_t ____formatting_5;
	// System.Boolean Newtonsoft.Json.JsonWriter::<CloseOutput>k__BackingField
	bool ___U3CCloseOutputU3Ek__BackingField_6;
	// Newtonsoft.Json.DateFormatHandling Newtonsoft.Json.JsonWriter::_dateFormatHandling
	int32_t ____dateFormatHandling_7;
	// Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonWriter::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_8;
	// Newtonsoft.Json.StringEscapeHandling Newtonsoft.Json.JsonWriter::_stringEscapeHandling
	int32_t ____stringEscapeHandling_9;
	// Newtonsoft.Json.FloatFormatHandling Newtonsoft.Json.JsonWriter::_floatFormatHandling
	int32_t ____floatFormatHandling_10;
	// System.String Newtonsoft.Json.JsonWriter::_dateFormatString
	String_t* ____dateFormatString_11;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonWriter::_culture
	CultureInfo_t4157843068 * ____culture_12;

public:
	inline static int32_t get_offset_of__stack_2() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____stack_2)); }
	inline List_1_t4000102456 * get__stack_2() const { return ____stack_2; }
	inline List_1_t4000102456 ** get_address_of__stack_2() { return &____stack_2; }
	inline void set__stack_2(List_1_t4000102456 * value)
	{
		____stack_2 = value;
		Il2CppCodeGenWriteBarrier((&____stack_2), value);
	}

	inline static int32_t get_offset_of__currentPosition_3() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____currentPosition_3)); }
	inline JsonPosition_t2528027714  get__currentPosition_3() const { return ____currentPosition_3; }
	inline JsonPosition_t2528027714 * get_address_of__currentPosition_3() { return &____currentPosition_3; }
	inline void set__currentPosition_3(JsonPosition_t2528027714  value)
	{
		____currentPosition_3 = value;
	}

	inline static int32_t get_offset_of__currentState_4() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____currentState_4)); }
	inline int32_t get__currentState_4() const { return ____currentState_4; }
	inline int32_t* get_address_of__currentState_4() { return &____currentState_4; }
	inline void set__currentState_4(int32_t value)
	{
		____currentState_4 = value;
	}

	inline static int32_t get_offset_of__formatting_5() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____formatting_5)); }
	inline int32_t get__formatting_5() const { return ____formatting_5; }
	inline int32_t* get_address_of__formatting_5() { return &____formatting_5; }
	inline void set__formatting_5(int32_t value)
	{
		____formatting_5 = value;
	}

	inline static int32_t get_offset_of_U3CCloseOutputU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ___U3CCloseOutputU3Ek__BackingField_6)); }
	inline bool get_U3CCloseOutputU3Ek__BackingField_6() const { return ___U3CCloseOutputU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CCloseOutputU3Ek__BackingField_6() { return &___U3CCloseOutputU3Ek__BackingField_6; }
	inline void set_U3CCloseOutputU3Ek__BackingField_6(bool value)
	{
		___U3CCloseOutputU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_7() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____dateFormatHandling_7)); }
	inline int32_t get__dateFormatHandling_7() const { return ____dateFormatHandling_7; }
	inline int32_t* get_address_of__dateFormatHandling_7() { return &____dateFormatHandling_7; }
	inline void set__dateFormatHandling_7(int32_t value)
	{
		____dateFormatHandling_7 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_8() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____dateTimeZoneHandling_8)); }
	inline int32_t get__dateTimeZoneHandling_8() const { return ____dateTimeZoneHandling_8; }
	inline int32_t* get_address_of__dateTimeZoneHandling_8() { return &____dateTimeZoneHandling_8; }
	inline void set__dateTimeZoneHandling_8(int32_t value)
	{
		____dateTimeZoneHandling_8 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_9() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____stringEscapeHandling_9)); }
	inline int32_t get__stringEscapeHandling_9() const { return ____stringEscapeHandling_9; }
	inline int32_t* get_address_of__stringEscapeHandling_9() { return &____stringEscapeHandling_9; }
	inline void set__stringEscapeHandling_9(int32_t value)
	{
		____stringEscapeHandling_9 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_10() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____floatFormatHandling_10)); }
	inline int32_t get__floatFormatHandling_10() const { return ____floatFormatHandling_10; }
	inline int32_t* get_address_of__floatFormatHandling_10() { return &____floatFormatHandling_10; }
	inline void set__floatFormatHandling_10(int32_t value)
	{
		____floatFormatHandling_10 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_11() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____dateFormatString_11)); }
	inline String_t* get__dateFormatString_11() const { return ____dateFormatString_11; }
	inline String_t** get_address_of__dateFormatString_11() { return &____dateFormatString_11; }
	inline void set__dateFormatString_11(String_t* value)
	{
		____dateFormatString_11 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_11), value);
	}

	inline static int32_t get_offset_of__culture_12() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____culture_12)); }
	inline CultureInfo_t4157843068 * get__culture_12() const { return ____culture_12; }
	inline CultureInfo_t4157843068 ** get_address_of__culture_12() { return &____culture_12; }
	inline void set__culture_12(CultureInfo_t4157843068 * value)
	{
		____culture_12 = value;
		Il2CppCodeGenWriteBarrier((&____culture_12), value);
	}
};

struct JsonWriter_t1467272295_StaticFields
{
public:
	// Newtonsoft.Json.JsonWriter/State[][] Newtonsoft.Json.JsonWriter::StateArray
	StateU5BU5DU5BU5D_t3190318925* ___StateArray_0;
	// Newtonsoft.Json.JsonWriter/State[][] Newtonsoft.Json.JsonWriter::StateArrayTempate
	StateU5BU5DU5BU5D_t3190318925* ___StateArrayTempate_1;

public:
	inline static int32_t get_offset_of_StateArray_0() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295_StaticFields, ___StateArray_0)); }
	inline StateU5BU5DU5BU5D_t3190318925* get_StateArray_0() const { return ___StateArray_0; }
	inline StateU5BU5DU5BU5D_t3190318925** get_address_of_StateArray_0() { return &___StateArray_0; }
	inline void set_StateArray_0(StateU5BU5DU5BU5D_t3190318925* value)
	{
		___StateArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___StateArray_0), value);
	}

	inline static int32_t get_offset_of_StateArrayTempate_1() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295_StaticFields, ___StateArrayTempate_1)); }
	inline StateU5BU5DU5BU5D_t3190318925* get_StateArrayTempate_1() const { return ___StateArrayTempate_1; }
	inline StateU5BU5DU5BU5D_t3190318925** get_address_of_StateArrayTempate_1() { return &___StateArrayTempate_1; }
	inline void set_StateArrayTempate_1(StateU5BU5DU5BU5D_t3190318925* value)
	{
		___StateArrayTempate_1 = value;
		Il2CppCodeGenWriteBarrier((&___StateArrayTempate_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONWRITER_T1467272295_H
#ifndef JSONSERIALIZER_T1424496335_H
#define JSONSERIALIZER_T1424496335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonSerializer
struct  JsonSerializer_t1424496335  : public RuntimeObject
{
public:
	// Newtonsoft.Json.TypeNameHandling Newtonsoft.Json.JsonSerializer::_typeNameHandling
	int32_t ____typeNameHandling_0;
	// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle Newtonsoft.Json.JsonSerializer::_typeNameAssemblyFormat
	int32_t ____typeNameAssemblyFormat_1;
	// Newtonsoft.Json.PreserveReferencesHandling Newtonsoft.Json.JsonSerializer::_preserveReferencesHandling
	int32_t ____preserveReferencesHandling_2;
	// Newtonsoft.Json.ReferenceLoopHandling Newtonsoft.Json.JsonSerializer::_referenceLoopHandling
	int32_t ____referenceLoopHandling_3;
	// Newtonsoft.Json.MissingMemberHandling Newtonsoft.Json.JsonSerializer::_missingMemberHandling
	int32_t ____missingMemberHandling_4;
	// Newtonsoft.Json.ObjectCreationHandling Newtonsoft.Json.JsonSerializer::_objectCreationHandling
	int32_t ____objectCreationHandling_5;
	// Newtonsoft.Json.NullValueHandling Newtonsoft.Json.JsonSerializer::_nullValueHandling
	int32_t ____nullValueHandling_6;
	// Newtonsoft.Json.DefaultValueHandling Newtonsoft.Json.JsonSerializer::_defaultValueHandling
	int32_t ____defaultValueHandling_7;
	// Newtonsoft.Json.ConstructorHandling Newtonsoft.Json.JsonSerializer::_constructorHandling
	int32_t ____constructorHandling_8;
	// Newtonsoft.Json.MetadataPropertyHandling Newtonsoft.Json.JsonSerializer::_metadataPropertyHandling
	int32_t ____metadataPropertyHandling_9;
	// Newtonsoft.Json.JsonConverterCollection Newtonsoft.Json.JsonSerializer::_converters
	JsonConverterCollection_t3675964822 * ____converters_10;
	// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.JsonSerializer::_contractResolver
	RuntimeObject* ____contractResolver_11;
	// Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.JsonSerializer::_traceWriter
	RuntimeObject* ____traceWriter_12;
	// System.Collections.IEqualityComparer Newtonsoft.Json.JsonSerializer::_equalityComparer
	RuntimeObject* ____equalityComparer_13;
	// System.Runtime.Serialization.SerializationBinder Newtonsoft.Json.JsonSerializer::_binder
	SerializationBinder_t274213469 * ____binder_14;
	// System.Runtime.Serialization.StreamingContext Newtonsoft.Json.JsonSerializer::_context
	StreamingContext_t3711869237  ____context_15;
	// Newtonsoft.Json.Serialization.IReferenceResolver Newtonsoft.Json.JsonSerializer::_referenceResolver
	RuntimeObject* ____referenceResolver_16;
	// System.Nullable`1<Newtonsoft.Json.Formatting> Newtonsoft.Json.JsonSerializer::_formatting
	Nullable_1_t3914607011  ____formatting_17;
	// System.Nullable`1<Newtonsoft.Json.DateFormatHandling> Newtonsoft.Json.JsonSerializer::_dateFormatHandling
	Nullable_1_t3098729937  ____dateFormatHandling_18;
	// System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling> Newtonsoft.Json.JsonSerializer::_dateTimeZoneHandling
	Nullable_1_t430194516  ____dateTimeZoneHandling_19;
	// System.Nullable`1<Newtonsoft.Json.DateParseHandling> Newtonsoft.Json.JsonSerializer::_dateParseHandling
	Nullable_1_t179296662  ____dateParseHandling_20;
	// System.Nullable`1<Newtonsoft.Json.FloatFormatHandling> Newtonsoft.Json.JsonSerializer::_floatFormatHandling
	Nullable_1_t2285469903  ____floatFormatHandling_21;
	// System.Nullable`1<Newtonsoft.Json.FloatParseHandling> Newtonsoft.Json.JsonSerializer::_floatParseHandling
	Nullable_1_t2729527740  ____floatParseHandling_22;
	// System.Nullable`1<Newtonsoft.Json.StringEscapeHandling> Newtonsoft.Json.JsonSerializer::_stringEscapeHandling
	Nullable_1_t1505470351  ____stringEscapeHandling_23;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonSerializer::_culture
	CultureInfo_t4157843068 * ____culture_24;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonSerializer::_maxDepth
	Nullable_1_t378540539  ____maxDepth_25;
	// System.Boolean Newtonsoft.Json.JsonSerializer::_maxDepthSet
	bool ____maxDepthSet_26;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonSerializer::_checkAdditionalContent
	Nullable_1_t1819850047  ____checkAdditionalContent_27;
	// System.String Newtonsoft.Json.JsonSerializer::_dateFormatString
	String_t* ____dateFormatString_28;
	// System.Boolean Newtonsoft.Json.JsonSerializer::_dateFormatStringSet
	bool ____dateFormatStringSet_29;
	// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs> Newtonsoft.Json.JsonSerializer::Error
	EventHandler_1_t2935836205 * ___Error_30;

public:
	inline static int32_t get_offset_of__typeNameHandling_0() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____typeNameHandling_0)); }
	inline int32_t get__typeNameHandling_0() const { return ____typeNameHandling_0; }
	inline int32_t* get_address_of__typeNameHandling_0() { return &____typeNameHandling_0; }
	inline void set__typeNameHandling_0(int32_t value)
	{
		____typeNameHandling_0 = value;
	}

	inline static int32_t get_offset_of__typeNameAssemblyFormat_1() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____typeNameAssemblyFormat_1)); }
	inline int32_t get__typeNameAssemblyFormat_1() const { return ____typeNameAssemblyFormat_1; }
	inline int32_t* get_address_of__typeNameAssemblyFormat_1() { return &____typeNameAssemblyFormat_1; }
	inline void set__typeNameAssemblyFormat_1(int32_t value)
	{
		____typeNameAssemblyFormat_1 = value;
	}

	inline static int32_t get_offset_of__preserveReferencesHandling_2() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____preserveReferencesHandling_2)); }
	inline int32_t get__preserveReferencesHandling_2() const { return ____preserveReferencesHandling_2; }
	inline int32_t* get_address_of__preserveReferencesHandling_2() { return &____preserveReferencesHandling_2; }
	inline void set__preserveReferencesHandling_2(int32_t value)
	{
		____preserveReferencesHandling_2 = value;
	}

	inline static int32_t get_offset_of__referenceLoopHandling_3() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____referenceLoopHandling_3)); }
	inline int32_t get__referenceLoopHandling_3() const { return ____referenceLoopHandling_3; }
	inline int32_t* get_address_of__referenceLoopHandling_3() { return &____referenceLoopHandling_3; }
	inline void set__referenceLoopHandling_3(int32_t value)
	{
		____referenceLoopHandling_3 = value;
	}

	inline static int32_t get_offset_of__missingMemberHandling_4() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____missingMemberHandling_4)); }
	inline int32_t get__missingMemberHandling_4() const { return ____missingMemberHandling_4; }
	inline int32_t* get_address_of__missingMemberHandling_4() { return &____missingMemberHandling_4; }
	inline void set__missingMemberHandling_4(int32_t value)
	{
		____missingMemberHandling_4 = value;
	}

	inline static int32_t get_offset_of__objectCreationHandling_5() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____objectCreationHandling_5)); }
	inline int32_t get__objectCreationHandling_5() const { return ____objectCreationHandling_5; }
	inline int32_t* get_address_of__objectCreationHandling_5() { return &____objectCreationHandling_5; }
	inline void set__objectCreationHandling_5(int32_t value)
	{
		____objectCreationHandling_5 = value;
	}

	inline static int32_t get_offset_of__nullValueHandling_6() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____nullValueHandling_6)); }
	inline int32_t get__nullValueHandling_6() const { return ____nullValueHandling_6; }
	inline int32_t* get_address_of__nullValueHandling_6() { return &____nullValueHandling_6; }
	inline void set__nullValueHandling_6(int32_t value)
	{
		____nullValueHandling_6 = value;
	}

	inline static int32_t get_offset_of__defaultValueHandling_7() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____defaultValueHandling_7)); }
	inline int32_t get__defaultValueHandling_7() const { return ____defaultValueHandling_7; }
	inline int32_t* get_address_of__defaultValueHandling_7() { return &____defaultValueHandling_7; }
	inline void set__defaultValueHandling_7(int32_t value)
	{
		____defaultValueHandling_7 = value;
	}

	inline static int32_t get_offset_of__constructorHandling_8() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____constructorHandling_8)); }
	inline int32_t get__constructorHandling_8() const { return ____constructorHandling_8; }
	inline int32_t* get_address_of__constructorHandling_8() { return &____constructorHandling_8; }
	inline void set__constructorHandling_8(int32_t value)
	{
		____constructorHandling_8 = value;
	}

	inline static int32_t get_offset_of__metadataPropertyHandling_9() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____metadataPropertyHandling_9)); }
	inline int32_t get__metadataPropertyHandling_9() const { return ____metadataPropertyHandling_9; }
	inline int32_t* get_address_of__metadataPropertyHandling_9() { return &____metadataPropertyHandling_9; }
	inline void set__metadataPropertyHandling_9(int32_t value)
	{
		____metadataPropertyHandling_9 = value;
	}

	inline static int32_t get_offset_of__converters_10() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____converters_10)); }
	inline JsonConverterCollection_t3675964822 * get__converters_10() const { return ____converters_10; }
	inline JsonConverterCollection_t3675964822 ** get_address_of__converters_10() { return &____converters_10; }
	inline void set__converters_10(JsonConverterCollection_t3675964822 * value)
	{
		____converters_10 = value;
		Il2CppCodeGenWriteBarrier((&____converters_10), value);
	}

	inline static int32_t get_offset_of__contractResolver_11() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____contractResolver_11)); }
	inline RuntimeObject* get__contractResolver_11() const { return ____contractResolver_11; }
	inline RuntimeObject** get_address_of__contractResolver_11() { return &____contractResolver_11; }
	inline void set__contractResolver_11(RuntimeObject* value)
	{
		____contractResolver_11 = value;
		Il2CppCodeGenWriteBarrier((&____contractResolver_11), value);
	}

	inline static int32_t get_offset_of__traceWriter_12() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____traceWriter_12)); }
	inline RuntimeObject* get__traceWriter_12() const { return ____traceWriter_12; }
	inline RuntimeObject** get_address_of__traceWriter_12() { return &____traceWriter_12; }
	inline void set__traceWriter_12(RuntimeObject* value)
	{
		____traceWriter_12 = value;
		Il2CppCodeGenWriteBarrier((&____traceWriter_12), value);
	}

	inline static int32_t get_offset_of__equalityComparer_13() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____equalityComparer_13)); }
	inline RuntimeObject* get__equalityComparer_13() const { return ____equalityComparer_13; }
	inline RuntimeObject** get_address_of__equalityComparer_13() { return &____equalityComparer_13; }
	inline void set__equalityComparer_13(RuntimeObject* value)
	{
		____equalityComparer_13 = value;
		Il2CppCodeGenWriteBarrier((&____equalityComparer_13), value);
	}

	inline static int32_t get_offset_of__binder_14() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____binder_14)); }
	inline SerializationBinder_t274213469 * get__binder_14() const { return ____binder_14; }
	inline SerializationBinder_t274213469 ** get_address_of__binder_14() { return &____binder_14; }
	inline void set__binder_14(SerializationBinder_t274213469 * value)
	{
		____binder_14 = value;
		Il2CppCodeGenWriteBarrier((&____binder_14), value);
	}

	inline static int32_t get_offset_of__context_15() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____context_15)); }
	inline StreamingContext_t3711869237  get__context_15() const { return ____context_15; }
	inline StreamingContext_t3711869237 * get_address_of__context_15() { return &____context_15; }
	inline void set__context_15(StreamingContext_t3711869237  value)
	{
		____context_15 = value;
	}

	inline static int32_t get_offset_of__referenceResolver_16() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____referenceResolver_16)); }
	inline RuntimeObject* get__referenceResolver_16() const { return ____referenceResolver_16; }
	inline RuntimeObject** get_address_of__referenceResolver_16() { return &____referenceResolver_16; }
	inline void set__referenceResolver_16(RuntimeObject* value)
	{
		____referenceResolver_16 = value;
		Il2CppCodeGenWriteBarrier((&____referenceResolver_16), value);
	}

	inline static int32_t get_offset_of__formatting_17() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____formatting_17)); }
	inline Nullable_1_t3914607011  get__formatting_17() const { return ____formatting_17; }
	inline Nullable_1_t3914607011 * get_address_of__formatting_17() { return &____formatting_17; }
	inline void set__formatting_17(Nullable_1_t3914607011  value)
	{
		____formatting_17 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_18() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____dateFormatHandling_18)); }
	inline Nullable_1_t3098729937  get__dateFormatHandling_18() const { return ____dateFormatHandling_18; }
	inline Nullable_1_t3098729937 * get_address_of__dateFormatHandling_18() { return &____dateFormatHandling_18; }
	inline void set__dateFormatHandling_18(Nullable_1_t3098729937  value)
	{
		____dateFormatHandling_18 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_19() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____dateTimeZoneHandling_19)); }
	inline Nullable_1_t430194516  get__dateTimeZoneHandling_19() const { return ____dateTimeZoneHandling_19; }
	inline Nullable_1_t430194516 * get_address_of__dateTimeZoneHandling_19() { return &____dateTimeZoneHandling_19; }
	inline void set__dateTimeZoneHandling_19(Nullable_1_t430194516  value)
	{
		____dateTimeZoneHandling_19 = value;
	}

	inline static int32_t get_offset_of__dateParseHandling_20() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____dateParseHandling_20)); }
	inline Nullable_1_t179296662  get__dateParseHandling_20() const { return ____dateParseHandling_20; }
	inline Nullable_1_t179296662 * get_address_of__dateParseHandling_20() { return &____dateParseHandling_20; }
	inline void set__dateParseHandling_20(Nullable_1_t179296662  value)
	{
		____dateParseHandling_20 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_21() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____floatFormatHandling_21)); }
	inline Nullable_1_t2285469903  get__floatFormatHandling_21() const { return ____floatFormatHandling_21; }
	inline Nullable_1_t2285469903 * get_address_of__floatFormatHandling_21() { return &____floatFormatHandling_21; }
	inline void set__floatFormatHandling_21(Nullable_1_t2285469903  value)
	{
		____floatFormatHandling_21 = value;
	}

	inline static int32_t get_offset_of__floatParseHandling_22() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____floatParseHandling_22)); }
	inline Nullable_1_t2729527740  get__floatParseHandling_22() const { return ____floatParseHandling_22; }
	inline Nullable_1_t2729527740 * get_address_of__floatParseHandling_22() { return &____floatParseHandling_22; }
	inline void set__floatParseHandling_22(Nullable_1_t2729527740  value)
	{
		____floatParseHandling_22 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_23() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____stringEscapeHandling_23)); }
	inline Nullable_1_t1505470351  get__stringEscapeHandling_23() const { return ____stringEscapeHandling_23; }
	inline Nullable_1_t1505470351 * get_address_of__stringEscapeHandling_23() { return &____stringEscapeHandling_23; }
	inline void set__stringEscapeHandling_23(Nullable_1_t1505470351  value)
	{
		____stringEscapeHandling_23 = value;
	}

	inline static int32_t get_offset_of__culture_24() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____culture_24)); }
	inline CultureInfo_t4157843068 * get__culture_24() const { return ____culture_24; }
	inline CultureInfo_t4157843068 ** get_address_of__culture_24() { return &____culture_24; }
	inline void set__culture_24(CultureInfo_t4157843068 * value)
	{
		____culture_24 = value;
		Il2CppCodeGenWriteBarrier((&____culture_24), value);
	}

	inline static int32_t get_offset_of__maxDepth_25() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____maxDepth_25)); }
	inline Nullable_1_t378540539  get__maxDepth_25() const { return ____maxDepth_25; }
	inline Nullable_1_t378540539 * get_address_of__maxDepth_25() { return &____maxDepth_25; }
	inline void set__maxDepth_25(Nullable_1_t378540539  value)
	{
		____maxDepth_25 = value;
	}

	inline static int32_t get_offset_of__maxDepthSet_26() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____maxDepthSet_26)); }
	inline bool get__maxDepthSet_26() const { return ____maxDepthSet_26; }
	inline bool* get_address_of__maxDepthSet_26() { return &____maxDepthSet_26; }
	inline void set__maxDepthSet_26(bool value)
	{
		____maxDepthSet_26 = value;
	}

	inline static int32_t get_offset_of__checkAdditionalContent_27() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____checkAdditionalContent_27)); }
	inline Nullable_1_t1819850047  get__checkAdditionalContent_27() const { return ____checkAdditionalContent_27; }
	inline Nullable_1_t1819850047 * get_address_of__checkAdditionalContent_27() { return &____checkAdditionalContent_27; }
	inline void set__checkAdditionalContent_27(Nullable_1_t1819850047  value)
	{
		____checkAdditionalContent_27 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_28() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____dateFormatString_28)); }
	inline String_t* get__dateFormatString_28() const { return ____dateFormatString_28; }
	inline String_t** get_address_of__dateFormatString_28() { return &____dateFormatString_28; }
	inline void set__dateFormatString_28(String_t* value)
	{
		____dateFormatString_28 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_28), value);
	}

	inline static int32_t get_offset_of__dateFormatStringSet_29() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ____dateFormatStringSet_29)); }
	inline bool get__dateFormatStringSet_29() const { return ____dateFormatStringSet_29; }
	inline bool* get_address_of__dateFormatStringSet_29() { return &____dateFormatStringSet_29; }
	inline void set__dateFormatStringSet_29(bool value)
	{
		____dateFormatStringSet_29 = value;
	}

	inline static int32_t get_offset_of_Error_30() { return static_cast<int32_t>(offsetof(JsonSerializer_t1424496335, ___Error_30)); }
	inline EventHandler_1_t2935836205 * get_Error_30() const { return ___Error_30; }
	inline EventHandler_1_t2935836205 ** get_address_of_Error_30() { return &___Error_30; }
	inline void set_Error_30(EventHandler_1_t2935836205 * value)
	{
		___Error_30 = value;
		Il2CppCodeGenWriteBarrier((&___Error_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZER_T1424496335_H
#ifndef JSONPRIMITIVECONTRACT_T336098342_H
#define JSONPRIMITIVECONTRACT_T336098342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonPrimitiveContract
struct  JsonPrimitiveContract_t336098342  : public JsonContract_t3715061318
{
public:
	// Newtonsoft.Json.Utilities.PrimitiveTypeCode Newtonsoft.Json.Serialization.JsonPrimitiveContract::<TypeCode>k__BackingField
	int32_t ___U3CTypeCodeU3Ek__BackingField_21;

public:
	inline static int32_t get_offset_of_U3CTypeCodeU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(JsonPrimitiveContract_t336098342, ___U3CTypeCodeU3Ek__BackingField_21)); }
	inline int32_t get_U3CTypeCodeU3Ek__BackingField_21() const { return ___U3CTypeCodeU3Ek__BackingField_21; }
	inline int32_t* get_address_of_U3CTypeCodeU3Ek__BackingField_21() { return &___U3CTypeCodeU3Ek__BackingField_21; }
	inline void set_U3CTypeCodeU3Ek__BackingField_21(int32_t value)
	{
		___U3CTypeCodeU3Ek__BackingField_21 = value;
	}
};

struct JsonPrimitiveContract_t336098342_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,Newtonsoft.Json.ReadType> Newtonsoft.Json.Serialization.JsonPrimitiveContract::ReadTypeMap
	Dictionary_2_t2785133644 * ___ReadTypeMap_22;

public:
	inline static int32_t get_offset_of_ReadTypeMap_22() { return static_cast<int32_t>(offsetof(JsonPrimitiveContract_t336098342_StaticFields, ___ReadTypeMap_22)); }
	inline Dictionary_2_t2785133644 * get_ReadTypeMap_22() const { return ___ReadTypeMap_22; }
	inline Dictionary_2_t2785133644 ** get_address_of_ReadTypeMap_22() { return &___ReadTypeMap_22; }
	inline void set_ReadTypeMap_22(Dictionary_2_t2785133644 * value)
	{
		___ReadTypeMap_22 = value;
		Il2CppCodeGenWriteBarrier((&___ReadTypeMap_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPRIMITIVECONTRACT_T336098342_H
#ifndef CREATORPROPERTYCONTEXT_T610304857_H
#define CREATORPROPERTYCONTEXT_T610304857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext
struct  CreatorPropertyContext_t610304857  : public RuntimeObject
{
public:
	// System.String Newtonsoft.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext::Name
	String_t* ___Name_0;
	// Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext::Property
	JsonProperty_t1405154176 * ___Property_1;
	// Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext::ConstructorProperty
	JsonProperty_t1405154176 * ___ConstructorProperty_2;
	// System.Nullable`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence> Newtonsoft.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext::Presence
	Nullable_1_t4213156694  ___Presence_3;
	// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext::Value
	RuntimeObject * ___Value_4;
	// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext::Used
	bool ___Used_5;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t610304857, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Property_1() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t610304857, ___Property_1)); }
	inline JsonProperty_t1405154176 * get_Property_1() const { return ___Property_1; }
	inline JsonProperty_t1405154176 ** get_address_of_Property_1() { return &___Property_1; }
	inline void set_Property_1(JsonProperty_t1405154176 * value)
	{
		___Property_1 = value;
		Il2CppCodeGenWriteBarrier((&___Property_1), value);
	}

	inline static int32_t get_offset_of_ConstructorProperty_2() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t610304857, ___ConstructorProperty_2)); }
	inline JsonProperty_t1405154176 * get_ConstructorProperty_2() const { return ___ConstructorProperty_2; }
	inline JsonProperty_t1405154176 ** get_address_of_ConstructorProperty_2() { return &___ConstructorProperty_2; }
	inline void set_ConstructorProperty_2(JsonProperty_t1405154176 * value)
	{
		___ConstructorProperty_2 = value;
		Il2CppCodeGenWriteBarrier((&___ConstructorProperty_2), value);
	}

	inline static int32_t get_offset_of_Presence_3() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t610304857, ___Presence_3)); }
	inline Nullable_1_t4213156694  get_Presence_3() const { return ___Presence_3; }
	inline Nullable_1_t4213156694 * get_address_of_Presence_3() { return &___Presence_3; }
	inline void set_Presence_3(Nullable_1_t4213156694  value)
	{
		___Presence_3 = value;
	}

	inline static int32_t get_offset_of_Value_4() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t610304857, ___Value_4)); }
	inline RuntimeObject * get_Value_4() const { return ___Value_4; }
	inline RuntimeObject ** get_address_of_Value_4() { return &___Value_4; }
	inline void set_Value_4(RuntimeObject * value)
	{
		___Value_4 = value;
		Il2CppCodeGenWriteBarrier((&___Value_4), value);
	}

	inline static int32_t get_offset_of_Used_5() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t610304857, ___Used_5)); }
	inline bool get_Used_5() const { return ___Used_5; }
	inline bool* get_address_of_Used_5() { return &___Used_5; }
	inline void set_Used_5(bool value)
	{
		___Used_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATORPROPERTYCONTEXT_T610304857_H
#ifndef COMPOSITEEXPRESSION_T1516235144_H
#define COMPOSITEEXPRESSION_T1516235144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.CompositeExpression
struct  CompositeExpression_t1516235144  : public QueryExpression_t3171385195
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JsonPath.QueryExpression> Newtonsoft.Json.Linq.JsonPath.CompositeExpression::<Expressions>k__BackingField
	List_1_t348492641 * ___U3CExpressionsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CExpressionsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CompositeExpression_t1516235144, ___U3CExpressionsU3Ek__BackingField_1)); }
	inline List_1_t348492641 * get_U3CExpressionsU3Ek__BackingField_1() const { return ___U3CExpressionsU3Ek__BackingField_1; }
	inline List_1_t348492641 ** get_address_of_U3CExpressionsU3Ek__BackingField_1() { return &___U3CExpressionsU3Ek__BackingField_1; }
	inline void set_U3CExpressionsU3Ek__BackingField_1(List_1_t348492641 * value)
	{
		___U3CExpressionsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExpressionsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOSITEEXPRESSION_T1516235144_H
#ifndef JRAW_T446372596_H
#define JRAW_T446372596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JRaw
struct  JRaw_t446372596  : public JValue_t1448862567
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JRAW_T446372596_H
#ifndef BOOLEANQUERYEXPRESSION_T4249992578_H
#define BOOLEANQUERYEXPRESSION_T4249992578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonPath.BooleanQueryExpression
struct  BooleanQueryExpression_t4249992578  : public QueryExpression_t3171385195
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JsonPath.PathFilter> Newtonsoft.Json.Linq.JsonPath.BooleanQueryExpression::<Path>k__BackingField
	List_1_t1040076091 * ___U3CPathU3Ek__BackingField_1;
	// Newtonsoft.Json.Linq.JValue Newtonsoft.Json.Linq.JsonPath.BooleanQueryExpression::<Value>k__BackingField
	JValue_t1448862567 * ___U3CValueU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BooleanQueryExpression_t4249992578, ___U3CPathU3Ek__BackingField_1)); }
	inline List_1_t1040076091 * get_U3CPathU3Ek__BackingField_1() const { return ___U3CPathU3Ek__BackingField_1; }
	inline List_1_t1040076091 ** get_address_of_U3CPathU3Ek__BackingField_1() { return &___U3CPathU3Ek__BackingField_1; }
	inline void set_U3CPathU3Ek__BackingField_1(List_1_t1040076091 * value)
	{
		___U3CPathU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BooleanQueryExpression_t4249992578, ___U3CValueU3Ek__BackingField_2)); }
	inline JValue_t1448862567 * get_U3CValueU3Ek__BackingField_2() const { return ___U3CValueU3Ek__BackingField_2; }
	inline JValue_t1448862567 ** get_address_of_U3CValueU3Ek__BackingField_2() { return &___U3CValueU3Ek__BackingField_2; }
	inline void set_U3CValueU3Ek__BackingField_2(JValue_t1448862567 * value)
	{
		___U3CValueU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEANQUERYEXPRESSION_T4249992578_H
#ifndef JTOKENREADER_T3769275704_H
#define JTOKENREADER_T3769275704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JTokenReader
struct  JTokenReader_t3769275704  : public JsonReader_t2369136700
{
public:
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenReader::_root
	JToken_t1038539247 * ____root_15;
	// System.String Newtonsoft.Json.Linq.JTokenReader::_initialPath
	String_t* ____initialPath_16;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenReader::_parent
	JToken_t1038539247 * ____parent_17;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenReader::_current
	JToken_t1038539247 * ____current_18;

public:
	inline static int32_t get_offset_of__root_15() { return static_cast<int32_t>(offsetof(JTokenReader_t3769275704, ____root_15)); }
	inline JToken_t1038539247 * get__root_15() const { return ____root_15; }
	inline JToken_t1038539247 ** get_address_of__root_15() { return &____root_15; }
	inline void set__root_15(JToken_t1038539247 * value)
	{
		____root_15 = value;
		Il2CppCodeGenWriteBarrier((&____root_15), value);
	}

	inline static int32_t get_offset_of__initialPath_16() { return static_cast<int32_t>(offsetof(JTokenReader_t3769275704, ____initialPath_16)); }
	inline String_t* get__initialPath_16() const { return ____initialPath_16; }
	inline String_t** get_address_of__initialPath_16() { return &____initialPath_16; }
	inline void set__initialPath_16(String_t* value)
	{
		____initialPath_16 = value;
		Il2CppCodeGenWriteBarrier((&____initialPath_16), value);
	}

	inline static int32_t get_offset_of__parent_17() { return static_cast<int32_t>(offsetof(JTokenReader_t3769275704, ____parent_17)); }
	inline JToken_t1038539247 * get__parent_17() const { return ____parent_17; }
	inline JToken_t1038539247 ** get_address_of__parent_17() { return &____parent_17; }
	inline void set__parent_17(JToken_t1038539247 * value)
	{
		____parent_17 = value;
		Il2CppCodeGenWriteBarrier((&____parent_17), value);
	}

	inline static int32_t get_offset_of__current_18() { return static_cast<int32_t>(offsetof(JTokenReader_t3769275704, ____current_18)); }
	inline JToken_t1038539247 * get__current_18() const { return ____current_18; }
	inline JToken_t1038539247 ** get_address_of__current_18() { return &____current_18; }
	inline void set__current_18(JToken_t1038539247 * value)
	{
		____current_18 = value;
		Il2CppCodeGenWriteBarrier((&____current_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKENREADER_T3769275704_H
#ifndef JSONSTRINGCONTRACT_T1298846671_H
#define JSONSTRINGCONTRACT_T1298846671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonStringContract
struct  JsonStringContract_t1298846671  : public JsonPrimitiveContract_t336098342
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSTRINGCONTRACT_T1298846671_H
#ifndef JSONSERIALIZERPROXY_T657065764_H
#define JSONSERIALIZERPROXY_T657065764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerProxy
struct  JsonSerializerProxy_t657065764  : public JsonSerializer_t1424496335
{
public:
	// Newtonsoft.Json.Serialization.JsonSerializerInternalReader Newtonsoft.Json.Serialization.JsonSerializerProxy::_serializerReader
	JsonSerializerInternalReader_t927352408 * ____serializerReader_31;
	// Newtonsoft.Json.Serialization.JsonSerializerInternalWriter Newtonsoft.Json.Serialization.JsonSerializerProxy::_serializerWriter
	JsonSerializerInternalWriter_t3588727337 * ____serializerWriter_32;
	// Newtonsoft.Json.JsonSerializer Newtonsoft.Json.Serialization.JsonSerializerProxy::_serializer
	JsonSerializer_t1424496335 * ____serializer_33;

public:
	inline static int32_t get_offset_of__serializerReader_31() { return static_cast<int32_t>(offsetof(JsonSerializerProxy_t657065764, ____serializerReader_31)); }
	inline JsonSerializerInternalReader_t927352408 * get__serializerReader_31() const { return ____serializerReader_31; }
	inline JsonSerializerInternalReader_t927352408 ** get_address_of__serializerReader_31() { return &____serializerReader_31; }
	inline void set__serializerReader_31(JsonSerializerInternalReader_t927352408 * value)
	{
		____serializerReader_31 = value;
		Il2CppCodeGenWriteBarrier((&____serializerReader_31), value);
	}

	inline static int32_t get_offset_of__serializerWriter_32() { return static_cast<int32_t>(offsetof(JsonSerializerProxy_t657065764, ____serializerWriter_32)); }
	inline JsonSerializerInternalWriter_t3588727337 * get__serializerWriter_32() const { return ____serializerWriter_32; }
	inline JsonSerializerInternalWriter_t3588727337 ** get_address_of__serializerWriter_32() { return &____serializerWriter_32; }
	inline void set__serializerWriter_32(JsonSerializerInternalWriter_t3588727337 * value)
	{
		____serializerWriter_32 = value;
		Il2CppCodeGenWriteBarrier((&____serializerWriter_32), value);
	}

	inline static int32_t get_offset_of__serializer_33() { return static_cast<int32_t>(offsetof(JsonSerializerProxy_t657065764, ____serializer_33)); }
	inline JsonSerializer_t1424496335 * get__serializer_33() const { return ____serializer_33; }
	inline JsonSerializer_t1424496335 ** get_address_of__serializer_33() { return &____serializer_33; }
	inline void set__serializer_33(JsonSerializer_t1424496335 * value)
	{
		____serializer_33 = value;
		Il2CppCodeGenWriteBarrier((&____serializer_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZERPROXY_T657065764_H
#ifndef JTOKENWRITER_T671501819_H
#define JTOKENWRITER_T671501819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JTokenWriter
struct  JTokenWriter_t671501819  : public JsonWriter_t1467272295
{
public:
	// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JTokenWriter::_token
	JContainer_t1023664833 * ____token_13;
	// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JTokenWriter::_parent
	JContainer_t1023664833 * ____parent_14;
	// Newtonsoft.Json.Linq.JValue Newtonsoft.Json.Linq.JTokenWriter::_value
	JValue_t1448862567 * ____value_15;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenWriter::_current
	JToken_t1038539247 * ____current_16;

public:
	inline static int32_t get_offset_of__token_13() { return static_cast<int32_t>(offsetof(JTokenWriter_t671501819, ____token_13)); }
	inline JContainer_t1023664833 * get__token_13() const { return ____token_13; }
	inline JContainer_t1023664833 ** get_address_of__token_13() { return &____token_13; }
	inline void set__token_13(JContainer_t1023664833 * value)
	{
		____token_13 = value;
		Il2CppCodeGenWriteBarrier((&____token_13), value);
	}

	inline static int32_t get_offset_of__parent_14() { return static_cast<int32_t>(offsetof(JTokenWriter_t671501819, ____parent_14)); }
	inline JContainer_t1023664833 * get__parent_14() const { return ____parent_14; }
	inline JContainer_t1023664833 ** get_address_of__parent_14() { return &____parent_14; }
	inline void set__parent_14(JContainer_t1023664833 * value)
	{
		____parent_14 = value;
		Il2CppCodeGenWriteBarrier((&____parent_14), value);
	}

	inline static int32_t get_offset_of__value_15() { return static_cast<int32_t>(offsetof(JTokenWriter_t671501819, ____value_15)); }
	inline JValue_t1448862567 * get__value_15() const { return ____value_15; }
	inline JValue_t1448862567 ** get_address_of__value_15() { return &____value_15; }
	inline void set__value_15(JValue_t1448862567 * value)
	{
		____value_15 = value;
		Il2CppCodeGenWriteBarrier((&____value_15), value);
	}

	inline static int32_t get_offset_of__current_16() { return static_cast<int32_t>(offsetof(JTokenWriter_t671501819, ____current_16)); }
	inline JToken_t1038539247 * get__current_16() const { return ____current_16; }
	inline JToken_t1038539247 ** get_address_of__current_16() { return &____current_16; }
	inline void set__current_16(JToken_t1038539247 * value)
	{
		____current_16 = value;
		Il2CppCodeGenWriteBarrier((&____current_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKENWRITER_T671501819_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3200 = { sizeof (CreatorPropertyContext_t610304857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3200[6] = 
{
	CreatorPropertyContext_t610304857::get_offset_of_Name_0(),
	CreatorPropertyContext_t610304857::get_offset_of_Property_1(),
	CreatorPropertyContext_t610304857::get_offset_of_ConstructorProperty_2(),
	CreatorPropertyContext_t610304857::get_offset_of_Presence_3(),
	CreatorPropertyContext_t610304857::get_offset_of_Value_4(),
	CreatorPropertyContext_t610304857::get_offset_of_Used_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3201 = { sizeof (U3CU3Ec__DisplayClass36_0_t358886780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3201[1] = 
{
	U3CU3Ec__DisplayClass36_0_t358886780::get_offset_of_property_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3202 = { sizeof (U3CU3Ec_t4156374721), -1, sizeof(U3CU3Ec_t4156374721_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3202[5] = 
{
	U3CU3Ec_t4156374721_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4156374721_StaticFields::get_offset_of_U3CU3E9__36_0_1(),
	U3CU3Ec_t4156374721_StaticFields::get_offset_of_U3CU3E9__36_2_2(),
	U3CU3Ec_t4156374721_StaticFields::get_offset_of_U3CU3E9__41_0_3(),
	U3CU3Ec_t4156374721_StaticFields::get_offset_of_U3CU3E9__41_1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3203 = { sizeof (JsonSerializerInternalWriter_t3588727337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3203[3] = 
{
	JsonSerializerInternalWriter_t3588727337::get_offset_of__rootType_5(),
	JsonSerializerInternalWriter_t3588727337::get_offset_of__rootLevel_6(),
	JsonSerializerInternalWriter_t3588727337::get_offset_of__serializeStack_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3204 = { sizeof (JsonSerializerProxy_t657065764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3204[3] = 
{
	JsonSerializerProxy_t657065764::get_offset_of__serializerReader_31(),
	JsonSerializerProxy_t657065764::get_offset_of__serializerWriter_32(),
	JsonSerializerProxy_t657065764::get_offset_of__serializer_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3205 = { sizeof (JsonStringContract_t1298846671), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3206 = { sizeof (JsonTypeReflector_t526591219), -1, sizeof(JsonTypeReflector_t526591219_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3206[4] = 
{
	JsonTypeReflector_t526591219_StaticFields::get_offset_of__fullyTrusted_0(),
	JsonTypeReflector_t526591219_StaticFields::get_offset_of_JsonConverterCreatorCache_1(),
	JsonTypeReflector_t526591219_StaticFields::get_offset_of_AssociatedMetadataTypesCache_2(),
	JsonTypeReflector_t526591219_StaticFields::get_offset_of__metadataTypeAttributeReflectionObject_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3207 = { sizeof (U3CU3Ec__DisplayClass18_0_t667614246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3207[2] = 
{
	U3CU3Ec__DisplayClass18_0_t667614246::get_offset_of_converterType_0(),
	U3CU3Ec__DisplayClass18_0_t667614246::get_offset_of_defaultConstructor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3208 = { sizeof (U3CU3Ec_t751952102), -1, sizeof(U3CU3Ec_t751952102_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3208[2] = 
{
	U3CU3Ec_t751952102_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t751952102_StaticFields::get_offset_of_U3CU3E9__18_1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3209 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3209[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3210 = { sizeof (ReflectionValueProvider_t2127813129), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3210[1] = 
{
	ReflectionValueProvider_t2127813129::get_offset_of__memberInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3211 = { sizeof (OnErrorAttribute_t1580718334), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3212 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3213 = { sizeof (CommentHandling_t2931237307)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3213[3] = 
{
	CommentHandling_t2931237307::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3214 = { sizeof (LineInfoHandling_t588682358)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3214[3] = 
{
	LineInfoHandling_t588682358::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3215 = { sizeof (JPropertyDescriptor_t1462014386), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3216 = { sizeof (JPropertyKeyedCollection_t1521123921), -1, sizeof(JPropertyKeyedCollection_t1521123921_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3216[2] = 
{
	JPropertyKeyedCollection_t1521123921_StaticFields::get_offset_of_Comparer_2(),
	JPropertyKeyedCollection_t1521123921::get_offset_of__dictionary_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3217 = { sizeof (JsonLoadSettings_t878621609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3217[2] = 
{
	JsonLoadSettings_t878621609::get_offset_of__commentHandling_0(),
	JsonLoadSettings_t878621609::get_offset_of__lineInfoHandling_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3218 = { sizeof (JsonMergeSettings_t3341555814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3219 = { sizeof (MergeArrayHandling_t691603146)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3219[5] = 
{
	MergeArrayHandling_t691603146::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3220 = { sizeof (MergeNullValueHandling_t2760645705)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3220[3] = 
{
	MergeNullValueHandling_t2760645705::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3221 = { sizeof (JRaw_t446372596), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3222 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3223 = { sizeof (JTokenEqualityComparer_t2051491032), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3224 = { sizeof (Extensions_t2945510795), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3225 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3225[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3226 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3226[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3227 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3227[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3228 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3228[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3229 = { sizeof (U3CU3Ec_t1720245494), -1, sizeof(U3CU3Ec_t1720245494_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3229[1] = 
{
	U3CU3Ec_t1720245494_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3230 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3230[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3231 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3231[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3232 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3232[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3233 = { sizeof (JConstructor_t3659803310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3233[2] = 
{
	JConstructor_t3659803310::get_offset_of__name_16(),
	JConstructor_t3659803310::get_offset_of__values_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3234 = { sizeof (JContainer_t1023664833), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3234[3] = 
{
	JContainer_t1023664833::get_offset_of__listChanged_13(),
	JContainer_t1023664833::get_offset_of__syncRoot_14(),
	JContainer_t1023664833::get_offset_of__busy_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3235 = { sizeof (U3CGetDescendantsU3Ed__29_t2717699431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3235[11] = 
{
	U3CGetDescendantsU3Ed__29_t2717699431::get_offset_of_U3CU3E1__state_0(),
	U3CGetDescendantsU3Ed__29_t2717699431::get_offset_of_U3CU3E2__current_1(),
	U3CGetDescendantsU3Ed__29_t2717699431::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetDescendantsU3Ed__29_t2717699431::get_offset_of_self_3(),
	U3CGetDescendantsU3Ed__29_t2717699431::get_offset_of_U3CU3E3__self_4(),
	U3CGetDescendantsU3Ed__29_t2717699431::get_offset_of_U3CU3E4__this_5(),
	U3CGetDescendantsU3Ed__29_t2717699431::get_offset_of_U3CU3Es__1_6(),
	U3CGetDescendantsU3Ed__29_t2717699431::get_offset_of_U3CoU3E5__2_7(),
	U3CGetDescendantsU3Ed__29_t2717699431::get_offset_of_U3CcU3E5__3_8(),
	U3CGetDescendantsU3Ed__29_t2717699431::get_offset_of_U3CU3Es__4_9(),
	U3CGetDescendantsU3Ed__29_t2717699431::get_offset_of_U3CdU3E5__5_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3236 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3236[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3237 = { sizeof (JObject_t2059125928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3237[3] = 
{
	JObject_t2059125928::get_offset_of__properties_16(),
	JObject_t2059125928::get_offset_of_PropertyChanged_17(),
	JObject_t2059125928::get_offset_of_PropertyChanging_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3238 = { sizeof (U3CU3Ec_t3140449829), -1, sizeof(U3CU3Ec_t3140449829_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3238[1] = 
{
	U3CU3Ec_t3140449829_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3239 = { sizeof (U3CGetEnumeratorU3Ed__58_t2686635841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3239[5] = 
{
	U3CGetEnumeratorU3Ed__58_t2686635841::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__58_t2686635841::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__58_t2686635841::get_offset_of_U3CU3E4__this_2(),
	U3CGetEnumeratorU3Ed__58_t2686635841::get_offset_of_U3CU3Es__1_3(),
	U3CGetEnumeratorU3Ed__58_t2686635841::get_offset_of_U3CpropertyU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3240 = { sizeof (JArray_t2963978544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3240[1] = 
{
	JArray_t2963978544::get_offset_of__values_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3241 = { sizeof (JTokenReader_t3769275704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3241[4] = 
{
	JTokenReader_t3769275704::get_offset_of__root_15(),
	JTokenReader_t3769275704::get_offset_of__initialPath_16(),
	JTokenReader_t3769275704::get_offset_of__parent_17(),
	JTokenReader_t3769275704::get_offset_of__current_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3242 = { sizeof (JTokenWriter_t671501819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3242[4] = 
{
	JTokenWriter_t671501819::get_offset_of__token_13(),
	JTokenWriter_t671501819::get_offset_of__parent_14(),
	JTokenWriter_t671501819::get_offset_of__value_15(),
	JTokenWriter_t671501819::get_offset_of__current_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3243 = { sizeof (JToken_t1038539247), -1, sizeof(JToken_t1038539247_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3243[13] = 
{
	JToken_t1038539247::get_offset_of__parent_0(),
	JToken_t1038539247::get_offset_of__previous_1(),
	JToken_t1038539247::get_offset_of__next_2(),
	JToken_t1038539247::get_offset_of__annotations_3(),
	JToken_t1038539247_StaticFields::get_offset_of_BooleanTypes_4(),
	JToken_t1038539247_StaticFields::get_offset_of_NumberTypes_5(),
	JToken_t1038539247_StaticFields::get_offset_of_StringTypes_6(),
	JToken_t1038539247_StaticFields::get_offset_of_GuidTypes_7(),
	JToken_t1038539247_StaticFields::get_offset_of_TimeSpanTypes_8(),
	JToken_t1038539247_StaticFields::get_offset_of_UriTypes_9(),
	JToken_t1038539247_StaticFields::get_offset_of_CharTypes_10(),
	JToken_t1038539247_StaticFields::get_offset_of_DateTimeTypes_11(),
	JToken_t1038539247_StaticFields::get_offset_of_BytesTypes_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3244 = { sizeof (LineInfoAnnotation_t2730592426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3244[2] = 
{
	LineInfoAnnotation_t2730592426::get_offset_of_LineNumber_0(),
	LineInfoAnnotation_t2730592426::get_offset_of_LinePosition_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3245 = { sizeof (U3CGetAncestorsU3Ed__41_t2485884922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3245[7] = 
{
	U3CGetAncestorsU3Ed__41_t2485884922::get_offset_of_U3CU3E1__state_0(),
	U3CGetAncestorsU3Ed__41_t2485884922::get_offset_of_U3CU3E2__current_1(),
	U3CGetAncestorsU3Ed__41_t2485884922::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetAncestorsU3Ed__41_t2485884922::get_offset_of_self_3(),
	U3CGetAncestorsU3Ed__41_t2485884922::get_offset_of_U3CU3E3__self_4(),
	U3CGetAncestorsU3Ed__41_t2485884922::get_offset_of_U3CU3E4__this_5(),
	U3CGetAncestorsU3Ed__41_t2485884922::get_offset_of_U3CcurrentU3E5__1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3246 = { sizeof (U3CAfterSelfU3Ed__42_t883970034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3246[5] = 
{
	U3CAfterSelfU3Ed__42_t883970034::get_offset_of_U3CU3E1__state_0(),
	U3CAfterSelfU3Ed__42_t883970034::get_offset_of_U3CU3E2__current_1(),
	U3CAfterSelfU3Ed__42_t883970034::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CAfterSelfU3Ed__42_t883970034::get_offset_of_U3CU3E4__this_3(),
	U3CAfterSelfU3Ed__42_t883970034::get_offset_of_U3CoU3E5__1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3247 = { sizeof (U3CBeforeSelfU3Ed__43_t2375137862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3247[5] = 
{
	U3CBeforeSelfU3Ed__43_t2375137862::get_offset_of_U3CU3E1__state_0(),
	U3CBeforeSelfU3Ed__43_t2375137862::get_offset_of_U3CU3E2__current_1(),
	U3CBeforeSelfU3Ed__43_t2375137862::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CBeforeSelfU3Ed__43_t2375137862::get_offset_of_U3CU3E4__this_3(),
	U3CBeforeSelfU3Ed__43_t2375137862::get_offset_of_U3CoU3E5__1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3248 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3248[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3249 = { sizeof (U3CAnnotationsU3Ed__172_t3494334042), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3249[9] = 
{
	U3CAnnotationsU3Ed__172_t3494334042::get_offset_of_U3CU3E1__state_0(),
	U3CAnnotationsU3Ed__172_t3494334042::get_offset_of_U3CU3E2__current_1(),
	U3CAnnotationsU3Ed__172_t3494334042::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CAnnotationsU3Ed__172_t3494334042::get_offset_of_type_3(),
	U3CAnnotationsU3Ed__172_t3494334042::get_offset_of_U3CU3E3__type_4(),
	U3CAnnotationsU3Ed__172_t3494334042::get_offset_of_U3CU3E4__this_5(),
	U3CAnnotationsU3Ed__172_t3494334042::get_offset_of_U3CannotationsU3E5__1_6(),
	U3CAnnotationsU3Ed__172_t3494334042::get_offset_of_U3CiU3E5__2_7(),
	U3CAnnotationsU3Ed__172_t3494334042::get_offset_of_U3CoU3E5__3_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3250 = { sizeof (JProperty_t3803048347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3250[2] = 
{
	JProperty_t3803048347::get_offset_of__content_16(),
	JProperty_t3803048347::get_offset_of__name_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3251 = { sizeof (JPropertyList_t1362261004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3251[1] = 
{
	JPropertyList_t1362261004::get_offset_of__token_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3252 = { sizeof (U3CGetEnumeratorU3Ed__1_t1232179978), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3252[3] = 
{
	U3CGetEnumeratorU3Ed__1_t1232179978::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__1_t1232179978::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__1_t1232179978::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3253 = { sizeof (JTokenType_t3022361662)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3253[19] = 
{
	JTokenType_t3022361662::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3254 = { sizeof (JValue_t1448862567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3254[2] = 
{
	JValue_t1448862567::get_offset_of__valueType_13(),
	JValue_t1448862567::get_offset_of__value_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3255 = { sizeof (ArrayIndexFilter_t2739218971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3255[1] = 
{
	ArrayIndexFilter_t2739218971::get_offset_of_U3CIndexU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3256 = { sizeof (U3CExecuteFilterU3Ed__4_t508957393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3256[13] = 
{
	U3CExecuteFilterU3Ed__4_t508957393::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__4_t508957393::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__4_t508957393::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__4_t508957393::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__4_t508957393::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__4_t508957393::get_offset_of_errorWhenNoMatch_5(),
	U3CExecuteFilterU3Ed__4_t508957393::get_offset_of_U3CU3E3__errorWhenNoMatch_6(),
	U3CExecuteFilterU3Ed__4_t508957393::get_offset_of_U3CU3E4__this_7(),
	U3CExecuteFilterU3Ed__4_t508957393::get_offset_of_U3CU3Es__1_8(),
	U3CExecuteFilterU3Ed__4_t508957393::get_offset_of_U3CtU3E5__2_9(),
	U3CExecuteFilterU3Ed__4_t508957393::get_offset_of_U3CvU3E5__3_10(),
	U3CExecuteFilterU3Ed__4_t508957393::get_offset_of_U3CU3Es__4_11(),
	U3CExecuteFilterU3Ed__4_t508957393::get_offset_of_U3CvU3E5__5_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3257 = { sizeof (ArrayMultipleIndexFilter_t2913121614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3257[1] = 
{
	ArrayMultipleIndexFilter_t2913121614::get_offset_of_U3CIndexesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3258 = { sizeof (U3CExecuteFilterU3Ed__4_t1113823742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3258[13] = 
{
	U3CExecuteFilterU3Ed__4_t1113823742::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__4_t1113823742::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__4_t1113823742::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__4_t1113823742::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__4_t1113823742::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__4_t1113823742::get_offset_of_errorWhenNoMatch_5(),
	U3CExecuteFilterU3Ed__4_t1113823742::get_offset_of_U3CU3E3__errorWhenNoMatch_6(),
	U3CExecuteFilterU3Ed__4_t1113823742::get_offset_of_U3CU3E4__this_7(),
	U3CExecuteFilterU3Ed__4_t1113823742::get_offset_of_U3CU3Es__1_8(),
	U3CExecuteFilterU3Ed__4_t1113823742::get_offset_of_U3CtU3E5__2_9(),
	U3CExecuteFilterU3Ed__4_t1113823742::get_offset_of_U3CU3Es__3_10(),
	U3CExecuteFilterU3Ed__4_t1113823742::get_offset_of_U3CiU3E5__4_11(),
	U3CExecuteFilterU3Ed__4_t1113823742::get_offset_of_U3CvU3E5__5_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3259 = { sizeof (ArraySliceFilter_t662461599), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3259[3] = 
{
	ArraySliceFilter_t662461599::get_offset_of_U3CStartU3Ek__BackingField_0(),
	ArraySliceFilter_t662461599::get_offset_of_U3CEndU3Ek__BackingField_1(),
	ArraySliceFilter_t662461599::get_offset_of_U3CStepU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3260 = { sizeof (U3CExecuteFilterU3Ed__12_t222072922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3260[16] = 
{
	U3CExecuteFilterU3Ed__12_t222072922::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__12_t222072922::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__12_t222072922::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__12_t222072922::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__12_t222072922::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__12_t222072922::get_offset_of_errorWhenNoMatch_5(),
	U3CExecuteFilterU3Ed__12_t222072922::get_offset_of_U3CU3E3__errorWhenNoMatch_6(),
	U3CExecuteFilterU3Ed__12_t222072922::get_offset_of_U3CU3E4__this_7(),
	U3CExecuteFilterU3Ed__12_t222072922::get_offset_of_U3CU3Es__1_8(),
	U3CExecuteFilterU3Ed__12_t222072922::get_offset_of_U3CtU3E5__2_9(),
	U3CExecuteFilterU3Ed__12_t222072922::get_offset_of_U3CaU3E5__3_10(),
	U3CExecuteFilterU3Ed__12_t222072922::get_offset_of_U3CstepCountU3E5__4_11(),
	U3CExecuteFilterU3Ed__12_t222072922::get_offset_of_U3CstartIndexU3E5__5_12(),
	U3CExecuteFilterU3Ed__12_t222072922::get_offset_of_U3CstopIndexU3E5__6_13(),
	U3CExecuteFilterU3Ed__12_t222072922::get_offset_of_U3CpositiveStepU3E5__7_14(),
	U3CExecuteFilterU3Ed__12_t222072922::get_offset_of_U3CiU3E5__8_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3261 = { sizeof (FieldFilter_t3654920868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3261[1] = 
{
	FieldFilter_t3654920868::get_offset_of_U3CNameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3262 = { sizeof (U3CExecuteFilterU3Ed__4_t2686066276), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3262[14] = 
{
	U3CExecuteFilterU3Ed__4_t2686066276::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__4_t2686066276::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__4_t2686066276::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__4_t2686066276::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__4_t2686066276::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__4_t2686066276::get_offset_of_errorWhenNoMatch_5(),
	U3CExecuteFilterU3Ed__4_t2686066276::get_offset_of_U3CU3E3__errorWhenNoMatch_6(),
	U3CExecuteFilterU3Ed__4_t2686066276::get_offset_of_U3CU3E4__this_7(),
	U3CExecuteFilterU3Ed__4_t2686066276::get_offset_of_U3CU3Es__1_8(),
	U3CExecuteFilterU3Ed__4_t2686066276::get_offset_of_U3CtU3E5__2_9(),
	U3CExecuteFilterU3Ed__4_t2686066276::get_offset_of_U3CoU3E5__3_10(),
	U3CExecuteFilterU3Ed__4_t2686066276::get_offset_of_U3CvU3E5__4_11(),
	U3CExecuteFilterU3Ed__4_t2686066276::get_offset_of_U3CU3Es__5_12(),
	U3CExecuteFilterU3Ed__4_t2686066276::get_offset_of_U3CpU3E5__6_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3263 = { sizeof (FieldMultipleFilter_t3452751328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3263[1] = 
{
	FieldMultipleFilter_t3452751328::get_offset_of_U3CNamesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3264 = { sizeof (U3CU3Ec_t266907836), -1, sizeof(U3CU3Ec_t266907836_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3264[2] = 
{
	U3CU3Ec_t266907836_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t266907836_StaticFields::get_offset_of_U3CU3E9__4_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3265 = { sizeof (U3CExecuteFilterU3Ed__4_t32591816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3265[14] = 
{
	U3CExecuteFilterU3Ed__4_t32591816::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__4_t32591816::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__4_t32591816::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__4_t32591816::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__4_t32591816::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__4_t32591816::get_offset_of_errorWhenNoMatch_5(),
	U3CExecuteFilterU3Ed__4_t32591816::get_offset_of_U3CU3E3__errorWhenNoMatch_6(),
	U3CExecuteFilterU3Ed__4_t32591816::get_offset_of_U3CU3E4__this_7(),
	U3CExecuteFilterU3Ed__4_t32591816::get_offset_of_U3CU3Es__1_8(),
	U3CExecuteFilterU3Ed__4_t32591816::get_offset_of_U3CtU3E5__2_9(),
	U3CExecuteFilterU3Ed__4_t32591816::get_offset_of_U3CoU3E5__3_10(),
	U3CExecuteFilterU3Ed__4_t32591816::get_offset_of_U3CU3Es__4_11(),
	U3CExecuteFilterU3Ed__4_t32591816::get_offset_of_U3CnameU3E5__5_12(),
	U3CExecuteFilterU3Ed__4_t32591816::get_offset_of_U3CvU3E5__6_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3266 = { sizeof (JPath_t4294290739), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3267 = { sizeof (PathFilter_t3862968645), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3268 = { sizeof (QueryOperator_t2966169422)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3268[11] = 
{
	QueryOperator_t2966169422::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3269 = { sizeof (QueryExpression_t3171385195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3269[1] = 
{
	QueryExpression_t3171385195::get_offset_of_U3COperatorU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3270 = { sizeof (CompositeExpression_t1516235144), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3270[1] = 
{
	CompositeExpression_t1516235144::get_offset_of_U3CExpressionsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3271 = { sizeof (BooleanQueryExpression_t4249992578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3271[2] = 
{
	BooleanQueryExpression_t4249992578::get_offset_of_U3CPathU3Ek__BackingField_1(),
	BooleanQueryExpression_t4249992578::get_offset_of_U3CValueU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3272 = { sizeof (QueryFilter_t1584032927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3272[1] = 
{
	QueryFilter_t1584032927::get_offset_of_U3CExpressionU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3273 = { sizeof (U3CExecuteFilterU3Ed__4_t2672090193), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3273[12] = 
{
	U3CExecuteFilterU3Ed__4_t2672090193::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__4_t2672090193::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__4_t2672090193::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__4_t2672090193::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__4_t2672090193::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__4_t2672090193::get_offset_of_errorWhenNoMatch_5(),
	U3CExecuteFilterU3Ed__4_t2672090193::get_offset_of_U3CU3E3__errorWhenNoMatch_6(),
	U3CExecuteFilterU3Ed__4_t2672090193::get_offset_of_U3CU3E4__this_7(),
	U3CExecuteFilterU3Ed__4_t2672090193::get_offset_of_U3CU3Es__1_8(),
	U3CExecuteFilterU3Ed__4_t2672090193::get_offset_of_U3CtU3E5__2_9(),
	U3CExecuteFilterU3Ed__4_t2672090193::get_offset_of_U3CU3Es__3_10(),
	U3CExecuteFilterU3Ed__4_t2672090193::get_offset_of_U3CvU3E5__4_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3274 = { sizeof (ScanFilter_t1566003751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3274[1] = 
{
	ScanFilter_t1566003751::get_offset_of_U3CNameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3275 = { sizeof (U3CExecuteFilterU3Ed__4_t3421602345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3275[13] = 
{
	U3CExecuteFilterU3Ed__4_t3421602345::get_offset_of_U3CU3E1__state_0(),
	U3CExecuteFilterU3Ed__4_t3421602345::get_offset_of_U3CU3E2__current_1(),
	U3CExecuteFilterU3Ed__4_t3421602345::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CExecuteFilterU3Ed__4_t3421602345::get_offset_of_current_3(),
	U3CExecuteFilterU3Ed__4_t3421602345::get_offset_of_U3CU3E3__current_4(),
	U3CExecuteFilterU3Ed__4_t3421602345::get_offset_of_errorWhenNoMatch_5(),
	U3CExecuteFilterU3Ed__4_t3421602345::get_offset_of_U3CU3E3__errorWhenNoMatch_6(),
	U3CExecuteFilterU3Ed__4_t3421602345::get_offset_of_U3CU3E4__this_7(),
	U3CExecuteFilterU3Ed__4_t3421602345::get_offset_of_U3CU3Es__1_8(),
	U3CExecuteFilterU3Ed__4_t3421602345::get_offset_of_U3CrootU3E5__2_9(),
	U3CExecuteFilterU3Ed__4_t3421602345::get_offset_of_U3CvalueU3E5__3_10(),
	U3CExecuteFilterU3Ed__4_t3421602345::get_offset_of_U3CcontainerU3E5__4_11(),
	U3CExecuteFilterU3Ed__4_t3421602345::get_offset_of_U3CeU3E5__5_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3276 = { sizeof (BinaryConverter_t779619639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3276[1] = 
{
	BinaryConverter_t779619639::get_offset_of__reflectionObject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3277 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3278 = { sizeof (DateTimeConverterBase_t4233217353), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3279 = { sizeof (HashSetConverter_t587720113), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3280 = { sizeof (KeyValuePairConverter_t2108458033), -1, sizeof(KeyValuePairConverter_t2108458033_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3280[1] = 
{
	KeyValuePairConverter_t2108458033_StaticFields::get_offset_of_ReflectionObjectPerType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3281 = { sizeof (BsonObjectIdConverter_t1190033754), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3282 = { sizeof (RegexConverter_t2517515405), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3283 = { sizeof (StringEnumConverter_t3708940030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3283[2] = 
{
	StringEnumConverter_t3708940030::get_offset_of_U3CCamelCaseTextU3Ek__BackingField_0(),
	StringEnumConverter_t3708940030::get_offset_of_U3CAllowIntegerValuesU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3284 = { sizeof (VectorConverter_t1294738057), -1, sizeof(VectorConverter_t1294738057_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3284[6] = 
{
	VectorConverter_t1294738057_StaticFields::get_offset_of_V2_0(),
	VectorConverter_t1294738057_StaticFields::get_offset_of_V3_1(),
	VectorConverter_t1294738057_StaticFields::get_offset_of_V4_2(),
	VectorConverter_t1294738057::get_offset_of_U3CEnableVector2U3Ek__BackingField_3(),
	VectorConverter_t1294738057::get_offset_of_U3CEnableVector3U3Ek__BackingField_4(),
	VectorConverter_t1294738057::get_offset_of_U3CEnableVector4U3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3285 = { sizeof (VersionConverter_t4153846029), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3286 = { sizeof (IsoDateTimeConverter_t3020114287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3286[3] = 
{
	IsoDateTimeConverter_t3020114287::get_offset_of__dateTimeStyles_0(),
	IsoDateTimeConverter_t3020114287::get_offset_of__dateTimeFormat_1(),
	IsoDateTimeConverter_t3020114287::get_offset_of__culture_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3287 = { sizeof (JavaScriptDateTimeConverter_t3307376016), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3288 = { sizeof (XmlDocumentWrapper_t3575962533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3288[1] = 
{
	XmlDocumentWrapper_t3575962533::get_offset_of__document_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3289 = { sizeof (XmlElementWrapper_t3185492177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3289[1] = 
{
	XmlElementWrapper_t3185492177::get_offset_of__element_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3290 = { sizeof (XmlDeclarationWrapper_t2867104133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3290[1] = 
{
	XmlDeclarationWrapper_t2867104133::get_offset_of__declaration_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3291 = { sizeof (XmlDocumentTypeWrapper_t2345418247), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3291[1] = 
{
	XmlDocumentTypeWrapper_t2345418247::get_offset_of__documentType_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3292 = { sizeof (XmlNodeWrapper_t3016097339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3292[3] = 
{
	XmlNodeWrapper_t3016097339::get_offset_of__node_0(),
	XmlNodeWrapper_t3016097339::get_offset_of__childNodes_1(),
	XmlNodeWrapper_t3016097339::get_offset_of__attributes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3293 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3294 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3295 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3296 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3297 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3298 = { sizeof (XDeclarationWrapper_t1986085046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3298[1] = 
{
	XDeclarationWrapper_t1986085046::get_offset_of_U3CDeclarationU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3299 = { sizeof (XDocumentTypeWrapper_t2526962505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3299[1] = 
{
	XDocumentTypeWrapper_t2526962505::get_offset_of__documentType_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
