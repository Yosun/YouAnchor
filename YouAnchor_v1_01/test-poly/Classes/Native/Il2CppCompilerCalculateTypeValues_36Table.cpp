﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// PolyToolkit.PolyAsset
struct PolyAsset_t1814153511;
// PolyToolkit.PolyFormat
struct PolyFormat_t1880249796;
// PolyToolkit.PolyApi/FetchFormatFilesCallback
struct FetchFormatFilesCallback_t2377205724;
// PolyToolkitInternal.PolyMainInternal/FetchProgressCallback
struct FetchProgressCallback_t1972555609;
// PolyToolkit.PolyApi/ListAssetsCallback
struct ListAssetsCallback_t3881593442;
// PolyToolkitInternal.PolyMainInternal
struct PolyMainInternal_t3329830951;
// PolyToolkitInternal.RawImage
struct RawImage_t3303103980;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// PolyToolkitInternal.client.model.util.WebRequestManager/<HandleWebRequest>c__Iterator0
struct U3CHandleWebRequestU3Ec__Iterator0_t3773947286;
// PolyToolkitInternal.model.export.FormatDataFile
struct FormatDataFile_t3534188782;
// System.Collections.Generic.List`1<PolyToolkitInternal.model.export.FormatDataFile>
struct List_1_t711296228;
// PolyToolkitInternal.client.model.util.WebRequestManager/CreationCallback
struct CreationCallback_t949890767;
// PolyToolkitInternal.client.model.util.WebRequestManager/CompletionCallback
struct CompletionCallback_t3367319633;
// PolyToolkit.PolyApi/GetAssetCallback
struct GetAssetCallback_t1146242383;
// System.Comparison`1<PolyToolkit.PolyAsset>
struct Comparison_1_t1589084690;
// PolyToolkit.PolyApi/ImportCallback
struct ImportCallback_t3513750376;
// PolyToolkitInternal.PolyMainInternal/FetchOperationState
struct FetchOperationState_t1520449150;
// PolyToolkitInternal.PolyMainInternal/<FetchFormatFiles>c__AnonStorey6
struct U3CFetchFormatFilesU3Ec__AnonStorey6_t1330540246;
// PolyToolkit.PolyApi/FetchThumbnailCallback
struct FetchThumbnailCallback_t3242648953;
// System.IO.Stream
struct Stream_t1273022909;
// System.Collections.Generic.List`1<UnityEngine.Texture2D>
struct List_1_t1017553631;
// PolyToolkitInternal.ImportGltf/ImportState
struct ImportState_t482765084;
// PolyToolkitInternal.IUriLoader
struct IUriLoader_t4098220267;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.IDisposable
struct IDisposable_t3640265483;
// PolyToolkitInternal.ImportGltf/GltfImportResult
struct GltfImportResult_t3877923054;
// PolyToolkitInternal.GltfMaterialConverter
struct GltfMaterialConverter_t1532388150;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Collections.Generic.IEnumerator`1<PolyToolkitInternal.GltfNodeBase>
struct IEnumerator_1_t3438337341;
// PolyToolkitInternal.GltfNodeBase
struct GltfNodeBase_t3005766873;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Collections.Generic.List`1<UnityEngine.Mesh>
struct List_1_t826071730;
// System.Collections.Generic.List`1<UnityEngine.Material>
struct List_1_t1812449865;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// UnityEngine.Material
struct Material_t340375123;
// TiltBrushToolkit.BrushDescriptor
struct BrushDescriptor_t2792727521;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.String[]
struct StringU5BU5D_t1281789340;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// PolyToolkitInternal.model.export.FormatSaveData
struct FormatSaveData_t3321373558;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t1424496335;
// System.Func`2<PolyToolkitInternal.GltfTextureBase,PolyToolkitInternal.GltfImageBase>
struct Func_2_t747370532;
// System.Func`2<PolyToolkitInternal.GltfImageBase,System.Boolean>
struct Func_2_t3992130116;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// PolyToolkitInternal.client.model.util.WebRequestManager/PendingRequest
struct PendingRequest_t652293891;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// PolyToolkitInternal.client.model.util.WebRequestManager/BufferHolder
struct BufferHolder_t424688828;
// UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t2928496527;
// PolyToolkitInternal.client.model.util.WebRequestManager
struct WebRequestManager_t2098015550;
// PolyToolkitInternal.client.model.util.WebRequestManager/<HandleWebRequest>c__Iterator0/<HandleWebRequest>c__AnonStorey1
struct U3CHandleWebRequestU3Ec__AnonStorey1_t2411719362;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// PolyToolkitInternal.GltfRootBase
struct GltfRootBase_t3065879770;
// PolyToolkitInternal.GltfSceneBase
struct GltfSceneBase_t3309998329;
// System.Collections.Generic.List`1<PolyToolkit.PolyAsset>
struct List_1_t3286228253;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// PolyToolkitInternal.GltfMeshBase
struct GltfMeshBase_t3664755873;
// PolyToolkitInternal.GltfPrimitiveBase
struct GltfPrimitiveBase_t1810376431;
// System.UInt16[]
struct UInt16U5BU5D_t3326319531;
// UnityEngine.Color32[]
struct Color32U5BU5D_t3850468773;
// PolyToolkit.PolyFile
struct PolyFile_t4211357808;
// System.Collections.Generic.List`1<PolyToolkit.PolyFile>
struct List_1_t1388465254;
// PolyToolkit.PolyFormatComplexity
struct PolyFormatComplexity_t3101848153;
// TiltBrushToolkit.BrushDescriptor[]
struct BrushDescriptorU5BU5D_t2115081084;
// System.Collections.Generic.Dictionary`2<System.Guid,TiltBrushToolkit.BrushDescriptor>
struct Dictionary_2_t1841745054;
// System.Linq.ILookup`2<System.String,TiltBrushToolkit.BrushDescriptor>
struct ILookup_2_t3928010529;
// System.Func`2<TiltBrushToolkit.BrushDescriptor,System.Guid>
struct Func_2_t1831698856;
// System.Func`2<TiltBrushToolkit.BrushDescriptor,System.String>
struct Func_2_t485616658;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// PolyToolkit.PolyStatusOr`1<PolyToolkit.PolyImportResult>
struct PolyStatusOr_1_t3732762846;
// System.Collections.Generic.List`1<PolyToolkit.PolyFormat>
struct List_1_t3352324538;
// PolyToolkit.PolyStatusOr`1<PolyToolkit.PolyAsset>
struct PolyStatusOr_1_t1175290422;
// PolyToolkit.PolyStatusOr`1<PolyToolkit.PolyListAssetsResult>
struct PolyStatusOr_1_t412004964;
// PolyToolkitInternal.PtSettings/SurfaceShaderMaterial[]
struct SurfaceShaderMaterialU5BU5D_t772937648;
// TiltBrushToolkit.BrushManifest
struct BrushManifest_t1931127838;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material>
struct Dictionary_2_t125631422;
// System.Func`2<PolyToolkitInternal.PtSettings/SurfaceShaderMaterial,System.String>
struct Func_2_t3438440518;
// System.Func`2<PolyToolkitInternal.PtSettings/SurfaceShaderMaterial,UnityEngine.Material>
struct Func_2_t1931364952;
// PolyToolkitInternal.model.util.ConcurrentQueue`1<PolyToolkitInternal.client.model.util.WebRequestManager/PendingRequest>
struct ConcurrentQueue_1_t3466100300;
// System.Collections.Generic.List`1<PolyToolkitInternal.client.model.util.WebRequestManager/BufferHolder>
struct List_1_t1896763570;
// PolyToolkitInternal.caching.PersistentBlobCache
struct PersistentBlobCache_t1212092596;
// System.Threading.Thread
struct Thread_t2300836069;
// PolyToolkitInternal.model.util.ConcurrentQueue`1<PolyToolkitInternal.model.util.BackgroundWork>
struct ConcurrentQueue_1_t133829771;
// PolyToolkitInternal.api_clients.poly_client.PolyClient
struct PolyClient_t2759672288;
// PolyToolkitInternal.AsyncImporter
struct AsyncImporter_t2267690090;
// PolyToolkitInternal.model.util.BackgroundMain
struct BackgroundMain_t2789389941;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef POLYINTERNALUTILS_T2048710808_H
#define POLYINTERNALUTILS_T2048710808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.PolyInternalUtils
struct  PolyInternalUtils_t2048710808  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYINTERNALUTILS_T2048710808_H
#ifndef FETCHOPERATIONSTATE_T1520449150_H
#define FETCHOPERATIONSTATE_T1520449150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.PolyMainInternal/FetchOperationState
struct  FetchOperationState_t1520449150  : public RuntimeObject
{
public:
	// System.Int32 PolyToolkitInternal.PolyMainInternal/FetchOperationState::totalFiles
	int32_t ___totalFiles_0;
	// System.Int32 PolyToolkitInternal.PolyMainInternal/FetchOperationState::pendingFiles
	int32_t ___pendingFiles_1;
	// PolyToolkit.PolyAsset PolyToolkitInternal.PolyMainInternal/FetchOperationState::asset
	PolyAsset_t1814153511 * ___asset_2;
	// PolyToolkit.PolyFormat PolyToolkitInternal.PolyMainInternal/FetchOperationState::packageBeingFetched
	PolyFormat_t1880249796 * ___packageBeingFetched_3;
	// PolyToolkit.PolyApi/FetchFormatFilesCallback PolyToolkitInternal.PolyMainInternal/FetchOperationState::completionCallback
	FetchFormatFilesCallback_t2377205724 * ___completionCallback_4;
	// PolyToolkitInternal.PolyMainInternal/FetchProgressCallback PolyToolkitInternal.PolyMainInternal/FetchOperationState::progressCallback
	FetchProgressCallback_t1972555609 * ___progressCallback_5;

public:
	inline static int32_t get_offset_of_totalFiles_0() { return static_cast<int32_t>(offsetof(FetchOperationState_t1520449150, ___totalFiles_0)); }
	inline int32_t get_totalFiles_0() const { return ___totalFiles_0; }
	inline int32_t* get_address_of_totalFiles_0() { return &___totalFiles_0; }
	inline void set_totalFiles_0(int32_t value)
	{
		___totalFiles_0 = value;
	}

	inline static int32_t get_offset_of_pendingFiles_1() { return static_cast<int32_t>(offsetof(FetchOperationState_t1520449150, ___pendingFiles_1)); }
	inline int32_t get_pendingFiles_1() const { return ___pendingFiles_1; }
	inline int32_t* get_address_of_pendingFiles_1() { return &___pendingFiles_1; }
	inline void set_pendingFiles_1(int32_t value)
	{
		___pendingFiles_1 = value;
	}

	inline static int32_t get_offset_of_asset_2() { return static_cast<int32_t>(offsetof(FetchOperationState_t1520449150, ___asset_2)); }
	inline PolyAsset_t1814153511 * get_asset_2() const { return ___asset_2; }
	inline PolyAsset_t1814153511 ** get_address_of_asset_2() { return &___asset_2; }
	inline void set_asset_2(PolyAsset_t1814153511 * value)
	{
		___asset_2 = value;
		Il2CppCodeGenWriteBarrier((&___asset_2), value);
	}

	inline static int32_t get_offset_of_packageBeingFetched_3() { return static_cast<int32_t>(offsetof(FetchOperationState_t1520449150, ___packageBeingFetched_3)); }
	inline PolyFormat_t1880249796 * get_packageBeingFetched_3() const { return ___packageBeingFetched_3; }
	inline PolyFormat_t1880249796 ** get_address_of_packageBeingFetched_3() { return &___packageBeingFetched_3; }
	inline void set_packageBeingFetched_3(PolyFormat_t1880249796 * value)
	{
		___packageBeingFetched_3 = value;
		Il2CppCodeGenWriteBarrier((&___packageBeingFetched_3), value);
	}

	inline static int32_t get_offset_of_completionCallback_4() { return static_cast<int32_t>(offsetof(FetchOperationState_t1520449150, ___completionCallback_4)); }
	inline FetchFormatFilesCallback_t2377205724 * get_completionCallback_4() const { return ___completionCallback_4; }
	inline FetchFormatFilesCallback_t2377205724 ** get_address_of_completionCallback_4() { return &___completionCallback_4; }
	inline void set_completionCallback_4(FetchFormatFilesCallback_t2377205724 * value)
	{
		___completionCallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___completionCallback_4), value);
	}

	inline static int32_t get_offset_of_progressCallback_5() { return static_cast<int32_t>(offsetof(FetchOperationState_t1520449150, ___progressCallback_5)); }
	inline FetchProgressCallback_t1972555609 * get_progressCallback_5() const { return ___progressCallback_5; }
	inline FetchProgressCallback_t1972555609 ** get_address_of_progressCallback_5() { return &___progressCallback_5; }
	inline void set_progressCallback_5(FetchProgressCallback_t1972555609 * value)
	{
		___progressCallback_5 = value;
		Il2CppCodeGenWriteBarrier((&___progressCallback_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FETCHOPERATIONSTATE_T1520449150_H
#ifndef U3CLISTASSETSU3EC__ANONSTOREY0_T1932574489_H
#define U3CLISTASSETSU3EC__ANONSTOREY0_T1932574489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.PolyMainInternal/<ListAssets>c__AnonStorey0
struct  U3CListAssetsU3Ec__AnonStorey0_t1932574489  : public RuntimeObject
{
public:
	// PolyToolkit.PolyApi/ListAssetsCallback PolyToolkitInternal.PolyMainInternal/<ListAssets>c__AnonStorey0::callback
	ListAssetsCallback_t3881593442 * ___callback_0;
	// PolyToolkitInternal.PolyMainInternal PolyToolkitInternal.PolyMainInternal/<ListAssets>c__AnonStorey0::$this
	PolyMainInternal_t3329830951 * ___U24this_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CListAssetsU3Ec__AnonStorey0_t1932574489, ___callback_0)); }
	inline ListAssetsCallback_t3881593442 * get_callback_0() const { return ___callback_0; }
	inline ListAssetsCallback_t3881593442 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(ListAssetsCallback_t3881593442 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CListAssetsU3Ec__AnonStorey0_t1932574489, ___U24this_1)); }
	inline PolyMainInternal_t3329830951 * get_U24this_1() const { return ___U24this_1; }
	inline PolyMainInternal_t3329830951 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(PolyMainInternal_t3329830951 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLISTASSETSU3EC__ANONSTOREY0_T1932574489_H
#ifndef GLTFIMAGEBASE_T4142612163_H
#define GLTFIMAGEBASE_T4142612163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfImageBase
struct  GltfImageBase_t4142612163  : public RuntimeObject
{
public:
	// System.String PolyToolkitInternal.GltfImageBase::uri
	String_t* ___uri_0;
	// PolyToolkitInternal.RawImage PolyToolkitInternal.GltfImageBase::data
	RawImage_t3303103980 * ___data_1;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(GltfImageBase_t4142612163, ___uri_0)); }
	inline String_t* get_uri_0() const { return ___uri_0; }
	inline String_t** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(String_t* value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(GltfImageBase_t4142612163, ___data_1)); }
	inline RawImage_t3303103980 * get_data_1() const { return ___data_1; }
	inline RawImage_t3303103980 ** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(RawImage_t3303103980 * value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFIMAGEBASE_T4142612163_H
#ifndef U3CLISTUSERASSETSU3EC__ANONSTOREY1_T4120733243_H
#define U3CLISTUSERASSETSU3EC__ANONSTOREY1_T4120733243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.PolyMainInternal/<ListUserAssets>c__AnonStorey1
struct  U3CListUserAssetsU3Ec__AnonStorey1_t4120733243  : public RuntimeObject
{
public:
	// PolyToolkit.PolyApi/ListAssetsCallback PolyToolkitInternal.PolyMainInternal/<ListUserAssets>c__AnonStorey1::callback
	ListAssetsCallback_t3881593442 * ___callback_0;
	// PolyToolkitInternal.PolyMainInternal PolyToolkitInternal.PolyMainInternal/<ListUserAssets>c__AnonStorey1::$this
	PolyMainInternal_t3329830951 * ___U24this_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CListUserAssetsU3Ec__AnonStorey1_t4120733243, ___callback_0)); }
	inline ListAssetsCallback_t3881593442 * get_callback_0() const { return ___callback_0; }
	inline ListAssetsCallback_t3881593442 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(ListAssetsCallback_t3881593442 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CListUserAssetsU3Ec__AnonStorey1_t4120733243, ___U24this_1)); }
	inline PolyMainInternal_t3329830951 * get_U24this_1() const { return ___U24this_1; }
	inline PolyMainInternal_t3329830951 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(PolyMainInternal_t3329830951 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLISTUSERASSETSU3EC__ANONSTOREY1_T4120733243_H
#ifndef U3CLISTLIKEDASSETSU3EC__ANONSTOREY2_T1445844308_H
#define U3CLISTLIKEDASSETSU3EC__ANONSTOREY2_T1445844308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.PolyMainInternal/<ListLikedAssets>c__AnonStorey2
struct  U3CListLikedAssetsU3Ec__AnonStorey2_t1445844308  : public RuntimeObject
{
public:
	// PolyToolkit.PolyApi/ListAssetsCallback PolyToolkitInternal.PolyMainInternal/<ListLikedAssets>c__AnonStorey2::callback
	ListAssetsCallback_t3881593442 * ___callback_0;
	// PolyToolkitInternal.PolyMainInternal PolyToolkitInternal.PolyMainInternal/<ListLikedAssets>c__AnonStorey2::$this
	PolyMainInternal_t3329830951 * ___U24this_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CListLikedAssetsU3Ec__AnonStorey2_t1445844308, ___callback_0)); }
	inline ListAssetsCallback_t3881593442 * get_callback_0() const { return ___callback_0; }
	inline ListAssetsCallback_t3881593442 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(ListAssetsCallback_t3881593442 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CListLikedAssetsU3Ec__AnonStorey2_t1445844308, ___U24this_1)); }
	inline PolyMainInternal_t3329830951 * get_U24this_1() const { return ___U24this_1; }
	inline PolyMainInternal_t3329830951 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(PolyMainInternal_t3329830951 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLISTLIKEDASSETSU3EC__ANONSTOREY2_T1445844308_H
#ifndef U3CHANDLEWEBREQUESTU3EC__ANONSTOREY1_T2411719362_H
#define U3CHANDLEWEBREQUESTU3EC__ANONSTOREY1_T2411719362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.client.model.util.WebRequestManager/<HandleWebRequest>c__Iterator0/<HandleWebRequest>c__AnonStorey1
struct  U3CHandleWebRequestU3Ec__AnonStorey1_t2411719362  : public RuntimeObject
{
public:
	// System.Boolean PolyToolkitInternal.client.model.util.WebRequestManager/<HandleWebRequest>c__Iterator0/<HandleWebRequest>c__AnonStorey1::cacheHit
	bool ___cacheHit_0;
	// System.Byte[] PolyToolkitInternal.client.model.util.WebRequestManager/<HandleWebRequest>c__Iterator0/<HandleWebRequest>c__AnonStorey1::cacheData
	ByteU5BU5D_t4116647657* ___cacheData_1;
	// System.Boolean PolyToolkitInternal.client.model.util.WebRequestManager/<HandleWebRequest>c__Iterator0/<HandleWebRequest>c__AnonStorey1::cacheReadDone
	bool ___cacheReadDone_2;
	// PolyToolkitInternal.client.model.util.WebRequestManager/<HandleWebRequest>c__Iterator0 PolyToolkitInternal.client.model.util.WebRequestManager/<HandleWebRequest>c__Iterator0/<HandleWebRequest>c__AnonStorey1::<>f__ref$0
	U3CHandleWebRequestU3Ec__Iterator0_t3773947286 * ___U3CU3Ef__refU240_3;

public:
	inline static int32_t get_offset_of_cacheHit_0() { return static_cast<int32_t>(offsetof(U3CHandleWebRequestU3Ec__AnonStorey1_t2411719362, ___cacheHit_0)); }
	inline bool get_cacheHit_0() const { return ___cacheHit_0; }
	inline bool* get_address_of_cacheHit_0() { return &___cacheHit_0; }
	inline void set_cacheHit_0(bool value)
	{
		___cacheHit_0 = value;
	}

	inline static int32_t get_offset_of_cacheData_1() { return static_cast<int32_t>(offsetof(U3CHandleWebRequestU3Ec__AnonStorey1_t2411719362, ___cacheData_1)); }
	inline ByteU5BU5D_t4116647657* get_cacheData_1() const { return ___cacheData_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_cacheData_1() { return &___cacheData_1; }
	inline void set_cacheData_1(ByteU5BU5D_t4116647657* value)
	{
		___cacheData_1 = value;
		Il2CppCodeGenWriteBarrier((&___cacheData_1), value);
	}

	inline static int32_t get_offset_of_cacheReadDone_2() { return static_cast<int32_t>(offsetof(U3CHandleWebRequestU3Ec__AnonStorey1_t2411719362, ___cacheReadDone_2)); }
	inline bool get_cacheReadDone_2() const { return ___cacheReadDone_2; }
	inline bool* get_address_of_cacheReadDone_2() { return &___cacheReadDone_2; }
	inline void set_cacheReadDone_2(bool value)
	{
		___cacheReadDone_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_3() { return static_cast<int32_t>(offsetof(U3CHandleWebRequestU3Ec__AnonStorey1_t2411719362, ___U3CU3Ef__refU240_3)); }
	inline U3CHandleWebRequestU3Ec__Iterator0_t3773947286 * get_U3CU3Ef__refU240_3() const { return ___U3CU3Ef__refU240_3; }
	inline U3CHandleWebRequestU3Ec__Iterator0_t3773947286 ** get_address_of_U3CU3Ef__refU240_3() { return &___U3CU3Ef__refU240_3; }
	inline void set_U3CU3Ef__refU240_3(U3CHandleWebRequestU3Ec__Iterator0_t3773947286 * value)
	{
		___U3CU3Ef__refU240_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHANDLEWEBREQUESTU3EC__ANONSTOREY1_T2411719362_H
#ifndef MEASUREMENTUNITS_T3638319501_H
#define MEASUREMENTUNITS_T3638319501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.MeasurementUnits
struct  MeasurementUnits_t3638319501  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEASUREMENTUNITS_T3638319501_H
#ifndef FORMATSAVEDATA_T3321373558_H
#define FORMATSAVEDATA_T3321373558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.model.export.FormatSaveData
struct  FormatSaveData_t3321373558  : public RuntimeObject
{
public:
	// PolyToolkitInternal.model.export.FormatDataFile PolyToolkitInternal.model.export.FormatSaveData::root
	FormatDataFile_t3534188782 * ___root_0;
	// System.Collections.Generic.List`1<PolyToolkitInternal.model.export.FormatDataFile> PolyToolkitInternal.model.export.FormatSaveData::resources
	List_1_t711296228 * ___resources_1;
	// System.Byte[] PolyToolkitInternal.model.export.FormatSaveData::zippedFiles
	ByteU5BU5D_t4116647657* ___zippedFiles_2;
	// System.Int64 PolyToolkitInternal.model.export.FormatSaveData::triangleCount
	int64_t ___triangleCount_3;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(FormatSaveData_t3321373558, ___root_0)); }
	inline FormatDataFile_t3534188782 * get_root_0() const { return ___root_0; }
	inline FormatDataFile_t3534188782 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(FormatDataFile_t3534188782 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_resources_1() { return static_cast<int32_t>(offsetof(FormatSaveData_t3321373558, ___resources_1)); }
	inline List_1_t711296228 * get_resources_1() const { return ___resources_1; }
	inline List_1_t711296228 ** get_address_of_resources_1() { return &___resources_1; }
	inline void set_resources_1(List_1_t711296228 * value)
	{
		___resources_1 = value;
		Il2CppCodeGenWriteBarrier((&___resources_1), value);
	}

	inline static int32_t get_offset_of_zippedFiles_2() { return static_cast<int32_t>(offsetof(FormatSaveData_t3321373558, ___zippedFiles_2)); }
	inline ByteU5BU5D_t4116647657* get_zippedFiles_2() const { return ___zippedFiles_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_zippedFiles_2() { return &___zippedFiles_2; }
	inline void set_zippedFiles_2(ByteU5BU5D_t4116647657* value)
	{
		___zippedFiles_2 = value;
		Il2CppCodeGenWriteBarrier((&___zippedFiles_2), value);
	}

	inline static int32_t get_offset_of_triangleCount_3() { return static_cast<int32_t>(offsetof(FormatSaveData_t3321373558, ___triangleCount_3)); }
	inline int64_t get_triangleCount_3() const { return ___triangleCount_3; }
	inline int64_t* get_address_of_triangleCount_3() { return &___triangleCount_3; }
	inline void set_triangleCount_3(int64_t value)
	{
		___triangleCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATSAVEDATA_T3321373558_H
#ifndef FORMATDATAFILE_T3534188782_H
#define FORMATDATAFILE_T3534188782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.model.export.FormatDataFile
struct  FormatDataFile_t3534188782  : public RuntimeObject
{
public:
	// System.String PolyToolkitInternal.model.export.FormatDataFile::fileName
	String_t* ___fileName_0;
	// System.String PolyToolkitInternal.model.export.FormatDataFile::mimeType
	String_t* ___mimeType_1;
	// System.Byte[] PolyToolkitInternal.model.export.FormatDataFile::bytes
	ByteU5BU5D_t4116647657* ___bytes_2;
	// System.String PolyToolkitInternal.model.export.FormatDataFile::tag
	String_t* ___tag_3;
	// System.Byte[] PolyToolkitInternal.model.export.FormatDataFile::multipartBytes
	ByteU5BU5D_t4116647657* ___multipartBytes_4;

public:
	inline static int32_t get_offset_of_fileName_0() { return static_cast<int32_t>(offsetof(FormatDataFile_t3534188782, ___fileName_0)); }
	inline String_t* get_fileName_0() const { return ___fileName_0; }
	inline String_t** get_address_of_fileName_0() { return &___fileName_0; }
	inline void set_fileName_0(String_t* value)
	{
		___fileName_0 = value;
		Il2CppCodeGenWriteBarrier((&___fileName_0), value);
	}

	inline static int32_t get_offset_of_mimeType_1() { return static_cast<int32_t>(offsetof(FormatDataFile_t3534188782, ___mimeType_1)); }
	inline String_t* get_mimeType_1() const { return ___mimeType_1; }
	inline String_t** get_address_of_mimeType_1() { return &___mimeType_1; }
	inline void set_mimeType_1(String_t* value)
	{
		___mimeType_1 = value;
		Il2CppCodeGenWriteBarrier((&___mimeType_1), value);
	}

	inline static int32_t get_offset_of_bytes_2() { return static_cast<int32_t>(offsetof(FormatDataFile_t3534188782, ___bytes_2)); }
	inline ByteU5BU5D_t4116647657* get_bytes_2() const { return ___bytes_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_bytes_2() { return &___bytes_2; }
	inline void set_bytes_2(ByteU5BU5D_t4116647657* value)
	{
		___bytes_2 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_2), value);
	}

	inline static int32_t get_offset_of_tag_3() { return static_cast<int32_t>(offsetof(FormatDataFile_t3534188782, ___tag_3)); }
	inline String_t* get_tag_3() const { return ___tag_3; }
	inline String_t** get_address_of_tag_3() { return &___tag_3; }
	inline void set_tag_3(String_t* value)
	{
		___tag_3 = value;
		Il2CppCodeGenWriteBarrier((&___tag_3), value);
	}

	inline static int32_t get_offset_of_multipartBytes_4() { return static_cast<int32_t>(offsetof(FormatDataFile_t3534188782, ___multipartBytes_4)); }
	inline ByteU5BU5D_t4116647657* get_multipartBytes_4() const { return ___multipartBytes_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_multipartBytes_4() { return &___multipartBytes_4; }
	inline void set_multipartBytes_4(ByteU5BU5D_t4116647657* value)
	{
		___multipartBytes_4 = value;
		Il2CppCodeGenWriteBarrier((&___multipartBytes_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATDATAFILE_T3534188782_H
#ifndef EXPORTUTILS_T2410614856_H
#define EXPORTUTILS_T2410614856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.model.export.ExportUtils
struct  ExportUtils_t2410614856  : public RuntimeObject
{
public:

public:
};

struct ExportUtils_t2410614856_StaticFields
{
public:
	// System.String PolyToolkitInternal.model.export.ExportUtils::OBJ_FILENAME
	String_t* ___OBJ_FILENAME_0;
	// System.String PolyToolkitInternal.model.export.ExportUtils::MTL_FILENAME
	String_t* ___MTL_FILENAME_1;
	// System.String PolyToolkitInternal.model.export.ExportUtils::THUMBNAIL_FILENAME
	String_t* ___THUMBNAIL_FILENAME_2;
	// System.String PolyToolkitInternal.model.export.ExportUtils::POLY_FILENAME
	String_t* ___POLY_FILENAME_3;
	// System.String PolyToolkitInternal.model.export.ExportUtils::OBJ_MTL_ZIP_FILENAME
	String_t* ___OBJ_MTL_ZIP_FILENAME_4;
	// System.String PolyToolkitInternal.model.export.ExportUtils::GLTF_FILENAME
	String_t* ___GLTF_FILENAME_5;
	// System.String PolyToolkitInternal.model.export.ExportUtils::GLTF_BIN_FILENAME
	String_t* ___GLTF_BIN_FILENAME_6;
	// System.String PolyToolkitInternal.model.export.ExportUtils::POLY_ZIP_FILENAME
	String_t* ___POLY_ZIP_FILENAME_7;

public:
	inline static int32_t get_offset_of_OBJ_FILENAME_0() { return static_cast<int32_t>(offsetof(ExportUtils_t2410614856_StaticFields, ___OBJ_FILENAME_0)); }
	inline String_t* get_OBJ_FILENAME_0() const { return ___OBJ_FILENAME_0; }
	inline String_t** get_address_of_OBJ_FILENAME_0() { return &___OBJ_FILENAME_0; }
	inline void set_OBJ_FILENAME_0(String_t* value)
	{
		___OBJ_FILENAME_0 = value;
		Il2CppCodeGenWriteBarrier((&___OBJ_FILENAME_0), value);
	}

	inline static int32_t get_offset_of_MTL_FILENAME_1() { return static_cast<int32_t>(offsetof(ExportUtils_t2410614856_StaticFields, ___MTL_FILENAME_1)); }
	inline String_t* get_MTL_FILENAME_1() const { return ___MTL_FILENAME_1; }
	inline String_t** get_address_of_MTL_FILENAME_1() { return &___MTL_FILENAME_1; }
	inline void set_MTL_FILENAME_1(String_t* value)
	{
		___MTL_FILENAME_1 = value;
		Il2CppCodeGenWriteBarrier((&___MTL_FILENAME_1), value);
	}

	inline static int32_t get_offset_of_THUMBNAIL_FILENAME_2() { return static_cast<int32_t>(offsetof(ExportUtils_t2410614856_StaticFields, ___THUMBNAIL_FILENAME_2)); }
	inline String_t* get_THUMBNAIL_FILENAME_2() const { return ___THUMBNAIL_FILENAME_2; }
	inline String_t** get_address_of_THUMBNAIL_FILENAME_2() { return &___THUMBNAIL_FILENAME_2; }
	inline void set_THUMBNAIL_FILENAME_2(String_t* value)
	{
		___THUMBNAIL_FILENAME_2 = value;
		Il2CppCodeGenWriteBarrier((&___THUMBNAIL_FILENAME_2), value);
	}

	inline static int32_t get_offset_of_POLY_FILENAME_3() { return static_cast<int32_t>(offsetof(ExportUtils_t2410614856_StaticFields, ___POLY_FILENAME_3)); }
	inline String_t* get_POLY_FILENAME_3() const { return ___POLY_FILENAME_3; }
	inline String_t** get_address_of_POLY_FILENAME_3() { return &___POLY_FILENAME_3; }
	inline void set_POLY_FILENAME_3(String_t* value)
	{
		___POLY_FILENAME_3 = value;
		Il2CppCodeGenWriteBarrier((&___POLY_FILENAME_3), value);
	}

	inline static int32_t get_offset_of_OBJ_MTL_ZIP_FILENAME_4() { return static_cast<int32_t>(offsetof(ExportUtils_t2410614856_StaticFields, ___OBJ_MTL_ZIP_FILENAME_4)); }
	inline String_t* get_OBJ_MTL_ZIP_FILENAME_4() const { return ___OBJ_MTL_ZIP_FILENAME_4; }
	inline String_t** get_address_of_OBJ_MTL_ZIP_FILENAME_4() { return &___OBJ_MTL_ZIP_FILENAME_4; }
	inline void set_OBJ_MTL_ZIP_FILENAME_4(String_t* value)
	{
		___OBJ_MTL_ZIP_FILENAME_4 = value;
		Il2CppCodeGenWriteBarrier((&___OBJ_MTL_ZIP_FILENAME_4), value);
	}

	inline static int32_t get_offset_of_GLTF_FILENAME_5() { return static_cast<int32_t>(offsetof(ExportUtils_t2410614856_StaticFields, ___GLTF_FILENAME_5)); }
	inline String_t* get_GLTF_FILENAME_5() const { return ___GLTF_FILENAME_5; }
	inline String_t** get_address_of_GLTF_FILENAME_5() { return &___GLTF_FILENAME_5; }
	inline void set_GLTF_FILENAME_5(String_t* value)
	{
		___GLTF_FILENAME_5 = value;
		Il2CppCodeGenWriteBarrier((&___GLTF_FILENAME_5), value);
	}

	inline static int32_t get_offset_of_GLTF_BIN_FILENAME_6() { return static_cast<int32_t>(offsetof(ExportUtils_t2410614856_StaticFields, ___GLTF_BIN_FILENAME_6)); }
	inline String_t* get_GLTF_BIN_FILENAME_6() const { return ___GLTF_BIN_FILENAME_6; }
	inline String_t** get_address_of_GLTF_BIN_FILENAME_6() { return &___GLTF_BIN_FILENAME_6; }
	inline void set_GLTF_BIN_FILENAME_6(String_t* value)
	{
		___GLTF_BIN_FILENAME_6 = value;
		Il2CppCodeGenWriteBarrier((&___GLTF_BIN_FILENAME_6), value);
	}

	inline static int32_t get_offset_of_POLY_ZIP_FILENAME_7() { return static_cast<int32_t>(offsetof(ExportUtils_t2410614856_StaticFields, ___POLY_ZIP_FILENAME_7)); }
	inline String_t* get_POLY_ZIP_FILENAME_7() const { return ___POLY_ZIP_FILENAME_7; }
	inline String_t** get_address_of_POLY_ZIP_FILENAME_7() { return &___POLY_ZIP_FILENAME_7; }
	inline void set_POLY_ZIP_FILENAME_7(String_t* value)
	{
		___POLY_ZIP_FILENAME_7 = value;
		Il2CppCodeGenWriteBarrier((&___POLY_ZIP_FILENAME_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPORTUTILS_T2410614856_H
#ifndef PENDINGREQUEST_T652293891_H
#define PENDINGREQUEST_T652293891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.client.model.util.WebRequestManager/PendingRequest
struct  PendingRequest_t652293891  : public RuntimeObject
{
public:
	// PolyToolkitInternal.client.model.util.WebRequestManager/CreationCallback PolyToolkitInternal.client.model.util.WebRequestManager/PendingRequest::creationCallback
	CreationCallback_t949890767 * ___creationCallback_0;
	// PolyToolkitInternal.client.model.util.WebRequestManager/CompletionCallback PolyToolkitInternal.client.model.util.WebRequestManager/PendingRequest::completionCallback
	CompletionCallback_t3367319633 * ___completionCallback_1;
	// System.Int64 PolyToolkitInternal.client.model.util.WebRequestManager/PendingRequest::maxAgeMillis
	int64_t ___maxAgeMillis_2;

public:
	inline static int32_t get_offset_of_creationCallback_0() { return static_cast<int32_t>(offsetof(PendingRequest_t652293891, ___creationCallback_0)); }
	inline CreationCallback_t949890767 * get_creationCallback_0() const { return ___creationCallback_0; }
	inline CreationCallback_t949890767 ** get_address_of_creationCallback_0() { return &___creationCallback_0; }
	inline void set_creationCallback_0(CreationCallback_t949890767 * value)
	{
		___creationCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___creationCallback_0), value);
	}

	inline static int32_t get_offset_of_completionCallback_1() { return static_cast<int32_t>(offsetof(PendingRequest_t652293891, ___completionCallback_1)); }
	inline CompletionCallback_t3367319633 * get_completionCallback_1() const { return ___completionCallback_1; }
	inline CompletionCallback_t3367319633 ** get_address_of_completionCallback_1() { return &___completionCallback_1; }
	inline void set_completionCallback_1(CompletionCallback_t3367319633 * value)
	{
		___completionCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___completionCallback_1), value);
	}

	inline static int32_t get_offset_of_maxAgeMillis_2() { return static_cast<int32_t>(offsetof(PendingRequest_t652293891, ___maxAgeMillis_2)); }
	inline int64_t get_maxAgeMillis_2() const { return ___maxAgeMillis_2; }
	inline int64_t* get_address_of_maxAgeMillis_2() { return &___maxAgeMillis_2; }
	inline void set_maxAgeMillis_2(int64_t value)
	{
		___maxAgeMillis_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PENDINGREQUEST_T652293891_H
#ifndef BUFFERHOLDER_T424688828_H
#define BUFFERHOLDER_T424688828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.client.model.util.WebRequestManager/BufferHolder
struct  BufferHolder_t424688828  : public RuntimeObject
{
public:
	// System.Byte[] PolyToolkitInternal.client.model.util.WebRequestManager/BufferHolder::tempBuffer
	ByteU5BU5D_t4116647657* ___tempBuffer_0;
	// System.Byte[] PolyToolkitInternal.client.model.util.WebRequestManager/BufferHolder::dataBuffer
	ByteU5BU5D_t4116647657* ___dataBuffer_1;

public:
	inline static int32_t get_offset_of_tempBuffer_0() { return static_cast<int32_t>(offsetof(BufferHolder_t424688828, ___tempBuffer_0)); }
	inline ByteU5BU5D_t4116647657* get_tempBuffer_0() const { return ___tempBuffer_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_tempBuffer_0() { return &___tempBuffer_0; }
	inline void set_tempBuffer_0(ByteU5BU5D_t4116647657* value)
	{
		___tempBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___tempBuffer_0), value);
	}

	inline static int32_t get_offset_of_dataBuffer_1() { return static_cast<int32_t>(offsetof(BufferHolder_t424688828, ___dataBuffer_1)); }
	inline ByteU5BU5D_t4116647657* get_dataBuffer_1() const { return ___dataBuffer_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_dataBuffer_1() { return &___dataBuffer_1; }
	inline void set_dataBuffer_1(ByteU5BU5D_t4116647657* value)
	{
		___dataBuffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___dataBuffer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERHOLDER_T424688828_H
#ifndef U3CGETASSETU3EC__ANONSTOREY3_T1917775743_H
#define U3CGETASSETU3EC__ANONSTOREY3_T1917775743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.PolyMainInternal/<GetAsset>c__AnonStorey3
struct  U3CGetAssetU3Ec__AnonStorey3_t1917775743  : public RuntimeObject
{
public:
	// PolyToolkit.PolyApi/GetAssetCallback PolyToolkitInternal.PolyMainInternal/<GetAsset>c__AnonStorey3::callback
	GetAssetCallback_t1146242383 * ___callback_0;
	// System.String PolyToolkitInternal.PolyMainInternal/<GetAsset>c__AnonStorey3::id
	String_t* ___id_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CGetAssetU3Ec__AnonStorey3_t1917775743, ___callback_0)); }
	inline GetAssetCallback_t1146242383 * get_callback_0() const { return ___callback_0; }
	inline GetAssetCallback_t1146242383 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(GetAssetCallback_t1146242383 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(U3CGetAssetU3Ec__AnonStorey3_t1917775743, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETASSETU3EC__ANONSTOREY3_T1917775743_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef POLYFORMATCOMPLEXITY_T3101848153_H
#define POLYFORMATCOMPLEXITY_T3101848153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyFormatComplexity
struct  PolyFormatComplexity_t3101848153  : public RuntimeObject
{
public:
	// System.Int64 PolyToolkit.PolyFormatComplexity::triangleCount
	int64_t ___triangleCount_0;
	// System.Int32 PolyToolkit.PolyFormatComplexity::lodHint
	int32_t ___lodHint_1;

public:
	inline static int32_t get_offset_of_triangleCount_0() { return static_cast<int32_t>(offsetof(PolyFormatComplexity_t3101848153, ___triangleCount_0)); }
	inline int64_t get_triangleCount_0() const { return ___triangleCount_0; }
	inline int64_t* get_address_of_triangleCount_0() { return &___triangleCount_0; }
	inline void set_triangleCount_0(int64_t value)
	{
		___triangleCount_0 = value;
	}

	inline static int32_t get_offset_of_lodHint_1() { return static_cast<int32_t>(offsetof(PolyFormatComplexity_t3101848153, ___lodHint_1)); }
	inline int32_t get_lodHint_1() const { return ___lodHint_1; }
	inline int32_t* get_address_of_lodHint_1() { return &___lodHint_1; }
	inline void set_lodHint_1(int32_t value)
	{
		___lodHint_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYFORMATCOMPLEXITY_T3101848153_H
#ifndef POLYFILE_T4211357808_H
#define POLYFILE_T4211357808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyFile
struct  PolyFile_t4211357808  : public RuntimeObject
{
public:
	// System.String PolyToolkit.PolyFile::relativePath
	String_t* ___relativePath_0;
	// System.String PolyToolkit.PolyFile::url
	String_t* ___url_1;
	// System.String PolyToolkit.PolyFile::contentType
	String_t* ___contentType_2;
	// System.Byte[] PolyToolkit.PolyFile::contents
	ByteU5BU5D_t4116647657* ___contents_3;
	// System.String PolyToolkit.PolyFile::text
	String_t* ___text_4;

public:
	inline static int32_t get_offset_of_relativePath_0() { return static_cast<int32_t>(offsetof(PolyFile_t4211357808, ___relativePath_0)); }
	inline String_t* get_relativePath_0() const { return ___relativePath_0; }
	inline String_t** get_address_of_relativePath_0() { return &___relativePath_0; }
	inline void set_relativePath_0(String_t* value)
	{
		___relativePath_0 = value;
		Il2CppCodeGenWriteBarrier((&___relativePath_0), value);
	}

	inline static int32_t get_offset_of_url_1() { return static_cast<int32_t>(offsetof(PolyFile_t4211357808, ___url_1)); }
	inline String_t* get_url_1() const { return ___url_1; }
	inline String_t** get_address_of_url_1() { return &___url_1; }
	inline void set_url_1(String_t* value)
	{
		___url_1 = value;
		Il2CppCodeGenWriteBarrier((&___url_1), value);
	}

	inline static int32_t get_offset_of_contentType_2() { return static_cast<int32_t>(offsetof(PolyFile_t4211357808, ___contentType_2)); }
	inline String_t* get_contentType_2() const { return ___contentType_2; }
	inline String_t** get_address_of_contentType_2() { return &___contentType_2; }
	inline void set_contentType_2(String_t* value)
	{
		___contentType_2 = value;
		Il2CppCodeGenWriteBarrier((&___contentType_2), value);
	}

	inline static int32_t get_offset_of_contents_3() { return static_cast<int32_t>(offsetof(PolyFile_t4211357808, ___contents_3)); }
	inline ByteU5BU5D_t4116647657* get_contents_3() const { return ___contents_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_contents_3() { return &___contents_3; }
	inline void set_contents_3(ByteU5BU5D_t4116647657* value)
	{
		___contents_3 = value;
		Il2CppCodeGenWriteBarrier((&___contents_3), value);
	}

	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(PolyFile_t4211357808, ___text_4)); }
	inline String_t* get_text_4() const { return ___text_4; }
	inline String_t** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(String_t* value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYFILE_T4211357808_H
#ifndef POLYAPI_T923893618_H
#define POLYAPI_T923893618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyApi
struct  PolyApi_t923893618  : public RuntimeObject
{
public:

public:
};

struct PolyApi_t923893618_StaticFields
{
public:
	// System.Boolean PolyToolkit.PolyApi::initialized
	bool ___initialized_0;
	// System.Comparison`1<PolyToolkit.PolyAsset> PolyToolkit.PolyApi::<>f__am$cache0
	Comparison_1_t1589084690 * ___U3CU3Ef__amU24cache0_1;

public:
	inline static int32_t get_offset_of_initialized_0() { return static_cast<int32_t>(offsetof(PolyApi_t923893618_StaticFields, ___initialized_0)); }
	inline bool get_initialized_0() const { return ___initialized_0; }
	inline bool* get_address_of_initialized_0() { return &___initialized_0; }
	inline void set_initialized_0(bool value)
	{
		___initialized_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(PolyApi_t923893618_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Comparison_1_t1589084690 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Comparison_1_t1589084690 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Comparison_1_t1589084690 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYAPI_T923893618_H
#ifndef UNITYCOMPAT_T3661118369_H
#define UNITYCOMPAT_T3661118369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.UnityCompat
struct  UnityCompat_t3661118369  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCOMPAT_T3661118369_H
#ifndef U3CIMPORTFORMATU3EC__ANONSTOREY5_T99629973_H
#define U3CIMPORTFORMATU3EC__ANONSTOREY5_T99629973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.PolyMainInternal/<ImportFormat>c__AnonStorey5
struct  U3CImportFormatU3Ec__AnonStorey5_t99629973  : public RuntimeObject
{
public:
	// PolyToolkit.PolyApi/ImportCallback PolyToolkitInternal.PolyMainInternal/<ImportFormat>c__AnonStorey5::callback
	ImportCallback_t3513750376 * ___callback_0;
	// PolyToolkit.PolyAsset PolyToolkitInternal.PolyMainInternal/<ImportFormat>c__AnonStorey5::asset
	PolyAsset_t1814153511 * ___asset_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CImportFormatU3Ec__AnonStorey5_t99629973, ___callback_0)); }
	inline ImportCallback_t3513750376 * get_callback_0() const { return ___callback_0; }
	inline ImportCallback_t3513750376 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(ImportCallback_t3513750376 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_asset_1() { return static_cast<int32_t>(offsetof(U3CImportFormatU3Ec__AnonStorey5_t99629973, ___asset_1)); }
	inline PolyAsset_t1814153511 * get_asset_1() const { return ___asset_1; }
	inline PolyAsset_t1814153511 ** get_address_of_asset_1() { return &___asset_1; }
	inline void set_asset_1(PolyAsset_t1814153511 * value)
	{
		___asset_1 = value;
		Il2CppCodeGenWriteBarrier((&___asset_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CIMPORTFORMATU3EC__ANONSTOREY5_T99629973_H
#ifndef U3CFETCHFORMATFILESU3EC__ANONSTOREY6_T1330540246_H
#define U3CFETCHFORMATFILESU3EC__ANONSTOREY6_T1330540246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.PolyMainInternal/<FetchFormatFiles>c__AnonStorey6
struct  U3CFetchFormatFilesU3Ec__AnonStorey6_t1330540246  : public RuntimeObject
{
public:
	// PolyToolkitInternal.PolyMainInternal/FetchOperationState PolyToolkitInternal.PolyMainInternal/<FetchFormatFiles>c__AnonStorey6::state
	FetchOperationState_t1520449150 * ___state_0;
	// PolyToolkitInternal.PolyMainInternal PolyToolkitInternal.PolyMainInternal/<FetchFormatFiles>c__AnonStorey6::$this
	PolyMainInternal_t3329830951 * ___U24this_1;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(U3CFetchFormatFilesU3Ec__AnonStorey6_t1330540246, ___state_0)); }
	inline FetchOperationState_t1520449150 * get_state_0() const { return ___state_0; }
	inline FetchOperationState_t1520449150 ** get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(FetchOperationState_t1520449150 * value)
	{
		___state_0 = value;
		Il2CppCodeGenWriteBarrier((&___state_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CFetchFormatFilesU3Ec__AnonStorey6_t1330540246, ___U24this_1)); }
	inline PolyMainInternal_t3329830951 * get_U24this_1() const { return ___U24this_1; }
	inline PolyMainInternal_t3329830951 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(PolyMainInternal_t3329830951 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFETCHFORMATFILESU3EC__ANONSTOREY6_T1330540246_H
#ifndef U3CFETCHFORMATFILESU3EC__ANONSTOREY7_T1330540247_H
#define U3CFETCHFORMATFILESU3EC__ANONSTOREY7_T1330540247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.PolyMainInternal/<FetchFormatFiles>c__AnonStorey7
struct  U3CFetchFormatFilesU3Ec__AnonStorey7_t1330540247  : public RuntimeObject
{
public:
	// System.Int32 PolyToolkitInternal.PolyMainInternal/<FetchFormatFiles>c__AnonStorey7::thisIndex
	int32_t ___thisIndex_0;
	// PolyToolkitInternal.PolyMainInternal/<FetchFormatFiles>c__AnonStorey6 PolyToolkitInternal.PolyMainInternal/<FetchFormatFiles>c__AnonStorey7::<>f__ref$6
	U3CFetchFormatFilesU3Ec__AnonStorey6_t1330540246 * ___U3CU3Ef__refU246_1;

public:
	inline static int32_t get_offset_of_thisIndex_0() { return static_cast<int32_t>(offsetof(U3CFetchFormatFilesU3Ec__AnonStorey7_t1330540247, ___thisIndex_0)); }
	inline int32_t get_thisIndex_0() const { return ___thisIndex_0; }
	inline int32_t* get_address_of_thisIndex_0() { return &___thisIndex_0; }
	inline void set_thisIndex_0(int32_t value)
	{
		___thisIndex_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU246_1() { return static_cast<int32_t>(offsetof(U3CFetchFormatFilesU3Ec__AnonStorey7_t1330540247, ___U3CU3Ef__refU246_1)); }
	inline U3CFetchFormatFilesU3Ec__AnonStorey6_t1330540246 * get_U3CU3Ef__refU246_1() const { return ___U3CU3Ef__refU246_1; }
	inline U3CFetchFormatFilesU3Ec__AnonStorey6_t1330540246 ** get_address_of_U3CU3Ef__refU246_1() { return &___U3CU3Ef__refU246_1; }
	inline void set_U3CU3Ef__refU246_1(U3CFetchFormatFilesU3Ec__AnonStorey6_t1330540246 * value)
	{
		___U3CU3Ef__refU246_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU246_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFETCHFORMATFILESU3EC__ANONSTOREY7_T1330540247_H
#ifndef POLYUTILS_T2128247007_H
#define POLYUTILS_T2128247007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.PolyUtils
struct  PolyUtils_t2128247007  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYUTILS_T2128247007_H
#ifndef PTDEBUG_T199111972_H
#define PTDEBUG_T199111972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.PtDebug
struct  PtDebug_t199111972  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PTDEBUG_T199111972_H
#ifndef THUMBNAILFETCHER_T4028205215_H
#define THUMBNAILFETCHER_T4028205215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.ThumbnailFetcher
struct  ThumbnailFetcher_t4028205215  : public RuntimeObject
{
public:
	// PolyToolkit.PolyAsset PolyToolkitInternal.ThumbnailFetcher::asset
	PolyAsset_t1814153511 * ___asset_1;
	// PolyToolkit.PolyApi/FetchThumbnailCallback PolyToolkitInternal.ThumbnailFetcher::callback
	FetchThumbnailCallback_t3242648953 * ___callback_2;

public:
	inline static int32_t get_offset_of_asset_1() { return static_cast<int32_t>(offsetof(ThumbnailFetcher_t4028205215, ___asset_1)); }
	inline PolyAsset_t1814153511 * get_asset_1() const { return ___asset_1; }
	inline PolyAsset_t1814153511 ** get_address_of_asset_1() { return &___asset_1; }
	inline void set_asset_1(PolyAsset_t1814153511 * value)
	{
		___asset_1 = value;
		Il2CppCodeGenWriteBarrier((&___asset_1), value);
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(ThumbnailFetcher_t4028205215, ___callback_2)); }
	inline FetchThumbnailCallback_t3242648953 * get_callback_2() const { return ___callback_2; }
	inline FetchThumbnailCallback_t3242648953 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(FetchThumbnailCallback_t3242648953 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THUMBNAILFETCHER_T4028205215_H
#ifndef BUFFEREDSTREAMLOADER_T3545088954_H
#define BUFFEREDSTREAMLOADER_T3545088954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.BufferedStreamLoader
struct  BufferedStreamLoader_t3545088954  : public RuntimeObject
{
public:
	// System.String PolyToolkitInternal.BufferedStreamLoader::uriBase
	String_t* ___uriBase_0;
	// System.Int32 PolyToolkitInternal.BufferedStreamLoader::bufferSize
	int32_t ___bufferSize_1;

public:
	inline static int32_t get_offset_of_uriBase_0() { return static_cast<int32_t>(offsetof(BufferedStreamLoader_t3545088954, ___uriBase_0)); }
	inline String_t* get_uriBase_0() const { return ___uriBase_0; }
	inline String_t** get_address_of_uriBase_0() { return &___uriBase_0; }
	inline void set_uriBase_0(String_t* value)
	{
		___uriBase_0 = value;
		Il2CppCodeGenWriteBarrier((&___uriBase_0), value);
	}

	inline static int32_t get_offset_of_bufferSize_1() { return static_cast<int32_t>(offsetof(BufferedStreamLoader_t3545088954, ___bufferSize_1)); }
	inline int32_t get_bufferSize_1() const { return ___bufferSize_1; }
	inline int32_t* get_address_of_bufferSize_1() { return &___bufferSize_1; }
	inline void set_bufferSize_1(int32_t value)
	{
		___bufferSize_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFEREDSTREAMLOADER_T3545088954_H
#ifndef FULLYBUFFEREDLOADER_T4158986460_H
#define FULLYBUFFEREDLOADER_T4158986460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.FullyBufferedLoader
struct  FullyBufferedLoader_t4158986460  : public RuntimeObject
{
public:
	// System.String PolyToolkitInternal.FullyBufferedLoader::uriBase
	String_t* ___uriBase_0;

public:
	inline static int32_t get_offset_of_uriBase_0() { return static_cast<int32_t>(offsetof(FullyBufferedLoader_t4158986460, ___uriBase_0)); }
	inline String_t* get_uriBase_0() const { return ___uriBase_0; }
	inline String_t** get_address_of_uriBase_0() { return &___uriBase_0; }
	inline void set_uriBase_0(String_t* value)
	{
		___uriBase_0 = value;
		Il2CppCodeGenWriteBarrier((&___uriBase_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FULLYBUFFEREDLOADER_T4158986460_H
#ifndef BUFFEREDSTREAMREADER_T3800439956_H
#define BUFFEREDSTREAMREADER_T3800439956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.BufferedStreamReader
struct  BufferedStreamReader_t3800439956  : public RuntimeObject
{
public:
	// System.IO.Stream PolyToolkitInternal.BufferedStreamReader::stream
	Stream_t1273022909 * ___stream_0;
	// System.Byte[] PolyToolkitInternal.BufferedStreamReader::tempBuffer
	ByteU5BU5D_t4116647657* ___tempBuffer_1;
	// System.Int64 PolyToolkitInternal.BufferedStreamReader::contentLength
	int64_t ___contentLength_2;

public:
	inline static int32_t get_offset_of_stream_0() { return static_cast<int32_t>(offsetof(BufferedStreamReader_t3800439956, ___stream_0)); }
	inline Stream_t1273022909 * get_stream_0() const { return ___stream_0; }
	inline Stream_t1273022909 ** get_address_of_stream_0() { return &___stream_0; }
	inline void set_stream_0(Stream_t1273022909 * value)
	{
		___stream_0 = value;
		Il2CppCodeGenWriteBarrier((&___stream_0), value);
	}

	inline static int32_t get_offset_of_tempBuffer_1() { return static_cast<int32_t>(offsetof(BufferedStreamReader_t3800439956, ___tempBuffer_1)); }
	inline ByteU5BU5D_t4116647657* get_tempBuffer_1() const { return ___tempBuffer_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_tempBuffer_1() { return &___tempBuffer_1; }
	inline void set_tempBuffer_1(ByteU5BU5D_t4116647657* value)
	{
		___tempBuffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___tempBuffer_1), value);
	}

	inline static int32_t get_offset_of_contentLength_2() { return static_cast<int32_t>(offsetof(BufferedStreamReader_t3800439956, ___contentLength_2)); }
	inline int64_t get_contentLength_2() const { return ___contentLength_2; }
	inline int64_t* get_address_of_contentLength_2() { return &___contentLength_2; }
	inline void set_contentLength_2(int64_t value)
	{
		___contentLength_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFEREDSTREAMREADER_T3800439956_H
#ifndef U3CCREATEGAMEOBJECTSFROMNODESU3EC__ITERATOR0_T3449319381_H
#define U3CCREATEGAMEOBJECTSFROMNODESU3EC__ITERATOR0_T3449319381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNodes>c__Iterator0
struct  U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNodes>c__Iterator0::<loaded>__0
	List_1_t1017553631 * ___U3CloadedU3E__0_0;
	// PolyToolkitInternal.ImportGltf/ImportState PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNodes>c__Iterator0::state
	ImportState_t482765084 * ___state_1;
	// PolyToolkitInternal.IUriLoader PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNodes>c__Iterator0::uriLoader
	RuntimeObject* ___uriLoader_2;
	// System.Collections.IEnumerator PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNodes>c__Iterator0::$locvar0
	RuntimeObject* ___U24locvar0_3;
	// System.Object PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNodes>c__Iterator0::<unused>__1
	RuntimeObject * ___U3CunusedU3E__1_4;
	// System.IDisposable PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNodes>c__Iterator0::$locvar1
	RuntimeObject* ___U24locvar1_5;
	// PolyToolkitInternal.ImportGltf/GltfImportResult PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNodes>c__Iterator0::result
	GltfImportResult_t3877923054 * ___result_6;
	// PolyToolkitInternal.GltfMaterialConverter PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNodes>c__Iterator0::<matConverter>__0
	GltfMaterialConverter_t1532388150 * ___U3CmatConverterU3E__0_7;
	// UnityEngine.Transform PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNodes>c__Iterator0::<rootTransform>__0
	Transform_t3600365921 * ___U3CrootTransformU3E__0_8;
	// System.Collections.Generic.IEnumerator`1<PolyToolkitInternal.GltfNodeBase> PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNodes>c__Iterator0::$locvar2
	RuntimeObject* ___U24locvar2_9;
	// PolyToolkitInternal.GltfNodeBase PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNodes>c__Iterator0::<node>__2
	GltfNodeBase_t3005766873 * ___U3CnodeU3E__2_10;
	// System.Collections.IEnumerator PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNodes>c__Iterator0::$locvar3
	RuntimeObject* ___U24locvar3_11;
	// System.Object PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNodes>c__Iterator0::<unused>__3
	RuntimeObject * ___U3CunusedU3E__3_12;
	// System.IDisposable PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNodes>c__Iterator0::$locvar4
	RuntimeObject* ___U24locvar4_13;
	// System.Collections.IEnumerator PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNodes>c__Iterator0::$locvar5
	RuntimeObject* ___U24locvar5_14;
	// System.IDisposable PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNodes>c__Iterator0::$locvar6
	RuntimeObject* ___U24locvar6_15;
	// System.Object PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNodes>c__Iterator0::$current
	RuntimeObject * ___U24current_16;
	// System.Boolean PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNodes>c__Iterator0::$disposing
	bool ___U24disposing_17;
	// System.Int32 PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNodes>c__Iterator0::$PC
	int32_t ___U24PC_18;

public:
	inline static int32_t get_offset_of_U3CloadedU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381, ___U3CloadedU3E__0_0)); }
	inline List_1_t1017553631 * get_U3CloadedU3E__0_0() const { return ___U3CloadedU3E__0_0; }
	inline List_1_t1017553631 ** get_address_of_U3CloadedU3E__0_0() { return &___U3CloadedU3E__0_0; }
	inline void set_U3CloadedU3E__0_0(List_1_t1017553631 * value)
	{
		___U3CloadedU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloadedU3E__0_0), value);
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381, ___state_1)); }
	inline ImportState_t482765084 * get_state_1() const { return ___state_1; }
	inline ImportState_t482765084 ** get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(ImportState_t482765084 * value)
	{
		___state_1 = value;
		Il2CppCodeGenWriteBarrier((&___state_1), value);
	}

	inline static int32_t get_offset_of_uriLoader_2() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381, ___uriLoader_2)); }
	inline RuntimeObject* get_uriLoader_2() const { return ___uriLoader_2; }
	inline RuntimeObject** get_address_of_uriLoader_2() { return &___uriLoader_2; }
	inline void set_uriLoader_2(RuntimeObject* value)
	{
		___uriLoader_2 = value;
		Il2CppCodeGenWriteBarrier((&___uriLoader_2), value);
	}

	inline static int32_t get_offset_of_U24locvar0_3() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381, ___U24locvar0_3)); }
	inline RuntimeObject* get_U24locvar0_3() const { return ___U24locvar0_3; }
	inline RuntimeObject** get_address_of_U24locvar0_3() { return &___U24locvar0_3; }
	inline void set_U24locvar0_3(RuntimeObject* value)
	{
		___U24locvar0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_3), value);
	}

	inline static int32_t get_offset_of_U3CunusedU3E__1_4() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381, ___U3CunusedU3E__1_4)); }
	inline RuntimeObject * get_U3CunusedU3E__1_4() const { return ___U3CunusedU3E__1_4; }
	inline RuntimeObject ** get_address_of_U3CunusedU3E__1_4() { return &___U3CunusedU3E__1_4; }
	inline void set_U3CunusedU3E__1_4(RuntimeObject * value)
	{
		___U3CunusedU3E__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CunusedU3E__1_4), value);
	}

	inline static int32_t get_offset_of_U24locvar1_5() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381, ___U24locvar1_5)); }
	inline RuntimeObject* get_U24locvar1_5() const { return ___U24locvar1_5; }
	inline RuntimeObject** get_address_of_U24locvar1_5() { return &___U24locvar1_5; }
	inline void set_U24locvar1_5(RuntimeObject* value)
	{
		___U24locvar1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar1_5), value);
	}

	inline static int32_t get_offset_of_result_6() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381, ___result_6)); }
	inline GltfImportResult_t3877923054 * get_result_6() const { return ___result_6; }
	inline GltfImportResult_t3877923054 ** get_address_of_result_6() { return &___result_6; }
	inline void set_result_6(GltfImportResult_t3877923054 * value)
	{
		___result_6 = value;
		Il2CppCodeGenWriteBarrier((&___result_6), value);
	}

	inline static int32_t get_offset_of_U3CmatConverterU3E__0_7() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381, ___U3CmatConverterU3E__0_7)); }
	inline GltfMaterialConverter_t1532388150 * get_U3CmatConverterU3E__0_7() const { return ___U3CmatConverterU3E__0_7; }
	inline GltfMaterialConverter_t1532388150 ** get_address_of_U3CmatConverterU3E__0_7() { return &___U3CmatConverterU3E__0_7; }
	inline void set_U3CmatConverterU3E__0_7(GltfMaterialConverter_t1532388150 * value)
	{
		___U3CmatConverterU3E__0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmatConverterU3E__0_7), value);
	}

	inline static int32_t get_offset_of_U3CrootTransformU3E__0_8() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381, ___U3CrootTransformU3E__0_8)); }
	inline Transform_t3600365921 * get_U3CrootTransformU3E__0_8() const { return ___U3CrootTransformU3E__0_8; }
	inline Transform_t3600365921 ** get_address_of_U3CrootTransformU3E__0_8() { return &___U3CrootTransformU3E__0_8; }
	inline void set_U3CrootTransformU3E__0_8(Transform_t3600365921 * value)
	{
		___U3CrootTransformU3E__0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrootTransformU3E__0_8), value);
	}

	inline static int32_t get_offset_of_U24locvar2_9() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381, ___U24locvar2_9)); }
	inline RuntimeObject* get_U24locvar2_9() const { return ___U24locvar2_9; }
	inline RuntimeObject** get_address_of_U24locvar2_9() { return &___U24locvar2_9; }
	inline void set_U24locvar2_9(RuntimeObject* value)
	{
		___U24locvar2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar2_9), value);
	}

	inline static int32_t get_offset_of_U3CnodeU3E__2_10() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381, ___U3CnodeU3E__2_10)); }
	inline GltfNodeBase_t3005766873 * get_U3CnodeU3E__2_10() const { return ___U3CnodeU3E__2_10; }
	inline GltfNodeBase_t3005766873 ** get_address_of_U3CnodeU3E__2_10() { return &___U3CnodeU3E__2_10; }
	inline void set_U3CnodeU3E__2_10(GltfNodeBase_t3005766873 * value)
	{
		___U3CnodeU3E__2_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeU3E__2_10), value);
	}

	inline static int32_t get_offset_of_U24locvar3_11() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381, ___U24locvar3_11)); }
	inline RuntimeObject* get_U24locvar3_11() const { return ___U24locvar3_11; }
	inline RuntimeObject** get_address_of_U24locvar3_11() { return &___U24locvar3_11; }
	inline void set_U24locvar3_11(RuntimeObject* value)
	{
		___U24locvar3_11 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar3_11), value);
	}

	inline static int32_t get_offset_of_U3CunusedU3E__3_12() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381, ___U3CunusedU3E__3_12)); }
	inline RuntimeObject * get_U3CunusedU3E__3_12() const { return ___U3CunusedU3E__3_12; }
	inline RuntimeObject ** get_address_of_U3CunusedU3E__3_12() { return &___U3CunusedU3E__3_12; }
	inline void set_U3CunusedU3E__3_12(RuntimeObject * value)
	{
		___U3CunusedU3E__3_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CunusedU3E__3_12), value);
	}

	inline static int32_t get_offset_of_U24locvar4_13() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381, ___U24locvar4_13)); }
	inline RuntimeObject* get_U24locvar4_13() const { return ___U24locvar4_13; }
	inline RuntimeObject** get_address_of_U24locvar4_13() { return &___U24locvar4_13; }
	inline void set_U24locvar4_13(RuntimeObject* value)
	{
		___U24locvar4_13 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar4_13), value);
	}

	inline static int32_t get_offset_of_U24locvar5_14() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381, ___U24locvar5_14)); }
	inline RuntimeObject* get_U24locvar5_14() const { return ___U24locvar5_14; }
	inline RuntimeObject** get_address_of_U24locvar5_14() { return &___U24locvar5_14; }
	inline void set_U24locvar5_14(RuntimeObject* value)
	{
		___U24locvar5_14 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar5_14), value);
	}

	inline static int32_t get_offset_of_U24locvar6_15() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381, ___U24locvar6_15)); }
	inline RuntimeObject* get_U24locvar6_15() const { return ___U24locvar6_15; }
	inline RuntimeObject** get_address_of_U24locvar6_15() { return &___U24locvar6_15; }
	inline void set_U24locvar6_15(RuntimeObject* value)
	{
		___U24locvar6_15 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar6_15), value);
	}

	inline static int32_t get_offset_of_U24current_16() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381, ___U24current_16)); }
	inline RuntimeObject * get_U24current_16() const { return ___U24current_16; }
	inline RuntimeObject ** get_address_of_U24current_16() { return &___U24current_16; }
	inline void set_U24current_16(RuntimeObject * value)
	{
		___U24current_16 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_16), value);
	}

	inline static int32_t get_offset_of_U24disposing_17() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381, ___U24disposing_17)); }
	inline bool get_U24disposing_17() const { return ___U24disposing_17; }
	inline bool* get_address_of_U24disposing_17() { return &___U24disposing_17; }
	inline void set_U24disposing_17(bool value)
	{
		___U24disposing_17 = value;
	}

	inline static int32_t get_offset_of_U24PC_18() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381, ___U24PC_18)); }
	inline int32_t get_U24PC_18() const { return ___U24PC_18; }
	inline int32_t* get_address_of_U24PC_18() { return &___U24PC_18; }
	inline void set_U24PC_18(int32_t value)
	{
		___U24PC_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATEGAMEOBJECTSFROMNODESU3EC__ITERATOR0_T3449319381_H
#ifndef HASHEDPATHBUFFEREDSTREAMLOADER_T106783494_H
#define HASHEDPATHBUFFEREDSTREAMLOADER_T106783494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.HashedPathBufferedStreamLoader
struct  HashedPathBufferedStreamLoader_t106783494  : public RuntimeObject
{
public:
	// System.String PolyToolkitInternal.HashedPathBufferedStreamLoader::uriBase
	String_t* ___uriBase_0;
	// System.Int32 PolyToolkitInternal.HashedPathBufferedStreamLoader::bufferSize
	int32_t ___bufferSize_1;

public:
	inline static int32_t get_offset_of_uriBase_0() { return static_cast<int32_t>(offsetof(HashedPathBufferedStreamLoader_t106783494, ___uriBase_0)); }
	inline String_t* get_uriBase_0() const { return ___uriBase_0; }
	inline String_t** get_address_of_uriBase_0() { return &___uriBase_0; }
	inline void set_uriBase_0(String_t* value)
	{
		___uriBase_0 = value;
		Il2CppCodeGenWriteBarrier((&___uriBase_0), value);
	}

	inline static int32_t get_offset_of_bufferSize_1() { return static_cast<int32_t>(offsetof(HashedPathBufferedStreamLoader_t106783494, ___bufferSize_1)); }
	inline int32_t get_bufferSize_1() const { return ___bufferSize_1; }
	inline int32_t* get_address_of_bufferSize_1() { return &___bufferSize_1; }
	inline void set_bufferSize_1(int32_t value)
	{
		___bufferSize_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHEDPATHBUFFEREDSTREAMLOADER_T106783494_H
#ifndef GLTFIMPORTRESULT_T3877923054_H
#define GLTFIMPORTRESULT_T3877923054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.ImportGltf/GltfImportResult
struct  GltfImportResult_t3877923054  : public RuntimeObject
{
public:
	// UnityEngine.GameObject PolyToolkitInternal.ImportGltf/GltfImportResult::root
	GameObject_t1113636619 * ___root_0;
	// System.Collections.Generic.List`1<UnityEngine.Mesh> PolyToolkitInternal.ImportGltf/GltfImportResult::meshes
	List_1_t826071730 * ___meshes_1;
	// System.Collections.Generic.List`1<UnityEngine.Material> PolyToolkitInternal.ImportGltf/GltfImportResult::materials
	List_1_t1812449865 * ___materials_2;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> PolyToolkitInternal.ImportGltf/GltfImportResult::textures
	List_1_t1017553631 * ___textures_3;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(GltfImportResult_t3877923054, ___root_0)); }
	inline GameObject_t1113636619 * get_root_0() const { return ___root_0; }
	inline GameObject_t1113636619 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(GameObject_t1113636619 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_meshes_1() { return static_cast<int32_t>(offsetof(GltfImportResult_t3877923054, ___meshes_1)); }
	inline List_1_t826071730 * get_meshes_1() const { return ___meshes_1; }
	inline List_1_t826071730 ** get_address_of_meshes_1() { return &___meshes_1; }
	inline void set_meshes_1(List_1_t826071730 * value)
	{
		___meshes_1 = value;
		Il2CppCodeGenWriteBarrier((&___meshes_1), value);
	}

	inline static int32_t get_offset_of_materials_2() { return static_cast<int32_t>(offsetof(GltfImportResult_t3877923054, ___materials_2)); }
	inline List_1_t1812449865 * get_materials_2() const { return ___materials_2; }
	inline List_1_t1812449865 ** get_address_of_materials_2() { return &___materials_2; }
	inline void set_materials_2(List_1_t1812449865 * value)
	{
		___materials_2 = value;
		Il2CppCodeGenWriteBarrier((&___materials_2), value);
	}

	inline static int32_t get_offset_of_textures_3() { return static_cast<int32_t>(offsetof(GltfImportResult_t3877923054, ___textures_3)); }
	inline List_1_t1017553631 * get_textures_3() const { return ___textures_3; }
	inline List_1_t1017553631 ** get_address_of_textures_3() { return &___textures_3; }
	inline void set_textures_3(List_1_t1017553631 * value)
	{
		___textures_3 = value;
		Il2CppCodeGenWriteBarrier((&___textures_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFIMPORTRESULT_T3877923054_H
#ifndef READER_T3716605413_H
#define READER_T3716605413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Reader
struct  Reader_t3716605413  : public RuntimeObject
{
public:
	// System.Byte[] PolyToolkitInternal.Reader::data
	ByteU5BU5D_t4116647657* ___data_0;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(Reader_t3716605413, ___data_0)); }
	inline ByteU5BU5D_t4116647657* get_data_0() const { return ___data_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(ByteU5BU5D_t4116647657* value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READER_T3716605413_H
#ifndef GLTFTEXTUREBASE_T3199118737_H
#define GLTFTEXTUREBASE_T3199118737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfTextureBase
struct  GltfTextureBase_t3199118737  : public RuntimeObject
{
public:
	// UnityEngine.Texture2D PolyToolkitInternal.GltfTextureBase::unityTexture
	Texture2D_t3840446185 * ___unityTexture_0;

public:
	inline static int32_t get_offset_of_unityTexture_0() { return static_cast<int32_t>(offsetof(GltfTextureBase_t3199118737, ___unityTexture_0)); }
	inline Texture2D_t3840446185 * get_unityTexture_0() const { return ___unityTexture_0; }
	inline Texture2D_t3840446185 ** get_address_of_unityTexture_0() { return &___unityTexture_0; }
	inline void set_unityTexture_0(Texture2D_t3840446185 * value)
	{
		___unityTexture_0 = value;
		Il2CppCodeGenWriteBarrier((&___unityTexture_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFTEXTUREBASE_T3199118737_H
#ifndef GLTFMATERIALBASE_T3607200052_H
#define GLTFMATERIALBASE_T3607200052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfMaterialBase
struct  GltfMaterialBase_t3607200052  : public RuntimeObject
{
public:
	// System.String PolyToolkitInternal.GltfMaterialBase::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(GltfMaterialBase_t3607200052, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFMATERIALBASE_T3607200052_H
#ifndef GLTFMESHBASE_T3664755873_H
#define GLTFMESHBASE_T3664755873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfMeshBase
struct  GltfMeshBase_t3664755873  : public RuntimeObject
{
public:
	// System.String PolyToolkitInternal.GltfMeshBase::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(GltfMeshBase_t3664755873, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFMESHBASE_T3664755873_H
#ifndef MATHUTILS_T3934572377_H
#define MATHUTILS_T3934572377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.MathUtils
struct  MathUtils_t3934572377  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHUTILS_T3934572377_H
#ifndef GLTFSCENEBASE_T3309998329_H
#define GLTFSCENEBASE_T3309998329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfSceneBase
struct  GltfSceneBase_t3309998329  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PolyToolkitInternal.GltfSceneBase::extras
	Dictionary_2_t1632706988 * ___extras_0;

public:
	inline static int32_t get_offset_of_extras_0() { return static_cast<int32_t>(offsetof(GltfSceneBase_t3309998329, ___extras_0)); }
	inline Dictionary_2_t1632706988 * get_extras_0() const { return ___extras_0; }
	inline Dictionary_2_t1632706988 ** get_address_of_extras_0() { return &___extras_0; }
	inline void set_extras_0(Dictionary_2_t1632706988 * value)
	{
		___extras_0 = value;
		Il2CppCodeGenWriteBarrier((&___extras_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFSCENEBASE_T3309998329_H
#ifndef FORMATLOADER_T2400397192_H
#define FORMATLOADER_T2400397192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.FormatLoader
struct  FormatLoader_t2400397192  : public RuntimeObject
{
public:
	// PolyToolkit.PolyFormat PolyToolkitInternal.FormatLoader::format
	PolyFormat_t1880249796 * ___format_0;

public:
	inline static int32_t get_offset_of_format_0() { return static_cast<int32_t>(offsetof(FormatLoader_t2400397192, ___format_0)); }
	inline PolyFormat_t1880249796 * get_format_0() const { return ___format_0; }
	inline PolyFormat_t1880249796 ** get_address_of_format_0() { return &___format_0; }
	inline void set_format_0(PolyFormat_t1880249796 * value)
	{
		___format_0 = value;
		Il2CppCodeGenWriteBarrier((&___format_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATLOADER_T2400397192_H
#ifndef U3CLOADU3EC__ANONSTOREY0_T4017912820_H
#define U3CLOADU3EC__ANONSTOREY0_T4017912820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.FormatLoader/<Load>c__AnonStorey0
struct  U3CLoadU3Ec__AnonStorey0_t4017912820  : public RuntimeObject
{
public:
	// System.String PolyToolkitInternal.FormatLoader/<Load>c__AnonStorey0::uri
	String_t* ___uri_0;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(U3CLoadU3Ec__AnonStorey0_t4017912820, ___uri_0)); }
	inline String_t* get_uri_0() const { return ___uri_0; }
	inline String_t** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(String_t* value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADU3EC__ANONSTOREY0_T4017912820_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef SERIALIZABLEGUID_T3370039383_H
#define SERIALIZABLEGUID_T3370039383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TiltBrushToolkit.SerializableGuid
struct  SerializableGuid_t3370039383 
{
public:
	// System.String TiltBrushToolkit.SerializableGuid::m_storage
	String_t* ___m_storage_0;

public:
	inline static int32_t get_offset_of_m_storage_0() { return static_cast<int32_t>(offsetof(SerializableGuid_t3370039383, ___m_storage_0)); }
	inline String_t* get_m_storage_0() const { return ___m_storage_0; }
	inline String_t** get_address_of_m_storage_0() { return &___m_storage_0; }
	inline void set_m_storage_0(String_t* value)
	{
		___m_storage_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_storage_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TiltBrushToolkit.SerializableGuid
struct SerializableGuid_t3370039383_marshaled_pinvoke
{
	char* ___m_storage_0;
};
// Native definition for COM marshalling of TiltBrushToolkit.SerializableGuid
struct SerializableGuid_t3370039383_marshaled_com
{
	Il2CppChar* ___m_storage_0;
};
#endif // SERIALIZABLEGUID_T3370039383_H
#ifndef SURFACESHADERMATERIAL_T2588431549_H
#define SURFACESHADERMATERIAL_T2588431549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.PtSettings/SurfaceShaderMaterial
struct  SurfaceShaderMaterial_t2588431549 
{
public:
	// System.String PolyToolkitInternal.PtSettings/SurfaceShaderMaterial::shaderUrl
	String_t* ___shaderUrl_0;
	// UnityEngine.Material PolyToolkitInternal.PtSettings/SurfaceShaderMaterial::material
	Material_t340375123 * ___material_1;
	// TiltBrushToolkit.BrushDescriptor PolyToolkitInternal.PtSettings/SurfaceShaderMaterial::descriptor
	BrushDescriptor_t2792727521 * ___descriptor_2;

public:
	inline static int32_t get_offset_of_shaderUrl_0() { return static_cast<int32_t>(offsetof(SurfaceShaderMaterial_t2588431549, ___shaderUrl_0)); }
	inline String_t* get_shaderUrl_0() const { return ___shaderUrl_0; }
	inline String_t** get_address_of_shaderUrl_0() { return &___shaderUrl_0; }
	inline void set_shaderUrl_0(String_t* value)
	{
		___shaderUrl_0 = value;
		Il2CppCodeGenWriteBarrier((&___shaderUrl_0), value);
	}

	inline static int32_t get_offset_of_material_1() { return static_cast<int32_t>(offsetof(SurfaceShaderMaterial_t2588431549, ___material_1)); }
	inline Material_t340375123 * get_material_1() const { return ___material_1; }
	inline Material_t340375123 ** get_address_of_material_1() { return &___material_1; }
	inline void set_material_1(Material_t340375123 * value)
	{
		___material_1 = value;
		Il2CppCodeGenWriteBarrier((&___material_1), value);
	}

	inline static int32_t get_offset_of_descriptor_2() { return static_cast<int32_t>(offsetof(SurfaceShaderMaterial_t2588431549, ___descriptor_2)); }
	inline BrushDescriptor_t2792727521 * get_descriptor_2() const { return ___descriptor_2; }
	inline BrushDescriptor_t2792727521 ** get_address_of_descriptor_2() { return &___descriptor_2; }
	inline void set_descriptor_2(BrushDescriptor_t2792727521 * value)
	{
		___descriptor_2 = value;
		Il2CppCodeGenWriteBarrier((&___descriptor_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PolyToolkitInternal.PtSettings/SurfaceShaderMaterial
struct SurfaceShaderMaterial_t2588431549_marshaled_pinvoke
{
	char* ___shaderUrl_0;
	Material_t340375123 * ___material_1;
	BrushDescriptor_t2792727521 * ___descriptor_2;
};
// Native definition for COM marshalling of PolyToolkitInternal.PtSettings/SurfaceShaderMaterial
struct SurfaceShaderMaterial_t2588431549_marshaled_com
{
	Il2CppChar* ___shaderUrl_0;
	Material_t340375123 * ___material_1;
	BrushDescriptor_t2792727521 * ___descriptor_2;
};
#endif // SURFACESHADERMATERIAL_T2588431549_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef VERSION_T724142152_H
#define VERSION_T724142152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Version
struct  Version_t724142152 
{
public:
	// System.Int32 PolyToolkitInternal.Version::major
	int32_t ___major_0;
	// System.Int32 PolyToolkitInternal.Version::minor
	int32_t ___minor_1;

public:
	inline static int32_t get_offset_of_major_0() { return static_cast<int32_t>(offsetof(Version_t724142152, ___major_0)); }
	inline int32_t get_major_0() const { return ___major_0; }
	inline int32_t* get_address_of_major_0() { return &___major_0; }
	inline void set_major_0(int32_t value)
	{
		___major_0 = value;
	}

	inline static int32_t get_offset_of_minor_1() { return static_cast<int32_t>(offsetof(Version_t724142152, ___minor_1)); }
	inline int32_t get_minor_1() const { return ___minor_1; }
	inline int32_t* get_address_of_minor_1() { return &___minor_1; }
	inline void set_minor_1(int32_t value)
	{
		___minor_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERSION_T724142152_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef POLYCACHECONFIG_T1397553534_H
#define POLYCACHECONFIG_T1397553534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyCacheConfig
struct  PolyCacheConfig_t1397553534 
{
public:
	// System.Boolean PolyToolkit.PolyCacheConfig::cacheEnabled
	bool ___cacheEnabled_0;
	// System.Int32 PolyToolkit.PolyCacheConfig::maxCacheSizeMb
	int32_t ___maxCacheSizeMb_1;
	// System.Int32 PolyToolkit.PolyCacheConfig::maxCacheEntries
	int32_t ___maxCacheEntries_2;
	// System.String PolyToolkit.PolyCacheConfig::cachePathOverride
	String_t* ___cachePathOverride_3;

public:
	inline static int32_t get_offset_of_cacheEnabled_0() { return static_cast<int32_t>(offsetof(PolyCacheConfig_t1397553534, ___cacheEnabled_0)); }
	inline bool get_cacheEnabled_0() const { return ___cacheEnabled_0; }
	inline bool* get_address_of_cacheEnabled_0() { return &___cacheEnabled_0; }
	inline void set_cacheEnabled_0(bool value)
	{
		___cacheEnabled_0 = value;
	}

	inline static int32_t get_offset_of_maxCacheSizeMb_1() { return static_cast<int32_t>(offsetof(PolyCacheConfig_t1397553534, ___maxCacheSizeMb_1)); }
	inline int32_t get_maxCacheSizeMb_1() const { return ___maxCacheSizeMb_1; }
	inline int32_t* get_address_of_maxCacheSizeMb_1() { return &___maxCacheSizeMb_1; }
	inline void set_maxCacheSizeMb_1(int32_t value)
	{
		___maxCacheSizeMb_1 = value;
	}

	inline static int32_t get_offset_of_maxCacheEntries_2() { return static_cast<int32_t>(offsetof(PolyCacheConfig_t1397553534, ___maxCacheEntries_2)); }
	inline int32_t get_maxCacheEntries_2() const { return ___maxCacheEntries_2; }
	inline int32_t* get_address_of_maxCacheEntries_2() { return &___maxCacheEntries_2; }
	inline void set_maxCacheEntries_2(int32_t value)
	{
		___maxCacheEntries_2 = value;
	}

	inline static int32_t get_offset_of_cachePathOverride_3() { return static_cast<int32_t>(offsetof(PolyCacheConfig_t1397553534, ___cachePathOverride_3)); }
	inline String_t* get_cachePathOverride_3() const { return ___cachePathOverride_3; }
	inline String_t** get_address_of_cachePathOverride_3() { return &___cachePathOverride_3; }
	inline void set_cachePathOverride_3(String_t* value)
	{
		___cachePathOverride_3 = value;
		Il2CppCodeGenWriteBarrier((&___cachePathOverride_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PolyToolkit.PolyCacheConfig
struct PolyCacheConfig_t1397553534_marshaled_pinvoke
{
	int32_t ___cacheEnabled_0;
	int32_t ___maxCacheSizeMb_1;
	int32_t ___maxCacheEntries_2;
	char* ___cachePathOverride_3;
};
// Native definition for COM marshalling of PolyToolkit.PolyCacheConfig
struct PolyCacheConfig_t1397553534_marshaled_com
{
	int32_t ___cacheEnabled_0;
	int32_t ___maxCacheSizeMb_1;
	int32_t ___maxCacheEntries_2;
	Il2CppChar* ___cachePathOverride_3;
};
#endif // POLYCACHECONFIG_T1397553534_H
#ifndef POLYAUTHCONFIG_T4147939475_H
#define POLYAUTHCONFIG_T4147939475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyAuthConfig
struct  PolyAuthConfig_t4147939475 
{
public:
	// System.String PolyToolkit.PolyAuthConfig::apiKey
	String_t* ___apiKey_0;
	// System.String PolyToolkit.PolyAuthConfig::clientId
	String_t* ___clientId_1;
	// System.String PolyToolkit.PolyAuthConfig::clientSecret
	String_t* ___clientSecret_2;
	// System.String[] PolyToolkit.PolyAuthConfig::additionalScopes
	StringU5BU5D_t1281789340* ___additionalScopes_3;
	// System.String PolyToolkit.PolyAuthConfig::serviceName
	String_t* ___serviceName_4;

public:
	inline static int32_t get_offset_of_apiKey_0() { return static_cast<int32_t>(offsetof(PolyAuthConfig_t4147939475, ___apiKey_0)); }
	inline String_t* get_apiKey_0() const { return ___apiKey_0; }
	inline String_t** get_address_of_apiKey_0() { return &___apiKey_0; }
	inline void set_apiKey_0(String_t* value)
	{
		___apiKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___apiKey_0), value);
	}

	inline static int32_t get_offset_of_clientId_1() { return static_cast<int32_t>(offsetof(PolyAuthConfig_t4147939475, ___clientId_1)); }
	inline String_t* get_clientId_1() const { return ___clientId_1; }
	inline String_t** get_address_of_clientId_1() { return &___clientId_1; }
	inline void set_clientId_1(String_t* value)
	{
		___clientId_1 = value;
		Il2CppCodeGenWriteBarrier((&___clientId_1), value);
	}

	inline static int32_t get_offset_of_clientSecret_2() { return static_cast<int32_t>(offsetof(PolyAuthConfig_t4147939475, ___clientSecret_2)); }
	inline String_t* get_clientSecret_2() const { return ___clientSecret_2; }
	inline String_t** get_address_of_clientSecret_2() { return &___clientSecret_2; }
	inline void set_clientSecret_2(String_t* value)
	{
		___clientSecret_2 = value;
		Il2CppCodeGenWriteBarrier((&___clientSecret_2), value);
	}

	inline static int32_t get_offset_of_additionalScopes_3() { return static_cast<int32_t>(offsetof(PolyAuthConfig_t4147939475, ___additionalScopes_3)); }
	inline StringU5BU5D_t1281789340* get_additionalScopes_3() const { return ___additionalScopes_3; }
	inline StringU5BU5D_t1281789340** get_address_of_additionalScopes_3() { return &___additionalScopes_3; }
	inline void set_additionalScopes_3(StringU5BU5D_t1281789340* value)
	{
		___additionalScopes_3 = value;
		Il2CppCodeGenWriteBarrier((&___additionalScopes_3), value);
	}

	inline static int32_t get_offset_of_serviceName_4() { return static_cast<int32_t>(offsetof(PolyAuthConfig_t4147939475, ___serviceName_4)); }
	inline String_t* get_serviceName_4() const { return ___serviceName_4; }
	inline String_t** get_address_of_serviceName_4() { return &___serviceName_4; }
	inline void set_serviceName_4(String_t* value)
	{
		___serviceName_4 = value;
		Il2CppCodeGenWriteBarrier((&___serviceName_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PolyToolkit.PolyAuthConfig
struct PolyAuthConfig_t4147939475_marshaled_pinvoke
{
	char* ___apiKey_0;
	char* ___clientId_1;
	char* ___clientSecret_2;
	char** ___additionalScopes_3;
	char* ___serviceName_4;
};
// Native definition for COM marshalling of PolyToolkit.PolyAuthConfig
struct PolyAuthConfig_t4147939475_marshaled_com
{
	Il2CppChar* ___apiKey_0;
	Il2CppChar* ___clientId_1;
	Il2CppChar* ___clientSecret_2;
	Il2CppChar** ___additionalScopes_3;
	Il2CppChar* ___serviceName_4;
};
#endif // POLYAUTHCONFIG_T4147939475_H
#ifndef ENUMERATOR_T2715315607_H
#define ENUMERATOR_T2715315607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Mesh>
struct  Enumerator_t2715315607 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t826071730 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Mesh_t3648964284 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2715315607, ___l_0)); }
	inline List_1_t826071730 * get_l_0() const { return ___l_0; }
	inline List_1_t826071730 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t826071730 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2715315607, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2715315607, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2715315607, ___current_3)); }
	inline Mesh_t3648964284 * get_current_3() const { return ___current_3; }
	inline Mesh_t3648964284 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Mesh_t3648964284 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2715315607_H
#ifndef POLYSTATUS_T3145373940_H
#define POLYSTATUS_T3145373940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyStatus
struct  PolyStatus_t3145373940 
{
public:
	// System.Boolean PolyToolkit.PolyStatus::ok
	bool ___ok_0;
	// System.String PolyToolkit.PolyStatus::errorMessage
	String_t* ___errorMessage_1;

public:
	inline static int32_t get_offset_of_ok_0() { return static_cast<int32_t>(offsetof(PolyStatus_t3145373940, ___ok_0)); }
	inline bool get_ok_0() const { return ___ok_0; }
	inline bool* get_address_of_ok_0() { return &___ok_0; }
	inline void set_ok_0(bool value)
	{
		___ok_0 = value;
	}

	inline static int32_t get_offset_of_errorMessage_1() { return static_cast<int32_t>(offsetof(PolyStatus_t3145373940, ___errorMessage_1)); }
	inline String_t* get_errorMessage_1() const { return ___errorMessage_1; }
	inline String_t** get_address_of_errorMessage_1() { return &___errorMessage_1; }
	inline void set_errorMessage_1(String_t* value)
	{
		___errorMessage_1 = value;
		Il2CppCodeGenWriteBarrier((&___errorMessage_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PolyToolkit.PolyStatus
struct PolyStatus_t3145373940_marshaled_pinvoke
{
	int32_t ___ok_0;
	char* ___errorMessage_1;
};
// Native definition for COM marshalling of PolyToolkit.PolyStatus
struct PolyStatus_t3145373940_marshaled_com
{
	int32_t ___ok_0;
	Il2CppChar* ___errorMessage_1;
};
#endif // POLYSTATUS_T3145373940_H
#ifndef SAVEDATA_T3667871004_H
#define SAVEDATA_T3667871004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.model.export.SaveData
struct  SaveData_t3667871004 
{
public:
	// System.String PolyToolkitInternal.model.export.SaveData::filenameBase
	String_t* ___filenameBase_0;
	// System.Byte[] PolyToolkitInternal.model.export.SaveData::objFile
	ByteU5BU5D_t4116647657* ___objFile_1;
	// System.Byte[] PolyToolkitInternal.model.export.SaveData::mtlFile
	ByteU5BU5D_t4116647657* ___mtlFile_2;
	// PolyToolkitInternal.model.export.FormatSaveData PolyToolkitInternal.model.export.SaveData::GLTFfiles
	FormatSaveData_t3321373558 * ___GLTFfiles_3;
	// System.Byte[] PolyToolkitInternal.model.export.SaveData::objMtlZip
	ByteU5BU5D_t4116647657* ___objMtlZip_4;
	// System.Byte[] PolyToolkitInternal.model.export.SaveData::polyFile
	ByteU5BU5D_t4116647657* ___polyFile_5;
	// System.Byte[] PolyToolkitInternal.model.export.SaveData::polyZip
	ByteU5BU5D_t4116647657* ___polyZip_6;
	// System.Byte[] PolyToolkitInternal.model.export.SaveData::thumbnailFile
	ByteU5BU5D_t4116647657* ___thumbnailFile_7;

public:
	inline static int32_t get_offset_of_filenameBase_0() { return static_cast<int32_t>(offsetof(SaveData_t3667871004, ___filenameBase_0)); }
	inline String_t* get_filenameBase_0() const { return ___filenameBase_0; }
	inline String_t** get_address_of_filenameBase_0() { return &___filenameBase_0; }
	inline void set_filenameBase_0(String_t* value)
	{
		___filenameBase_0 = value;
		Il2CppCodeGenWriteBarrier((&___filenameBase_0), value);
	}

	inline static int32_t get_offset_of_objFile_1() { return static_cast<int32_t>(offsetof(SaveData_t3667871004, ___objFile_1)); }
	inline ByteU5BU5D_t4116647657* get_objFile_1() const { return ___objFile_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_objFile_1() { return &___objFile_1; }
	inline void set_objFile_1(ByteU5BU5D_t4116647657* value)
	{
		___objFile_1 = value;
		Il2CppCodeGenWriteBarrier((&___objFile_1), value);
	}

	inline static int32_t get_offset_of_mtlFile_2() { return static_cast<int32_t>(offsetof(SaveData_t3667871004, ___mtlFile_2)); }
	inline ByteU5BU5D_t4116647657* get_mtlFile_2() const { return ___mtlFile_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_mtlFile_2() { return &___mtlFile_2; }
	inline void set_mtlFile_2(ByteU5BU5D_t4116647657* value)
	{
		___mtlFile_2 = value;
		Il2CppCodeGenWriteBarrier((&___mtlFile_2), value);
	}

	inline static int32_t get_offset_of_GLTFfiles_3() { return static_cast<int32_t>(offsetof(SaveData_t3667871004, ___GLTFfiles_3)); }
	inline FormatSaveData_t3321373558 * get_GLTFfiles_3() const { return ___GLTFfiles_3; }
	inline FormatSaveData_t3321373558 ** get_address_of_GLTFfiles_3() { return &___GLTFfiles_3; }
	inline void set_GLTFfiles_3(FormatSaveData_t3321373558 * value)
	{
		___GLTFfiles_3 = value;
		Il2CppCodeGenWriteBarrier((&___GLTFfiles_3), value);
	}

	inline static int32_t get_offset_of_objMtlZip_4() { return static_cast<int32_t>(offsetof(SaveData_t3667871004, ___objMtlZip_4)); }
	inline ByteU5BU5D_t4116647657* get_objMtlZip_4() const { return ___objMtlZip_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_objMtlZip_4() { return &___objMtlZip_4; }
	inline void set_objMtlZip_4(ByteU5BU5D_t4116647657* value)
	{
		___objMtlZip_4 = value;
		Il2CppCodeGenWriteBarrier((&___objMtlZip_4), value);
	}

	inline static int32_t get_offset_of_polyFile_5() { return static_cast<int32_t>(offsetof(SaveData_t3667871004, ___polyFile_5)); }
	inline ByteU5BU5D_t4116647657* get_polyFile_5() const { return ___polyFile_5; }
	inline ByteU5BU5D_t4116647657** get_address_of_polyFile_5() { return &___polyFile_5; }
	inline void set_polyFile_5(ByteU5BU5D_t4116647657* value)
	{
		___polyFile_5 = value;
		Il2CppCodeGenWriteBarrier((&___polyFile_5), value);
	}

	inline static int32_t get_offset_of_polyZip_6() { return static_cast<int32_t>(offsetof(SaveData_t3667871004, ___polyZip_6)); }
	inline ByteU5BU5D_t4116647657* get_polyZip_6() const { return ___polyZip_6; }
	inline ByteU5BU5D_t4116647657** get_address_of_polyZip_6() { return &___polyZip_6; }
	inline void set_polyZip_6(ByteU5BU5D_t4116647657* value)
	{
		___polyZip_6 = value;
		Il2CppCodeGenWriteBarrier((&___polyZip_6), value);
	}

	inline static int32_t get_offset_of_thumbnailFile_7() { return static_cast<int32_t>(offsetof(SaveData_t3667871004, ___thumbnailFile_7)); }
	inline ByteU5BU5D_t4116647657* get_thumbnailFile_7() const { return ___thumbnailFile_7; }
	inline ByteU5BU5D_t4116647657** get_address_of_thumbnailFile_7() { return &___thumbnailFile_7; }
	inline void set_thumbnailFile_7(ByteU5BU5D_t4116647657* value)
	{
		___thumbnailFile_7 = value;
		Il2CppCodeGenWriteBarrier((&___thumbnailFile_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PolyToolkitInternal.model.export.SaveData
struct SaveData_t3667871004_marshaled_pinvoke
{
	char* ___filenameBase_0;
	uint8_t* ___objFile_1;
	uint8_t* ___mtlFile_2;
	FormatSaveData_t3321373558 * ___GLTFfiles_3;
	uint8_t* ___objMtlZip_4;
	uint8_t* ___polyFile_5;
	uint8_t* ___polyZip_6;
	uint8_t* ___thumbnailFile_7;
};
// Native definition for COM marshalling of PolyToolkitInternal.model.export.SaveData
struct SaveData_t3667871004_marshaled_com
{
	Il2CppChar* ___filenameBase_0;
	uint8_t* ___objFile_1;
	uint8_t* ___mtlFile_2;
	FormatSaveData_t3321373558 * ___GLTFfiles_3;
	uint8_t* ___objMtlZip_4;
	uint8_t* ___polyFile_5;
	uint8_t* ___polyZip_6;
	uint8_t* ___thumbnailFile_7;
};
#endif // SAVEDATA_T3667871004_H
#ifndef INTRANGE_T4091832750_H
#define INTRANGE_T4091832750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.IntRange
struct  IntRange_t4091832750 
{
public:
	// System.Int32 PolyToolkitInternal.IntRange::min
	int32_t ___min_0;
	// System.Int32 PolyToolkitInternal.IntRange::max
	int32_t ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(IntRange_t4091832750, ___min_0)); }
	inline int32_t get_min_0() const { return ___min_0; }
	inline int32_t* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(int32_t value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(IntRange_t4091832750, ___max_1)); }
	inline int32_t get_max_1() const { return ___max_1; }
	inline int32_t* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(int32_t value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTRANGE_T4091832750_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef UNITYMATERIAL_T3949128076_H
#define UNITYMATERIAL_T3949128076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfMaterialConverter/UnityMaterial
struct  UnityMaterial_t3949128076 
{
public:
	// UnityEngine.Material PolyToolkitInternal.GltfMaterialConverter/UnityMaterial::material
	Material_t340375123 * ___material_0;
	// UnityEngine.Material PolyToolkitInternal.GltfMaterialConverter/UnityMaterial::template
	Material_t340375123 * ___template_1;

public:
	inline static int32_t get_offset_of_material_0() { return static_cast<int32_t>(offsetof(UnityMaterial_t3949128076, ___material_0)); }
	inline Material_t340375123 * get_material_0() const { return ___material_0; }
	inline Material_t340375123 ** get_address_of_material_0() { return &___material_0; }
	inline void set_material_0(Material_t340375123 * value)
	{
		___material_0 = value;
		Il2CppCodeGenWriteBarrier((&___material_0), value);
	}

	inline static int32_t get_offset_of_template_1() { return static_cast<int32_t>(offsetof(UnityMaterial_t3949128076, ___template_1)); }
	inline Material_t340375123 * get_template_1() const { return ___template_1; }
	inline Material_t340375123 ** get_address_of_template_1() { return &___template_1; }
	inline void set_template_1(Material_t340375123 * value)
	{
		___template_1 = value;
		Il2CppCodeGenWriteBarrier((&___template_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PolyToolkitInternal.GltfMaterialConverter/UnityMaterial
struct UnityMaterial_t3949128076_marshaled_pinvoke
{
	Material_t340375123 * ___material_0;
	Material_t340375123 * ___template_1;
};
// Native definition for COM marshalling of PolyToolkitInternal.GltfMaterialConverter/UnityMaterial
struct UnityMaterial_t3949128076_marshaled_com
{
	Material_t340375123 * ___material_0;
	Material_t340375123 * ___template_1;
};
#endif // UNITYMATERIAL_T3949128076_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef POLYFORMATTYPE_T1021068395_H
#define POLYFORMATTYPE_T1021068395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyFormatType
struct  PolyFormatType_t1021068395 
{
public:
	// System.Int32 PolyToolkit.PolyFormatType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PolyFormatType_t1021068395, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYFORMATTYPE_T1021068395_H
#ifndef POLYASSETLICENSE_T558986990_H
#define POLYASSETLICENSE_T558986990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyAssetLicense
struct  PolyAssetLicense_t558986990 
{
public:
	// System.Int32 PolyToolkit.PolyAssetLicense::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PolyAssetLicense_t558986990, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYASSETLICENSE_T558986990_H
#ifndef POLYVISIBILITYFILTER_T769998791_H
#define POLYVISIBILITYFILTER_T769998791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyVisibilityFilter
struct  PolyVisibilityFilter_t769998791 
{
public:
	// System.Int32 PolyToolkit.PolyVisibilityFilter::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PolyVisibilityFilter_t769998791, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYVISIBILITYFILTER_T769998791_H
#ifndef POLYBASERESULT_T2195255688_H
#define POLYBASERESULT_T2195255688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyBaseResult
struct  PolyBaseResult_t2195255688  : public RuntimeObject
{
public:
	// PolyToolkit.PolyStatus PolyToolkit.PolyBaseResult::status
	PolyStatus_t3145373940  ___status_0;

public:
	inline static int32_t get_offset_of_status_0() { return static_cast<int32_t>(offsetof(PolyBaseResult_t2195255688, ___status_0)); }
	inline PolyStatus_t3145373940  get_status_0() const { return ___status_0; }
	inline PolyStatus_t3145373940 * get_address_of_status_0() { return &___status_0; }
	inline void set_status_0(PolyStatus_t3145373940  value)
	{
		___status_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYBASERESULT_T2195255688_H
#ifndef LENGTHUNIT_T272761749_H
#define LENGTHUNIT_T272761749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.LengthUnit
struct  LengthUnit_t272761749 
{
public:
	// System.Int32 PolyToolkitInternal.LengthUnit::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LengthUnit_t272761749, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LENGTHUNIT_T272761749_H
#ifndef NULLABLE_1_T3540463925_H
#define NULLABLE_1_T3540463925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Matrix4x4>
struct  Nullable_1_t3540463925 
{
public:
	// T System.Nullable`1::value
	Matrix4x4_t1817901843  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3540463925, ___value_0)); }
	inline Matrix4x4_t1817901843  get_value_0() const { return ___value_0; }
	inline Matrix4x4_t1817901843 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Matrix4x4_t1817901843  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3540463925, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3540463925_H
#ifndef POLYMAXCOMPLEXITYFILTER_T2510204215_H
#define POLYMAXCOMPLEXITYFILTER_T2510204215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyMaxComplexityFilter
struct  PolyMaxComplexityFilter_t2510204215 
{
public:
	// System.Int32 PolyToolkit.PolyMaxComplexityFilter::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PolyMaxComplexityFilter_t2510204215, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYMAXCOMPLEXITYFILTER_T2510204215_H
#ifndef POLYFORMATFILTER_T1920599779_H
#define POLYFORMATFILTER_T1920599779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyFormatFilter
struct  PolyFormatFilter_t1920599779 
{
public:
	// System.Int32 PolyToolkit.PolyFormatFilter::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PolyFormatFilter_t1920599779, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYFORMATFILTER_T1920599779_H
#ifndef POLYORDERBY_T1813925015_H
#define POLYORDERBY_T1813925015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyOrderBy
struct  PolyOrderBy_t1813925015 
{
public:
	// System.Int32 PolyToolkit.PolyOrderBy::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PolyOrderBy_t1813925015, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYORDERBY_T1813925015_H
#ifndef POLYCATEGORY_T3361242767_H
#define POLYCATEGORY_T3361242767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyCategory
struct  PolyCategory_t3361242767 
{
public:
	// System.Int32 PolyToolkit.PolyCategory::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PolyCategory_t3361242767, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYCATEGORY_T3361242767_H
#ifndef POLYVISIBILITY_T305223869_H
#define POLYVISIBILITY_T305223869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyVisibility
struct  PolyVisibility_t305223869 
{
public:
	// System.Int32 PolyToolkit.PolyVisibility::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PolyVisibility_t305223869, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYVISIBILITY_T305223869_H
#ifndef RESCALINGMODE_T410920913_H
#define RESCALINGMODE_T410920913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyImportOptions/RescalingMode
struct  RescalingMode_t410920913 
{
public:
	// System.Int32 PolyToolkit.PolyImportOptions/RescalingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RescalingMode_t410920913, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESCALINGMODE_T410920913_H
#ifndef HELPTEXTATTRIBUTE_T979818096_H
#define HELPTEXTATTRIBUTE_T979818096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.HelpTextAttribute
struct  HelpTextAttribute_t979818096  : public PropertyAttribute_t3677895545
{
public:
	// System.String PolyToolkitInternal.HelpTextAttribute::text
	String_t* ___text_0;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(HelpTextAttribute_t979818096, ___text_0)); }
	inline String_t* get_text_0() const { return ___text_0; }
	inline String_t** get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(String_t* value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier((&___text_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HELPTEXTATTRIBUTE_T979818096_H
#ifndef MESHSUBSET_T1957016349_H
#define MESHSUBSET_T1957016349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.ImportGltf/MeshSubset
struct  MeshSubset_t1957016349 
{
public:
	// PolyToolkitInternal.IntRange PolyToolkitInternal.ImportGltf/MeshSubset::vertices
	IntRange_t4091832750  ___vertices_0;
	// PolyToolkitInternal.IntRange PolyToolkitInternal.ImportGltf/MeshSubset::triangles
	IntRange_t4091832750  ___triangles_1;

public:
	inline static int32_t get_offset_of_vertices_0() { return static_cast<int32_t>(offsetof(MeshSubset_t1957016349, ___vertices_0)); }
	inline IntRange_t4091832750  get_vertices_0() const { return ___vertices_0; }
	inline IntRange_t4091832750 * get_address_of_vertices_0() { return &___vertices_0; }
	inline void set_vertices_0(IntRange_t4091832750  value)
	{
		___vertices_0 = value;
	}

	inline static int32_t get_offset_of_triangles_1() { return static_cast<int32_t>(offsetof(MeshSubset_t1957016349, ___triangles_1)); }
	inline IntRange_t4091832750  get_triangles_1() const { return ___triangles_1; }
	inline IntRange_t4091832750 * get_address_of_triangles_1() { return &___triangles_1; }
	inline void set_triangles_1(IntRange_t4091832750  value)
	{
		___triangles_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHSUBSET_T1957016349_H
#ifndef IMPORTGLTF_T3245133338_H
#define IMPORTGLTF_T3245133338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.ImportGltf
struct  ImportGltf_t3245133338  : public RuntimeObject
{
public:

public:
};

struct ImportGltf_t3245133338_StaticFields
{
public:
	// Newtonsoft.Json.JsonSerializer PolyToolkitInternal.ImportGltf::kSerializer
	JsonSerializer_t1424496335 * ___kSerializer_2;
	// UnityEngine.Matrix4x4 PolyToolkitInternal.ImportGltf::<GltfFromUnity>k__BackingField
	Matrix4x4_t1817901843  ___U3CGltfFromUnityU3Ek__BackingField_5;
	// UnityEngine.Matrix4x4 PolyToolkitInternal.ImportGltf::<UnityFromGltf>k__BackingField
	Matrix4x4_t1817901843  ___U3CUnityFromGltfU3Ek__BackingField_6;
	// System.Func`2<PolyToolkitInternal.GltfTextureBase,PolyToolkitInternal.GltfImageBase> PolyToolkitInternal.ImportGltf::<>f__am$cache0
	Func_2_t747370532 * ___U3CU3Ef__amU24cache0_7;
	// System.Func`2<PolyToolkitInternal.GltfImageBase,System.Boolean> PolyToolkitInternal.ImportGltf::<>f__am$cache1
	Func_2_t3992130116 * ___U3CU3Ef__amU24cache1_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PolyToolkitInternal.ImportGltf::<>f__switch$map0
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map0_9;

public:
	inline static int32_t get_offset_of_kSerializer_2() { return static_cast<int32_t>(offsetof(ImportGltf_t3245133338_StaticFields, ___kSerializer_2)); }
	inline JsonSerializer_t1424496335 * get_kSerializer_2() const { return ___kSerializer_2; }
	inline JsonSerializer_t1424496335 ** get_address_of_kSerializer_2() { return &___kSerializer_2; }
	inline void set_kSerializer_2(JsonSerializer_t1424496335 * value)
	{
		___kSerializer_2 = value;
		Il2CppCodeGenWriteBarrier((&___kSerializer_2), value);
	}

	inline static int32_t get_offset_of_U3CGltfFromUnityU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ImportGltf_t3245133338_StaticFields, ___U3CGltfFromUnityU3Ek__BackingField_5)); }
	inline Matrix4x4_t1817901843  get_U3CGltfFromUnityU3Ek__BackingField_5() const { return ___U3CGltfFromUnityU3Ek__BackingField_5; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CGltfFromUnityU3Ek__BackingField_5() { return &___U3CGltfFromUnityU3Ek__BackingField_5; }
	inline void set_U3CGltfFromUnityU3Ek__BackingField_5(Matrix4x4_t1817901843  value)
	{
		___U3CGltfFromUnityU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CUnityFromGltfU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ImportGltf_t3245133338_StaticFields, ___U3CUnityFromGltfU3Ek__BackingField_6)); }
	inline Matrix4x4_t1817901843  get_U3CUnityFromGltfU3Ek__BackingField_6() const { return ___U3CUnityFromGltfU3Ek__BackingField_6; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CUnityFromGltfU3Ek__BackingField_6() { return &___U3CUnityFromGltfU3Ek__BackingField_6; }
	inline void set_U3CUnityFromGltfU3Ek__BackingField_6(Matrix4x4_t1817901843  value)
	{
		___U3CUnityFromGltfU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(ImportGltf_t3245133338_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Func_2_t747370532 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Func_2_t747370532 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Func_2_t747370532 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_8() { return static_cast<int32_t>(offsetof(ImportGltf_t3245133338_StaticFields, ___U3CU3Ef__amU24cache1_8)); }
	inline Func_2_t3992130116 * get_U3CU3Ef__amU24cache1_8() const { return ___U3CU3Ef__amU24cache1_8; }
	inline Func_2_t3992130116 ** get_address_of_U3CU3Ef__amU24cache1_8() { return &___U3CU3Ef__amU24cache1_8; }
	inline void set_U3CU3Ef__amU24cache1_8(Func_2_t3992130116 * value)
	{
		___U3CU3Ef__amU24cache1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_9() { return static_cast<int32_t>(offsetof(ImportGltf_t3245133338_StaticFields, ___U3CU3Ef__switchU24map0_9)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map0_9() const { return ___U3CU3Ef__switchU24map0_9; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map0_9() { return &___U3CU3Ef__switchU24map0_9; }
	inline void set_U3CU3Ef__switchU24map0_9(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMPORTGLTF_T3245133338_H
#ifndef SEMANTIC_T602446212_H
#define SEMANTIC_T602446212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TiltBrushToolkit.BrushDescriptor/Semantic
struct  Semantic_t602446212 
{
public:
	// System.Int32 TiltBrushToolkit.BrushDescriptor/Semantic::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Semantic_t602446212, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEMANTIC_T602446212_H
#ifndef COLORSPACE_T3453996949_H
#define COLORSPACE_T3453996949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ColorSpace
struct  ColorSpace_t3453996949 
{
public:
	// System.Int32 UnityEngine.ColorSpace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorSpace_t3453996949, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSPACE_T3453996949_H
#ifndef U3CCREATEGAMEOBJECTSFROMNODEU3EC__ITERATOR1_T2009757091_H
#define U3CCREATEGAMEOBJECTSFROMNODEU3EC__ITERATOR1_T2009757091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNode>c__Iterator1
struct  U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091  : public RuntimeObject
{
public:
	// PolyToolkitInternal.GltfNodeBase PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNode>c__Iterator1::node
	GltfNodeBase_t3005766873 * ___node_0;
	// UnityEngine.GameObject PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNode>c__Iterator1::<obj>__0
	GameObject_t1113636619 * ___U3CobjU3E__0_1;
	// UnityEngine.Transform PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNode>c__Iterator1::parent
	Transform_t3600365921 * ___parent_2;
	// PolyToolkitInternal.ImportGltf/ImportState PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNode>c__Iterator1::state
	ImportState_t482765084 * ___state_3;
	// UnityEngine.Vector3 PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNode>c__Iterator1::translationToApply
	Vector3_t3722313464  ___translationToApply_4;
	// PolyToolkitInternal.ImportGltf/GltfImportResult PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNode>c__Iterator1::result
	GltfImportResult_t3877923054 * ___result_5;
	// PolyToolkitInternal.GltfMaterialConverter PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNode>c__Iterator1::matConverter
	GltfMaterialConverter_t1532388150 * ___matConverter_6;
	// System.Collections.IEnumerator PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNode>c__Iterator1::$locvar0
	RuntimeObject* ___U24locvar0_7;
	// System.Object PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNode>c__Iterator1::<unused>__1
	RuntimeObject * ___U3CunusedU3E__1_8;
	// System.IDisposable PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNode>c__Iterator1::$locvar1
	RuntimeObject* ___U24locvar1_9;
	// System.Collections.Generic.IEnumerator`1<PolyToolkitInternal.GltfNodeBase> PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNode>c__Iterator1::$locvar2
	RuntimeObject* ___U24locvar2_10;
	// PolyToolkitInternal.GltfNodeBase PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNode>c__Iterator1::<childNode>__2
	GltfNodeBase_t3005766873 * ___U3CchildNodeU3E__2_11;
	// System.Collections.IEnumerator PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNode>c__Iterator1::$locvar3
	RuntimeObject* ___U24locvar3_12;
	// System.Object PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNode>c__Iterator1::<unused>__3
	RuntimeObject * ___U3CunusedU3E__3_13;
	// System.IDisposable PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNode>c__Iterator1::$locvar4
	RuntimeObject* ___U24locvar4_14;
	// System.Object PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNode>c__Iterator1::$current
	RuntimeObject * ___U24current_15;
	// System.Boolean PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNode>c__Iterator1::$disposing
	bool ___U24disposing_16;
	// System.Int32 PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromNode>c__Iterator1::$PC
	int32_t ___U24PC_17;

public:
	inline static int32_t get_offset_of_node_0() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091, ___node_0)); }
	inline GltfNodeBase_t3005766873 * get_node_0() const { return ___node_0; }
	inline GltfNodeBase_t3005766873 ** get_address_of_node_0() { return &___node_0; }
	inline void set_node_0(GltfNodeBase_t3005766873 * value)
	{
		___node_0 = value;
		Il2CppCodeGenWriteBarrier((&___node_0), value);
	}

	inline static int32_t get_offset_of_U3CobjU3E__0_1() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091, ___U3CobjU3E__0_1)); }
	inline GameObject_t1113636619 * get_U3CobjU3E__0_1() const { return ___U3CobjU3E__0_1; }
	inline GameObject_t1113636619 ** get_address_of_U3CobjU3E__0_1() { return &___U3CobjU3E__0_1; }
	inline void set_U3CobjU3E__0_1(GameObject_t1113636619 * value)
	{
		___U3CobjU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CobjU3E__0_1), value);
	}

	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091, ___parent_2)); }
	inline Transform_t3600365921 * get_parent_2() const { return ___parent_2; }
	inline Transform_t3600365921 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(Transform_t3600365921 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((&___parent_2), value);
	}

	inline static int32_t get_offset_of_state_3() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091, ___state_3)); }
	inline ImportState_t482765084 * get_state_3() const { return ___state_3; }
	inline ImportState_t482765084 ** get_address_of_state_3() { return &___state_3; }
	inline void set_state_3(ImportState_t482765084 * value)
	{
		___state_3 = value;
		Il2CppCodeGenWriteBarrier((&___state_3), value);
	}

	inline static int32_t get_offset_of_translationToApply_4() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091, ___translationToApply_4)); }
	inline Vector3_t3722313464  get_translationToApply_4() const { return ___translationToApply_4; }
	inline Vector3_t3722313464 * get_address_of_translationToApply_4() { return &___translationToApply_4; }
	inline void set_translationToApply_4(Vector3_t3722313464  value)
	{
		___translationToApply_4 = value;
	}

	inline static int32_t get_offset_of_result_5() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091, ___result_5)); }
	inline GltfImportResult_t3877923054 * get_result_5() const { return ___result_5; }
	inline GltfImportResult_t3877923054 ** get_address_of_result_5() { return &___result_5; }
	inline void set_result_5(GltfImportResult_t3877923054 * value)
	{
		___result_5 = value;
		Il2CppCodeGenWriteBarrier((&___result_5), value);
	}

	inline static int32_t get_offset_of_matConverter_6() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091, ___matConverter_6)); }
	inline GltfMaterialConverter_t1532388150 * get_matConverter_6() const { return ___matConverter_6; }
	inline GltfMaterialConverter_t1532388150 ** get_address_of_matConverter_6() { return &___matConverter_6; }
	inline void set_matConverter_6(GltfMaterialConverter_t1532388150 * value)
	{
		___matConverter_6 = value;
		Il2CppCodeGenWriteBarrier((&___matConverter_6), value);
	}

	inline static int32_t get_offset_of_U24locvar0_7() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091, ___U24locvar0_7)); }
	inline RuntimeObject* get_U24locvar0_7() const { return ___U24locvar0_7; }
	inline RuntimeObject** get_address_of_U24locvar0_7() { return &___U24locvar0_7; }
	inline void set_U24locvar0_7(RuntimeObject* value)
	{
		___U24locvar0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_7), value);
	}

	inline static int32_t get_offset_of_U3CunusedU3E__1_8() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091, ___U3CunusedU3E__1_8)); }
	inline RuntimeObject * get_U3CunusedU3E__1_8() const { return ___U3CunusedU3E__1_8; }
	inline RuntimeObject ** get_address_of_U3CunusedU3E__1_8() { return &___U3CunusedU3E__1_8; }
	inline void set_U3CunusedU3E__1_8(RuntimeObject * value)
	{
		___U3CunusedU3E__1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CunusedU3E__1_8), value);
	}

	inline static int32_t get_offset_of_U24locvar1_9() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091, ___U24locvar1_9)); }
	inline RuntimeObject* get_U24locvar1_9() const { return ___U24locvar1_9; }
	inline RuntimeObject** get_address_of_U24locvar1_9() { return &___U24locvar1_9; }
	inline void set_U24locvar1_9(RuntimeObject* value)
	{
		___U24locvar1_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar1_9), value);
	}

	inline static int32_t get_offset_of_U24locvar2_10() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091, ___U24locvar2_10)); }
	inline RuntimeObject* get_U24locvar2_10() const { return ___U24locvar2_10; }
	inline RuntimeObject** get_address_of_U24locvar2_10() { return &___U24locvar2_10; }
	inline void set_U24locvar2_10(RuntimeObject* value)
	{
		___U24locvar2_10 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar2_10), value);
	}

	inline static int32_t get_offset_of_U3CchildNodeU3E__2_11() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091, ___U3CchildNodeU3E__2_11)); }
	inline GltfNodeBase_t3005766873 * get_U3CchildNodeU3E__2_11() const { return ___U3CchildNodeU3E__2_11; }
	inline GltfNodeBase_t3005766873 ** get_address_of_U3CchildNodeU3E__2_11() { return &___U3CchildNodeU3E__2_11; }
	inline void set_U3CchildNodeU3E__2_11(GltfNodeBase_t3005766873 * value)
	{
		___U3CchildNodeU3E__2_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CchildNodeU3E__2_11), value);
	}

	inline static int32_t get_offset_of_U24locvar3_12() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091, ___U24locvar3_12)); }
	inline RuntimeObject* get_U24locvar3_12() const { return ___U24locvar3_12; }
	inline RuntimeObject** get_address_of_U24locvar3_12() { return &___U24locvar3_12; }
	inline void set_U24locvar3_12(RuntimeObject* value)
	{
		___U24locvar3_12 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar3_12), value);
	}

	inline static int32_t get_offset_of_U3CunusedU3E__3_13() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091, ___U3CunusedU3E__3_13)); }
	inline RuntimeObject * get_U3CunusedU3E__3_13() const { return ___U3CunusedU3E__3_13; }
	inline RuntimeObject ** get_address_of_U3CunusedU3E__3_13() { return &___U3CunusedU3E__3_13; }
	inline void set_U3CunusedU3E__3_13(RuntimeObject * value)
	{
		___U3CunusedU3E__3_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CunusedU3E__3_13), value);
	}

	inline static int32_t get_offset_of_U24locvar4_14() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091, ___U24locvar4_14)); }
	inline RuntimeObject* get_U24locvar4_14() const { return ___U24locvar4_14; }
	inline RuntimeObject** get_address_of_U24locvar4_14() { return &___U24locvar4_14; }
	inline void set_U24locvar4_14(RuntimeObject* value)
	{
		___U24locvar4_14 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar4_14), value);
	}

	inline static int32_t get_offset_of_U24current_15() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091, ___U24current_15)); }
	inline RuntimeObject * get_U24current_15() const { return ___U24current_15; }
	inline RuntimeObject ** get_address_of_U24current_15() { return &___U24current_15; }
	inline void set_U24current_15(RuntimeObject * value)
	{
		___U24current_15 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_15), value);
	}

	inline static int32_t get_offset_of_U24disposing_16() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091, ___U24disposing_16)); }
	inline bool get_U24disposing_16() const { return ___U24disposing_16; }
	inline bool* get_address_of_U24disposing_16() { return &___U24disposing_16; }
	inline void set_U24disposing_16(bool value)
	{
		___U24disposing_16 = value;
	}

	inline static int32_t get_offset_of_U24PC_17() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091, ___U24PC_17)); }
	inline int32_t get_U24PC_17() const { return ___U24PC_17; }
	inline int32_t* get_address_of_U24PC_17() { return &___U24PC_17; }
	inline void set_U24PC_17(int32_t value)
	{
		___U24PC_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATEGAMEOBJECTSFROMNODEU3EC__ITERATOR1_T2009757091_H
#ifndef COLORSPACE_T1646841580_H
#define COLORSPACE_T1646841580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.ImportGltf/ColorSpace
struct  ColorSpace_t1646841580 
{
public:
	// System.Int32 PolyToolkitInternal.ImportGltf/ColorSpace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorSpace_t1646841580, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORSPACE_T1646841580_H
#ifndef U3CHANDLEWEBREQUESTU3EC__ITERATOR0_T3773947286_H
#define U3CHANDLEWEBREQUESTU3EC__ITERATOR0_T3773947286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.client.model.util.WebRequestManager/<HandleWebRequest>c__Iterator0
struct  U3CHandleWebRequestU3Ec__Iterator0_t3773947286  : public RuntimeObject
{
public:
	// PolyToolkitInternal.client.model.util.WebRequestManager/PendingRequest PolyToolkitInternal.client.model.util.WebRequestManager/<HandleWebRequest>c__Iterator0::request
	PendingRequest_t652293891 * ___request_0;
	// UnityEngine.Networking.UnityWebRequest PolyToolkitInternal.client.model.util.WebRequestManager/<HandleWebRequest>c__Iterator0::<webRequest>__0
	UnityWebRequest_t463507806 * ___U3CwebRequestU3E__0_1;
	// System.Boolean PolyToolkitInternal.client.model.util.WebRequestManager/<HandleWebRequest>c__Iterator0::<cacheAllowed>__0
	bool ___U3CcacheAllowedU3E__0_2;
	// PolyToolkitInternal.client.model.util.WebRequestManager/BufferHolder PolyToolkitInternal.client.model.util.WebRequestManager/<HandleWebRequest>c__Iterator0::bufferHolder
	BufferHolder_t424688828 * ___bufferHolder_3;
	// UnityEngine.Networking.DownloadHandlerBuffer PolyToolkitInternal.client.model.util.WebRequestManager/<HandleWebRequest>c__Iterator0::<handler>__0
	DownloadHandlerBuffer_t2928496527 * ___U3ChandlerU3E__0_4;
	// PolyToolkit.PolyStatus PolyToolkitInternal.client.model.util.WebRequestManager/<HandleWebRequest>c__Iterator0::<status>__0
	PolyStatus_t3145373940  ___U3CstatusU3E__0_5;
	// PolyToolkitInternal.client.model.util.WebRequestManager PolyToolkitInternal.client.model.util.WebRequestManager/<HandleWebRequest>c__Iterator0::$this
	WebRequestManager_t2098015550 * ___U24this_6;
	// System.Object PolyToolkitInternal.client.model.util.WebRequestManager/<HandleWebRequest>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean PolyToolkitInternal.client.model.util.WebRequestManager/<HandleWebRequest>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 PolyToolkitInternal.client.model.util.WebRequestManager/<HandleWebRequest>c__Iterator0::$PC
	int32_t ___U24PC_9;
	// PolyToolkitInternal.client.model.util.WebRequestManager/<HandleWebRequest>c__Iterator0/<HandleWebRequest>c__AnonStorey1 PolyToolkitInternal.client.model.util.WebRequestManager/<HandleWebRequest>c__Iterator0::$locvar0
	U3CHandleWebRequestU3Ec__AnonStorey1_t2411719362 * ___U24locvar0_10;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(U3CHandleWebRequestU3Ec__Iterator0_t3773947286, ___request_0)); }
	inline PendingRequest_t652293891 * get_request_0() const { return ___request_0; }
	inline PendingRequest_t652293891 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(PendingRequest_t652293891 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}

	inline static int32_t get_offset_of_U3CwebRequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CHandleWebRequestU3Ec__Iterator0_t3773947286, ___U3CwebRequestU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CwebRequestU3E__0_1() const { return ___U3CwebRequestU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwebRequestU3E__0_1() { return &___U3CwebRequestU3E__0_1; }
	inline void set_U3CwebRequestU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CwebRequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwebRequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CcacheAllowedU3E__0_2() { return static_cast<int32_t>(offsetof(U3CHandleWebRequestU3Ec__Iterator0_t3773947286, ___U3CcacheAllowedU3E__0_2)); }
	inline bool get_U3CcacheAllowedU3E__0_2() const { return ___U3CcacheAllowedU3E__0_2; }
	inline bool* get_address_of_U3CcacheAllowedU3E__0_2() { return &___U3CcacheAllowedU3E__0_2; }
	inline void set_U3CcacheAllowedU3E__0_2(bool value)
	{
		___U3CcacheAllowedU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_bufferHolder_3() { return static_cast<int32_t>(offsetof(U3CHandleWebRequestU3Ec__Iterator0_t3773947286, ___bufferHolder_3)); }
	inline BufferHolder_t424688828 * get_bufferHolder_3() const { return ___bufferHolder_3; }
	inline BufferHolder_t424688828 ** get_address_of_bufferHolder_3() { return &___bufferHolder_3; }
	inline void set_bufferHolder_3(BufferHolder_t424688828 * value)
	{
		___bufferHolder_3 = value;
		Il2CppCodeGenWriteBarrier((&___bufferHolder_3), value);
	}

	inline static int32_t get_offset_of_U3ChandlerU3E__0_4() { return static_cast<int32_t>(offsetof(U3CHandleWebRequestU3Ec__Iterator0_t3773947286, ___U3ChandlerU3E__0_4)); }
	inline DownloadHandlerBuffer_t2928496527 * get_U3ChandlerU3E__0_4() const { return ___U3ChandlerU3E__0_4; }
	inline DownloadHandlerBuffer_t2928496527 ** get_address_of_U3ChandlerU3E__0_4() { return &___U3ChandlerU3E__0_4; }
	inline void set_U3ChandlerU3E__0_4(DownloadHandlerBuffer_t2928496527 * value)
	{
		___U3ChandlerU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3ChandlerU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CstatusU3E__0_5() { return static_cast<int32_t>(offsetof(U3CHandleWebRequestU3Ec__Iterator0_t3773947286, ___U3CstatusU3E__0_5)); }
	inline PolyStatus_t3145373940  get_U3CstatusU3E__0_5() const { return ___U3CstatusU3E__0_5; }
	inline PolyStatus_t3145373940 * get_address_of_U3CstatusU3E__0_5() { return &___U3CstatusU3E__0_5; }
	inline void set_U3CstatusU3E__0_5(PolyStatus_t3145373940  value)
	{
		___U3CstatusU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CHandleWebRequestU3Ec__Iterator0_t3773947286, ___U24this_6)); }
	inline WebRequestManager_t2098015550 * get_U24this_6() const { return ___U24this_6; }
	inline WebRequestManager_t2098015550 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(WebRequestManager_t2098015550 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CHandleWebRequestU3Ec__Iterator0_t3773947286, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CHandleWebRequestU3Ec__Iterator0_t3773947286, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CHandleWebRequestU3Ec__Iterator0_t3773947286, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_10() { return static_cast<int32_t>(offsetof(U3CHandleWebRequestU3Ec__Iterator0_t3773947286, ___U24locvar0_10)); }
	inline U3CHandleWebRequestU3Ec__AnonStorey1_t2411719362 * get_U24locvar0_10() const { return ___U24locvar0_10; }
	inline U3CHandleWebRequestU3Ec__AnonStorey1_t2411719362 ** get_address_of_U24locvar0_10() { return &___U24locvar0_10; }
	inline void set_U24locvar0_10(U3CHandleWebRequestU3Ec__AnonStorey1_t2411719362 * value)
	{
		___U24locvar0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHANDLEWEBREQUESTU3EC__ITERATOR0_T3773947286_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef NULLABLE_1_T1376722862_H
#define NULLABLE_1_T1376722862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<PolyToolkitInternal.GltfMaterialConverter/UnityMaterial>
struct  Nullable_1_t1376722862 
{
public:
	// T System.Nullable`1::value
	UnityMaterial_t3949128076  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1376722862, ___value_0)); }
	inline UnityMaterial_t3949128076  get_value_0() const { return ___value_0; }
	inline UnityMaterial_t3949128076 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(UnityMaterial_t3949128076  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1376722862, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1376722862_H
#ifndef NULLABLE_1_T1519427536_H
#define NULLABLE_1_T1519427536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<PolyToolkitInternal.IntRange>
struct  Nullable_1_t1519427536 
{
public:
	// T System.Nullable`1::value
	IntRange_t4091832750  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1519427536, ___value_0)); }
	inline IntRange_t4091832750  get_value_0() const { return ___value_0; }
	inline IntRange_t4091832750 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(IntRange_t4091832750  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1519427536, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1519427536_H
#ifndef IMPORTSTATE_T482765084_H
#define IMPORTSTATE_T482765084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.ImportGltf/ImportState
struct  ImportState_t482765084  : public RuntimeObject
{
public:
	// PolyToolkitInternal.GltfRootBase PolyToolkitInternal.ImportGltf/ImportState::root
	GltfRootBase_t3065879770 * ___root_0;
	// PolyToolkitInternal.GltfSceneBase PolyToolkitInternal.ImportGltf/ImportState::desiredScene
	GltfSceneBase_t3309998329 * ___desiredScene_1;
	// System.Single PolyToolkitInternal.ImportGltf/ImportState::scaleFactor
	float ___scaleFactor_2;
	// System.Single PolyToolkitInternal.ImportGltf/ImportState::nodeScaleFactor
	float ___nodeScaleFactor_3;
	// UnityEngine.Vector3 PolyToolkitInternal.ImportGltf/ImportState::translation
	Vector3_t3722313464  ___translation_4;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(ImportState_t482765084, ___root_0)); }
	inline GltfRootBase_t3065879770 * get_root_0() const { return ___root_0; }
	inline GltfRootBase_t3065879770 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(GltfRootBase_t3065879770 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_desiredScene_1() { return static_cast<int32_t>(offsetof(ImportState_t482765084, ___desiredScene_1)); }
	inline GltfSceneBase_t3309998329 * get_desiredScene_1() const { return ___desiredScene_1; }
	inline GltfSceneBase_t3309998329 ** get_address_of_desiredScene_1() { return &___desiredScene_1; }
	inline void set_desiredScene_1(GltfSceneBase_t3309998329 * value)
	{
		___desiredScene_1 = value;
		Il2CppCodeGenWriteBarrier((&___desiredScene_1), value);
	}

	inline static int32_t get_offset_of_scaleFactor_2() { return static_cast<int32_t>(offsetof(ImportState_t482765084, ___scaleFactor_2)); }
	inline float get_scaleFactor_2() const { return ___scaleFactor_2; }
	inline float* get_address_of_scaleFactor_2() { return &___scaleFactor_2; }
	inline void set_scaleFactor_2(float value)
	{
		___scaleFactor_2 = value;
	}

	inline static int32_t get_offset_of_nodeScaleFactor_3() { return static_cast<int32_t>(offsetof(ImportState_t482765084, ___nodeScaleFactor_3)); }
	inline float get_nodeScaleFactor_3() const { return ___nodeScaleFactor_3; }
	inline float* get_address_of_nodeScaleFactor_3() { return &___nodeScaleFactor_3; }
	inline void set_nodeScaleFactor_3(float value)
	{
		___nodeScaleFactor_3 = value;
	}

	inline static int32_t get_offset_of_translation_4() { return static_cast<int32_t>(offsetof(ImportState_t482765084, ___translation_4)); }
	inline Vector3_t3722313464  get_translation_4() const { return ___translation_4; }
	inline Vector3_t3722313464 * get_address_of_translation_4() { return &___translation_4; }
	inline void set_translation_4(Vector3_t3722313464  value)
	{
		___translation_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMPORTSTATE_T482765084_H
#ifndef TEXTUREFORMAT_T2701165832_H
#define TEXTUREFORMAT_T2701165832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t2701165832 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureFormat_t2701165832, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T2701165832_H
#ifndef POLYLISTASSETSRESULT_T1050868053_H
#define POLYLISTASSETSRESULT_T1050868053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyListAssetsResult
struct  PolyListAssetsResult_t1050868053  : public PolyBaseResult_t2195255688
{
public:
	// System.Collections.Generic.List`1<PolyToolkit.PolyAsset> PolyToolkit.PolyListAssetsResult::assets
	List_1_t3286228253 * ___assets_1;
	// System.Int32 PolyToolkit.PolyListAssetsResult::totalSize
	int32_t ___totalSize_2;
	// System.String PolyToolkit.PolyListAssetsResult::nextPageToken
	String_t* ___nextPageToken_3;

public:
	inline static int32_t get_offset_of_assets_1() { return static_cast<int32_t>(offsetof(PolyListAssetsResult_t1050868053, ___assets_1)); }
	inline List_1_t3286228253 * get_assets_1() const { return ___assets_1; }
	inline List_1_t3286228253 ** get_address_of_assets_1() { return &___assets_1; }
	inline void set_assets_1(List_1_t3286228253 * value)
	{
		___assets_1 = value;
		Il2CppCodeGenWriteBarrier((&___assets_1), value);
	}

	inline static int32_t get_offset_of_totalSize_2() { return static_cast<int32_t>(offsetof(PolyListAssetsResult_t1050868053, ___totalSize_2)); }
	inline int32_t get_totalSize_2() const { return ___totalSize_2; }
	inline int32_t* get_address_of_totalSize_2() { return &___totalSize_2; }
	inline void set_totalSize_2(int32_t value)
	{
		___totalSize_2 = value;
	}

	inline static int32_t get_offset_of_nextPageToken_3() { return static_cast<int32_t>(offsetof(PolyListAssetsResult_t1050868053, ___nextPageToken_3)); }
	inline String_t* get_nextPageToken_3() const { return ___nextPageToken_3; }
	inline String_t** get_address_of_nextPageToken_3() { return &___nextPageToken_3; }
	inline void set_nextPageToken_3(String_t* value)
	{
		___nextPageToken_3 = value;
		Il2CppCodeGenWriteBarrier((&___nextPageToken_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYLISTASSETSRESULT_T1050868053_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_10)); }
	inline TimeSpan_t881159249  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t881159249 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t881159249  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_12)); }
	inline DateTime_t3738529785  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t3738529785  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_13)); }
	inline DateTime_t3738529785  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t3738529785 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t3738529785  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef NULLABLE_1_T3643161861_H
#define NULLABLE_1_T3643161861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<PolyToolkit.PolyFormatFilter>
struct  Nullable_1_t3643161861 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3643161861, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3643161861, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3643161861_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef U3CCREATEGAMEOBJECTSFROMMESHU3EC__ITERATOR2_T4216039538_H
#define U3CCREATEGAMEOBJECTSFROMMESHU3EC__ITERATOR2_T4216039538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromMesh>c__Iterator2
struct  U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538  : public RuntimeObject
{
public:
	// System.Int32 PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromMesh>c__Iterator2::<iP>__1
	int32_t ___U3CiPU3E__1_0;
	// PolyToolkitInternal.GltfMeshBase PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromMesh>c__Iterator2::mesh
	GltfMeshBase_t3664755873 * ___mesh_1;
	// PolyToolkitInternal.GltfPrimitiveBase PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromMesh>c__Iterator2::<prim>__2
	GltfPrimitiveBase_t1810376431 * ___U3CprimU3E__2_2;
	// PolyToolkitInternal.GltfMaterialConverter PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromMesh>c__Iterator2::matConverter
	GltfMaterialConverter_t1532388150 * ___matConverter_3;
	// System.Nullable`1<PolyToolkitInternal.GltfMaterialConverter/UnityMaterial> PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromMesh>c__Iterator2::<unityMaterial>__2
	Nullable_1_t1376722862  ___U3CunityMaterialU3E__2_4;
	// System.String PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromMesh>c__Iterator2::<primName>__2
	String_t* ___U3CprimNameU3E__2_5;
	// System.Int32 PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromMesh>c__Iterator2::<iM>__3
	int32_t ___U3CiMU3E__3_6;
	// System.String PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromMesh>c__Iterator2::<meshName>__4
	String_t* ___U3CmeshNameU3E__4_7;
	// UnityEngine.Mesh PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromMesh>c__Iterator2::<umesh>__4
	Mesh_t3648964284 * ___U3CumeshU3E__4_8;
	// PolyToolkitInternal.ImportGltf/GltfImportResult PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromMesh>c__Iterator2::result
	GltfImportResult_t3877923054 * ___result_9;
	// System.Collections.Generic.List`1/Enumerator<UnityEngine.Mesh> PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromMesh>c__Iterator2::$locvar0
	Enumerator_t2715315607  ___U24locvar0_10;
	// System.Boolean PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromMesh>c__Iterator2::allowMeshInParent
	bool ___allowMeshInParent_11;
	// UnityEngine.Transform PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromMesh>c__Iterator2::parent
	Transform_t3600365921 * ___parent_12;
	// System.Object PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromMesh>c__Iterator2::$current
	RuntimeObject * ___U24current_13;
	// System.Boolean PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromMesh>c__Iterator2::$disposing
	bool ___U24disposing_14;
	// System.Int32 PolyToolkitInternal.ImportGltf/<CreateGameObjectsFromMesh>c__Iterator2::$PC
	int32_t ___U24PC_15;

public:
	inline static int32_t get_offset_of_U3CiPU3E__1_0() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538, ___U3CiPU3E__1_0)); }
	inline int32_t get_U3CiPU3E__1_0() const { return ___U3CiPU3E__1_0; }
	inline int32_t* get_address_of_U3CiPU3E__1_0() { return &___U3CiPU3E__1_0; }
	inline void set_U3CiPU3E__1_0(int32_t value)
	{
		___U3CiPU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_mesh_1() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538, ___mesh_1)); }
	inline GltfMeshBase_t3664755873 * get_mesh_1() const { return ___mesh_1; }
	inline GltfMeshBase_t3664755873 ** get_address_of_mesh_1() { return &___mesh_1; }
	inline void set_mesh_1(GltfMeshBase_t3664755873 * value)
	{
		___mesh_1 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_1), value);
	}

	inline static int32_t get_offset_of_U3CprimU3E__2_2() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538, ___U3CprimU3E__2_2)); }
	inline GltfPrimitiveBase_t1810376431 * get_U3CprimU3E__2_2() const { return ___U3CprimU3E__2_2; }
	inline GltfPrimitiveBase_t1810376431 ** get_address_of_U3CprimU3E__2_2() { return &___U3CprimU3E__2_2; }
	inline void set_U3CprimU3E__2_2(GltfPrimitiveBase_t1810376431 * value)
	{
		___U3CprimU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CprimU3E__2_2), value);
	}

	inline static int32_t get_offset_of_matConverter_3() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538, ___matConverter_3)); }
	inline GltfMaterialConverter_t1532388150 * get_matConverter_3() const { return ___matConverter_3; }
	inline GltfMaterialConverter_t1532388150 ** get_address_of_matConverter_3() { return &___matConverter_3; }
	inline void set_matConverter_3(GltfMaterialConverter_t1532388150 * value)
	{
		___matConverter_3 = value;
		Il2CppCodeGenWriteBarrier((&___matConverter_3), value);
	}

	inline static int32_t get_offset_of_U3CunityMaterialU3E__2_4() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538, ___U3CunityMaterialU3E__2_4)); }
	inline Nullable_1_t1376722862  get_U3CunityMaterialU3E__2_4() const { return ___U3CunityMaterialU3E__2_4; }
	inline Nullable_1_t1376722862 * get_address_of_U3CunityMaterialU3E__2_4() { return &___U3CunityMaterialU3E__2_4; }
	inline void set_U3CunityMaterialU3E__2_4(Nullable_1_t1376722862  value)
	{
		___U3CunityMaterialU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CprimNameU3E__2_5() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538, ___U3CprimNameU3E__2_5)); }
	inline String_t* get_U3CprimNameU3E__2_5() const { return ___U3CprimNameU3E__2_5; }
	inline String_t** get_address_of_U3CprimNameU3E__2_5() { return &___U3CprimNameU3E__2_5; }
	inline void set_U3CprimNameU3E__2_5(String_t* value)
	{
		___U3CprimNameU3E__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CprimNameU3E__2_5), value);
	}

	inline static int32_t get_offset_of_U3CiMU3E__3_6() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538, ___U3CiMU3E__3_6)); }
	inline int32_t get_U3CiMU3E__3_6() const { return ___U3CiMU3E__3_6; }
	inline int32_t* get_address_of_U3CiMU3E__3_6() { return &___U3CiMU3E__3_6; }
	inline void set_U3CiMU3E__3_6(int32_t value)
	{
		___U3CiMU3E__3_6 = value;
	}

	inline static int32_t get_offset_of_U3CmeshNameU3E__4_7() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538, ___U3CmeshNameU3E__4_7)); }
	inline String_t* get_U3CmeshNameU3E__4_7() const { return ___U3CmeshNameU3E__4_7; }
	inline String_t** get_address_of_U3CmeshNameU3E__4_7() { return &___U3CmeshNameU3E__4_7; }
	inline void set_U3CmeshNameU3E__4_7(String_t* value)
	{
		___U3CmeshNameU3E__4_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmeshNameU3E__4_7), value);
	}

	inline static int32_t get_offset_of_U3CumeshU3E__4_8() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538, ___U3CumeshU3E__4_8)); }
	inline Mesh_t3648964284 * get_U3CumeshU3E__4_8() const { return ___U3CumeshU3E__4_8; }
	inline Mesh_t3648964284 ** get_address_of_U3CumeshU3E__4_8() { return &___U3CumeshU3E__4_8; }
	inline void set_U3CumeshU3E__4_8(Mesh_t3648964284 * value)
	{
		___U3CumeshU3E__4_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CumeshU3E__4_8), value);
	}

	inline static int32_t get_offset_of_result_9() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538, ___result_9)); }
	inline GltfImportResult_t3877923054 * get_result_9() const { return ___result_9; }
	inline GltfImportResult_t3877923054 ** get_address_of_result_9() { return &___result_9; }
	inline void set_result_9(GltfImportResult_t3877923054 * value)
	{
		___result_9 = value;
		Il2CppCodeGenWriteBarrier((&___result_9), value);
	}

	inline static int32_t get_offset_of_U24locvar0_10() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538, ___U24locvar0_10)); }
	inline Enumerator_t2715315607  get_U24locvar0_10() const { return ___U24locvar0_10; }
	inline Enumerator_t2715315607 * get_address_of_U24locvar0_10() { return &___U24locvar0_10; }
	inline void set_U24locvar0_10(Enumerator_t2715315607  value)
	{
		___U24locvar0_10 = value;
	}

	inline static int32_t get_offset_of_allowMeshInParent_11() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538, ___allowMeshInParent_11)); }
	inline bool get_allowMeshInParent_11() const { return ___allowMeshInParent_11; }
	inline bool* get_address_of_allowMeshInParent_11() { return &___allowMeshInParent_11; }
	inline void set_allowMeshInParent_11(bool value)
	{
		___allowMeshInParent_11 = value;
	}

	inline static int32_t get_offset_of_parent_12() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538, ___parent_12)); }
	inline Transform_t3600365921 * get_parent_12() const { return ___parent_12; }
	inline Transform_t3600365921 ** get_address_of_parent_12() { return &___parent_12; }
	inline void set_parent_12(Transform_t3600365921 * value)
	{
		___parent_12 = value;
		Il2CppCodeGenWriteBarrier((&___parent_12), value);
	}

	inline static int32_t get_offset_of_U24current_13() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538, ___U24current_13)); }
	inline RuntimeObject * get_U24current_13() const { return ___U24current_13; }
	inline RuntimeObject ** get_address_of_U24current_13() { return &___U24current_13; }
	inline void set_U24current_13(RuntimeObject * value)
	{
		___U24current_13 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_13), value);
	}

	inline static int32_t get_offset_of_U24disposing_14() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538, ___U24disposing_14)); }
	inline bool get_U24disposing_14() const { return ___U24disposing_14; }
	inline bool* get_address_of_U24disposing_14() { return &___U24disposing_14; }
	inline void set_U24disposing_14(bool value)
	{
		___U24disposing_14 = value;
	}

	inline static int32_t get_offset_of_U24PC_15() { return static_cast<int32_t>(offsetof(U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538, ___U24PC_15)); }
	inline int32_t get_U24PC_15() const { return ___U24PC_15; }
	inline int32_t* get_address_of_U24PC_15() { return &___U24PC_15; }
	inline void set_U24PC_15(int32_t value)
	{
		___U24PC_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATEGAMEOBJECTSFROMMESHU3EC__ITERATOR2_T4216039538_H
#ifndef U3CGENERATEMESHSUBSETSU3EC__ITERATOR3_T1345351942_H
#define U3CGENERATEMESHSUBSETSU3EC__ITERATOR3_T1345351942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.ImportGltf/<GenerateMeshSubsets>c__Iterator3
struct  U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942  : public RuntimeObject
{
public:
	// System.Int32 PolyToolkitInternal.ImportGltf/<GenerateMeshSubsets>c__Iterator3::numVerts
	int32_t ___numVerts_0;
	// System.Int32 PolyToolkitInternal.ImportGltf/<GenerateMeshSubsets>c__Iterator3::maxSubsetVerts
	int32_t ___maxSubsetVerts_1;
	// System.UInt16[] PolyToolkitInternal.ImportGltf/<GenerateMeshSubsets>c__Iterator3::triangles
	UInt16U5BU5D_t3326319531* ___triangles_2;
	// System.Int32 PolyToolkitInternal.ImportGltf/<GenerateMeshSubsets>c__Iterator3::<count>__0
	int32_t ___U3CcountU3E__0_3;
	// System.Nullable`1<PolyToolkitInternal.IntRange> PolyToolkitInternal.ImportGltf/<GenerateMeshSubsets>c__Iterator3::<vertsUsed>__0
	Nullable_1_t1519427536  ___U3CvertsUsedU3E__0_4;
	// System.Nullable`1<PolyToolkitInternal.IntRange> PolyToolkitInternal.ImportGltf/<GenerateMeshSubsets>c__Iterator3::<trisUsed>__0
	Nullable_1_t1519427536  ___U3CtrisUsedU3E__0_5;
	// System.Int32 PolyToolkitInternal.ImportGltf/<GenerateMeshSubsets>c__Iterator3::<iTri>__1
	int32_t ___U3CiTriU3E__1_6;
	// PolyToolkitInternal.IntRange PolyToolkitInternal.ImportGltf/<GenerateMeshSubsets>c__Iterator3::<triVerts>__2
	IntRange_t4091832750  ___U3CtriVertsU3E__2_7;
	// PolyToolkitInternal.IntRange PolyToolkitInternal.ImportGltf/<GenerateMeshSubsets>c__Iterator3::<newVertsUsed>__3
	IntRange_t4091832750  ___U3CnewVertsUsedU3E__3_8;
	// PolyToolkitInternal.IntRange PolyToolkitInternal.ImportGltf/<GenerateMeshSubsets>c__Iterator3::<newTrisUsed>__3
	IntRange_t4091832750  ___U3CnewTrisUsedU3E__3_9;
	// PolyToolkitInternal.ImportGltf/MeshSubset PolyToolkitInternal.ImportGltf/<GenerateMeshSubsets>c__Iterator3::$current
	MeshSubset_t1957016349  ___U24current_10;
	// System.Boolean PolyToolkitInternal.ImportGltf/<GenerateMeshSubsets>c__Iterator3::$disposing
	bool ___U24disposing_11;
	// System.Int32 PolyToolkitInternal.ImportGltf/<GenerateMeshSubsets>c__Iterator3::$PC
	int32_t ___U24PC_12;

public:
	inline static int32_t get_offset_of_numVerts_0() { return static_cast<int32_t>(offsetof(U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942, ___numVerts_0)); }
	inline int32_t get_numVerts_0() const { return ___numVerts_0; }
	inline int32_t* get_address_of_numVerts_0() { return &___numVerts_0; }
	inline void set_numVerts_0(int32_t value)
	{
		___numVerts_0 = value;
	}

	inline static int32_t get_offset_of_maxSubsetVerts_1() { return static_cast<int32_t>(offsetof(U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942, ___maxSubsetVerts_1)); }
	inline int32_t get_maxSubsetVerts_1() const { return ___maxSubsetVerts_1; }
	inline int32_t* get_address_of_maxSubsetVerts_1() { return &___maxSubsetVerts_1; }
	inline void set_maxSubsetVerts_1(int32_t value)
	{
		___maxSubsetVerts_1 = value;
	}

	inline static int32_t get_offset_of_triangles_2() { return static_cast<int32_t>(offsetof(U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942, ___triangles_2)); }
	inline UInt16U5BU5D_t3326319531* get_triangles_2() const { return ___triangles_2; }
	inline UInt16U5BU5D_t3326319531** get_address_of_triangles_2() { return &___triangles_2; }
	inline void set_triangles_2(UInt16U5BU5D_t3326319531* value)
	{
		___triangles_2 = value;
		Il2CppCodeGenWriteBarrier((&___triangles_2), value);
	}

	inline static int32_t get_offset_of_U3CcountU3E__0_3() { return static_cast<int32_t>(offsetof(U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942, ___U3CcountU3E__0_3)); }
	inline int32_t get_U3CcountU3E__0_3() const { return ___U3CcountU3E__0_3; }
	inline int32_t* get_address_of_U3CcountU3E__0_3() { return &___U3CcountU3E__0_3; }
	inline void set_U3CcountU3E__0_3(int32_t value)
	{
		___U3CcountU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CvertsUsedU3E__0_4() { return static_cast<int32_t>(offsetof(U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942, ___U3CvertsUsedU3E__0_4)); }
	inline Nullable_1_t1519427536  get_U3CvertsUsedU3E__0_4() const { return ___U3CvertsUsedU3E__0_4; }
	inline Nullable_1_t1519427536 * get_address_of_U3CvertsUsedU3E__0_4() { return &___U3CvertsUsedU3E__0_4; }
	inline void set_U3CvertsUsedU3E__0_4(Nullable_1_t1519427536  value)
	{
		___U3CvertsUsedU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CtrisUsedU3E__0_5() { return static_cast<int32_t>(offsetof(U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942, ___U3CtrisUsedU3E__0_5)); }
	inline Nullable_1_t1519427536  get_U3CtrisUsedU3E__0_5() const { return ___U3CtrisUsedU3E__0_5; }
	inline Nullable_1_t1519427536 * get_address_of_U3CtrisUsedU3E__0_5() { return &___U3CtrisUsedU3E__0_5; }
	inline void set_U3CtrisUsedU3E__0_5(Nullable_1_t1519427536  value)
	{
		___U3CtrisUsedU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3CiTriU3E__1_6() { return static_cast<int32_t>(offsetof(U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942, ___U3CiTriU3E__1_6)); }
	inline int32_t get_U3CiTriU3E__1_6() const { return ___U3CiTriU3E__1_6; }
	inline int32_t* get_address_of_U3CiTriU3E__1_6() { return &___U3CiTriU3E__1_6; }
	inline void set_U3CiTriU3E__1_6(int32_t value)
	{
		___U3CiTriU3E__1_6 = value;
	}

	inline static int32_t get_offset_of_U3CtriVertsU3E__2_7() { return static_cast<int32_t>(offsetof(U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942, ___U3CtriVertsU3E__2_7)); }
	inline IntRange_t4091832750  get_U3CtriVertsU3E__2_7() const { return ___U3CtriVertsU3E__2_7; }
	inline IntRange_t4091832750 * get_address_of_U3CtriVertsU3E__2_7() { return &___U3CtriVertsU3E__2_7; }
	inline void set_U3CtriVertsU3E__2_7(IntRange_t4091832750  value)
	{
		___U3CtriVertsU3E__2_7 = value;
	}

	inline static int32_t get_offset_of_U3CnewVertsUsedU3E__3_8() { return static_cast<int32_t>(offsetof(U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942, ___U3CnewVertsUsedU3E__3_8)); }
	inline IntRange_t4091832750  get_U3CnewVertsUsedU3E__3_8() const { return ___U3CnewVertsUsedU3E__3_8; }
	inline IntRange_t4091832750 * get_address_of_U3CnewVertsUsedU3E__3_8() { return &___U3CnewVertsUsedU3E__3_8; }
	inline void set_U3CnewVertsUsedU3E__3_8(IntRange_t4091832750  value)
	{
		___U3CnewVertsUsedU3E__3_8 = value;
	}

	inline static int32_t get_offset_of_U3CnewTrisUsedU3E__3_9() { return static_cast<int32_t>(offsetof(U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942, ___U3CnewTrisUsedU3E__3_9)); }
	inline IntRange_t4091832750  get_U3CnewTrisUsedU3E__3_9() const { return ___U3CnewTrisUsedU3E__3_9; }
	inline IntRange_t4091832750 * get_address_of_U3CnewTrisUsedU3E__3_9() { return &___U3CnewTrisUsedU3E__3_9; }
	inline void set_U3CnewTrisUsedU3E__3_9(IntRange_t4091832750  value)
	{
		___U3CnewTrisUsedU3E__3_9 = value;
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942, ___U24current_10)); }
	inline MeshSubset_t1957016349  get_U24current_10() const { return ___U24current_10; }
	inline MeshSubset_t1957016349 * get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(MeshSubset_t1957016349  value)
	{
		___U24current_10 = value;
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGENERATEMESHSUBSETSU3EC__ITERATOR3_T1345351942_H
#ifndef POLYIMPORTOPTIONS_T4213423452_H
#define POLYIMPORTOPTIONS_T4213423452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyImportOptions
struct  PolyImportOptions_t4213423452 
{
public:
	// PolyToolkit.PolyImportOptions/RescalingMode PolyToolkit.PolyImportOptions::rescalingMode
	int32_t ___rescalingMode_0;
	// System.Single PolyToolkit.PolyImportOptions::scaleFactor
	float ___scaleFactor_1;
	// System.Single PolyToolkit.PolyImportOptions::desiredSize
	float ___desiredSize_2;
	// System.Boolean PolyToolkit.PolyImportOptions::recenter
	bool ___recenter_3;
	// System.Boolean PolyToolkit.PolyImportOptions::clientThrottledMainThread
	bool ___clientThrottledMainThread_4;

public:
	inline static int32_t get_offset_of_rescalingMode_0() { return static_cast<int32_t>(offsetof(PolyImportOptions_t4213423452, ___rescalingMode_0)); }
	inline int32_t get_rescalingMode_0() const { return ___rescalingMode_0; }
	inline int32_t* get_address_of_rescalingMode_0() { return &___rescalingMode_0; }
	inline void set_rescalingMode_0(int32_t value)
	{
		___rescalingMode_0 = value;
	}

	inline static int32_t get_offset_of_scaleFactor_1() { return static_cast<int32_t>(offsetof(PolyImportOptions_t4213423452, ___scaleFactor_1)); }
	inline float get_scaleFactor_1() const { return ___scaleFactor_1; }
	inline float* get_address_of_scaleFactor_1() { return &___scaleFactor_1; }
	inline void set_scaleFactor_1(float value)
	{
		___scaleFactor_1 = value;
	}

	inline static int32_t get_offset_of_desiredSize_2() { return static_cast<int32_t>(offsetof(PolyImportOptions_t4213423452, ___desiredSize_2)); }
	inline float get_desiredSize_2() const { return ___desiredSize_2; }
	inline float* get_address_of_desiredSize_2() { return &___desiredSize_2; }
	inline void set_desiredSize_2(float value)
	{
		___desiredSize_2 = value;
	}

	inline static int32_t get_offset_of_recenter_3() { return static_cast<int32_t>(offsetof(PolyImportOptions_t4213423452, ___recenter_3)); }
	inline bool get_recenter_3() const { return ___recenter_3; }
	inline bool* get_address_of_recenter_3() { return &___recenter_3; }
	inline void set_recenter_3(bool value)
	{
		___recenter_3 = value;
	}

	inline static int32_t get_offset_of_clientThrottledMainThread_4() { return static_cast<int32_t>(offsetof(PolyImportOptions_t4213423452, ___clientThrottledMainThread_4)); }
	inline bool get_clientThrottledMainThread_4() const { return ___clientThrottledMainThread_4; }
	inline bool* get_address_of_clientThrottledMainThread_4() { return &___clientThrottledMainThread_4; }
	inline void set_clientThrottledMainThread_4(bool value)
	{
		___clientThrottledMainThread_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PolyToolkit.PolyImportOptions
struct PolyImportOptions_t4213423452_marshaled_pinvoke
{
	int32_t ___rescalingMode_0;
	float ___scaleFactor_1;
	float ___desiredSize_2;
	int32_t ___recenter_3;
	int32_t ___clientThrottledMainThread_4;
};
// Native definition for COM marshalling of PolyToolkit.PolyImportOptions
struct PolyImportOptions_t4213423452_marshaled_com
{
	int32_t ___rescalingMode_0;
	float ___scaleFactor_1;
	float ___desiredSize_2;
	int32_t ___recenter_3;
	int32_t ___clientThrottledMainThread_4;
};
#endif // POLYIMPORTOPTIONS_T4213423452_H
#ifndef LENGTHWITHUNIT_T3846227886_H
#define LENGTHWITHUNIT_T3846227886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.LengthWithUnit
struct  LengthWithUnit_t3846227886 
{
public:
	// System.Single PolyToolkitInternal.LengthWithUnit::amount
	float ___amount_0;
	// PolyToolkitInternal.LengthUnit PolyToolkitInternal.LengthWithUnit::unit
	int32_t ___unit_1;

public:
	inline static int32_t get_offset_of_amount_0() { return static_cast<int32_t>(offsetof(LengthWithUnit_t3846227886, ___amount_0)); }
	inline float get_amount_0() const { return ___amount_0; }
	inline float* get_address_of_amount_0() { return &___amount_0; }
	inline void set_amount_0(float value)
	{
		___amount_0 = value;
	}

	inline static int32_t get_offset_of_unit_1() { return static_cast<int32_t>(offsetof(LengthWithUnit_t3846227886, ___unit_1)); }
	inline int32_t get_unit_1() const { return ___unit_1; }
	inline int32_t* get_address_of_unit_1() { return &___unit_1; }
	inline void set_unit_1(int32_t value)
	{
		___unit_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LENGTHWITHUNIT_T3846227886_H
#ifndef RAWIMAGE_T3303103980_H
#define RAWIMAGE_T3303103980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.RawImage
struct  RawImage_t3303103980  : public RuntimeObject
{
public:
	// UnityEngine.Color32[] PolyToolkitInternal.RawImage::colorData
	Color32U5BU5D_t3850468773* ___colorData_0;
	// System.Int32 PolyToolkitInternal.RawImage::colorWidth
	int32_t ___colorWidth_1;
	// System.Int32 PolyToolkitInternal.RawImage::colorHeight
	int32_t ___colorHeight_2;
	// UnityEngine.TextureFormat PolyToolkitInternal.RawImage::format
	int32_t ___format_3;

public:
	inline static int32_t get_offset_of_colorData_0() { return static_cast<int32_t>(offsetof(RawImage_t3303103980, ___colorData_0)); }
	inline Color32U5BU5D_t3850468773* get_colorData_0() const { return ___colorData_0; }
	inline Color32U5BU5D_t3850468773** get_address_of_colorData_0() { return &___colorData_0; }
	inline void set_colorData_0(Color32U5BU5D_t3850468773* value)
	{
		___colorData_0 = value;
		Il2CppCodeGenWriteBarrier((&___colorData_0), value);
	}

	inline static int32_t get_offset_of_colorWidth_1() { return static_cast<int32_t>(offsetof(RawImage_t3303103980, ___colorWidth_1)); }
	inline int32_t get_colorWidth_1() const { return ___colorWidth_1; }
	inline int32_t* get_address_of_colorWidth_1() { return &___colorWidth_1; }
	inline void set_colorWidth_1(int32_t value)
	{
		___colorWidth_1 = value;
	}

	inline static int32_t get_offset_of_colorHeight_2() { return static_cast<int32_t>(offsetof(RawImage_t3303103980, ___colorHeight_2)); }
	inline int32_t get_colorHeight_2() const { return ___colorHeight_2; }
	inline int32_t* get_address_of_colorHeight_2() { return &___colorHeight_2; }
	inline void set_colorHeight_2(int32_t value)
	{
		___colorHeight_2 = value;
	}

	inline static int32_t get_offset_of_format_3() { return static_cast<int32_t>(offsetof(RawImage_t3303103980, ___format_3)); }
	inline int32_t get_format_3() const { return ___format_3; }
	inline int32_t* get_address_of_format_3() { return &___format_3; }
	inline void set_format_3(int32_t value)
	{
		___format_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWIMAGE_T3303103980_H
#ifndef POLYFORMAT_T1880249796_H
#define POLYFORMAT_T1880249796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyFormat
struct  PolyFormat_t1880249796  : public RuntimeObject
{
public:
	// PolyToolkit.PolyFormatType PolyToolkit.PolyFormat::formatType
	int32_t ___formatType_0;
	// PolyToolkit.PolyFile PolyToolkit.PolyFormat::root
	PolyFile_t4211357808 * ___root_1;
	// System.Collections.Generic.List`1<PolyToolkit.PolyFile> PolyToolkit.PolyFormat::resources
	List_1_t1388465254 * ___resources_2;
	// PolyToolkit.PolyFormatComplexity PolyToolkit.PolyFormat::formatComplexity
	PolyFormatComplexity_t3101848153 * ___formatComplexity_3;

public:
	inline static int32_t get_offset_of_formatType_0() { return static_cast<int32_t>(offsetof(PolyFormat_t1880249796, ___formatType_0)); }
	inline int32_t get_formatType_0() const { return ___formatType_0; }
	inline int32_t* get_address_of_formatType_0() { return &___formatType_0; }
	inline void set_formatType_0(int32_t value)
	{
		___formatType_0 = value;
	}

	inline static int32_t get_offset_of_root_1() { return static_cast<int32_t>(offsetof(PolyFormat_t1880249796, ___root_1)); }
	inline PolyFile_t4211357808 * get_root_1() const { return ___root_1; }
	inline PolyFile_t4211357808 ** get_address_of_root_1() { return &___root_1; }
	inline void set_root_1(PolyFile_t4211357808 * value)
	{
		___root_1 = value;
		Il2CppCodeGenWriteBarrier((&___root_1), value);
	}

	inline static int32_t get_offset_of_resources_2() { return static_cast<int32_t>(offsetof(PolyFormat_t1880249796, ___resources_2)); }
	inline List_1_t1388465254 * get_resources_2() const { return ___resources_2; }
	inline List_1_t1388465254 ** get_address_of_resources_2() { return &___resources_2; }
	inline void set_resources_2(List_1_t1388465254 * value)
	{
		___resources_2 = value;
		Il2CppCodeGenWriteBarrier((&___resources_2), value);
	}

	inline static int32_t get_offset_of_formatComplexity_3() { return static_cast<int32_t>(offsetof(PolyFormat_t1880249796, ___formatComplexity_3)); }
	inline PolyFormatComplexity_t3101848153 * get_formatComplexity_3() const { return ___formatComplexity_3; }
	inline PolyFormatComplexity_t3101848153 ** get_address_of_formatComplexity_3() { return &___formatComplexity_3; }
	inline void set_formatComplexity_3(PolyFormatComplexity_t3101848153 * value)
	{
		___formatComplexity_3 = value;
		Il2CppCodeGenWriteBarrier((&___formatComplexity_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYFORMAT_T1880249796_H
#ifndef GLTFNODEBASE_T3005766873_H
#define GLTFNODEBASE_T3005766873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfNodeBase
struct  GltfNodeBase_t3005766873  : public RuntimeObject
{
public:
	// System.String PolyToolkitInternal.GltfNodeBase::name
	String_t* ___name_0;
	// System.Nullable`1<UnityEngine.Matrix4x4> PolyToolkitInternal.GltfNodeBase::matrix
	Nullable_1_t3540463925  ___matrix_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(GltfNodeBase_t3005766873, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_matrix_1() { return static_cast<int32_t>(offsetof(GltfNodeBase_t3005766873, ___matrix_1)); }
	inline Nullable_1_t3540463925  get_matrix_1() const { return ___matrix_1; }
	inline Nullable_1_t3540463925 * get_address_of_matrix_1() { return &___matrix_1; }
	inline void set_matrix_1(Nullable_1_t3540463925  value)
	{
		___matrix_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFNODEBASE_T3005766873_H
#ifndef POLYREQUEST_T2374976743_H
#define POLYREQUEST_T2374976743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyRequest
struct  PolyRequest_t2374976743  : public RuntimeObject
{
public:
	// PolyToolkit.PolyOrderBy PolyToolkit.PolyRequest::orderBy
	int32_t ___orderBy_0;
	// System.Int32 PolyToolkit.PolyRequest::pageSize
	int32_t ___pageSize_1;
	// System.String PolyToolkit.PolyRequest::pageToken
	String_t* ___pageToken_2;

public:
	inline static int32_t get_offset_of_orderBy_0() { return static_cast<int32_t>(offsetof(PolyRequest_t2374976743, ___orderBy_0)); }
	inline int32_t get_orderBy_0() const { return ___orderBy_0; }
	inline int32_t* get_address_of_orderBy_0() { return &___orderBy_0; }
	inline void set_orderBy_0(int32_t value)
	{
		___orderBy_0 = value;
	}

	inline static int32_t get_offset_of_pageSize_1() { return static_cast<int32_t>(offsetof(PolyRequest_t2374976743, ___pageSize_1)); }
	inline int32_t get_pageSize_1() const { return ___pageSize_1; }
	inline int32_t* get_address_of_pageSize_1() { return &___pageSize_1; }
	inline void set_pageSize_1(int32_t value)
	{
		___pageSize_1 = value;
	}

	inline static int32_t get_offset_of_pageToken_2() { return static_cast<int32_t>(offsetof(PolyRequest_t2374976743, ___pageToken_2)); }
	inline String_t* get_pageToken_2() const { return ___pageToken_2; }
	inline String_t** get_address_of_pageToken_2() { return &___pageToken_2; }
	inline void set_pageToken_2(String_t* value)
	{
		___pageToken_2 = value;
		Il2CppCodeGenWriteBarrier((&___pageToken_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYREQUEST_T2374976743_H
#ifndef BRUSHMANIFEST_T1931127838_H
#define BRUSHMANIFEST_T1931127838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TiltBrushToolkit.BrushManifest
struct  BrushManifest_t1931127838  : public ScriptableObject_t2528358522
{
public:
	// TiltBrushToolkit.BrushManifest TiltBrushToolkit.BrushManifest::m_DefaultManifest
	BrushManifest_t1931127838 * ___m_DefaultManifest_3;
	// TiltBrushToolkit.BrushDescriptor[] TiltBrushToolkit.BrushManifest::m_Brushes
	BrushDescriptorU5BU5D_t2115081084* ___m_Brushes_4;
	// System.Collections.Generic.Dictionary`2<System.Guid,TiltBrushToolkit.BrushDescriptor> TiltBrushToolkit.BrushManifest::m_ByGuid
	Dictionary_2_t1841745054 * ___m_ByGuid_5;
	// System.Linq.ILookup`2<System.String,TiltBrushToolkit.BrushDescriptor> TiltBrushToolkit.BrushManifest::m_ByName
	RuntimeObject* ___m_ByName_6;

public:
	inline static int32_t get_offset_of_m_DefaultManifest_3() { return static_cast<int32_t>(offsetof(BrushManifest_t1931127838, ___m_DefaultManifest_3)); }
	inline BrushManifest_t1931127838 * get_m_DefaultManifest_3() const { return ___m_DefaultManifest_3; }
	inline BrushManifest_t1931127838 ** get_address_of_m_DefaultManifest_3() { return &___m_DefaultManifest_3; }
	inline void set_m_DefaultManifest_3(BrushManifest_t1931127838 * value)
	{
		___m_DefaultManifest_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultManifest_3), value);
	}

	inline static int32_t get_offset_of_m_Brushes_4() { return static_cast<int32_t>(offsetof(BrushManifest_t1931127838, ___m_Brushes_4)); }
	inline BrushDescriptorU5BU5D_t2115081084* get_m_Brushes_4() const { return ___m_Brushes_4; }
	inline BrushDescriptorU5BU5D_t2115081084** get_address_of_m_Brushes_4() { return &___m_Brushes_4; }
	inline void set_m_Brushes_4(BrushDescriptorU5BU5D_t2115081084* value)
	{
		___m_Brushes_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Brushes_4), value);
	}

	inline static int32_t get_offset_of_m_ByGuid_5() { return static_cast<int32_t>(offsetof(BrushManifest_t1931127838, ___m_ByGuid_5)); }
	inline Dictionary_2_t1841745054 * get_m_ByGuid_5() const { return ___m_ByGuid_5; }
	inline Dictionary_2_t1841745054 ** get_address_of_m_ByGuid_5() { return &___m_ByGuid_5; }
	inline void set_m_ByGuid_5(Dictionary_2_t1841745054 * value)
	{
		___m_ByGuid_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ByGuid_5), value);
	}

	inline static int32_t get_offset_of_m_ByName_6() { return static_cast<int32_t>(offsetof(BrushManifest_t1931127838, ___m_ByName_6)); }
	inline RuntimeObject* get_m_ByName_6() const { return ___m_ByName_6; }
	inline RuntimeObject** get_address_of_m_ByName_6() { return &___m_ByName_6; }
	inline void set_m_ByName_6(RuntimeObject* value)
	{
		___m_ByName_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_ByName_6), value);
	}
};

struct BrushManifest_t1931127838_StaticFields
{
public:
	// TiltBrushToolkit.BrushManifest TiltBrushToolkit.BrushManifest::sm_Instance
	BrushManifest_t1931127838 * ___sm_Instance_2;
	// System.Func`2<TiltBrushToolkit.BrushDescriptor,System.Guid> TiltBrushToolkit.BrushManifest::<>f__am$cache0
	Func_2_t1831698856 * ___U3CU3Ef__amU24cache0_7;
	// System.Func`2<TiltBrushToolkit.BrushDescriptor,System.String> TiltBrushToolkit.BrushManifest::<>f__am$cache1
	Func_2_t485616658 * ___U3CU3Ef__amU24cache1_8;

public:
	inline static int32_t get_offset_of_sm_Instance_2() { return static_cast<int32_t>(offsetof(BrushManifest_t1931127838_StaticFields, ___sm_Instance_2)); }
	inline BrushManifest_t1931127838 * get_sm_Instance_2() const { return ___sm_Instance_2; }
	inline BrushManifest_t1931127838 ** get_address_of_sm_Instance_2() { return &___sm_Instance_2; }
	inline void set_sm_Instance_2(BrushManifest_t1931127838 * value)
	{
		___sm_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___sm_Instance_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(BrushManifest_t1931127838_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline Func_2_t1831698856 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline Func_2_t1831698856 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(Func_2_t1831698856 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_8() { return static_cast<int32_t>(offsetof(BrushManifest_t1931127838_StaticFields, ___U3CU3Ef__amU24cache1_8)); }
	inline Func_2_t485616658 * get_U3CU3Ef__amU24cache1_8() const { return ___U3CU3Ef__amU24cache1_8; }
	inline Func_2_t485616658 ** get_address_of_U3CU3Ef__amU24cache1_8() { return &___U3CU3Ef__amU24cache1_8; }
	inline void set_U3CU3Ef__amU24cache1_8(Func_2_t485616658 * value)
	{
		___U3CU3Ef__amU24cache1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRUSHMANIFEST_T1931127838_H
#ifndef BRUSHDESCRIPTOR_T2792727521_H
#define BRUSHDESCRIPTOR_T2792727521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TiltBrushToolkit.BrushDescriptor
struct  BrushDescriptor_t2792727521  : public ScriptableObject_t2528358522
{
public:
	// TiltBrushToolkit.SerializableGuid TiltBrushToolkit.BrushDescriptor::m_Guid
	SerializableGuid_t3370039383  ___m_Guid_2;
	// System.String TiltBrushToolkit.BrushDescriptor::m_DurableName
	String_t* ___m_DurableName_3;
	// UnityEngine.Material TiltBrushToolkit.BrushDescriptor::m_Material
	Material_t340375123 * ___m_Material_4;
	// System.Boolean TiltBrushToolkit.BrushDescriptor::m_IsParticle
	bool ___m_IsParticle_5;
	// System.Int32 TiltBrushToolkit.BrushDescriptor::m_uv0Size
	int32_t ___m_uv0Size_6;
	// TiltBrushToolkit.BrushDescriptor/Semantic TiltBrushToolkit.BrushDescriptor::m_uv0Semantic
	int32_t ___m_uv0Semantic_7;
	// System.Int32 TiltBrushToolkit.BrushDescriptor::m_uv1Size
	int32_t ___m_uv1Size_8;
	// TiltBrushToolkit.BrushDescriptor/Semantic TiltBrushToolkit.BrushDescriptor::m_uv1Semantic
	int32_t ___m_uv1Semantic_9;
	// System.Boolean TiltBrushToolkit.BrushDescriptor::m_bUseNormals
	bool ___m_bUseNormals_10;
	// TiltBrushToolkit.BrushDescriptor/Semantic TiltBrushToolkit.BrushDescriptor::m_normalSemantic
	int32_t ___m_normalSemantic_11;
	// System.Boolean TiltBrushToolkit.BrushDescriptor::m_bFbxExportNormalAsTexcoord1
	bool ___m_bFbxExportNormalAsTexcoord1_12;

public:
	inline static int32_t get_offset_of_m_Guid_2() { return static_cast<int32_t>(offsetof(BrushDescriptor_t2792727521, ___m_Guid_2)); }
	inline SerializableGuid_t3370039383  get_m_Guid_2() const { return ___m_Guid_2; }
	inline SerializableGuid_t3370039383 * get_address_of_m_Guid_2() { return &___m_Guid_2; }
	inline void set_m_Guid_2(SerializableGuid_t3370039383  value)
	{
		___m_Guid_2 = value;
	}

	inline static int32_t get_offset_of_m_DurableName_3() { return static_cast<int32_t>(offsetof(BrushDescriptor_t2792727521, ___m_DurableName_3)); }
	inline String_t* get_m_DurableName_3() const { return ___m_DurableName_3; }
	inline String_t** get_address_of_m_DurableName_3() { return &___m_DurableName_3; }
	inline void set_m_DurableName_3(String_t* value)
	{
		___m_DurableName_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_DurableName_3), value);
	}

	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(BrushDescriptor_t2792727521, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_IsParticle_5() { return static_cast<int32_t>(offsetof(BrushDescriptor_t2792727521, ___m_IsParticle_5)); }
	inline bool get_m_IsParticle_5() const { return ___m_IsParticle_5; }
	inline bool* get_address_of_m_IsParticle_5() { return &___m_IsParticle_5; }
	inline void set_m_IsParticle_5(bool value)
	{
		___m_IsParticle_5 = value;
	}

	inline static int32_t get_offset_of_m_uv0Size_6() { return static_cast<int32_t>(offsetof(BrushDescriptor_t2792727521, ___m_uv0Size_6)); }
	inline int32_t get_m_uv0Size_6() const { return ___m_uv0Size_6; }
	inline int32_t* get_address_of_m_uv0Size_6() { return &___m_uv0Size_6; }
	inline void set_m_uv0Size_6(int32_t value)
	{
		___m_uv0Size_6 = value;
	}

	inline static int32_t get_offset_of_m_uv0Semantic_7() { return static_cast<int32_t>(offsetof(BrushDescriptor_t2792727521, ___m_uv0Semantic_7)); }
	inline int32_t get_m_uv0Semantic_7() const { return ___m_uv0Semantic_7; }
	inline int32_t* get_address_of_m_uv0Semantic_7() { return &___m_uv0Semantic_7; }
	inline void set_m_uv0Semantic_7(int32_t value)
	{
		___m_uv0Semantic_7 = value;
	}

	inline static int32_t get_offset_of_m_uv1Size_8() { return static_cast<int32_t>(offsetof(BrushDescriptor_t2792727521, ___m_uv1Size_8)); }
	inline int32_t get_m_uv1Size_8() const { return ___m_uv1Size_8; }
	inline int32_t* get_address_of_m_uv1Size_8() { return &___m_uv1Size_8; }
	inline void set_m_uv1Size_8(int32_t value)
	{
		___m_uv1Size_8 = value;
	}

	inline static int32_t get_offset_of_m_uv1Semantic_9() { return static_cast<int32_t>(offsetof(BrushDescriptor_t2792727521, ___m_uv1Semantic_9)); }
	inline int32_t get_m_uv1Semantic_9() const { return ___m_uv1Semantic_9; }
	inline int32_t* get_address_of_m_uv1Semantic_9() { return &___m_uv1Semantic_9; }
	inline void set_m_uv1Semantic_9(int32_t value)
	{
		___m_uv1Semantic_9 = value;
	}

	inline static int32_t get_offset_of_m_bUseNormals_10() { return static_cast<int32_t>(offsetof(BrushDescriptor_t2792727521, ___m_bUseNormals_10)); }
	inline bool get_m_bUseNormals_10() const { return ___m_bUseNormals_10; }
	inline bool* get_address_of_m_bUseNormals_10() { return &___m_bUseNormals_10; }
	inline void set_m_bUseNormals_10(bool value)
	{
		___m_bUseNormals_10 = value;
	}

	inline static int32_t get_offset_of_m_normalSemantic_11() { return static_cast<int32_t>(offsetof(BrushDescriptor_t2792727521, ___m_normalSemantic_11)); }
	inline int32_t get_m_normalSemantic_11() const { return ___m_normalSemantic_11; }
	inline int32_t* get_address_of_m_normalSemantic_11() { return &___m_normalSemantic_11; }
	inline void set_m_normalSemantic_11(int32_t value)
	{
		___m_normalSemantic_11 = value;
	}

	inline static int32_t get_offset_of_m_bFbxExportNormalAsTexcoord1_12() { return static_cast<int32_t>(offsetof(BrushDescriptor_t2792727521, ___m_bFbxExportNormalAsTexcoord1_12)); }
	inline bool get_m_bFbxExportNormalAsTexcoord1_12() const { return ___m_bFbxExportNormalAsTexcoord1_12; }
	inline bool* get_address_of_m_bFbxExportNormalAsTexcoord1_12() { return &___m_bFbxExportNormalAsTexcoord1_12; }
	inline void set_m_bFbxExportNormalAsTexcoord1_12(bool value)
	{
		___m_bFbxExportNormalAsTexcoord1_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRUSHDESCRIPTOR_T2792727521_H
#ifndef U3CFETCHANDIMPORTFORMATU3EC__ANONSTOREY4_T209984363_H
#define U3CFETCHANDIMPORTFORMATU3EC__ANONSTOREY4_T209984363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.PolyMainInternal/<FetchAndImportFormat>c__AnonStorey4
struct  U3CFetchAndImportFormatU3Ec__AnonStorey4_t209984363  : public RuntimeObject
{
public:
	// PolyToolkit.PolyFormat PolyToolkitInternal.PolyMainInternal/<FetchAndImportFormat>c__AnonStorey4::format
	PolyFormat_t1880249796 * ___format_0;
	// PolyToolkit.PolyAsset PolyToolkitInternal.PolyMainInternal/<FetchAndImportFormat>c__AnonStorey4::asset
	PolyAsset_t1814153511 * ___asset_1;
	// PolyToolkit.PolyImportOptions PolyToolkitInternal.PolyMainInternal/<FetchAndImportFormat>c__AnonStorey4::options
	PolyImportOptions_t4213423452  ___options_2;
	// PolyToolkit.PolyApi/ImportCallback PolyToolkitInternal.PolyMainInternal/<FetchAndImportFormat>c__AnonStorey4::callback
	ImportCallback_t3513750376 * ___callback_3;
	// PolyToolkitInternal.PolyMainInternal PolyToolkitInternal.PolyMainInternal/<FetchAndImportFormat>c__AnonStorey4::$this
	PolyMainInternal_t3329830951 * ___U24this_4;

public:
	inline static int32_t get_offset_of_format_0() { return static_cast<int32_t>(offsetof(U3CFetchAndImportFormatU3Ec__AnonStorey4_t209984363, ___format_0)); }
	inline PolyFormat_t1880249796 * get_format_0() const { return ___format_0; }
	inline PolyFormat_t1880249796 ** get_address_of_format_0() { return &___format_0; }
	inline void set_format_0(PolyFormat_t1880249796 * value)
	{
		___format_0 = value;
		Il2CppCodeGenWriteBarrier((&___format_0), value);
	}

	inline static int32_t get_offset_of_asset_1() { return static_cast<int32_t>(offsetof(U3CFetchAndImportFormatU3Ec__AnonStorey4_t209984363, ___asset_1)); }
	inline PolyAsset_t1814153511 * get_asset_1() const { return ___asset_1; }
	inline PolyAsset_t1814153511 ** get_address_of_asset_1() { return &___asset_1; }
	inline void set_asset_1(PolyAsset_t1814153511 * value)
	{
		___asset_1 = value;
		Il2CppCodeGenWriteBarrier((&___asset_1), value);
	}

	inline static int32_t get_offset_of_options_2() { return static_cast<int32_t>(offsetof(U3CFetchAndImportFormatU3Ec__AnonStorey4_t209984363, ___options_2)); }
	inline PolyImportOptions_t4213423452  get_options_2() const { return ___options_2; }
	inline PolyImportOptions_t4213423452 * get_address_of_options_2() { return &___options_2; }
	inline void set_options_2(PolyImportOptions_t4213423452  value)
	{
		___options_2 = value;
	}

	inline static int32_t get_offset_of_callback_3() { return static_cast<int32_t>(offsetof(U3CFetchAndImportFormatU3Ec__AnonStorey4_t209984363, ___callback_3)); }
	inline ImportCallback_t3513750376 * get_callback_3() const { return ___callback_3; }
	inline ImportCallback_t3513750376 ** get_address_of_callback_3() { return &___callback_3; }
	inline void set_callback_3(ImportCallback_t3513750376 * value)
	{
		___callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___callback_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CFetchAndImportFormatU3Ec__AnonStorey4_t209984363, ___U24this_4)); }
	inline PolyMainInternal_t3329830951 * get_U24this_4() const { return ___U24this_4; }
	inline PolyMainInternal_t3329830951 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(PolyMainInternal_t3329830951 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFETCHANDIMPORTFORMATU3EC__ANONSTOREY4_T209984363_H
#ifndef EDITTIMEIMPORTOPTIONS_T4030780733_H
#define EDITTIMEIMPORTOPTIONS_T4030780733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.EditTimeImportOptions
struct  EditTimeImportOptions_t4030780733 
{
public:
	// PolyToolkit.PolyImportOptions PolyToolkitInternal.EditTimeImportOptions::baseOptions
	PolyImportOptions_t4213423452  ___baseOptions_0;
	// System.Boolean PolyToolkitInternal.EditTimeImportOptions::alsoInstantiate
	bool ___alsoInstantiate_1;

public:
	inline static int32_t get_offset_of_baseOptions_0() { return static_cast<int32_t>(offsetof(EditTimeImportOptions_t4030780733, ___baseOptions_0)); }
	inline PolyImportOptions_t4213423452  get_baseOptions_0() const { return ___baseOptions_0; }
	inline PolyImportOptions_t4213423452 * get_address_of_baseOptions_0() { return &___baseOptions_0; }
	inline void set_baseOptions_0(PolyImportOptions_t4213423452  value)
	{
		___baseOptions_0 = value;
	}

	inline static int32_t get_offset_of_alsoInstantiate_1() { return static_cast<int32_t>(offsetof(EditTimeImportOptions_t4030780733, ___alsoInstantiate_1)); }
	inline bool get_alsoInstantiate_1() const { return ___alsoInstantiate_1; }
	inline bool* get_address_of_alsoInstantiate_1() { return &___alsoInstantiate_1; }
	inline void set_alsoInstantiate_1(bool value)
	{
		___alsoInstantiate_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PolyToolkitInternal.EditTimeImportOptions
struct EditTimeImportOptions_t4030780733_marshaled_pinvoke
{
	PolyImportOptions_t4213423452_marshaled_pinvoke ___baseOptions_0;
	int32_t ___alsoInstantiate_1;
};
// Native definition for COM marshalling of PolyToolkitInternal.EditTimeImportOptions
struct EditTimeImportOptions_t4030780733_marshaled_com
{
	PolyImportOptions_t4213423452_marshaled_com ___baseOptions_0;
	int32_t ___alsoInstantiate_1;
};
#endif // EDITTIMEIMPORTOPTIONS_T4030780733_H
#ifndef COMPLETIONCALLBACK_T3367319633_H
#define COMPLETIONCALLBACK_T3367319633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.client.model.util.WebRequestManager/CompletionCallback
struct  CompletionCallback_t3367319633  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPLETIONCALLBACK_T3367319633_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef FETCHPROGRESSCALLBACK_T1972555609_H
#define FETCHPROGRESSCALLBACK_T1972555609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.PolyMainInternal/FetchProgressCallback
struct  FetchProgressCallback_t1972555609  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FETCHPROGRESSCALLBACK_T1972555609_H
#ifndef IMPORTCALLBACK_T3513750376_H
#define IMPORTCALLBACK_T3513750376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyApi/ImportCallback
struct  ImportCallback_t3513750376  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMPORTCALLBACK_T3513750376_H
#ifndef POLYASSET_T1814153511_H
#define POLYASSET_T1814153511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyAsset
struct  PolyAsset_t1814153511  : public RuntimeObject
{
public:
	// System.String PolyToolkit.PolyAsset::name
	String_t* ___name_1;
	// System.String PolyToolkit.PolyAsset::displayName
	String_t* ___displayName_2;
	// System.String PolyToolkit.PolyAsset::authorName
	String_t* ___authorName_3;
	// System.String PolyToolkit.PolyAsset::description
	String_t* ___description_4;
	// System.DateTime PolyToolkit.PolyAsset::createTime
	DateTime_t3738529785  ___createTime_5;
	// System.DateTime PolyToolkit.PolyAsset::updateTime
	DateTime_t3738529785  ___updateTime_6;
	// System.Collections.Generic.List`1<PolyToolkit.PolyFormat> PolyToolkit.PolyAsset::formats
	List_1_t3352324538 * ___formats_7;
	// PolyToolkit.PolyFile PolyToolkit.PolyAsset::thumbnail
	PolyFile_t4211357808 * ___thumbnail_8;
	// PolyToolkit.PolyAssetLicense PolyToolkit.PolyAsset::license
	int32_t ___license_9;
	// PolyToolkit.PolyVisibility PolyToolkit.PolyAsset::visibility
	int32_t ___visibility_10;
	// System.Boolean PolyToolkit.PolyAsset::isCurated
	bool ___isCurated_11;
	// UnityEngine.Texture2D PolyToolkit.PolyAsset::thumbnailTexture
	Texture2D_t3840446185 * ___thumbnailTexture_12;

public:
	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(PolyAsset_t1814153511, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_displayName_2() { return static_cast<int32_t>(offsetof(PolyAsset_t1814153511, ___displayName_2)); }
	inline String_t* get_displayName_2() const { return ___displayName_2; }
	inline String_t** get_address_of_displayName_2() { return &___displayName_2; }
	inline void set_displayName_2(String_t* value)
	{
		___displayName_2 = value;
		Il2CppCodeGenWriteBarrier((&___displayName_2), value);
	}

	inline static int32_t get_offset_of_authorName_3() { return static_cast<int32_t>(offsetof(PolyAsset_t1814153511, ___authorName_3)); }
	inline String_t* get_authorName_3() const { return ___authorName_3; }
	inline String_t** get_address_of_authorName_3() { return &___authorName_3; }
	inline void set_authorName_3(String_t* value)
	{
		___authorName_3 = value;
		Il2CppCodeGenWriteBarrier((&___authorName_3), value);
	}

	inline static int32_t get_offset_of_description_4() { return static_cast<int32_t>(offsetof(PolyAsset_t1814153511, ___description_4)); }
	inline String_t* get_description_4() const { return ___description_4; }
	inline String_t** get_address_of_description_4() { return &___description_4; }
	inline void set_description_4(String_t* value)
	{
		___description_4 = value;
		Il2CppCodeGenWriteBarrier((&___description_4), value);
	}

	inline static int32_t get_offset_of_createTime_5() { return static_cast<int32_t>(offsetof(PolyAsset_t1814153511, ___createTime_5)); }
	inline DateTime_t3738529785  get_createTime_5() const { return ___createTime_5; }
	inline DateTime_t3738529785 * get_address_of_createTime_5() { return &___createTime_5; }
	inline void set_createTime_5(DateTime_t3738529785  value)
	{
		___createTime_5 = value;
	}

	inline static int32_t get_offset_of_updateTime_6() { return static_cast<int32_t>(offsetof(PolyAsset_t1814153511, ___updateTime_6)); }
	inline DateTime_t3738529785  get_updateTime_6() const { return ___updateTime_6; }
	inline DateTime_t3738529785 * get_address_of_updateTime_6() { return &___updateTime_6; }
	inline void set_updateTime_6(DateTime_t3738529785  value)
	{
		___updateTime_6 = value;
	}

	inline static int32_t get_offset_of_formats_7() { return static_cast<int32_t>(offsetof(PolyAsset_t1814153511, ___formats_7)); }
	inline List_1_t3352324538 * get_formats_7() const { return ___formats_7; }
	inline List_1_t3352324538 ** get_address_of_formats_7() { return &___formats_7; }
	inline void set_formats_7(List_1_t3352324538 * value)
	{
		___formats_7 = value;
		Il2CppCodeGenWriteBarrier((&___formats_7), value);
	}

	inline static int32_t get_offset_of_thumbnail_8() { return static_cast<int32_t>(offsetof(PolyAsset_t1814153511, ___thumbnail_8)); }
	inline PolyFile_t4211357808 * get_thumbnail_8() const { return ___thumbnail_8; }
	inline PolyFile_t4211357808 ** get_address_of_thumbnail_8() { return &___thumbnail_8; }
	inline void set_thumbnail_8(PolyFile_t4211357808 * value)
	{
		___thumbnail_8 = value;
		Il2CppCodeGenWriteBarrier((&___thumbnail_8), value);
	}

	inline static int32_t get_offset_of_license_9() { return static_cast<int32_t>(offsetof(PolyAsset_t1814153511, ___license_9)); }
	inline int32_t get_license_9() const { return ___license_9; }
	inline int32_t* get_address_of_license_9() { return &___license_9; }
	inline void set_license_9(int32_t value)
	{
		___license_9 = value;
	}

	inline static int32_t get_offset_of_visibility_10() { return static_cast<int32_t>(offsetof(PolyAsset_t1814153511, ___visibility_10)); }
	inline int32_t get_visibility_10() const { return ___visibility_10; }
	inline int32_t* get_address_of_visibility_10() { return &___visibility_10; }
	inline void set_visibility_10(int32_t value)
	{
		___visibility_10 = value;
	}

	inline static int32_t get_offset_of_isCurated_11() { return static_cast<int32_t>(offsetof(PolyAsset_t1814153511, ___isCurated_11)); }
	inline bool get_isCurated_11() const { return ___isCurated_11; }
	inline bool* get_address_of_isCurated_11() { return &___isCurated_11; }
	inline void set_isCurated_11(bool value)
	{
		___isCurated_11 = value;
	}

	inline static int32_t get_offset_of_thumbnailTexture_12() { return static_cast<int32_t>(offsetof(PolyAsset_t1814153511, ___thumbnailTexture_12)); }
	inline Texture2D_t3840446185 * get_thumbnailTexture_12() const { return ___thumbnailTexture_12; }
	inline Texture2D_t3840446185 ** get_address_of_thumbnailTexture_12() { return &___thumbnailTexture_12; }
	inline void set_thumbnailTexture_12(Texture2D_t3840446185 * value)
	{
		___thumbnailTexture_12 = value;
		Il2CppCodeGenWriteBarrier((&___thumbnailTexture_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYASSET_T1814153511_H
#ifndef FETCHTHUMBNAILCALLBACK_T3242648953_H
#define FETCHTHUMBNAILCALLBACK_T3242648953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyApi/FetchThumbnailCallback
struct  FetchThumbnailCallback_t3242648953  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FETCHTHUMBNAILCALLBACK_T3242648953_H
#ifndef POLYLISTUSERASSETSREQUEST_T1125477019_H
#define POLYLISTUSERASSETSREQUEST_T1125477019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyListUserAssetsRequest
struct  PolyListUserAssetsRequest_t1125477019  : public PolyRequest_t2374976743
{
public:
	// PolyToolkit.PolyFormatType PolyToolkit.PolyListUserAssetsRequest::format
	int32_t ___format_3;
	// PolyToolkit.PolyVisibilityFilter PolyToolkit.PolyListUserAssetsRequest::visibility
	int32_t ___visibility_4;
	// System.Nullable`1<PolyToolkit.PolyFormatFilter> PolyToolkit.PolyListUserAssetsRequest::formatFilter
	Nullable_1_t3643161861  ___formatFilter_5;

public:
	inline static int32_t get_offset_of_format_3() { return static_cast<int32_t>(offsetof(PolyListUserAssetsRequest_t1125477019, ___format_3)); }
	inline int32_t get_format_3() const { return ___format_3; }
	inline int32_t* get_address_of_format_3() { return &___format_3; }
	inline void set_format_3(int32_t value)
	{
		___format_3 = value;
	}

	inline static int32_t get_offset_of_visibility_4() { return static_cast<int32_t>(offsetof(PolyListUserAssetsRequest_t1125477019, ___visibility_4)); }
	inline int32_t get_visibility_4() const { return ___visibility_4; }
	inline int32_t* get_address_of_visibility_4() { return &___visibility_4; }
	inline void set_visibility_4(int32_t value)
	{
		___visibility_4 = value;
	}

	inline static int32_t get_offset_of_formatFilter_5() { return static_cast<int32_t>(offsetof(PolyListUserAssetsRequest_t1125477019, ___formatFilter_5)); }
	inline Nullable_1_t3643161861  get_formatFilter_5() const { return ___formatFilter_5; }
	inline Nullable_1_t3643161861 * get_address_of_formatFilter_5() { return &___formatFilter_5; }
	inline void set_formatFilter_5(Nullable_1_t3643161861  value)
	{
		___formatFilter_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYLISTUSERASSETSREQUEST_T1125477019_H
#ifndef FETCHFORMATFILESCALLBACK_T2377205724_H
#define FETCHFORMATFILESCALLBACK_T2377205724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyApi/FetchFormatFilesCallback
struct  FetchFormatFilesCallback_t2377205724  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FETCHFORMATFILESCALLBACK_T2377205724_H
#ifndef GETASSETCALLBACK_T1146242383_H
#define GETASSETCALLBACK_T1146242383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyApi/GetAssetCallback
struct  GetAssetCallback_t1146242383  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETASSETCALLBACK_T1146242383_H
#ifndef POLYLISTLIKEDASSETSREQUEST_T4202721371_H
#define POLYLISTLIKEDASSETSREQUEST_T4202721371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyListLikedAssetsRequest
struct  PolyListLikedAssetsRequest_t4202721371  : public PolyRequest_t2374976743
{
public:
	// System.String PolyToolkit.PolyListLikedAssetsRequest::name
	String_t* ___name_3;

public:
	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(PolyListLikedAssetsRequest_t4202721371, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYLISTLIKEDASSETSREQUEST_T4202721371_H
#ifndef CREATIONCALLBACK_T949890767_H
#define CREATIONCALLBACK_T949890767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.client.model.util.WebRequestManager/CreationCallback
struct  CreationCallback_t949890767  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATIONCALLBACK_T949890767_H
#ifndef LISTASSETSCALLBACK_T3881593442_H
#define LISTASSETSCALLBACK_T3881593442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyApi/ListAssetsCallback
struct  ListAssetsCallback_t3881593442  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTASSETSCALLBACK_T3881593442_H
#ifndef POLYLISTASSETSREQUEST_T3768481913_H
#define POLYLISTASSETSREQUEST_T3768481913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyListAssetsRequest
struct  PolyListAssetsRequest_t3768481913  : public PolyRequest_t2374976743
{
public:
	// System.String PolyToolkit.PolyListAssetsRequest::keywords
	String_t* ___keywords_3;
	// System.Boolean PolyToolkit.PolyListAssetsRequest::curated
	bool ___curated_4;
	// PolyToolkit.PolyCategory PolyToolkit.PolyListAssetsRequest::category
	int32_t ___category_5;
	// PolyToolkit.PolyMaxComplexityFilter PolyToolkit.PolyListAssetsRequest::maxComplexity
	int32_t ___maxComplexity_6;
	// System.Nullable`1<PolyToolkit.PolyFormatFilter> PolyToolkit.PolyListAssetsRequest::formatFilter
	Nullable_1_t3643161861  ___formatFilter_7;

public:
	inline static int32_t get_offset_of_keywords_3() { return static_cast<int32_t>(offsetof(PolyListAssetsRequest_t3768481913, ___keywords_3)); }
	inline String_t* get_keywords_3() const { return ___keywords_3; }
	inline String_t** get_address_of_keywords_3() { return &___keywords_3; }
	inline void set_keywords_3(String_t* value)
	{
		___keywords_3 = value;
		Il2CppCodeGenWriteBarrier((&___keywords_3), value);
	}

	inline static int32_t get_offset_of_curated_4() { return static_cast<int32_t>(offsetof(PolyListAssetsRequest_t3768481913, ___curated_4)); }
	inline bool get_curated_4() const { return ___curated_4; }
	inline bool* get_address_of_curated_4() { return &___curated_4; }
	inline void set_curated_4(bool value)
	{
		___curated_4 = value;
	}

	inline static int32_t get_offset_of_category_5() { return static_cast<int32_t>(offsetof(PolyListAssetsRequest_t3768481913, ___category_5)); }
	inline int32_t get_category_5() const { return ___category_5; }
	inline int32_t* get_address_of_category_5() { return &___category_5; }
	inline void set_category_5(int32_t value)
	{
		___category_5 = value;
	}

	inline static int32_t get_offset_of_maxComplexity_6() { return static_cast<int32_t>(offsetof(PolyListAssetsRequest_t3768481913, ___maxComplexity_6)); }
	inline int32_t get_maxComplexity_6() const { return ___maxComplexity_6; }
	inline int32_t* get_address_of_maxComplexity_6() { return &___maxComplexity_6; }
	inline void set_maxComplexity_6(int32_t value)
	{
		___maxComplexity_6 = value;
	}

	inline static int32_t get_offset_of_formatFilter_7() { return static_cast<int32_t>(offsetof(PolyListAssetsRequest_t3768481913, ___formatFilter_7)); }
	inline Nullable_1_t3643161861  get_formatFilter_7() const { return ___formatFilter_7; }
	inline Nullable_1_t3643161861 * get_address_of_formatFilter_7() { return &___formatFilter_7; }
	inline void set_formatFilter_7(Nullable_1_t3643161861  value)
	{
		___formatFilter_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYLISTASSETSREQUEST_T3768481913_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef PTSETTINGS_T265752063_H
#define PTSETTINGS_T265752063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.PtSettings
struct  PtSettings_t265752063  : public ScriptableObject_t2528358522
{
public:
	// UnityEngine.ColorSpace PolyToolkitInternal.PtSettings::playerColorSpace
	int32_t ___playerColorSpace_3;
	// PolyToolkitInternal.PtSettings/SurfaceShaderMaterial[] PolyToolkitInternal.PtSettings::surfaceShaderMaterials
	SurfaceShaderMaterialU5BU5D_t772937648* ___surfaceShaderMaterials_4;
	// System.String PolyToolkitInternal.PtSettings::assetObjectsPath
	String_t* ___assetObjectsPath_5;
	// System.String PolyToolkitInternal.PtSettings::assetSourcesPath
	String_t* ___assetSourcesPath_6;
	// System.String PolyToolkitInternal.PtSettings::resourcesPath
	String_t* ___resourcesPath_7;
	// PolyToolkitInternal.LengthWithUnit PolyToolkitInternal.PtSettings::sceneUnit
	LengthWithUnit_t3846227886  ___sceneUnit_8;
	// PolyToolkitInternal.EditTimeImportOptions PolyToolkitInternal.PtSettings::defaultImportOptions
	EditTimeImportOptions_t4030780733  ___defaultImportOptions_9;
	// TiltBrushToolkit.BrushManifest PolyToolkitInternal.PtSettings::brushManifest
	BrushManifest_t1931127838 * ___brushManifest_10;
	// UnityEngine.Material PolyToolkitInternal.PtSettings::basePbrOpaqueDoubleSidedMaterial
	Material_t340375123 * ___basePbrOpaqueDoubleSidedMaterial_11;
	// UnityEngine.Material PolyToolkitInternal.PtSettings::basePbrBlendDoubleSidedMaterial
	Material_t340375123 * ___basePbrBlendDoubleSidedMaterial_12;
	// PolyToolkit.PolyAuthConfig PolyToolkitInternal.PtSettings::authConfig
	PolyAuthConfig_t4147939475  ___authConfig_13;
	// PolyToolkit.PolyCacheConfig PolyToolkitInternal.PtSettings::cacheConfig
	PolyCacheConfig_t1397553534  ___cacheConfig_14;
	// System.Boolean PolyToolkitInternal.PtSettings::sendEditorAnalytics
	bool ___sendEditorAnalytics_15;
	// System.Boolean PolyToolkitInternal.PtSettings::warnOnSourceOverwrite
	bool ___warnOnSourceOverwrite_16;
	// System.Boolean PolyToolkitInternal.PtSettings::warnOfApiCompatibility
	bool ___warnOfApiCompatibility_17;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material> PolyToolkitInternal.PtSettings::surfaceShaderMaterialLookup
	Dictionary_2_t125631422 * ___surfaceShaderMaterialLookup_18;

public:
	inline static int32_t get_offset_of_playerColorSpace_3() { return static_cast<int32_t>(offsetof(PtSettings_t265752063, ___playerColorSpace_3)); }
	inline int32_t get_playerColorSpace_3() const { return ___playerColorSpace_3; }
	inline int32_t* get_address_of_playerColorSpace_3() { return &___playerColorSpace_3; }
	inline void set_playerColorSpace_3(int32_t value)
	{
		___playerColorSpace_3 = value;
	}

	inline static int32_t get_offset_of_surfaceShaderMaterials_4() { return static_cast<int32_t>(offsetof(PtSettings_t265752063, ___surfaceShaderMaterials_4)); }
	inline SurfaceShaderMaterialU5BU5D_t772937648* get_surfaceShaderMaterials_4() const { return ___surfaceShaderMaterials_4; }
	inline SurfaceShaderMaterialU5BU5D_t772937648** get_address_of_surfaceShaderMaterials_4() { return &___surfaceShaderMaterials_4; }
	inline void set_surfaceShaderMaterials_4(SurfaceShaderMaterialU5BU5D_t772937648* value)
	{
		___surfaceShaderMaterials_4 = value;
		Il2CppCodeGenWriteBarrier((&___surfaceShaderMaterials_4), value);
	}

	inline static int32_t get_offset_of_assetObjectsPath_5() { return static_cast<int32_t>(offsetof(PtSettings_t265752063, ___assetObjectsPath_5)); }
	inline String_t* get_assetObjectsPath_5() const { return ___assetObjectsPath_5; }
	inline String_t** get_address_of_assetObjectsPath_5() { return &___assetObjectsPath_5; }
	inline void set_assetObjectsPath_5(String_t* value)
	{
		___assetObjectsPath_5 = value;
		Il2CppCodeGenWriteBarrier((&___assetObjectsPath_5), value);
	}

	inline static int32_t get_offset_of_assetSourcesPath_6() { return static_cast<int32_t>(offsetof(PtSettings_t265752063, ___assetSourcesPath_6)); }
	inline String_t* get_assetSourcesPath_6() const { return ___assetSourcesPath_6; }
	inline String_t** get_address_of_assetSourcesPath_6() { return &___assetSourcesPath_6; }
	inline void set_assetSourcesPath_6(String_t* value)
	{
		___assetSourcesPath_6 = value;
		Il2CppCodeGenWriteBarrier((&___assetSourcesPath_6), value);
	}

	inline static int32_t get_offset_of_resourcesPath_7() { return static_cast<int32_t>(offsetof(PtSettings_t265752063, ___resourcesPath_7)); }
	inline String_t* get_resourcesPath_7() const { return ___resourcesPath_7; }
	inline String_t** get_address_of_resourcesPath_7() { return &___resourcesPath_7; }
	inline void set_resourcesPath_7(String_t* value)
	{
		___resourcesPath_7 = value;
		Il2CppCodeGenWriteBarrier((&___resourcesPath_7), value);
	}

	inline static int32_t get_offset_of_sceneUnit_8() { return static_cast<int32_t>(offsetof(PtSettings_t265752063, ___sceneUnit_8)); }
	inline LengthWithUnit_t3846227886  get_sceneUnit_8() const { return ___sceneUnit_8; }
	inline LengthWithUnit_t3846227886 * get_address_of_sceneUnit_8() { return &___sceneUnit_8; }
	inline void set_sceneUnit_8(LengthWithUnit_t3846227886  value)
	{
		___sceneUnit_8 = value;
	}

	inline static int32_t get_offset_of_defaultImportOptions_9() { return static_cast<int32_t>(offsetof(PtSettings_t265752063, ___defaultImportOptions_9)); }
	inline EditTimeImportOptions_t4030780733  get_defaultImportOptions_9() const { return ___defaultImportOptions_9; }
	inline EditTimeImportOptions_t4030780733 * get_address_of_defaultImportOptions_9() { return &___defaultImportOptions_9; }
	inline void set_defaultImportOptions_9(EditTimeImportOptions_t4030780733  value)
	{
		___defaultImportOptions_9 = value;
	}

	inline static int32_t get_offset_of_brushManifest_10() { return static_cast<int32_t>(offsetof(PtSettings_t265752063, ___brushManifest_10)); }
	inline BrushManifest_t1931127838 * get_brushManifest_10() const { return ___brushManifest_10; }
	inline BrushManifest_t1931127838 ** get_address_of_brushManifest_10() { return &___brushManifest_10; }
	inline void set_brushManifest_10(BrushManifest_t1931127838 * value)
	{
		___brushManifest_10 = value;
		Il2CppCodeGenWriteBarrier((&___brushManifest_10), value);
	}

	inline static int32_t get_offset_of_basePbrOpaqueDoubleSidedMaterial_11() { return static_cast<int32_t>(offsetof(PtSettings_t265752063, ___basePbrOpaqueDoubleSidedMaterial_11)); }
	inline Material_t340375123 * get_basePbrOpaqueDoubleSidedMaterial_11() const { return ___basePbrOpaqueDoubleSidedMaterial_11; }
	inline Material_t340375123 ** get_address_of_basePbrOpaqueDoubleSidedMaterial_11() { return &___basePbrOpaqueDoubleSidedMaterial_11; }
	inline void set_basePbrOpaqueDoubleSidedMaterial_11(Material_t340375123 * value)
	{
		___basePbrOpaqueDoubleSidedMaterial_11 = value;
		Il2CppCodeGenWriteBarrier((&___basePbrOpaqueDoubleSidedMaterial_11), value);
	}

	inline static int32_t get_offset_of_basePbrBlendDoubleSidedMaterial_12() { return static_cast<int32_t>(offsetof(PtSettings_t265752063, ___basePbrBlendDoubleSidedMaterial_12)); }
	inline Material_t340375123 * get_basePbrBlendDoubleSidedMaterial_12() const { return ___basePbrBlendDoubleSidedMaterial_12; }
	inline Material_t340375123 ** get_address_of_basePbrBlendDoubleSidedMaterial_12() { return &___basePbrBlendDoubleSidedMaterial_12; }
	inline void set_basePbrBlendDoubleSidedMaterial_12(Material_t340375123 * value)
	{
		___basePbrBlendDoubleSidedMaterial_12 = value;
		Il2CppCodeGenWriteBarrier((&___basePbrBlendDoubleSidedMaterial_12), value);
	}

	inline static int32_t get_offset_of_authConfig_13() { return static_cast<int32_t>(offsetof(PtSettings_t265752063, ___authConfig_13)); }
	inline PolyAuthConfig_t4147939475  get_authConfig_13() const { return ___authConfig_13; }
	inline PolyAuthConfig_t4147939475 * get_address_of_authConfig_13() { return &___authConfig_13; }
	inline void set_authConfig_13(PolyAuthConfig_t4147939475  value)
	{
		___authConfig_13 = value;
	}

	inline static int32_t get_offset_of_cacheConfig_14() { return static_cast<int32_t>(offsetof(PtSettings_t265752063, ___cacheConfig_14)); }
	inline PolyCacheConfig_t1397553534  get_cacheConfig_14() const { return ___cacheConfig_14; }
	inline PolyCacheConfig_t1397553534 * get_address_of_cacheConfig_14() { return &___cacheConfig_14; }
	inline void set_cacheConfig_14(PolyCacheConfig_t1397553534  value)
	{
		___cacheConfig_14 = value;
	}

	inline static int32_t get_offset_of_sendEditorAnalytics_15() { return static_cast<int32_t>(offsetof(PtSettings_t265752063, ___sendEditorAnalytics_15)); }
	inline bool get_sendEditorAnalytics_15() const { return ___sendEditorAnalytics_15; }
	inline bool* get_address_of_sendEditorAnalytics_15() { return &___sendEditorAnalytics_15; }
	inline void set_sendEditorAnalytics_15(bool value)
	{
		___sendEditorAnalytics_15 = value;
	}

	inline static int32_t get_offset_of_warnOnSourceOverwrite_16() { return static_cast<int32_t>(offsetof(PtSettings_t265752063, ___warnOnSourceOverwrite_16)); }
	inline bool get_warnOnSourceOverwrite_16() const { return ___warnOnSourceOverwrite_16; }
	inline bool* get_address_of_warnOnSourceOverwrite_16() { return &___warnOnSourceOverwrite_16; }
	inline void set_warnOnSourceOverwrite_16(bool value)
	{
		___warnOnSourceOverwrite_16 = value;
	}

	inline static int32_t get_offset_of_warnOfApiCompatibility_17() { return static_cast<int32_t>(offsetof(PtSettings_t265752063, ___warnOfApiCompatibility_17)); }
	inline bool get_warnOfApiCompatibility_17() const { return ___warnOfApiCompatibility_17; }
	inline bool* get_address_of_warnOfApiCompatibility_17() { return &___warnOfApiCompatibility_17; }
	inline void set_warnOfApiCompatibility_17(bool value)
	{
		___warnOfApiCompatibility_17 = value;
	}

	inline static int32_t get_offset_of_surfaceShaderMaterialLookup_18() { return static_cast<int32_t>(offsetof(PtSettings_t265752063, ___surfaceShaderMaterialLookup_18)); }
	inline Dictionary_2_t125631422 * get_surfaceShaderMaterialLookup_18() const { return ___surfaceShaderMaterialLookup_18; }
	inline Dictionary_2_t125631422 ** get_address_of_surfaceShaderMaterialLookup_18() { return &___surfaceShaderMaterialLookup_18; }
	inline void set_surfaceShaderMaterialLookup_18(Dictionary_2_t125631422 * value)
	{
		___surfaceShaderMaterialLookup_18 = value;
		Il2CppCodeGenWriteBarrier((&___surfaceShaderMaterialLookup_18), value);
	}
};

struct PtSettings_t265752063_StaticFields
{
public:
	// PolyToolkitInternal.PtSettings PolyToolkitInternal.PtSettings::instance
	PtSettings_t265752063 * ___instance_2;
	// System.Func`2<PolyToolkitInternal.PtSettings/SurfaceShaderMaterial,System.String> PolyToolkitInternal.PtSettings::<>f__am$cache0
	Func_2_t3438440518 * ___U3CU3Ef__amU24cache0_19;
	// System.Func`2<PolyToolkitInternal.PtSettings/SurfaceShaderMaterial,UnityEngine.Material> PolyToolkitInternal.PtSettings::<>f__am$cache1
	Func_2_t1931364952 * ___U3CU3Ef__amU24cache1_20;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(PtSettings_t265752063_StaticFields, ___instance_2)); }
	inline PtSettings_t265752063 * get_instance_2() const { return ___instance_2; }
	inline PtSettings_t265752063 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(PtSettings_t265752063 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_19() { return static_cast<int32_t>(offsetof(PtSettings_t265752063_StaticFields, ___U3CU3Ef__amU24cache0_19)); }
	inline Func_2_t3438440518 * get_U3CU3Ef__amU24cache0_19() const { return ___U3CU3Ef__amU24cache0_19; }
	inline Func_2_t3438440518 ** get_address_of_U3CU3Ef__amU24cache0_19() { return &___U3CU3Ef__amU24cache0_19; }
	inline void set_U3CU3Ef__amU24cache0_19(Func_2_t3438440518 * value)
	{
		___U3CU3Ef__amU24cache0_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_20() { return static_cast<int32_t>(offsetof(PtSettings_t265752063_StaticFields, ___U3CU3Ef__amU24cache1_20)); }
	inline Func_2_t1931364952 * get_U3CU3Ef__amU24cache1_20() const { return ___U3CU3Ef__amU24cache1_20; }
	inline Func_2_t1931364952 ** get_address_of_U3CU3Ef__amU24cache1_20() { return &___U3CU3Ef__amU24cache1_20; }
	inline void set_U3CU3Ef__amU24cache1_20(Func_2_t1931364952 * value)
	{
		___U3CU3Ef__amU24cache1_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PTSETTINGS_T265752063_H
#ifndef WEBREQUESTMANAGER_T2098015550_H
#define WEBREQUESTMANAGER_T2098015550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.client.model.util.WebRequestManager
struct  WebRequestManager_t2098015550  : public MonoBehaviour_t3962482529
{
public:
	// PolyToolkitInternal.model.util.ConcurrentQueue`1<PolyToolkitInternal.client.model.util.WebRequestManager/PendingRequest> PolyToolkitInternal.client.model.util.WebRequestManager::pendingRequests
	ConcurrentQueue_1_t3466100300 * ___pendingRequests_7;
	// System.Collections.Generic.List`1<PolyToolkitInternal.client.model.util.WebRequestManager/BufferHolder> PolyToolkitInternal.client.model.util.WebRequestManager::idleBuffers
	List_1_t1896763570 * ___idleBuffers_8;
	// PolyToolkitInternal.caching.PersistentBlobCache PolyToolkitInternal.client.model.util.WebRequestManager::cache
	PersistentBlobCache_t1212092596 * ___cache_9;

public:
	inline static int32_t get_offset_of_pendingRequests_7() { return static_cast<int32_t>(offsetof(WebRequestManager_t2098015550, ___pendingRequests_7)); }
	inline ConcurrentQueue_1_t3466100300 * get_pendingRequests_7() const { return ___pendingRequests_7; }
	inline ConcurrentQueue_1_t3466100300 ** get_address_of_pendingRequests_7() { return &___pendingRequests_7; }
	inline void set_pendingRequests_7(ConcurrentQueue_1_t3466100300 * value)
	{
		___pendingRequests_7 = value;
		Il2CppCodeGenWriteBarrier((&___pendingRequests_7), value);
	}

	inline static int32_t get_offset_of_idleBuffers_8() { return static_cast<int32_t>(offsetof(WebRequestManager_t2098015550, ___idleBuffers_8)); }
	inline List_1_t1896763570 * get_idleBuffers_8() const { return ___idleBuffers_8; }
	inline List_1_t1896763570 ** get_address_of_idleBuffers_8() { return &___idleBuffers_8; }
	inline void set_idleBuffers_8(List_1_t1896763570 * value)
	{
		___idleBuffers_8 = value;
		Il2CppCodeGenWriteBarrier((&___idleBuffers_8), value);
	}

	inline static int32_t get_offset_of_cache_9() { return static_cast<int32_t>(offsetof(WebRequestManager_t2098015550, ___cache_9)); }
	inline PersistentBlobCache_t1212092596 * get_cache_9() const { return ___cache_9; }
	inline PersistentBlobCache_t1212092596 ** get_address_of_cache_9() { return &___cache_9; }
	inline void set_cache_9(PersistentBlobCache_t1212092596 * value)
	{
		___cache_9 = value;
		Il2CppCodeGenWriteBarrier((&___cache_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTMANAGER_T2098015550_H
#ifndef BACKGROUNDMAIN_T2789389941_H
#define BACKGROUNDMAIN_T2789389941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.model.util.BackgroundMain
struct  BackgroundMain_t2789389941  : public MonoBehaviour_t3962482529
{
public:
	// System.Threading.Thread PolyToolkitInternal.model.util.BackgroundMain::backgroundThread
	Thread_t2300836069 * ___backgroundThread_2;
	// System.Boolean PolyToolkitInternal.model.util.BackgroundMain::running
	bool ___running_3;
	// System.Boolean PolyToolkitInternal.model.util.BackgroundMain::setupDone
	bool ___setupDone_4;
	// PolyToolkitInternal.model.util.ConcurrentQueue`1<PolyToolkitInternal.model.util.BackgroundWork> PolyToolkitInternal.model.util.BackgroundMain::backgroundQueue
	ConcurrentQueue_1_t133829771 * ___backgroundQueue_5;
	// PolyToolkitInternal.model.util.ConcurrentQueue`1<PolyToolkitInternal.model.util.BackgroundWork> PolyToolkitInternal.model.util.BackgroundMain::forMainThread
	ConcurrentQueue_1_t133829771 * ___forMainThread_6;

public:
	inline static int32_t get_offset_of_backgroundThread_2() { return static_cast<int32_t>(offsetof(BackgroundMain_t2789389941, ___backgroundThread_2)); }
	inline Thread_t2300836069 * get_backgroundThread_2() const { return ___backgroundThread_2; }
	inline Thread_t2300836069 ** get_address_of_backgroundThread_2() { return &___backgroundThread_2; }
	inline void set_backgroundThread_2(Thread_t2300836069 * value)
	{
		___backgroundThread_2 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundThread_2), value);
	}

	inline static int32_t get_offset_of_running_3() { return static_cast<int32_t>(offsetof(BackgroundMain_t2789389941, ___running_3)); }
	inline bool get_running_3() const { return ___running_3; }
	inline bool* get_address_of_running_3() { return &___running_3; }
	inline void set_running_3(bool value)
	{
		___running_3 = value;
	}

	inline static int32_t get_offset_of_setupDone_4() { return static_cast<int32_t>(offsetof(BackgroundMain_t2789389941, ___setupDone_4)); }
	inline bool get_setupDone_4() const { return ___setupDone_4; }
	inline bool* get_address_of_setupDone_4() { return &___setupDone_4; }
	inline void set_setupDone_4(bool value)
	{
		___setupDone_4 = value;
	}

	inline static int32_t get_offset_of_backgroundQueue_5() { return static_cast<int32_t>(offsetof(BackgroundMain_t2789389941, ___backgroundQueue_5)); }
	inline ConcurrentQueue_1_t133829771 * get_backgroundQueue_5() const { return ___backgroundQueue_5; }
	inline ConcurrentQueue_1_t133829771 ** get_address_of_backgroundQueue_5() { return &___backgroundQueue_5; }
	inline void set_backgroundQueue_5(ConcurrentQueue_1_t133829771 * value)
	{
		___backgroundQueue_5 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundQueue_5), value);
	}

	inline static int32_t get_offset_of_forMainThread_6() { return static_cast<int32_t>(offsetof(BackgroundMain_t2789389941, ___forMainThread_6)); }
	inline ConcurrentQueue_1_t133829771 * get_forMainThread_6() const { return ___forMainThread_6; }
	inline ConcurrentQueue_1_t133829771 ** get_address_of_forMainThread_6() { return &___forMainThread_6; }
	inline void set_forMainThread_6(ConcurrentQueue_1_t133829771 * value)
	{
		___forMainThread_6 = value;
		Il2CppCodeGenWriteBarrier((&___forMainThread_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKGROUNDMAIN_T2789389941_H
#ifndef POLYMAININTERNAL_T3329830951_H
#define POLYMAININTERNAL_T3329830951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.PolyMainInternal
struct  PolyMainInternal_t3329830951  : public MonoBehaviour_t3962482529
{
public:
	// System.String PolyToolkitInternal.PolyMainInternal::<apiKey>k__BackingField
	String_t* ___U3CapiKeyU3Ek__BackingField_5;
	// PolyToolkitInternal.client.model.util.WebRequestManager PolyToolkitInternal.PolyMainInternal::<webRequestManager>k__BackingField
	WebRequestManager_t2098015550 * ___U3CwebRequestManagerU3Ek__BackingField_6;
	// PolyToolkitInternal.api_clients.poly_client.PolyClient PolyToolkitInternal.PolyMainInternal::<polyClient>k__BackingField
	PolyClient_t2759672288 * ___U3CpolyClientU3Ek__BackingField_7;
	// System.String PolyToolkitInternal.PolyMainInternal::manuallyProvidedAccessToken
	String_t* ___manuallyProvidedAccessToken_8;
	// PolyToolkitInternal.AsyncImporter PolyToolkitInternal.PolyMainInternal::asyncImporter
	AsyncImporter_t2267690090 * ___asyncImporter_9;
	// PolyToolkitInternal.model.util.BackgroundMain PolyToolkitInternal.PolyMainInternal::backgroundMain
	BackgroundMain_t2789389941 * ___backgroundMain_12;

public:
	inline static int32_t get_offset_of_U3CapiKeyU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PolyMainInternal_t3329830951, ___U3CapiKeyU3Ek__BackingField_5)); }
	inline String_t* get_U3CapiKeyU3Ek__BackingField_5() const { return ___U3CapiKeyU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CapiKeyU3Ek__BackingField_5() { return &___U3CapiKeyU3Ek__BackingField_5; }
	inline void set_U3CapiKeyU3Ek__BackingField_5(String_t* value)
	{
		___U3CapiKeyU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CapiKeyU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CwebRequestManagerU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PolyMainInternal_t3329830951, ___U3CwebRequestManagerU3Ek__BackingField_6)); }
	inline WebRequestManager_t2098015550 * get_U3CwebRequestManagerU3Ek__BackingField_6() const { return ___U3CwebRequestManagerU3Ek__BackingField_6; }
	inline WebRequestManager_t2098015550 ** get_address_of_U3CwebRequestManagerU3Ek__BackingField_6() { return &___U3CwebRequestManagerU3Ek__BackingField_6; }
	inline void set_U3CwebRequestManagerU3Ek__BackingField_6(WebRequestManager_t2098015550 * value)
	{
		___U3CwebRequestManagerU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwebRequestManagerU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CpolyClientU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PolyMainInternal_t3329830951, ___U3CpolyClientU3Ek__BackingField_7)); }
	inline PolyClient_t2759672288 * get_U3CpolyClientU3Ek__BackingField_7() const { return ___U3CpolyClientU3Ek__BackingField_7; }
	inline PolyClient_t2759672288 ** get_address_of_U3CpolyClientU3Ek__BackingField_7() { return &___U3CpolyClientU3Ek__BackingField_7; }
	inline void set_U3CpolyClientU3Ek__BackingField_7(PolyClient_t2759672288 * value)
	{
		___U3CpolyClientU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpolyClientU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_manuallyProvidedAccessToken_8() { return static_cast<int32_t>(offsetof(PolyMainInternal_t3329830951, ___manuallyProvidedAccessToken_8)); }
	inline String_t* get_manuallyProvidedAccessToken_8() const { return ___manuallyProvidedAccessToken_8; }
	inline String_t** get_address_of_manuallyProvidedAccessToken_8() { return &___manuallyProvidedAccessToken_8; }
	inline void set_manuallyProvidedAccessToken_8(String_t* value)
	{
		___manuallyProvidedAccessToken_8 = value;
		Il2CppCodeGenWriteBarrier((&___manuallyProvidedAccessToken_8), value);
	}

	inline static int32_t get_offset_of_asyncImporter_9() { return static_cast<int32_t>(offsetof(PolyMainInternal_t3329830951, ___asyncImporter_9)); }
	inline AsyncImporter_t2267690090 * get_asyncImporter_9() const { return ___asyncImporter_9; }
	inline AsyncImporter_t2267690090 ** get_address_of_asyncImporter_9() { return &___asyncImporter_9; }
	inline void set_asyncImporter_9(AsyncImporter_t2267690090 * value)
	{
		___asyncImporter_9 = value;
		Il2CppCodeGenWriteBarrier((&___asyncImporter_9), value);
	}

	inline static int32_t get_offset_of_backgroundMain_12() { return static_cast<int32_t>(offsetof(PolyMainInternal_t3329830951, ___backgroundMain_12)); }
	inline BackgroundMain_t2789389941 * get_backgroundMain_12() const { return ___backgroundMain_12; }
	inline BackgroundMain_t2789389941 ** get_address_of_backgroundMain_12() { return &___backgroundMain_12; }
	inline void set_backgroundMain_12(BackgroundMain_t2789389941 * value)
	{
		___backgroundMain_12 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundMain_12), value);
	}
};

struct PolyMainInternal_t3329830951_StaticFields
{
public:
	// UnityEngine.GameObject PolyToolkitInternal.PolyMainInternal::polyObject
	GameObject_t1113636619 * ___polyObject_10;
	// PolyToolkitInternal.PolyMainInternal PolyToolkitInternal.PolyMainInternal::instance
	PolyMainInternal_t3329830951 * ___instance_11;

public:
	inline static int32_t get_offset_of_polyObject_10() { return static_cast<int32_t>(offsetof(PolyMainInternal_t3329830951_StaticFields, ___polyObject_10)); }
	inline GameObject_t1113636619 * get_polyObject_10() const { return ___polyObject_10; }
	inline GameObject_t1113636619 ** get_address_of_polyObject_10() { return &___polyObject_10; }
	inline void set_polyObject_10(GameObject_t1113636619 * value)
	{
		___polyObject_10 = value;
		Il2CppCodeGenWriteBarrier((&___polyObject_10), value);
	}

	inline static int32_t get_offset_of_instance_11() { return static_cast<int32_t>(offsetof(PolyMainInternal_t3329830951_StaticFields, ___instance_11)); }
	inline PolyMainInternal_t3329830951 * get_instance_11() const { return ___instance_11; }
	inline PolyMainInternal_t3329830951 ** get_address_of_instance_11() { return &___instance_11; }
	inline void set_instance_11(PolyMainInternal_t3329830951 * value)
	{
		___instance_11 = value;
		Il2CppCodeGenWriteBarrier((&___instance_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYMAININTERNAL_T3329830951_H
#ifndef TESTRUNTIMEIMPORT_T237426821_H
#define TESTRUNTIMEIMPORT_T237426821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestRuntimeImport
struct  TestRuntimeImport_t237426821  : public MonoBehaviour_t3962482529
{
public:
	// PolyToolkit.PolyAuthConfig TestRuntimeImport::authConfig
	PolyAuthConfig_t4147939475  ___authConfig_8;

public:
	inline static int32_t get_offset_of_authConfig_8() { return static_cast<int32_t>(offsetof(TestRuntimeImport_t237426821, ___authConfig_8)); }
	inline PolyAuthConfig_t4147939475  get_authConfig_8() const { return ___authConfig_8; }
	inline PolyAuthConfig_t4147939475 * get_address_of_authConfig_8() { return &___authConfig_8; }
	inline void set_authConfig_8(PolyAuthConfig_t4147939475  value)
	{
		___authConfig_8 = value;
	}
};

struct TestRuntimeImport_t237426821_StaticFields
{
public:
	// PolyToolkit.PolyApi/GetAssetCallback TestRuntimeImport::<>f__am$cache0
	GetAssetCallback_t1146242383 * ___U3CU3Ef__amU24cache0_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(TestRuntimeImport_t237426821_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline GetAssetCallback_t1146242383 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline GetAssetCallback_t1146242383 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(GetAssetCallback_t1146242383 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTRUNTIMEIMPORT_T237426821_H
#ifndef POLYTOOLKITMANAGER_T3701942652_H
#define POLYTOOLKITMANAGER_T3701942652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyToolkitManager
struct  PolyToolkitManager_t3701942652  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYTOOLKITMANAGER_T3701942652_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3600 = { sizeof (GltfImageBase_t4142612163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3600[2] = 
{
	GltfImageBase_t4142612163::get_offset_of_uri_0(),
	GltfImageBase_t4142612163::get_offset_of_data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3601 = { sizeof (GltfTextureBase_t3199118737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3601[1] = 
{
	GltfTextureBase_t3199118737::get_offset_of_unityTexture_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3602 = { sizeof (GltfMaterialBase_t3607200052), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3602[1] = 
{
	GltfMaterialBase_t3607200052::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3603 = { sizeof (GltfMeshBase_t3664755873), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3603[1] = 
{
	GltfMeshBase_t3664755873::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3604 = { sizeof (GltfNodeBase_t3005766873), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3604[2] = 
{
	GltfNodeBase_t3005766873::get_offset_of_name_0(),
	GltfNodeBase_t3005766873::get_offset_of_matrix_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3605 = { sizeof (GltfSceneBase_t3309998329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3605[1] = 
{
	GltfSceneBase_t3309998329::get_offset_of_extras_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3606 = { sizeof (HelpTextAttribute_t979818096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3606[1] = 
{
	HelpTextAttribute_t979818096::get_offset_of_text_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3607 = { sizeof (ImportGltf_t3245133338), -1, sizeof(ImportGltf_t3245133338_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3607[10] = 
{
	0,
	0,
	ImportGltf_t3245133338_StaticFields::get_offset_of_kSerializer_2(),
	0,
	0,
	ImportGltf_t3245133338_StaticFields::get_offset_of_U3CGltfFromUnityU3Ek__BackingField_5(),
	ImportGltf_t3245133338_StaticFields::get_offset_of_U3CUnityFromGltfU3Ek__BackingField_6(),
	ImportGltf_t3245133338_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
	ImportGltf_t3245133338_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_8(),
	ImportGltf_t3245133338_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3608 = { sizeof (GltfImportResult_t3877923054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3608[4] = 
{
	GltfImportResult_t3877923054::get_offset_of_root_0(),
	GltfImportResult_t3877923054::get_offset_of_meshes_1(),
	GltfImportResult_t3877923054::get_offset_of_materials_2(),
	GltfImportResult_t3877923054::get_offset_of_textures_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3609 = { sizeof (ImportState_t482765084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3609[5] = 
{
	ImportState_t482765084::get_offset_of_root_0(),
	ImportState_t482765084::get_offset_of_desiredScene_1(),
	ImportState_t482765084::get_offset_of_scaleFactor_2(),
	ImportState_t482765084::get_offset_of_nodeScaleFactor_3(),
	ImportState_t482765084::get_offset_of_translation_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3610 = { sizeof (MeshSubset_t1957016349)+ sizeof (RuntimeObject), sizeof(MeshSubset_t1957016349 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3610[2] = 
{
	MeshSubset_t1957016349::get_offset_of_vertices_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshSubset_t1957016349::get_offset_of_triangles_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3611 = { sizeof (ColorSpace_t1646841580)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3611[4] = 
{
	ColorSpace_t1646841580::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3612 = { sizeof (U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3612[19] = 
{
	U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381::get_offset_of_U3CloadedU3E__0_0(),
	U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381::get_offset_of_state_1(),
	U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381::get_offset_of_uriLoader_2(),
	U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381::get_offset_of_U24locvar0_3(),
	U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381::get_offset_of_U3CunusedU3E__1_4(),
	U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381::get_offset_of_U24locvar1_5(),
	U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381::get_offset_of_result_6(),
	U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381::get_offset_of_U3CmatConverterU3E__0_7(),
	U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381::get_offset_of_U3CrootTransformU3E__0_8(),
	U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381::get_offset_of_U24locvar2_9(),
	U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381::get_offset_of_U3CnodeU3E__2_10(),
	U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381::get_offset_of_U24locvar3_11(),
	U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381::get_offset_of_U3CunusedU3E__3_12(),
	U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381::get_offset_of_U24locvar4_13(),
	U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381::get_offset_of_U24locvar5_14(),
	U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381::get_offset_of_U24locvar6_15(),
	U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381::get_offset_of_U24current_16(),
	U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381::get_offset_of_U24disposing_17(),
	U3CCreateGameObjectsFromNodesU3Ec__Iterator0_t3449319381::get_offset_of_U24PC_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3613 = { sizeof (U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3613[18] = 
{
	U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091::get_offset_of_node_0(),
	U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091::get_offset_of_U3CobjU3E__0_1(),
	U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091::get_offset_of_parent_2(),
	U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091::get_offset_of_state_3(),
	U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091::get_offset_of_translationToApply_4(),
	U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091::get_offset_of_result_5(),
	U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091::get_offset_of_matConverter_6(),
	U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091::get_offset_of_U24locvar0_7(),
	U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091::get_offset_of_U3CunusedU3E__1_8(),
	U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091::get_offset_of_U24locvar1_9(),
	U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091::get_offset_of_U24locvar2_10(),
	U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091::get_offset_of_U3CchildNodeU3E__2_11(),
	U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091::get_offset_of_U24locvar3_12(),
	U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091::get_offset_of_U3CunusedU3E__3_13(),
	U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091::get_offset_of_U24locvar4_14(),
	U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091::get_offset_of_U24current_15(),
	U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091::get_offset_of_U24disposing_16(),
	U3CCreateGameObjectsFromNodeU3Ec__Iterator1_t2009757091::get_offset_of_U24PC_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3614 = { sizeof (U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3614[16] = 
{
	U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538::get_offset_of_U3CiPU3E__1_0(),
	U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538::get_offset_of_mesh_1(),
	U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538::get_offset_of_U3CprimU3E__2_2(),
	U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538::get_offset_of_matConverter_3(),
	U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538::get_offset_of_U3CunityMaterialU3E__2_4(),
	U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538::get_offset_of_U3CprimNameU3E__2_5(),
	U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538::get_offset_of_U3CiMU3E__3_6(),
	U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538::get_offset_of_U3CmeshNameU3E__4_7(),
	U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538::get_offset_of_U3CumeshU3E__4_8(),
	U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538::get_offset_of_result_9(),
	U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538::get_offset_of_U24locvar0_10(),
	U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538::get_offset_of_allowMeshInParent_11(),
	U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538::get_offset_of_parent_12(),
	U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538::get_offset_of_U24current_13(),
	U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538::get_offset_of_U24disposing_14(),
	U3CCreateGameObjectsFromMeshU3Ec__Iterator2_t4216039538::get_offset_of_U24PC_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3615 = { sizeof (U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3615[13] = 
{
	U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942::get_offset_of_numVerts_0(),
	U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942::get_offset_of_maxSubsetVerts_1(),
	U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942::get_offset_of_triangles_2(),
	U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942::get_offset_of_U3CcountU3E__0_3(),
	U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942::get_offset_of_U3CvertsUsedU3E__0_4(),
	U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942::get_offset_of_U3CtrisUsedU3E__0_5(),
	U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942::get_offset_of_U3CiTriU3E__1_6(),
	U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942::get_offset_of_U3CtriVertsU3E__2_7(),
	U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942::get_offset_of_U3CnewVertsUsedU3E__3_8(),
	U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942::get_offset_of_U3CnewTrisUsedU3E__3_9(),
	U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942::get_offset_of_U24current_10(),
	U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942::get_offset_of_U24disposing_11(),
	U3CGenerateMeshSubsetsU3Ec__Iterator3_t1345351942::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3616 = { sizeof (IntRange_t4091832750)+ sizeof (RuntimeObject), sizeof(IntRange_t4091832750 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3616[2] = 
{
	IntRange_t4091832750::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IntRange_t4091832750::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3617 = { sizeof (RawImage_t3303103980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3617[4] = 
{
	RawImage_t3303103980::get_offset_of_colorData_0(),
	RawImage_t3303103980::get_offset_of_colorWidth_1(),
	RawImage_t3303103980::get_offset_of_colorHeight_2(),
	RawImage_t3303103980::get_offset_of_format_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3618 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3619 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3620 = { sizeof (Reader_t3716605413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3620[1] = 
{
	Reader_t3716605413::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3621 = { sizeof (BufferedStreamLoader_t3545088954), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3621[2] = 
{
	BufferedStreamLoader_t3545088954::get_offset_of_uriBase_0(),
	BufferedStreamLoader_t3545088954::get_offset_of_bufferSize_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3622 = { sizeof (HashedPathBufferedStreamLoader_t106783494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3622[2] = 
{
	HashedPathBufferedStreamLoader_t106783494::get_offset_of_uriBase_0(),
	HashedPathBufferedStreamLoader_t106783494::get_offset_of_bufferSize_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3623 = { sizeof (BufferedStreamReader_t3800439956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3623[3] = 
{
	BufferedStreamReader_t3800439956::get_offset_of_stream_0(),
	BufferedStreamReader_t3800439956::get_offset_of_tempBuffer_1(),
	BufferedStreamReader_t3800439956::get_offset_of_contentLength_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3624 = { sizeof (FullyBufferedLoader_t4158986460), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3624[1] = 
{
	FullyBufferedLoader_t4158986460::get_offset_of_uriBase_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3625 = { sizeof (FormatLoader_t2400397192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3625[1] = 
{
	FormatLoader_t2400397192::get_offset_of_format_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3626 = { sizeof (U3CLoadU3Ec__AnonStorey0_t4017912820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3626[1] = 
{
	U3CLoadU3Ec__AnonStorey0_t4017912820::get_offset_of_uri_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3627 = { sizeof (MathUtils_t3934572377), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3628 = { sizeof (LengthUnit_t272761749)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3628[7] = 
{
	LengthUnit_t272761749::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3629 = { sizeof (LengthWithUnit_t3846227886)+ sizeof (RuntimeObject), sizeof(LengthWithUnit_t3846227886 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3629[2] = 
{
	LengthWithUnit_t3846227886::get_offset_of_amount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LengthWithUnit_t3846227886::get_offset_of_unit_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3630 = { sizeof (MeasurementUnits_t3638319501), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3631 = { sizeof (FormatSaveData_t3321373558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3631[4] = 
{
	FormatSaveData_t3321373558::get_offset_of_root_0(),
	FormatSaveData_t3321373558::get_offset_of_resources_1(),
	FormatSaveData_t3321373558::get_offset_of_zippedFiles_2(),
	FormatSaveData_t3321373558::get_offset_of_triangleCount_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3632 = { sizeof (FormatDataFile_t3534188782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3632[5] = 
{
	FormatDataFile_t3534188782::get_offset_of_fileName_0(),
	FormatDataFile_t3534188782::get_offset_of_mimeType_1(),
	FormatDataFile_t3534188782::get_offset_of_bytes_2(),
	FormatDataFile_t3534188782::get_offset_of_tag_3(),
	FormatDataFile_t3534188782::get_offset_of_multipartBytes_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3633 = { sizeof (SaveData_t3667871004)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3633[8] = 
{
	SaveData_t3667871004::get_offset_of_filenameBase_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SaveData_t3667871004::get_offset_of_objFile_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SaveData_t3667871004::get_offset_of_mtlFile_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SaveData_t3667871004::get_offset_of_GLTFfiles_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SaveData_t3667871004::get_offset_of_objMtlZip_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SaveData_t3667871004::get_offset_of_polyFile_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SaveData_t3667871004::get_offset_of_polyZip_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SaveData_t3667871004::get_offset_of_thumbnailFile_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3634 = { sizeof (ExportUtils_t2410614856), -1, sizeof(ExportUtils_t2410614856_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3634[8] = 
{
	ExportUtils_t2410614856_StaticFields::get_offset_of_OBJ_FILENAME_0(),
	ExportUtils_t2410614856_StaticFields::get_offset_of_MTL_FILENAME_1(),
	ExportUtils_t2410614856_StaticFields::get_offset_of_THUMBNAIL_FILENAME_2(),
	ExportUtils_t2410614856_StaticFields::get_offset_of_POLY_FILENAME_3(),
	ExportUtils_t2410614856_StaticFields::get_offset_of_OBJ_MTL_ZIP_FILENAME_4(),
	ExportUtils_t2410614856_StaticFields::get_offset_of_GLTF_FILENAME_5(),
	ExportUtils_t2410614856_StaticFields::get_offset_of_GLTF_BIN_FILENAME_6(),
	ExportUtils_t2410614856_StaticFields::get_offset_of_POLY_ZIP_FILENAME_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3635 = { sizeof (BackgroundMain_t2789389941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3635[5] = 
{
	BackgroundMain_t2789389941::get_offset_of_backgroundThread_2(),
	BackgroundMain_t2789389941::get_offset_of_running_3(),
	BackgroundMain_t2789389941::get_offset_of_setupDone_4(),
	BackgroundMain_t2789389941::get_offset_of_backgroundQueue_5(),
	BackgroundMain_t2789389941::get_offset_of_forMainThread_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3636 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3637 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3637[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3638 = { sizeof (WebRequestManager_t2098015550), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3638[8] = 
{
	0,
	0,
	0,
	0,
	0,
	WebRequestManager_t2098015550::get_offset_of_pendingRequests_7(),
	WebRequestManager_t2098015550::get_offset_of_idleBuffers_8(),
	WebRequestManager_t2098015550::get_offset_of_cache_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3639 = { sizeof (CreationCallback_t949890767), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3640 = { sizeof (CompletionCallback_t3367319633), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3641 = { sizeof (PendingRequest_t652293891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3641[3] = 
{
	PendingRequest_t652293891::get_offset_of_creationCallback_0(),
	PendingRequest_t652293891::get_offset_of_completionCallback_1(),
	PendingRequest_t652293891::get_offset_of_maxAgeMillis_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3642 = { sizeof (BufferHolder_t424688828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3642[2] = 
{
	BufferHolder_t424688828::get_offset_of_tempBuffer_0(),
	BufferHolder_t424688828::get_offset_of_dataBuffer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3643 = { sizeof (U3CHandleWebRequestU3Ec__Iterator0_t3773947286), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3643[11] = 
{
	U3CHandleWebRequestU3Ec__Iterator0_t3773947286::get_offset_of_request_0(),
	U3CHandleWebRequestU3Ec__Iterator0_t3773947286::get_offset_of_U3CwebRequestU3E__0_1(),
	U3CHandleWebRequestU3Ec__Iterator0_t3773947286::get_offset_of_U3CcacheAllowedU3E__0_2(),
	U3CHandleWebRequestU3Ec__Iterator0_t3773947286::get_offset_of_bufferHolder_3(),
	U3CHandleWebRequestU3Ec__Iterator0_t3773947286::get_offset_of_U3ChandlerU3E__0_4(),
	U3CHandleWebRequestU3Ec__Iterator0_t3773947286::get_offset_of_U3CstatusU3E__0_5(),
	U3CHandleWebRequestU3Ec__Iterator0_t3773947286::get_offset_of_U24this_6(),
	U3CHandleWebRequestU3Ec__Iterator0_t3773947286::get_offset_of_U24current_7(),
	U3CHandleWebRequestU3Ec__Iterator0_t3773947286::get_offset_of_U24disposing_8(),
	U3CHandleWebRequestU3Ec__Iterator0_t3773947286::get_offset_of_U24PC_9(),
	U3CHandleWebRequestU3Ec__Iterator0_t3773947286::get_offset_of_U24locvar0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3644 = { sizeof (U3CHandleWebRequestU3Ec__AnonStorey1_t2411719362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3644[4] = 
{
	U3CHandleWebRequestU3Ec__AnonStorey1_t2411719362::get_offset_of_cacheHit_0(),
	U3CHandleWebRequestU3Ec__AnonStorey1_t2411719362::get_offset_of_cacheData_1(),
	U3CHandleWebRequestU3Ec__AnonStorey1_t2411719362::get_offset_of_cacheReadDone_2(),
	U3CHandleWebRequestU3Ec__AnonStorey1_t2411719362::get_offset_of_U3CU3Ef__refU240_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3645 = { sizeof (PolyInternalUtils_t2048710808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3645[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3646 = { sizeof (PolyMainInternal_t3329830951), -1, sizeof(PolyMainInternal_t3329830951_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3646[11] = 
{
	0,
	0,
	0,
	PolyMainInternal_t3329830951::get_offset_of_U3CapiKeyU3Ek__BackingField_5(),
	PolyMainInternal_t3329830951::get_offset_of_U3CwebRequestManagerU3Ek__BackingField_6(),
	PolyMainInternal_t3329830951::get_offset_of_U3CpolyClientU3Ek__BackingField_7(),
	PolyMainInternal_t3329830951::get_offset_of_manuallyProvidedAccessToken_8(),
	PolyMainInternal_t3329830951::get_offset_of_asyncImporter_9(),
	PolyMainInternal_t3329830951_StaticFields::get_offset_of_polyObject_10(),
	PolyMainInternal_t3329830951_StaticFields::get_offset_of_instance_11(),
	PolyMainInternal_t3329830951::get_offset_of_backgroundMain_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3647 = { sizeof (FetchProgressCallback_t1972555609), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3648 = { sizeof (FetchOperationState_t1520449150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3648[6] = 
{
	FetchOperationState_t1520449150::get_offset_of_totalFiles_0(),
	FetchOperationState_t1520449150::get_offset_of_pendingFiles_1(),
	FetchOperationState_t1520449150::get_offset_of_asset_2(),
	FetchOperationState_t1520449150::get_offset_of_packageBeingFetched_3(),
	FetchOperationState_t1520449150::get_offset_of_completionCallback_4(),
	FetchOperationState_t1520449150::get_offset_of_progressCallback_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3649 = { sizeof (U3CListAssetsU3Ec__AnonStorey0_t1932574489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3649[2] = 
{
	U3CListAssetsU3Ec__AnonStorey0_t1932574489::get_offset_of_callback_0(),
	U3CListAssetsU3Ec__AnonStorey0_t1932574489::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3650 = { sizeof (U3CListUserAssetsU3Ec__AnonStorey1_t4120733243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3650[2] = 
{
	U3CListUserAssetsU3Ec__AnonStorey1_t4120733243::get_offset_of_callback_0(),
	U3CListUserAssetsU3Ec__AnonStorey1_t4120733243::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3651 = { sizeof (U3CListLikedAssetsU3Ec__AnonStorey2_t1445844308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3651[2] = 
{
	U3CListLikedAssetsU3Ec__AnonStorey2_t1445844308::get_offset_of_callback_0(),
	U3CListLikedAssetsU3Ec__AnonStorey2_t1445844308::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3652 = { sizeof (U3CGetAssetU3Ec__AnonStorey3_t1917775743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3652[2] = 
{
	U3CGetAssetU3Ec__AnonStorey3_t1917775743::get_offset_of_callback_0(),
	U3CGetAssetU3Ec__AnonStorey3_t1917775743::get_offset_of_id_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3653 = { sizeof (U3CFetchAndImportFormatU3Ec__AnonStorey4_t209984363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3653[5] = 
{
	U3CFetchAndImportFormatU3Ec__AnonStorey4_t209984363::get_offset_of_format_0(),
	U3CFetchAndImportFormatU3Ec__AnonStorey4_t209984363::get_offset_of_asset_1(),
	U3CFetchAndImportFormatU3Ec__AnonStorey4_t209984363::get_offset_of_options_2(),
	U3CFetchAndImportFormatU3Ec__AnonStorey4_t209984363::get_offset_of_callback_3(),
	U3CFetchAndImportFormatU3Ec__AnonStorey4_t209984363::get_offset_of_U24this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3654 = { sizeof (U3CImportFormatU3Ec__AnonStorey5_t99629973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3654[2] = 
{
	U3CImportFormatU3Ec__AnonStorey5_t99629973::get_offset_of_callback_0(),
	U3CImportFormatU3Ec__AnonStorey5_t99629973::get_offset_of_asset_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3655 = { sizeof (U3CFetchFormatFilesU3Ec__AnonStorey6_t1330540246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3655[2] = 
{
	U3CFetchFormatFilesU3Ec__AnonStorey6_t1330540246::get_offset_of_state_0(),
	U3CFetchFormatFilesU3Ec__AnonStorey6_t1330540246::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3656 = { sizeof (U3CFetchFormatFilesU3Ec__AnonStorey7_t1330540247), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3656[2] = 
{
	U3CFetchFormatFilesU3Ec__AnonStorey7_t1330540247::get_offset_of_thisIndex_0(),
	U3CFetchFormatFilesU3Ec__AnonStorey7_t1330540247::get_offset_of_U3CU3Ef__refU246_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3657 = { sizeof (PolyUtils_t2128247007), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3658 = { sizeof (PtDebug_t199111972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3658[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3659 = { sizeof (PtSettings_t265752063), -1, sizeof(PtSettings_t265752063_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3659[19] = 
{
	PtSettings_t265752063_StaticFields::get_offset_of_instance_2(),
	PtSettings_t265752063::get_offset_of_playerColorSpace_3(),
	PtSettings_t265752063::get_offset_of_surfaceShaderMaterials_4(),
	PtSettings_t265752063::get_offset_of_assetObjectsPath_5(),
	PtSettings_t265752063::get_offset_of_assetSourcesPath_6(),
	PtSettings_t265752063::get_offset_of_resourcesPath_7(),
	PtSettings_t265752063::get_offset_of_sceneUnit_8(),
	PtSettings_t265752063::get_offset_of_defaultImportOptions_9(),
	PtSettings_t265752063::get_offset_of_brushManifest_10(),
	PtSettings_t265752063::get_offset_of_basePbrOpaqueDoubleSidedMaterial_11(),
	PtSettings_t265752063::get_offset_of_basePbrBlendDoubleSidedMaterial_12(),
	PtSettings_t265752063::get_offset_of_authConfig_13(),
	PtSettings_t265752063::get_offset_of_cacheConfig_14(),
	PtSettings_t265752063::get_offset_of_sendEditorAnalytics_15(),
	PtSettings_t265752063::get_offset_of_warnOnSourceOverwrite_16(),
	PtSettings_t265752063::get_offset_of_warnOfApiCompatibility_17(),
	PtSettings_t265752063::get_offset_of_surfaceShaderMaterialLookup_18(),
	PtSettings_t265752063_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_19(),
	PtSettings_t265752063_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3660 = { sizeof (SurfaceShaderMaterial_t2588431549)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3660[3] = 
{
	SurfaceShaderMaterial_t2588431549::get_offset_of_shaderUrl_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SurfaceShaderMaterial_t2588431549::get_offset_of_material_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SurfaceShaderMaterial_t2588431549::get_offset_of_descriptor_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3661 = { sizeof (BrushDescriptor_t2792727521), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3661[11] = 
{
	BrushDescriptor_t2792727521::get_offset_of_m_Guid_2(),
	BrushDescriptor_t2792727521::get_offset_of_m_DurableName_3(),
	BrushDescriptor_t2792727521::get_offset_of_m_Material_4(),
	BrushDescriptor_t2792727521::get_offset_of_m_IsParticle_5(),
	BrushDescriptor_t2792727521::get_offset_of_m_uv0Size_6(),
	BrushDescriptor_t2792727521::get_offset_of_m_uv0Semantic_7(),
	BrushDescriptor_t2792727521::get_offset_of_m_uv1Size_8(),
	BrushDescriptor_t2792727521::get_offset_of_m_uv1Semantic_9(),
	BrushDescriptor_t2792727521::get_offset_of_m_bUseNormals_10(),
	BrushDescriptor_t2792727521::get_offset_of_m_normalSemantic_11(),
	BrushDescriptor_t2792727521::get_offset_of_m_bFbxExportNormalAsTexcoord1_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3662 = { sizeof (Semantic_t602446212)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3662[7] = 
{
	Semantic_t602446212::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3663 = { sizeof (BrushManifest_t1931127838), -1, sizeof(BrushManifest_t1931127838_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3663[7] = 
{
	BrushManifest_t1931127838_StaticFields::get_offset_of_sm_Instance_2(),
	BrushManifest_t1931127838::get_offset_of_m_DefaultManifest_3(),
	BrushManifest_t1931127838::get_offset_of_m_Brushes_4(),
	BrushManifest_t1931127838::get_offset_of_m_ByGuid_5(),
	BrushManifest_t1931127838::get_offset_of_m_ByName_6(),
	BrushManifest_t1931127838_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
	BrushManifest_t1931127838_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3664 = { sizeof (SerializableGuid_t3370039383)+ sizeof (RuntimeObject), sizeof(SerializableGuid_t3370039383_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3664[1] = 
{
	SerializableGuid_t3370039383::get_offset_of_m_storage_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3665 = { sizeof (TestRuntimeImport_t237426821), -1, sizeof(TestRuntimeImport_t237426821_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3665[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	TestRuntimeImport_t237426821::get_offset_of_authConfig_8(),
	TestRuntimeImport_t237426821_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3666 = { sizeof (ThumbnailFetcher_t4028205215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3666[3] = 
{
	0,
	ThumbnailFetcher_t4028205215::get_offset_of_asset_1(),
	ThumbnailFetcher_t4028205215::get_offset_of_callback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3667 = { sizeof (UnityCompat_t3661118369), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3668 = { sizeof (Version_t724142152)+ sizeof (RuntimeObject), sizeof(Version_t724142152 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3668[2] = 
{
	Version_t724142152::get_offset_of_major_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Version_t724142152::get_offset_of_minor_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3669 = { sizeof (PolyApi_t923893618), -1, sizeof(PolyApi_t923893618_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3669[2] = 
{
	PolyApi_t923893618_StaticFields::get_offset_of_initialized_0(),
	PolyApi_t923893618_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3670 = { sizeof (ListAssetsCallback_t3881593442), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3671 = { sizeof (GetAssetCallback_t1146242383), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3672 = { sizeof (FetchFormatFilesCallback_t2377205724), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3673 = { sizeof (ImportCallback_t3513750376), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3674 = { sizeof (FetchThumbnailCallback_t3242648953), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3675 = { sizeof (PolyAuthConfig_t4147939475)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3675[5] = 
{
	PolyAuthConfig_t4147939475::get_offset_of_apiKey_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PolyAuthConfig_t4147939475::get_offset_of_clientId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PolyAuthConfig_t4147939475::get_offset_of_clientSecret_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PolyAuthConfig_t4147939475::get_offset_of_additionalScopes_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PolyAuthConfig_t4147939475::get_offset_of_serviceName_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3676 = { sizeof (PolyCacheConfig_t1397553534)+ sizeof (RuntimeObject), sizeof(PolyCacheConfig_t1397553534_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3676[4] = 
{
	PolyCacheConfig_t1397553534::get_offset_of_cacheEnabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PolyCacheConfig_t1397553534::get_offset_of_maxCacheSizeMb_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PolyCacheConfig_t1397553534::get_offset_of_maxCacheEntries_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PolyCacheConfig_t1397553534::get_offset_of_cachePathOverride_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3677 = { sizeof (PolyImportOptions_t4213423452)+ sizeof (RuntimeObject), sizeof(PolyImportOptions_t4213423452_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3677[5] = 
{
	PolyImportOptions_t4213423452::get_offset_of_rescalingMode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PolyImportOptions_t4213423452::get_offset_of_scaleFactor_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PolyImportOptions_t4213423452::get_offset_of_desiredSize_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PolyImportOptions_t4213423452::get_offset_of_recenter_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PolyImportOptions_t4213423452::get_offset_of_clientThrottledMainThread_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3678 = { sizeof (RescalingMode_t410920913)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3678[3] = 
{
	RescalingMode_t410920913::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3679 = { sizeof (PolyToolkitManager_t3701942652), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3680 = { sizeof (PolyAsset_t1814153511), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3680[13] = 
{
	0,
	PolyAsset_t1814153511::get_offset_of_name_1(),
	PolyAsset_t1814153511::get_offset_of_displayName_2(),
	PolyAsset_t1814153511::get_offset_of_authorName_3(),
	PolyAsset_t1814153511::get_offset_of_description_4(),
	PolyAsset_t1814153511::get_offset_of_createTime_5(),
	PolyAsset_t1814153511::get_offset_of_updateTime_6(),
	PolyAsset_t1814153511::get_offset_of_formats_7(),
	PolyAsset_t1814153511::get_offset_of_thumbnail_8(),
	PolyAsset_t1814153511::get_offset_of_license_9(),
	PolyAsset_t1814153511::get_offset_of_visibility_10(),
	PolyAsset_t1814153511::get_offset_of_isCurated_11(),
	PolyAsset_t1814153511::get_offset_of_thumbnailTexture_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3681 = { sizeof (PolyFormat_t1880249796), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3681[4] = 
{
	PolyFormat_t1880249796::get_offset_of_formatType_0(),
	PolyFormat_t1880249796::get_offset_of_root_1(),
	PolyFormat_t1880249796::get_offset_of_resources_2(),
	PolyFormat_t1880249796::get_offset_of_formatComplexity_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3682 = { sizeof (PolyFile_t4211357808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3682[5] = 
{
	PolyFile_t4211357808::get_offset_of_relativePath_0(),
	PolyFile_t4211357808::get_offset_of_url_1(),
	PolyFile_t4211357808::get_offset_of_contentType_2(),
	PolyFile_t4211357808::get_offset_of_contents_3(),
	PolyFile_t4211357808::get_offset_of_text_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3683 = { sizeof (PolyFormatComplexity_t3101848153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3683[2] = 
{
	PolyFormatComplexity_t3101848153::get_offset_of_triangleCount_0(),
	PolyFormatComplexity_t3101848153::get_offset_of_lodHint_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3684 = { sizeof (PolyFormatType_t1021068395)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3684[6] = 
{
	PolyFormatType_t1021068395::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3685 = { sizeof (PolyAssetLicense_t558986990)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3685[4] = 
{
	PolyAssetLicense_t558986990::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3686 = { sizeof (PolyVisibilityFilter_t769998791)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3686[4] = 
{
	PolyVisibilityFilter_t769998791::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3687 = { sizeof (PolyVisibility_t305223869)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3687[5] = 
{
	PolyVisibility_t305223869::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3688 = { sizeof (PolyCategory_t3361242767)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3688[12] = 
{
	PolyCategory_t3361242767::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3689 = { sizeof (PolyOrderBy_t1813925015)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3689[5] = 
{
	PolyOrderBy_t1813925015::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3690 = { sizeof (PolyFormatFilter_t1920599779)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3690[7] = 
{
	PolyFormatFilter_t1920599779::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3691 = { sizeof (PolyMaxComplexityFilter_t2510204215)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3691[5] = 
{
	PolyMaxComplexityFilter_t2510204215::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3692 = { sizeof (PolyRequest_t2374976743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3692[3] = 
{
	PolyRequest_t2374976743::get_offset_of_orderBy_0(),
	PolyRequest_t2374976743::get_offset_of_pageSize_1(),
	PolyRequest_t2374976743::get_offset_of_pageToken_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3693 = { sizeof (PolyListAssetsRequest_t3768481913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3693[5] = 
{
	PolyListAssetsRequest_t3768481913::get_offset_of_keywords_3(),
	PolyListAssetsRequest_t3768481913::get_offset_of_curated_4(),
	PolyListAssetsRequest_t3768481913::get_offset_of_category_5(),
	PolyListAssetsRequest_t3768481913::get_offset_of_maxComplexity_6(),
	PolyListAssetsRequest_t3768481913::get_offset_of_formatFilter_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3694 = { sizeof (PolyListUserAssetsRequest_t1125477019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3694[3] = 
{
	PolyListUserAssetsRequest_t1125477019::get_offset_of_format_3(),
	PolyListUserAssetsRequest_t1125477019::get_offset_of_visibility_4(),
	PolyListUserAssetsRequest_t1125477019::get_offset_of_formatFilter_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3695 = { sizeof (PolyListLikedAssetsRequest_t4202721371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3695[1] = 
{
	PolyListLikedAssetsRequest_t4202721371::get_offset_of_name_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3696 = { sizeof (PolyStatus_t3145373940)+ sizeof (RuntimeObject), sizeof(PolyStatus_t3145373940_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3696[2] = 
{
	PolyStatus_t3145373940::get_offset_of_ok_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PolyStatus_t3145373940::get_offset_of_errorMessage_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3697 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3697[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3698 = { sizeof (PolyBaseResult_t2195255688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3698[1] = 
{
	PolyBaseResult_t2195255688::get_offset_of_status_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3699 = { sizeof (PolyListAssetsResult_t1050868053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3699[3] = 
{
	PolyListAssetsResult_t1050868053::get_offset_of_assets_1(),
	PolyListAssetsResult_t1050868053::get_offset_of_totalSize_2(),
	PolyListAssetsResult_t1050868053::get_offset_of_nextPageToken_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
