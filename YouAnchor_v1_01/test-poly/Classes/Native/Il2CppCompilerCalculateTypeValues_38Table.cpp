﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BuildingAltitudePicking
struct BuildingAltitudePicking_t1082500057;
// System.String
struct String_t;
// Wrld.MapCamera.CameraApi/TransitionStartHandler
struct TransitionStartHandler_t1803894657;
// Wrld.MapCamera.CameraApi/TransitionEndHandler
struct TransitionEndHandler_t4082272293;
// UnityEngine.Camera
struct Camera_t4157153871;
// Wrld.ApiImplementation
struct ApiImplementation_t1854519848;
// Wrld.MapCamera.CameraInputHandler
struct CameraInputHandler_t3237370388;
// SelectingBuildings
struct SelectingBuildings_t1914154491;
// Wrld.Resources.Buildings.Highlight
struct Highlight_t3200169708;
// HighlightingBuildings
struct HighlightingBuildings_t423429914;
// ClearingBuildingHighlights
struct ClearingBuildingHighlights_t199549764;
// System.Double[]
struct DoubleU5BU5D_t3413330114;
// PositionObjectAtLatitudeAndLongitude
struct PositionObjectAtLatitudeAndLongitude_t3453145834;
// RotateObjectOnMap
struct RotateObjectOnMap_t115929985;
// Wrld.Concurrency.ThreadService/ThreadStartDelegate
struct ThreadStartDelegate_t2173555516;
// Wrld.MapInput.IUnityInputHandler
struct IUnityInputHandler_t448527725;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.Thread>
struct Dictionary_2_t1189549400;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Touch[]
struct TouchU5BU5D_t1849554061;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// MoveObjectOnMap
struct MoveObjectOnMap_t383061840;
// PositionObjectAtECEFCoordinate
struct PositionObjectAtECEFCoordinate_t1179719296;
// UnityEngine.XR.iOS.UnityARDirectionalLightEstimate
struct UnityARDirectionalLightEstimate_t2924556994;
// Wrld.MapInput.IUnityInputProcessor
struct IUnityInputProcessor_t4178120605;
// System.Func`2<UnityEngine.Touch,System.Boolean>
struct Func_2_t2764894207;
// System.Func`3<UnityEngine.Touch,System.Int32,Wrld.MapInput.Touch.TouchInputPointerEvent>
struct Func_3_t904424527;
// Wrld.NativePluginRunner
struct NativePluginRunner_t3528041536;
// Wrld.MapCamera.CameraApi
struct CameraApi_t3006904385;
// Wrld.Resources.Buildings.BuildingsApi
struct BuildingsApi_t3854622187;
// Wrld.Space.GeographicApi
struct GeographicApi_t2934948604;
// Wrld.Space.UnityWorldSpaceCoordinateFrame
struct UnityWorldSpaceCoordinateFrame_t1382732960;
// Wrld.MapCamera.InterestPointProvider
struct InterestPointProvider_t3426197040;
// Wrld.Streaming.GameObjectStreamer
struct GameObjectStreamer_t3452608707;
// Wrld.MapGameObjectScene
struct MapGameObjectScene_t128928738;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.XR.iOS.ARFaceAnchor
struct ARFaceAnchor_t1844206636;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// Wrld.Space.GeographicTransform
struct GeographicTransform_t2262143282;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Dropdown
struct Dropdown_t2274391225;
// ExampleChanger/Example[]
struct ExampleU5BU5D_t2836667186;
// Wrld.Api
struct Api_t1190036922;

struct Touch_t1921856868 ;
struct Vector3_t3722313464 ;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CEXAMPLEU3EC__ITERATOR0_T1959257276_H
#define U3CEXAMPLEU3EC__ITERATOR0_T1959257276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingAltitudePicking/<Example>c__Iterator0
struct  U3CExampleU3Ec__Iterator0_t1959257276  : public RuntimeObject
{
public:
	// BuildingAltitudePicking BuildingAltitudePicking/<Example>c__Iterator0::$this
	BuildingAltitudePicking_t1082500057 * ___U24this_0;
	// System.Object BuildingAltitudePicking/<Example>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean BuildingAltitudePicking/<Example>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 BuildingAltitudePicking/<Example>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t1959257276, ___U24this_0)); }
	inline BuildingAltitudePicking_t1082500057 * get_U24this_0() const { return ___U24this_0; }
	inline BuildingAltitudePicking_t1082500057 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(BuildingAltitudePicking_t1082500057 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t1959257276, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t1959257276, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t1959257276, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXAMPLEU3EC__ITERATOR0_T1959257276_H
#ifndef CAMERAAPI_T3006904385_H
#define CAMERAAPI_T3006904385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapCamera.CameraApi
struct  CameraApi_t3006904385  : public RuntimeObject
{
public:
	// Wrld.MapCamera.CameraApi/TransitionStartHandler Wrld.MapCamera.CameraApi::OnTransitionStart
	TransitionStartHandler_t1803894657 * ___OnTransitionStart_1;
	// Wrld.MapCamera.CameraApi/TransitionEndHandler Wrld.MapCamera.CameraApi::OnTransitionEnd
	TransitionEndHandler_t4082272293 * ___OnTransitionEnd_2;
	// System.Boolean Wrld.MapCamera.CameraApi::<IsTransitioning>k__BackingField
	bool ___U3CIsTransitioningU3Ek__BackingField_3;
	// UnityEngine.Camera Wrld.MapCamera.CameraApi::m_controlledCamera
	Camera_t4157153871 * ___m_controlledCamera_4;
	// Wrld.ApiImplementation Wrld.MapCamera.CameraApi::m_apiImplementation
	ApiImplementation_t1854519848 * ___m_apiImplementation_5;
	// Wrld.MapCamera.CameraInputHandler Wrld.MapCamera.CameraApi::m_inputHandler
	CameraInputHandler_t3237370388 * ___m_inputHandler_6;

public:
	inline static int32_t get_offset_of_OnTransitionStart_1() { return static_cast<int32_t>(offsetof(CameraApi_t3006904385, ___OnTransitionStart_1)); }
	inline TransitionStartHandler_t1803894657 * get_OnTransitionStart_1() const { return ___OnTransitionStart_1; }
	inline TransitionStartHandler_t1803894657 ** get_address_of_OnTransitionStart_1() { return &___OnTransitionStart_1; }
	inline void set_OnTransitionStart_1(TransitionStartHandler_t1803894657 * value)
	{
		___OnTransitionStart_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnTransitionStart_1), value);
	}

	inline static int32_t get_offset_of_OnTransitionEnd_2() { return static_cast<int32_t>(offsetof(CameraApi_t3006904385, ___OnTransitionEnd_2)); }
	inline TransitionEndHandler_t4082272293 * get_OnTransitionEnd_2() const { return ___OnTransitionEnd_2; }
	inline TransitionEndHandler_t4082272293 ** get_address_of_OnTransitionEnd_2() { return &___OnTransitionEnd_2; }
	inline void set_OnTransitionEnd_2(TransitionEndHandler_t4082272293 * value)
	{
		___OnTransitionEnd_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnTransitionEnd_2), value);
	}

	inline static int32_t get_offset_of_U3CIsTransitioningU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CameraApi_t3006904385, ___U3CIsTransitioningU3Ek__BackingField_3)); }
	inline bool get_U3CIsTransitioningU3Ek__BackingField_3() const { return ___U3CIsTransitioningU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsTransitioningU3Ek__BackingField_3() { return &___U3CIsTransitioningU3Ek__BackingField_3; }
	inline void set_U3CIsTransitioningU3Ek__BackingField_3(bool value)
	{
		___U3CIsTransitioningU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_m_controlledCamera_4() { return static_cast<int32_t>(offsetof(CameraApi_t3006904385, ___m_controlledCamera_4)); }
	inline Camera_t4157153871 * get_m_controlledCamera_4() const { return ___m_controlledCamera_4; }
	inline Camera_t4157153871 ** get_address_of_m_controlledCamera_4() { return &___m_controlledCamera_4; }
	inline void set_m_controlledCamera_4(Camera_t4157153871 * value)
	{
		___m_controlledCamera_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_controlledCamera_4), value);
	}

	inline static int32_t get_offset_of_m_apiImplementation_5() { return static_cast<int32_t>(offsetof(CameraApi_t3006904385, ___m_apiImplementation_5)); }
	inline ApiImplementation_t1854519848 * get_m_apiImplementation_5() const { return ___m_apiImplementation_5; }
	inline ApiImplementation_t1854519848 ** get_address_of_m_apiImplementation_5() { return &___m_apiImplementation_5; }
	inline void set_m_apiImplementation_5(ApiImplementation_t1854519848 * value)
	{
		___m_apiImplementation_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_apiImplementation_5), value);
	}

	inline static int32_t get_offset_of_m_inputHandler_6() { return static_cast<int32_t>(offsetof(CameraApi_t3006904385, ___m_inputHandler_6)); }
	inline CameraInputHandler_t3237370388 * get_m_inputHandler_6() const { return ___m_inputHandler_6; }
	inline CameraInputHandler_t3237370388 ** get_address_of_m_inputHandler_6() { return &___m_inputHandler_6; }
	inline void set_m_inputHandler_6(CameraInputHandler_t3237370388 * value)
	{
		___m_inputHandler_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_inputHandler_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAAPI_T3006904385_H
#ifndef CAMERAHELPERS_T2558075301_H
#define CAMERAHELPERS_T2558075301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Common.Camera.CameraHelpers
struct  CameraHelpers_t2558075301  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAHELPERS_T2558075301_H
#ifndef U3CEXAMPLEU3EC__ITERATOR0_T3005514146_H
#define U3CEXAMPLEU3EC__ITERATOR0_T3005514146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectingBuildings/<Example>c__Iterator0
struct  U3CExampleU3Ec__Iterator0_t3005514146  : public RuntimeObject
{
public:
	// SelectingBuildings SelectingBuildings/<Example>c__Iterator0::$this
	SelectingBuildings_t1914154491 * ___U24this_0;
	// System.Object SelectingBuildings/<Example>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean SelectingBuildings/<Example>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 SelectingBuildings/<Example>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t3005514146, ___U24this_0)); }
	inline SelectingBuildings_t1914154491 * get_U24this_0() const { return ___U24this_0; }
	inline SelectingBuildings_t1914154491 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SelectingBuildings_t1914154491 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t3005514146, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t3005514146, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t3005514146, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXAMPLEU3EC__ITERATOR0_T3005514146_H
#ifndef U3CCLEARHIGHLIGHTU3EC__ITERATOR0_T4062986860_H
#define U3CCLEARHIGHLIGHTU3EC__ITERATOR0_T4062986860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PickingBuildings/<ClearHighlight>c__Iterator0
struct  U3CClearHighlightU3Ec__Iterator0_t4062986860  : public RuntimeObject
{
public:
	// Wrld.Resources.Buildings.Highlight PickingBuildings/<ClearHighlight>c__Iterator0::highlight
	Highlight_t3200169708 * ___highlight_0;
	// System.Object PickingBuildings/<ClearHighlight>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean PickingBuildings/<ClearHighlight>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 PickingBuildings/<ClearHighlight>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_highlight_0() { return static_cast<int32_t>(offsetof(U3CClearHighlightU3Ec__Iterator0_t4062986860, ___highlight_0)); }
	inline Highlight_t3200169708 * get_highlight_0() const { return ___highlight_0; }
	inline Highlight_t3200169708 ** get_address_of_highlight_0() { return &___highlight_0; }
	inline void set_highlight_0(Highlight_t3200169708 * value)
	{
		___highlight_0 = value;
		Il2CppCodeGenWriteBarrier((&___highlight_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CClearHighlightU3Ec__Iterator0_t4062986860, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CClearHighlightU3Ec__Iterator0_t4062986860, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CClearHighlightU3Ec__Iterator0_t4062986860, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCLEARHIGHLIGHTU3EC__ITERATOR0_T4062986860_H
#ifndef U3CEXAMPLEU3EC__ITERATOR0_T3905759373_H
#define U3CEXAMPLEU3EC__ITERATOR0_T3905759373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HighlightingBuildings/<Example>c__Iterator0
struct  U3CExampleU3Ec__Iterator0_t3905759373  : public RuntimeObject
{
public:
	// HighlightingBuildings HighlightingBuildings/<Example>c__Iterator0::$this
	HighlightingBuildings_t423429914 * ___U24this_0;
	// System.Object HighlightingBuildings/<Example>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean HighlightingBuildings/<Example>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 HighlightingBuildings/<Example>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t3905759373, ___U24this_0)); }
	inline HighlightingBuildings_t423429914 * get_U24this_0() const { return ___U24this_0; }
	inline HighlightingBuildings_t423429914 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(HighlightingBuildings_t423429914 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t3905759373, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t3905759373, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t3905759373, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXAMPLEU3EC__ITERATOR0_T3905759373_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CCLEARHIGHLIGHTU3EC__ITERATOR1_T1353935622_H
#define U3CCLEARHIGHLIGHTU3EC__ITERATOR1_T1353935622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClearingBuildingHighlights/<ClearHighlight>c__Iterator1
struct  U3CClearHighlightU3Ec__Iterator1_t1353935622  : public RuntimeObject
{
public:
	// Wrld.Resources.Buildings.Highlight ClearingBuildingHighlights/<ClearHighlight>c__Iterator1::highlight
	Highlight_t3200169708 * ___highlight_0;
	// System.Object ClearingBuildingHighlights/<ClearHighlight>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean ClearingBuildingHighlights/<ClearHighlight>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 ClearingBuildingHighlights/<ClearHighlight>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_highlight_0() { return static_cast<int32_t>(offsetof(U3CClearHighlightU3Ec__Iterator1_t1353935622, ___highlight_0)); }
	inline Highlight_t3200169708 * get_highlight_0() const { return ___highlight_0; }
	inline Highlight_t3200169708 ** get_address_of_highlight_0() { return &___highlight_0; }
	inline void set_highlight_0(Highlight_t3200169708 * value)
	{
		___highlight_0 = value;
		Il2CppCodeGenWriteBarrier((&___highlight_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CClearHighlightU3Ec__Iterator1_t1353935622, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CClearHighlightU3Ec__Iterator1_t1353935622, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CClearHighlightU3Ec__Iterator1_t1353935622, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCLEARHIGHLIGHTU3EC__ITERATOR1_T1353935622_H
#ifndef U3CEXAMPLEU3EC__ITERATOR0_T93868077_H
#define U3CEXAMPLEU3EC__ITERATOR0_T93868077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClearingBuildingHighlights/<Example>c__Iterator0
struct  U3CExampleU3Ec__Iterator0_t93868077  : public RuntimeObject
{
public:
	// ClearingBuildingHighlights ClearingBuildingHighlights/<Example>c__Iterator0::$this
	ClearingBuildingHighlights_t199549764 * ___U24this_0;
	// System.Object ClearingBuildingHighlights/<Example>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean ClearingBuildingHighlights/<Example>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 ClearingBuildingHighlights/<Example>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t93868077, ___U24this_0)); }
	inline ClearingBuildingHighlights_t199549764 * get_U24this_0() const { return ___U24this_0; }
	inline ClearingBuildingHighlights_t199549764 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(ClearingBuildingHighlights_t199549764 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t93868077, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t93868077, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t93868077, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXAMPLEU3EC__ITERATOR0_T93868077_H
#ifndef ZOOMLEVELHELPERS_T1711485203_H
#define ZOOMLEVELHELPERS_T1711485203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapCamera.ZoomLevelHelpers
struct  ZoomLevelHelpers_t1711485203  : public RuntimeObject
{
public:

public:
};

struct ZoomLevelHelpers_t1711485203_StaticFields
{
public:
	// System.Double[] Wrld.MapCamera.ZoomLevelHelpers::ms_zoomToDistances
	DoubleU5BU5D_t3413330114* ___ms_zoomToDistances_0;

public:
	inline static int32_t get_offset_of_ms_zoomToDistances_0() { return static_cast<int32_t>(offsetof(ZoomLevelHelpers_t1711485203_StaticFields, ___ms_zoomToDistances_0)); }
	inline DoubleU5BU5D_t3413330114* get_ms_zoomToDistances_0() const { return ___ms_zoomToDistances_0; }
	inline DoubleU5BU5D_t3413330114** get_address_of_ms_zoomToDistances_0() { return &___ms_zoomToDistances_0; }
	inline void set_ms_zoomToDistances_0(DoubleU5BU5D_t3413330114* value)
	{
		___ms_zoomToDistances_0 = value;
		Il2CppCodeGenWriteBarrier((&___ms_zoomToDistances_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOOMLEVELHELPERS_T1711485203_H
#ifndef U3CEXAMPLEU3EC__ITERATOR0_T4264857786_H
#define U3CEXAMPLEU3EC__ITERATOR0_T4264857786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PositionObjectAtLatitudeAndLongitude/<Example>c__Iterator0
struct  U3CExampleU3Ec__Iterator0_t4264857786  : public RuntimeObject
{
public:
	// PositionObjectAtLatitudeAndLongitude PositionObjectAtLatitudeAndLongitude/<Example>c__Iterator0::$this
	PositionObjectAtLatitudeAndLongitude_t3453145834 * ___U24this_0;
	// System.Object PositionObjectAtLatitudeAndLongitude/<Example>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean PositionObjectAtLatitudeAndLongitude/<Example>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 PositionObjectAtLatitudeAndLongitude/<Example>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t4264857786, ___U24this_0)); }
	inline PositionObjectAtLatitudeAndLongitude_t3453145834 * get_U24this_0() const { return ___U24this_0; }
	inline PositionObjectAtLatitudeAndLongitude_t3453145834 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(PositionObjectAtLatitudeAndLongitude_t3453145834 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t4264857786, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t4264857786, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t4264857786, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXAMPLEU3EC__ITERATOR0_T4264857786_H
#ifndef U3CEXAMPLEU3EC__ITERATOR0_T3168082780_H
#define U3CEXAMPLEU3EC__ITERATOR0_T3168082780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotateObjectOnMap/<Example>c__Iterator0
struct  U3CExampleU3Ec__Iterator0_t3168082780  : public RuntimeObject
{
public:
	// RotateObjectOnMap RotateObjectOnMap/<Example>c__Iterator0::$this
	RotateObjectOnMap_t115929985 * ___U24this_0;
	// System.Object RotateObjectOnMap/<Example>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean RotateObjectOnMap/<Example>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 RotateObjectOnMap/<Example>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t3168082780, ___U24this_0)); }
	inline RotateObjectOnMap_t115929985 * get_U24this_0() const { return ___U24this_0; }
	inline RotateObjectOnMap_t115929985 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(RotateObjectOnMap_t115929985 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t3168082780, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t3168082780, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t3168082780, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXAMPLEU3EC__ITERATOR0_T3168082780_H
#ifndef U3CCREATETHREADINTERNALU3EC__ANONSTOREY0_T2493176562_H
#define U3CCREATETHREADINTERNALU3EC__ANONSTOREY0_T2493176562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Concurrency.ThreadService/<CreateThreadInternal>c__AnonStorey0
struct  U3CCreateThreadInternalU3Ec__AnonStorey0_t2493176562  : public RuntimeObject
{
public:
	// Wrld.Concurrency.ThreadService/ThreadStartDelegate Wrld.Concurrency.ThreadService/<CreateThreadInternal>c__AnonStorey0::runFunc
	ThreadStartDelegate_t2173555516 * ___runFunc_0;

public:
	inline static int32_t get_offset_of_runFunc_0() { return static_cast<int32_t>(offsetof(U3CCreateThreadInternalU3Ec__AnonStorey0_t2493176562, ___runFunc_0)); }
	inline ThreadStartDelegate_t2173555516 * get_runFunc_0() const { return ___runFunc_0; }
	inline ThreadStartDelegate_t2173555516 ** get_address_of_runFunc_0() { return &___runFunc_0; }
	inline void set_runFunc_0(ThreadStartDelegate_t2173555516 * value)
	{
		___runFunc_0 = value;
		Il2CppCodeGenWriteBarrier((&___runFunc_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATETHREADINTERNALU3EC__ANONSTOREY0_T2493176562_H
#ifndef CONSTANTS_T3443931233_H
#define CONSTANTS_T3443931233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Constants
struct  Constants_t3443931233  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTANTS_T3443931233_H
#ifndef MOUSETAPGESTURE_T1901348987_H
#define MOUSETAPGESTURE_T1901348987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.Mouse.MouseTapGesture
struct  MouseTapGesture_t1901348987  : public RuntimeObject
{
public:
	// System.Int32 Wrld.MapInput.Mouse.MouseTapGesture::m_tapDownCount
	int32_t ___m_tapDownCount_0;
	// System.Int32 Wrld.MapInput.Mouse.MouseTapGesture::m_tapUpCount
	int32_t ___m_tapUpCount_1;
	// System.Int32 Wrld.MapInput.Mouse.MouseTapGesture::m_currentPointerTrackingStack
	int32_t ___m_currentPointerTrackingStack_2;
	// System.Single Wrld.MapInput.Mouse.MouseTapGesture::m_tapAnchorX
	float ___m_tapAnchorX_3;
	// System.Single Wrld.MapInput.Mouse.MouseTapGesture::m_tapAnchorY
	float ___m_tapAnchorY_4;
	// System.Int64 Wrld.MapInput.Mouse.MouseTapGesture::m_tapUnixTime
	int64_t ___m_tapUnixTime_5;
	// Wrld.MapInput.IUnityInputHandler Wrld.MapInput.Mouse.MouseTapGesture::m_tapHandler
	RuntimeObject* ___m_tapHandler_9;

public:
	inline static int32_t get_offset_of_m_tapDownCount_0() { return static_cast<int32_t>(offsetof(MouseTapGesture_t1901348987, ___m_tapDownCount_0)); }
	inline int32_t get_m_tapDownCount_0() const { return ___m_tapDownCount_0; }
	inline int32_t* get_address_of_m_tapDownCount_0() { return &___m_tapDownCount_0; }
	inline void set_m_tapDownCount_0(int32_t value)
	{
		___m_tapDownCount_0 = value;
	}

	inline static int32_t get_offset_of_m_tapUpCount_1() { return static_cast<int32_t>(offsetof(MouseTapGesture_t1901348987, ___m_tapUpCount_1)); }
	inline int32_t get_m_tapUpCount_1() const { return ___m_tapUpCount_1; }
	inline int32_t* get_address_of_m_tapUpCount_1() { return &___m_tapUpCount_1; }
	inline void set_m_tapUpCount_1(int32_t value)
	{
		___m_tapUpCount_1 = value;
	}

	inline static int32_t get_offset_of_m_currentPointerTrackingStack_2() { return static_cast<int32_t>(offsetof(MouseTapGesture_t1901348987, ___m_currentPointerTrackingStack_2)); }
	inline int32_t get_m_currentPointerTrackingStack_2() const { return ___m_currentPointerTrackingStack_2; }
	inline int32_t* get_address_of_m_currentPointerTrackingStack_2() { return &___m_currentPointerTrackingStack_2; }
	inline void set_m_currentPointerTrackingStack_2(int32_t value)
	{
		___m_currentPointerTrackingStack_2 = value;
	}

	inline static int32_t get_offset_of_m_tapAnchorX_3() { return static_cast<int32_t>(offsetof(MouseTapGesture_t1901348987, ___m_tapAnchorX_3)); }
	inline float get_m_tapAnchorX_3() const { return ___m_tapAnchorX_3; }
	inline float* get_address_of_m_tapAnchorX_3() { return &___m_tapAnchorX_3; }
	inline void set_m_tapAnchorX_3(float value)
	{
		___m_tapAnchorX_3 = value;
	}

	inline static int32_t get_offset_of_m_tapAnchorY_4() { return static_cast<int32_t>(offsetof(MouseTapGesture_t1901348987, ___m_tapAnchorY_4)); }
	inline float get_m_tapAnchorY_4() const { return ___m_tapAnchorY_4; }
	inline float* get_address_of_m_tapAnchorY_4() { return &___m_tapAnchorY_4; }
	inline void set_m_tapAnchorY_4(float value)
	{
		___m_tapAnchorY_4 = value;
	}

	inline static int32_t get_offset_of_m_tapUnixTime_5() { return static_cast<int32_t>(offsetof(MouseTapGesture_t1901348987, ___m_tapUnixTime_5)); }
	inline int64_t get_m_tapUnixTime_5() const { return ___m_tapUnixTime_5; }
	inline int64_t* get_address_of_m_tapUnixTime_5() { return &___m_tapUnixTime_5; }
	inline void set_m_tapUnixTime_5(int64_t value)
	{
		___m_tapUnixTime_5 = value;
	}

	inline static int32_t get_offset_of_m_tapHandler_9() { return static_cast<int32_t>(offsetof(MouseTapGesture_t1901348987, ___m_tapHandler_9)); }
	inline RuntimeObject* get_m_tapHandler_9() const { return ___m_tapHandler_9; }
	inline RuntimeObject** get_address_of_m_tapHandler_9() { return &___m_tapHandler_9; }
	inline void set_m_tapHandler_9(RuntimeObject* value)
	{
		___m_tapHandler_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_tapHandler_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSETAPGESTURE_T1901348987_H
#ifndef MOUSETILTGESTURE_T4197683903_H
#define MOUSETILTGESTURE_T4197683903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.Mouse.MouseTiltGesture
struct  MouseTiltGesture_t4197683903  : public RuntimeObject
{
public:
	// Wrld.MapInput.IUnityInputHandler Wrld.MapInput.Mouse.MouseTiltGesture::m_handler
	RuntimeObject* ___m_handler_0;
	// System.Boolean Wrld.MapInput.Mouse.MouseTiltGesture::m_tilting
	bool ___m_tilting_1;
	// System.Single Wrld.MapInput.Mouse.MouseTiltGesture::m_previousMousePositionY
	float ___m_previousMousePositionY_2;

public:
	inline static int32_t get_offset_of_m_handler_0() { return static_cast<int32_t>(offsetof(MouseTiltGesture_t4197683903, ___m_handler_0)); }
	inline RuntimeObject* get_m_handler_0() const { return ___m_handler_0; }
	inline RuntimeObject** get_address_of_m_handler_0() { return &___m_handler_0; }
	inline void set_m_handler_0(RuntimeObject* value)
	{
		___m_handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_handler_0), value);
	}

	inline static int32_t get_offset_of_m_tilting_1() { return static_cast<int32_t>(offsetof(MouseTiltGesture_t4197683903, ___m_tilting_1)); }
	inline bool get_m_tilting_1() const { return ___m_tilting_1; }
	inline bool* get_address_of_m_tilting_1() { return &___m_tilting_1; }
	inline void set_m_tilting_1(bool value)
	{
		___m_tilting_1 = value;
	}

	inline static int32_t get_offset_of_m_previousMousePositionY_2() { return static_cast<int32_t>(offsetof(MouseTiltGesture_t4197683903, ___m_previousMousePositionY_2)); }
	inline float get_m_previousMousePositionY_2() const { return ___m_previousMousePositionY_2; }
	inline float* get_address_of_m_previousMousePositionY_2() { return &___m_previousMousePositionY_2; }
	inline void set_m_previousMousePositionY_2(float value)
	{
		___m_previousMousePositionY_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSETILTGESTURE_T4197683903_H
#ifndef THREADSERVICE_T1369597789_H
#define THREADSERVICE_T1369597789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Concurrency.ThreadService
struct  ThreadService_t1369597789  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.Thread> Wrld.Concurrency.ThreadService::m_threads
	Dictionary_2_t1189549400 * ___m_threads_1;
	// System.Int32 Wrld.Concurrency.ThreadService::m_nextThreadID
	int32_t ___m_nextThreadID_2;

public:
	inline static int32_t get_offset_of_m_threads_1() { return static_cast<int32_t>(offsetof(ThreadService_t1369597789, ___m_threads_1)); }
	inline Dictionary_2_t1189549400 * get_m_threads_1() const { return ___m_threads_1; }
	inline Dictionary_2_t1189549400 ** get_address_of_m_threads_1() { return &___m_threads_1; }
	inline void set_m_threads_1(Dictionary_2_t1189549400 * value)
	{
		___m_threads_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_threads_1), value);
	}

	inline static int32_t get_offset_of_m_nextThreadID_2() { return static_cast<int32_t>(offsetof(ThreadService_t1369597789, ___m_nextThreadID_2)); }
	inline int32_t get_m_nextThreadID_2() const { return ___m_nextThreadID_2; }
	inline int32_t* get_address_of_m_nextThreadID_2() { return &___m_nextThreadID_2; }
	inline void set_m_nextThreadID_2(int32_t value)
	{
		___m_nextThreadID_2 = value;
	}
};

struct ThreadService_t1369597789_StaticFields
{
public:
	// Wrld.Concurrency.ThreadService Wrld.Concurrency.ThreadService::ms_instance
	ThreadService_t1369597789 * ___ms_instance_0;

public:
	inline static int32_t get_offset_of_ms_instance_0() { return static_cast<int32_t>(offsetof(ThreadService_t1369597789_StaticFields, ___ms_instance_0)); }
	inline ThreadService_t1369597789 * get_ms_instance_0() const { return ___ms_instance_0; }
	inline ThreadService_t1369597789 ** get_address_of_ms_instance_0() { return &___ms_instance_0; }
	inline void set_ms_instance_0(ThreadService_t1369597789 * value)
	{
		___ms_instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___ms_instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADSERVICE_T1369597789_H
#ifndef EXAMPLE_T2853439971_H
#define EXAMPLE_T2853439971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleChanger/Example
struct  Example_t2853439971 
{
public:
	// System.String ExampleChanger/Example::Name
	String_t* ___Name_0;
	// UnityEngine.GameObject ExampleChanger/Example::Root
	GameObject_t1113636619 * ___Root_1;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(Example_t2853439971, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Root_1() { return static_cast<int32_t>(offsetof(Example_t2853439971, ___Root_1)); }
	inline GameObject_t1113636619 * get_Root_1() const { return ___Root_1; }
	inline GameObject_t1113636619 ** get_address_of_Root_1() { return &___Root_1; }
	inline void set_Root_1(GameObject_t1113636619 * value)
	{
		___Root_1 = value;
		Il2CppCodeGenWriteBarrier((&___Root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of ExampleChanger/Example
struct Example_t2853439971_marshaled_pinvoke
{
	char* ___Name_0;
	GameObject_t1113636619 * ___Root_1;
};
// Native definition for COM marshalling of ExampleChanger/Example
struct Example_t2853439971_marshaled_com
{
	Il2CppChar* ___Name_0;
	GameObject_t1113636619 * ___Root_1;
};
#endif // EXAMPLE_T2853439971_H
#ifndef KEYBOARDINPUTEVENT_T410871953_H
#define KEYBOARDINPUTEVENT_T410871953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.Mouse.KeyboardInputEvent
struct  KeyboardInputEvent_t410871953 
{
public:
	// System.Char Wrld.MapInput.Mouse.KeyboardInputEvent::KeyCode
	Il2CppChar ___KeyCode_0;
	// System.Boolean Wrld.MapInput.Mouse.KeyboardInputEvent::KeyDownEvent
	bool ___KeyDownEvent_1;

public:
	inline static int32_t get_offset_of_KeyCode_0() { return static_cast<int32_t>(offsetof(KeyboardInputEvent_t410871953, ___KeyCode_0)); }
	inline Il2CppChar get_KeyCode_0() const { return ___KeyCode_0; }
	inline Il2CppChar* get_address_of_KeyCode_0() { return &___KeyCode_0; }
	inline void set_KeyCode_0(Il2CppChar value)
	{
		___KeyCode_0 = value;
	}

	inline static int32_t get_offset_of_KeyDownEvent_1() { return static_cast<int32_t>(offsetof(KeyboardInputEvent_t410871953, ___KeyDownEvent_1)); }
	inline bool get_KeyDownEvent_1() const { return ___KeyDownEvent_1; }
	inline bool* get_address_of_KeyDownEvent_1() { return &___KeyDownEvent_1; }
	inline void set_KeyDownEvent_1(bool value)
	{
		___KeyDownEvent_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Wrld.MapInput.Mouse.KeyboardInputEvent
struct KeyboardInputEvent_t410871953_marshaled_pinvoke
{
	uint8_t ___KeyCode_0;
	int32_t ___KeyDownEvent_1;
};
// Native definition for COM marshalling of Wrld.MapInput.Mouse.KeyboardInputEvent
struct KeyboardInputEvent_t410871953_marshaled_com
{
	uint8_t ___KeyCode_0;
	int32_t ___KeyDownEvent_1;
};
#endif // KEYBOARDINPUTEVENT_T410871953_H
#ifndef TILTDATA_T2861649398_H
#define TILTDATA_T2861649398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.AppInterface.TiltData
struct  TiltData_t2861649398 
{
public:
	// System.Single Wrld.MapInput.AppInterface.TiltData::distance
	float ___distance_0;
	// System.Single Wrld.MapInput.AppInterface.TiltData::screenHeight
	float ___screenHeight_1;
	// System.Single Wrld.MapInput.AppInterface.TiltData::screenPercentageNormalized
	float ___screenPercentageNormalized_2;

public:
	inline static int32_t get_offset_of_distance_0() { return static_cast<int32_t>(offsetof(TiltData_t2861649398, ___distance_0)); }
	inline float get_distance_0() const { return ___distance_0; }
	inline float* get_address_of_distance_0() { return &___distance_0; }
	inline void set_distance_0(float value)
	{
		___distance_0 = value;
	}

	inline static int32_t get_offset_of_screenHeight_1() { return static_cast<int32_t>(offsetof(TiltData_t2861649398, ___screenHeight_1)); }
	inline float get_screenHeight_1() const { return ___screenHeight_1; }
	inline float* get_address_of_screenHeight_1() { return &___screenHeight_1; }
	inline void set_screenHeight_1(float value)
	{
		___screenHeight_1 = value;
	}

	inline static int32_t get_offset_of_screenPercentageNormalized_2() { return static_cast<int32_t>(offsetof(TiltData_t2861649398, ___screenPercentageNormalized_2)); }
	inline float get_screenPercentageNormalized_2() const { return ___screenPercentageNormalized_2; }
	inline float* get_address_of_screenPercentageNormalized_2() { return &___screenPercentageNormalized_2; }
	inline void set_screenPercentageNormalized_2(float value)
	{
		___screenPercentageNormalized_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTDATA_T2861649398_H
#ifndef ZOOMDATA_T3547862491_H
#define ZOOMDATA_T3547862491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.AppInterface.ZoomData
struct  ZoomData_t3547862491 
{
public:
	// System.Single Wrld.MapInput.AppInterface.ZoomData::distance
	float ___distance_0;

public:
	inline static int32_t get_offset_of_distance_0() { return static_cast<int32_t>(offsetof(ZoomData_t3547862491, ___distance_0)); }
	inline float get_distance_0() const { return ___distance_0; }
	inline float* get_address_of_distance_0() { return &___distance_0; }
	inline void set_distance_0(float value)
	{
		___distance_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOOMDATA_T3547862491_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef KEYBOARDDATA_T2354704333_H
#define KEYBOARDDATA_T2354704333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.AppInterface.KeyboardData
struct  KeyboardData_t2354704333 
{
public:
	// System.Char Wrld.MapInput.AppInterface.KeyboardData::keyCode
	Il2CppChar ___keyCode_0;
	// System.UInt32 Wrld.MapInput.AppInterface.KeyboardData::metaKeys
	uint32_t ___metaKeys_1;
	// System.Boolean Wrld.MapInput.AppInterface.KeyboardData::printable
	bool ___printable_2;
	// System.Boolean Wrld.MapInput.AppInterface.KeyboardData::isKeyDown
	bool ___isKeyDown_3;

public:
	inline static int32_t get_offset_of_keyCode_0() { return static_cast<int32_t>(offsetof(KeyboardData_t2354704333, ___keyCode_0)); }
	inline Il2CppChar get_keyCode_0() const { return ___keyCode_0; }
	inline Il2CppChar* get_address_of_keyCode_0() { return &___keyCode_0; }
	inline void set_keyCode_0(Il2CppChar value)
	{
		___keyCode_0 = value;
	}

	inline static int32_t get_offset_of_metaKeys_1() { return static_cast<int32_t>(offsetof(KeyboardData_t2354704333, ___metaKeys_1)); }
	inline uint32_t get_metaKeys_1() const { return ___metaKeys_1; }
	inline uint32_t* get_address_of_metaKeys_1() { return &___metaKeys_1; }
	inline void set_metaKeys_1(uint32_t value)
	{
		___metaKeys_1 = value;
	}

	inline static int32_t get_offset_of_printable_2() { return static_cast<int32_t>(offsetof(KeyboardData_t2354704333, ___printable_2)); }
	inline bool get_printable_2() const { return ___printable_2; }
	inline bool* get_address_of_printable_2() { return &___printable_2; }
	inline void set_printable_2(bool value)
	{
		___printable_2 = value;
	}

	inline static int32_t get_offset_of_isKeyDown_3() { return static_cast<int32_t>(offsetof(KeyboardData_t2354704333, ___isKeyDown_3)); }
	inline bool get_isKeyDown_3() const { return ___isKeyDown_3; }
	inline bool* get_address_of_isKeyDown_3() { return &___isKeyDown_3; }
	inline void set_isKeyDown_3(bool value)
	{
		___isKeyDown_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Wrld.MapInput.AppInterface.KeyboardData
struct KeyboardData_t2354704333_marshaled_pinvoke
{
	uint8_t ___keyCode_0;
	uint32_t ___metaKeys_1;
	int32_t ___printable_2;
	int32_t ___isKeyDown_3;
};
// Native definition for COM marshalling of Wrld.MapInput.AppInterface.KeyboardData
struct KeyboardData_t2354704333_marshaled_com
{
	uint8_t ___keyCode_0;
	uint32_t ___metaKeys_1;
	int32_t ___printable_2;
	int32_t ___isKeyDown_3;
};
#endif // KEYBOARDDATA_T2354704333_H
#ifndef LATLONG_T2936018554_H
#define LATLONG_T2936018554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Space.LatLong
struct  LatLong_t2936018554 
{
public:
	// System.Double Wrld.Space.LatLong::m_latitudeInDegrees
	double ___m_latitudeInDegrees_0;
	// System.Double Wrld.Space.LatLong::m_longitudeInDegrees
	double ___m_longitudeInDegrees_1;

public:
	inline static int32_t get_offset_of_m_latitudeInDegrees_0() { return static_cast<int32_t>(offsetof(LatLong_t2936018554, ___m_latitudeInDegrees_0)); }
	inline double get_m_latitudeInDegrees_0() const { return ___m_latitudeInDegrees_0; }
	inline double* get_address_of_m_latitudeInDegrees_0() { return &___m_latitudeInDegrees_0; }
	inline void set_m_latitudeInDegrees_0(double value)
	{
		___m_latitudeInDegrees_0 = value;
	}

	inline static int32_t get_offset_of_m_longitudeInDegrees_1() { return static_cast<int32_t>(offsetof(LatLong_t2936018554, ___m_longitudeInDegrees_1)); }
	inline double get_m_longitudeInDegrees_1() const { return ___m_longitudeInDegrees_1; }
	inline double* get_address_of_m_longitudeInDegrees_1() { return &___m_longitudeInDegrees_1; }
	inline void set_m_longitudeInDegrees_1(double value)
	{
		___m_longitudeInDegrees_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LATLONG_T2936018554_H
#ifndef ROTATEDATA_T3683904859_H
#define ROTATEDATA_T3683904859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.AppInterface.RotateData
struct  RotateData_t3683904859 
{
public:
	// System.Single Wrld.MapInput.AppInterface.RotateData::rotation
	float ___rotation_0;
	// System.Single Wrld.MapInput.AppInterface.RotateData::velocity
	float ___velocity_1;
	// System.Int32 Wrld.MapInput.AppInterface.RotateData::numTouches
	int32_t ___numTouches_2;

public:
	inline static int32_t get_offset_of_rotation_0() { return static_cast<int32_t>(offsetof(RotateData_t3683904859, ___rotation_0)); }
	inline float get_rotation_0() const { return ___rotation_0; }
	inline float* get_address_of_rotation_0() { return &___rotation_0; }
	inline void set_rotation_0(float value)
	{
		___rotation_0 = value;
	}

	inline static int32_t get_offset_of_velocity_1() { return static_cast<int32_t>(offsetof(RotateData_t3683904859, ___velocity_1)); }
	inline float get_velocity_1() const { return ___velocity_1; }
	inline float* get_address_of_velocity_1() { return &___velocity_1; }
	inline void set_velocity_1(float value)
	{
		___velocity_1 = value;
	}

	inline static int32_t get_offset_of_numTouches_2() { return static_cast<int32_t>(offsetof(RotateData_t3683904859, ___numTouches_2)); }
	inline int32_t get_numTouches_2() const { return ___numTouches_2; }
	inline int32_t* get_address_of_numTouches_2() { return &___numTouches_2; }
	inline void set_numTouches_2(int32_t value)
	{
		___numTouches_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEDATA_T3683904859_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef PINCHDATA_T2028234657_H
#define PINCHDATA_T2028234657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.AppInterface.PinchData
struct  PinchData_t2028234657 
{
public:
	// System.Single Wrld.MapInput.AppInterface.PinchData::scale
	float ___scale_0;

public:
	inline static int32_t get_offset_of_scale_0() { return static_cast<int32_t>(offsetof(PinchData_t2028234657, ___scale_0)); }
	inline float get_scale_0() const { return ___scale_0; }
	inline float* get_address_of_scale_0() { return &___scale_0; }
	inline void set_scale_0(float value)
	{
		___scale_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINCHDATA_T2028234657_H
#ifndef COLLISIONCONFIG_T3653208188_H
#define COLLISIONCONFIG_T3653208188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.ConfigParams/CollisionConfig
struct  CollisionConfig_t3653208188 
{
public:
	union
	{
		struct
		{
			// System.Boolean Wrld.ConfigParams/CollisionConfig::<TerrainCollision>k__BackingField
			bool ___U3CTerrainCollisionU3Ek__BackingField_0;
			// System.Boolean Wrld.ConfigParams/CollisionConfig::<RoadCollision>k__BackingField
			bool ___U3CRoadCollisionU3Ek__BackingField_1;
			// System.Boolean Wrld.ConfigParams/CollisionConfig::<BuildingCollision>k__BackingField
			bool ___U3CBuildingCollisionU3Ek__BackingField_2;
		};
		uint8_t CollisionConfig_t3653208188__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CTerrainCollisionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CollisionConfig_t3653208188, ___U3CTerrainCollisionU3Ek__BackingField_0)); }
	inline bool get_U3CTerrainCollisionU3Ek__BackingField_0() const { return ___U3CTerrainCollisionU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CTerrainCollisionU3Ek__BackingField_0() { return &___U3CTerrainCollisionU3Ek__BackingField_0; }
	inline void set_U3CTerrainCollisionU3Ek__BackingField_0(bool value)
	{
		___U3CTerrainCollisionU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CRoadCollisionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CollisionConfig_t3653208188, ___U3CRoadCollisionU3Ek__BackingField_1)); }
	inline bool get_U3CRoadCollisionU3Ek__BackingField_1() const { return ___U3CRoadCollisionU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CRoadCollisionU3Ek__BackingField_1() { return &___U3CRoadCollisionU3Ek__BackingField_1; }
	inline void set_U3CRoadCollisionU3Ek__BackingField_1(bool value)
	{
		___U3CRoadCollisionU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CBuildingCollisionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CollisionConfig_t3653208188, ___U3CBuildingCollisionU3Ek__BackingField_2)); }
	inline bool get_U3CBuildingCollisionU3Ek__BackingField_2() const { return ___U3CBuildingCollisionU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CBuildingCollisionU3Ek__BackingField_2() { return &___U3CBuildingCollisionU3Ek__BackingField_2; }
	inline void set_U3CBuildingCollisionU3Ek__BackingField_2(bool value)
	{
		___U3CBuildingCollisionU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Wrld.ConfigParams/CollisionConfig
struct CollisionConfig_t3653208188_marshaled_pinvoke
{
	union
	{
		struct
		{
			int32_t ___U3CTerrainCollisionU3Ek__BackingField_0;
			int32_t ___U3CRoadCollisionU3Ek__BackingField_1;
			int32_t ___U3CBuildingCollisionU3Ek__BackingField_2;
		};
		uint8_t CollisionConfig_t3653208188__padding[1];
	};
};
// Native definition for COM marshalling of Wrld.ConfigParams/CollisionConfig
struct CollisionConfig_t3653208188_marshaled_com
{
	union
	{
		struct
		{
			int32_t ___U3CTerrainCollisionU3Ek__BackingField_0;
			int32_t ___U3CRoadCollisionU3Ek__BackingField_1;
			int32_t ___U3CBuildingCollisionU3Ek__BackingField_2;
		};
		uint8_t CollisionConfig_t3653208188__padding[1];
	};
};
#endif // COLLISIONCONFIG_T3653208188_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef NATIVECONFIG_T18506486_H
#define NATIVECONFIG_T18506486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.ConfigParams/NativeConfig
struct  NativeConfig_t18506486 
{
public:
	// System.Double Wrld.ConfigParams/NativeConfig::m_latitudeDegrees
	double ___m_latitudeDegrees_0;
	// System.Double Wrld.ConfigParams/NativeConfig::m_longitudeDegrees
	double ___m_longitudeDegrees_1;
	// System.Double Wrld.ConfigParams/NativeConfig::m_distanceToInterest
	double ___m_distanceToInterest_2;
	// System.Double Wrld.ConfigParams/NativeConfig::m_headingDegrees
	double ___m_headingDegrees_3;
	// System.Boolean Wrld.ConfigParams/NativeConfig::m_streamingLodBasedOnDistance
	bool ___m_streamingLodBasedOnDistance_4;

public:
	inline static int32_t get_offset_of_m_latitudeDegrees_0() { return static_cast<int32_t>(offsetof(NativeConfig_t18506486, ___m_latitudeDegrees_0)); }
	inline double get_m_latitudeDegrees_0() const { return ___m_latitudeDegrees_0; }
	inline double* get_address_of_m_latitudeDegrees_0() { return &___m_latitudeDegrees_0; }
	inline void set_m_latitudeDegrees_0(double value)
	{
		___m_latitudeDegrees_0 = value;
	}

	inline static int32_t get_offset_of_m_longitudeDegrees_1() { return static_cast<int32_t>(offsetof(NativeConfig_t18506486, ___m_longitudeDegrees_1)); }
	inline double get_m_longitudeDegrees_1() const { return ___m_longitudeDegrees_1; }
	inline double* get_address_of_m_longitudeDegrees_1() { return &___m_longitudeDegrees_1; }
	inline void set_m_longitudeDegrees_1(double value)
	{
		___m_longitudeDegrees_1 = value;
	}

	inline static int32_t get_offset_of_m_distanceToInterest_2() { return static_cast<int32_t>(offsetof(NativeConfig_t18506486, ___m_distanceToInterest_2)); }
	inline double get_m_distanceToInterest_2() const { return ___m_distanceToInterest_2; }
	inline double* get_address_of_m_distanceToInterest_2() { return &___m_distanceToInterest_2; }
	inline void set_m_distanceToInterest_2(double value)
	{
		___m_distanceToInterest_2 = value;
	}

	inline static int32_t get_offset_of_m_headingDegrees_3() { return static_cast<int32_t>(offsetof(NativeConfig_t18506486, ___m_headingDegrees_3)); }
	inline double get_m_headingDegrees_3() const { return ___m_headingDegrees_3; }
	inline double* get_address_of_m_headingDegrees_3() { return &___m_headingDegrees_3; }
	inline void set_m_headingDegrees_3(double value)
	{
		___m_headingDegrees_3 = value;
	}

	inline static int32_t get_offset_of_m_streamingLodBasedOnDistance_4() { return static_cast<int32_t>(offsetof(NativeConfig_t18506486, ___m_streamingLodBasedOnDistance_4)); }
	inline bool get_m_streamingLodBasedOnDistance_4() const { return ___m_streamingLodBasedOnDistance_4; }
	inline bool* get_address_of_m_streamingLodBasedOnDistance_4() { return &___m_streamingLodBasedOnDistance_4; }
	inline void set_m_streamingLodBasedOnDistance_4(bool value)
	{
		___m_streamingLodBasedOnDistance_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Wrld.ConfigParams/NativeConfig
struct NativeConfig_t18506486_marshaled_pinvoke
{
	double ___m_latitudeDegrees_0;
	double ___m_longitudeDegrees_1;
	double ___m_distanceToInterest_2;
	double ___m_headingDegrees_3;
	int32_t ___m_streamingLodBasedOnDistance_4;
};
// Native definition for COM marshalling of Wrld.ConfigParams/NativeConfig
struct NativeConfig_t18506486_marshaled_com
{
	double ___m_latitudeDegrees_0;
	double ___m_longitudeDegrees_1;
	double ___m_distanceToInterest_2;
	double ___m_headingDegrees_3;
	int32_t ___m_streamingLodBasedOnDistance_4;
};
#endif // NATIVECONFIG_T18506486_H
#ifndef UNITYARLIGHTESTIMATE_T1498306117_H
#define UNITYARLIGHTESTIMATE_T1498306117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARLightEstimate
struct  UnityARLightEstimate_t1498306117 
{
public:
	// System.Single UnityEngine.XR.iOS.UnityARLightEstimate::ambientIntensity
	float ___ambientIntensity_0;
	// System.Single UnityEngine.XR.iOS.UnityARLightEstimate::ambientColorTemperature
	float ___ambientColorTemperature_1;

public:
	inline static int32_t get_offset_of_ambientIntensity_0() { return static_cast<int32_t>(offsetof(UnityARLightEstimate_t1498306117, ___ambientIntensity_0)); }
	inline float get_ambientIntensity_0() const { return ___ambientIntensity_0; }
	inline float* get_address_of_ambientIntensity_0() { return &___ambientIntensity_0; }
	inline void set_ambientIntensity_0(float value)
	{
		___ambientIntensity_0 = value;
	}

	inline static int32_t get_offset_of_ambientColorTemperature_1() { return static_cast<int32_t>(offsetof(UnityARLightEstimate_t1498306117, ___ambientColorTemperature_1)); }
	inline float get_ambientColorTemperature_1() const { return ___ambientColorTemperature_1; }
	inline float* get_address_of_ambientColorTemperature_1() { return &___ambientColorTemperature_1; }
	inline void set_ambientColorTemperature_1(float value)
	{
		___ambientColorTemperature_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARLIGHTESTIMATE_T1498306117_H
#ifndef DOUBLEVECTOR3_T761704365_H
#define DOUBLEVECTOR3_T761704365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Common.Maths.DoubleVector3
struct  DoubleVector3_t761704365 
{
public:
	// System.Double Wrld.Common.Maths.DoubleVector3::x
	double ___x_3;
	// System.Double Wrld.Common.Maths.DoubleVector3::y
	double ___y_4;
	// System.Double Wrld.Common.Maths.DoubleVector3::z
	double ___z_5;

public:
	inline static int32_t get_offset_of_x_3() { return static_cast<int32_t>(offsetof(DoubleVector3_t761704365, ___x_3)); }
	inline double get_x_3() const { return ___x_3; }
	inline double* get_address_of_x_3() { return &___x_3; }
	inline void set_x_3(double value)
	{
		___x_3 = value;
	}

	inline static int32_t get_offset_of_y_4() { return static_cast<int32_t>(offsetof(DoubleVector3_t761704365, ___y_4)); }
	inline double get_y_4() const { return ___y_4; }
	inline double* get_address_of_y_4() { return &___y_4; }
	inline void set_y_4(double value)
	{
		___y_4 = value;
	}

	inline static int32_t get_offset_of_z_5() { return static_cast<int32_t>(offsetof(DoubleVector3_t761704365, ___z_5)); }
	inline double get_z_5() const { return ___z_5; }
	inline double* get_address_of_z_5() { return &___z_5; }
	inline void set_z_5(double value)
	{
		___z_5 = value;
	}
};

struct DoubleVector3_t761704365_StaticFields
{
public:
	// Wrld.Common.Maths.DoubleVector3 Wrld.Common.Maths.DoubleVector3::zero
	DoubleVector3_t761704365  ___zero_1;
	// Wrld.Common.Maths.DoubleVector3 Wrld.Common.Maths.DoubleVector3::one
	DoubleVector3_t761704365  ___one_2;

public:
	inline static int32_t get_offset_of_zero_1() { return static_cast<int32_t>(offsetof(DoubleVector3_t761704365_StaticFields, ___zero_1)); }
	inline DoubleVector3_t761704365  get_zero_1() const { return ___zero_1; }
	inline DoubleVector3_t761704365 * get_address_of_zero_1() { return &___zero_1; }
	inline void set_zero_1(DoubleVector3_t761704365  value)
	{
		___zero_1 = value;
	}

	inline static int32_t get_offset_of_one_2() { return static_cast<int32_t>(offsetof(DoubleVector3_t761704365_StaticFields, ___one_2)); }
	inline DoubleVector3_t761704365  get_one_2() const { return ___one_2; }
	inline DoubleVector3_t761704365 * get_address_of_one_2() { return &___one_2; }
	inline void set_one_2(DoubleVector3_t761704365  value)
	{
		___one_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLEVECTOR3_T761704365_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef KEYBOARDMODIFIERS_T1080473607_H
#define KEYBOARDMODIFIERS_T1080473607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.Mouse.KeyboardModifiers
struct  KeyboardModifiers_t1080473607 
{
public:
	// System.Int32 Wrld.MapInput.Mouse.KeyboardModifiers::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyboardModifiers_t1080473607, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDMODIFIERS_T1080473607_H
#ifndef CONFIGPARAMS_T592212086_H
#define CONFIGPARAMS_T592212086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.ConfigParams
struct  ConfigParams_t592212086 
{
public:
	// Wrld.ConfigParams/NativeConfig Wrld.ConfigParams::m_nativeConfig
	NativeConfig_t18506486  ___m_nativeConfig_0;
	// System.String Wrld.ConfigParams::MaterialsDirectory
	String_t* ___MaterialsDirectory_1;
	// UnityEngine.Material Wrld.ConfigParams::OverrideLandmarkMaterial
	Material_t340375123 * ___OverrideLandmarkMaterial_2;
	// System.String Wrld.ConfigParams::CoverageTreeManifestUrl
	String_t* ___CoverageTreeManifestUrl_3;
	// System.String Wrld.ConfigParams::ThemeManifestUrl
	String_t* ___ThemeManifestUrl_4;
	// Wrld.ConfigParams/CollisionConfig Wrld.ConfigParams::Collisions
	CollisionConfig_t3653208188  ___Collisions_5;

public:
	inline static int32_t get_offset_of_m_nativeConfig_0() { return static_cast<int32_t>(offsetof(ConfigParams_t592212086, ___m_nativeConfig_0)); }
	inline NativeConfig_t18506486  get_m_nativeConfig_0() const { return ___m_nativeConfig_0; }
	inline NativeConfig_t18506486 * get_address_of_m_nativeConfig_0() { return &___m_nativeConfig_0; }
	inline void set_m_nativeConfig_0(NativeConfig_t18506486  value)
	{
		___m_nativeConfig_0 = value;
	}

	inline static int32_t get_offset_of_MaterialsDirectory_1() { return static_cast<int32_t>(offsetof(ConfigParams_t592212086, ___MaterialsDirectory_1)); }
	inline String_t* get_MaterialsDirectory_1() const { return ___MaterialsDirectory_1; }
	inline String_t** get_address_of_MaterialsDirectory_1() { return &___MaterialsDirectory_1; }
	inline void set_MaterialsDirectory_1(String_t* value)
	{
		___MaterialsDirectory_1 = value;
		Il2CppCodeGenWriteBarrier((&___MaterialsDirectory_1), value);
	}

	inline static int32_t get_offset_of_OverrideLandmarkMaterial_2() { return static_cast<int32_t>(offsetof(ConfigParams_t592212086, ___OverrideLandmarkMaterial_2)); }
	inline Material_t340375123 * get_OverrideLandmarkMaterial_2() const { return ___OverrideLandmarkMaterial_2; }
	inline Material_t340375123 ** get_address_of_OverrideLandmarkMaterial_2() { return &___OverrideLandmarkMaterial_2; }
	inline void set_OverrideLandmarkMaterial_2(Material_t340375123 * value)
	{
		___OverrideLandmarkMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___OverrideLandmarkMaterial_2), value);
	}

	inline static int32_t get_offset_of_CoverageTreeManifestUrl_3() { return static_cast<int32_t>(offsetof(ConfigParams_t592212086, ___CoverageTreeManifestUrl_3)); }
	inline String_t* get_CoverageTreeManifestUrl_3() const { return ___CoverageTreeManifestUrl_3; }
	inline String_t** get_address_of_CoverageTreeManifestUrl_3() { return &___CoverageTreeManifestUrl_3; }
	inline void set_CoverageTreeManifestUrl_3(String_t* value)
	{
		___CoverageTreeManifestUrl_3 = value;
		Il2CppCodeGenWriteBarrier((&___CoverageTreeManifestUrl_3), value);
	}

	inline static int32_t get_offset_of_ThemeManifestUrl_4() { return static_cast<int32_t>(offsetof(ConfigParams_t592212086, ___ThemeManifestUrl_4)); }
	inline String_t* get_ThemeManifestUrl_4() const { return ___ThemeManifestUrl_4; }
	inline String_t** get_address_of_ThemeManifestUrl_4() { return &___ThemeManifestUrl_4; }
	inline void set_ThemeManifestUrl_4(String_t* value)
	{
		___ThemeManifestUrl_4 = value;
		Il2CppCodeGenWriteBarrier((&___ThemeManifestUrl_4), value);
	}

	inline static int32_t get_offset_of_Collisions_5() { return static_cast<int32_t>(offsetof(ConfigParams_t592212086, ___Collisions_5)); }
	inline CollisionConfig_t3653208188  get_Collisions_5() const { return ___Collisions_5; }
	inline CollisionConfig_t3653208188 * get_address_of_Collisions_5() { return &___Collisions_5; }
	inline void set_Collisions_5(CollisionConfig_t3653208188  value)
	{
		___Collisions_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Wrld.ConfigParams
struct ConfigParams_t592212086_marshaled_pinvoke
{
	NativeConfig_t18506486_marshaled_pinvoke ___m_nativeConfig_0;
	char* ___MaterialsDirectory_1;
	Material_t340375123 * ___OverrideLandmarkMaterial_2;
	char* ___CoverageTreeManifestUrl_3;
	char* ___ThemeManifestUrl_4;
	CollisionConfig_t3653208188_marshaled_pinvoke ___Collisions_5;
};
// Native definition for COM marshalling of Wrld.ConfigParams
struct ConfigParams_t592212086_marshaled_com
{
	NativeConfig_t18506486_marshaled_com ___m_nativeConfig_0;
	Il2CppChar* ___MaterialsDirectory_1;
	Material_t340375123 * ___OverrideLandmarkMaterial_2;
	Il2CppChar* ___CoverageTreeManifestUrl_3;
	Il2CppChar* ___ThemeManifestUrl_4;
	CollisionConfig_t3653208188_marshaled_com ___Collisions_5;
};
#endif // CONFIGPARAMS_T592212086_H
#ifndef NATIVECAMERASTATE_T1948103164_H
#define NATIVECAMERASTATE_T1948103164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.NativeCameraState
struct  NativeCameraState_t1948103164 
{
public:
	// System.Single Wrld.NativeCameraState::nearClipPlaneDistance
	float ___nearClipPlaneDistance_0;
	// System.Single Wrld.NativeCameraState::farClipPlaneDistance
	float ___farClipPlaneDistance_1;
	// System.Single Wrld.NativeCameraState::fieldOfViewDegrees
	float ___fieldOfViewDegrees_2;
	// System.Single Wrld.NativeCameraState::aspect
	float ___aspect_3;
	// System.Single Wrld.NativeCameraState::pitchDegrees
	float ___pitchDegrees_4;
	// System.Single Wrld.NativeCameraState::distanceToInterestPoint
	float ___distanceToInterestPoint_5;
	// Wrld.Common.Maths.DoubleVector3 Wrld.NativeCameraState::originECEF
	DoubleVector3_t761704365  ___originECEF_6;
	// Wrld.Common.Maths.DoubleVector3 Wrld.NativeCameraState::interestPointECEF
	DoubleVector3_t761704365  ___interestPointECEF_7;
	// UnityEngine.Vector3 Wrld.NativeCameraState::interestBasisRightECEF
	Vector3_t3722313464  ___interestBasisRightECEF_8;
	// UnityEngine.Vector3 Wrld.NativeCameraState::interestBasisUpECEF
	Vector3_t3722313464  ___interestBasisUpECEF_9;
	// UnityEngine.Vector3 Wrld.NativeCameraState::interestBasisForwardECEF
	Vector3_t3722313464  ___interestBasisForwardECEF_10;

public:
	inline static int32_t get_offset_of_nearClipPlaneDistance_0() { return static_cast<int32_t>(offsetof(NativeCameraState_t1948103164, ___nearClipPlaneDistance_0)); }
	inline float get_nearClipPlaneDistance_0() const { return ___nearClipPlaneDistance_0; }
	inline float* get_address_of_nearClipPlaneDistance_0() { return &___nearClipPlaneDistance_0; }
	inline void set_nearClipPlaneDistance_0(float value)
	{
		___nearClipPlaneDistance_0 = value;
	}

	inline static int32_t get_offset_of_farClipPlaneDistance_1() { return static_cast<int32_t>(offsetof(NativeCameraState_t1948103164, ___farClipPlaneDistance_1)); }
	inline float get_farClipPlaneDistance_1() const { return ___farClipPlaneDistance_1; }
	inline float* get_address_of_farClipPlaneDistance_1() { return &___farClipPlaneDistance_1; }
	inline void set_farClipPlaneDistance_1(float value)
	{
		___farClipPlaneDistance_1 = value;
	}

	inline static int32_t get_offset_of_fieldOfViewDegrees_2() { return static_cast<int32_t>(offsetof(NativeCameraState_t1948103164, ___fieldOfViewDegrees_2)); }
	inline float get_fieldOfViewDegrees_2() const { return ___fieldOfViewDegrees_2; }
	inline float* get_address_of_fieldOfViewDegrees_2() { return &___fieldOfViewDegrees_2; }
	inline void set_fieldOfViewDegrees_2(float value)
	{
		___fieldOfViewDegrees_2 = value;
	}

	inline static int32_t get_offset_of_aspect_3() { return static_cast<int32_t>(offsetof(NativeCameraState_t1948103164, ___aspect_3)); }
	inline float get_aspect_3() const { return ___aspect_3; }
	inline float* get_address_of_aspect_3() { return &___aspect_3; }
	inline void set_aspect_3(float value)
	{
		___aspect_3 = value;
	}

	inline static int32_t get_offset_of_pitchDegrees_4() { return static_cast<int32_t>(offsetof(NativeCameraState_t1948103164, ___pitchDegrees_4)); }
	inline float get_pitchDegrees_4() const { return ___pitchDegrees_4; }
	inline float* get_address_of_pitchDegrees_4() { return &___pitchDegrees_4; }
	inline void set_pitchDegrees_4(float value)
	{
		___pitchDegrees_4 = value;
	}

	inline static int32_t get_offset_of_distanceToInterestPoint_5() { return static_cast<int32_t>(offsetof(NativeCameraState_t1948103164, ___distanceToInterestPoint_5)); }
	inline float get_distanceToInterestPoint_5() const { return ___distanceToInterestPoint_5; }
	inline float* get_address_of_distanceToInterestPoint_5() { return &___distanceToInterestPoint_5; }
	inline void set_distanceToInterestPoint_5(float value)
	{
		___distanceToInterestPoint_5 = value;
	}

	inline static int32_t get_offset_of_originECEF_6() { return static_cast<int32_t>(offsetof(NativeCameraState_t1948103164, ___originECEF_6)); }
	inline DoubleVector3_t761704365  get_originECEF_6() const { return ___originECEF_6; }
	inline DoubleVector3_t761704365 * get_address_of_originECEF_6() { return &___originECEF_6; }
	inline void set_originECEF_6(DoubleVector3_t761704365  value)
	{
		___originECEF_6 = value;
	}

	inline static int32_t get_offset_of_interestPointECEF_7() { return static_cast<int32_t>(offsetof(NativeCameraState_t1948103164, ___interestPointECEF_7)); }
	inline DoubleVector3_t761704365  get_interestPointECEF_7() const { return ___interestPointECEF_7; }
	inline DoubleVector3_t761704365 * get_address_of_interestPointECEF_7() { return &___interestPointECEF_7; }
	inline void set_interestPointECEF_7(DoubleVector3_t761704365  value)
	{
		___interestPointECEF_7 = value;
	}

	inline static int32_t get_offset_of_interestBasisRightECEF_8() { return static_cast<int32_t>(offsetof(NativeCameraState_t1948103164, ___interestBasisRightECEF_8)); }
	inline Vector3_t3722313464  get_interestBasisRightECEF_8() const { return ___interestBasisRightECEF_8; }
	inline Vector3_t3722313464 * get_address_of_interestBasisRightECEF_8() { return &___interestBasisRightECEF_8; }
	inline void set_interestBasisRightECEF_8(Vector3_t3722313464  value)
	{
		___interestBasisRightECEF_8 = value;
	}

	inline static int32_t get_offset_of_interestBasisUpECEF_9() { return static_cast<int32_t>(offsetof(NativeCameraState_t1948103164, ___interestBasisUpECEF_9)); }
	inline Vector3_t3722313464  get_interestBasisUpECEF_9() const { return ___interestBasisUpECEF_9; }
	inline Vector3_t3722313464 * get_address_of_interestBasisUpECEF_9() { return &___interestBasisUpECEF_9; }
	inline void set_interestBasisUpECEF_9(Vector3_t3722313464  value)
	{
		___interestBasisUpECEF_9 = value;
	}

	inline static int32_t get_offset_of_interestBasisForwardECEF_10() { return static_cast<int32_t>(offsetof(NativeCameraState_t1948103164, ___interestBasisForwardECEF_10)); }
	inline Vector3_t3722313464  get_interestBasisForwardECEF_10() const { return ___interestBasisForwardECEF_10; }
	inline Vector3_t3722313464 * get_address_of_interestBasisForwardECEF_10() { return &___interestBasisForwardECEF_10; }
	inline void set_interestBasisForwardECEF_10(Vector3_t3722313464  value)
	{
		___interestBasisForwardECEF_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVECAMERASTATE_T1948103164_H
#ifndef TOUCHDATA_T718366571_H
#define TOUCHDATA_T718366571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.AppInterface.TouchData
struct  TouchData_t718366571 
{
public:
	// UnityEngine.Vector2 Wrld.MapInput.AppInterface.TouchData::point
	Vector2_t2156229523  ___point_0;

public:
	inline static int32_t get_offset_of_point_0() { return static_cast<int32_t>(offsetof(TouchData_t718366571, ___point_0)); }
	inline Vector2_t2156229523  get_point_0() const { return ___point_0; }
	inline Vector2_t2156229523 * get_address_of_point_0() { return &___point_0; }
	inline void set_point_0(Vector2_t2156229523  value)
	{
		___point_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHDATA_T718366571_H
#ifndef TOUCHHELDDATA_T1271796919_H
#define TOUCHHELDDATA_T1271796919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.AppInterface.TouchHeldData
struct  TouchHeldData_t1271796919 
{
public:
	// UnityEngine.Vector2 Wrld.MapInput.AppInterface.TouchHeldData::point
	Vector2_t2156229523  ___point_0;

public:
	inline static int32_t get_offset_of_point_0() { return static_cast<int32_t>(offsetof(TouchHeldData_t1271796919, ___point_0)); }
	inline Vector2_t2156229523  get_point_0() const { return ___point_0; }
	inline Vector2_t2156229523 * get_address_of_point_0() { return &___point_0; }
	inline void set_point_0(Vector2_t2156229523  value)
	{
		___point_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHHELDDATA_T1271796919_H
#ifndef INPUTFRAME_T648463210_H
#define INPUTFRAME_T648463210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapCamera.CameraInputHandler/InputFrame
struct  InputFrame_t648463210 
{
public:
	// UnityEngine.Touch[] Wrld.MapCamera.CameraInputHandler/InputFrame::Touches
	TouchU5BU5D_t1849554061* ___Touches_0;
	// UnityEngine.Vector3 Wrld.MapCamera.CameraInputHandler/InputFrame::MousePosition
	Vector3_t3722313464  ___MousePosition_1;
	// System.Boolean Wrld.MapCamera.CameraInputHandler/InputFrame::IsLeftDown
	bool ___IsLeftDown_2;
	// System.Boolean Wrld.MapCamera.CameraInputHandler/InputFrame::IsLeftUp
	bool ___IsLeftUp_3;
	// System.Boolean Wrld.MapCamera.CameraInputHandler/InputFrame::IsRightDown
	bool ___IsRightDown_4;
	// System.Boolean Wrld.MapCamera.CameraInputHandler/InputFrame::IsRightUp
	bool ___IsRightUp_5;
	// System.Boolean Wrld.MapCamera.CameraInputHandler/InputFrame::IsMiddleDown
	bool ___IsMiddleDown_6;
	// System.Boolean Wrld.MapCamera.CameraInputHandler/InputFrame::IsMiddleUp
	bool ___IsMiddleUp_7;
	// System.Single Wrld.MapCamera.CameraInputHandler/InputFrame::MouseXDelta
	float ___MouseXDelta_8;
	// System.Single Wrld.MapCamera.CameraInputHandler/InputFrame::MouseYDelta
	float ___MouseYDelta_9;
	// System.Single Wrld.MapCamera.CameraInputHandler/InputFrame::MouseWheelDelta
	float ___MouseWheelDelta_10;
	// System.Boolean Wrld.MapCamera.CameraInputHandler/InputFrame::HasMouseMoved
	bool ___HasMouseMoved_11;

public:
	inline static int32_t get_offset_of_Touches_0() { return static_cast<int32_t>(offsetof(InputFrame_t648463210, ___Touches_0)); }
	inline TouchU5BU5D_t1849554061* get_Touches_0() const { return ___Touches_0; }
	inline TouchU5BU5D_t1849554061** get_address_of_Touches_0() { return &___Touches_0; }
	inline void set_Touches_0(TouchU5BU5D_t1849554061* value)
	{
		___Touches_0 = value;
		Il2CppCodeGenWriteBarrier((&___Touches_0), value);
	}

	inline static int32_t get_offset_of_MousePosition_1() { return static_cast<int32_t>(offsetof(InputFrame_t648463210, ___MousePosition_1)); }
	inline Vector3_t3722313464  get_MousePosition_1() const { return ___MousePosition_1; }
	inline Vector3_t3722313464 * get_address_of_MousePosition_1() { return &___MousePosition_1; }
	inline void set_MousePosition_1(Vector3_t3722313464  value)
	{
		___MousePosition_1 = value;
	}

	inline static int32_t get_offset_of_IsLeftDown_2() { return static_cast<int32_t>(offsetof(InputFrame_t648463210, ___IsLeftDown_2)); }
	inline bool get_IsLeftDown_2() const { return ___IsLeftDown_2; }
	inline bool* get_address_of_IsLeftDown_2() { return &___IsLeftDown_2; }
	inline void set_IsLeftDown_2(bool value)
	{
		___IsLeftDown_2 = value;
	}

	inline static int32_t get_offset_of_IsLeftUp_3() { return static_cast<int32_t>(offsetof(InputFrame_t648463210, ___IsLeftUp_3)); }
	inline bool get_IsLeftUp_3() const { return ___IsLeftUp_3; }
	inline bool* get_address_of_IsLeftUp_3() { return &___IsLeftUp_3; }
	inline void set_IsLeftUp_3(bool value)
	{
		___IsLeftUp_3 = value;
	}

	inline static int32_t get_offset_of_IsRightDown_4() { return static_cast<int32_t>(offsetof(InputFrame_t648463210, ___IsRightDown_4)); }
	inline bool get_IsRightDown_4() const { return ___IsRightDown_4; }
	inline bool* get_address_of_IsRightDown_4() { return &___IsRightDown_4; }
	inline void set_IsRightDown_4(bool value)
	{
		___IsRightDown_4 = value;
	}

	inline static int32_t get_offset_of_IsRightUp_5() { return static_cast<int32_t>(offsetof(InputFrame_t648463210, ___IsRightUp_5)); }
	inline bool get_IsRightUp_5() const { return ___IsRightUp_5; }
	inline bool* get_address_of_IsRightUp_5() { return &___IsRightUp_5; }
	inline void set_IsRightUp_5(bool value)
	{
		___IsRightUp_5 = value;
	}

	inline static int32_t get_offset_of_IsMiddleDown_6() { return static_cast<int32_t>(offsetof(InputFrame_t648463210, ___IsMiddleDown_6)); }
	inline bool get_IsMiddleDown_6() const { return ___IsMiddleDown_6; }
	inline bool* get_address_of_IsMiddleDown_6() { return &___IsMiddleDown_6; }
	inline void set_IsMiddleDown_6(bool value)
	{
		___IsMiddleDown_6 = value;
	}

	inline static int32_t get_offset_of_IsMiddleUp_7() { return static_cast<int32_t>(offsetof(InputFrame_t648463210, ___IsMiddleUp_7)); }
	inline bool get_IsMiddleUp_7() const { return ___IsMiddleUp_7; }
	inline bool* get_address_of_IsMiddleUp_7() { return &___IsMiddleUp_7; }
	inline void set_IsMiddleUp_7(bool value)
	{
		___IsMiddleUp_7 = value;
	}

	inline static int32_t get_offset_of_MouseXDelta_8() { return static_cast<int32_t>(offsetof(InputFrame_t648463210, ___MouseXDelta_8)); }
	inline float get_MouseXDelta_8() const { return ___MouseXDelta_8; }
	inline float* get_address_of_MouseXDelta_8() { return &___MouseXDelta_8; }
	inline void set_MouseXDelta_8(float value)
	{
		___MouseXDelta_8 = value;
	}

	inline static int32_t get_offset_of_MouseYDelta_9() { return static_cast<int32_t>(offsetof(InputFrame_t648463210, ___MouseYDelta_9)); }
	inline float get_MouseYDelta_9() const { return ___MouseYDelta_9; }
	inline float* get_address_of_MouseYDelta_9() { return &___MouseYDelta_9; }
	inline void set_MouseYDelta_9(float value)
	{
		___MouseYDelta_9 = value;
	}

	inline static int32_t get_offset_of_MouseWheelDelta_10() { return static_cast<int32_t>(offsetof(InputFrame_t648463210, ___MouseWheelDelta_10)); }
	inline float get_MouseWheelDelta_10() const { return ___MouseWheelDelta_10; }
	inline float* get_address_of_MouseWheelDelta_10() { return &___MouseWheelDelta_10; }
	inline void set_MouseWheelDelta_10(float value)
	{
		___MouseWheelDelta_10 = value;
	}

	inline static int32_t get_offset_of_HasMouseMoved_11() { return static_cast<int32_t>(offsetof(InputFrame_t648463210, ___HasMouseMoved_11)); }
	inline bool get_HasMouseMoved_11() const { return ___HasMouseMoved_11; }
	inline bool* get_address_of_HasMouseMoved_11() { return &___HasMouseMoved_11; }
	inline void set_HasMouseMoved_11(bool value)
	{
		___HasMouseMoved_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Wrld.MapCamera.CameraInputHandler/InputFrame
struct InputFrame_t648463210_marshaled_pinvoke
{
	Touch_t1921856868 * ___Touches_0;
	Vector3_t3722313464  ___MousePosition_1;
	int32_t ___IsLeftDown_2;
	int32_t ___IsLeftUp_3;
	int32_t ___IsRightDown_4;
	int32_t ___IsRightUp_5;
	int32_t ___IsMiddleDown_6;
	int32_t ___IsMiddleUp_7;
	float ___MouseXDelta_8;
	float ___MouseYDelta_9;
	float ___MouseWheelDelta_10;
	int32_t ___HasMouseMoved_11;
};
// Native definition for COM marshalling of Wrld.MapCamera.CameraInputHandler/InputFrame
struct InputFrame_t648463210_marshaled_com
{
	Touch_t1921856868 * ___Touches_0;
	Vector3_t3722313464  ___MousePosition_1;
	int32_t ___IsLeftDown_2;
	int32_t ___IsLeftUp_3;
	int32_t ___IsRightDown_4;
	int32_t ___IsRightUp_5;
	int32_t ___IsMiddleDown_6;
	int32_t ___IsMiddleUp_7;
	float ___MouseXDelta_8;
	float ___MouseYDelta_9;
	float ___MouseWheelDelta_10;
	int32_t ___HasMouseMoved_11;
};
#endif // INPUTFRAME_T648463210_H
#ifndef TAPDATA_T745490888_H
#define TAPDATA_T745490888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.AppInterface.TapData
struct  TapData_t745490888 
{
public:
	// UnityEngine.Vector2 Wrld.MapInput.AppInterface.TapData::point
	Vector2_t2156229523  ___point_0;

public:
	inline static int32_t get_offset_of_point_0() { return static_cast<int32_t>(offsetof(TapData_t745490888, ___point_0)); }
	inline Vector2_t2156229523  get_point_0() const { return ___point_0; }
	inline Vector2_t2156229523 * get_address_of_point_0() { return &___point_0; }
	inline void set_point_0(Vector2_t2156229523  value)
	{
		___point_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPDATA_T745490888_H
#ifndef CAMERAEVENTTYPE_T2774415872_H
#define CAMERAEVENTTYPE_T2774415872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapCamera.CameraApi/CameraEventType
struct  CameraEventType_t2774415872 
{
public:
	// System.Int32 Wrld.MapCamera.CameraApi/CameraEventType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraEventType_t2774415872, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAEVENTTYPE_T2774415872_H
#ifndef PANDATA_T3291826632_H
#define PANDATA_T3291826632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.AppInterface.PanData
struct  PanData_t3291826632 
{
public:
	// UnityEngine.Vector2 Wrld.MapInput.AppInterface.PanData::pointRelative
	Vector2_t2156229523  ___pointRelative_0;
	// UnityEngine.Vector2 Wrld.MapInput.AppInterface.PanData::pointAbsolute
	Vector2_t2156229523  ___pointAbsolute_1;
	// UnityEngine.Vector2 Wrld.MapInput.AppInterface.PanData::pointRelativeNormalized
	Vector2_t2156229523  ___pointRelativeNormalized_2;
	// UnityEngine.Vector2 Wrld.MapInput.AppInterface.PanData::velocity
	Vector2_t2156229523  ___velocity_3;
	// UnityEngine.Vector2 Wrld.MapInput.AppInterface.PanData::touchExtents
	Vector2_t2156229523  ___touchExtents_4;
	// System.Int32 Wrld.MapInput.AppInterface.PanData::numTouches
	int32_t ___numTouches_5;
	// System.Single Wrld.MapInput.AppInterface.PanData::majorScreenDimension
	float ___majorScreenDimension_6;

public:
	inline static int32_t get_offset_of_pointRelative_0() { return static_cast<int32_t>(offsetof(PanData_t3291826632, ___pointRelative_0)); }
	inline Vector2_t2156229523  get_pointRelative_0() const { return ___pointRelative_0; }
	inline Vector2_t2156229523 * get_address_of_pointRelative_0() { return &___pointRelative_0; }
	inline void set_pointRelative_0(Vector2_t2156229523  value)
	{
		___pointRelative_0 = value;
	}

	inline static int32_t get_offset_of_pointAbsolute_1() { return static_cast<int32_t>(offsetof(PanData_t3291826632, ___pointAbsolute_1)); }
	inline Vector2_t2156229523  get_pointAbsolute_1() const { return ___pointAbsolute_1; }
	inline Vector2_t2156229523 * get_address_of_pointAbsolute_1() { return &___pointAbsolute_1; }
	inline void set_pointAbsolute_1(Vector2_t2156229523  value)
	{
		___pointAbsolute_1 = value;
	}

	inline static int32_t get_offset_of_pointRelativeNormalized_2() { return static_cast<int32_t>(offsetof(PanData_t3291826632, ___pointRelativeNormalized_2)); }
	inline Vector2_t2156229523  get_pointRelativeNormalized_2() const { return ___pointRelativeNormalized_2; }
	inline Vector2_t2156229523 * get_address_of_pointRelativeNormalized_2() { return &___pointRelativeNormalized_2; }
	inline void set_pointRelativeNormalized_2(Vector2_t2156229523  value)
	{
		___pointRelativeNormalized_2 = value;
	}

	inline static int32_t get_offset_of_velocity_3() { return static_cast<int32_t>(offsetof(PanData_t3291826632, ___velocity_3)); }
	inline Vector2_t2156229523  get_velocity_3() const { return ___velocity_3; }
	inline Vector2_t2156229523 * get_address_of_velocity_3() { return &___velocity_3; }
	inline void set_velocity_3(Vector2_t2156229523  value)
	{
		___velocity_3 = value;
	}

	inline static int32_t get_offset_of_touchExtents_4() { return static_cast<int32_t>(offsetof(PanData_t3291826632, ___touchExtents_4)); }
	inline Vector2_t2156229523  get_touchExtents_4() const { return ___touchExtents_4; }
	inline Vector2_t2156229523 * get_address_of_touchExtents_4() { return &___touchExtents_4; }
	inline void set_touchExtents_4(Vector2_t2156229523  value)
	{
		___touchExtents_4 = value;
	}

	inline static int32_t get_offset_of_numTouches_5() { return static_cast<int32_t>(offsetof(PanData_t3291826632, ___numTouches_5)); }
	inline int32_t get_numTouches_5() const { return ___numTouches_5; }
	inline int32_t* get_address_of_numTouches_5() { return &___numTouches_5; }
	inline void set_numTouches_5(int32_t value)
	{
		___numTouches_5 = value;
	}

	inline static int32_t get_offset_of_majorScreenDimension_6() { return static_cast<int32_t>(offsetof(PanData_t3291826632, ___majorScreenDimension_6)); }
	inline float get_majorScreenDimension_6() const { return ___majorScreenDimension_6; }
	inline float* get_address_of_majorScreenDimension_6() { return &___majorScreenDimension_6; }
	inline void set_majorScreenDimension_6(float value)
	{
		___majorScreenDimension_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PANDATA_T3291826632_H
#ifndef MOUSEINPUTACTION_T556709297_H
#define MOUSEINPUTACTION_T556709297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.Mouse.MouseInputAction
struct  MouseInputAction_t556709297 
{
public:
	// System.Int32 Wrld.MapInput.Mouse.MouseInputAction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MouseInputAction_t556709297, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEINPUTACTION_T556709297_H
#ifndef INTERESTPOINTPROVIDER_T3426197040_H
#define INTERESTPOINTPROVIDER_T3426197040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapCamera.InterestPointProvider
struct  InterestPointProvider_t3426197040  : public RuntimeObject
{
public:
	// Wrld.Common.Maths.DoubleVector3 Wrld.MapCamera.InterestPointProvider::m_interestPointECEF
	DoubleVector3_t761704365  ___m_interestPointECEF_0;
	// System.Boolean Wrld.MapCamera.InterestPointProvider::m_hasInterestPointFromNativeController
	bool ___m_hasInterestPointFromNativeController_1;

public:
	inline static int32_t get_offset_of_m_interestPointECEF_0() { return static_cast<int32_t>(offsetof(InterestPointProvider_t3426197040, ___m_interestPointECEF_0)); }
	inline DoubleVector3_t761704365  get_m_interestPointECEF_0() const { return ___m_interestPointECEF_0; }
	inline DoubleVector3_t761704365 * get_address_of_m_interestPointECEF_0() { return &___m_interestPointECEF_0; }
	inline void set_m_interestPointECEF_0(DoubleVector3_t761704365  value)
	{
		___m_interestPointECEF_0 = value;
	}

	inline static int32_t get_offset_of_m_hasInterestPointFromNativeController_1() { return static_cast<int32_t>(offsetof(InterestPointProvider_t3426197040, ___m_hasInterestPointFromNativeController_1)); }
	inline bool get_m_hasInterestPointFromNativeController_1() const { return ___m_hasInterestPointFromNativeController_1; }
	inline bool* get_address_of_m_hasInterestPointFromNativeController_1() { return &___m_hasInterestPointFromNativeController_1; }
	inline void set_m_hasInterestPointFromNativeController_1(bool value)
	{
		___m_hasInterestPointFromNativeController_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERESTPOINTPROVIDER_T3426197040_H
#ifndef MOUSEPANGESTURE_T639745302_H
#define MOUSEPANGESTURE_T639745302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.Mouse.MousePanGesture
struct  MousePanGesture_t639745302  : public RuntimeObject
{
public:
	// Wrld.MapInput.IUnityInputHandler Wrld.MapInput.Mouse.MousePanGesture::m_handler
	RuntimeObject* ___m_handler_0;
	// System.Boolean Wrld.MapInput.Mouse.MousePanGesture::m_panButtonDown
	bool ___m_panButtonDown_1;
	// System.Boolean Wrld.MapInput.Mouse.MousePanGesture::m_panning
	bool ___m_panning_2;
	// UnityEngine.Vector2 Wrld.MapInput.Mouse.MousePanGesture::m_current
	Vector2_t2156229523  ___m_current_3;
	// UnityEngine.Vector2 Wrld.MapInput.Mouse.MousePanGesture::m_anchor
	Vector2_t2156229523  ___m_anchor_4;
	// System.Single Wrld.MapInput.Mouse.MousePanGesture::majorScreenDimension
	float ___majorScreenDimension_5;

public:
	inline static int32_t get_offset_of_m_handler_0() { return static_cast<int32_t>(offsetof(MousePanGesture_t639745302, ___m_handler_0)); }
	inline RuntimeObject* get_m_handler_0() const { return ___m_handler_0; }
	inline RuntimeObject** get_address_of_m_handler_0() { return &___m_handler_0; }
	inline void set_m_handler_0(RuntimeObject* value)
	{
		___m_handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_handler_0), value);
	}

	inline static int32_t get_offset_of_m_panButtonDown_1() { return static_cast<int32_t>(offsetof(MousePanGesture_t639745302, ___m_panButtonDown_1)); }
	inline bool get_m_panButtonDown_1() const { return ___m_panButtonDown_1; }
	inline bool* get_address_of_m_panButtonDown_1() { return &___m_panButtonDown_1; }
	inline void set_m_panButtonDown_1(bool value)
	{
		___m_panButtonDown_1 = value;
	}

	inline static int32_t get_offset_of_m_panning_2() { return static_cast<int32_t>(offsetof(MousePanGesture_t639745302, ___m_panning_2)); }
	inline bool get_m_panning_2() const { return ___m_panning_2; }
	inline bool* get_address_of_m_panning_2() { return &___m_panning_2; }
	inline void set_m_panning_2(bool value)
	{
		___m_panning_2 = value;
	}

	inline static int32_t get_offset_of_m_current_3() { return static_cast<int32_t>(offsetof(MousePanGesture_t639745302, ___m_current_3)); }
	inline Vector2_t2156229523  get_m_current_3() const { return ___m_current_3; }
	inline Vector2_t2156229523 * get_address_of_m_current_3() { return &___m_current_3; }
	inline void set_m_current_3(Vector2_t2156229523  value)
	{
		___m_current_3 = value;
	}

	inline static int32_t get_offset_of_m_anchor_4() { return static_cast<int32_t>(offsetof(MousePanGesture_t639745302, ___m_anchor_4)); }
	inline Vector2_t2156229523  get_m_anchor_4() const { return ___m_anchor_4; }
	inline Vector2_t2156229523 * get_address_of_m_anchor_4() { return &___m_anchor_4; }
	inline void set_m_anchor_4(Vector2_t2156229523  value)
	{
		___m_anchor_4 = value;
	}

	inline static int32_t get_offset_of_majorScreenDimension_5() { return static_cast<int32_t>(offsetof(MousePanGesture_t639745302, ___majorScreenDimension_5)); }
	inline float get_majorScreenDimension_5() const { return ___majorScreenDimension_5; }
	inline float* get_address_of_majorScreenDimension_5() { return &___majorScreenDimension_5; }
	inline void set_majorScreenDimension_5(float value)
	{
		___majorScreenDimension_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEPANGESTURE_T639745302_H
#ifndef API_T1190036922_H
#define API_T1190036922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Api
struct  Api_t1190036922  : public RuntimeObject
{
public:
	// Wrld.ConfigParams/CollisionConfig Wrld.Api::CollisionStates
	CollisionConfig_t3653208188  ___CollisionStates_1;
	// Wrld.ApiImplementation Wrld.Api::m_implementation
	ApiImplementation_t1854519848 * ___m_implementation_4;

public:
	inline static int32_t get_offset_of_CollisionStates_1() { return static_cast<int32_t>(offsetof(Api_t1190036922, ___CollisionStates_1)); }
	inline CollisionConfig_t3653208188  get_CollisionStates_1() const { return ___CollisionStates_1; }
	inline CollisionConfig_t3653208188 * get_address_of_CollisionStates_1() { return &___CollisionStates_1; }
	inline void set_CollisionStates_1(CollisionConfig_t3653208188  value)
	{
		___CollisionStates_1 = value;
	}

	inline static int32_t get_offset_of_m_implementation_4() { return static_cast<int32_t>(offsetof(Api_t1190036922, ___m_implementation_4)); }
	inline ApiImplementation_t1854519848 * get_m_implementation_4() const { return ___m_implementation_4; }
	inline ApiImplementation_t1854519848 ** get_address_of_m_implementation_4() { return &___m_implementation_4; }
	inline void set_m_implementation_4(ApiImplementation_t1854519848 * value)
	{
		___m_implementation_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_implementation_4), value);
	}
};

struct Api_t1190036922_StaticFields
{
public:
	// Wrld.Api Wrld.Api::Instance
	Api_t1190036922 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(Api_t1190036922_StaticFields, ___Instance_0)); }
	inline Api_t1190036922 * get_Instance_0() const { return ___Instance_0; }
	inline Api_t1190036922 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(Api_t1190036922 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // API_T1190036922_H
#ifndef COORDINATESYSTEM_T2504309499_H
#define COORDINATESYSTEM_T2504309499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.CoordinateSystem
struct  CoordinateSystem_t2504309499 
{
public:
	// System.Int32 Wrld.CoordinateSystem::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CoordinateSystem_t2504309499, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COORDINATESYSTEM_T2504309499_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef UNITYARMATRIX4X4_T4073345847_H
#define UNITYARMATRIX4X4_T4073345847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARMatrix4x4
struct  UnityARMatrix4x4_t4073345847 
{
public:
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column0
	Vector4_t3319028937  ___column0_0;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column1
	Vector4_t3319028937  ___column1_1;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column2
	Vector4_t3319028937  ___column2_2;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARMatrix4x4::column3
	Vector4_t3319028937  ___column3_3;

public:
	inline static int32_t get_offset_of_column0_0() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t4073345847, ___column0_0)); }
	inline Vector4_t3319028937  get_column0_0() const { return ___column0_0; }
	inline Vector4_t3319028937 * get_address_of_column0_0() { return &___column0_0; }
	inline void set_column0_0(Vector4_t3319028937  value)
	{
		___column0_0 = value;
	}

	inline static int32_t get_offset_of_column1_1() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t4073345847, ___column1_1)); }
	inline Vector4_t3319028937  get_column1_1() const { return ___column1_1; }
	inline Vector4_t3319028937 * get_address_of_column1_1() { return &___column1_1; }
	inline void set_column1_1(Vector4_t3319028937  value)
	{
		___column1_1 = value;
	}

	inline static int32_t get_offset_of_column2_2() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t4073345847, ___column2_2)); }
	inline Vector4_t3319028937  get_column2_2() const { return ___column2_2; }
	inline Vector4_t3319028937 * get_address_of_column2_2() { return &___column2_2; }
	inline void set_column2_2(Vector4_t3319028937  value)
	{
		___column2_2 = value;
	}

	inline static int32_t get_offset_of_column3_3() { return static_cast<int32_t>(offsetof(UnityARMatrix4x4_t4073345847, ___column3_3)); }
	inline Vector4_t3319028937  get_column3_3() const { return ___column3_3; }
	inline Vector4_t3319028937 * get_address_of_column3_3() { return &___column3_3; }
	inline void set_column3_3(Vector4_t3319028937  value)
	{
		___column3_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARMATRIX4X4_T4073345847_H
#ifndef ARTRACKINGSTATE_T3182235352_H
#define ARTRACKINGSTATE_T3182235352_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingState
struct  ARTrackingState_t3182235352 
{
public:
	// System.Int32 UnityEngine.XR.iOS.ARTrackingState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARTrackingState_t3182235352, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGSTATE_T3182235352_H
#ifndef ARTRACKINGSTATEREASON_T2348933773_H
#define ARTRACKINGSTATEREASON_T2348933773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARTrackingStateReason
struct  ARTrackingStateReason_t2348933773 
{
public:
	// System.Int32 UnityEngine.XR.iOS.ARTrackingStateReason::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARTrackingStateReason_t2348933773, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARTRACKINGSTATEREASON_T2348933773_H
#ifndef MARSHALDIRECTIONALLIGHTESTIMATE_T3803901471_H
#define MARSHALDIRECTIONALLIGHTESTIMATE_T3803901471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.MarshalDirectionalLightEstimate
struct  MarshalDirectionalLightEstimate_t3803901471 
{
public:
	// UnityEngine.Vector4 UnityEngine.XR.iOS.MarshalDirectionalLightEstimate::primaryDirAndIntensity
	Vector4_t3319028937  ___primaryDirAndIntensity_0;
	// System.IntPtr UnityEngine.XR.iOS.MarshalDirectionalLightEstimate::sphericalHarmonicCoefficientsPtr
	intptr_t ___sphericalHarmonicCoefficientsPtr_1;

public:
	inline static int32_t get_offset_of_primaryDirAndIntensity_0() { return static_cast<int32_t>(offsetof(MarshalDirectionalLightEstimate_t3803901471, ___primaryDirAndIntensity_0)); }
	inline Vector4_t3319028937  get_primaryDirAndIntensity_0() const { return ___primaryDirAndIntensity_0; }
	inline Vector4_t3319028937 * get_address_of_primaryDirAndIntensity_0() { return &___primaryDirAndIntensity_0; }
	inline void set_primaryDirAndIntensity_0(Vector4_t3319028937  value)
	{
		___primaryDirAndIntensity_0 = value;
	}

	inline static int32_t get_offset_of_sphericalHarmonicCoefficientsPtr_1() { return static_cast<int32_t>(offsetof(MarshalDirectionalLightEstimate_t3803901471, ___sphericalHarmonicCoefficientsPtr_1)); }
	inline intptr_t get_sphericalHarmonicCoefficientsPtr_1() const { return ___sphericalHarmonicCoefficientsPtr_1; }
	inline intptr_t* get_address_of_sphericalHarmonicCoefficientsPtr_1() { return &___sphericalHarmonicCoefficientsPtr_1; }
	inline void set_sphericalHarmonicCoefficientsPtr_1(intptr_t value)
	{
		___sphericalHarmonicCoefficientsPtr_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALDIRECTIONALLIGHTESTIMATE_T3803901471_H
#ifndef LIGHTDATATYPE_T2323651587_H
#define LIGHTDATATYPE_T2323651587_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.LightDataType
struct  LightDataType_t2323651587 
{
public:
	// System.Int32 UnityEngine.XR.iOS.LightDataType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LightDataType_t2323651587, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTDATATYPE_T2323651587_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef UNITYARFACEGEOMETRY_T4178775532_H
#define UNITYARFACEGEOMETRY_T4178775532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARFaceGeometry
struct  UnityARFaceGeometry_t4178775532 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityARFaceGeometry::vertexCount
	int32_t ___vertexCount_0;
	// System.IntPtr UnityEngine.XR.iOS.UnityARFaceGeometry::vertices
	intptr_t ___vertices_1;
	// System.Int32 UnityEngine.XR.iOS.UnityARFaceGeometry::textureCoordinateCount
	int32_t ___textureCoordinateCount_2;
	// System.IntPtr UnityEngine.XR.iOS.UnityARFaceGeometry::textureCoordinates
	intptr_t ___textureCoordinates_3;
	// System.Int32 UnityEngine.XR.iOS.UnityARFaceGeometry::triangleCount
	int32_t ___triangleCount_4;
	// System.IntPtr UnityEngine.XR.iOS.UnityARFaceGeometry::triangleIndices
	intptr_t ___triangleIndices_5;

public:
	inline static int32_t get_offset_of_vertexCount_0() { return static_cast<int32_t>(offsetof(UnityARFaceGeometry_t4178775532, ___vertexCount_0)); }
	inline int32_t get_vertexCount_0() const { return ___vertexCount_0; }
	inline int32_t* get_address_of_vertexCount_0() { return &___vertexCount_0; }
	inline void set_vertexCount_0(int32_t value)
	{
		___vertexCount_0 = value;
	}

	inline static int32_t get_offset_of_vertices_1() { return static_cast<int32_t>(offsetof(UnityARFaceGeometry_t4178775532, ___vertices_1)); }
	inline intptr_t get_vertices_1() const { return ___vertices_1; }
	inline intptr_t* get_address_of_vertices_1() { return &___vertices_1; }
	inline void set_vertices_1(intptr_t value)
	{
		___vertices_1 = value;
	}

	inline static int32_t get_offset_of_textureCoordinateCount_2() { return static_cast<int32_t>(offsetof(UnityARFaceGeometry_t4178775532, ___textureCoordinateCount_2)); }
	inline int32_t get_textureCoordinateCount_2() const { return ___textureCoordinateCount_2; }
	inline int32_t* get_address_of_textureCoordinateCount_2() { return &___textureCoordinateCount_2; }
	inline void set_textureCoordinateCount_2(int32_t value)
	{
		___textureCoordinateCount_2 = value;
	}

	inline static int32_t get_offset_of_textureCoordinates_3() { return static_cast<int32_t>(offsetof(UnityARFaceGeometry_t4178775532, ___textureCoordinates_3)); }
	inline intptr_t get_textureCoordinates_3() const { return ___textureCoordinates_3; }
	inline intptr_t* get_address_of_textureCoordinates_3() { return &___textureCoordinates_3; }
	inline void set_textureCoordinates_3(intptr_t value)
	{
		___textureCoordinates_3 = value;
	}

	inline static int32_t get_offset_of_triangleCount_4() { return static_cast<int32_t>(offsetof(UnityARFaceGeometry_t4178775532, ___triangleCount_4)); }
	inline int32_t get_triangleCount_4() const { return ___triangleCount_4; }
	inline int32_t* get_address_of_triangleCount_4() { return &___triangleCount_4; }
	inline void set_triangleCount_4(int32_t value)
	{
		___triangleCount_4 = value;
	}

	inline static int32_t get_offset_of_triangleIndices_5() { return static_cast<int32_t>(offsetof(UnityARFaceGeometry_t4178775532, ___triangleIndices_5)); }
	inline intptr_t get_triangleIndices_5() const { return ___triangleIndices_5; }
	inline intptr_t* get_address_of_triangleIndices_5() { return &___triangleIndices_5; }
	inline void set_triangleIndices_5(intptr_t value)
	{
		___triangleIndices_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARFACEGEOMETRY_T4178775532_H
#ifndef ARPLANEANCHORALIGNMENT_T2311256121_H
#define ARPLANEANCHORALIGNMENT_T2311256121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARPlaneAnchorAlignment
struct  ARPlaneAnchorAlignment_t2311256121 
{
public:
	// System.Int64 UnityEngine.XR.iOS.ARPlaneAnchorAlignment::value__
	int64_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ARPlaneAnchorAlignment_t2311256121, ___value___1)); }
	inline int64_t get_value___1() const { return ___value___1; }
	inline int64_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int64_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARPLANEANCHORALIGNMENT_T2311256121_H
#ifndef UNITYVIDEOPARAMS_T4155354995_H
#define UNITYVIDEOPARAMS_T4155354995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityVideoParams
struct  UnityVideoParams_t4155354995 
{
public:
	// System.Int32 UnityEngine.XR.iOS.UnityVideoParams::yWidth
	int32_t ___yWidth_0;
	// System.Int32 UnityEngine.XR.iOS.UnityVideoParams::yHeight
	int32_t ___yHeight_1;
	// System.Int32 UnityEngine.XR.iOS.UnityVideoParams::screenOrientation
	int32_t ___screenOrientation_2;
	// System.Single UnityEngine.XR.iOS.UnityVideoParams::texCoordScale
	float ___texCoordScale_3;
	// System.IntPtr UnityEngine.XR.iOS.UnityVideoParams::cvPixelBufferPtr
	intptr_t ___cvPixelBufferPtr_4;

public:
	inline static int32_t get_offset_of_yWidth_0() { return static_cast<int32_t>(offsetof(UnityVideoParams_t4155354995, ___yWidth_0)); }
	inline int32_t get_yWidth_0() const { return ___yWidth_0; }
	inline int32_t* get_address_of_yWidth_0() { return &___yWidth_0; }
	inline void set_yWidth_0(int32_t value)
	{
		___yWidth_0 = value;
	}

	inline static int32_t get_offset_of_yHeight_1() { return static_cast<int32_t>(offsetof(UnityVideoParams_t4155354995, ___yHeight_1)); }
	inline int32_t get_yHeight_1() const { return ___yHeight_1; }
	inline int32_t* get_address_of_yHeight_1() { return &___yHeight_1; }
	inline void set_yHeight_1(int32_t value)
	{
		___yHeight_1 = value;
	}

	inline static int32_t get_offset_of_screenOrientation_2() { return static_cast<int32_t>(offsetof(UnityVideoParams_t4155354995, ___screenOrientation_2)); }
	inline int32_t get_screenOrientation_2() const { return ___screenOrientation_2; }
	inline int32_t* get_address_of_screenOrientation_2() { return &___screenOrientation_2; }
	inline void set_screenOrientation_2(int32_t value)
	{
		___screenOrientation_2 = value;
	}

	inline static int32_t get_offset_of_texCoordScale_3() { return static_cast<int32_t>(offsetof(UnityVideoParams_t4155354995, ___texCoordScale_3)); }
	inline float get_texCoordScale_3() const { return ___texCoordScale_3; }
	inline float* get_address_of_texCoordScale_3() { return &___texCoordScale_3; }
	inline void set_texCoordScale_3(float value)
	{
		___texCoordScale_3 = value;
	}

	inline static int32_t get_offset_of_cvPixelBufferPtr_4() { return static_cast<int32_t>(offsetof(UnityVideoParams_t4155354995, ___cvPixelBufferPtr_4)); }
	inline intptr_t get_cvPixelBufferPtr_4() const { return ___cvPixelBufferPtr_4; }
	inline intptr_t* get_address_of_cvPixelBufferPtr_4() { return &___cvPixelBufferPtr_4; }
	inline void set_cvPixelBufferPtr_4(intptr_t value)
	{
		___cvPixelBufferPtr_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYVIDEOPARAMS_T4155354995_H
#ifndef ARUSERANCHOR_T1406831531_H
#define ARUSERANCHOR_T1406831531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.ARUserAnchor
struct  ARUserAnchor_t1406831531 
{
public:
	// System.String UnityEngine.XR.iOS.ARUserAnchor::identifier
	String_t* ___identifier_0;
	// UnityEngine.Matrix4x4 UnityEngine.XR.iOS.ARUserAnchor::transform
	Matrix4x4_t1817901843  ___transform_1;

public:
	inline static int32_t get_offset_of_identifier_0() { return static_cast<int32_t>(offsetof(ARUserAnchor_t1406831531, ___identifier_0)); }
	inline String_t* get_identifier_0() const { return ___identifier_0; }
	inline String_t** get_address_of_identifier_0() { return &___identifier_0; }
	inline void set_identifier_0(String_t* value)
	{
		___identifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_0), value);
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(ARUserAnchor_t1406831531, ___transform_1)); }
	inline Matrix4x4_t1817901843  get_transform_1() const { return ___transform_1; }
	inline Matrix4x4_t1817901843 * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(Matrix4x4_t1817901843  value)
	{
		___transform_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.ARUserAnchor
struct ARUserAnchor_t1406831531_marshaled_pinvoke
{
	char* ___identifier_0;
	Matrix4x4_t1817901843  ___transform_1;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.ARUserAnchor
struct ARUserAnchor_t1406831531_marshaled_com
{
	Il2CppChar* ___identifier_0;
	Matrix4x4_t1817901843  ___transform_1;
};
#endif // ARUSERANCHOR_T1406831531_H
#ifndef U3CEXAMPLEU3EC__ITERATOR0_T1380115621_H
#define U3CEXAMPLEU3EC__ITERATOR0_T1380115621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveObjectOnMap/<Example>c__Iterator0
struct  U3CExampleU3Ec__Iterator0_t1380115621  : public RuntimeObject
{
public:
	// System.Single MoveObjectOnMap/<Example>c__Iterator0::<direction>__0
	float ___U3CdirectionU3E__0_0;
	// System.Single MoveObjectOnMap/<Example>c__Iterator0::<forwardTimeElapsed>__1
	float ___U3CforwardTimeElapsedU3E__1_1;
	// UnityEngine.Vector3 MoveObjectOnMap/<Example>c__Iterator0::<movement>__2
	Vector3_t3722313464  ___U3CmovementU3E__2_2;
	// MoveObjectOnMap MoveObjectOnMap/<Example>c__Iterator0::$this
	MoveObjectOnMap_t383061840 * ___U24this_3;
	// System.Object MoveObjectOnMap/<Example>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean MoveObjectOnMap/<Example>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 MoveObjectOnMap/<Example>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CdirectionU3E__0_0() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t1380115621, ___U3CdirectionU3E__0_0)); }
	inline float get_U3CdirectionU3E__0_0() const { return ___U3CdirectionU3E__0_0; }
	inline float* get_address_of_U3CdirectionU3E__0_0() { return &___U3CdirectionU3E__0_0; }
	inline void set_U3CdirectionU3E__0_0(float value)
	{
		___U3CdirectionU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CforwardTimeElapsedU3E__1_1() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t1380115621, ___U3CforwardTimeElapsedU3E__1_1)); }
	inline float get_U3CforwardTimeElapsedU3E__1_1() const { return ___U3CforwardTimeElapsedU3E__1_1; }
	inline float* get_address_of_U3CforwardTimeElapsedU3E__1_1() { return &___U3CforwardTimeElapsedU3E__1_1; }
	inline void set_U3CforwardTimeElapsedU3E__1_1(float value)
	{
		___U3CforwardTimeElapsedU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CmovementU3E__2_2() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t1380115621, ___U3CmovementU3E__2_2)); }
	inline Vector3_t3722313464  get_U3CmovementU3E__2_2() const { return ___U3CmovementU3E__2_2; }
	inline Vector3_t3722313464 * get_address_of_U3CmovementU3E__2_2() { return &___U3CmovementU3E__2_2; }
	inline void set_U3CmovementU3E__2_2(Vector3_t3722313464  value)
	{
		___U3CmovementU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t1380115621, ___U24this_3)); }
	inline MoveObjectOnMap_t383061840 * get_U24this_3() const { return ___U24this_3; }
	inline MoveObjectOnMap_t383061840 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(MoveObjectOnMap_t383061840 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t1380115621, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t1380115621, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t1380115621, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXAMPLEU3EC__ITERATOR0_T1380115621_H
#ifndef U3CEXAMPLEU3EC__ITERATOR0_T1732569348_H
#define U3CEXAMPLEU3EC__ITERATOR0_T1732569348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraTransitionMovingCamera/<Example>c__Iterator0
struct  U3CExampleU3Ec__Iterator0_t1732569348  : public RuntimeObject
{
public:
	// Wrld.Space.LatLong CameraTransitionMovingCamera/<Example>c__Iterator0::<startLocation>__0
	LatLong_t2936018554  ___U3CstartLocationU3E__0_0;
	// Wrld.Space.LatLong CameraTransitionMovingCamera/<Example>c__Iterator0::<destLocation>__0
	LatLong_t2936018554  ___U3CdestLocationU3E__0_1;
	// System.Object CameraTransitionMovingCamera/<Example>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean CameraTransitionMovingCamera/<Example>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 CameraTransitionMovingCamera/<Example>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CstartLocationU3E__0_0() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t1732569348, ___U3CstartLocationU3E__0_0)); }
	inline LatLong_t2936018554  get_U3CstartLocationU3E__0_0() const { return ___U3CstartLocationU3E__0_0; }
	inline LatLong_t2936018554 * get_address_of_U3CstartLocationU3E__0_0() { return &___U3CstartLocationU3E__0_0; }
	inline void set_U3CstartLocationU3E__0_0(LatLong_t2936018554  value)
	{
		___U3CstartLocationU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CdestLocationU3E__0_1() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t1732569348, ___U3CdestLocationU3E__0_1)); }
	inline LatLong_t2936018554  get_U3CdestLocationU3E__0_1() const { return ___U3CdestLocationU3E__0_1; }
	inline LatLong_t2936018554 * get_address_of_U3CdestLocationU3E__0_1() { return &___U3CdestLocationU3E__0_1; }
	inline void set_U3CdestLocationU3E__0_1(LatLong_t2936018554  value)
	{
		___U3CdestLocationU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t1732569348, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t1732569348, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t1732569348, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXAMPLEU3EC__ITERATOR0_T1732569348_H
#ifndef CAMERASTATE_T2128520787_H
#define CAMERASTATE_T2128520787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapCamera.CameraState
struct  CameraState_t2128520787  : public RuntimeObject
{
public:
	// Wrld.Common.Maths.DoubleVector3 Wrld.MapCamera.CameraState::LocationEcef
	DoubleVector3_t761704365  ___LocationEcef_0;
	// Wrld.Common.Maths.DoubleVector3 Wrld.MapCamera.CameraState::InterestPointEcef
	DoubleVector3_t761704365  ___InterestPointEcef_1;
	// UnityEngine.Matrix4x4 Wrld.MapCamera.CameraState::ViewMatrix
	Matrix4x4_t1817901843  ___ViewMatrix_2;
	// UnityEngine.Matrix4x4 Wrld.MapCamera.CameraState::ProjectMatrix
	Matrix4x4_t1817901843  ___ProjectMatrix_3;

public:
	inline static int32_t get_offset_of_LocationEcef_0() { return static_cast<int32_t>(offsetof(CameraState_t2128520787, ___LocationEcef_0)); }
	inline DoubleVector3_t761704365  get_LocationEcef_0() const { return ___LocationEcef_0; }
	inline DoubleVector3_t761704365 * get_address_of_LocationEcef_0() { return &___LocationEcef_0; }
	inline void set_LocationEcef_0(DoubleVector3_t761704365  value)
	{
		___LocationEcef_0 = value;
	}

	inline static int32_t get_offset_of_InterestPointEcef_1() { return static_cast<int32_t>(offsetof(CameraState_t2128520787, ___InterestPointEcef_1)); }
	inline DoubleVector3_t761704365  get_InterestPointEcef_1() const { return ___InterestPointEcef_1; }
	inline DoubleVector3_t761704365 * get_address_of_InterestPointEcef_1() { return &___InterestPointEcef_1; }
	inline void set_InterestPointEcef_1(DoubleVector3_t761704365  value)
	{
		___InterestPointEcef_1 = value;
	}

	inline static int32_t get_offset_of_ViewMatrix_2() { return static_cast<int32_t>(offsetof(CameraState_t2128520787, ___ViewMatrix_2)); }
	inline Matrix4x4_t1817901843  get_ViewMatrix_2() const { return ___ViewMatrix_2; }
	inline Matrix4x4_t1817901843 * get_address_of_ViewMatrix_2() { return &___ViewMatrix_2; }
	inline void set_ViewMatrix_2(Matrix4x4_t1817901843  value)
	{
		___ViewMatrix_2 = value;
	}

	inline static int32_t get_offset_of_ProjectMatrix_3() { return static_cast<int32_t>(offsetof(CameraState_t2128520787, ___ProjectMatrix_3)); }
	inline Matrix4x4_t1817901843  get_ProjectMatrix_3() const { return ___ProjectMatrix_3; }
	inline Matrix4x4_t1817901843 * get_address_of_ProjectMatrix_3() { return &___ProjectMatrix_3; }
	inline void set_ProjectMatrix_3(Matrix4x4_t1817901843  value)
	{
		___ProjectMatrix_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Wrld.MapCamera.CameraState
struct CameraState_t2128520787_marshaled_pinvoke
{
	DoubleVector3_t761704365  ___LocationEcef_0;
	DoubleVector3_t761704365  ___InterestPointEcef_1;
	Matrix4x4_t1817901843  ___ViewMatrix_2;
	Matrix4x4_t1817901843  ___ProjectMatrix_3;
};
// Native definition for COM marshalling of Wrld.MapCamera.CameraState
struct CameraState_t2128520787_marshaled_com
{
	DoubleVector3_t761704365  ___LocationEcef_0;
	DoubleVector3_t761704365  ___InterestPointEcef_1;
	Matrix4x4_t1817901843  ___ViewMatrix_2;
	Matrix4x4_t1817901843  ___ProjectMatrix_3;
};
#endif // CAMERASTATE_T2128520787_H
#ifndef U3CEXAMPLEU3EC__ITERATOR0_T526138521_H
#define U3CEXAMPLEU3EC__ITERATOR0_T526138521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraTransitionHeadingAndTilt/<Example>c__Iterator0
struct  U3CExampleU3Ec__Iterator0_t526138521  : public RuntimeObject
{
public:
	// Wrld.Space.LatLong CameraTransitionHeadingAndTilt/<Example>c__Iterator0::<startLocation>__0
	LatLong_t2936018554  ___U3CstartLocationU3E__0_0;
	// Wrld.Space.LatLong CameraTransitionHeadingAndTilt/<Example>c__Iterator0::<endLocation>__0
	LatLong_t2936018554  ___U3CendLocationU3E__0_1;
	// System.Object CameraTransitionHeadingAndTilt/<Example>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean CameraTransitionHeadingAndTilt/<Example>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 CameraTransitionHeadingAndTilt/<Example>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CstartLocationU3E__0_0() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t526138521, ___U3CstartLocationU3E__0_0)); }
	inline LatLong_t2936018554  get_U3CstartLocationU3E__0_0() const { return ___U3CstartLocationU3E__0_0; }
	inline LatLong_t2936018554 * get_address_of_U3CstartLocationU3E__0_0() { return &___U3CstartLocationU3E__0_0; }
	inline void set_U3CstartLocationU3E__0_0(LatLong_t2936018554  value)
	{
		___U3CstartLocationU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CendLocationU3E__0_1() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t526138521, ___U3CendLocationU3E__0_1)); }
	inline LatLong_t2936018554  get_U3CendLocationU3E__0_1() const { return ___U3CendLocationU3E__0_1; }
	inline LatLong_t2936018554 * get_address_of_U3CendLocationU3E__0_1() { return &___U3CendLocationU3E__0_1; }
	inline void set_U3CendLocationU3E__0_1(LatLong_t2936018554  value)
	{
		___U3CendLocationU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t526138521, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t526138521, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t526138521, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXAMPLEU3EC__ITERATOR0_T526138521_H
#ifndef MOUSEROTATEGESTURE_T1726080705_H
#define MOUSEROTATEGESTURE_T1726080705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.Mouse.MouseRotateGesture
struct  MouseRotateGesture_t1726080705  : public RuntimeObject
{
public:
	// Wrld.MapInput.IUnityInputHandler Wrld.MapInput.Mouse.MouseRotateGesture::m_handler
	RuntimeObject* ___m_handler_0;
	// System.Boolean Wrld.MapInput.Mouse.MouseRotateGesture::m_rotating
	bool ___m_rotating_1;
	// UnityEngine.Vector2 Wrld.MapInput.Mouse.MouseRotateGesture::m_anchor
	Vector2_t2156229523  ___m_anchor_2;
	// System.Single Wrld.MapInput.Mouse.MouseRotateGesture::m_totalRotation
	float ___m_totalRotation_3;

public:
	inline static int32_t get_offset_of_m_handler_0() { return static_cast<int32_t>(offsetof(MouseRotateGesture_t1726080705, ___m_handler_0)); }
	inline RuntimeObject* get_m_handler_0() const { return ___m_handler_0; }
	inline RuntimeObject** get_address_of_m_handler_0() { return &___m_handler_0; }
	inline void set_m_handler_0(RuntimeObject* value)
	{
		___m_handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_handler_0), value);
	}

	inline static int32_t get_offset_of_m_rotating_1() { return static_cast<int32_t>(offsetof(MouseRotateGesture_t1726080705, ___m_rotating_1)); }
	inline bool get_m_rotating_1() const { return ___m_rotating_1; }
	inline bool* get_address_of_m_rotating_1() { return &___m_rotating_1; }
	inline void set_m_rotating_1(bool value)
	{
		___m_rotating_1 = value;
	}

	inline static int32_t get_offset_of_m_anchor_2() { return static_cast<int32_t>(offsetof(MouseRotateGesture_t1726080705, ___m_anchor_2)); }
	inline Vector2_t2156229523  get_m_anchor_2() const { return ___m_anchor_2; }
	inline Vector2_t2156229523 * get_address_of_m_anchor_2() { return &___m_anchor_2; }
	inline void set_m_anchor_2(Vector2_t2156229523  value)
	{
		___m_anchor_2 = value;
	}

	inline static int32_t get_offset_of_m_totalRotation_3() { return static_cast<int32_t>(offsetof(MouseRotateGesture_t1726080705, ___m_totalRotation_3)); }
	inline float get_m_totalRotation_3() const { return ___m_totalRotation_3; }
	inline float* get_address_of_m_totalRotation_3() { return &___m_totalRotation_3; }
	inline void set_m_totalRotation_3(float value)
	{
		___m_totalRotation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEROTATEGESTURE_T1726080705_H
#ifndef U3CEXAMPLEU3EC__ITERATOR0_T1338300621_H
#define U3CEXAMPLEU3EC__ITERATOR0_T1338300621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraTransitionAnimate/<Example>c__Iterator0
struct  U3CExampleU3Ec__Iterator0_t1338300621  : public RuntimeObject
{
public:
	// Wrld.Space.LatLong CameraTransitionAnimate/<Example>c__Iterator0::<startLocation>__0
	LatLong_t2936018554  ___U3CstartLocationU3E__0_0;
	// Wrld.Space.LatLong CameraTransitionAnimate/<Example>c__Iterator0::<destLocation>__0
	LatLong_t2936018554  ___U3CdestLocationU3E__0_1;
	// System.Object CameraTransitionAnimate/<Example>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean CameraTransitionAnimate/<Example>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 CameraTransitionAnimate/<Example>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CstartLocationU3E__0_0() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t1338300621, ___U3CstartLocationU3E__0_0)); }
	inline LatLong_t2936018554  get_U3CstartLocationU3E__0_0() const { return ___U3CstartLocationU3E__0_0; }
	inline LatLong_t2936018554 * get_address_of_U3CstartLocationU3E__0_0() { return &___U3CstartLocationU3E__0_0; }
	inline void set_U3CstartLocationU3E__0_0(LatLong_t2936018554  value)
	{
		___U3CstartLocationU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CdestLocationU3E__0_1() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t1338300621, ___U3CdestLocationU3E__0_1)); }
	inline LatLong_t2936018554  get_U3CdestLocationU3E__0_1() const { return ___U3CdestLocationU3E__0_1; }
	inline LatLong_t2936018554 * get_address_of_U3CdestLocationU3E__0_1() { return &___U3CdestLocationU3E__0_1; }
	inline void set_U3CdestLocationU3E__0_1(LatLong_t2936018554  value)
	{
		___U3CdestLocationU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t1338300621, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t1338300621, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t1338300621, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXAMPLEU3EC__ITERATOR0_T1338300621_H
#ifndef U3CEXAMPLEU3EC__ITERATOR0_T2007946409_H
#define U3CEXAMPLEU3EC__ITERATOR0_T2007946409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PositionObjectAtECEFCoordinate/<Example>c__Iterator0
struct  U3CExampleU3Ec__Iterator0_t2007946409  : public RuntimeObject
{
public:
	// Wrld.Common.Maths.DoubleVector3 PositionObjectAtECEFCoordinate/<Example>c__Iterator0::<ecefPointA>__0
	DoubleVector3_t761704365  ___U3CecefPointAU3E__0_0;
	// Wrld.Common.Maths.DoubleVector3 PositionObjectAtECEFCoordinate/<Example>c__Iterator0::<ecefPointB>__0
	DoubleVector3_t761704365  ___U3CecefPointBU3E__0_1;
	// PositionObjectAtECEFCoordinate PositionObjectAtECEFCoordinate/<Example>c__Iterator0::$this
	PositionObjectAtECEFCoordinate_t1179719296 * ___U24this_2;
	// System.Object PositionObjectAtECEFCoordinate/<Example>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean PositionObjectAtECEFCoordinate/<Example>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 PositionObjectAtECEFCoordinate/<Example>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CecefPointAU3E__0_0() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t2007946409, ___U3CecefPointAU3E__0_0)); }
	inline DoubleVector3_t761704365  get_U3CecefPointAU3E__0_0() const { return ___U3CecefPointAU3E__0_0; }
	inline DoubleVector3_t761704365 * get_address_of_U3CecefPointAU3E__0_0() { return &___U3CecefPointAU3E__0_0; }
	inline void set_U3CecefPointAU3E__0_0(DoubleVector3_t761704365  value)
	{
		___U3CecefPointAU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CecefPointBU3E__0_1() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t2007946409, ___U3CecefPointBU3E__0_1)); }
	inline DoubleVector3_t761704365  get_U3CecefPointBU3E__0_1() const { return ___U3CecefPointBU3E__0_1; }
	inline DoubleVector3_t761704365 * get_address_of_U3CecefPointBU3E__0_1() { return &___U3CecefPointBU3E__0_1; }
	inline void set_U3CecefPointBU3E__0_1(DoubleVector3_t761704365  value)
	{
		___U3CecefPointBU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t2007946409, ___U24this_2)); }
	inline PositionObjectAtECEFCoordinate_t1179719296 * get_U24this_2() const { return ___U24this_2; }
	inline PositionObjectAtECEFCoordinate_t1179719296 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(PositionObjectAtECEFCoordinate_t1179719296 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t2007946409, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t2007946409, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CExampleU3Ec__Iterator0_t2007946409, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXAMPLEU3EC__ITERATOR0_T2007946409_H
#ifndef MOUSEINPUTEVENT_T292520373_H
#define MOUSEINPUTEVENT_T292520373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.Mouse.MouseInputEvent
struct  MouseInputEvent_t292520373 
{
public:
	// Wrld.MapInput.Mouse.MouseInputAction Wrld.MapInput.Mouse.MouseInputEvent::Action
	int32_t ___Action_0;
	// Wrld.MapInput.Mouse.KeyboardModifiers Wrld.MapInput.Mouse.MouseInputEvent::KeyboardModifiers
	int32_t ___KeyboardModifiers_1;
	// System.Single Wrld.MapInput.Mouse.MouseInputEvent::x
	float ___x_2;
	// System.Single Wrld.MapInput.Mouse.MouseInputEvent::y
	float ___y_3;
	// System.Single Wrld.MapInput.Mouse.MouseInputEvent::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_Action_0() { return static_cast<int32_t>(offsetof(MouseInputEvent_t292520373, ___Action_0)); }
	inline int32_t get_Action_0() const { return ___Action_0; }
	inline int32_t* get_address_of_Action_0() { return &___Action_0; }
	inline void set_Action_0(int32_t value)
	{
		___Action_0 = value;
	}

	inline static int32_t get_offset_of_KeyboardModifiers_1() { return static_cast<int32_t>(offsetof(MouseInputEvent_t292520373, ___KeyboardModifiers_1)); }
	inline int32_t get_KeyboardModifiers_1() const { return ___KeyboardModifiers_1; }
	inline int32_t* get_address_of_KeyboardModifiers_1() { return &___KeyboardModifiers_1; }
	inline void set_KeyboardModifiers_1(int32_t value)
	{
		___KeyboardModifiers_1 = value;
	}

	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(MouseInputEvent_t292520373, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(MouseInputEvent_t292520373, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(MouseInputEvent_t292520373, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEINPUTEVENT_T292520373_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef UNITYMARSHALLIGHTDATA_T1623228070_H
#define UNITYMARSHALLIGHTDATA_T1623228070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityMarshalLightData
struct  UnityMarshalLightData_t1623228070 
{
public:
	// UnityEngine.XR.iOS.LightDataType UnityEngine.XR.iOS.UnityMarshalLightData::arLightingType
	int32_t ___arLightingType_0;
	// UnityEngine.XR.iOS.UnityARLightEstimate UnityEngine.XR.iOS.UnityMarshalLightData::arLightEstimate
	UnityARLightEstimate_t1498306117  ___arLightEstimate_1;
	// UnityEngine.XR.iOS.MarshalDirectionalLightEstimate UnityEngine.XR.iOS.UnityMarshalLightData::arDirectonalLightEstimate
	MarshalDirectionalLightEstimate_t3803901471  ___arDirectonalLightEstimate_2;

public:
	inline static int32_t get_offset_of_arLightingType_0() { return static_cast<int32_t>(offsetof(UnityMarshalLightData_t1623228070, ___arLightingType_0)); }
	inline int32_t get_arLightingType_0() const { return ___arLightingType_0; }
	inline int32_t* get_address_of_arLightingType_0() { return &___arLightingType_0; }
	inline void set_arLightingType_0(int32_t value)
	{
		___arLightingType_0 = value;
	}

	inline static int32_t get_offset_of_arLightEstimate_1() { return static_cast<int32_t>(offsetof(UnityMarshalLightData_t1623228070, ___arLightEstimate_1)); }
	inline UnityARLightEstimate_t1498306117  get_arLightEstimate_1() const { return ___arLightEstimate_1; }
	inline UnityARLightEstimate_t1498306117 * get_address_of_arLightEstimate_1() { return &___arLightEstimate_1; }
	inline void set_arLightEstimate_1(UnityARLightEstimate_t1498306117  value)
	{
		___arLightEstimate_1 = value;
	}

	inline static int32_t get_offset_of_arDirectonalLightEstimate_2() { return static_cast<int32_t>(offsetof(UnityMarshalLightData_t1623228070, ___arDirectonalLightEstimate_2)); }
	inline MarshalDirectionalLightEstimate_t3803901471  get_arDirectonalLightEstimate_2() const { return ___arDirectonalLightEstimate_2; }
	inline MarshalDirectionalLightEstimate_t3803901471 * get_address_of_arDirectonalLightEstimate_2() { return &___arDirectonalLightEstimate_2; }
	inline void set_arDirectonalLightEstimate_2(MarshalDirectionalLightEstimate_t3803901471  value)
	{
		___arDirectonalLightEstimate_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYMARSHALLIGHTDATA_T1623228070_H
#ifndef UNITYARLIGHTDATA_T2160616730_H
#define UNITYARLIGHTDATA_T2160616730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARLightData
struct  UnityARLightData_t2160616730 
{
public:
	// UnityEngine.XR.iOS.LightDataType UnityEngine.XR.iOS.UnityARLightData::arLightingType
	int32_t ___arLightingType_0;
	// UnityEngine.XR.iOS.UnityARLightEstimate UnityEngine.XR.iOS.UnityARLightData::arLightEstimate
	UnityARLightEstimate_t1498306117  ___arLightEstimate_1;
	// UnityEngine.XR.iOS.UnityARDirectionalLightEstimate UnityEngine.XR.iOS.UnityARLightData::arDirectonalLightEstimate
	UnityARDirectionalLightEstimate_t2924556994 * ___arDirectonalLightEstimate_2;

public:
	inline static int32_t get_offset_of_arLightingType_0() { return static_cast<int32_t>(offsetof(UnityARLightData_t2160616730, ___arLightingType_0)); }
	inline int32_t get_arLightingType_0() const { return ___arLightingType_0; }
	inline int32_t* get_address_of_arLightingType_0() { return &___arLightingType_0; }
	inline void set_arLightingType_0(int32_t value)
	{
		___arLightingType_0 = value;
	}

	inline static int32_t get_offset_of_arLightEstimate_1() { return static_cast<int32_t>(offsetof(UnityARLightData_t2160616730, ___arLightEstimate_1)); }
	inline UnityARLightEstimate_t1498306117  get_arLightEstimate_1() const { return ___arLightEstimate_1; }
	inline UnityARLightEstimate_t1498306117 * get_address_of_arLightEstimate_1() { return &___arLightEstimate_1; }
	inline void set_arLightEstimate_1(UnityARLightEstimate_t1498306117  value)
	{
		___arLightEstimate_1 = value;
	}

	inline static int32_t get_offset_of_arDirectonalLightEstimate_2() { return static_cast<int32_t>(offsetof(UnityARLightData_t2160616730, ___arDirectonalLightEstimate_2)); }
	inline UnityARDirectionalLightEstimate_t2924556994 * get_arDirectonalLightEstimate_2() const { return ___arDirectonalLightEstimate_2; }
	inline UnityARDirectionalLightEstimate_t2924556994 ** get_address_of_arDirectonalLightEstimate_2() { return &___arDirectonalLightEstimate_2; }
	inline void set_arDirectonalLightEstimate_2(UnityARDirectionalLightEstimate_t2924556994 * value)
	{
		___arDirectonalLightEstimate_2 = value;
		Il2CppCodeGenWriteBarrier((&___arDirectonalLightEstimate_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.UnityARLightData
struct UnityARLightData_t2160616730_marshaled_pinvoke
{
	int32_t ___arLightingType_0;
	UnityARLightEstimate_t1498306117  ___arLightEstimate_1;
	UnityARDirectionalLightEstimate_t2924556994 * ___arDirectonalLightEstimate_2;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.UnityARLightData
struct UnityARLightData_t2160616730_marshaled_com
{
	int32_t ___arLightingType_0;
	UnityARLightEstimate_t1498306117  ___arLightEstimate_1;
	UnityARDirectionalLightEstimate_t2924556994 * ___arDirectonalLightEstimate_2;
};
#endif // UNITYARLIGHTDATA_T2160616730_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef UNITYARUSERANCHORDATA_T1976826249_H
#define UNITYARUSERANCHORDATA_T1976826249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARUserAnchorData
struct  UnityARUserAnchorData_t1976826249 
{
public:
	// System.IntPtr UnityEngine.XR.iOS.UnityARUserAnchorData::ptrIdentifier
	intptr_t ___ptrIdentifier_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARUserAnchorData::transform
	UnityARMatrix4x4_t4073345847  ___transform_1;

public:
	inline static int32_t get_offset_of_ptrIdentifier_0() { return static_cast<int32_t>(offsetof(UnityARUserAnchorData_t1976826249, ___ptrIdentifier_0)); }
	inline intptr_t get_ptrIdentifier_0() const { return ___ptrIdentifier_0; }
	inline intptr_t* get_address_of_ptrIdentifier_0() { return &___ptrIdentifier_0; }
	inline void set_ptrIdentifier_0(intptr_t value)
	{
		___ptrIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(UnityARUserAnchorData_t1976826249, ___transform_1)); }
	inline UnityARMatrix4x4_t4073345847  get_transform_1() const { return ___transform_1; }
	inline UnityARMatrix4x4_t4073345847 * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(UnityARMatrix4x4_t4073345847  value)
	{
		___transform_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARUSERANCHORDATA_T1976826249_H
#ifndef UNITYARFACEANCHORDATA_T2028622935_H
#define UNITYARFACEANCHORDATA_T2028622935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARFaceAnchorData
struct  UnityARFaceAnchorData_t2028622935 
{
public:
	// System.IntPtr UnityEngine.XR.iOS.UnityARFaceAnchorData::ptrIdentifier
	intptr_t ___ptrIdentifier_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARFaceAnchorData::transform
	UnityARMatrix4x4_t4073345847  ___transform_1;
	// UnityEngine.XR.iOS.UnityARFaceGeometry UnityEngine.XR.iOS.UnityARFaceAnchorData::faceGeometry
	UnityARFaceGeometry_t4178775532  ___faceGeometry_2;
	// System.IntPtr UnityEngine.XR.iOS.UnityARFaceAnchorData::blendShapes
	intptr_t ___blendShapes_3;

public:
	inline static int32_t get_offset_of_ptrIdentifier_0() { return static_cast<int32_t>(offsetof(UnityARFaceAnchorData_t2028622935, ___ptrIdentifier_0)); }
	inline intptr_t get_ptrIdentifier_0() const { return ___ptrIdentifier_0; }
	inline intptr_t* get_address_of_ptrIdentifier_0() { return &___ptrIdentifier_0; }
	inline void set_ptrIdentifier_0(intptr_t value)
	{
		___ptrIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(UnityARFaceAnchorData_t2028622935, ___transform_1)); }
	inline UnityARMatrix4x4_t4073345847  get_transform_1() const { return ___transform_1; }
	inline UnityARMatrix4x4_t4073345847 * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(UnityARMatrix4x4_t4073345847  value)
	{
		___transform_1 = value;
	}

	inline static int32_t get_offset_of_faceGeometry_2() { return static_cast<int32_t>(offsetof(UnityARFaceAnchorData_t2028622935, ___faceGeometry_2)); }
	inline UnityARFaceGeometry_t4178775532  get_faceGeometry_2() const { return ___faceGeometry_2; }
	inline UnityARFaceGeometry_t4178775532 * get_address_of_faceGeometry_2() { return &___faceGeometry_2; }
	inline void set_faceGeometry_2(UnityARFaceGeometry_t4178775532  value)
	{
		___faceGeometry_2 = value;
	}

	inline static int32_t get_offset_of_blendShapes_3() { return static_cast<int32_t>(offsetof(UnityARFaceAnchorData_t2028622935, ___blendShapes_3)); }
	inline intptr_t get_blendShapes_3() const { return ___blendShapes_3; }
	inline intptr_t* get_address_of_blendShapes_3() { return &___blendShapes_3; }
	inline void set_blendShapes_3(intptr_t value)
	{
		___blendShapes_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARFACEANCHORDATA_T2028622935_H
#ifndef UNITYARANCHORDATA_T1157236668_H
#define UNITYARANCHORDATA_T1157236668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAnchorData
struct  UnityARAnchorData_t1157236668 
{
public:
	// System.IntPtr UnityEngine.XR.iOS.UnityARAnchorData::ptrIdentifier
	intptr_t ___ptrIdentifier_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARAnchorData::transform
	UnityARMatrix4x4_t4073345847  ___transform_1;
	// UnityEngine.XR.iOS.ARPlaneAnchorAlignment UnityEngine.XR.iOS.UnityARAnchorData::alignment
	int64_t ___alignment_2;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARAnchorData::center
	Vector4_t3319028937  ___center_3;
	// UnityEngine.Vector4 UnityEngine.XR.iOS.UnityARAnchorData::extent
	Vector4_t3319028937  ___extent_4;

public:
	inline static int32_t get_offset_of_ptrIdentifier_0() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t1157236668, ___ptrIdentifier_0)); }
	inline intptr_t get_ptrIdentifier_0() const { return ___ptrIdentifier_0; }
	inline intptr_t* get_address_of_ptrIdentifier_0() { return &___ptrIdentifier_0; }
	inline void set_ptrIdentifier_0(intptr_t value)
	{
		___ptrIdentifier_0 = value;
	}

	inline static int32_t get_offset_of_transform_1() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t1157236668, ___transform_1)); }
	inline UnityARMatrix4x4_t4073345847  get_transform_1() const { return ___transform_1; }
	inline UnityARMatrix4x4_t4073345847 * get_address_of_transform_1() { return &___transform_1; }
	inline void set_transform_1(UnityARMatrix4x4_t4073345847  value)
	{
		___transform_1 = value;
	}

	inline static int32_t get_offset_of_alignment_2() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t1157236668, ___alignment_2)); }
	inline int64_t get_alignment_2() const { return ___alignment_2; }
	inline int64_t* get_address_of_alignment_2() { return &___alignment_2; }
	inline void set_alignment_2(int64_t value)
	{
		___alignment_2 = value;
	}

	inline static int32_t get_offset_of_center_3() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t1157236668, ___center_3)); }
	inline Vector4_t3319028937  get_center_3() const { return ___center_3; }
	inline Vector4_t3319028937 * get_address_of_center_3() { return &___center_3; }
	inline void set_center_3(Vector4_t3319028937  value)
	{
		___center_3 = value;
	}

	inline static int32_t get_offset_of_extent_4() { return static_cast<int32_t>(offsetof(UnityARAnchorData_t1157236668, ___extent_4)); }
	inline Vector4_t3319028937  get_extent_4() const { return ___extent_4; }
	inline Vector4_t3319028937 * get_address_of_extent_4() { return &___extent_4; }
	inline void set_extent_4(Vector4_t3319028937  value)
	{
		___extent_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARANCHORDATA_T1157236668_H
#ifndef CAMERAINPUTHANDLER_T3237370388_H
#define CAMERAINPUTHANDLER_T3237370388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapCamera.CameraInputHandler
struct  CameraInputHandler_t3237370388  : public RuntimeObject
{
public:
	// System.Boolean Wrld.MapCamera.CameraInputHandler::m_isTouchSupported
	bool ___m_isTouchSupported_0;
	// Wrld.MapCamera.CameraInputHandler/InputFrame Wrld.MapCamera.CameraInputHandler::m_inputFrame
	InputFrame_t648463210  ___m_inputFrame_1;
	// UnityEngine.Vector2 Wrld.MapCamera.CameraInputHandler::m_previousMousePosition
	Vector2_t2156229523  ___m_previousMousePosition_2;
	// Wrld.MapInput.IUnityInputProcessor Wrld.MapCamera.CameraInputHandler::m_inputProcessor
	RuntimeObject* ___m_inputProcessor_3;

public:
	inline static int32_t get_offset_of_m_isTouchSupported_0() { return static_cast<int32_t>(offsetof(CameraInputHandler_t3237370388, ___m_isTouchSupported_0)); }
	inline bool get_m_isTouchSupported_0() const { return ___m_isTouchSupported_0; }
	inline bool* get_address_of_m_isTouchSupported_0() { return &___m_isTouchSupported_0; }
	inline void set_m_isTouchSupported_0(bool value)
	{
		___m_isTouchSupported_0 = value;
	}

	inline static int32_t get_offset_of_m_inputFrame_1() { return static_cast<int32_t>(offsetof(CameraInputHandler_t3237370388, ___m_inputFrame_1)); }
	inline InputFrame_t648463210  get_m_inputFrame_1() const { return ___m_inputFrame_1; }
	inline InputFrame_t648463210 * get_address_of_m_inputFrame_1() { return &___m_inputFrame_1; }
	inline void set_m_inputFrame_1(InputFrame_t648463210  value)
	{
		___m_inputFrame_1 = value;
	}

	inline static int32_t get_offset_of_m_previousMousePosition_2() { return static_cast<int32_t>(offsetof(CameraInputHandler_t3237370388, ___m_previousMousePosition_2)); }
	inline Vector2_t2156229523  get_m_previousMousePosition_2() const { return ___m_previousMousePosition_2; }
	inline Vector2_t2156229523 * get_address_of_m_previousMousePosition_2() { return &___m_previousMousePosition_2; }
	inline void set_m_previousMousePosition_2(Vector2_t2156229523  value)
	{
		___m_previousMousePosition_2 = value;
	}

	inline static int32_t get_offset_of_m_inputProcessor_3() { return static_cast<int32_t>(offsetof(CameraInputHandler_t3237370388, ___m_inputProcessor_3)); }
	inline RuntimeObject* get_m_inputProcessor_3() const { return ___m_inputProcessor_3; }
	inline RuntimeObject** get_address_of_m_inputProcessor_3() { return &___m_inputProcessor_3; }
	inline void set_m_inputProcessor_3(RuntimeObject* value)
	{
		___m_inputProcessor_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_inputProcessor_3), value);
	}
};

struct CameraInputHandler_t3237370388_StaticFields
{
public:
	// System.Func`2<UnityEngine.Touch,System.Boolean> Wrld.MapCamera.CameraInputHandler::<>f__am$cache0
	Func_2_t2764894207 * ___U3CU3Ef__amU24cache0_4;
	// System.Func`2<UnityEngine.Touch,System.Boolean> Wrld.MapCamera.CameraInputHandler::<>f__am$cache1
	Func_2_t2764894207 * ___U3CU3Ef__amU24cache1_5;
	// System.Func`3<UnityEngine.Touch,System.Int32,Wrld.MapInput.Touch.TouchInputPointerEvent> Wrld.MapCamera.CameraInputHandler::<>f__am$cache2
	Func_3_t904424527 * ___U3CU3Ef__amU24cache2_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(CameraInputHandler_t3237370388_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Func_2_t2764894207 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Func_2_t2764894207 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Func_2_t2764894207 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(CameraInputHandler_t3237370388_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline Func_2_t2764894207 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline Func_2_t2764894207 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(Func_2_t2764894207 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_6() { return static_cast<int32_t>(offsetof(CameraInputHandler_t3237370388_StaticFields, ___U3CU3Ef__amU24cache2_6)); }
	inline Func_3_t904424527 * get_U3CU3Ef__amU24cache2_6() const { return ___U3CU3Ef__amU24cache2_6; }
	inline Func_3_t904424527 ** get_address_of_U3CU3Ef__amU24cache2_6() { return &___U3CU3Ef__amU24cache2_6; }
	inline void set_U3CU3Ef__amU24cache2_6(Func_3_t904424527 * value)
	{
		___U3CU3Ef__amU24cache2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAINPUTHANDLER_T3237370388_H
#ifndef APIIMPLEMENTATION_T1854519848_H
#define APIIMPLEMENTATION_T1854519848_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.ApiImplementation
struct  ApiImplementation_t1854519848  : public RuntimeObject
{
public:
	// Wrld.NativePluginRunner Wrld.ApiImplementation::m_nativePluginRunner
	NativePluginRunner_t3528041536 * ___m_nativePluginRunner_0;
	// Wrld.CoordinateSystem Wrld.ApiImplementation::m_coordinateSystem
	int32_t ___m_coordinateSystem_1;
	// Wrld.MapCamera.CameraApi Wrld.ApiImplementation::m_cameraController
	CameraApi_t3006904385 * ___m_cameraController_2;
	// Wrld.Resources.Buildings.BuildingsApi Wrld.ApiImplementation::m_buildingsApi
	BuildingsApi_t3854622187 * ___m_buildingsApi_3;
	// Wrld.Space.GeographicApi Wrld.ApiImplementation::m_geographicApi
	GeographicApi_t2934948604 * ___m_geographicApi_4;
	// Wrld.Space.UnityWorldSpaceCoordinateFrame Wrld.ApiImplementation::m_frame
	UnityWorldSpaceCoordinateFrame_t1382732960 * ___m_frame_5;
	// Wrld.Common.Maths.DoubleVector3 Wrld.ApiImplementation::m_originECEF
	DoubleVector3_t761704365  ___m_originECEF_6;
	// Wrld.MapCamera.InterestPointProvider Wrld.ApiImplementation::m_interestPointProvider
	InterestPointProvider_t3426197040 * ___m_interestPointProvider_7;
	// Wrld.Streaming.GameObjectStreamer Wrld.ApiImplementation::m_terrainStreamer
	GameObjectStreamer_t3452608707 * ___m_terrainStreamer_8;
	// Wrld.Streaming.GameObjectStreamer Wrld.ApiImplementation::m_roadStreamer
	GameObjectStreamer_t3452608707 * ___m_roadStreamer_9;
	// Wrld.Streaming.GameObjectStreamer Wrld.ApiImplementation::m_buildingStreamer
	GameObjectStreamer_t3452608707 * ___m_buildingStreamer_10;
	// Wrld.Streaming.GameObjectStreamer Wrld.ApiImplementation::m_highlightStreamer
	GameObjectStreamer_t3452608707 * ___m_highlightStreamer_11;
	// Wrld.MapGameObjectScene Wrld.ApiImplementation::m_mapGameObjectScene
	MapGameObjectScene_t128928738 * ___m_mapGameObjectScene_12;

public:
	inline static int32_t get_offset_of_m_nativePluginRunner_0() { return static_cast<int32_t>(offsetof(ApiImplementation_t1854519848, ___m_nativePluginRunner_0)); }
	inline NativePluginRunner_t3528041536 * get_m_nativePluginRunner_0() const { return ___m_nativePluginRunner_0; }
	inline NativePluginRunner_t3528041536 ** get_address_of_m_nativePluginRunner_0() { return &___m_nativePluginRunner_0; }
	inline void set_m_nativePluginRunner_0(NativePluginRunner_t3528041536 * value)
	{
		___m_nativePluginRunner_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_nativePluginRunner_0), value);
	}

	inline static int32_t get_offset_of_m_coordinateSystem_1() { return static_cast<int32_t>(offsetof(ApiImplementation_t1854519848, ___m_coordinateSystem_1)); }
	inline int32_t get_m_coordinateSystem_1() const { return ___m_coordinateSystem_1; }
	inline int32_t* get_address_of_m_coordinateSystem_1() { return &___m_coordinateSystem_1; }
	inline void set_m_coordinateSystem_1(int32_t value)
	{
		___m_coordinateSystem_1 = value;
	}

	inline static int32_t get_offset_of_m_cameraController_2() { return static_cast<int32_t>(offsetof(ApiImplementation_t1854519848, ___m_cameraController_2)); }
	inline CameraApi_t3006904385 * get_m_cameraController_2() const { return ___m_cameraController_2; }
	inline CameraApi_t3006904385 ** get_address_of_m_cameraController_2() { return &___m_cameraController_2; }
	inline void set_m_cameraController_2(CameraApi_t3006904385 * value)
	{
		___m_cameraController_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_cameraController_2), value);
	}

	inline static int32_t get_offset_of_m_buildingsApi_3() { return static_cast<int32_t>(offsetof(ApiImplementation_t1854519848, ___m_buildingsApi_3)); }
	inline BuildingsApi_t3854622187 * get_m_buildingsApi_3() const { return ___m_buildingsApi_3; }
	inline BuildingsApi_t3854622187 ** get_address_of_m_buildingsApi_3() { return &___m_buildingsApi_3; }
	inline void set_m_buildingsApi_3(BuildingsApi_t3854622187 * value)
	{
		___m_buildingsApi_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_buildingsApi_3), value);
	}

	inline static int32_t get_offset_of_m_geographicApi_4() { return static_cast<int32_t>(offsetof(ApiImplementation_t1854519848, ___m_geographicApi_4)); }
	inline GeographicApi_t2934948604 * get_m_geographicApi_4() const { return ___m_geographicApi_4; }
	inline GeographicApi_t2934948604 ** get_address_of_m_geographicApi_4() { return &___m_geographicApi_4; }
	inline void set_m_geographicApi_4(GeographicApi_t2934948604 * value)
	{
		___m_geographicApi_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_geographicApi_4), value);
	}

	inline static int32_t get_offset_of_m_frame_5() { return static_cast<int32_t>(offsetof(ApiImplementation_t1854519848, ___m_frame_5)); }
	inline UnityWorldSpaceCoordinateFrame_t1382732960 * get_m_frame_5() const { return ___m_frame_5; }
	inline UnityWorldSpaceCoordinateFrame_t1382732960 ** get_address_of_m_frame_5() { return &___m_frame_5; }
	inline void set_m_frame_5(UnityWorldSpaceCoordinateFrame_t1382732960 * value)
	{
		___m_frame_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_frame_5), value);
	}

	inline static int32_t get_offset_of_m_originECEF_6() { return static_cast<int32_t>(offsetof(ApiImplementation_t1854519848, ___m_originECEF_6)); }
	inline DoubleVector3_t761704365  get_m_originECEF_6() const { return ___m_originECEF_6; }
	inline DoubleVector3_t761704365 * get_address_of_m_originECEF_6() { return &___m_originECEF_6; }
	inline void set_m_originECEF_6(DoubleVector3_t761704365  value)
	{
		___m_originECEF_6 = value;
	}

	inline static int32_t get_offset_of_m_interestPointProvider_7() { return static_cast<int32_t>(offsetof(ApiImplementation_t1854519848, ___m_interestPointProvider_7)); }
	inline InterestPointProvider_t3426197040 * get_m_interestPointProvider_7() const { return ___m_interestPointProvider_7; }
	inline InterestPointProvider_t3426197040 ** get_address_of_m_interestPointProvider_7() { return &___m_interestPointProvider_7; }
	inline void set_m_interestPointProvider_7(InterestPointProvider_t3426197040 * value)
	{
		___m_interestPointProvider_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_interestPointProvider_7), value);
	}

	inline static int32_t get_offset_of_m_terrainStreamer_8() { return static_cast<int32_t>(offsetof(ApiImplementation_t1854519848, ___m_terrainStreamer_8)); }
	inline GameObjectStreamer_t3452608707 * get_m_terrainStreamer_8() const { return ___m_terrainStreamer_8; }
	inline GameObjectStreamer_t3452608707 ** get_address_of_m_terrainStreamer_8() { return &___m_terrainStreamer_8; }
	inline void set_m_terrainStreamer_8(GameObjectStreamer_t3452608707 * value)
	{
		___m_terrainStreamer_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_terrainStreamer_8), value);
	}

	inline static int32_t get_offset_of_m_roadStreamer_9() { return static_cast<int32_t>(offsetof(ApiImplementation_t1854519848, ___m_roadStreamer_9)); }
	inline GameObjectStreamer_t3452608707 * get_m_roadStreamer_9() const { return ___m_roadStreamer_9; }
	inline GameObjectStreamer_t3452608707 ** get_address_of_m_roadStreamer_9() { return &___m_roadStreamer_9; }
	inline void set_m_roadStreamer_9(GameObjectStreamer_t3452608707 * value)
	{
		___m_roadStreamer_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_roadStreamer_9), value);
	}

	inline static int32_t get_offset_of_m_buildingStreamer_10() { return static_cast<int32_t>(offsetof(ApiImplementation_t1854519848, ___m_buildingStreamer_10)); }
	inline GameObjectStreamer_t3452608707 * get_m_buildingStreamer_10() const { return ___m_buildingStreamer_10; }
	inline GameObjectStreamer_t3452608707 ** get_address_of_m_buildingStreamer_10() { return &___m_buildingStreamer_10; }
	inline void set_m_buildingStreamer_10(GameObjectStreamer_t3452608707 * value)
	{
		___m_buildingStreamer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_buildingStreamer_10), value);
	}

	inline static int32_t get_offset_of_m_highlightStreamer_11() { return static_cast<int32_t>(offsetof(ApiImplementation_t1854519848, ___m_highlightStreamer_11)); }
	inline GameObjectStreamer_t3452608707 * get_m_highlightStreamer_11() const { return ___m_highlightStreamer_11; }
	inline GameObjectStreamer_t3452608707 ** get_address_of_m_highlightStreamer_11() { return &___m_highlightStreamer_11; }
	inline void set_m_highlightStreamer_11(GameObjectStreamer_t3452608707 * value)
	{
		___m_highlightStreamer_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_highlightStreamer_11), value);
	}

	inline static int32_t get_offset_of_m_mapGameObjectScene_12() { return static_cast<int32_t>(offsetof(ApiImplementation_t1854519848, ___m_mapGameObjectScene_12)); }
	inline MapGameObjectScene_t128928738 * get_m_mapGameObjectScene_12() const { return ___m_mapGameObjectScene_12; }
	inline MapGameObjectScene_t128928738 ** get_address_of_m_mapGameObjectScene_12() { return &___m_mapGameObjectScene_12; }
	inline void set_m_mapGameObjectScene_12(MapGameObjectScene_t128928738 * value)
	{
		___m_mapGameObjectScene_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_mapGameObjectScene_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIIMPLEMENTATION_T1854519848_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef INTERNAL_ARFACEANCHORREMOVED_T2563439402_H
#define INTERNAL_ARFACEANCHORREMOVED_T2563439402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorRemoved
struct  internal_ARFaceAnchorRemoved_t2563439402  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARFACEANCHORREMOVED_T2563439402_H
#ifndef INTERNAL_ARUSERANCHORADDED_T3285282493_H
#define INTERNAL_ARUSERANCHORADDED_T3285282493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorAdded
struct  internal_ARUserAnchorAdded_t3285282493  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARUSERANCHORADDED_T3285282493_H
#ifndef INTERNAL_ARUSERANCHORUPDATED_T3964727538_H
#define INTERNAL_ARUSERANCHORUPDATED_T3964727538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorUpdated
struct  internal_ARUserAnchorUpdated_t3964727538  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARUSERANCHORUPDATED_T3964727538_H
#ifndef INTERNAL_UNITYARCAMERA_T3920739388_H
#define INTERNAL_UNITYARCAMERA_T3920739388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.internal_UnityARCamera
struct  internal_UnityARCamera_t3920739388 
{
public:
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.internal_UnityARCamera::worldTransform
	UnityARMatrix4x4_t4073345847  ___worldTransform_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.internal_UnityARCamera::projectionMatrix
	UnityARMatrix4x4_t4073345847  ___projectionMatrix_1;
	// UnityEngine.XR.iOS.ARTrackingState UnityEngine.XR.iOS.internal_UnityARCamera::trackingState
	int32_t ___trackingState_2;
	// UnityEngine.XR.iOS.ARTrackingStateReason UnityEngine.XR.iOS.internal_UnityARCamera::trackingReason
	int32_t ___trackingReason_3;
	// UnityEngine.XR.iOS.UnityVideoParams UnityEngine.XR.iOS.internal_UnityARCamera::videoParams
	UnityVideoParams_t4155354995  ___videoParams_4;
	// UnityEngine.XR.iOS.UnityMarshalLightData UnityEngine.XR.iOS.internal_UnityARCamera::lightData
	UnityMarshalLightData_t1623228070  ___lightData_5;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.internal_UnityARCamera::displayTransform
	UnityARMatrix4x4_t4073345847  ___displayTransform_6;
	// System.UInt32 UnityEngine.XR.iOS.internal_UnityARCamera::getPointCloudData
	uint32_t ___getPointCloudData_7;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t3920739388, ___worldTransform_0)); }
	inline UnityARMatrix4x4_t4073345847  get_worldTransform_0() const { return ___worldTransform_0; }
	inline UnityARMatrix4x4_t4073345847 * get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(UnityARMatrix4x4_t4073345847  value)
	{
		___worldTransform_0 = value;
	}

	inline static int32_t get_offset_of_projectionMatrix_1() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t3920739388, ___projectionMatrix_1)); }
	inline UnityARMatrix4x4_t4073345847  get_projectionMatrix_1() const { return ___projectionMatrix_1; }
	inline UnityARMatrix4x4_t4073345847 * get_address_of_projectionMatrix_1() { return &___projectionMatrix_1; }
	inline void set_projectionMatrix_1(UnityARMatrix4x4_t4073345847  value)
	{
		___projectionMatrix_1 = value;
	}

	inline static int32_t get_offset_of_trackingState_2() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t3920739388, ___trackingState_2)); }
	inline int32_t get_trackingState_2() const { return ___trackingState_2; }
	inline int32_t* get_address_of_trackingState_2() { return &___trackingState_2; }
	inline void set_trackingState_2(int32_t value)
	{
		___trackingState_2 = value;
	}

	inline static int32_t get_offset_of_trackingReason_3() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t3920739388, ___trackingReason_3)); }
	inline int32_t get_trackingReason_3() const { return ___trackingReason_3; }
	inline int32_t* get_address_of_trackingReason_3() { return &___trackingReason_3; }
	inline void set_trackingReason_3(int32_t value)
	{
		___trackingReason_3 = value;
	}

	inline static int32_t get_offset_of_videoParams_4() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t3920739388, ___videoParams_4)); }
	inline UnityVideoParams_t4155354995  get_videoParams_4() const { return ___videoParams_4; }
	inline UnityVideoParams_t4155354995 * get_address_of_videoParams_4() { return &___videoParams_4; }
	inline void set_videoParams_4(UnityVideoParams_t4155354995  value)
	{
		___videoParams_4 = value;
	}

	inline static int32_t get_offset_of_lightData_5() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t3920739388, ___lightData_5)); }
	inline UnityMarshalLightData_t1623228070  get_lightData_5() const { return ___lightData_5; }
	inline UnityMarshalLightData_t1623228070 * get_address_of_lightData_5() { return &___lightData_5; }
	inline void set_lightData_5(UnityMarshalLightData_t1623228070  value)
	{
		___lightData_5 = value;
	}

	inline static int32_t get_offset_of_displayTransform_6() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t3920739388, ___displayTransform_6)); }
	inline UnityARMatrix4x4_t4073345847  get_displayTransform_6() const { return ___displayTransform_6; }
	inline UnityARMatrix4x4_t4073345847 * get_address_of_displayTransform_6() { return &___displayTransform_6; }
	inline void set_displayTransform_6(UnityARMatrix4x4_t4073345847  value)
	{
		___displayTransform_6 = value;
	}

	inline static int32_t get_offset_of_getPointCloudData_7() { return static_cast<int32_t>(offsetof(internal_UnityARCamera_t3920739388, ___getPointCloudData_7)); }
	inline uint32_t get_getPointCloudData_7() const { return ___getPointCloudData_7; }
	inline uint32_t* get_address_of_getPointCloudData_7() { return &___getPointCloudData_7; }
	inline void set_getPointCloudData_7(uint32_t value)
	{
		___getPointCloudData_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_UNITYARCAMERA_T3920739388_H
#ifndef INTERNAL_ARUSERANCHORREMOVED_T386858594_H
#define INTERNAL_ARUSERANCHORREMOVED_T386858594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorRemoved
struct  internal_ARUserAnchorRemoved_t386858594  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARUSERANCHORREMOVED_T386858594_H
#ifndef INTERNAL_ARFACEANCHORADDED_T1021040265_H
#define INTERNAL_ARFACEANCHORADDED_T1021040265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorAdded
struct  internal_ARFaceAnchorAdded_t1021040265  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARFACEANCHORADDED_T1021040265_H
#ifndef ARUSERANCHORUPDATED_T4007601678_H
#define ARUSERANCHORUPDATED_T4007601678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorUpdated
struct  ARUserAnchorUpdated_t4007601678  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARUSERANCHORUPDATED_T4007601678_H
#ifndef UNITYARKITPLUGINSETTINGS_T2201217663_H
#define UNITYARKITPLUGINSETTINGS_T2201217663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityARKitPluginSettings
struct  UnityARKitPluginSettings_t2201217663  : public ScriptableObject_t2528358522
{
public:
	// System.Boolean UnityARKitPluginSettings::m_ARKitUsesFacetracking
	bool ___m_ARKitUsesFacetracking_2;
	// System.Boolean UnityARKitPluginSettings::AppRequiresARKit
	bool ___AppRequiresARKit_3;

public:
	inline static int32_t get_offset_of_m_ARKitUsesFacetracking_2() { return static_cast<int32_t>(offsetof(UnityARKitPluginSettings_t2201217663, ___m_ARKitUsesFacetracking_2)); }
	inline bool get_m_ARKitUsesFacetracking_2() const { return ___m_ARKitUsesFacetracking_2; }
	inline bool* get_address_of_m_ARKitUsesFacetracking_2() { return &___m_ARKitUsesFacetracking_2; }
	inline void set_m_ARKitUsesFacetracking_2(bool value)
	{
		___m_ARKitUsesFacetracking_2 = value;
	}

	inline static int32_t get_offset_of_AppRequiresARKit_3() { return static_cast<int32_t>(offsetof(UnityARKitPluginSettings_t2201217663, ___AppRequiresARKit_3)); }
	inline bool get_AppRequiresARKit_3() const { return ___AppRequiresARKit_3; }
	inline bool* get_address_of_AppRequiresARKit_3() { return &___AppRequiresARKit_3; }
	inline void set_AppRequiresARKit_3(bool value)
	{
		___AppRequiresARKit_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARKITPLUGINSETTINGS_T2201217663_H
#ifndef INTERNAL_ARFACEANCHORUPDATED_T3423900432_H
#define INTERNAL_ARFACEANCHORUPDATED_T3423900432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorUpdated
struct  internal_ARFaceAnchorUpdated_t3423900432  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARFACEANCHORUPDATED_T3423900432_H
#ifndef ARUSERANCHORREMOVED_T23344545_H
#define ARUSERANCHORREMOVED_T23344545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorRemoved
struct  ARUserAnchorRemoved_t23344545  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARUSERANCHORREMOVED_T23344545_H
#ifndef ARFACEANCHORADDED_T3526051790_H
#define ARFACEANCHORADDED_T3526051790_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFaceAnchorAdded
struct  ARFaceAnchorAdded_t3526051790  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARFACEANCHORADDED_T3526051790_H
#ifndef ARFACEANCHORUPDATED_T3258688950_H
#define ARFACEANCHORUPDATED_T3258688950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFaceAnchorUpdated
struct  ARFaceAnchorUpdated_t3258688950  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARFACEANCHORUPDATED_T3258688950_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ARFACEANCHORREMOVED_T2550278937_H
#define ARFACEANCHORREMOVED_T2550278937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARFaceAnchorRemoved
struct  ARFaceAnchorRemoved_t2550278937  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARFACEANCHORREMOVED_T2550278937_H
#ifndef ARSESSIONFAILED_T2125002991_H
#define ARSESSIONFAILED_T2125002991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed
struct  ARSessionFailed_t2125002991  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARSESSIONFAILED_T2125002991_H
#ifndef ARSESSIONCALLBACK_T3772093212_H
#define ARSESSIONCALLBACK_T3772093212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionCallback
struct  ARSessionCallback_t3772093212  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARSESSIONCALLBACK_T3772093212_H
#ifndef INTERNAL_ARANCHORADDED_T1565083332_H
#define INTERNAL_ARANCHORADDED_T1565083332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded
struct  internal_ARAnchorAdded_t1565083332  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARANCHORADDED_T1565083332_H
#ifndef INTERNAL_ARANCHORUPDATED_T2645242205_H
#define INTERNAL_ARANCHORUPDATED_T2645242205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated
struct  internal_ARAnchorUpdated_t2645242205  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARANCHORUPDATED_T2645242205_H
#ifndef INTERNAL_ARANCHORREMOVED_T3371657877_H
#define INTERNAL_ARANCHORREMOVED_T3371657877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved
struct  internal_ARAnchorRemoved_t3371657877  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARANCHORREMOVED_T3371657877_H
#ifndef UNITYARCAMERA_T2069150450_H
#define UNITYARCAMERA_T2069150450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARCamera
struct  UnityARCamera_t2069150450 
{
public:
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARCamera::worldTransform
	UnityARMatrix4x4_t4073345847  ___worldTransform_0;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARCamera::projectionMatrix
	UnityARMatrix4x4_t4073345847  ___projectionMatrix_1;
	// UnityEngine.XR.iOS.ARTrackingState UnityEngine.XR.iOS.UnityARCamera::trackingState
	int32_t ___trackingState_2;
	// UnityEngine.XR.iOS.ARTrackingStateReason UnityEngine.XR.iOS.UnityARCamera::trackingReason
	int32_t ___trackingReason_3;
	// UnityEngine.XR.iOS.UnityVideoParams UnityEngine.XR.iOS.UnityARCamera::videoParams
	UnityVideoParams_t4155354995  ___videoParams_4;
	// UnityEngine.XR.iOS.UnityARLightData UnityEngine.XR.iOS.UnityARCamera::lightData
	UnityARLightData_t2160616730  ___lightData_5;
	// UnityEngine.XR.iOS.UnityARMatrix4x4 UnityEngine.XR.iOS.UnityARCamera::displayTransform
	UnityARMatrix4x4_t4073345847  ___displayTransform_6;
	// UnityEngine.Vector3[] UnityEngine.XR.iOS.UnityARCamera::pointCloudData
	Vector3U5BU5D_t1718750761* ___pointCloudData_7;

public:
	inline static int32_t get_offset_of_worldTransform_0() { return static_cast<int32_t>(offsetof(UnityARCamera_t2069150450, ___worldTransform_0)); }
	inline UnityARMatrix4x4_t4073345847  get_worldTransform_0() const { return ___worldTransform_0; }
	inline UnityARMatrix4x4_t4073345847 * get_address_of_worldTransform_0() { return &___worldTransform_0; }
	inline void set_worldTransform_0(UnityARMatrix4x4_t4073345847  value)
	{
		___worldTransform_0 = value;
	}

	inline static int32_t get_offset_of_projectionMatrix_1() { return static_cast<int32_t>(offsetof(UnityARCamera_t2069150450, ___projectionMatrix_1)); }
	inline UnityARMatrix4x4_t4073345847  get_projectionMatrix_1() const { return ___projectionMatrix_1; }
	inline UnityARMatrix4x4_t4073345847 * get_address_of_projectionMatrix_1() { return &___projectionMatrix_1; }
	inline void set_projectionMatrix_1(UnityARMatrix4x4_t4073345847  value)
	{
		___projectionMatrix_1 = value;
	}

	inline static int32_t get_offset_of_trackingState_2() { return static_cast<int32_t>(offsetof(UnityARCamera_t2069150450, ___trackingState_2)); }
	inline int32_t get_trackingState_2() const { return ___trackingState_2; }
	inline int32_t* get_address_of_trackingState_2() { return &___trackingState_2; }
	inline void set_trackingState_2(int32_t value)
	{
		___trackingState_2 = value;
	}

	inline static int32_t get_offset_of_trackingReason_3() { return static_cast<int32_t>(offsetof(UnityARCamera_t2069150450, ___trackingReason_3)); }
	inline int32_t get_trackingReason_3() const { return ___trackingReason_3; }
	inline int32_t* get_address_of_trackingReason_3() { return &___trackingReason_3; }
	inline void set_trackingReason_3(int32_t value)
	{
		___trackingReason_3 = value;
	}

	inline static int32_t get_offset_of_videoParams_4() { return static_cast<int32_t>(offsetof(UnityARCamera_t2069150450, ___videoParams_4)); }
	inline UnityVideoParams_t4155354995  get_videoParams_4() const { return ___videoParams_4; }
	inline UnityVideoParams_t4155354995 * get_address_of_videoParams_4() { return &___videoParams_4; }
	inline void set_videoParams_4(UnityVideoParams_t4155354995  value)
	{
		___videoParams_4 = value;
	}

	inline static int32_t get_offset_of_lightData_5() { return static_cast<int32_t>(offsetof(UnityARCamera_t2069150450, ___lightData_5)); }
	inline UnityARLightData_t2160616730  get_lightData_5() const { return ___lightData_5; }
	inline UnityARLightData_t2160616730 * get_address_of_lightData_5() { return &___lightData_5; }
	inline void set_lightData_5(UnityARLightData_t2160616730  value)
	{
		___lightData_5 = value;
	}

	inline static int32_t get_offset_of_displayTransform_6() { return static_cast<int32_t>(offsetof(UnityARCamera_t2069150450, ___displayTransform_6)); }
	inline UnityARMatrix4x4_t4073345847  get_displayTransform_6() const { return ___displayTransform_6; }
	inline UnityARMatrix4x4_t4073345847 * get_address_of_displayTransform_6() { return &___displayTransform_6; }
	inline void set_displayTransform_6(UnityARMatrix4x4_t4073345847  value)
	{
		___displayTransform_6 = value;
	}

	inline static int32_t get_offset_of_pointCloudData_7() { return static_cast<int32_t>(offsetof(UnityARCamera_t2069150450, ___pointCloudData_7)); }
	inline Vector3U5BU5D_t1718750761* get_pointCloudData_7() const { return ___pointCloudData_7; }
	inline Vector3U5BU5D_t1718750761** get_address_of_pointCloudData_7() { return &___pointCloudData_7; }
	inline void set_pointCloudData_7(Vector3U5BU5D_t1718750761* value)
	{
		___pointCloudData_7 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudData_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.iOS.UnityARCamera
struct UnityARCamera_t2069150450_marshaled_pinvoke
{
	UnityARMatrix4x4_t4073345847  ___worldTransform_0;
	UnityARMatrix4x4_t4073345847  ___projectionMatrix_1;
	int32_t ___trackingState_2;
	int32_t ___trackingReason_3;
	UnityVideoParams_t4155354995  ___videoParams_4;
	UnityARLightData_t2160616730_marshaled_pinvoke ___lightData_5;
	UnityARMatrix4x4_t4073345847  ___displayTransform_6;
	Vector3_t3722313464 * ___pointCloudData_7;
};
// Native definition for COM marshalling of UnityEngine.XR.iOS.UnityARCamera
struct UnityARCamera_t2069150450_marshaled_com
{
	UnityARMatrix4x4_t4073345847  ___worldTransform_0;
	UnityARMatrix4x4_t4073345847  ___projectionMatrix_1;
	int32_t ___trackingState_2;
	int32_t ___trackingReason_3;
	UnityVideoParams_t4155354995  ___videoParams_4;
	UnityARLightData_t2160616730_marshaled_com ___lightData_5;
	UnityARMatrix4x4_t4073345847  ___displayTransform_6;
	Vector3_t3722313464 * ___pointCloudData_7;
};
#endif // UNITYARCAMERA_T2069150450_H
#ifndef SENDACTIONDELEGATE_T3936809903_H
#define SENDACTIONDELEGATE_T3936809903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapCamera.CameraInputHandler/SendActionDelegate
struct  SendActionDelegate_t3936809903  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDACTIONDELEGATE_T3936809903_H
#ifndef U3CHANDLEMOUSEINPUTU3EC__ANONSTOREY0_T3986541062_H
#define U3CHANDLEMOUSEINPUTU3EC__ANONSTOREY0_T3986541062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapCamera.CameraInputHandler/<HandleMouseInput>c__AnonStorey0
struct  U3CHandleMouseInputU3Ec__AnonStorey0_t3986541062  : public RuntimeObject
{
public:
	// Wrld.MapInput.Mouse.MouseInputEvent Wrld.MapCamera.CameraInputHandler/<HandleMouseInput>c__AnonStorey0::mouseEvent
	MouseInputEvent_t292520373  ___mouseEvent_0;
	// Wrld.MapCamera.CameraInputHandler Wrld.MapCamera.CameraInputHandler/<HandleMouseInput>c__AnonStorey0::$this
	CameraInputHandler_t3237370388 * ___U24this_1;

public:
	inline static int32_t get_offset_of_mouseEvent_0() { return static_cast<int32_t>(offsetof(U3CHandleMouseInputU3Ec__AnonStorey0_t3986541062, ___mouseEvent_0)); }
	inline MouseInputEvent_t292520373  get_mouseEvent_0() const { return ___mouseEvent_0; }
	inline MouseInputEvent_t292520373 * get_address_of_mouseEvent_0() { return &___mouseEvent_0; }
	inline void set_mouseEvent_0(MouseInputEvent_t292520373  value)
	{
		___mouseEvent_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CHandleMouseInputU3Ec__AnonStorey0_t3986541062, ___U24this_1)); }
	inline CameraInputHandler_t3237370388 * get_U24this_1() const { return ___U24this_1; }
	inline CameraInputHandler_t3237370388 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(CameraInputHandler_t3237370388 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHANDLEMOUSEINPUTU3EC__ANONSTOREY0_T3986541062_H
#ifndef CAMERAEVENTCALLBACK_T1620885257_H
#define CAMERAEVENTCALLBACK_T1620885257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapCamera.CameraApi/CameraEventCallback
struct  CameraEventCallback_t1620885257  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAEVENTCALLBACK_T1620885257_H
#ifndef THREADSTARTDELEGATE_T2173555516_H
#define THREADSTARTDELEGATE_T2173555516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Concurrency.ThreadService/ThreadStartDelegate
struct  ThreadStartDelegate_t2173555516  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADSTARTDELEGATE_T2173555516_H
#ifndef CREATETHREADDELEGATE_T2573395908_H
#define CREATETHREADDELEGATE_T2573395908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Concurrency.ThreadService/CreateThreadDelegate
struct  CreateThreadDelegate_t2573395908  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATETHREADDELEGATE_T2573395908_H
#ifndef TRANSITIONSTARTHANDLER_T1803894657_H
#define TRANSITIONSTARTHANDLER_T1803894657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapCamera.CameraApi/TransitionStartHandler
struct  TransitionStartHandler_t1803894657  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITIONSTARTHANDLER_T1803894657_H
#ifndef JOINTHREADDELEGATE_T1262337526_H
#define JOINTHREADDELEGATE_T1262337526_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Concurrency.ThreadService/JoinThreadDelegate
struct  JoinThreadDelegate_t1262337526  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOINTHREADDELEGATE_T1262337526_H
#ifndef TRANSITIONENDHANDLER_T4082272293_H
#define TRANSITIONENDHANDLER_T4082272293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapCamera.CameraApi/TransitionEndHandler
struct  TransitionEndHandler_t4082272293  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITIONENDHANDLER_T4082272293_H
#ifndef INTERNAL_ARSESSIONTRACKINGCHANGED_T1988849735_H
#define INTERNAL_ARSESSIONTRACKINGCHANGED_T1988849735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARSessionTrackingChanged
struct  internal_ARSessionTrackingChanged_t1988849735  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARSESSIONTRACKINGCHANGED_T1988849735_H
#ifndef INTERNAL_ARFRAMEUPDATE_T3254989823_H
#define INTERNAL_ARFRAMEUPDATE_T3254989823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate
struct  internal_ARFrameUpdate_t3254989823  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNAL_ARFRAMEUPDATE_T3254989823_H
#ifndef ARSESSIONTRACKINGCHANGED_T923029411_H
#define ARSESSIONTRACKINGCHANGED_T923029411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionTrackingChanged
struct  ARSessionTrackingChanged_t923029411  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARSESSIONTRACKINGCHANGED_T923029411_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ROTATEOBJECTONMAP_T115929985_H
#define ROTATEOBJECTONMAP_T115929985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotateObjectOnMap
struct  RotateObjectOnMap_t115929985  : public MonoBehaviour_t3962482529
{
public:
	// Wrld.Space.GeographicTransform RotateObjectOnMap::coordinateFrame
	GeographicTransform_t2262143282 * ___coordinateFrame_4;
	// UnityEngine.Transform RotateObjectOnMap::box
	Transform_t3600365921 * ___box_5;

public:
	inline static int32_t get_offset_of_coordinateFrame_4() { return static_cast<int32_t>(offsetof(RotateObjectOnMap_t115929985, ___coordinateFrame_4)); }
	inline GeographicTransform_t2262143282 * get_coordinateFrame_4() const { return ___coordinateFrame_4; }
	inline GeographicTransform_t2262143282 ** get_address_of_coordinateFrame_4() { return &___coordinateFrame_4; }
	inline void set_coordinateFrame_4(GeographicTransform_t2262143282 * value)
	{
		___coordinateFrame_4 = value;
		Il2CppCodeGenWriteBarrier((&___coordinateFrame_4), value);
	}

	inline static int32_t get_offset_of_box_5() { return static_cast<int32_t>(offsetof(RotateObjectOnMap_t115929985, ___box_5)); }
	inline Transform_t3600365921 * get_box_5() const { return ___box_5; }
	inline Transform_t3600365921 ** get_address_of_box_5() { return &___box_5; }
	inline void set_box_5(Transform_t3600365921 * value)
	{
		___box_5 = value;
		Il2CppCodeGenWriteBarrier((&___box_5), value);
	}
};

struct RotateObjectOnMap_t115929985_StaticFields
{
public:
	// Wrld.Space.LatLong RotateObjectOnMap::startPosition
	LatLong_t2936018554  ___startPosition_2;
	// System.Single RotateObjectOnMap::rotationSpeed
	float ___rotationSpeed_3;

public:
	inline static int32_t get_offset_of_startPosition_2() { return static_cast<int32_t>(offsetof(RotateObjectOnMap_t115929985_StaticFields, ___startPosition_2)); }
	inline LatLong_t2936018554  get_startPosition_2() const { return ___startPosition_2; }
	inline LatLong_t2936018554 * get_address_of_startPosition_2() { return &___startPosition_2; }
	inline void set_startPosition_2(LatLong_t2936018554  value)
	{
		___startPosition_2 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_3() { return static_cast<int32_t>(offsetof(RotateObjectOnMap_t115929985_StaticFields, ___rotationSpeed_3)); }
	inline float get_rotationSpeed_3() const { return ___rotationSpeed_3; }
	inline float* get_address_of_rotationSpeed_3() { return &___rotationSpeed_3; }
	inline void set_rotationSpeed_3(float value)
	{
		___rotationSpeed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEOBJECTONMAP_T115929985_H
#ifndef SEPARATINGSTREAMINGANDRENDERING_T2111089326_H
#define SEPARATINGSTREAMINGANDRENDERING_T2111089326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SeparatingStreamingAndRendering
struct  SeparatingStreamingAndRendering_t2111089326  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera SeparatingStreamingAndRendering::renderingCamera
	Camera_t4157153871 * ___renderingCamera_2;

public:
	inline static int32_t get_offset_of_renderingCamera_2() { return static_cast<int32_t>(offsetof(SeparatingStreamingAndRendering_t2111089326, ___renderingCamera_2)); }
	inline Camera_t4157153871 * get_renderingCamera_2() const { return ___renderingCamera_2; }
	inline Camera_t4157153871 ** get_address_of_renderingCamera_2() { return &___renderingCamera_2; }
	inline void set_renderingCamera_2(Camera_t4157153871 * value)
	{
		___renderingCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___renderingCamera_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEPARATINGSTREAMINGANDRENDERING_T2111089326_H
#ifndef FPS_T3702678127_H
#define FPS_T3702678127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FPS
struct  FPS_t3702678127  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text FPS::fps
	Text_t1901882714 * ___fps_2;

public:
	inline static int32_t get_offset_of_fps_2() { return static_cast<int32_t>(offsetof(FPS_t3702678127, ___fps_2)); }
	inline Text_t1901882714 * get_fps_2() const { return ___fps_2; }
	inline Text_t1901882714 ** get_address_of_fps_2() { return &___fps_2; }
	inline void set_fps_2(Text_t1901882714 * value)
	{
		___fps_2 = value;
		Il2CppCodeGenWriteBarrier((&___fps_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPS_T3702678127_H
#ifndef CAMERAJUMPBEHAVIOUR_T2963329382_H
#define CAMERAJUMPBEHAVIOUR_T2963329382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapCamera.CameraJumpBehaviour
struct  CameraJumpBehaviour_t2963329382  : public MonoBehaviour_t3962482529
{
public:
	// System.Double Wrld.MapCamera.CameraJumpBehaviour::InterestPointLatitudeDegrees
	double ___InterestPointLatitudeDegrees_2;
	// System.Double Wrld.MapCamera.CameraJumpBehaviour::InterestPointLongitudeDegrees
	double ___InterestPointLongitudeDegrees_3;
	// System.Double Wrld.MapCamera.CameraJumpBehaviour::DistanceFromInterestPoint
	double ___DistanceFromInterestPoint_4;

public:
	inline static int32_t get_offset_of_InterestPointLatitudeDegrees_2() { return static_cast<int32_t>(offsetof(CameraJumpBehaviour_t2963329382, ___InterestPointLatitudeDegrees_2)); }
	inline double get_InterestPointLatitudeDegrees_2() const { return ___InterestPointLatitudeDegrees_2; }
	inline double* get_address_of_InterestPointLatitudeDegrees_2() { return &___InterestPointLatitudeDegrees_2; }
	inline void set_InterestPointLatitudeDegrees_2(double value)
	{
		___InterestPointLatitudeDegrees_2 = value;
	}

	inline static int32_t get_offset_of_InterestPointLongitudeDegrees_3() { return static_cast<int32_t>(offsetof(CameraJumpBehaviour_t2963329382, ___InterestPointLongitudeDegrees_3)); }
	inline double get_InterestPointLongitudeDegrees_3() const { return ___InterestPointLongitudeDegrees_3; }
	inline double* get_address_of_InterestPointLongitudeDegrees_3() { return &___InterestPointLongitudeDegrees_3; }
	inline void set_InterestPointLongitudeDegrees_3(double value)
	{
		___InterestPointLongitudeDegrees_3 = value;
	}

	inline static int32_t get_offset_of_DistanceFromInterestPoint_4() { return static_cast<int32_t>(offsetof(CameraJumpBehaviour_t2963329382, ___DistanceFromInterestPoint_4)); }
	inline double get_DistanceFromInterestPoint_4() const { return ___DistanceFromInterestPoint_4; }
	inline double* get_address_of_DistanceFromInterestPoint_4() { return &___DistanceFromInterestPoint_4; }
	inline void set_DistanceFromInterestPoint_4(double value)
	{
		___DistanceFromInterestPoint_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAJUMPBEHAVIOUR_T2963329382_H
#ifndef POSITIONOBJECTATLATITUDEANDLONGITUDE_T3453145834_H
#define POSITIONOBJECTATLATITUDEANDLONGITUDE_T3453145834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PositionObjectAtLatitudeAndLongitude
struct  PositionObjectAtLatitudeAndLongitude_t3453145834  : public MonoBehaviour_t3962482529
{
public:
	// Wrld.Space.GeographicTransform PositionObjectAtLatitudeAndLongitude::coordinateFrame
	GeographicTransform_t2262143282 * ___coordinateFrame_4;
	// UnityEngine.Transform PositionObjectAtLatitudeAndLongitude::box
	Transform_t3600365921 * ___box_5;

public:
	inline static int32_t get_offset_of_coordinateFrame_4() { return static_cast<int32_t>(offsetof(PositionObjectAtLatitudeAndLongitude_t3453145834, ___coordinateFrame_4)); }
	inline GeographicTransform_t2262143282 * get_coordinateFrame_4() const { return ___coordinateFrame_4; }
	inline GeographicTransform_t2262143282 ** get_address_of_coordinateFrame_4() { return &___coordinateFrame_4; }
	inline void set_coordinateFrame_4(GeographicTransform_t2262143282 * value)
	{
		___coordinateFrame_4 = value;
		Il2CppCodeGenWriteBarrier((&___coordinateFrame_4), value);
	}

	inline static int32_t get_offset_of_box_5() { return static_cast<int32_t>(offsetof(PositionObjectAtLatitudeAndLongitude_t3453145834, ___box_5)); }
	inline Transform_t3600365921 * get_box_5() const { return ___box_5; }
	inline Transform_t3600365921 ** get_address_of_box_5() { return &___box_5; }
	inline void set_box_5(Transform_t3600365921 * value)
	{
		___box_5 = value;
		Il2CppCodeGenWriteBarrier((&___box_5), value);
	}
};

struct PositionObjectAtLatitudeAndLongitude_t3453145834_StaticFields
{
public:
	// Wrld.Space.LatLong PositionObjectAtLatitudeAndLongitude::pointA
	LatLong_t2936018554  ___pointA_2;
	// Wrld.Space.LatLong PositionObjectAtLatitudeAndLongitude::pointB
	LatLong_t2936018554  ___pointB_3;

public:
	inline static int32_t get_offset_of_pointA_2() { return static_cast<int32_t>(offsetof(PositionObjectAtLatitudeAndLongitude_t3453145834_StaticFields, ___pointA_2)); }
	inline LatLong_t2936018554  get_pointA_2() const { return ___pointA_2; }
	inline LatLong_t2936018554 * get_address_of_pointA_2() { return &___pointA_2; }
	inline void set_pointA_2(LatLong_t2936018554  value)
	{
		___pointA_2 = value;
	}

	inline static int32_t get_offset_of_pointB_3() { return static_cast<int32_t>(offsetof(PositionObjectAtLatitudeAndLongitude_t3453145834_StaticFields, ___pointB_3)); }
	inline LatLong_t2936018554  get_pointB_3() const { return ___pointB_3; }
	inline LatLong_t2936018554 * get_address_of_pointB_3() { return &___pointB_3; }
	inline void set_pointB_3(LatLong_t2936018554  value)
	{
		___pointB_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONOBJECTATLATITUDEANDLONGITUDE_T3453145834_H
#ifndef PICKINGBUILDINGS_T1444756309_H
#define PICKINGBUILDINGS_T1444756309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PickingBuildings
struct  PickingBuildings_t1444756309  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material PickingBuildings::highlightMaterial
	Material_t340375123 * ___highlightMaterial_2;
	// UnityEngine.Vector3 PickingBuildings::mouseDownPosition
	Vector3_t3722313464  ___mouseDownPosition_3;

public:
	inline static int32_t get_offset_of_highlightMaterial_2() { return static_cast<int32_t>(offsetof(PickingBuildings_t1444756309, ___highlightMaterial_2)); }
	inline Material_t340375123 * get_highlightMaterial_2() const { return ___highlightMaterial_2; }
	inline Material_t340375123 ** get_address_of_highlightMaterial_2() { return &___highlightMaterial_2; }
	inline void set_highlightMaterial_2(Material_t340375123 * value)
	{
		___highlightMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___highlightMaterial_2), value);
	}

	inline static int32_t get_offset_of_mouseDownPosition_3() { return static_cast<int32_t>(offsetof(PickingBuildings_t1444756309, ___mouseDownPosition_3)); }
	inline Vector3_t3722313464  get_mouseDownPosition_3() const { return ___mouseDownPosition_3; }
	inline Vector3_t3722313464 * get_address_of_mouseDownPosition_3() { return &___mouseDownPosition_3; }
	inline void set_mouseDownPosition_3(Vector3_t3722313464  value)
	{
		___mouseDownPosition_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PICKINGBUILDINGS_T1444756309_H
#ifndef SELECTINGBUILDINGS_T1914154491_H
#define SELECTINGBUILDINGS_T1914154491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectingBuildings
struct  SelectingBuildings_t1914154491  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject SelectingBuildings::boxPrefab
	GameObject_t1113636619 * ___boxPrefab_2;
	// Wrld.Space.LatLong SelectingBuildings::buildingLocation
	LatLong_t2936018554  ___buildingLocation_3;

public:
	inline static int32_t get_offset_of_boxPrefab_2() { return static_cast<int32_t>(offsetof(SelectingBuildings_t1914154491, ___boxPrefab_2)); }
	inline GameObject_t1113636619 * get_boxPrefab_2() const { return ___boxPrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_boxPrefab_2() { return &___boxPrefab_2; }
	inline void set_boxPrefab_2(GameObject_t1113636619 * value)
	{
		___boxPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___boxPrefab_2), value);
	}

	inline static int32_t get_offset_of_buildingLocation_3() { return static_cast<int32_t>(offsetof(SelectingBuildings_t1914154491, ___buildingLocation_3)); }
	inline LatLong_t2936018554  get_buildingLocation_3() const { return ___buildingLocation_3; }
	inline LatLong_t2936018554 * get_address_of_buildingLocation_3() { return &___buildingLocation_3; }
	inline void set_buildingLocation_3(LatLong_t2936018554  value)
	{
		___buildingLocation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTINGBUILDINGS_T1914154491_H
#ifndef FIREPROJECTILE_T4057714197_H
#define FIREPROJECTILE_T4057714197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FireProjectile
struct  FireProjectile_t4057714197  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject FireProjectile::m_projectile
	GameObject_t1113636619 * ___m_projectile_2;
	// UnityEngine.Camera FireProjectile::m_camera
	Camera_t4157153871 * ___m_camera_3;
	// System.Single FireProjectile::m_velocity
	float ___m_velocity_4;
	// System.Single FireProjectile::m_lifetime
	float ___m_lifetime_5;

public:
	inline static int32_t get_offset_of_m_projectile_2() { return static_cast<int32_t>(offsetof(FireProjectile_t4057714197, ___m_projectile_2)); }
	inline GameObject_t1113636619 * get_m_projectile_2() const { return ___m_projectile_2; }
	inline GameObject_t1113636619 ** get_address_of_m_projectile_2() { return &___m_projectile_2; }
	inline void set_m_projectile_2(GameObject_t1113636619 * value)
	{
		___m_projectile_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_projectile_2), value);
	}

	inline static int32_t get_offset_of_m_camera_3() { return static_cast<int32_t>(offsetof(FireProjectile_t4057714197, ___m_camera_3)); }
	inline Camera_t4157153871 * get_m_camera_3() const { return ___m_camera_3; }
	inline Camera_t4157153871 ** get_address_of_m_camera_3() { return &___m_camera_3; }
	inline void set_m_camera_3(Camera_t4157153871 * value)
	{
		___m_camera_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_3), value);
	}

	inline static int32_t get_offset_of_m_velocity_4() { return static_cast<int32_t>(offsetof(FireProjectile_t4057714197, ___m_velocity_4)); }
	inline float get_m_velocity_4() const { return ___m_velocity_4; }
	inline float* get_address_of_m_velocity_4() { return &___m_velocity_4; }
	inline void set_m_velocity_4(float value)
	{
		___m_velocity_4 = value;
	}

	inline static int32_t get_offset_of_m_lifetime_5() { return static_cast<int32_t>(offsetof(FireProjectile_t4057714197, ___m_lifetime_5)); }
	inline float get_m_lifetime_5() const { return ___m_lifetime_5; }
	inline float* get_address_of_m_lifetime_5() { return &___m_lifetime_5; }
	inline void set_m_lifetime_5(float value)
	{
		___m_lifetime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIREPROJECTILE_T4057714197_H
#ifndef CAMERATRANSITIONHEADINGANDTILT_T3004851910_H
#define CAMERATRANSITIONHEADINGANDTILT_T3004851910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraTransitionHeadingAndTilt
struct  CameraTransitionHeadingAndTilt_t3004851910  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERATRANSITIONHEADINGANDTILT_T3004851910_H
#ifndef CAMERATRANSITIONMOVINGCAMERA_T2676393551_H
#define CAMERATRANSITIONMOVINGCAMERA_T2676393551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraTransitionMovingCamera
struct  CameraTransitionMovingCamera_t2676393551  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERATRANSITIONMOVINGCAMERA_T2676393551_H
#ifndef EXAMPLECHANGER_T2484020936_H
#define EXAMPLECHANGER_T2484020936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleChanger
struct  ExampleChanger_t2484020936  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Dropdown ExampleChanger::m_dropdown
	Dropdown_t2274391225 * ___m_dropdown_2;
	// ExampleChanger/Example[] ExampleChanger::ExampleList
	ExampleU5BU5D_t2836667186* ___ExampleList_3;
	// System.Int32 ExampleChanger::m_currentActiveExample
	int32_t ___m_currentActiveExample_4;

public:
	inline static int32_t get_offset_of_m_dropdown_2() { return static_cast<int32_t>(offsetof(ExampleChanger_t2484020936, ___m_dropdown_2)); }
	inline Dropdown_t2274391225 * get_m_dropdown_2() const { return ___m_dropdown_2; }
	inline Dropdown_t2274391225 ** get_address_of_m_dropdown_2() { return &___m_dropdown_2; }
	inline void set_m_dropdown_2(Dropdown_t2274391225 * value)
	{
		___m_dropdown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_dropdown_2), value);
	}

	inline static int32_t get_offset_of_ExampleList_3() { return static_cast<int32_t>(offsetof(ExampleChanger_t2484020936, ___ExampleList_3)); }
	inline ExampleU5BU5D_t2836667186* get_ExampleList_3() const { return ___ExampleList_3; }
	inline ExampleU5BU5D_t2836667186** get_address_of_ExampleList_3() { return &___ExampleList_3; }
	inline void set_ExampleList_3(ExampleU5BU5D_t2836667186* value)
	{
		___ExampleList_3 = value;
		Il2CppCodeGenWriteBarrier((&___ExampleList_3), value);
	}

	inline static int32_t get_offset_of_m_currentActiveExample_4() { return static_cast<int32_t>(offsetof(ExampleChanger_t2484020936, ___m_currentActiveExample_4)); }
	inline int32_t get_m_currentActiveExample_4() const { return ___m_currentActiveExample_4; }
	inline int32_t* get_address_of_m_currentActiveExample_4() { return &___m_currentActiveExample_4; }
	inline void set_m_currentActiveExample_4(int32_t value)
	{
		___m_currentActiveExample_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLECHANGER_T2484020936_H
#ifndef HIGHLIGHTINGBUILDINGS_T423429914_H
#define HIGHLIGHTINGBUILDINGS_T423429914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HighlightingBuildings
struct  HighlightingBuildings_t423429914  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material HighlightingBuildings::material
	Material_t340375123 * ___material_2;
	// Wrld.Space.LatLong HighlightingBuildings::buildingLocation
	LatLong_t2936018554  ___buildingLocation_3;

public:
	inline static int32_t get_offset_of_material_2() { return static_cast<int32_t>(offsetof(HighlightingBuildings_t423429914, ___material_2)); }
	inline Material_t340375123 * get_material_2() const { return ___material_2; }
	inline Material_t340375123 ** get_address_of_material_2() { return &___material_2; }
	inline void set_material_2(Material_t340375123 * value)
	{
		___material_2 = value;
		Il2CppCodeGenWriteBarrier((&___material_2), value);
	}

	inline static int32_t get_offset_of_buildingLocation_3() { return static_cast<int32_t>(offsetof(HighlightingBuildings_t423429914, ___buildingLocation_3)); }
	inline LatLong_t2936018554  get_buildingLocation_3() const { return ___buildingLocation_3; }
	inline LatLong_t2936018554 * get_address_of_buildingLocation_3() { return &___buildingLocation_3; }
	inline void set_buildingLocation_3(LatLong_t2936018554  value)
	{
		___buildingLocation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIGHLIGHTINGBUILDINGS_T423429914_H
#ifndef POSITIONOBJECTATECEFCOORDINATE_T1179719296_H
#define POSITIONOBJECTATECEFCOORDINATE_T1179719296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PositionObjectAtECEFCoordinate
struct  PositionObjectAtECEFCoordinate_t1179719296  : public MonoBehaviour_t3962482529
{
public:
	// Wrld.Space.GeographicTransform PositionObjectAtECEFCoordinate::coordinateFrame
	GeographicTransform_t2262143282 * ___coordinateFrame_3;
	// UnityEngine.Transform PositionObjectAtECEFCoordinate::box
	Transform_t3600365921 * ___box_4;
	// Wrld.Space.GeographicTransform PositionObjectAtECEFCoordinate::targetCoordinateFrame
	GeographicTransform_t2262143282 * ___targetCoordinateFrame_5;

public:
	inline static int32_t get_offset_of_coordinateFrame_3() { return static_cast<int32_t>(offsetof(PositionObjectAtECEFCoordinate_t1179719296, ___coordinateFrame_3)); }
	inline GeographicTransform_t2262143282 * get_coordinateFrame_3() const { return ___coordinateFrame_3; }
	inline GeographicTransform_t2262143282 ** get_address_of_coordinateFrame_3() { return &___coordinateFrame_3; }
	inline void set_coordinateFrame_3(GeographicTransform_t2262143282 * value)
	{
		___coordinateFrame_3 = value;
		Il2CppCodeGenWriteBarrier((&___coordinateFrame_3), value);
	}

	inline static int32_t get_offset_of_box_4() { return static_cast<int32_t>(offsetof(PositionObjectAtECEFCoordinate_t1179719296, ___box_4)); }
	inline Transform_t3600365921 * get_box_4() const { return ___box_4; }
	inline Transform_t3600365921 ** get_address_of_box_4() { return &___box_4; }
	inline void set_box_4(Transform_t3600365921 * value)
	{
		___box_4 = value;
		Il2CppCodeGenWriteBarrier((&___box_4), value);
	}

	inline static int32_t get_offset_of_targetCoordinateFrame_5() { return static_cast<int32_t>(offsetof(PositionObjectAtECEFCoordinate_t1179719296, ___targetCoordinateFrame_5)); }
	inline GeographicTransform_t2262143282 * get_targetCoordinateFrame_5() const { return ___targetCoordinateFrame_5; }
	inline GeographicTransform_t2262143282 ** get_address_of_targetCoordinateFrame_5() { return &___targetCoordinateFrame_5; }
	inline void set_targetCoordinateFrame_5(GeographicTransform_t2262143282 * value)
	{
		___targetCoordinateFrame_5 = value;
		Il2CppCodeGenWriteBarrier((&___targetCoordinateFrame_5), value);
	}
};

struct PositionObjectAtECEFCoordinate_t1179719296_StaticFields
{
public:
	// Wrld.Space.LatLong PositionObjectAtECEFCoordinate::cameraPosition
	LatLong_t2936018554  ___cameraPosition_2;

public:
	inline static int32_t get_offset_of_cameraPosition_2() { return static_cast<int32_t>(offsetof(PositionObjectAtECEFCoordinate_t1179719296_StaticFields, ___cameraPosition_2)); }
	inline LatLong_t2936018554  get_cameraPosition_2() const { return ___cameraPosition_2; }
	inline LatLong_t2936018554 * get_address_of_cameraPosition_2() { return &___cameraPosition_2; }
	inline void set_cameraPosition_2(LatLong_t2936018554  value)
	{
		___cameraPosition_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONOBJECTATECEFCOORDINATE_T1179719296_H
#ifndef MOVEOBJECTONMAP_T383061840_H
#define MOVEOBJECTONMAP_T383061840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveObjectOnMap
struct  MoveObjectOnMap_t383061840  : public MonoBehaviour_t3962482529
{
public:
	// Wrld.Space.GeographicTransform MoveObjectOnMap::coordinateFrame
	GeographicTransform_t2262143282 * ___coordinateFrame_4;
	// UnityEngine.Transform MoveObjectOnMap::box
	Transform_t3600365921 * ___box_5;

public:
	inline static int32_t get_offset_of_coordinateFrame_4() { return static_cast<int32_t>(offsetof(MoveObjectOnMap_t383061840, ___coordinateFrame_4)); }
	inline GeographicTransform_t2262143282 * get_coordinateFrame_4() const { return ___coordinateFrame_4; }
	inline GeographicTransform_t2262143282 ** get_address_of_coordinateFrame_4() { return &___coordinateFrame_4; }
	inline void set_coordinateFrame_4(GeographicTransform_t2262143282 * value)
	{
		___coordinateFrame_4 = value;
		Il2CppCodeGenWriteBarrier((&___coordinateFrame_4), value);
	}

	inline static int32_t get_offset_of_box_5() { return static_cast<int32_t>(offsetof(MoveObjectOnMap_t383061840, ___box_5)); }
	inline Transform_t3600365921 * get_box_5() const { return ___box_5; }
	inline Transform_t3600365921 ** get_address_of_box_5() { return &___box_5; }
	inline void set_box_5(Transform_t3600365921 * value)
	{
		___box_5 = value;
		Il2CppCodeGenWriteBarrier((&___box_5), value);
	}
};

struct MoveObjectOnMap_t383061840_StaticFields
{
public:
	// Wrld.Space.LatLong MoveObjectOnMap::startPosition
	LatLong_t2936018554  ___startPosition_2;
	// System.Single MoveObjectOnMap::movementSpeed
	float ___movementSpeed_3;

public:
	inline static int32_t get_offset_of_startPosition_2() { return static_cast<int32_t>(offsetof(MoveObjectOnMap_t383061840_StaticFields, ___startPosition_2)); }
	inline LatLong_t2936018554  get_startPosition_2() const { return ___startPosition_2; }
	inline LatLong_t2936018554 * get_address_of_startPosition_2() { return &___startPosition_2; }
	inline void set_startPosition_2(LatLong_t2936018554  value)
	{
		___startPosition_2 = value;
	}

	inline static int32_t get_offset_of_movementSpeed_3() { return static_cast<int32_t>(offsetof(MoveObjectOnMap_t383061840_StaticFields, ___movementSpeed_3)); }
	inline float get_movementSpeed_3() const { return ___movementSpeed_3; }
	inline float* get_address_of_movementSpeed_3() { return &___movementSpeed_3; }
	inline void set_movementSpeed_3(float value)
	{
		___movementSpeed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEOBJECTONMAP_T383061840_H
#ifndef FLYOBJECTOVERMAP_T3625196572_H
#define FLYOBJECTOVERMAP_T3625196572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlyObjectOverMap
struct  FlyObjectOverMap_t3625196572  : public MonoBehaviour_t3962482529
{
public:
	// Wrld.Space.GeographicTransform FlyObjectOverMap::coordinateFrame
	GeographicTransform_t2262143282 * ___coordinateFrame_5;
	// UnityEngine.Transform FlyObjectOverMap::box
	Transform_t3600365921 * ___box_6;

public:
	inline static int32_t get_offset_of_coordinateFrame_5() { return static_cast<int32_t>(offsetof(FlyObjectOverMap_t3625196572, ___coordinateFrame_5)); }
	inline GeographicTransform_t2262143282 * get_coordinateFrame_5() const { return ___coordinateFrame_5; }
	inline GeographicTransform_t2262143282 ** get_address_of_coordinateFrame_5() { return &___coordinateFrame_5; }
	inline void set_coordinateFrame_5(GeographicTransform_t2262143282 * value)
	{
		___coordinateFrame_5 = value;
		Il2CppCodeGenWriteBarrier((&___coordinateFrame_5), value);
	}

	inline static int32_t get_offset_of_box_6() { return static_cast<int32_t>(offsetof(FlyObjectOverMap_t3625196572, ___box_6)); }
	inline Transform_t3600365921 * get_box_6() const { return ___box_6; }
	inline Transform_t3600365921 ** get_address_of_box_6() { return &___box_6; }
	inline void set_box_6(Transform_t3600365921 * value)
	{
		___box_6 = value;
		Il2CppCodeGenWriteBarrier((&___box_6), value);
	}
};

struct FlyObjectOverMap_t3625196572_StaticFields
{
public:
	// Wrld.Space.LatLong FlyObjectOverMap::startPosition
	LatLong_t2936018554  ___startPosition_2;
	// System.Single FlyObjectOverMap::movementSpeed
	float ___movementSpeed_3;
	// System.Single FlyObjectOverMap::turningSpeed
	float ___turningSpeed_4;

public:
	inline static int32_t get_offset_of_startPosition_2() { return static_cast<int32_t>(offsetof(FlyObjectOverMap_t3625196572_StaticFields, ___startPosition_2)); }
	inline LatLong_t2936018554  get_startPosition_2() const { return ___startPosition_2; }
	inline LatLong_t2936018554 * get_address_of_startPosition_2() { return &___startPosition_2; }
	inline void set_startPosition_2(LatLong_t2936018554  value)
	{
		___startPosition_2 = value;
	}

	inline static int32_t get_offset_of_movementSpeed_3() { return static_cast<int32_t>(offsetof(FlyObjectOverMap_t3625196572_StaticFields, ___movementSpeed_3)); }
	inline float get_movementSpeed_3() const { return ___movementSpeed_3; }
	inline float* get_address_of_movementSpeed_3() { return &___movementSpeed_3; }
	inline void set_movementSpeed_3(float value)
	{
		___movementSpeed_3 = value;
	}

	inline static int32_t get_offset_of_turningSpeed_4() { return static_cast<int32_t>(offsetof(FlyObjectOverMap_t3625196572_StaticFields, ___turningSpeed_4)); }
	inline float get_turningSpeed_4() const { return ___turningSpeed_4; }
	inline float* get_address_of_turningSpeed_4() { return &___turningSpeed_4; }
	inline void set_turningSpeed_4(float value)
	{
		___turningSpeed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLYOBJECTOVERMAP_T3625196572_H
#ifndef PROJECTILE_T1440994518_H
#define PROJECTILE_T1440994518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Projectile
struct  Projectile_t1440994518  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Projectile::Lifetime
	float ___Lifetime_2;

public:
	inline static int32_t get_offset_of_Lifetime_2() { return static_cast<int32_t>(offsetof(Projectile_t1440994518, ___Lifetime_2)); }
	inline float get_Lifetime_2() const { return ___Lifetime_2; }
	inline float* get_address_of_Lifetime_2() { return &___Lifetime_2; }
	inline void set_Lifetime_2(float value)
	{
		___Lifetime_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTILE_T1440994518_H
#ifndef WRLDMAP_T2502476010_H
#define WRLDMAP_T2502476010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WrldMap
struct  WrldMap_t2502476010  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Camera WrldMap::m_streamingCamera
	Camera_t4157153871 * ___m_streamingCamera_2;
	// System.String WrldMap::m_apiKey
	String_t* ___m_apiKey_3;
	// System.Double WrldMap::m_latitudeDegrees
	double ___m_latitudeDegrees_4;
	// System.Double WrldMap::m_longitudeDegrees
	double ___m_longitudeDegrees_5;
	// System.Double WrldMap::m_distanceToInterest
	double ___m_distanceToInterest_6;
	// System.Double WrldMap::m_headingDegrees
	double ___m_headingDegrees_7;
	// Wrld.CoordinateSystem WrldMap::m_coordSystem
	int32_t ___m_coordSystem_8;
	// System.Boolean WrldMap::m_streamingLodBasedOnDistance
	bool ___m_streamingLodBasedOnDistance_9;
	// System.String WrldMap::m_materialDirectory
	String_t* ___m_materialDirectory_10;
	// UnityEngine.Material WrldMap::m_overrideLandmarkMaterial
	Material_t340375123 * ___m_overrideLandmarkMaterial_11;
	// System.Boolean WrldMap::m_useBuiltInCameraControls
	bool ___m_useBuiltInCameraControls_12;
	// System.Boolean WrldMap::m_terrainCollisions
	bool ___m_terrainCollisions_13;
	// System.Boolean WrldMap::m_roadCollisions
	bool ___m_roadCollisions_14;
	// System.Boolean WrldMap::m_buildingCollisions
	bool ___m_buildingCollisions_15;
	// Wrld.Api WrldMap::m_api
	Api_t1190036922 * ___m_api_16;

public:
	inline static int32_t get_offset_of_m_streamingCamera_2() { return static_cast<int32_t>(offsetof(WrldMap_t2502476010, ___m_streamingCamera_2)); }
	inline Camera_t4157153871 * get_m_streamingCamera_2() const { return ___m_streamingCamera_2; }
	inline Camera_t4157153871 ** get_address_of_m_streamingCamera_2() { return &___m_streamingCamera_2; }
	inline void set_m_streamingCamera_2(Camera_t4157153871 * value)
	{
		___m_streamingCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_streamingCamera_2), value);
	}

	inline static int32_t get_offset_of_m_apiKey_3() { return static_cast<int32_t>(offsetof(WrldMap_t2502476010, ___m_apiKey_3)); }
	inline String_t* get_m_apiKey_3() const { return ___m_apiKey_3; }
	inline String_t** get_address_of_m_apiKey_3() { return &___m_apiKey_3; }
	inline void set_m_apiKey_3(String_t* value)
	{
		___m_apiKey_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_apiKey_3), value);
	}

	inline static int32_t get_offset_of_m_latitudeDegrees_4() { return static_cast<int32_t>(offsetof(WrldMap_t2502476010, ___m_latitudeDegrees_4)); }
	inline double get_m_latitudeDegrees_4() const { return ___m_latitudeDegrees_4; }
	inline double* get_address_of_m_latitudeDegrees_4() { return &___m_latitudeDegrees_4; }
	inline void set_m_latitudeDegrees_4(double value)
	{
		___m_latitudeDegrees_4 = value;
	}

	inline static int32_t get_offset_of_m_longitudeDegrees_5() { return static_cast<int32_t>(offsetof(WrldMap_t2502476010, ___m_longitudeDegrees_5)); }
	inline double get_m_longitudeDegrees_5() const { return ___m_longitudeDegrees_5; }
	inline double* get_address_of_m_longitudeDegrees_5() { return &___m_longitudeDegrees_5; }
	inline void set_m_longitudeDegrees_5(double value)
	{
		___m_longitudeDegrees_5 = value;
	}

	inline static int32_t get_offset_of_m_distanceToInterest_6() { return static_cast<int32_t>(offsetof(WrldMap_t2502476010, ___m_distanceToInterest_6)); }
	inline double get_m_distanceToInterest_6() const { return ___m_distanceToInterest_6; }
	inline double* get_address_of_m_distanceToInterest_6() { return &___m_distanceToInterest_6; }
	inline void set_m_distanceToInterest_6(double value)
	{
		___m_distanceToInterest_6 = value;
	}

	inline static int32_t get_offset_of_m_headingDegrees_7() { return static_cast<int32_t>(offsetof(WrldMap_t2502476010, ___m_headingDegrees_7)); }
	inline double get_m_headingDegrees_7() const { return ___m_headingDegrees_7; }
	inline double* get_address_of_m_headingDegrees_7() { return &___m_headingDegrees_7; }
	inline void set_m_headingDegrees_7(double value)
	{
		___m_headingDegrees_7 = value;
	}

	inline static int32_t get_offset_of_m_coordSystem_8() { return static_cast<int32_t>(offsetof(WrldMap_t2502476010, ___m_coordSystem_8)); }
	inline int32_t get_m_coordSystem_8() const { return ___m_coordSystem_8; }
	inline int32_t* get_address_of_m_coordSystem_8() { return &___m_coordSystem_8; }
	inline void set_m_coordSystem_8(int32_t value)
	{
		___m_coordSystem_8 = value;
	}

	inline static int32_t get_offset_of_m_streamingLodBasedOnDistance_9() { return static_cast<int32_t>(offsetof(WrldMap_t2502476010, ___m_streamingLodBasedOnDistance_9)); }
	inline bool get_m_streamingLodBasedOnDistance_9() const { return ___m_streamingLodBasedOnDistance_9; }
	inline bool* get_address_of_m_streamingLodBasedOnDistance_9() { return &___m_streamingLodBasedOnDistance_9; }
	inline void set_m_streamingLodBasedOnDistance_9(bool value)
	{
		___m_streamingLodBasedOnDistance_9 = value;
	}

	inline static int32_t get_offset_of_m_materialDirectory_10() { return static_cast<int32_t>(offsetof(WrldMap_t2502476010, ___m_materialDirectory_10)); }
	inline String_t* get_m_materialDirectory_10() const { return ___m_materialDirectory_10; }
	inline String_t** get_address_of_m_materialDirectory_10() { return &___m_materialDirectory_10; }
	inline void set_m_materialDirectory_10(String_t* value)
	{
		___m_materialDirectory_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialDirectory_10), value);
	}

	inline static int32_t get_offset_of_m_overrideLandmarkMaterial_11() { return static_cast<int32_t>(offsetof(WrldMap_t2502476010, ___m_overrideLandmarkMaterial_11)); }
	inline Material_t340375123 * get_m_overrideLandmarkMaterial_11() const { return ___m_overrideLandmarkMaterial_11; }
	inline Material_t340375123 ** get_address_of_m_overrideLandmarkMaterial_11() { return &___m_overrideLandmarkMaterial_11; }
	inline void set_m_overrideLandmarkMaterial_11(Material_t340375123 * value)
	{
		___m_overrideLandmarkMaterial_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_overrideLandmarkMaterial_11), value);
	}

	inline static int32_t get_offset_of_m_useBuiltInCameraControls_12() { return static_cast<int32_t>(offsetof(WrldMap_t2502476010, ___m_useBuiltInCameraControls_12)); }
	inline bool get_m_useBuiltInCameraControls_12() const { return ___m_useBuiltInCameraControls_12; }
	inline bool* get_address_of_m_useBuiltInCameraControls_12() { return &___m_useBuiltInCameraControls_12; }
	inline void set_m_useBuiltInCameraControls_12(bool value)
	{
		___m_useBuiltInCameraControls_12 = value;
	}

	inline static int32_t get_offset_of_m_terrainCollisions_13() { return static_cast<int32_t>(offsetof(WrldMap_t2502476010, ___m_terrainCollisions_13)); }
	inline bool get_m_terrainCollisions_13() const { return ___m_terrainCollisions_13; }
	inline bool* get_address_of_m_terrainCollisions_13() { return &___m_terrainCollisions_13; }
	inline void set_m_terrainCollisions_13(bool value)
	{
		___m_terrainCollisions_13 = value;
	}

	inline static int32_t get_offset_of_m_roadCollisions_14() { return static_cast<int32_t>(offsetof(WrldMap_t2502476010, ___m_roadCollisions_14)); }
	inline bool get_m_roadCollisions_14() const { return ___m_roadCollisions_14; }
	inline bool* get_address_of_m_roadCollisions_14() { return &___m_roadCollisions_14; }
	inline void set_m_roadCollisions_14(bool value)
	{
		___m_roadCollisions_14 = value;
	}

	inline static int32_t get_offset_of_m_buildingCollisions_15() { return static_cast<int32_t>(offsetof(WrldMap_t2502476010, ___m_buildingCollisions_15)); }
	inline bool get_m_buildingCollisions_15() const { return ___m_buildingCollisions_15; }
	inline bool* get_address_of_m_buildingCollisions_15() { return &___m_buildingCollisions_15; }
	inline void set_m_buildingCollisions_15(bool value)
	{
		___m_buildingCollisions_15 = value;
	}

	inline static int32_t get_offset_of_m_api_16() { return static_cast<int32_t>(offsetof(WrldMap_t2502476010, ___m_api_16)); }
	inline Api_t1190036922 * get_m_api_16() const { return ___m_api_16; }
	inline Api_t1190036922 ** get_address_of_m_api_16() { return &___m_api_16; }
	inline void set_m_api_16(Api_t1190036922 * value)
	{
		___m_api_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_api_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRLDMAP_T2502476010_H
#ifndef BUILDINGALTITUDEPICKING_T1082500057_H
#define BUILDINGALTITUDEPICKING_T1082500057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingAltitudePicking
struct  BuildingAltitudePicking_t1082500057  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject BuildingAltitudePicking::boxPrefab
	GameObject_t1113636619 * ___boxPrefab_2;
	// Wrld.Space.LatLong BuildingAltitudePicking::cameraLocation
	LatLong_t2936018554  ___cameraLocation_3;
	// Wrld.Space.LatLong BuildingAltitudePicking::boxLocation1
	LatLong_t2936018554  ___boxLocation1_4;
	// Wrld.Space.LatLong BuildingAltitudePicking::boxLocation2
	LatLong_t2936018554  ___boxLocation2_5;

public:
	inline static int32_t get_offset_of_boxPrefab_2() { return static_cast<int32_t>(offsetof(BuildingAltitudePicking_t1082500057, ___boxPrefab_2)); }
	inline GameObject_t1113636619 * get_boxPrefab_2() const { return ___boxPrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_boxPrefab_2() { return &___boxPrefab_2; }
	inline void set_boxPrefab_2(GameObject_t1113636619 * value)
	{
		___boxPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___boxPrefab_2), value);
	}

	inline static int32_t get_offset_of_cameraLocation_3() { return static_cast<int32_t>(offsetof(BuildingAltitudePicking_t1082500057, ___cameraLocation_3)); }
	inline LatLong_t2936018554  get_cameraLocation_3() const { return ___cameraLocation_3; }
	inline LatLong_t2936018554 * get_address_of_cameraLocation_3() { return &___cameraLocation_3; }
	inline void set_cameraLocation_3(LatLong_t2936018554  value)
	{
		___cameraLocation_3 = value;
	}

	inline static int32_t get_offset_of_boxLocation1_4() { return static_cast<int32_t>(offsetof(BuildingAltitudePicking_t1082500057, ___boxLocation1_4)); }
	inline LatLong_t2936018554  get_boxLocation1_4() const { return ___boxLocation1_4; }
	inline LatLong_t2936018554 * get_address_of_boxLocation1_4() { return &___boxLocation1_4; }
	inline void set_boxLocation1_4(LatLong_t2936018554  value)
	{
		___boxLocation1_4 = value;
	}

	inline static int32_t get_offset_of_boxLocation2_5() { return static_cast<int32_t>(offsetof(BuildingAltitudePicking_t1082500057, ___boxLocation2_5)); }
	inline LatLong_t2936018554  get_boxLocation2_5() const { return ___boxLocation2_5; }
	inline LatLong_t2936018554 * get_address_of_boxLocation2_5() { return &___boxLocation2_5; }
	inline void set_boxLocation2_5(LatLong_t2936018554  value)
	{
		___boxLocation2_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDINGALTITUDEPICKING_T1082500057_H
#ifndef CLEARINGBUILDINGHIGHLIGHTS_T199549764_H
#define CLEARINGBUILDINGHIGHLIGHTS_T199549764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClearingBuildingHighlights
struct  ClearingBuildingHighlights_t199549764  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material ClearingBuildingHighlights::material
	Material_t340375123 * ___material_2;
	// Wrld.Space.LatLong ClearingBuildingHighlights::buildingLocation
	LatLong_t2936018554  ___buildingLocation_3;

public:
	inline static int32_t get_offset_of_material_2() { return static_cast<int32_t>(offsetof(ClearingBuildingHighlights_t199549764, ___material_2)); }
	inline Material_t340375123 * get_material_2() const { return ___material_2; }
	inline Material_t340375123 ** get_address_of_material_2() { return &___material_2; }
	inline void set_material_2(Material_t340375123 * value)
	{
		___material_2 = value;
		Il2CppCodeGenWriteBarrier((&___material_2), value);
	}

	inline static int32_t get_offset_of_buildingLocation_3() { return static_cast<int32_t>(offsetof(ClearingBuildingHighlights_t199549764, ___buildingLocation_3)); }
	inline LatLong_t2936018554  get_buildingLocation_3() const { return ___buildingLocation_3; }
	inline LatLong_t2936018554 * get_address_of_buildingLocation_3() { return &___buildingLocation_3; }
	inline void set_buildingLocation_3(LatLong_t2936018554  value)
	{
		___buildingLocation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLEARINGBUILDINGHIGHLIGHTS_T199549764_H
#ifndef CAMERATRANSITIONANIMATE_T512914638_H
#define CAMERATRANSITIONANIMATE_T512914638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraTransitionAnimate
struct  CameraTransitionAnimate_t512914638  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERATRANSITIONANIMATE_T512914638_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3800 = { sizeof (ARUserAnchorUpdated_t4007601678), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3801 = { sizeof (ARUserAnchorRemoved_t23344545), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3802 = { sizeof (ARFaceAnchorAdded_t3526051790), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3803 = { sizeof (ARFaceAnchorUpdated_t3258688950), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3804 = { sizeof (ARFaceAnchorRemoved_t2550278937), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3805 = { sizeof (ARSessionFailed_t2125002991), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3806 = { sizeof (ARSessionCallback_t3772093212), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3807 = { sizeof (ARSessionTrackingChanged_t923029411), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3808 = { sizeof (internal_ARFrameUpdate_t3254989823), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3809 = { sizeof (internal_ARAnchorAdded_t1565083332), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3810 = { sizeof (internal_ARAnchorUpdated_t2645242205), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3811 = { sizeof (internal_ARAnchorRemoved_t3371657877), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3812 = { sizeof (internal_ARUserAnchorAdded_t3285282493), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3813 = { sizeof (internal_ARUserAnchorUpdated_t3964727538), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3814 = { sizeof (internal_ARUserAnchorRemoved_t386858594), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3815 = { sizeof (internal_ARFaceAnchorAdded_t1021040265), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3816 = { sizeof (internal_ARFaceAnchorUpdated_t3423900432), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3817 = { sizeof (internal_ARFaceAnchorRemoved_t2563439402), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3818 = { sizeof (internal_ARSessionTrackingChanged_t1988849735), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3819 = { sizeof (UnityARKitPluginSettings_t2201217663), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3819[2] = 
{
	UnityARKitPluginSettings_t2201217663::get_offset_of_m_ARKitUsesFacetracking_2(),
	UnityARKitPluginSettings_t2201217663::get_offset_of_AppRequiresARKit_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3820 = { sizeof (CoordinateSystem_t2504309499)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3820[3] = 
{
	CoordinateSystem_t2504309499::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3821 = { sizeof (Api_t1190036922), -1, sizeof(Api_t1190036922_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3821[5] = 
{
	Api_t1190036922_StaticFields::get_offset_of_Instance_0(),
	Api_t1190036922::get_offset_of_CollisionStates_1(),
	0,
	0,
	Api_t1190036922::get_offset_of_m_implementation_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3822 = { sizeof (WrldMap_t2502476010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3822[15] = 
{
	WrldMap_t2502476010::get_offset_of_m_streamingCamera_2(),
	WrldMap_t2502476010::get_offset_of_m_apiKey_3(),
	WrldMap_t2502476010::get_offset_of_m_latitudeDegrees_4(),
	WrldMap_t2502476010::get_offset_of_m_longitudeDegrees_5(),
	WrldMap_t2502476010::get_offset_of_m_distanceToInterest_6(),
	WrldMap_t2502476010::get_offset_of_m_headingDegrees_7(),
	WrldMap_t2502476010::get_offset_of_m_coordSystem_8(),
	WrldMap_t2502476010::get_offset_of_m_streamingLodBasedOnDistance_9(),
	WrldMap_t2502476010::get_offset_of_m_materialDirectory_10(),
	WrldMap_t2502476010::get_offset_of_m_overrideLandmarkMaterial_11(),
	WrldMap_t2502476010::get_offset_of_m_useBuiltInCameraControls_12(),
	WrldMap_t2502476010::get_offset_of_m_terrainCollisions_13(),
	WrldMap_t2502476010::get_offset_of_m_roadCollisions_14(),
	WrldMap_t2502476010::get_offset_of_m_buildingCollisions_15(),
	WrldMap_t2502476010::get_offset_of_m_api_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3823 = { sizeof (BuildingAltitudePicking_t1082500057), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3823[4] = 
{
	BuildingAltitudePicking_t1082500057::get_offset_of_boxPrefab_2(),
	BuildingAltitudePicking_t1082500057::get_offset_of_cameraLocation_3(),
	BuildingAltitudePicking_t1082500057::get_offset_of_boxLocation1_4(),
	BuildingAltitudePicking_t1082500057::get_offset_of_boxLocation2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3824 = { sizeof (U3CExampleU3Ec__Iterator0_t1959257276), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3824[4] = 
{
	U3CExampleU3Ec__Iterator0_t1959257276::get_offset_of_U24this_0(),
	U3CExampleU3Ec__Iterator0_t1959257276::get_offset_of_U24current_1(),
	U3CExampleU3Ec__Iterator0_t1959257276::get_offset_of_U24disposing_2(),
	U3CExampleU3Ec__Iterator0_t1959257276::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3825 = { sizeof (ClearingBuildingHighlights_t199549764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3825[2] = 
{
	ClearingBuildingHighlights_t199549764::get_offset_of_material_2(),
	ClearingBuildingHighlights_t199549764::get_offset_of_buildingLocation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3826 = { sizeof (U3CExampleU3Ec__Iterator0_t93868077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3826[4] = 
{
	U3CExampleU3Ec__Iterator0_t93868077::get_offset_of_U24this_0(),
	U3CExampleU3Ec__Iterator0_t93868077::get_offset_of_U24current_1(),
	U3CExampleU3Ec__Iterator0_t93868077::get_offset_of_U24disposing_2(),
	U3CExampleU3Ec__Iterator0_t93868077::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3827 = { sizeof (U3CClearHighlightU3Ec__Iterator1_t1353935622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3827[4] = 
{
	U3CClearHighlightU3Ec__Iterator1_t1353935622::get_offset_of_highlight_0(),
	U3CClearHighlightU3Ec__Iterator1_t1353935622::get_offset_of_U24current_1(),
	U3CClearHighlightU3Ec__Iterator1_t1353935622::get_offset_of_U24disposing_2(),
	U3CClearHighlightU3Ec__Iterator1_t1353935622::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3828 = { sizeof (HighlightingBuildings_t423429914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3828[2] = 
{
	HighlightingBuildings_t423429914::get_offset_of_material_2(),
	HighlightingBuildings_t423429914::get_offset_of_buildingLocation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3829 = { sizeof (U3CExampleU3Ec__Iterator0_t3905759373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3829[4] = 
{
	U3CExampleU3Ec__Iterator0_t3905759373::get_offset_of_U24this_0(),
	U3CExampleU3Ec__Iterator0_t3905759373::get_offset_of_U24current_1(),
	U3CExampleU3Ec__Iterator0_t3905759373::get_offset_of_U24disposing_2(),
	U3CExampleU3Ec__Iterator0_t3905759373::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3830 = { sizeof (PickingBuildings_t1444756309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3830[2] = 
{
	PickingBuildings_t1444756309::get_offset_of_highlightMaterial_2(),
	PickingBuildings_t1444756309::get_offset_of_mouseDownPosition_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3831 = { sizeof (U3CClearHighlightU3Ec__Iterator0_t4062986860), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3831[4] = 
{
	U3CClearHighlightU3Ec__Iterator0_t4062986860::get_offset_of_highlight_0(),
	U3CClearHighlightU3Ec__Iterator0_t4062986860::get_offset_of_U24current_1(),
	U3CClearHighlightU3Ec__Iterator0_t4062986860::get_offset_of_U24disposing_2(),
	U3CClearHighlightU3Ec__Iterator0_t4062986860::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3832 = { sizeof (SelectingBuildings_t1914154491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3832[2] = 
{
	SelectingBuildings_t1914154491::get_offset_of_boxPrefab_2(),
	SelectingBuildings_t1914154491::get_offset_of_buildingLocation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3833 = { sizeof (U3CExampleU3Ec__Iterator0_t3005514146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3833[4] = 
{
	U3CExampleU3Ec__Iterator0_t3005514146::get_offset_of_U24this_0(),
	U3CExampleU3Ec__Iterator0_t3005514146::get_offset_of_U24current_1(),
	U3CExampleU3Ec__Iterator0_t3005514146::get_offset_of_U24disposing_2(),
	U3CExampleU3Ec__Iterator0_t3005514146::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3834 = { sizeof (CameraTransitionAnimate_t512914638), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3835 = { sizeof (U3CExampleU3Ec__Iterator0_t1338300621), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3835[5] = 
{
	U3CExampleU3Ec__Iterator0_t1338300621::get_offset_of_U3CstartLocationU3E__0_0(),
	U3CExampleU3Ec__Iterator0_t1338300621::get_offset_of_U3CdestLocationU3E__0_1(),
	U3CExampleU3Ec__Iterator0_t1338300621::get_offset_of_U24current_2(),
	U3CExampleU3Ec__Iterator0_t1338300621::get_offset_of_U24disposing_3(),
	U3CExampleU3Ec__Iterator0_t1338300621::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3836 = { sizeof (CameraTransitionHeadingAndTilt_t3004851910), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3837 = { sizeof (U3CExampleU3Ec__Iterator0_t526138521), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3837[5] = 
{
	U3CExampleU3Ec__Iterator0_t526138521::get_offset_of_U3CstartLocationU3E__0_0(),
	U3CExampleU3Ec__Iterator0_t526138521::get_offset_of_U3CendLocationU3E__0_1(),
	U3CExampleU3Ec__Iterator0_t526138521::get_offset_of_U24current_2(),
	U3CExampleU3Ec__Iterator0_t526138521::get_offset_of_U24disposing_3(),
	U3CExampleU3Ec__Iterator0_t526138521::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3838 = { sizeof (CameraTransitionMovingCamera_t2676393551), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3839 = { sizeof (U3CExampleU3Ec__Iterator0_t1732569348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3839[5] = 
{
	U3CExampleU3Ec__Iterator0_t1732569348::get_offset_of_U3CstartLocationU3E__0_0(),
	U3CExampleU3Ec__Iterator0_t1732569348::get_offset_of_U3CdestLocationU3E__0_1(),
	U3CExampleU3Ec__Iterator0_t1732569348::get_offset_of_U24current_2(),
	U3CExampleU3Ec__Iterator0_t1732569348::get_offset_of_U24disposing_3(),
	U3CExampleU3Ec__Iterator0_t1732569348::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3840 = { sizeof (ExampleChanger_t2484020936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3840[3] = 
{
	ExampleChanger_t2484020936::get_offset_of_m_dropdown_2(),
	ExampleChanger_t2484020936::get_offset_of_ExampleList_3(),
	ExampleChanger_t2484020936::get_offset_of_m_currentActiveExample_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3841 = { sizeof (Example_t2853439971)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3841[2] = 
{
	Example_t2853439971::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Example_t2853439971::get_offset_of_Root_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3842 = { sizeof (FireProjectile_t4057714197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3842[4] = 
{
	FireProjectile_t4057714197::get_offset_of_m_projectile_2(),
	FireProjectile_t4057714197::get_offset_of_m_camera_3(),
	FireProjectile_t4057714197::get_offset_of_m_velocity_4(),
	FireProjectile_t4057714197::get_offset_of_m_lifetime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3843 = { sizeof (Projectile_t1440994518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3843[1] = 
{
	Projectile_t1440994518::get_offset_of_Lifetime_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3844 = { sizeof (FlyObjectOverMap_t3625196572), -1, sizeof(FlyObjectOverMap_t3625196572_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3844[5] = 
{
	FlyObjectOverMap_t3625196572_StaticFields::get_offset_of_startPosition_2(),
	FlyObjectOverMap_t3625196572_StaticFields::get_offset_of_movementSpeed_3(),
	FlyObjectOverMap_t3625196572_StaticFields::get_offset_of_turningSpeed_4(),
	FlyObjectOverMap_t3625196572::get_offset_of_coordinateFrame_5(),
	FlyObjectOverMap_t3625196572::get_offset_of_box_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3845 = { sizeof (MoveObjectOnMap_t383061840), -1, sizeof(MoveObjectOnMap_t383061840_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3845[4] = 
{
	MoveObjectOnMap_t383061840_StaticFields::get_offset_of_startPosition_2(),
	MoveObjectOnMap_t383061840_StaticFields::get_offset_of_movementSpeed_3(),
	MoveObjectOnMap_t383061840::get_offset_of_coordinateFrame_4(),
	MoveObjectOnMap_t383061840::get_offset_of_box_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3846 = { sizeof (U3CExampleU3Ec__Iterator0_t1380115621), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3846[7] = 
{
	U3CExampleU3Ec__Iterator0_t1380115621::get_offset_of_U3CdirectionU3E__0_0(),
	U3CExampleU3Ec__Iterator0_t1380115621::get_offset_of_U3CforwardTimeElapsedU3E__1_1(),
	U3CExampleU3Ec__Iterator0_t1380115621::get_offset_of_U3CmovementU3E__2_2(),
	U3CExampleU3Ec__Iterator0_t1380115621::get_offset_of_U24this_3(),
	U3CExampleU3Ec__Iterator0_t1380115621::get_offset_of_U24current_4(),
	U3CExampleU3Ec__Iterator0_t1380115621::get_offset_of_U24disposing_5(),
	U3CExampleU3Ec__Iterator0_t1380115621::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3847 = { sizeof (PositionObjectAtECEFCoordinate_t1179719296), -1, sizeof(PositionObjectAtECEFCoordinate_t1179719296_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3847[4] = 
{
	PositionObjectAtECEFCoordinate_t1179719296_StaticFields::get_offset_of_cameraPosition_2(),
	PositionObjectAtECEFCoordinate_t1179719296::get_offset_of_coordinateFrame_3(),
	PositionObjectAtECEFCoordinate_t1179719296::get_offset_of_box_4(),
	PositionObjectAtECEFCoordinate_t1179719296::get_offset_of_targetCoordinateFrame_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3848 = { sizeof (U3CExampleU3Ec__Iterator0_t2007946409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3848[6] = 
{
	U3CExampleU3Ec__Iterator0_t2007946409::get_offset_of_U3CecefPointAU3E__0_0(),
	U3CExampleU3Ec__Iterator0_t2007946409::get_offset_of_U3CecefPointBU3E__0_1(),
	U3CExampleU3Ec__Iterator0_t2007946409::get_offset_of_U24this_2(),
	U3CExampleU3Ec__Iterator0_t2007946409::get_offset_of_U24current_3(),
	U3CExampleU3Ec__Iterator0_t2007946409::get_offset_of_U24disposing_4(),
	U3CExampleU3Ec__Iterator0_t2007946409::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3849 = { sizeof (PositionObjectAtLatitudeAndLongitude_t3453145834), -1, sizeof(PositionObjectAtLatitudeAndLongitude_t3453145834_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3849[4] = 
{
	PositionObjectAtLatitudeAndLongitude_t3453145834_StaticFields::get_offset_of_pointA_2(),
	PositionObjectAtLatitudeAndLongitude_t3453145834_StaticFields::get_offset_of_pointB_3(),
	PositionObjectAtLatitudeAndLongitude_t3453145834::get_offset_of_coordinateFrame_4(),
	PositionObjectAtLatitudeAndLongitude_t3453145834::get_offset_of_box_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3850 = { sizeof (U3CExampleU3Ec__Iterator0_t4264857786), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3850[4] = 
{
	U3CExampleU3Ec__Iterator0_t4264857786::get_offset_of_U24this_0(),
	U3CExampleU3Ec__Iterator0_t4264857786::get_offset_of_U24current_1(),
	U3CExampleU3Ec__Iterator0_t4264857786::get_offset_of_U24disposing_2(),
	U3CExampleU3Ec__Iterator0_t4264857786::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3851 = { sizeof (RotateObjectOnMap_t115929985), -1, sizeof(RotateObjectOnMap_t115929985_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3851[4] = 
{
	RotateObjectOnMap_t115929985_StaticFields::get_offset_of_startPosition_2(),
	RotateObjectOnMap_t115929985_StaticFields::get_offset_of_rotationSpeed_3(),
	RotateObjectOnMap_t115929985::get_offset_of_coordinateFrame_4(),
	RotateObjectOnMap_t115929985::get_offset_of_box_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3852 = { sizeof (U3CExampleU3Ec__Iterator0_t3168082780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3852[4] = 
{
	U3CExampleU3Ec__Iterator0_t3168082780::get_offset_of_U24this_0(),
	U3CExampleU3Ec__Iterator0_t3168082780::get_offset_of_U24current_1(),
	U3CExampleU3Ec__Iterator0_t3168082780::get_offset_of_U24disposing_2(),
	U3CExampleU3Ec__Iterator0_t3168082780::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3853 = { sizeof (SeparatingStreamingAndRendering_t2111089326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3853[1] = 
{
	SeparatingStreamingAndRendering_t2111089326::get_offset_of_renderingCamera_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3854 = { sizeof (ApiImplementation_t1854519848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3854[13] = 
{
	ApiImplementation_t1854519848::get_offset_of_m_nativePluginRunner_0(),
	ApiImplementation_t1854519848::get_offset_of_m_coordinateSystem_1(),
	ApiImplementation_t1854519848::get_offset_of_m_cameraController_2(),
	ApiImplementation_t1854519848::get_offset_of_m_buildingsApi_3(),
	ApiImplementation_t1854519848::get_offset_of_m_geographicApi_4(),
	ApiImplementation_t1854519848::get_offset_of_m_frame_5(),
	ApiImplementation_t1854519848::get_offset_of_m_originECEF_6(),
	ApiImplementation_t1854519848::get_offset_of_m_interestPointProvider_7(),
	ApiImplementation_t1854519848::get_offset_of_m_terrainStreamer_8(),
	ApiImplementation_t1854519848::get_offset_of_m_roadStreamer_9(),
	ApiImplementation_t1854519848::get_offset_of_m_buildingStreamer_10(),
	ApiImplementation_t1854519848::get_offset_of_m_highlightStreamer_11(),
	ApiImplementation_t1854519848::get_offset_of_m_mapGameObjectScene_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3855 = { sizeof (CameraApi_t3006904385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3855[7] = 
{
	0,
	CameraApi_t3006904385::get_offset_of_OnTransitionStart_1(),
	CameraApi_t3006904385::get_offset_of_OnTransitionEnd_2(),
	CameraApi_t3006904385::get_offset_of_U3CIsTransitioningU3Ek__BackingField_3(),
	CameraApi_t3006904385::get_offset_of_m_controlledCamera_4(),
	CameraApi_t3006904385::get_offset_of_m_apiImplementation_5(),
	CameraApi_t3006904385::get_offset_of_m_inputHandler_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3856 = { sizeof (TransitionStartHandler_t1803894657), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3857 = { sizeof (TransitionEndHandler_t4082272293), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3858 = { sizeof (CameraEventType_t2774415872)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3858[21] = 
{
	CameraEventType_t2774415872::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3859 = { sizeof (CameraEventCallback_t1620885257), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3860 = { sizeof (CameraHelpers_t2558075301), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3861 = { sizeof (CameraInputHandler_t3237370388), -1, sizeof(CameraInputHandler_t3237370388_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3861[7] = 
{
	CameraInputHandler_t3237370388::get_offset_of_m_isTouchSupported_0(),
	CameraInputHandler_t3237370388::get_offset_of_m_inputFrame_1(),
	CameraInputHandler_t3237370388::get_offset_of_m_previousMousePosition_2(),
	CameraInputHandler_t3237370388::get_offset_of_m_inputProcessor_3(),
	CameraInputHandler_t3237370388_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	CameraInputHandler_t3237370388_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	CameraInputHandler_t3237370388_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3862 = { sizeof (InputFrame_t648463210)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3862[12] = 
{
	InputFrame_t648463210::get_offset_of_Touches_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InputFrame_t648463210::get_offset_of_MousePosition_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InputFrame_t648463210::get_offset_of_IsLeftDown_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InputFrame_t648463210::get_offset_of_IsLeftUp_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InputFrame_t648463210::get_offset_of_IsRightDown_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InputFrame_t648463210::get_offset_of_IsRightUp_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InputFrame_t648463210::get_offset_of_IsMiddleDown_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InputFrame_t648463210::get_offset_of_IsMiddleUp_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InputFrame_t648463210::get_offset_of_MouseXDelta_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InputFrame_t648463210::get_offset_of_MouseYDelta_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InputFrame_t648463210::get_offset_of_MouseWheelDelta_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	InputFrame_t648463210::get_offset_of_HasMouseMoved_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3863 = { sizeof (SendActionDelegate_t3936809903), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3864 = { sizeof (U3CHandleMouseInputU3Ec__AnonStorey0_t3986541062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3864[2] = 
{
	U3CHandleMouseInputU3Ec__AnonStorey0_t3986541062::get_offset_of_mouseEvent_0(),
	U3CHandleMouseInputU3Ec__AnonStorey0_t3986541062::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3865 = { sizeof (CameraJumpBehaviour_t2963329382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3865[3] = 
{
	CameraJumpBehaviour_t2963329382::get_offset_of_InterestPointLatitudeDegrees_2(),
	CameraJumpBehaviour_t2963329382::get_offset_of_InterestPointLongitudeDegrees_3(),
	CameraJumpBehaviour_t2963329382::get_offset_of_DistanceFromInterestPoint_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3866 = { sizeof (CameraState_t2128520787), sizeof(CameraState_t2128520787_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3866[4] = 
{
	CameraState_t2128520787::get_offset_of_LocationEcef_0(),
	CameraState_t2128520787::get_offset_of_InterestPointEcef_1(),
	CameraState_t2128520787::get_offset_of_ViewMatrix_2(),
	CameraState_t2128520787::get_offset_of_ProjectMatrix_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3867 = { sizeof (InterestPointProvider_t3426197040), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3867[2] = 
{
	InterestPointProvider_t3426197040::get_offset_of_m_interestPointECEF_0(),
	InterestPointProvider_t3426197040::get_offset_of_m_hasInterestPointFromNativeController_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3868 = { sizeof (NativeCameraState_t1948103164)+ sizeof (RuntimeObject), sizeof(NativeCameraState_t1948103164 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3868[11] = 
{
	NativeCameraState_t1948103164::get_offset_of_nearClipPlaneDistance_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NativeCameraState_t1948103164::get_offset_of_farClipPlaneDistance_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NativeCameraState_t1948103164::get_offset_of_fieldOfViewDegrees_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NativeCameraState_t1948103164::get_offset_of_aspect_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NativeCameraState_t1948103164::get_offset_of_pitchDegrees_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NativeCameraState_t1948103164::get_offset_of_distanceToInterestPoint_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NativeCameraState_t1948103164::get_offset_of_originECEF_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NativeCameraState_t1948103164::get_offset_of_interestPointECEF_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NativeCameraState_t1948103164::get_offset_of_interestBasisRightECEF_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NativeCameraState_t1948103164::get_offset_of_interestBasisUpECEF_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NativeCameraState_t1948103164::get_offset_of_interestBasisForwardECEF_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3869 = { sizeof (ZoomLevelHelpers_t1711485203), -1, sizeof(ZoomLevelHelpers_t1711485203_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3869[1] = 
{
	ZoomLevelHelpers_t1711485203_StaticFields::get_offset_of_ms_zoomToDistances_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3870 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3870[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3871 = { sizeof (ThreadService_t1369597789), -1, sizeof(ThreadService_t1369597789_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3871[4] = 
{
	ThreadService_t1369597789_StaticFields::get_offset_of_ms_instance_0(),
	ThreadService_t1369597789::get_offset_of_m_threads_1(),
	ThreadService_t1369597789::get_offset_of_m_nextThreadID_2(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3872 = { sizeof (ThreadStartDelegate_t2173555516), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3873 = { sizeof (CreateThreadDelegate_t2573395908), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3874 = { sizeof (JoinThreadDelegate_t1262337526), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3875 = { sizeof (U3CCreateThreadInternalU3Ec__AnonStorey0_t2493176562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3875[1] = 
{
	U3CCreateThreadInternalU3Ec__AnonStorey0_t2493176562::get_offset_of_runFunc_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3876 = { sizeof (ConfigParams_t592212086)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3876[6] = 
{
	ConfigParams_t592212086::get_offset_of_m_nativeConfig_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConfigParams_t592212086::get_offset_of_MaterialsDirectory_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConfigParams_t592212086::get_offset_of_OverrideLandmarkMaterial_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConfigParams_t592212086::get_offset_of_CoverageTreeManifestUrl_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConfigParams_t592212086::get_offset_of_ThemeManifestUrl_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ConfigParams_t592212086::get_offset_of_Collisions_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3877 = { sizeof (NativeConfig_t18506486)+ sizeof (RuntimeObject), sizeof(NativeConfig_t18506486_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3877[5] = 
{
	NativeConfig_t18506486::get_offset_of_m_latitudeDegrees_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NativeConfig_t18506486::get_offset_of_m_longitudeDegrees_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NativeConfig_t18506486::get_offset_of_m_distanceToInterest_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NativeConfig_t18506486::get_offset_of_m_headingDegrees_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NativeConfig_t18506486::get_offset_of_m_streamingLodBasedOnDistance_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3878 = { sizeof (CollisionConfig_t3653208188)+ sizeof (RuntimeObject), sizeof(CollisionConfig_t3653208188_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3878[3] = 
{
	CollisionConfig_t3653208188::get_offset_of_U3CTerrainCollisionU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CollisionConfig_t3653208188::get_offset_of_U3CRoadCollisionU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CollisionConfig_t3653208188::get_offset_of_U3CBuildingCollisionU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3879 = { sizeof (Constants_t3443931233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3879[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3880 = { sizeof (FPS_t3702678127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3880[1] = 
{
	FPS_t3702678127::get_offset_of_fps_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3881 = { sizeof (RotateData_t3683904859)+ sizeof (RuntimeObject), sizeof(RotateData_t3683904859 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3881[3] = 
{
	RotateData_t3683904859::get_offset_of_rotation_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RotateData_t3683904859::get_offset_of_velocity_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RotateData_t3683904859::get_offset_of_numTouches_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3882 = { sizeof (PinchData_t2028234657)+ sizeof (RuntimeObject), sizeof(PinchData_t2028234657 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3882[1] = 
{
	PinchData_t2028234657::get_offset_of_scale_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3883 = { sizeof (PanData_t3291826632)+ sizeof (RuntimeObject), sizeof(PanData_t3291826632 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3883[7] = 
{
	PanData_t3291826632::get_offset_of_pointRelative_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PanData_t3291826632::get_offset_of_pointAbsolute_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PanData_t3291826632::get_offset_of_pointRelativeNormalized_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PanData_t3291826632::get_offset_of_velocity_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PanData_t3291826632::get_offset_of_touchExtents_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PanData_t3291826632::get_offset_of_numTouches_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PanData_t3291826632::get_offset_of_majorScreenDimension_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3884 = { sizeof (TapData_t745490888)+ sizeof (RuntimeObject), sizeof(TapData_t745490888 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3884[1] = 
{
	TapData_t745490888::get_offset_of_point_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3885 = { sizeof (TouchHeldData_t1271796919)+ sizeof (RuntimeObject), sizeof(TouchHeldData_t1271796919 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3885[1] = 
{
	TouchHeldData_t1271796919::get_offset_of_point_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3886 = { sizeof (TouchData_t718366571)+ sizeof (RuntimeObject), sizeof(TouchData_t718366571 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3886[1] = 
{
	TouchData_t718366571::get_offset_of_point_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3887 = { sizeof (KeyboardData_t2354704333)+ sizeof (RuntimeObject), sizeof(KeyboardData_t2354704333_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3887[4] = 
{
	KeyboardData_t2354704333::get_offset_of_keyCode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	KeyboardData_t2354704333::get_offset_of_metaKeys_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	KeyboardData_t2354704333::get_offset_of_printable_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	KeyboardData_t2354704333::get_offset_of_isKeyDown_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3888 = { sizeof (ZoomData_t3547862491)+ sizeof (RuntimeObject), sizeof(ZoomData_t3547862491 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3888[1] = 
{
	ZoomData_t3547862491::get_offset_of_distance_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3889 = { sizeof (TiltData_t2861649398)+ sizeof (RuntimeObject), sizeof(TiltData_t2861649398 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3889[3] = 
{
	TiltData_t2861649398::get_offset_of_distance_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TiltData_t2861649398::get_offset_of_screenHeight_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TiltData_t2861649398::get_offset_of_screenPercentageNormalized_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3890 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3891 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3892 = { sizeof (MouseInputAction_t556709297)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3892[10] = 
{
	MouseInputAction_t556709297::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3893 = { sizeof (KeyboardModifiers_t1080473607)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3893[5] = 
{
	KeyboardModifiers_t1080473607::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3894 = { sizeof (MouseInputEvent_t292520373)+ sizeof (RuntimeObject), sizeof(MouseInputEvent_t292520373 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3894[5] = 
{
	MouseInputEvent_t292520373::get_offset_of_Action_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MouseInputEvent_t292520373::get_offset_of_KeyboardModifiers_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MouseInputEvent_t292520373::get_offset_of_x_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MouseInputEvent_t292520373::get_offset_of_y_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MouseInputEvent_t292520373::get_offset_of_z_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3895 = { sizeof (KeyboardInputEvent_t410871953)+ sizeof (RuntimeObject), sizeof(KeyboardInputEvent_t410871953_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3895[2] = 
{
	KeyboardInputEvent_t410871953::get_offset_of_KeyCode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	KeyboardInputEvent_t410871953::get_offset_of_KeyDownEvent_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3896 = { sizeof (MousePanGesture_t639745302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3896[9] = 
{
	MousePanGesture_t639745302::get_offset_of_m_handler_0(),
	MousePanGesture_t639745302::get_offset_of_m_panButtonDown_1(),
	MousePanGesture_t639745302::get_offset_of_m_panning_2(),
	MousePanGesture_t639745302::get_offset_of_m_current_3(),
	MousePanGesture_t639745302::get_offset_of_m_anchor_4(),
	MousePanGesture_t639745302::get_offset_of_majorScreenDimension_5(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3897 = { sizeof (MouseRotateGesture_t1726080705), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3897[4] = 
{
	MouseRotateGesture_t1726080705::get_offset_of_m_handler_0(),
	MouseRotateGesture_t1726080705::get_offset_of_m_rotating_1(),
	MouseRotateGesture_t1726080705::get_offset_of_m_anchor_2(),
	MouseRotateGesture_t1726080705::get_offset_of_m_totalRotation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3898 = { sizeof (MouseTapGesture_t1901348987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3898[10] = 
{
	MouseTapGesture_t1901348987::get_offset_of_m_tapDownCount_0(),
	MouseTapGesture_t1901348987::get_offset_of_m_tapUpCount_1(),
	MouseTapGesture_t1901348987::get_offset_of_m_currentPointerTrackingStack_2(),
	MouseTapGesture_t1901348987::get_offset_of_m_tapAnchorX_3(),
	MouseTapGesture_t1901348987::get_offset_of_m_tapAnchorY_4(),
	MouseTapGesture_t1901348987::get_offset_of_m_tapUnixTime_5(),
	0,
	0,
	0,
	MouseTapGesture_t1901348987::get_offset_of_m_tapHandler_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3899 = { sizeof (MouseTiltGesture_t4197683903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3899[3] = 
{
	MouseTiltGesture_t4197683903::get_offset_of_m_handler_0(),
	MouseTiltGesture_t4197683903::get_offset_of_m_tilting_1(),
	MouseTiltGesture_t4197683903::get_offset_of_m_previousMousePositionY_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
