﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.TrailRenderer
struct TrailRenderer_t1820797054;
// DG.Tweening.Tween
struct Tween_t2342918553;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// DG.Tweening.Core.DOSetter`1<System.Single>
struct DOSetter_1_t2447375106;
// DG.Tweening.TweenCallback`1<System.Single>
struct TweenCallback_1_t1456286679;
// DG.Tweening.EaseFunction
struct EaseFunction_t3531141372;
// UnityEngine.Material
struct Material_t340375123;
// System.String
struct String_t;
// UnityEngine.Light
struct Light_t3756812086;
// System.Char[]
struct CharU5BU5D_t3528271667;
// DG.Tweening.Sequence
struct Sequence_t2050373119;
// DG.Tweening.TweenCallback
struct TweenCallback_t3727756325;
// UnityEngine.LineRenderer
struct LineRenderer_t3154350270;
// DG.Tweening.TweenCallback`1<System.Int32>
struct TweenCallback_1_t3009965658;
// DG.Tweening.Core.DOTweenComponent
struct DOTweenComponent_t828035757;
// System.Collections.Generic.List`1<DG.Tweening.TweenCallback>
struct List_1_t904863771;
// System.Type
struct Type_t;
// System.Collections.Generic.List`1<DG.Tweening.Tween>
struct List_1_t3814993295;
// System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>
struct List_1_t553148457;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CU3EC__DISPLAYCLASS41_0_T2368797384_H
#define U3CU3EC__DISPLAYCLASS41_0_T2368797384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass41_0
struct  U3CU3Ec__DisplayClass41_0_t2368797384  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass41_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass41_0_t2368797384, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS41_0_T2368797384_H
#ifndef U3CU3EC__DISPLAYCLASS43_0_T2368666312_H
#define U3CU3EC__DISPLAYCLASS43_0_T2368666312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass43_0
struct  U3CU3Ec__DisplayClass43_0_t2368666312  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass43_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass43_0_t2368666312, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS43_0_T2368666312_H
#ifndef U3CU3EC__DISPLAYCLASS44_0_T2368993992_H
#define U3CU3EC__DISPLAYCLASS44_0_T2368993992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass44_0
struct  U3CU3Ec__DisplayClass44_0_t2368993992  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass44_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass44_0_t2368993992, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS44_0_T2368993992_H
#ifndef U3CU3EC__DISPLAYCLASS45_0_T2369059528_H
#define U3CU3EC__DISPLAYCLASS45_0_T2369059528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass45_0
struct  U3CU3Ec__DisplayClass45_0_t2369059528  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass45_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass45_0_t2369059528, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS45_0_T2369059528_H
#ifndef U3CU3EC__DISPLAYCLASS46_0_T2368862920_H
#define U3CU3EC__DISPLAYCLASS46_0_T2368862920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass46_0
struct  U3CU3Ec__DisplayClass46_0_t2368862920  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass46_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass46_0_t2368862920, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS46_0_T2368862920_H
#ifndef U3CU3EC__DISPLAYCLASS47_0_T2368928456_H
#define U3CU3EC__DISPLAYCLASS47_0_T2368928456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass47_0
struct  U3CU3Ec__DisplayClass47_0_t2368928456  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass47_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass47_0_t2368928456, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS47_0_T2368928456_H
#ifndef U3CU3EC__DISPLAYCLASS48_0_T2368207560_H
#define U3CU3EC__DISPLAYCLASS48_0_T2368207560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass48_0
struct  U3CU3Ec__DisplayClass48_0_t2368207560  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass48_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass48_0_t2368207560, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS48_0_T2368207560_H
#ifndef U3CU3EC__DISPLAYCLASS49_0_T2368273096_H
#define U3CU3EC__DISPLAYCLASS49_0_T2368273096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass49_0
struct  U3CU3Ec__DisplayClass49_0_t2368273096  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass49_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass49_0_t2368273096, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS49_0_T2368273096_H
#ifndef U3CU3EC__DISPLAYCLASS50_0_T2368731849_H
#define U3CU3EC__DISPLAYCLASS50_0_T2368731849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass50_0
struct  U3CU3Ec__DisplayClass50_0_t2368731849  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass50_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass50_0_t2368731849, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS50_0_T2368731849_H
#ifndef U3CU3EC__DISPLAYCLASS51_0_T2368797385_H
#define U3CU3EC__DISPLAYCLASS51_0_T2368797385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass51_0
struct  U3CU3Ec__DisplayClass51_0_t2368797385  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass51_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass51_0_t2368797385, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS51_0_T2368797385_H
#ifndef U3CU3EC__DISPLAYCLASS42_0_T2368600776_H
#define U3CU3EC__DISPLAYCLASS42_0_T2368600776_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass42_0
struct  U3CU3Ec__DisplayClass42_0_t2368600776  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass42_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass42_0_t2368600776, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS42_0_T2368600776_H
#ifndef U3CU3EC__DISPLAYCLASS30_0_T2368731855_H
#define U3CU3EC__DISPLAYCLASS30_0_T2368731855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass30_0
struct  U3CU3Ec__DisplayClass30_0_t2368731855  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.ShortcutExtensions/<>c__DisplayClass30_0::target
	Rigidbody_t3916780224 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_t2368731855, ___target_0)); }
	inline Rigidbody_t3916780224 * get_target_0() const { return ___target_0; }
	inline Rigidbody_t3916780224 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t3916780224 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS30_0_T2368731855_H
#ifndef U3CU3EC__DISPLAYCLASS31_0_T2368797391_H
#define U3CU3EC__DISPLAYCLASS31_0_T2368797391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass31_0
struct  U3CU3Ec__DisplayClass31_0_t2368797391  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.ShortcutExtensions/<>c__DisplayClass31_0::target
	Rigidbody_t3916780224 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_0_t2368797391, ___target_0)); }
	inline Rigidbody_t3916780224 * get_target_0() const { return ___target_0; }
	inline Rigidbody_t3916780224 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t3916780224 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS31_0_T2368797391_H
#ifndef U3CU3EC__DISPLAYCLASS32_0_T2368600783_H
#define U3CU3EC__DISPLAYCLASS32_0_T2368600783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass32_0
struct  U3CU3Ec__DisplayClass32_0_t2368600783  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.ShortcutExtensions/<>c__DisplayClass32_0::target
	Rigidbody_t3916780224 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass32_0_t2368600783, ___target_0)); }
	inline Rigidbody_t3916780224 * get_target_0() const { return ___target_0; }
	inline Rigidbody_t3916780224 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t3916780224 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS32_0_T2368600783_H
#ifndef U3CU3EC__DISPLAYCLASS33_0_T2368666319_H
#define U3CU3EC__DISPLAYCLASS33_0_T2368666319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass33_0
struct  U3CU3Ec__DisplayClass33_0_t2368666319  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.ShortcutExtensions/<>c__DisplayClass33_0::target
	Rigidbody_t3916780224 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t2368666319, ___target_0)); }
	inline Rigidbody_t3916780224 * get_target_0() const { return ___target_0; }
	inline Rigidbody_t3916780224 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t3916780224 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS33_0_T2368666319_H
#ifndef U3CU3EC__DISPLAYCLASS35_0_T2369059535_H
#define U3CU3EC__DISPLAYCLASS35_0_T2369059535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass35_0
struct  U3CU3Ec__DisplayClass35_0_t2369059535  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.ShortcutExtensions/<>c__DisplayClass35_0::target
	Rigidbody_t3916780224 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t2369059535, ___target_0)); }
	inline Rigidbody_t3916780224 * get_target_0() const { return ___target_0; }
	inline Rigidbody_t3916780224 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t3916780224 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS35_0_T2369059535_H
#ifndef U3CU3EC__DISPLAYCLASS36_0_T2368862927_H
#define U3CU3EC__DISPLAYCLASS36_0_T2368862927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass36_0
struct  U3CU3Ec__DisplayClass36_0_t2368862927  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass36_0::trans
	Transform_t3600365921 * ___trans_0;
	// UnityEngine.Rigidbody DG.Tweening.ShortcutExtensions/<>c__DisplayClass36_0::target
	Rigidbody_t3916780224 * ___target_1;

public:
	inline static int32_t get_offset_of_trans_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_t2368862927, ___trans_0)); }
	inline Transform_t3600365921 * get_trans_0() const { return ___trans_0; }
	inline Transform_t3600365921 ** get_address_of_trans_0() { return &___trans_0; }
	inline void set_trans_0(Transform_t3600365921 * value)
	{
		___trans_0 = value;
		Il2CppCodeGenWriteBarrier((&___trans_0), value);
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_t2368862927, ___target_1)); }
	inline Rigidbody_t3916780224 * get_target_1() const { return ___target_1; }
	inline Rigidbody_t3916780224 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Rigidbody_t3916780224 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS36_0_T2368862927_H
#ifndef U3CU3EC__DISPLAYCLASS37_0_T2368928463_H
#define U3CU3EC__DISPLAYCLASS37_0_T2368928463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass37_0
struct  U3CU3Ec__DisplayClass37_0_t2368928463  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.ShortcutExtensions/<>c__DisplayClass37_0::target
	Rigidbody_t3916780224 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass37_0_t2368928463, ___target_0)); }
	inline Rigidbody_t3916780224 * get_target_0() const { return ___target_0; }
	inline Rigidbody_t3916780224 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t3916780224 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS37_0_T2368928463_H
#ifndef U3CU3EC__DISPLAYCLASS38_0_T2368207567_H
#define U3CU3EC__DISPLAYCLASS38_0_T2368207567_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass38_0
struct  U3CU3Ec__DisplayClass38_0_t2368207567  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass38_0::trans
	Transform_t3600365921 * ___trans_0;
	// UnityEngine.Rigidbody DG.Tweening.ShortcutExtensions/<>c__DisplayClass38_0::target
	Rigidbody_t3916780224 * ___target_1;

public:
	inline static int32_t get_offset_of_trans_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t2368207567, ___trans_0)); }
	inline Transform_t3600365921 * get_trans_0() const { return ___trans_0; }
	inline Transform_t3600365921 ** get_address_of_trans_0() { return &___trans_0; }
	inline void set_trans_0(Transform_t3600365921 * value)
	{
		___trans_0 = value;
		Il2CppCodeGenWriteBarrier((&___trans_0), value);
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t2368207567, ___target_1)); }
	inline Rigidbody_t3916780224 * get_target_1() const { return ___target_1; }
	inline Rigidbody_t3916780224 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Rigidbody_t3916780224 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS38_0_T2368207567_H
#ifndef U3CU3EC__DISPLAYCLASS39_0_T2368273103_H
#define U3CU3EC__DISPLAYCLASS39_0_T2368273103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass39_0
struct  U3CU3Ec__DisplayClass39_0_t2368273103  : public RuntimeObject
{
public:
	// UnityEngine.TrailRenderer DG.Tweening.ShortcutExtensions/<>c__DisplayClass39_0::target
	TrailRenderer_t1820797054 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass39_0_t2368273103, ___target_0)); }
	inline TrailRenderer_t1820797054 * get_target_0() const { return ___target_0; }
	inline TrailRenderer_t1820797054 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(TrailRenderer_t1820797054 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS39_0_T2368273103_H
#ifndef U3CU3EC__DISPLAYCLASS40_0_T2368731848_H
#define U3CU3EC__DISPLAYCLASS40_0_T2368731848_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass40_0
struct  U3CU3Ec__DisplayClass40_0_t2368731848  : public RuntimeObject
{
public:
	// UnityEngine.TrailRenderer DG.Tweening.ShortcutExtensions/<>c__DisplayClass40_0::target
	TrailRenderer_t1820797054 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_0_t2368731848, ___target_0)); }
	inline TrailRenderer_t1820797054 * get_target_0() const { return ___target_0; }
	inline TrailRenderer_t1820797054 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(TrailRenderer_t1820797054 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS40_0_T2368731848_H
#ifndef U3CU3EC__DISPLAYCLASS52_0_T2368600777_H
#define U3CU3EC__DISPLAYCLASS52_0_T2368600777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass52_0
struct  U3CU3Ec__DisplayClass52_0_t2368600777  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass52_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass52_0_t2368600777, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS52_0_T2368600777_H
#ifndef U3CU3EC__DISPLAYCLASS64_0_T2368993994_H
#define U3CU3EC__DISPLAYCLASS64_0_T2368993994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass64_0
struct  U3CU3Ec__DisplayClass64_0_t2368993994  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass64_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass64_0_t2368993994, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS64_0_T2368993994_H
#ifndef U3CU3EC__DISPLAYCLASS65_0_T2369059530_H
#define U3CU3EC__DISPLAYCLASS65_0_T2369059530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass65_0
struct  U3CU3Ec__DisplayClass65_0_t2369059530  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass65_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass65_0_t2369059530, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS65_0_T2369059530_H
#ifndef U3CU3EC__DISPLAYCLASS66_0_T2368862922_H
#define U3CU3EC__DISPLAYCLASS66_0_T2368862922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass66_0
struct  U3CU3Ec__DisplayClass66_0_t2368862922  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass66_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass66_0_t2368862922, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS66_0_T2368862922_H
#ifndef U3CU3EC__DISPLAYCLASS67_0_T2368928458_H
#define U3CU3EC__DISPLAYCLASS67_0_T2368928458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass67_0
struct  U3CU3Ec__DisplayClass67_0_t2368928458  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass67_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass67_0_t2368928458, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS67_0_T2368928458_H
#ifndef U3CU3EC__DISPLAYCLASS70_0_T2368731851_H
#define U3CU3EC__DISPLAYCLASS70_0_T2368731851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass70_0
struct  U3CU3Ec__DisplayClass70_0_t2368731851  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass70_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass70_0_t2368731851, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS70_0_T2368731851_H
#ifndef U3CU3EC__DISPLAYCLASS71_0_T2368797387_H
#define U3CU3EC__DISPLAYCLASS71_0_T2368797387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass71_0
struct  U3CU3Ec__DisplayClass71_0_t2368797387  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass71_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass71_0_t2368797387, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS71_0_T2368797387_H
#ifndef U3CU3EC__DISPLAYCLASS72_0_T2368600779_H
#define U3CU3EC__DISPLAYCLASS72_0_T2368600779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass72_0
struct  U3CU3Ec__DisplayClass72_0_t2368600779  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass72_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass72_0_t2368600779, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS72_0_T2368600779_H
#ifndef U3CU3EC__DISPLAYCLASS73_0_T2368666315_H
#define U3CU3EC__DISPLAYCLASS73_0_T2368666315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass73_0
struct  U3CU3Ec__DisplayClass73_0_t2368666315  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass73_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass73_0_t2368666315, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS73_0_T2368666315_H
#ifndef U3CU3EC__DISPLAYCLASS74_0_T2368993995_H
#define U3CU3EC__DISPLAYCLASS74_0_T2368993995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass74_0
struct  U3CU3Ec__DisplayClass74_0_t2368993995  : public RuntimeObject
{
public:
	// DG.Tweening.Tween DG.Tweening.ShortcutExtensions/<>c__DisplayClass74_0::target
	Tween_t2342918553 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass74_0_t2368993995, ___target_0)); }
	inline Tween_t2342918553 * get_target_0() const { return ___target_0; }
	inline Tween_t2342918553 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Tween_t2342918553 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS74_0_T2368993995_H
#ifndef U3CU3EC__DISPLAYCLASS63_0_T2368666314_H
#define U3CU3EC__DISPLAYCLASS63_0_T2368666314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass63_0
struct  U3CU3Ec__DisplayClass63_0_t2368666314  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass63_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass63_0_t2368666314, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS63_0_T2368666314_H
#ifndef U3CU3EC__DISPLAYCLASS53_0_T2368666313_H
#define U3CU3EC__DISPLAYCLASS53_0_T2368666313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass53_0
struct  U3CU3Ec__DisplayClass53_0_t2368666313  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass53_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass53_0_t2368666313, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS53_0_T2368666313_H
#ifndef U3CU3EC__DISPLAYCLASS54_0_T2368993993_H
#define U3CU3EC__DISPLAYCLASS54_0_T2368993993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass54_0
struct  U3CU3Ec__DisplayClass54_0_t2368993993  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass54_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass54_0_t2368993993, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS54_0_T2368993993_H
#ifndef U3CU3EC__DISPLAYCLASS55_0_T2369059529_H
#define U3CU3EC__DISPLAYCLASS55_0_T2369059529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass55_0
struct  U3CU3Ec__DisplayClass55_0_t2369059529  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass55_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass55_0_t2369059529, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS55_0_T2369059529_H
#ifndef U3CU3EC__DISPLAYCLASS56_0_T2368862921_H
#define U3CU3EC__DISPLAYCLASS56_0_T2368862921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass56_0
struct  U3CU3Ec__DisplayClass56_0_t2368862921  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass56_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass56_0_t2368862921, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS56_0_T2368862921_H
#ifndef U3CU3EC__DISPLAYCLASS57_0_T2368928457_H
#define U3CU3EC__DISPLAYCLASS57_0_T2368928457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass57_0
struct  U3CU3Ec__DisplayClass57_0_t2368928457  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass57_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass57_0_t2368928457, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS57_0_T2368928457_H
#ifndef U3CU3EC__DISPLAYCLASS58_0_T2368207561_H
#define U3CU3EC__DISPLAYCLASS58_0_T2368207561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass58_0
struct  U3CU3Ec__DisplayClass58_0_t2368207561  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass58_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass58_0_t2368207561, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS58_0_T2368207561_H
#ifndef U3CU3EC__DISPLAYCLASS59_0_T2368273097_H
#define U3CU3EC__DISPLAYCLASS59_0_T2368273097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass59_0
struct  U3CU3Ec__DisplayClass59_0_t2368273097  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass59_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass59_0_t2368273097, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS59_0_T2368273097_H
#ifndef U3CU3EC__DISPLAYCLASS60_0_T2368731850_H
#define U3CU3EC__DISPLAYCLASS60_0_T2368731850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass60_0
struct  U3CU3Ec__DisplayClass60_0_t2368731850  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass60_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass60_0_t2368731850, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS60_0_T2368731850_H
#ifndef U3CU3EC__DISPLAYCLASS61_0_T2368797386_H
#define U3CU3EC__DISPLAYCLASS61_0_T2368797386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass61_0
struct  U3CU3Ec__DisplayClass61_0_t2368797386  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass61_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass61_0_t2368797386, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS61_0_T2368797386_H
#ifndef U3CU3EC__DISPLAYCLASS62_0_T2368600778_H
#define U3CU3EC__DISPLAYCLASS62_0_T2368600778_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass62_0
struct  U3CU3Ec__DisplayClass62_0_t2368600778  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass62_0::target
	Transform_t3600365921 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass62_0_t2368600778, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS62_0_T2368600778_H
#ifndef U3CU3EC__DISPLAYCLASS29_0_T2368273102_H
#define U3CU3EC__DISPLAYCLASS29_0_T2368273102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass29_0
struct  U3CU3Ec__DisplayClass29_0_t2368273102  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.ShortcutExtensions/<>c__DisplayClass29_0::target
	Rigidbody_t3916780224 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_t2368273102, ___target_0)); }
	inline Rigidbody_t3916780224 * get_target_0() const { return ___target_0; }
	inline Rigidbody_t3916780224 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t3916780224 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS29_0_T2368273102_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_T481505373_H
#define U3CU3EC__DISPLAYCLASS2_0_T481505373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t481505373  : public RuntimeObject
{
public:
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass2_0::target
	Camera_t4157153871 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t481505373, ___target_0)); }
	inline Camera_t4157153871 * get_target_0() const { return ___target_0; }
	inline Camera_t4157153871 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Camera_t4157153871 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_T481505373_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T481505374_H
#define U3CU3EC__DISPLAYCLASS3_0_T481505374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t481505374  : public RuntimeObject
{
public:
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass3_0::target
	Camera_t4157153871 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t481505374, ___target_0)); }
	inline Camera_t4157153871 * get_target_0() const { return ___target_0; }
	inline Camera_t4157153871 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Camera_t4157153871 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T481505374_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T481505367_H
#define U3CU3EC__DISPLAYCLASS4_0_T481505367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t481505367  : public RuntimeObject
{
public:
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass4_0::target
	Camera_t4157153871 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t481505367, ___target_0)); }
	inline Camera_t4157153871 * get_target_0() const { return ___target_0; }
	inline Camera_t4157153871 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Camera_t4157153871 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T481505367_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T481505368_H
#define U3CU3EC__DISPLAYCLASS5_0_T481505368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t481505368  : public RuntimeObject
{
public:
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass5_0::target
	Camera_t4157153871 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t481505368, ___target_0)); }
	inline Camera_t4157153871 * get_target_0() const { return ___target_0; }
	inline Camera_t4157153871 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Camera_t4157153871 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T481505368_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_T481505369_H
#define U3CU3EC__DISPLAYCLASS6_0_T481505369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_t481505369  : public RuntimeObject
{
public:
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass6_0::target
	Camera_t4157153871 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t481505369, ___target_0)); }
	inline Camera_t4157153871 * get_target_0() const { return ___target_0; }
	inline Camera_t4157153871 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Camera_t4157153871 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_T481505369_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_T481505370_H
#define U3CU3EC__DISPLAYCLASS7_0_T481505370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t481505370  : public RuntimeObject
{
public:
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass7_0::target
	Camera_t4157153871 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t481505370, ___target_0)); }
	inline Camera_t4157153871 * get_target_0() const { return ___target_0; }
	inline Camera_t4157153871 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Camera_t4157153871 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_T481505370_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_T481505379_H
#define U3CU3EC__DISPLAYCLASS8_0_T481505379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_t481505379  : public RuntimeObject
{
public:
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass8_0::target
	Camera_t4157153871 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t481505379, ___target_0)); }
	inline Camera_t4157153871 * get_target_0() const { return ___target_0; }
	inline Camera_t4157153871 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Camera_t4157153871 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_T481505379_H
#ifndef U3CU3EC__DISPLAYCLASS9_0_T481505380_H
#define U3CU3EC__DISPLAYCLASS9_0_T481505380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_t481505380  : public RuntimeObject
{
public:
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass9_0::target
	Camera_t4157153871 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t481505380, ___target_0)); }
	inline Camera_t4157153871 * get_target_0() const { return ___target_0; }
	inline Camera_t4157153871 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Camera_t4157153871 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS9_0_T481505380_H
#ifndef U3CU3EC__DISPLAYCLASS10_0_T2368731853_H
#define U3CU3EC__DISPLAYCLASS10_0_T2368731853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass10_0
struct  U3CU3Ec__DisplayClass10_0_t2368731853  : public RuntimeObject
{
public:
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass10_0::target
	Camera_t4157153871 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t2368731853, ___target_0)); }
	inline Camera_t4157153871 * get_target_0() const { return ___target_0; }
	inline Camera_t4157153871 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Camera_t4157153871 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS10_0_T2368731853_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_T481505372_H
#define U3CU3EC__DISPLAYCLASS1_0_T481505372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t481505372  : public RuntimeObject
{
public:
	// UnityEngine.AudioSource DG.Tweening.ShortcutExtensions/<>c__DisplayClass1_0::target
	AudioSource_t3935305588 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t481505372, ___target_0)); }
	inline AudioSource_t3935305588 * get_target_0() const { return ___target_0; }
	inline AudioSource_t3935305588 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(AudioSource_t3935305588 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_T481505372_H
#ifndef U3CU3EC__DISPLAYCLASS54_0_T1212542191_H
#define U3CU3EC__DISPLAYCLASS54_0_T1212542191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTween/<>c__DisplayClass54_0
struct  U3CU3Ec__DisplayClass54_0_t1212542191  : public RuntimeObject
{
public:
	// System.Single DG.Tweening.DOTween/<>c__DisplayClass54_0::v
	float ___v_0;
	// DG.Tweening.Core.DOSetter`1<System.Single> DG.Tweening.DOTween/<>c__DisplayClass54_0::setter
	DOSetter_1_t2447375106 * ___setter_1;

public:
	inline static int32_t get_offset_of_v_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass54_0_t1212542191, ___v_0)); }
	inline float get_v_0() const { return ___v_0; }
	inline float* get_address_of_v_0() { return &___v_0; }
	inline void set_v_0(float value)
	{
		___v_0 = value;
	}

	inline static int32_t get_offset_of_setter_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass54_0_t1212542191, ___setter_1)); }
	inline DOSetter_1_t2447375106 * get_setter_1() const { return ___setter_1; }
	inline DOSetter_1_t2447375106 ** get_address_of_setter_1() { return &___setter_1; }
	inline void set_setter_1(DOSetter_1_t2447375106 * value)
	{
		___setter_1 = value;
		Il2CppCodeGenWriteBarrier((&___setter_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS54_0_T1212542191_H
#ifndef DOVIRTUAL_T3355183605_H
#define DOVIRTUAL_T3355183605_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOVirtual
struct  DOVirtual_t3355183605  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOVIRTUAL_T3355183605_H
#ifndef U3CU3EC__DISPLAYCLASS0_0_T3914388664_H
#define U3CU3EC__DISPLAYCLASS0_0_T3914388664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOVirtual/<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_t3914388664  : public RuntimeObject
{
public:
	// System.Single DG.Tweening.DOVirtual/<>c__DisplayClass0_0::val
	float ___val_0;
	// DG.Tweening.TweenCallback`1<System.Single> DG.Tweening.DOVirtual/<>c__DisplayClass0_0::onVirtualUpdate
	TweenCallback_1_t1456286679 * ___onVirtualUpdate_1;

public:
	inline static int32_t get_offset_of_val_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t3914388664, ___val_0)); }
	inline float get_val_0() const { return ___val_0; }
	inline float* get_address_of_val_0() { return &___val_0; }
	inline void set_val_0(float value)
	{
		___val_0 = value;
	}

	inline static int32_t get_offset_of_onVirtualUpdate_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t3914388664, ___onVirtualUpdate_1)); }
	inline TweenCallback_1_t1456286679 * get_onVirtualUpdate_1() const { return ___onVirtualUpdate_1; }
	inline TweenCallback_1_t1456286679 ** get_address_of_onVirtualUpdate_1() { return &___onVirtualUpdate_1; }
	inline void set_onVirtualUpdate_1(TweenCallback_1_t1456286679 * value)
	{
		___onVirtualUpdate_1 = value;
		Il2CppCodeGenWriteBarrier((&___onVirtualUpdate_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS0_0_T3914388664_H
#ifndef EASEFACTORY_T2344806846_H
#define EASEFACTORY_T2344806846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.EaseFactory
struct  EaseFactory_t2344806846  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASEFACTORY_T2344806846_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_T753111370_H
#define U3CU3EC__DISPLAYCLASS2_0_T753111370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.EaseFactory/<>c__DisplayClass2_0
struct  U3CU3Ec__DisplayClass2_0_t753111370  : public RuntimeObject
{
public:
	// System.Single DG.Tweening.EaseFactory/<>c__DisplayClass2_0::motionDelay
	float ___motionDelay_0;
	// DG.Tweening.EaseFunction DG.Tweening.EaseFactory/<>c__DisplayClass2_0::customEase
	EaseFunction_t3531141372 * ___customEase_1;

public:
	inline static int32_t get_offset_of_motionDelay_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t753111370, ___motionDelay_0)); }
	inline float get_motionDelay_0() const { return ___motionDelay_0; }
	inline float* get_address_of_motionDelay_0() { return &___motionDelay_0; }
	inline void set_motionDelay_0(float value)
	{
		___motionDelay_0 = value;
	}

	inline static int32_t get_offset_of_customEase_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t753111370, ___customEase_1)); }
	inline EaseFunction_t3531141372 * get_customEase_1() const { return ___customEase_1; }
	inline EaseFunction_t3531141372 ** get_address_of_customEase_1() { return &___customEase_1; }
	inline void set_customEase_1(EaseFunction_t3531141372 * value)
	{
		___customEase_1 = value;
		Il2CppCodeGenWriteBarrier((&___customEase_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_T753111370_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef TWEENEXTENSIONS_T3641337881_H
#define TWEENEXTENSIONS_T3641337881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.TweenExtensions
struct  TweenExtensions_t3641337881  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENEXTENSIONS_T3641337881_H
#ifndef SHORTCUTEXTENSIONS_T1665800578_H
#define SHORTCUTEXTENSIONS_T1665800578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions
struct  ShortcutExtensions_t1665800578  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHORTCUTEXTENSIONS_T1665800578_H
#ifndef U3CU3EC__DISPLAYCLASS0_0_T481505371_H
#define U3CU3EC__DISPLAYCLASS0_0_T481505371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_t481505371  : public RuntimeObject
{
public:
	// UnityEngine.AudioSource DG.Tweening.ShortcutExtensions/<>c__DisplayClass0_0::target
	AudioSource_t3935305588 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t481505371, ___target_0)); }
	inline AudioSource_t3935305588 * get_target_0() const { return ___target_0; }
	inline AudioSource_t3935305588 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(AudioSource_t3935305588 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS0_0_T481505371_H
#ifndef U3CU3EC__DISPLAYCLASS11_0_T2368797389_H
#define U3CU3EC__DISPLAYCLASS11_0_T2368797389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t2368797389  : public RuntimeObject
{
public:
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass11_0::target
	Camera_t4157153871 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t2368797389, ___target_0)); }
	inline Camera_t4157153871 * get_target_0() const { return ___target_0; }
	inline Camera_t4157153871 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Camera_t4157153871 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_0_T2368797389_H
#ifndef U3CU3EC__DISPLAYCLASS21_0_T2368797390_H
#define U3CU3EC__DISPLAYCLASS21_0_T2368797390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass21_0
struct  U3CU3Ec__DisplayClass21_0_t2368797390  : public RuntimeObject
{
public:
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass21_0::target
	Material_t340375123 * ___target_0;
	// System.String DG.Tweening.ShortcutExtensions/<>c__DisplayClass21_0::property
	String_t* ___property_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_t2368797390, ___target_0)); }
	inline Material_t340375123 * get_target_0() const { return ___target_0; }
	inline Material_t340375123 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Material_t340375123 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_property_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_t2368797390, ___property_1)); }
	inline String_t* get_property_1() const { return ___property_1; }
	inline String_t** get_address_of_property_1() { return &___property_1; }
	inline void set_property_1(String_t* value)
	{
		___property_1 = value;
		Il2CppCodeGenWriteBarrier((&___property_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS21_0_T2368797390_H
#ifndef U3CU3EC__DISPLAYCLASS22_0_T2368600782_H
#define U3CU3EC__DISPLAYCLASS22_0_T2368600782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass22_0
struct  U3CU3Ec__DisplayClass22_0_t2368600782  : public RuntimeObject
{
public:
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass22_0::target
	Material_t340375123 * ___target_0;
	// System.String DG.Tweening.ShortcutExtensions/<>c__DisplayClass22_0::property
	String_t* ___property_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t2368600782, ___target_0)); }
	inline Material_t340375123 * get_target_0() const { return ___target_0; }
	inline Material_t340375123 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Material_t340375123 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_property_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t2368600782, ___property_1)); }
	inline String_t* get_property_1() const { return ___property_1; }
	inline String_t** get_address_of_property_1() { return &___property_1; }
	inline void set_property_1(String_t* value)
	{
		___property_1 = value;
		Il2CppCodeGenWriteBarrier((&___property_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS22_0_T2368600782_H
#ifndef U3CU3EC__DISPLAYCLASS23_0_T2368666318_H
#define U3CU3EC__DISPLAYCLASS23_0_T2368666318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass23_0
struct  U3CU3Ec__DisplayClass23_0_t2368666318  : public RuntimeObject
{
public:
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass23_0::target
	Material_t340375123 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass23_0_t2368666318, ___target_0)); }
	inline Material_t340375123 * get_target_0() const { return ___target_0; }
	inline Material_t340375123 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Material_t340375123 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS23_0_T2368666318_H
#ifndef U3CU3EC__DISPLAYCLASS24_0_T2368993998_H
#define U3CU3EC__DISPLAYCLASS24_0_T2368993998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass24_0
struct  U3CU3Ec__DisplayClass24_0_t2368993998  : public RuntimeObject
{
public:
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass24_0::target
	Material_t340375123 * ___target_0;
	// System.String DG.Tweening.ShortcutExtensions/<>c__DisplayClass24_0::property
	String_t* ___property_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_t2368993998, ___target_0)); }
	inline Material_t340375123 * get_target_0() const { return ___target_0; }
	inline Material_t340375123 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Material_t340375123 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_property_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_t2368993998, ___property_1)); }
	inline String_t* get_property_1() const { return ___property_1; }
	inline String_t** get_address_of_property_1() { return &___property_1; }
	inline void set_property_1(String_t* value)
	{
		___property_1 = value;
		Il2CppCodeGenWriteBarrier((&___property_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS24_0_T2368993998_H
#ifndef U3CU3EC__DISPLAYCLASS25_0_T2369059534_H
#define U3CU3EC__DISPLAYCLASS25_0_T2369059534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass25_0
struct  U3CU3Ec__DisplayClass25_0_t2369059534  : public RuntimeObject
{
public:
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass25_0::target
	Material_t340375123 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass25_0_t2369059534, ___target_0)); }
	inline Material_t340375123 * get_target_0() const { return ___target_0; }
	inline Material_t340375123 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Material_t340375123 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS25_0_T2369059534_H
#ifndef U3CU3EC__DISPLAYCLASS26_0_T2368862926_H
#define U3CU3EC__DISPLAYCLASS26_0_T2368862926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass26_0
struct  U3CU3Ec__DisplayClass26_0_t2368862926  : public RuntimeObject
{
public:
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass26_0::target
	Material_t340375123 * ___target_0;
	// System.String DG.Tweening.ShortcutExtensions/<>c__DisplayClass26_0::property
	String_t* ___property_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass26_0_t2368862926, ___target_0)); }
	inline Material_t340375123 * get_target_0() const { return ___target_0; }
	inline Material_t340375123 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Material_t340375123 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_property_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass26_0_t2368862926, ___property_1)); }
	inline String_t* get_property_1() const { return ___property_1; }
	inline String_t** get_address_of_property_1() { return &___property_1; }
	inline void set_property_1(String_t* value)
	{
		___property_1 = value;
		Il2CppCodeGenWriteBarrier((&___property_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS26_0_T2368862926_H
#ifndef U3CU3EC__DISPLAYCLASS27_0_T2368928462_H
#define U3CU3EC__DISPLAYCLASS27_0_T2368928462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass27_0
struct  U3CU3Ec__DisplayClass27_0_t2368928462  : public RuntimeObject
{
public:
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass27_0::target
	Material_t340375123 * ___target_0;
	// System.String DG.Tweening.ShortcutExtensions/<>c__DisplayClass27_0::property
	String_t* ___property_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass27_0_t2368928462, ___target_0)); }
	inline Material_t340375123 * get_target_0() const { return ___target_0; }
	inline Material_t340375123 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Material_t340375123 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_property_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass27_0_t2368928462, ___property_1)); }
	inline String_t* get_property_1() const { return ___property_1; }
	inline String_t** get_address_of_property_1() { return &___property_1; }
	inline void set_property_1(String_t* value)
	{
		___property_1 = value;
		Il2CppCodeGenWriteBarrier((&___property_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS27_0_T2368928462_H
#ifndef U3CU3EC__DISPLAYCLASS28_0_T2368207566_H
#define U3CU3EC__DISPLAYCLASS28_0_T2368207566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass28_0
struct  U3CU3Ec__DisplayClass28_0_t2368207566  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.ShortcutExtensions/<>c__DisplayClass28_0::target
	Rigidbody_t3916780224 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass28_0_t2368207566, ___target_0)); }
	inline Rigidbody_t3916780224 * get_target_0() const { return ___target_0; }
	inline Rigidbody_t3916780224 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t3916780224 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS28_0_T2368207566_H
#ifndef U3CU3EC__DISPLAYCLASS20_0_T2368731854_H
#define U3CU3EC__DISPLAYCLASS20_0_T2368731854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass20_0
struct  U3CU3Ec__DisplayClass20_0_t2368731854  : public RuntimeObject
{
public:
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass20_0::target
	Material_t340375123 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_t2368731854, ___target_0)); }
	inline Material_t340375123 * get_target_0() const { return ___target_0; }
	inline Material_t340375123 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Material_t340375123 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS20_0_T2368731854_H
#ifndef U3CU3EC__DISPLAYCLASS15_0_T2369059533_H
#define U3CU3EC__DISPLAYCLASS15_0_T2369059533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass15_0
struct  U3CU3Ec__DisplayClass15_0_t2369059533  : public RuntimeObject
{
public:
	// UnityEngine.Light DG.Tweening.ShortcutExtensions/<>c__DisplayClass15_0::target
	Light_t3756812086 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_t2369059533, ___target_0)); }
	inline Light_t3756812086 * get_target_0() const { return ___target_0; }
	inline Light_t3756812086 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Light_t3756812086 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS15_0_T2369059533_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_T2368666317_H
#define U3CU3EC__DISPLAYCLASS13_0_T2368666317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_t2368666317  : public RuntimeObject
{
public:
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass13_0::target
	Camera_t4157153871 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t2368666317, ___target_0)); }
	inline Camera_t4157153871 * get_target_0() const { return ___target_0; }
	inline Camera_t4157153871 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Camera_t4157153871 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_T2368666317_H
#ifndef U3CU3EC__DISPLAYCLASS16_0_T2368862925_H
#define U3CU3EC__DISPLAYCLASS16_0_T2368862925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass16_0
struct  U3CU3Ec__DisplayClass16_0_t2368862925  : public RuntimeObject
{
public:
	// UnityEngine.Light DG.Tweening.ShortcutExtensions/<>c__DisplayClass16_0::target
	Light_t3756812086 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_t2368862925, ___target_0)); }
	inline Light_t3756812086 * get_target_0() const { return ___target_0; }
	inline Light_t3756812086 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Light_t3756812086 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS16_0_T2368862925_H
#ifndef U3CU3EC__DISPLAYCLASS12_0_T2368600781_H
#define U3CU3EC__DISPLAYCLASS12_0_T2368600781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass12_0
struct  U3CU3Ec__DisplayClass12_0_t2368600781  : public RuntimeObject
{
public:
	// UnityEngine.Camera DG.Tweening.ShortcutExtensions/<>c__DisplayClass12_0::target
	Camera_t4157153871 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t2368600781, ___target_0)); }
	inline Camera_t4157153871 * get_target_0() const { return ___target_0; }
	inline Camera_t4157153871 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Camera_t4157153871 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS12_0_T2368600781_H
#ifndef U3CU3EC__DISPLAYCLASS14_0_T2368993997_H
#define U3CU3EC__DISPLAYCLASS14_0_T2368993997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass14_0
struct  U3CU3Ec__DisplayClass14_0_t2368993997  : public RuntimeObject
{
public:
	// UnityEngine.Light DG.Tweening.ShortcutExtensions/<>c__DisplayClass14_0::target
	Light_t3756812086 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass14_0_t2368993997, ___target_0)); }
	inline Light_t3756812086 * get_target_0() const { return ___target_0; }
	inline Light_t3756812086 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Light_t3756812086 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS14_0_T2368993997_H
#ifndef U3CU3EC__DISPLAYCLASS19_0_T2368273101_H
#define U3CU3EC__DISPLAYCLASS19_0_T2368273101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass19_0
struct  U3CU3Ec__DisplayClass19_0_t2368273101  : public RuntimeObject
{
public:
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass19_0::target
	Material_t340375123 * ___target_0;
	// System.String DG.Tweening.ShortcutExtensions/<>c__DisplayClass19_0::property
	String_t* ___property_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_0_t2368273101, ___target_0)); }
	inline Material_t340375123 * get_target_0() const { return ___target_0; }
	inline Material_t340375123 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Material_t340375123 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_property_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_0_t2368273101, ___property_1)); }
	inline String_t* get_property_1() const { return ___property_1; }
	inline String_t** get_address_of_property_1() { return &___property_1; }
	inline void set_property_1(String_t* value)
	{
		___property_1 = value;
		Il2CppCodeGenWriteBarrier((&___property_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS19_0_T2368273101_H
#ifndef U3CU3EC__DISPLAYCLASS18_0_T2368207565_H
#define U3CU3EC__DISPLAYCLASS18_0_T2368207565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass18_0
struct  U3CU3Ec__DisplayClass18_0_t2368207565  : public RuntimeObject
{
public:
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass18_0::target
	Material_t340375123 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t2368207565, ___target_0)); }
	inline Material_t340375123 * get_target_0() const { return ___target_0; }
	inline Material_t340375123 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Material_t340375123 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS18_0_T2368207565_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef U3CU3EC__DISPLAYCLASS79_0_T2368273099_H
#define U3CU3EC__DISPLAYCLASS79_0_T2368273099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass79_0
struct  U3CU3Ec__DisplayClass79_0_t2368273099  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass79_0::to
	Vector3_t3722313464  ___to_0;
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass79_0::target
	Transform_t3600365921 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass79_0_t2368273099, ___to_0)); }
	inline Vector3_t3722313464  get_to_0() const { return ___to_0; }
	inline Vector3_t3722313464 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Vector3_t3722313464  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass79_0_t2368273099, ___target_1)); }
	inline Transform_t3600365921 * get_target_1() const { return ___target_1; }
	inline Transform_t3600365921 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Transform_t3600365921 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS79_0_T2368273099_H
#ifndef U3CU3EC__DISPLAYCLASS80_0_T2368731860_H
#define U3CU3EC__DISPLAYCLASS80_0_T2368731860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass80_0
struct  U3CU3Ec__DisplayClass80_0_t2368731860  : public RuntimeObject
{
public:
	// UnityEngine.Quaternion DG.Tweening.ShortcutExtensions/<>c__DisplayClass80_0::to
	Quaternion_t2301928331  ___to_0;
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass80_0::target
	Transform_t3600365921 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass80_0_t2368731860, ___to_0)); }
	inline Quaternion_t2301928331  get_to_0() const { return ___to_0; }
	inline Quaternion_t2301928331 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Quaternion_t2301928331  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass80_0_t2368731860, ___target_1)); }
	inline Transform_t3600365921 * get_target_1() const { return ___target_1; }
	inline Transform_t3600365921 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Transform_t3600365921 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS80_0_T2368731860_H
#ifndef U3CU3EC__DISPLAYCLASS81_0_T2368797396_H
#define U3CU3EC__DISPLAYCLASS81_0_T2368797396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass81_0
struct  U3CU3Ec__DisplayClass81_0_t2368797396  : public RuntimeObject
{
public:
	// UnityEngine.Quaternion DG.Tweening.ShortcutExtensions/<>c__DisplayClass81_0::to
	Quaternion_t2301928331  ___to_0;
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass81_0::target
	Transform_t3600365921 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass81_0_t2368797396, ___to_0)); }
	inline Quaternion_t2301928331  get_to_0() const { return ___to_0; }
	inline Quaternion_t2301928331 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Quaternion_t2301928331  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass81_0_t2368797396, ___target_1)); }
	inline Transform_t3600365921 * get_target_1() const { return ___target_1; }
	inline Transform_t3600365921 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Transform_t3600365921 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS81_0_T2368797396_H
#ifndef LOGBEHAVIOUR_T1548882435_H
#define LOGBEHAVIOUR_T1548882435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LogBehaviour
struct  LogBehaviour_t1548882435 
{
public:
	// System.Int32 DG.Tweening.LogBehaviour::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogBehaviour_t1548882435, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGBEHAVIOUR_T1548882435_H
#ifndef COLOR2_T3097643075_H
#define COLOR2_T3097643075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Color2
struct  Color2_t3097643075 
{
public:
	// UnityEngine.Color DG.Tweening.Color2::ca
	Color_t2555686324  ___ca_0;
	// UnityEngine.Color DG.Tweening.Color2::cb
	Color_t2555686324  ___cb_1;

public:
	inline static int32_t get_offset_of_ca_0() { return static_cast<int32_t>(offsetof(Color2_t3097643075, ___ca_0)); }
	inline Color_t2555686324  get_ca_0() const { return ___ca_0; }
	inline Color_t2555686324 * get_address_of_ca_0() { return &___ca_0; }
	inline void set_ca_0(Color_t2555686324  value)
	{
		___ca_0 = value;
	}

	inline static int32_t get_offset_of_cb_1() { return static_cast<int32_t>(offsetof(Color2_t3097643075, ___cb_1)); }
	inline Color_t2555686324  get_cb_1() const { return ___cb_1; }
	inline Color_t2555686324 * get_address_of_cb_1() { return &___cb_1; }
	inline void set_cb_1(Color_t2555686324  value)
	{
		___cb_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR2_T3097643075_H
#ifndef AUTOPLAY_T1346164433_H
#define AUTOPLAY_T1346164433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.AutoPlay
struct  AutoPlay_t1346164433 
{
public:
	// System.Int32 DG.Tweening.AutoPlay::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AutoPlay_t1346164433, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOPLAY_T1346164433_H
#ifndef UPDATETYPE_T3937729206_H
#define UPDATETYPE_T3937729206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.UpdateType
struct  UpdateType_t3937729206 
{
public:
	// System.Int32 DG.Tweening.UpdateType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdateType_t3937729206, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATETYPE_T3937729206_H
#ifndef U3CU3EC__DISPLAYCLASS82_0_T2368600788_H
#define U3CU3EC__DISPLAYCLASS82_0_T2368600788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass82_0
struct  U3CU3Ec__DisplayClass82_0_t2368600788  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass82_0::to
	Vector3_t3722313464  ___to_0;
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass82_0::target
	Transform_t3600365921 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass82_0_t2368600788, ___to_0)); }
	inline Vector3_t3722313464  get_to_0() const { return ___to_0; }
	inline Vector3_t3722313464 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Vector3_t3722313464  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass82_0_t2368600788, ___target_1)); }
	inline Transform_t3600365921 * get_target_1() const { return ___target_1; }
	inline Transform_t3600365921 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Transform_t3600365921 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS82_0_T2368600788_H
#ifndef SPECIALSTARTUPMODE_T1644068939_H
#define SPECIALSTARTUPMODE_T1644068939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Enums.SpecialStartupMode
struct  SpecialStartupMode_t1644068939 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.SpecialStartupMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SpecialStartupMode_t1644068939, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALSTARTUPMODE_T1644068939_H
#ifndef U3CU3EC__DISPLAYCLASS78_0_T2368207563_H
#define U3CU3EC__DISPLAYCLASS78_0_T2368207563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass78_0
struct  U3CU3Ec__DisplayClass78_0_t2368207563  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass78_0::to
	Vector3_t3722313464  ___to_0;
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass78_0::target
	Transform_t3600365921 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass78_0_t2368207563, ___to_0)); }
	inline Vector3_t3722313464  get_to_0() const { return ___to_0; }
	inline Vector3_t3722313464 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Vector3_t3722313464  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass78_0_t2368207563, ___target_1)); }
	inline Transform_t3600365921 * get_target_1() const { return ___target_1; }
	inline Transform_t3600365921 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Transform_t3600365921 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS78_0_T2368207563_H
#ifndef SCRAMBLEMODE_T1285273342_H
#define SCRAMBLEMODE_T1285273342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ScrambleMode
struct  ScrambleMode_t1285273342 
{
public:
	// System.Int32 DG.Tweening.ScrambleMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScrambleMode_t1285273342, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRAMBLEMODE_T1285273342_H
#ifndef LOOPTYPE_T3049802818_H
#define LOOPTYPE_T3049802818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LoopType
struct  LoopType_t3049802818 
{
public:
	// System.Int32 DG.Tweening.LoopType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoopType_t3049802818, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T3049802818_H
#ifndef PATHTYPE_T3777299409_H
#define PATHTYPE_T3777299409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.PathType
struct  PathType_t3777299409 
{
public:
	// System.Int32 DG.Tweening.PathType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PathType_t3777299409, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHTYPE_T3777299409_H
#ifndef PATHMODE_T2165603100_H
#define PATHMODE_T2165603100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.PathMode
struct  PathMode_t2165603100 
{
public:
	// System.Int32 DG.Tweening.PathMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PathMode_t2165603100, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHMODE_T2165603100_H
#ifndef U3CU3EC__DISPLAYCLASS34_0_T2368993999_H
#define U3CU3EC__DISPLAYCLASS34_0_T2368993999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass34_0
struct  U3CU3Ec__DisplayClass34_0_t2368993999  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.ShortcutExtensions/<>c__DisplayClass34_0::target
	Rigidbody_t3916780224 * ___target_0;
	// System.Boolean DG.Tweening.ShortcutExtensions/<>c__DisplayClass34_0::offsetYSet
	bool ___offsetYSet_1;
	// System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass34_0::offsetY
	float ___offsetY_2;
	// DG.Tweening.Sequence DG.Tweening.ShortcutExtensions/<>c__DisplayClass34_0::s
	Sequence_t2050373119 * ___s_3;
	// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass34_0::endValue
	Vector3_t3722313464  ___endValue_4;
	// System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass34_0::startPosY
	float ___startPosY_5;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_0_t2368993999, ___target_0)); }
	inline Rigidbody_t3916780224 * get_target_0() const { return ___target_0; }
	inline Rigidbody_t3916780224 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t3916780224 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_offsetYSet_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_0_t2368993999, ___offsetYSet_1)); }
	inline bool get_offsetYSet_1() const { return ___offsetYSet_1; }
	inline bool* get_address_of_offsetYSet_1() { return &___offsetYSet_1; }
	inline void set_offsetYSet_1(bool value)
	{
		___offsetYSet_1 = value;
	}

	inline static int32_t get_offset_of_offsetY_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_0_t2368993999, ___offsetY_2)); }
	inline float get_offsetY_2() const { return ___offsetY_2; }
	inline float* get_address_of_offsetY_2() { return &___offsetY_2; }
	inline void set_offsetY_2(float value)
	{
		___offsetY_2 = value;
	}

	inline static int32_t get_offset_of_s_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_0_t2368993999, ___s_3)); }
	inline Sequence_t2050373119 * get_s_3() const { return ___s_3; }
	inline Sequence_t2050373119 ** get_address_of_s_3() { return &___s_3; }
	inline void set_s_3(Sequence_t2050373119 * value)
	{
		___s_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_3), value);
	}

	inline static int32_t get_offset_of_endValue_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_0_t2368993999, ___endValue_4)); }
	inline Vector3_t3722313464  get_endValue_4() const { return ___endValue_4; }
	inline Vector3_t3722313464 * get_address_of_endValue_4() { return &___endValue_4; }
	inline void set_endValue_4(Vector3_t3722313464  value)
	{
		___endValue_4 = value;
	}

	inline static int32_t get_offset_of_startPosY_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_0_t2368993999, ___startPosY_5)); }
	inline float get_startPosY_5() const { return ___startPosY_5; }
	inline float* get_address_of_startPosY_5() { return &___startPosY_5; }
	inline void set_startPosY_5(float value)
	{
		___startPosY_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS34_0_T2368993999_H
#ifndef TWEENTYPE_T1971673186_H
#define TWEENTYPE_T1971673186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.TweenType
struct  TweenType_t1971673186 
{
public:
	// System.Int32 DG.Tweening.TweenType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TweenType_t1971673186, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENTYPE_T1971673186_H
#ifndef U3CU3EC__DISPLAYCLASS77_0_T2368928459_H
#define U3CU3EC__DISPLAYCLASS77_0_T2368928459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass77_0
struct  U3CU3Ec__DisplayClass77_0_t2368928459  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass77_0::to
	Color_t2555686324  ___to_0;
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass77_0::target
	Material_t340375123 * ___target_1;
	// System.String DG.Tweening.ShortcutExtensions/<>c__DisplayClass77_0::property
	String_t* ___property_2;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass77_0_t2368928459, ___to_0)); }
	inline Color_t2555686324  get_to_0() const { return ___to_0; }
	inline Color_t2555686324 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_t2555686324  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass77_0_t2368928459, ___target_1)); }
	inline Material_t340375123 * get_target_1() const { return ___target_1; }
	inline Material_t340375123 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Material_t340375123 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}

	inline static int32_t get_offset_of_property_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass77_0_t2368928459, ___property_2)); }
	inline String_t* get_property_2() const { return ___property_2; }
	inline String_t** get_address_of_property_2() { return &___property_2; }
	inline void set_property_2(String_t* value)
	{
		___property_2 = value;
		Il2CppCodeGenWriteBarrier((&___property_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS77_0_T2368928459_H
#ifndef U3CU3EC__DISPLAYCLASS76_0_T2368862923_H
#define U3CU3EC__DISPLAYCLASS76_0_T2368862923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass76_0
struct  U3CU3Ec__DisplayClass76_0_t2368862923  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass76_0::to
	Color_t2555686324  ___to_0;
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass76_0::target
	Material_t340375123 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass76_0_t2368862923, ___to_0)); }
	inline Color_t2555686324  get_to_0() const { return ___to_0; }
	inline Color_t2555686324 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_t2555686324  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass76_0_t2368862923, ___target_1)); }
	inline Material_t340375123 * get_target_1() const { return ___target_1; }
	inline Material_t340375123 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Material_t340375123 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS76_0_T2368862923_H
#ifndef U3CU3EC__DISPLAYCLASS75_0_T2369059531_H
#define U3CU3EC__DISPLAYCLASS75_0_T2369059531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass75_0
struct  U3CU3Ec__DisplayClass75_0_t2369059531  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass75_0::to
	Color_t2555686324  ___to_0;
	// UnityEngine.Light DG.Tweening.ShortcutExtensions/<>c__DisplayClass75_0::target
	Light_t3756812086 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass75_0_t2369059531, ___to_0)); }
	inline Color_t2555686324  get_to_0() const { return ___to_0; }
	inline Color_t2555686324 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_t2555686324  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass75_0_t2369059531, ___target_1)); }
	inline Light_t3756812086 * get_target_1() const { return ___target_1; }
	inline Light_t3756812086 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Light_t3756812086 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS75_0_T2369059531_H
#ifndef U3CU3EC__DISPLAYCLASS69_0_T2368273098_H
#define U3CU3EC__DISPLAYCLASS69_0_T2368273098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0
struct  U3CU3Ec__DisplayClass69_0_t2368273098  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::target
	Transform_t3600365921 * ___target_0;
	// System.Boolean DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::offsetYSet
	bool ___offsetYSet_1;
	// System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::offsetY
	float ___offsetY_2;
	// DG.Tweening.Sequence DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::s
	Sequence_t2050373119 * ___s_3;
	// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::endValue
	Vector3_t3722313464  ___endValue_4;
	// System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::startPosY
	float ___startPosY_5;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass69_0_t2368273098, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_offsetYSet_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass69_0_t2368273098, ___offsetYSet_1)); }
	inline bool get_offsetYSet_1() const { return ___offsetYSet_1; }
	inline bool* get_address_of_offsetYSet_1() { return &___offsetYSet_1; }
	inline void set_offsetYSet_1(bool value)
	{
		___offsetYSet_1 = value;
	}

	inline static int32_t get_offset_of_offsetY_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass69_0_t2368273098, ___offsetY_2)); }
	inline float get_offsetY_2() const { return ___offsetY_2; }
	inline float* get_address_of_offsetY_2() { return &___offsetY_2; }
	inline void set_offsetY_2(float value)
	{
		___offsetY_2 = value;
	}

	inline static int32_t get_offset_of_s_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass69_0_t2368273098, ___s_3)); }
	inline Sequence_t2050373119 * get_s_3() const { return ___s_3; }
	inline Sequence_t2050373119 ** get_address_of_s_3() { return &___s_3; }
	inline void set_s_3(Sequence_t2050373119 * value)
	{
		___s_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_3), value);
	}

	inline static int32_t get_offset_of_endValue_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass69_0_t2368273098, ___endValue_4)); }
	inline Vector3_t3722313464  get_endValue_4() const { return ___endValue_4; }
	inline Vector3_t3722313464 * get_address_of_endValue_4() { return &___endValue_4; }
	inline void set_endValue_4(Vector3_t3722313464  value)
	{
		___endValue_4 = value;
	}

	inline static int32_t get_offset_of_startPosY_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass69_0_t2368273098, ___startPosY_5)); }
	inline float get_startPosY_5() const { return ___startPosY_5; }
	inline float* get_address_of_startPosY_5() { return &___startPosY_5; }
	inline void set_startPosY_5(float value)
	{
		___startPosY_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS69_0_T2368273098_H
#ifndef EASE_T4010715394_H
#define EASE_T4010715394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Ease
struct  Ease_t4010715394 
{
public:
	// System.Int32 DG.Tweening.Ease::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Ease_t4010715394, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASE_T4010715394_H
#ifndef U3CU3EC__DISPLAYCLASS68_0_T2368207562_H
#define U3CU3EC__DISPLAYCLASS68_0_T2368207562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0
struct  U3CU3Ec__DisplayClass68_0_t2368207562  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::target
	Transform_t3600365921 * ___target_0;
	// System.Boolean DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::offsetYSet
	bool ___offsetYSet_1;
	// System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::offsetY
	float ___offsetY_2;
	// DG.Tweening.Sequence DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::s
	Sequence_t2050373119 * ___s_3;
	// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::endValue
	Vector3_t3722313464  ___endValue_4;
	// System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::startPosY
	float ___startPosY_5;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass68_0_t2368207562, ___target_0)); }
	inline Transform_t3600365921 * get_target_0() const { return ___target_0; }
	inline Transform_t3600365921 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3600365921 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_offsetYSet_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass68_0_t2368207562, ___offsetYSet_1)); }
	inline bool get_offsetYSet_1() const { return ___offsetYSet_1; }
	inline bool* get_address_of_offsetYSet_1() { return &___offsetYSet_1; }
	inline void set_offsetYSet_1(bool value)
	{
		___offsetYSet_1 = value;
	}

	inline static int32_t get_offset_of_offsetY_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass68_0_t2368207562, ___offsetY_2)); }
	inline float get_offsetY_2() const { return ___offsetY_2; }
	inline float* get_address_of_offsetY_2() { return &___offsetY_2; }
	inline void set_offsetY_2(float value)
	{
		___offsetY_2 = value;
	}

	inline static int32_t get_offset_of_s_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass68_0_t2368207562, ___s_3)); }
	inline Sequence_t2050373119 * get_s_3() const { return ___s_3; }
	inline Sequence_t2050373119 ** get_address_of_s_3() { return &___s_3; }
	inline void set_s_3(Sequence_t2050373119 * value)
	{
		___s_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_3), value);
	}

	inline static int32_t get_offset_of_endValue_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass68_0_t2368207562, ___endValue_4)); }
	inline Vector3_t3722313464  get_endValue_4() const { return ___endValue_4; }
	inline Vector3_t3722313464 * get_address_of_endValue_4() { return &___endValue_4; }
	inline void set_endValue_4(Vector3_t3722313464  value)
	{
		___endValue_4 = value;
	}

	inline static int32_t get_offset_of_startPosY_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass68_0_t2368207562, ___startPosY_5)); }
	inline float get_startPosY_5() const { return ___startPosY_5; }
	inline float* get_address_of_startPosY_5() { return &___startPosY_5; }
	inline void set_startPosY_5(float value)
	{
		___startPosY_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS68_0_T2368207562_H
#ifndef ROTATEMODE_T2548570174_H
#define ROTATEMODE_T2548570174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.RotateMode
struct  RotateMode_t2548570174 
{
public:
	// System.Int32 DG.Tweening.RotateMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RotateMode_t2548570174, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEMODE_T2548570174_H
#ifndef ABSSEQUENTIABLE_T3376041011_H
#define ABSSEQUENTIABLE_T3376041011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.ABSSequentiable
struct  ABSSequentiable_t3376041011  : public RuntimeObject
{
public:
	// DG.Tweening.TweenType DG.Tweening.Core.ABSSequentiable::tweenType
	int32_t ___tweenType_0;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedPosition
	float ___sequencedPosition_1;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedEndPosition
	float ___sequencedEndPosition_2;
	// DG.Tweening.TweenCallback DG.Tweening.Core.ABSSequentiable::onStart
	TweenCallback_t3727756325 * ___onStart_3;

public:
	inline static int32_t get_offset_of_tweenType_0() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___tweenType_0)); }
	inline int32_t get_tweenType_0() const { return ___tweenType_0; }
	inline int32_t* get_address_of_tweenType_0() { return &___tweenType_0; }
	inline void set_tweenType_0(int32_t value)
	{
		___tweenType_0 = value;
	}

	inline static int32_t get_offset_of_sequencedPosition_1() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___sequencedPosition_1)); }
	inline float get_sequencedPosition_1() const { return ___sequencedPosition_1; }
	inline float* get_address_of_sequencedPosition_1() { return &___sequencedPosition_1; }
	inline void set_sequencedPosition_1(float value)
	{
		___sequencedPosition_1 = value;
	}

	inline static int32_t get_offset_of_sequencedEndPosition_2() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___sequencedEndPosition_2)); }
	inline float get_sequencedEndPosition_2() const { return ___sequencedEndPosition_2; }
	inline float* get_address_of_sequencedEndPosition_2() { return &___sequencedEndPosition_2; }
	inline void set_sequencedEndPosition_2(float value)
	{
		___sequencedEndPosition_2 = value;
	}

	inline static int32_t get_offset_of_onStart_3() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___onStart_3)); }
	inline TweenCallback_t3727756325 * get_onStart_3() const { return ___onStart_3; }
	inline TweenCallback_t3727756325 ** get_address_of_onStart_3() { return &___onStart_3; }
	inline void set_onStart_3(TweenCallback_t3727756325 * value)
	{
		___onStart_3 = value;
		Il2CppCodeGenWriteBarrier((&___onStart_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSSEQUENTIABLE_T3376041011_H
#ifndef U3CU3EC__DISPLAYCLASS17_0_T2368928461_H
#define U3CU3EC__DISPLAYCLASS17_0_T2368928461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass17_0
struct  U3CU3Ec__DisplayClass17_0_t2368928461  : public RuntimeObject
{
public:
	// DG.Tweening.Color2 DG.Tweening.ShortcutExtensions/<>c__DisplayClass17_0::startValue
	Color2_t3097643075  ___startValue_0;
	// UnityEngine.LineRenderer DG.Tweening.ShortcutExtensions/<>c__DisplayClass17_0::target
	LineRenderer_t3154350270 * ___target_1;

public:
	inline static int32_t get_offset_of_startValue_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t2368928461, ___startValue_0)); }
	inline Color2_t3097643075  get_startValue_0() const { return ___startValue_0; }
	inline Color2_t3097643075 * get_address_of_startValue_0() { return &___startValue_0; }
	inline void set_startValue_0(Color2_t3097643075  value)
	{
		___startValue_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t2368928461, ___target_1)); }
	inline LineRenderer_t3154350270 * get_target_1() const { return ___target_1; }
	inline LineRenderer_t3154350270 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(LineRenderer_t3154350270 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS17_0_T2368928461_H
#ifndef TWEENPARAMS_T4171191025_H
#define TWEENPARAMS_T4171191025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.TweenParams
struct  TweenParams_t4171191025  : public RuntimeObject
{
public:
	// System.Object DG.Tweening.TweenParams::id
	RuntimeObject * ___id_1;
	// System.Object DG.Tweening.TweenParams::target
	RuntimeObject * ___target_2;
	// DG.Tweening.UpdateType DG.Tweening.TweenParams::updateType
	int32_t ___updateType_3;
	// System.Boolean DG.Tweening.TweenParams::isIndependentUpdate
	bool ___isIndependentUpdate_4;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onStart
	TweenCallback_t3727756325 * ___onStart_5;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onPlay
	TweenCallback_t3727756325 * ___onPlay_6;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onRewind
	TweenCallback_t3727756325 * ___onRewind_7;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onUpdate
	TweenCallback_t3727756325 * ___onUpdate_8;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onStepComplete
	TweenCallback_t3727756325 * ___onStepComplete_9;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onComplete
	TweenCallback_t3727756325 * ___onComplete_10;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onKill
	TweenCallback_t3727756325 * ___onKill_11;
	// DG.Tweening.TweenCallback`1<System.Int32> DG.Tweening.TweenParams::onWaypointChange
	TweenCallback_1_t3009965658 * ___onWaypointChange_12;
	// System.Boolean DG.Tweening.TweenParams::isRecyclable
	bool ___isRecyclable_13;
	// System.Boolean DG.Tweening.TweenParams::isSpeedBased
	bool ___isSpeedBased_14;
	// System.Boolean DG.Tweening.TweenParams::autoKill
	bool ___autoKill_15;
	// System.Int32 DG.Tweening.TweenParams::loops
	int32_t ___loops_16;
	// DG.Tweening.LoopType DG.Tweening.TweenParams::loopType
	int32_t ___loopType_17;
	// System.Single DG.Tweening.TweenParams::delay
	float ___delay_18;
	// System.Boolean DG.Tweening.TweenParams::isRelative
	bool ___isRelative_19;
	// DG.Tweening.Ease DG.Tweening.TweenParams::easeType
	int32_t ___easeType_20;
	// DG.Tweening.EaseFunction DG.Tweening.TweenParams::customEase
	EaseFunction_t3531141372 * ___customEase_21;
	// System.Single DG.Tweening.TweenParams::easeOvershootOrAmplitude
	float ___easeOvershootOrAmplitude_22;
	// System.Single DG.Tweening.TweenParams::easePeriod
	float ___easePeriod_23;

public:
	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___id_1)); }
	inline RuntimeObject * get_id_1() const { return ___id_1; }
	inline RuntimeObject ** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(RuntimeObject * value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___target_2)); }
	inline RuntimeObject * get_target_2() const { return ___target_2; }
	inline RuntimeObject ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(RuntimeObject * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_updateType_3() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___updateType_3)); }
	inline int32_t get_updateType_3() const { return ___updateType_3; }
	inline int32_t* get_address_of_updateType_3() { return &___updateType_3; }
	inline void set_updateType_3(int32_t value)
	{
		___updateType_3 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_4() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___isIndependentUpdate_4)); }
	inline bool get_isIndependentUpdate_4() const { return ___isIndependentUpdate_4; }
	inline bool* get_address_of_isIndependentUpdate_4() { return &___isIndependentUpdate_4; }
	inline void set_isIndependentUpdate_4(bool value)
	{
		___isIndependentUpdate_4 = value;
	}

	inline static int32_t get_offset_of_onStart_5() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___onStart_5)); }
	inline TweenCallback_t3727756325 * get_onStart_5() const { return ___onStart_5; }
	inline TweenCallback_t3727756325 ** get_address_of_onStart_5() { return &___onStart_5; }
	inline void set_onStart_5(TweenCallback_t3727756325 * value)
	{
		___onStart_5 = value;
		Il2CppCodeGenWriteBarrier((&___onStart_5), value);
	}

	inline static int32_t get_offset_of_onPlay_6() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___onPlay_6)); }
	inline TweenCallback_t3727756325 * get_onPlay_6() const { return ___onPlay_6; }
	inline TweenCallback_t3727756325 ** get_address_of_onPlay_6() { return &___onPlay_6; }
	inline void set_onPlay_6(TweenCallback_t3727756325 * value)
	{
		___onPlay_6 = value;
		Il2CppCodeGenWriteBarrier((&___onPlay_6), value);
	}

	inline static int32_t get_offset_of_onRewind_7() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___onRewind_7)); }
	inline TweenCallback_t3727756325 * get_onRewind_7() const { return ___onRewind_7; }
	inline TweenCallback_t3727756325 ** get_address_of_onRewind_7() { return &___onRewind_7; }
	inline void set_onRewind_7(TweenCallback_t3727756325 * value)
	{
		___onRewind_7 = value;
		Il2CppCodeGenWriteBarrier((&___onRewind_7), value);
	}

	inline static int32_t get_offset_of_onUpdate_8() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___onUpdate_8)); }
	inline TweenCallback_t3727756325 * get_onUpdate_8() const { return ___onUpdate_8; }
	inline TweenCallback_t3727756325 ** get_address_of_onUpdate_8() { return &___onUpdate_8; }
	inline void set_onUpdate_8(TweenCallback_t3727756325 * value)
	{
		___onUpdate_8 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdate_8), value);
	}

	inline static int32_t get_offset_of_onStepComplete_9() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___onStepComplete_9)); }
	inline TweenCallback_t3727756325 * get_onStepComplete_9() const { return ___onStepComplete_9; }
	inline TweenCallback_t3727756325 ** get_address_of_onStepComplete_9() { return &___onStepComplete_9; }
	inline void set_onStepComplete_9(TweenCallback_t3727756325 * value)
	{
		___onStepComplete_9 = value;
		Il2CppCodeGenWriteBarrier((&___onStepComplete_9), value);
	}

	inline static int32_t get_offset_of_onComplete_10() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___onComplete_10)); }
	inline TweenCallback_t3727756325 * get_onComplete_10() const { return ___onComplete_10; }
	inline TweenCallback_t3727756325 ** get_address_of_onComplete_10() { return &___onComplete_10; }
	inline void set_onComplete_10(TweenCallback_t3727756325 * value)
	{
		___onComplete_10 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_10), value);
	}

	inline static int32_t get_offset_of_onKill_11() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___onKill_11)); }
	inline TweenCallback_t3727756325 * get_onKill_11() const { return ___onKill_11; }
	inline TweenCallback_t3727756325 ** get_address_of_onKill_11() { return &___onKill_11; }
	inline void set_onKill_11(TweenCallback_t3727756325 * value)
	{
		___onKill_11 = value;
		Il2CppCodeGenWriteBarrier((&___onKill_11), value);
	}

	inline static int32_t get_offset_of_onWaypointChange_12() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___onWaypointChange_12)); }
	inline TweenCallback_1_t3009965658 * get_onWaypointChange_12() const { return ___onWaypointChange_12; }
	inline TweenCallback_1_t3009965658 ** get_address_of_onWaypointChange_12() { return &___onWaypointChange_12; }
	inline void set_onWaypointChange_12(TweenCallback_1_t3009965658 * value)
	{
		___onWaypointChange_12 = value;
		Il2CppCodeGenWriteBarrier((&___onWaypointChange_12), value);
	}

	inline static int32_t get_offset_of_isRecyclable_13() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___isRecyclable_13)); }
	inline bool get_isRecyclable_13() const { return ___isRecyclable_13; }
	inline bool* get_address_of_isRecyclable_13() { return &___isRecyclable_13; }
	inline void set_isRecyclable_13(bool value)
	{
		___isRecyclable_13 = value;
	}

	inline static int32_t get_offset_of_isSpeedBased_14() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___isSpeedBased_14)); }
	inline bool get_isSpeedBased_14() const { return ___isSpeedBased_14; }
	inline bool* get_address_of_isSpeedBased_14() { return &___isSpeedBased_14; }
	inline void set_isSpeedBased_14(bool value)
	{
		___isSpeedBased_14 = value;
	}

	inline static int32_t get_offset_of_autoKill_15() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___autoKill_15)); }
	inline bool get_autoKill_15() const { return ___autoKill_15; }
	inline bool* get_address_of_autoKill_15() { return &___autoKill_15; }
	inline void set_autoKill_15(bool value)
	{
		___autoKill_15 = value;
	}

	inline static int32_t get_offset_of_loops_16() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___loops_16)); }
	inline int32_t get_loops_16() const { return ___loops_16; }
	inline int32_t* get_address_of_loops_16() { return &___loops_16; }
	inline void set_loops_16(int32_t value)
	{
		___loops_16 = value;
	}

	inline static int32_t get_offset_of_loopType_17() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___loopType_17)); }
	inline int32_t get_loopType_17() const { return ___loopType_17; }
	inline int32_t* get_address_of_loopType_17() { return &___loopType_17; }
	inline void set_loopType_17(int32_t value)
	{
		___loopType_17 = value;
	}

	inline static int32_t get_offset_of_delay_18() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___delay_18)); }
	inline float get_delay_18() const { return ___delay_18; }
	inline float* get_address_of_delay_18() { return &___delay_18; }
	inline void set_delay_18(float value)
	{
		___delay_18 = value;
	}

	inline static int32_t get_offset_of_isRelative_19() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___isRelative_19)); }
	inline bool get_isRelative_19() const { return ___isRelative_19; }
	inline bool* get_address_of_isRelative_19() { return &___isRelative_19; }
	inline void set_isRelative_19(bool value)
	{
		___isRelative_19 = value;
	}

	inline static int32_t get_offset_of_easeType_20() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___easeType_20)); }
	inline int32_t get_easeType_20() const { return ___easeType_20; }
	inline int32_t* get_address_of_easeType_20() { return &___easeType_20; }
	inline void set_easeType_20(int32_t value)
	{
		___easeType_20 = value;
	}

	inline static int32_t get_offset_of_customEase_21() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___customEase_21)); }
	inline EaseFunction_t3531141372 * get_customEase_21() const { return ___customEase_21; }
	inline EaseFunction_t3531141372 ** get_address_of_customEase_21() { return &___customEase_21; }
	inline void set_customEase_21(EaseFunction_t3531141372 * value)
	{
		___customEase_21 = value;
		Il2CppCodeGenWriteBarrier((&___customEase_21), value);
	}

	inline static int32_t get_offset_of_easeOvershootOrAmplitude_22() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___easeOvershootOrAmplitude_22)); }
	inline float get_easeOvershootOrAmplitude_22() const { return ___easeOvershootOrAmplitude_22; }
	inline float* get_address_of_easeOvershootOrAmplitude_22() { return &___easeOvershootOrAmplitude_22; }
	inline void set_easeOvershootOrAmplitude_22(float value)
	{
		___easeOvershootOrAmplitude_22 = value;
	}

	inline static int32_t get_offset_of_easePeriod_23() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025, ___easePeriod_23)); }
	inline float get_easePeriod_23() const { return ___easePeriod_23; }
	inline float* get_address_of_easePeriod_23() { return &___easePeriod_23; }
	inline void set_easePeriod_23(float value)
	{
		___easePeriod_23 = value;
	}
};

struct TweenParams_t4171191025_StaticFields
{
public:
	// DG.Tweening.TweenParams DG.Tweening.TweenParams::Params
	TweenParams_t4171191025 * ___Params_0;

public:
	inline static int32_t get_offset_of_Params_0() { return static_cast<int32_t>(offsetof(TweenParams_t4171191025_StaticFields, ___Params_0)); }
	inline TweenParams_t4171191025 * get_Params_0() const { return ___Params_0; }
	inline TweenParams_t4171191025 ** get_address_of_Params_0() { return &___Params_0; }
	inline void set_Params_0(TweenParams_t4171191025 * value)
	{
		___Params_0 = value;
		Il2CppCodeGenWriteBarrier((&___Params_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENPARAMS_T4171191025_H
#ifndef DOTWEEN_T2744875806_H
#define DOTWEEN_T2744875806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTween
struct  DOTween_t2744875806  : public RuntimeObject
{
public:

public:
};

struct DOTween_t2744875806_StaticFields
{
public:
	// System.String DG.Tweening.DOTween::Version
	String_t* ___Version_0;
	// System.Boolean DG.Tweening.DOTween::useSafeMode
	bool ___useSafeMode_1;
	// System.Boolean DG.Tweening.DOTween::showUnityEditorReport
	bool ___showUnityEditorReport_2;
	// System.Single DG.Tweening.DOTween::timeScale
	float ___timeScale_3;
	// System.Boolean DG.Tweening.DOTween::useSmoothDeltaTime
	bool ___useSmoothDeltaTime_4;
	// System.Single DG.Tweening.DOTween::maxSmoothUnscaledTime
	float ___maxSmoothUnscaledTime_5;
	// DG.Tweening.LogBehaviour DG.Tweening.DOTween::_logBehaviour
	int32_t ____logBehaviour_6;
	// System.Boolean DG.Tweening.DOTween::drawGizmos
	bool ___drawGizmos_7;
	// DG.Tweening.UpdateType DG.Tweening.DOTween::defaultUpdateType
	int32_t ___defaultUpdateType_8;
	// System.Boolean DG.Tweening.DOTween::defaultTimeScaleIndependent
	bool ___defaultTimeScaleIndependent_9;
	// DG.Tweening.AutoPlay DG.Tweening.DOTween::defaultAutoPlay
	int32_t ___defaultAutoPlay_10;
	// System.Boolean DG.Tweening.DOTween::defaultAutoKill
	bool ___defaultAutoKill_11;
	// DG.Tweening.LoopType DG.Tweening.DOTween::defaultLoopType
	int32_t ___defaultLoopType_12;
	// System.Boolean DG.Tweening.DOTween::defaultRecyclable
	bool ___defaultRecyclable_13;
	// DG.Tweening.Ease DG.Tweening.DOTween::defaultEaseType
	int32_t ___defaultEaseType_14;
	// System.Single DG.Tweening.DOTween::defaultEaseOvershootOrAmplitude
	float ___defaultEaseOvershootOrAmplitude_15;
	// System.Single DG.Tweening.DOTween::defaultEasePeriod
	float ___defaultEasePeriod_16;
	// DG.Tweening.Core.DOTweenComponent DG.Tweening.DOTween::instance
	DOTweenComponent_t828035757 * ___instance_17;
	// System.Boolean DG.Tweening.DOTween::isUnityEditor
	bool ___isUnityEditor_18;
	// System.Boolean DG.Tweening.DOTween::isDebugBuild
	bool ___isDebugBuild_19;
	// System.Int32 DG.Tweening.DOTween::maxActiveTweenersReached
	int32_t ___maxActiveTweenersReached_20;
	// System.Int32 DG.Tweening.DOTween::maxActiveSequencesReached
	int32_t ___maxActiveSequencesReached_21;
	// System.Collections.Generic.List`1<DG.Tweening.TweenCallback> DG.Tweening.DOTween::GizmosDelegates
	List_1_t904863771 * ___GizmosDelegates_22;
	// System.Boolean DG.Tweening.DOTween::initialized
	bool ___initialized_23;
	// System.Boolean DG.Tweening.DOTween::isQuitting
	bool ___isQuitting_24;

public:
	inline static int32_t get_offset_of_Version_0() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___Version_0)); }
	inline String_t* get_Version_0() const { return ___Version_0; }
	inline String_t** get_address_of_Version_0() { return &___Version_0; }
	inline void set_Version_0(String_t* value)
	{
		___Version_0 = value;
		Il2CppCodeGenWriteBarrier((&___Version_0), value);
	}

	inline static int32_t get_offset_of_useSafeMode_1() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___useSafeMode_1)); }
	inline bool get_useSafeMode_1() const { return ___useSafeMode_1; }
	inline bool* get_address_of_useSafeMode_1() { return &___useSafeMode_1; }
	inline void set_useSafeMode_1(bool value)
	{
		___useSafeMode_1 = value;
	}

	inline static int32_t get_offset_of_showUnityEditorReport_2() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___showUnityEditorReport_2)); }
	inline bool get_showUnityEditorReport_2() const { return ___showUnityEditorReport_2; }
	inline bool* get_address_of_showUnityEditorReport_2() { return &___showUnityEditorReport_2; }
	inline void set_showUnityEditorReport_2(bool value)
	{
		___showUnityEditorReport_2 = value;
	}

	inline static int32_t get_offset_of_timeScale_3() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___timeScale_3)); }
	inline float get_timeScale_3() const { return ___timeScale_3; }
	inline float* get_address_of_timeScale_3() { return &___timeScale_3; }
	inline void set_timeScale_3(float value)
	{
		___timeScale_3 = value;
	}

	inline static int32_t get_offset_of_useSmoothDeltaTime_4() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___useSmoothDeltaTime_4)); }
	inline bool get_useSmoothDeltaTime_4() const { return ___useSmoothDeltaTime_4; }
	inline bool* get_address_of_useSmoothDeltaTime_4() { return &___useSmoothDeltaTime_4; }
	inline void set_useSmoothDeltaTime_4(bool value)
	{
		___useSmoothDeltaTime_4 = value;
	}

	inline static int32_t get_offset_of_maxSmoothUnscaledTime_5() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___maxSmoothUnscaledTime_5)); }
	inline float get_maxSmoothUnscaledTime_5() const { return ___maxSmoothUnscaledTime_5; }
	inline float* get_address_of_maxSmoothUnscaledTime_5() { return &___maxSmoothUnscaledTime_5; }
	inline void set_maxSmoothUnscaledTime_5(float value)
	{
		___maxSmoothUnscaledTime_5 = value;
	}

	inline static int32_t get_offset_of__logBehaviour_6() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ____logBehaviour_6)); }
	inline int32_t get__logBehaviour_6() const { return ____logBehaviour_6; }
	inline int32_t* get_address_of__logBehaviour_6() { return &____logBehaviour_6; }
	inline void set__logBehaviour_6(int32_t value)
	{
		____logBehaviour_6 = value;
	}

	inline static int32_t get_offset_of_drawGizmos_7() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___drawGizmos_7)); }
	inline bool get_drawGizmos_7() const { return ___drawGizmos_7; }
	inline bool* get_address_of_drawGizmos_7() { return &___drawGizmos_7; }
	inline void set_drawGizmos_7(bool value)
	{
		___drawGizmos_7 = value;
	}

	inline static int32_t get_offset_of_defaultUpdateType_8() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultUpdateType_8)); }
	inline int32_t get_defaultUpdateType_8() const { return ___defaultUpdateType_8; }
	inline int32_t* get_address_of_defaultUpdateType_8() { return &___defaultUpdateType_8; }
	inline void set_defaultUpdateType_8(int32_t value)
	{
		___defaultUpdateType_8 = value;
	}

	inline static int32_t get_offset_of_defaultTimeScaleIndependent_9() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultTimeScaleIndependent_9)); }
	inline bool get_defaultTimeScaleIndependent_9() const { return ___defaultTimeScaleIndependent_9; }
	inline bool* get_address_of_defaultTimeScaleIndependent_9() { return &___defaultTimeScaleIndependent_9; }
	inline void set_defaultTimeScaleIndependent_9(bool value)
	{
		___defaultTimeScaleIndependent_9 = value;
	}

	inline static int32_t get_offset_of_defaultAutoPlay_10() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultAutoPlay_10)); }
	inline int32_t get_defaultAutoPlay_10() const { return ___defaultAutoPlay_10; }
	inline int32_t* get_address_of_defaultAutoPlay_10() { return &___defaultAutoPlay_10; }
	inline void set_defaultAutoPlay_10(int32_t value)
	{
		___defaultAutoPlay_10 = value;
	}

	inline static int32_t get_offset_of_defaultAutoKill_11() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultAutoKill_11)); }
	inline bool get_defaultAutoKill_11() const { return ___defaultAutoKill_11; }
	inline bool* get_address_of_defaultAutoKill_11() { return &___defaultAutoKill_11; }
	inline void set_defaultAutoKill_11(bool value)
	{
		___defaultAutoKill_11 = value;
	}

	inline static int32_t get_offset_of_defaultLoopType_12() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultLoopType_12)); }
	inline int32_t get_defaultLoopType_12() const { return ___defaultLoopType_12; }
	inline int32_t* get_address_of_defaultLoopType_12() { return &___defaultLoopType_12; }
	inline void set_defaultLoopType_12(int32_t value)
	{
		___defaultLoopType_12 = value;
	}

	inline static int32_t get_offset_of_defaultRecyclable_13() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultRecyclable_13)); }
	inline bool get_defaultRecyclable_13() const { return ___defaultRecyclable_13; }
	inline bool* get_address_of_defaultRecyclable_13() { return &___defaultRecyclable_13; }
	inline void set_defaultRecyclable_13(bool value)
	{
		___defaultRecyclable_13 = value;
	}

	inline static int32_t get_offset_of_defaultEaseType_14() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultEaseType_14)); }
	inline int32_t get_defaultEaseType_14() const { return ___defaultEaseType_14; }
	inline int32_t* get_address_of_defaultEaseType_14() { return &___defaultEaseType_14; }
	inline void set_defaultEaseType_14(int32_t value)
	{
		___defaultEaseType_14 = value;
	}

	inline static int32_t get_offset_of_defaultEaseOvershootOrAmplitude_15() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultEaseOvershootOrAmplitude_15)); }
	inline float get_defaultEaseOvershootOrAmplitude_15() const { return ___defaultEaseOvershootOrAmplitude_15; }
	inline float* get_address_of_defaultEaseOvershootOrAmplitude_15() { return &___defaultEaseOvershootOrAmplitude_15; }
	inline void set_defaultEaseOvershootOrAmplitude_15(float value)
	{
		___defaultEaseOvershootOrAmplitude_15 = value;
	}

	inline static int32_t get_offset_of_defaultEasePeriod_16() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___defaultEasePeriod_16)); }
	inline float get_defaultEasePeriod_16() const { return ___defaultEasePeriod_16; }
	inline float* get_address_of_defaultEasePeriod_16() { return &___defaultEasePeriod_16; }
	inline void set_defaultEasePeriod_16(float value)
	{
		___defaultEasePeriod_16 = value;
	}

	inline static int32_t get_offset_of_instance_17() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___instance_17)); }
	inline DOTweenComponent_t828035757 * get_instance_17() const { return ___instance_17; }
	inline DOTweenComponent_t828035757 ** get_address_of_instance_17() { return &___instance_17; }
	inline void set_instance_17(DOTweenComponent_t828035757 * value)
	{
		___instance_17 = value;
		Il2CppCodeGenWriteBarrier((&___instance_17), value);
	}

	inline static int32_t get_offset_of_isUnityEditor_18() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___isUnityEditor_18)); }
	inline bool get_isUnityEditor_18() const { return ___isUnityEditor_18; }
	inline bool* get_address_of_isUnityEditor_18() { return &___isUnityEditor_18; }
	inline void set_isUnityEditor_18(bool value)
	{
		___isUnityEditor_18 = value;
	}

	inline static int32_t get_offset_of_isDebugBuild_19() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___isDebugBuild_19)); }
	inline bool get_isDebugBuild_19() const { return ___isDebugBuild_19; }
	inline bool* get_address_of_isDebugBuild_19() { return &___isDebugBuild_19; }
	inline void set_isDebugBuild_19(bool value)
	{
		___isDebugBuild_19 = value;
	}

	inline static int32_t get_offset_of_maxActiveTweenersReached_20() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___maxActiveTweenersReached_20)); }
	inline int32_t get_maxActiveTweenersReached_20() const { return ___maxActiveTweenersReached_20; }
	inline int32_t* get_address_of_maxActiveTweenersReached_20() { return &___maxActiveTweenersReached_20; }
	inline void set_maxActiveTweenersReached_20(int32_t value)
	{
		___maxActiveTweenersReached_20 = value;
	}

	inline static int32_t get_offset_of_maxActiveSequencesReached_21() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___maxActiveSequencesReached_21)); }
	inline int32_t get_maxActiveSequencesReached_21() const { return ___maxActiveSequencesReached_21; }
	inline int32_t* get_address_of_maxActiveSequencesReached_21() { return &___maxActiveSequencesReached_21; }
	inline void set_maxActiveSequencesReached_21(int32_t value)
	{
		___maxActiveSequencesReached_21 = value;
	}

	inline static int32_t get_offset_of_GizmosDelegates_22() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___GizmosDelegates_22)); }
	inline List_1_t904863771 * get_GizmosDelegates_22() const { return ___GizmosDelegates_22; }
	inline List_1_t904863771 ** get_address_of_GizmosDelegates_22() { return &___GizmosDelegates_22; }
	inline void set_GizmosDelegates_22(List_1_t904863771 * value)
	{
		___GizmosDelegates_22 = value;
		Il2CppCodeGenWriteBarrier((&___GizmosDelegates_22), value);
	}

	inline static int32_t get_offset_of_initialized_23() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___initialized_23)); }
	inline bool get_initialized_23() const { return ___initialized_23; }
	inline bool* get_address_of_initialized_23() { return &___initialized_23; }
	inline void set_initialized_23(bool value)
	{
		___initialized_23 = value;
	}

	inline static int32_t get_offset_of_isQuitting_24() { return static_cast<int32_t>(offsetof(DOTween_t2744875806_StaticFields, ___isQuitting_24)); }
	inline bool get_isQuitting_24() const { return ___isQuitting_24; }
	inline bool* get_address_of_isQuitting_24() { return &___isQuitting_24; }
	inline void set_isQuitting_24(bool value)
	{
		___isQuitting_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEEN_T2744875806_H
#ifndef TWEEN_T2342918553_H
#define TWEEN_T2342918553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Tween
struct  Tween_t2342918553  : public ABSSequentiable_t3376041011
{
public:
	// System.Single DG.Tweening.Tween::timeScale
	float ___timeScale_4;
	// System.Boolean DG.Tweening.Tween::isBackwards
	bool ___isBackwards_5;
	// System.Object DG.Tweening.Tween::id
	RuntimeObject * ___id_6;
	// System.Object DG.Tweening.Tween::target
	RuntimeObject * ___target_7;
	// DG.Tweening.UpdateType DG.Tweening.Tween::updateType
	int32_t ___updateType_8;
	// System.Boolean DG.Tweening.Tween::isIndependentUpdate
	bool ___isIndependentUpdate_9;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPlay
	TweenCallback_t3727756325 * ___onPlay_10;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPause
	TweenCallback_t3727756325 * ___onPause_11;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onRewind
	TweenCallback_t3727756325 * ___onRewind_12;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onUpdate
	TweenCallback_t3727756325 * ___onUpdate_13;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onStepComplete
	TweenCallback_t3727756325 * ___onStepComplete_14;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onComplete
	TweenCallback_t3727756325 * ___onComplete_15;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onKill
	TweenCallback_t3727756325 * ___onKill_16;
	// DG.Tweening.TweenCallback`1<System.Int32> DG.Tweening.Tween::onWaypointChange
	TweenCallback_1_t3009965658 * ___onWaypointChange_17;
	// System.Boolean DG.Tweening.Tween::isFrom
	bool ___isFrom_18;
	// System.Boolean DG.Tweening.Tween::isBlendable
	bool ___isBlendable_19;
	// System.Boolean DG.Tweening.Tween::isRecyclable
	bool ___isRecyclable_20;
	// System.Boolean DG.Tweening.Tween::isSpeedBased
	bool ___isSpeedBased_21;
	// System.Boolean DG.Tweening.Tween::autoKill
	bool ___autoKill_22;
	// System.Single DG.Tweening.Tween::duration
	float ___duration_23;
	// System.Int32 DG.Tweening.Tween::loops
	int32_t ___loops_24;
	// DG.Tweening.LoopType DG.Tweening.Tween::loopType
	int32_t ___loopType_25;
	// System.Single DG.Tweening.Tween::delay
	float ___delay_26;
	// System.Boolean DG.Tweening.Tween::isRelative
	bool ___isRelative_27;
	// DG.Tweening.Ease DG.Tweening.Tween::easeType
	int32_t ___easeType_28;
	// DG.Tweening.EaseFunction DG.Tweening.Tween::customEase
	EaseFunction_t3531141372 * ___customEase_29;
	// System.Single DG.Tweening.Tween::easeOvershootOrAmplitude
	float ___easeOvershootOrAmplitude_30;
	// System.Single DG.Tweening.Tween::easePeriod
	float ___easePeriod_31;
	// System.Type DG.Tweening.Tween::typeofT1
	Type_t * ___typeofT1_32;
	// System.Type DG.Tweening.Tween::typeofT2
	Type_t * ___typeofT2_33;
	// System.Type DG.Tweening.Tween::typeofTPlugOptions
	Type_t * ___typeofTPlugOptions_34;
	// System.Boolean DG.Tweening.Tween::active
	bool ___active_35;
	// System.Boolean DG.Tweening.Tween::isSequenced
	bool ___isSequenced_36;
	// DG.Tweening.Sequence DG.Tweening.Tween::sequenceParent
	Sequence_t2050373119 * ___sequenceParent_37;
	// System.Int32 DG.Tweening.Tween::activeId
	int32_t ___activeId_38;
	// DG.Tweening.Core.Enums.SpecialStartupMode DG.Tweening.Tween::specialStartupMode
	int32_t ___specialStartupMode_39;
	// System.Boolean DG.Tweening.Tween::creationLocked
	bool ___creationLocked_40;
	// System.Boolean DG.Tweening.Tween::startupDone
	bool ___startupDone_41;
	// System.Boolean DG.Tweening.Tween::playedOnce
	bool ___playedOnce_42;
	// System.Single DG.Tweening.Tween::position
	float ___position_43;
	// System.Single DG.Tweening.Tween::fullDuration
	float ___fullDuration_44;
	// System.Int32 DG.Tweening.Tween::completedLoops
	int32_t ___completedLoops_45;
	// System.Boolean DG.Tweening.Tween::isPlaying
	bool ___isPlaying_46;
	// System.Boolean DG.Tweening.Tween::isComplete
	bool ___isComplete_47;
	// System.Single DG.Tweening.Tween::elapsedDelay
	float ___elapsedDelay_48;
	// System.Boolean DG.Tweening.Tween::delayComplete
	bool ___delayComplete_49;
	// System.Int32 DG.Tweening.Tween::miscInt
	int32_t ___miscInt_50;

public:
	inline static int32_t get_offset_of_timeScale_4() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___timeScale_4)); }
	inline float get_timeScale_4() const { return ___timeScale_4; }
	inline float* get_address_of_timeScale_4() { return &___timeScale_4; }
	inline void set_timeScale_4(float value)
	{
		___timeScale_4 = value;
	}

	inline static int32_t get_offset_of_isBackwards_5() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isBackwards_5)); }
	inline bool get_isBackwards_5() const { return ___isBackwards_5; }
	inline bool* get_address_of_isBackwards_5() { return &___isBackwards_5; }
	inline void set_isBackwards_5(bool value)
	{
		___isBackwards_5 = value;
	}

	inline static int32_t get_offset_of_id_6() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___id_6)); }
	inline RuntimeObject * get_id_6() const { return ___id_6; }
	inline RuntimeObject ** get_address_of_id_6() { return &___id_6; }
	inline void set_id_6(RuntimeObject * value)
	{
		___id_6 = value;
		Il2CppCodeGenWriteBarrier((&___id_6), value);
	}

	inline static int32_t get_offset_of_target_7() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___target_7)); }
	inline RuntimeObject * get_target_7() const { return ___target_7; }
	inline RuntimeObject ** get_address_of_target_7() { return &___target_7; }
	inline void set_target_7(RuntimeObject * value)
	{
		___target_7 = value;
		Il2CppCodeGenWriteBarrier((&___target_7), value);
	}

	inline static int32_t get_offset_of_updateType_8() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___updateType_8)); }
	inline int32_t get_updateType_8() const { return ___updateType_8; }
	inline int32_t* get_address_of_updateType_8() { return &___updateType_8; }
	inline void set_updateType_8(int32_t value)
	{
		___updateType_8 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_9() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isIndependentUpdate_9)); }
	inline bool get_isIndependentUpdate_9() const { return ___isIndependentUpdate_9; }
	inline bool* get_address_of_isIndependentUpdate_9() { return &___isIndependentUpdate_9; }
	inline void set_isIndependentUpdate_9(bool value)
	{
		___isIndependentUpdate_9 = value;
	}

	inline static int32_t get_offset_of_onPlay_10() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onPlay_10)); }
	inline TweenCallback_t3727756325 * get_onPlay_10() const { return ___onPlay_10; }
	inline TweenCallback_t3727756325 ** get_address_of_onPlay_10() { return &___onPlay_10; }
	inline void set_onPlay_10(TweenCallback_t3727756325 * value)
	{
		___onPlay_10 = value;
		Il2CppCodeGenWriteBarrier((&___onPlay_10), value);
	}

	inline static int32_t get_offset_of_onPause_11() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onPause_11)); }
	inline TweenCallback_t3727756325 * get_onPause_11() const { return ___onPause_11; }
	inline TweenCallback_t3727756325 ** get_address_of_onPause_11() { return &___onPause_11; }
	inline void set_onPause_11(TweenCallback_t3727756325 * value)
	{
		___onPause_11 = value;
		Il2CppCodeGenWriteBarrier((&___onPause_11), value);
	}

	inline static int32_t get_offset_of_onRewind_12() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onRewind_12)); }
	inline TweenCallback_t3727756325 * get_onRewind_12() const { return ___onRewind_12; }
	inline TweenCallback_t3727756325 ** get_address_of_onRewind_12() { return &___onRewind_12; }
	inline void set_onRewind_12(TweenCallback_t3727756325 * value)
	{
		___onRewind_12 = value;
		Il2CppCodeGenWriteBarrier((&___onRewind_12), value);
	}

	inline static int32_t get_offset_of_onUpdate_13() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onUpdate_13)); }
	inline TweenCallback_t3727756325 * get_onUpdate_13() const { return ___onUpdate_13; }
	inline TweenCallback_t3727756325 ** get_address_of_onUpdate_13() { return &___onUpdate_13; }
	inline void set_onUpdate_13(TweenCallback_t3727756325 * value)
	{
		___onUpdate_13 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdate_13), value);
	}

	inline static int32_t get_offset_of_onStepComplete_14() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onStepComplete_14)); }
	inline TweenCallback_t3727756325 * get_onStepComplete_14() const { return ___onStepComplete_14; }
	inline TweenCallback_t3727756325 ** get_address_of_onStepComplete_14() { return &___onStepComplete_14; }
	inline void set_onStepComplete_14(TweenCallback_t3727756325 * value)
	{
		___onStepComplete_14 = value;
		Il2CppCodeGenWriteBarrier((&___onStepComplete_14), value);
	}

	inline static int32_t get_offset_of_onComplete_15() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onComplete_15)); }
	inline TweenCallback_t3727756325 * get_onComplete_15() const { return ___onComplete_15; }
	inline TweenCallback_t3727756325 ** get_address_of_onComplete_15() { return &___onComplete_15; }
	inline void set_onComplete_15(TweenCallback_t3727756325 * value)
	{
		___onComplete_15 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_15), value);
	}

	inline static int32_t get_offset_of_onKill_16() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onKill_16)); }
	inline TweenCallback_t3727756325 * get_onKill_16() const { return ___onKill_16; }
	inline TweenCallback_t3727756325 ** get_address_of_onKill_16() { return &___onKill_16; }
	inline void set_onKill_16(TweenCallback_t3727756325 * value)
	{
		___onKill_16 = value;
		Il2CppCodeGenWriteBarrier((&___onKill_16), value);
	}

	inline static int32_t get_offset_of_onWaypointChange_17() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onWaypointChange_17)); }
	inline TweenCallback_1_t3009965658 * get_onWaypointChange_17() const { return ___onWaypointChange_17; }
	inline TweenCallback_1_t3009965658 ** get_address_of_onWaypointChange_17() { return &___onWaypointChange_17; }
	inline void set_onWaypointChange_17(TweenCallback_1_t3009965658 * value)
	{
		___onWaypointChange_17 = value;
		Il2CppCodeGenWriteBarrier((&___onWaypointChange_17), value);
	}

	inline static int32_t get_offset_of_isFrom_18() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isFrom_18)); }
	inline bool get_isFrom_18() const { return ___isFrom_18; }
	inline bool* get_address_of_isFrom_18() { return &___isFrom_18; }
	inline void set_isFrom_18(bool value)
	{
		___isFrom_18 = value;
	}

	inline static int32_t get_offset_of_isBlendable_19() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isBlendable_19)); }
	inline bool get_isBlendable_19() const { return ___isBlendable_19; }
	inline bool* get_address_of_isBlendable_19() { return &___isBlendable_19; }
	inline void set_isBlendable_19(bool value)
	{
		___isBlendable_19 = value;
	}

	inline static int32_t get_offset_of_isRecyclable_20() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isRecyclable_20)); }
	inline bool get_isRecyclable_20() const { return ___isRecyclable_20; }
	inline bool* get_address_of_isRecyclable_20() { return &___isRecyclable_20; }
	inline void set_isRecyclable_20(bool value)
	{
		___isRecyclable_20 = value;
	}

	inline static int32_t get_offset_of_isSpeedBased_21() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isSpeedBased_21)); }
	inline bool get_isSpeedBased_21() const { return ___isSpeedBased_21; }
	inline bool* get_address_of_isSpeedBased_21() { return &___isSpeedBased_21; }
	inline void set_isSpeedBased_21(bool value)
	{
		___isSpeedBased_21 = value;
	}

	inline static int32_t get_offset_of_autoKill_22() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___autoKill_22)); }
	inline bool get_autoKill_22() const { return ___autoKill_22; }
	inline bool* get_address_of_autoKill_22() { return &___autoKill_22; }
	inline void set_autoKill_22(bool value)
	{
		___autoKill_22 = value;
	}

	inline static int32_t get_offset_of_duration_23() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___duration_23)); }
	inline float get_duration_23() const { return ___duration_23; }
	inline float* get_address_of_duration_23() { return &___duration_23; }
	inline void set_duration_23(float value)
	{
		___duration_23 = value;
	}

	inline static int32_t get_offset_of_loops_24() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___loops_24)); }
	inline int32_t get_loops_24() const { return ___loops_24; }
	inline int32_t* get_address_of_loops_24() { return &___loops_24; }
	inline void set_loops_24(int32_t value)
	{
		___loops_24 = value;
	}

	inline static int32_t get_offset_of_loopType_25() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___loopType_25)); }
	inline int32_t get_loopType_25() const { return ___loopType_25; }
	inline int32_t* get_address_of_loopType_25() { return &___loopType_25; }
	inline void set_loopType_25(int32_t value)
	{
		___loopType_25 = value;
	}

	inline static int32_t get_offset_of_delay_26() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___delay_26)); }
	inline float get_delay_26() const { return ___delay_26; }
	inline float* get_address_of_delay_26() { return &___delay_26; }
	inline void set_delay_26(float value)
	{
		___delay_26 = value;
	}

	inline static int32_t get_offset_of_isRelative_27() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isRelative_27)); }
	inline bool get_isRelative_27() const { return ___isRelative_27; }
	inline bool* get_address_of_isRelative_27() { return &___isRelative_27; }
	inline void set_isRelative_27(bool value)
	{
		___isRelative_27 = value;
	}

	inline static int32_t get_offset_of_easeType_28() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___easeType_28)); }
	inline int32_t get_easeType_28() const { return ___easeType_28; }
	inline int32_t* get_address_of_easeType_28() { return &___easeType_28; }
	inline void set_easeType_28(int32_t value)
	{
		___easeType_28 = value;
	}

	inline static int32_t get_offset_of_customEase_29() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___customEase_29)); }
	inline EaseFunction_t3531141372 * get_customEase_29() const { return ___customEase_29; }
	inline EaseFunction_t3531141372 ** get_address_of_customEase_29() { return &___customEase_29; }
	inline void set_customEase_29(EaseFunction_t3531141372 * value)
	{
		___customEase_29 = value;
		Il2CppCodeGenWriteBarrier((&___customEase_29), value);
	}

	inline static int32_t get_offset_of_easeOvershootOrAmplitude_30() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___easeOvershootOrAmplitude_30)); }
	inline float get_easeOvershootOrAmplitude_30() const { return ___easeOvershootOrAmplitude_30; }
	inline float* get_address_of_easeOvershootOrAmplitude_30() { return &___easeOvershootOrAmplitude_30; }
	inline void set_easeOvershootOrAmplitude_30(float value)
	{
		___easeOvershootOrAmplitude_30 = value;
	}

	inline static int32_t get_offset_of_easePeriod_31() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___easePeriod_31)); }
	inline float get_easePeriod_31() const { return ___easePeriod_31; }
	inline float* get_address_of_easePeriod_31() { return &___easePeriod_31; }
	inline void set_easePeriod_31(float value)
	{
		___easePeriod_31 = value;
	}

	inline static int32_t get_offset_of_typeofT1_32() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___typeofT1_32)); }
	inline Type_t * get_typeofT1_32() const { return ___typeofT1_32; }
	inline Type_t ** get_address_of_typeofT1_32() { return &___typeofT1_32; }
	inline void set_typeofT1_32(Type_t * value)
	{
		___typeofT1_32 = value;
		Il2CppCodeGenWriteBarrier((&___typeofT1_32), value);
	}

	inline static int32_t get_offset_of_typeofT2_33() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___typeofT2_33)); }
	inline Type_t * get_typeofT2_33() const { return ___typeofT2_33; }
	inline Type_t ** get_address_of_typeofT2_33() { return &___typeofT2_33; }
	inline void set_typeofT2_33(Type_t * value)
	{
		___typeofT2_33 = value;
		Il2CppCodeGenWriteBarrier((&___typeofT2_33), value);
	}

	inline static int32_t get_offset_of_typeofTPlugOptions_34() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___typeofTPlugOptions_34)); }
	inline Type_t * get_typeofTPlugOptions_34() const { return ___typeofTPlugOptions_34; }
	inline Type_t ** get_address_of_typeofTPlugOptions_34() { return &___typeofTPlugOptions_34; }
	inline void set_typeofTPlugOptions_34(Type_t * value)
	{
		___typeofTPlugOptions_34 = value;
		Il2CppCodeGenWriteBarrier((&___typeofTPlugOptions_34), value);
	}

	inline static int32_t get_offset_of_active_35() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___active_35)); }
	inline bool get_active_35() const { return ___active_35; }
	inline bool* get_address_of_active_35() { return &___active_35; }
	inline void set_active_35(bool value)
	{
		___active_35 = value;
	}

	inline static int32_t get_offset_of_isSequenced_36() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isSequenced_36)); }
	inline bool get_isSequenced_36() const { return ___isSequenced_36; }
	inline bool* get_address_of_isSequenced_36() { return &___isSequenced_36; }
	inline void set_isSequenced_36(bool value)
	{
		___isSequenced_36 = value;
	}

	inline static int32_t get_offset_of_sequenceParent_37() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___sequenceParent_37)); }
	inline Sequence_t2050373119 * get_sequenceParent_37() const { return ___sequenceParent_37; }
	inline Sequence_t2050373119 ** get_address_of_sequenceParent_37() { return &___sequenceParent_37; }
	inline void set_sequenceParent_37(Sequence_t2050373119 * value)
	{
		___sequenceParent_37 = value;
		Il2CppCodeGenWriteBarrier((&___sequenceParent_37), value);
	}

	inline static int32_t get_offset_of_activeId_38() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___activeId_38)); }
	inline int32_t get_activeId_38() const { return ___activeId_38; }
	inline int32_t* get_address_of_activeId_38() { return &___activeId_38; }
	inline void set_activeId_38(int32_t value)
	{
		___activeId_38 = value;
	}

	inline static int32_t get_offset_of_specialStartupMode_39() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___specialStartupMode_39)); }
	inline int32_t get_specialStartupMode_39() const { return ___specialStartupMode_39; }
	inline int32_t* get_address_of_specialStartupMode_39() { return &___specialStartupMode_39; }
	inline void set_specialStartupMode_39(int32_t value)
	{
		___specialStartupMode_39 = value;
	}

	inline static int32_t get_offset_of_creationLocked_40() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___creationLocked_40)); }
	inline bool get_creationLocked_40() const { return ___creationLocked_40; }
	inline bool* get_address_of_creationLocked_40() { return &___creationLocked_40; }
	inline void set_creationLocked_40(bool value)
	{
		___creationLocked_40 = value;
	}

	inline static int32_t get_offset_of_startupDone_41() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___startupDone_41)); }
	inline bool get_startupDone_41() const { return ___startupDone_41; }
	inline bool* get_address_of_startupDone_41() { return &___startupDone_41; }
	inline void set_startupDone_41(bool value)
	{
		___startupDone_41 = value;
	}

	inline static int32_t get_offset_of_playedOnce_42() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___playedOnce_42)); }
	inline bool get_playedOnce_42() const { return ___playedOnce_42; }
	inline bool* get_address_of_playedOnce_42() { return &___playedOnce_42; }
	inline void set_playedOnce_42(bool value)
	{
		___playedOnce_42 = value;
	}

	inline static int32_t get_offset_of_position_43() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___position_43)); }
	inline float get_position_43() const { return ___position_43; }
	inline float* get_address_of_position_43() { return &___position_43; }
	inline void set_position_43(float value)
	{
		___position_43 = value;
	}

	inline static int32_t get_offset_of_fullDuration_44() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___fullDuration_44)); }
	inline float get_fullDuration_44() const { return ___fullDuration_44; }
	inline float* get_address_of_fullDuration_44() { return &___fullDuration_44; }
	inline void set_fullDuration_44(float value)
	{
		___fullDuration_44 = value;
	}

	inline static int32_t get_offset_of_completedLoops_45() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___completedLoops_45)); }
	inline int32_t get_completedLoops_45() const { return ___completedLoops_45; }
	inline int32_t* get_address_of_completedLoops_45() { return &___completedLoops_45; }
	inline void set_completedLoops_45(int32_t value)
	{
		___completedLoops_45 = value;
	}

	inline static int32_t get_offset_of_isPlaying_46() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isPlaying_46)); }
	inline bool get_isPlaying_46() const { return ___isPlaying_46; }
	inline bool* get_address_of_isPlaying_46() { return &___isPlaying_46; }
	inline void set_isPlaying_46(bool value)
	{
		___isPlaying_46 = value;
	}

	inline static int32_t get_offset_of_isComplete_47() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isComplete_47)); }
	inline bool get_isComplete_47() const { return ___isComplete_47; }
	inline bool* get_address_of_isComplete_47() { return &___isComplete_47; }
	inline void set_isComplete_47(bool value)
	{
		___isComplete_47 = value;
	}

	inline static int32_t get_offset_of_elapsedDelay_48() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___elapsedDelay_48)); }
	inline float get_elapsedDelay_48() const { return ___elapsedDelay_48; }
	inline float* get_address_of_elapsedDelay_48() { return &___elapsedDelay_48; }
	inline void set_elapsedDelay_48(float value)
	{
		___elapsedDelay_48 = value;
	}

	inline static int32_t get_offset_of_delayComplete_49() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___delayComplete_49)); }
	inline bool get_delayComplete_49() const { return ___delayComplete_49; }
	inline bool* get_address_of_delayComplete_49() { return &___delayComplete_49; }
	inline void set_delayComplete_49(bool value)
	{
		___delayComplete_49 = value;
	}

	inline static int32_t get_offset_of_miscInt_50() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___miscInt_50)); }
	inline int32_t get_miscInt_50() const { return ___miscInt_50; }
	inline int32_t* get_address_of_miscInt_50() { return &___miscInt_50; }
	inline void set_miscInt_50(int32_t value)
	{
		___miscInt_50 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEEN_T2342918553_H
#ifndef SEQUENCE_T2050373119_H
#define SEQUENCE_T2050373119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Sequence
struct  Sequence_t2050373119  : public Tween_t2342918553
{
public:
	// System.Collections.Generic.List`1<DG.Tweening.Tween> DG.Tweening.Sequence::sequencedTweens
	List_1_t3814993295 * ___sequencedTweens_51;
	// System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable> DG.Tweening.Sequence::_sequencedObjs
	List_1_t553148457 * ____sequencedObjs_52;
	// System.Single DG.Tweening.Sequence::lastTweenInsertTime
	float ___lastTweenInsertTime_53;

public:
	inline static int32_t get_offset_of_sequencedTweens_51() { return static_cast<int32_t>(offsetof(Sequence_t2050373119, ___sequencedTweens_51)); }
	inline List_1_t3814993295 * get_sequencedTweens_51() const { return ___sequencedTweens_51; }
	inline List_1_t3814993295 ** get_address_of_sequencedTweens_51() { return &___sequencedTweens_51; }
	inline void set_sequencedTweens_51(List_1_t3814993295 * value)
	{
		___sequencedTweens_51 = value;
		Il2CppCodeGenWriteBarrier((&___sequencedTweens_51), value);
	}

	inline static int32_t get_offset_of__sequencedObjs_52() { return static_cast<int32_t>(offsetof(Sequence_t2050373119, ____sequencedObjs_52)); }
	inline List_1_t553148457 * get__sequencedObjs_52() const { return ____sequencedObjs_52; }
	inline List_1_t553148457 ** get_address_of__sequencedObjs_52() { return &____sequencedObjs_52; }
	inline void set__sequencedObjs_52(List_1_t553148457 * value)
	{
		____sequencedObjs_52 = value;
		Il2CppCodeGenWriteBarrier((&____sequencedObjs_52), value);
	}

	inline static int32_t get_offset_of_lastTweenInsertTime_53() { return static_cast<int32_t>(offsetof(Sequence_t2050373119, ___lastTweenInsertTime_53)); }
	inline float get_lastTweenInsertTime_53() const { return ___lastTweenInsertTime_53; }
	inline float* get_address_of_lastTweenInsertTime_53() { return &___lastTweenInsertTime_53; }
	inline void set_lastTweenInsertTime_53(float value)
	{
		___lastTweenInsertTime_53 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEQUENCE_T2050373119_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (DOTween_t2744875806), -1, sizeof(DOTween_t2744875806_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2600[25] = 
{
	DOTween_t2744875806_StaticFields::get_offset_of_Version_0(),
	DOTween_t2744875806_StaticFields::get_offset_of_useSafeMode_1(),
	DOTween_t2744875806_StaticFields::get_offset_of_showUnityEditorReport_2(),
	DOTween_t2744875806_StaticFields::get_offset_of_timeScale_3(),
	DOTween_t2744875806_StaticFields::get_offset_of_useSmoothDeltaTime_4(),
	DOTween_t2744875806_StaticFields::get_offset_of_maxSmoothUnscaledTime_5(),
	DOTween_t2744875806_StaticFields::get_offset_of__logBehaviour_6(),
	DOTween_t2744875806_StaticFields::get_offset_of_drawGizmos_7(),
	DOTween_t2744875806_StaticFields::get_offset_of_defaultUpdateType_8(),
	DOTween_t2744875806_StaticFields::get_offset_of_defaultTimeScaleIndependent_9(),
	DOTween_t2744875806_StaticFields::get_offset_of_defaultAutoPlay_10(),
	DOTween_t2744875806_StaticFields::get_offset_of_defaultAutoKill_11(),
	DOTween_t2744875806_StaticFields::get_offset_of_defaultLoopType_12(),
	DOTween_t2744875806_StaticFields::get_offset_of_defaultRecyclable_13(),
	DOTween_t2744875806_StaticFields::get_offset_of_defaultEaseType_14(),
	DOTween_t2744875806_StaticFields::get_offset_of_defaultEaseOvershootOrAmplitude_15(),
	DOTween_t2744875806_StaticFields::get_offset_of_defaultEasePeriod_16(),
	DOTween_t2744875806_StaticFields::get_offset_of_instance_17(),
	DOTween_t2744875806_StaticFields::get_offset_of_isUnityEditor_18(),
	DOTween_t2744875806_StaticFields::get_offset_of_isDebugBuild_19(),
	DOTween_t2744875806_StaticFields::get_offset_of_maxActiveTweenersReached_20(),
	DOTween_t2744875806_StaticFields::get_offset_of_maxActiveSequencesReached_21(),
	DOTween_t2744875806_StaticFields::get_offset_of_GizmosDelegates_22(),
	DOTween_t2744875806_StaticFields::get_offset_of_initialized_23(),
	DOTween_t2744875806_StaticFields::get_offset_of_isQuitting_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { sizeof (U3CU3Ec__DisplayClass54_0_t1212542191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2601[2] = 
{
	U3CU3Ec__DisplayClass54_0_t1212542191::get_offset_of_v_0(),
	U3CU3Ec__DisplayClass54_0_t1212542191::get_offset_of_setter_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { sizeof (DOVirtual_t3355183605), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { sizeof (U3CU3Ec__DisplayClass0_0_t3914388664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2603[2] = 
{
	U3CU3Ec__DisplayClass0_0_t3914388664::get_offset_of_val_0(),
	U3CU3Ec__DisplayClass0_0_t3914388664::get_offset_of_onVirtualUpdate_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { sizeof (Ease_t4010715394)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2604[39] = 
{
	Ease_t4010715394::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (EaseFactory_t2344806846), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (U3CU3Ec__DisplayClass2_0_t753111370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2606[2] = 
{
	U3CU3Ec__DisplayClass2_0_t753111370::get_offset_of_motionDelay_0(),
	U3CU3Ec__DisplayClass2_0_t753111370::get_offset_of_customEase_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (PathMode_t2165603100)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2608[5] = 
{
	PathMode_t2165603100::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (PathType_t3777299409)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2609[3] = 
{
	PathType_t3777299409::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (RotateMode_t2548570174)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2610[5] = 
{
	RotateMode_t2548570174::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (ScrambleMode_t1285273342)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2611[7] = 
{
	ScrambleMode_t1285273342::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { sizeof (TweenExtensions_t3641337881), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { sizeof (LoopType_t3049802818)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2613[4] = 
{
	LoopType_t3049802818::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (Sequence_t2050373119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2614[3] = 
{
	Sequence_t2050373119::get_offset_of_sequencedTweens_51(),
	Sequence_t2050373119::get_offset_of__sequencedObjs_52(),
	Sequence_t2050373119::get_offset_of_lastTweenInsertTime_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { sizeof (ShortcutExtensions_t1665800578), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { sizeof (U3CU3Ec__DisplayClass0_0_t481505371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2616[1] = 
{
	U3CU3Ec__DisplayClass0_0_t481505371::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (U3CU3Ec__DisplayClass1_0_t481505372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2617[1] = 
{
	U3CU3Ec__DisplayClass1_0_t481505372::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { sizeof (U3CU3Ec__DisplayClass2_0_t481505373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2618[1] = 
{
	U3CU3Ec__DisplayClass2_0_t481505373::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { sizeof (U3CU3Ec__DisplayClass3_0_t481505374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2619[1] = 
{
	U3CU3Ec__DisplayClass3_0_t481505374::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { sizeof (U3CU3Ec__DisplayClass4_0_t481505367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2620[1] = 
{
	U3CU3Ec__DisplayClass4_0_t481505367::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { sizeof (U3CU3Ec__DisplayClass5_0_t481505368), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2621[1] = 
{
	U3CU3Ec__DisplayClass5_0_t481505368::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { sizeof (U3CU3Ec__DisplayClass6_0_t481505369), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2622[1] = 
{
	U3CU3Ec__DisplayClass6_0_t481505369::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { sizeof (U3CU3Ec__DisplayClass7_0_t481505370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2623[1] = 
{
	U3CU3Ec__DisplayClass7_0_t481505370::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (U3CU3Ec__DisplayClass8_0_t481505379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2624[1] = 
{
	U3CU3Ec__DisplayClass8_0_t481505379::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (U3CU3Ec__DisplayClass9_0_t481505380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2625[1] = 
{
	U3CU3Ec__DisplayClass9_0_t481505380::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (U3CU3Ec__DisplayClass10_0_t2368731853), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2626[1] = 
{
	U3CU3Ec__DisplayClass10_0_t2368731853::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { sizeof (U3CU3Ec__DisplayClass11_0_t2368797389), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2627[1] = 
{
	U3CU3Ec__DisplayClass11_0_t2368797389::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { sizeof (U3CU3Ec__DisplayClass12_0_t2368600781), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2628[1] = 
{
	U3CU3Ec__DisplayClass12_0_t2368600781::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { sizeof (U3CU3Ec__DisplayClass13_0_t2368666317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2629[1] = 
{
	U3CU3Ec__DisplayClass13_0_t2368666317::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { sizeof (U3CU3Ec__DisplayClass14_0_t2368993997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2630[1] = 
{
	U3CU3Ec__DisplayClass14_0_t2368993997::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { sizeof (U3CU3Ec__DisplayClass15_0_t2369059533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2631[1] = 
{
	U3CU3Ec__DisplayClass15_0_t2369059533::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { sizeof (U3CU3Ec__DisplayClass16_0_t2368862925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2632[1] = 
{
	U3CU3Ec__DisplayClass16_0_t2368862925::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { sizeof (U3CU3Ec__DisplayClass17_0_t2368928461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2633[2] = 
{
	U3CU3Ec__DisplayClass17_0_t2368928461::get_offset_of_startValue_0(),
	U3CU3Ec__DisplayClass17_0_t2368928461::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { sizeof (U3CU3Ec__DisplayClass18_0_t2368207565), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2634[1] = 
{
	U3CU3Ec__DisplayClass18_0_t2368207565::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { sizeof (U3CU3Ec__DisplayClass19_0_t2368273101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2635[2] = 
{
	U3CU3Ec__DisplayClass19_0_t2368273101::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass19_0_t2368273101::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { sizeof (U3CU3Ec__DisplayClass20_0_t2368731854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2636[1] = 
{
	U3CU3Ec__DisplayClass20_0_t2368731854::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (U3CU3Ec__DisplayClass21_0_t2368797390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2637[2] = 
{
	U3CU3Ec__DisplayClass21_0_t2368797390::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass21_0_t2368797390::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (U3CU3Ec__DisplayClass22_0_t2368600782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2638[2] = 
{
	U3CU3Ec__DisplayClass22_0_t2368600782::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass22_0_t2368600782::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (U3CU3Ec__DisplayClass23_0_t2368666318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2639[1] = 
{
	U3CU3Ec__DisplayClass23_0_t2368666318::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (U3CU3Ec__DisplayClass24_0_t2368993998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2640[2] = 
{
	U3CU3Ec__DisplayClass24_0_t2368993998::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass24_0_t2368993998::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { sizeof (U3CU3Ec__DisplayClass25_0_t2369059534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2641[1] = 
{
	U3CU3Ec__DisplayClass25_0_t2369059534::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (U3CU3Ec__DisplayClass26_0_t2368862926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2642[2] = 
{
	U3CU3Ec__DisplayClass26_0_t2368862926::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass26_0_t2368862926::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (U3CU3Ec__DisplayClass27_0_t2368928462), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2643[2] = 
{
	U3CU3Ec__DisplayClass27_0_t2368928462::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass27_0_t2368928462::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { sizeof (U3CU3Ec__DisplayClass28_0_t2368207566), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2644[1] = 
{
	U3CU3Ec__DisplayClass28_0_t2368207566::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { sizeof (U3CU3Ec__DisplayClass29_0_t2368273102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2645[1] = 
{
	U3CU3Ec__DisplayClass29_0_t2368273102::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { sizeof (U3CU3Ec__DisplayClass30_0_t2368731855), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2646[1] = 
{
	U3CU3Ec__DisplayClass30_0_t2368731855::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { sizeof (U3CU3Ec__DisplayClass31_0_t2368797391), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2647[1] = 
{
	U3CU3Ec__DisplayClass31_0_t2368797391::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { sizeof (U3CU3Ec__DisplayClass32_0_t2368600783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2648[1] = 
{
	U3CU3Ec__DisplayClass32_0_t2368600783::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { sizeof (U3CU3Ec__DisplayClass33_0_t2368666319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2649[1] = 
{
	U3CU3Ec__DisplayClass33_0_t2368666319::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { sizeof (U3CU3Ec__DisplayClass34_0_t2368993999), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2650[6] = 
{
	U3CU3Ec__DisplayClass34_0_t2368993999::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass34_0_t2368993999::get_offset_of_offsetYSet_1(),
	U3CU3Ec__DisplayClass34_0_t2368993999::get_offset_of_offsetY_2(),
	U3CU3Ec__DisplayClass34_0_t2368993999::get_offset_of_s_3(),
	U3CU3Ec__DisplayClass34_0_t2368993999::get_offset_of_endValue_4(),
	U3CU3Ec__DisplayClass34_0_t2368993999::get_offset_of_startPosY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (U3CU3Ec__DisplayClass35_0_t2369059535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2651[1] = 
{
	U3CU3Ec__DisplayClass35_0_t2369059535::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (U3CU3Ec__DisplayClass36_0_t2368862927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2652[2] = 
{
	U3CU3Ec__DisplayClass36_0_t2368862927::get_offset_of_trans_0(),
	U3CU3Ec__DisplayClass36_0_t2368862927::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (U3CU3Ec__DisplayClass37_0_t2368928463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2653[1] = 
{
	U3CU3Ec__DisplayClass37_0_t2368928463::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { sizeof (U3CU3Ec__DisplayClass38_0_t2368207567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2654[2] = 
{
	U3CU3Ec__DisplayClass38_0_t2368207567::get_offset_of_trans_0(),
	U3CU3Ec__DisplayClass38_0_t2368207567::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (U3CU3Ec__DisplayClass39_0_t2368273103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2655[1] = 
{
	U3CU3Ec__DisplayClass39_0_t2368273103::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { sizeof (U3CU3Ec__DisplayClass40_0_t2368731848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2656[1] = 
{
	U3CU3Ec__DisplayClass40_0_t2368731848::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { sizeof (U3CU3Ec__DisplayClass41_0_t2368797384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2657[1] = 
{
	U3CU3Ec__DisplayClass41_0_t2368797384::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (U3CU3Ec__DisplayClass42_0_t2368600776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2658[1] = 
{
	U3CU3Ec__DisplayClass42_0_t2368600776::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { sizeof (U3CU3Ec__DisplayClass43_0_t2368666312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2659[1] = 
{
	U3CU3Ec__DisplayClass43_0_t2368666312::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { sizeof (U3CU3Ec__DisplayClass44_0_t2368993992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2660[1] = 
{
	U3CU3Ec__DisplayClass44_0_t2368993992::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { sizeof (U3CU3Ec__DisplayClass45_0_t2369059528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2661[1] = 
{
	U3CU3Ec__DisplayClass45_0_t2369059528::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { sizeof (U3CU3Ec__DisplayClass46_0_t2368862920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2662[1] = 
{
	U3CU3Ec__DisplayClass46_0_t2368862920::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (U3CU3Ec__DisplayClass47_0_t2368928456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2663[1] = 
{
	U3CU3Ec__DisplayClass47_0_t2368928456::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (U3CU3Ec__DisplayClass48_0_t2368207560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2664[1] = 
{
	U3CU3Ec__DisplayClass48_0_t2368207560::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (U3CU3Ec__DisplayClass49_0_t2368273096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2665[1] = 
{
	U3CU3Ec__DisplayClass49_0_t2368273096::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (U3CU3Ec__DisplayClass50_0_t2368731849), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2666[1] = 
{
	U3CU3Ec__DisplayClass50_0_t2368731849::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (U3CU3Ec__DisplayClass51_0_t2368797385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2667[1] = 
{
	U3CU3Ec__DisplayClass51_0_t2368797385::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (U3CU3Ec__DisplayClass52_0_t2368600777), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2668[1] = 
{
	U3CU3Ec__DisplayClass52_0_t2368600777::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (U3CU3Ec__DisplayClass53_0_t2368666313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2669[1] = 
{
	U3CU3Ec__DisplayClass53_0_t2368666313::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { sizeof (U3CU3Ec__DisplayClass54_0_t2368993993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2670[1] = 
{
	U3CU3Ec__DisplayClass54_0_t2368993993::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (U3CU3Ec__DisplayClass55_0_t2369059529), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2671[1] = 
{
	U3CU3Ec__DisplayClass55_0_t2369059529::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { sizeof (U3CU3Ec__DisplayClass56_0_t2368862921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2672[1] = 
{
	U3CU3Ec__DisplayClass56_0_t2368862921::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (U3CU3Ec__DisplayClass57_0_t2368928457), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2673[1] = 
{
	U3CU3Ec__DisplayClass57_0_t2368928457::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (U3CU3Ec__DisplayClass58_0_t2368207561), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2674[1] = 
{
	U3CU3Ec__DisplayClass58_0_t2368207561::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { sizeof (U3CU3Ec__DisplayClass59_0_t2368273097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2675[1] = 
{
	U3CU3Ec__DisplayClass59_0_t2368273097::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (U3CU3Ec__DisplayClass60_0_t2368731850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2676[1] = 
{
	U3CU3Ec__DisplayClass60_0_t2368731850::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (U3CU3Ec__DisplayClass61_0_t2368797386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2677[1] = 
{
	U3CU3Ec__DisplayClass61_0_t2368797386::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (U3CU3Ec__DisplayClass62_0_t2368600778), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2678[1] = 
{
	U3CU3Ec__DisplayClass62_0_t2368600778::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (U3CU3Ec__DisplayClass63_0_t2368666314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2679[1] = 
{
	U3CU3Ec__DisplayClass63_0_t2368666314::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (U3CU3Ec__DisplayClass64_0_t2368993994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2680[1] = 
{
	U3CU3Ec__DisplayClass64_0_t2368993994::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (U3CU3Ec__DisplayClass65_0_t2369059530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2681[1] = 
{
	U3CU3Ec__DisplayClass65_0_t2369059530::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (U3CU3Ec__DisplayClass66_0_t2368862922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2682[1] = 
{
	U3CU3Ec__DisplayClass66_0_t2368862922::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (U3CU3Ec__DisplayClass67_0_t2368928458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2683[1] = 
{
	U3CU3Ec__DisplayClass67_0_t2368928458::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (U3CU3Ec__DisplayClass68_0_t2368207562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2684[6] = 
{
	U3CU3Ec__DisplayClass68_0_t2368207562::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass68_0_t2368207562::get_offset_of_offsetYSet_1(),
	U3CU3Ec__DisplayClass68_0_t2368207562::get_offset_of_offsetY_2(),
	U3CU3Ec__DisplayClass68_0_t2368207562::get_offset_of_s_3(),
	U3CU3Ec__DisplayClass68_0_t2368207562::get_offset_of_endValue_4(),
	U3CU3Ec__DisplayClass68_0_t2368207562::get_offset_of_startPosY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (U3CU3Ec__DisplayClass69_0_t2368273098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2685[6] = 
{
	U3CU3Ec__DisplayClass69_0_t2368273098::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass69_0_t2368273098::get_offset_of_offsetYSet_1(),
	U3CU3Ec__DisplayClass69_0_t2368273098::get_offset_of_offsetY_2(),
	U3CU3Ec__DisplayClass69_0_t2368273098::get_offset_of_s_3(),
	U3CU3Ec__DisplayClass69_0_t2368273098::get_offset_of_endValue_4(),
	U3CU3Ec__DisplayClass69_0_t2368273098::get_offset_of_startPosY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (U3CU3Ec__DisplayClass70_0_t2368731851), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2686[1] = 
{
	U3CU3Ec__DisplayClass70_0_t2368731851::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { sizeof (U3CU3Ec__DisplayClass71_0_t2368797387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2687[1] = 
{
	U3CU3Ec__DisplayClass71_0_t2368797387::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (U3CU3Ec__DisplayClass72_0_t2368600779), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2688[1] = 
{
	U3CU3Ec__DisplayClass72_0_t2368600779::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (U3CU3Ec__DisplayClass73_0_t2368666315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2689[1] = 
{
	U3CU3Ec__DisplayClass73_0_t2368666315::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (U3CU3Ec__DisplayClass74_0_t2368993995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2690[1] = 
{
	U3CU3Ec__DisplayClass74_0_t2368993995::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (U3CU3Ec__DisplayClass75_0_t2369059531), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2691[2] = 
{
	U3CU3Ec__DisplayClass75_0_t2369059531::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass75_0_t2369059531::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (U3CU3Ec__DisplayClass76_0_t2368862923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2692[2] = 
{
	U3CU3Ec__DisplayClass76_0_t2368862923::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass76_0_t2368862923::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (U3CU3Ec__DisplayClass77_0_t2368928459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2693[3] = 
{
	U3CU3Ec__DisplayClass77_0_t2368928459::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass77_0_t2368928459::get_offset_of_target_1(),
	U3CU3Ec__DisplayClass77_0_t2368928459::get_offset_of_property_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (U3CU3Ec__DisplayClass78_0_t2368207563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2694[2] = 
{
	U3CU3Ec__DisplayClass78_0_t2368207563::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass78_0_t2368207563::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (U3CU3Ec__DisplayClass79_0_t2368273099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2695[2] = 
{
	U3CU3Ec__DisplayClass79_0_t2368273099::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass79_0_t2368273099::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { sizeof (U3CU3Ec__DisplayClass80_0_t2368731860), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2696[2] = 
{
	U3CU3Ec__DisplayClass80_0_t2368731860::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass80_0_t2368731860::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { sizeof (U3CU3Ec__DisplayClass81_0_t2368797396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2697[2] = 
{
	U3CU3Ec__DisplayClass81_0_t2368797396::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass81_0_t2368797396::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (U3CU3Ec__DisplayClass82_0_t2368600788), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2698[2] = 
{
	U3CU3Ec__DisplayClass82_0_t2368600788::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass82_0_t2368600788::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (TweenParams_t4171191025), -1, sizeof(TweenParams_t4171191025_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2699[24] = 
{
	TweenParams_t4171191025_StaticFields::get_offset_of_Params_0(),
	TweenParams_t4171191025::get_offset_of_id_1(),
	TweenParams_t4171191025::get_offset_of_target_2(),
	TweenParams_t4171191025::get_offset_of_updateType_3(),
	TweenParams_t4171191025::get_offset_of_isIndependentUpdate_4(),
	TweenParams_t4171191025::get_offset_of_onStart_5(),
	TweenParams_t4171191025::get_offset_of_onPlay_6(),
	TweenParams_t4171191025::get_offset_of_onRewind_7(),
	TweenParams_t4171191025::get_offset_of_onUpdate_8(),
	TweenParams_t4171191025::get_offset_of_onStepComplete_9(),
	TweenParams_t4171191025::get_offset_of_onComplete_10(),
	TweenParams_t4171191025::get_offset_of_onKill_11(),
	TweenParams_t4171191025::get_offset_of_onWaypointChange_12(),
	TweenParams_t4171191025::get_offset_of_isRecyclable_13(),
	TweenParams_t4171191025::get_offset_of_isSpeedBased_14(),
	TweenParams_t4171191025::get_offset_of_autoKill_15(),
	TweenParams_t4171191025::get_offset_of_loops_16(),
	TweenParams_t4171191025::get_offset_of_loopType_17(),
	TweenParams_t4171191025::get_offset_of_delay_18(),
	TweenParams_t4171191025::get_offset_of_isRelative_19(),
	TweenParams_t4171191025::get_offset_of_easeType_20(),
	TweenParams_t4171191025::get_offset_of_customEase_21(),
	TweenParams_t4171191025::get_offset_of_easeOvershootOrAmplitude_22(),
	TweenParams_t4171191025::get_offset_of_easePeriod_23(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
