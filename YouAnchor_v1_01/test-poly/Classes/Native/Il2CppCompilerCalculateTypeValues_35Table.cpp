﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// System.String
struct String_t;
// PolyToolkitInternal.entitlement.OAuth2Identity
struct OAuth2Identity_t1039733419;
// PolyToolkitInternal.entitlement.OAuth2Identity/UserInfo
struct UserInfo_t3564753064;
// Newtonsoft.Json.Linq.JObject
struct JObject_t2059125928;
// System.Action
struct Action_t1264377477;
// System.Action`1<System.String>
struct Action_1_t2019918284;
// UnityEngine.Sprite
struct Sprite_t280657092;
// PolyToolkitInternal.RawImage
struct RawImage_t3303103980;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// PolyToolkitInternal.GltfTextureBase
struct GltfTextureBase_t3199118737;
// System.Collections.Generic.IEnumerator`1<PolyToolkitInternal.Gltf2Material/TextureInfo>
struct IEnumerator_1_t2967912198;
// PolyToolkitInternal.Gltf2Material/TextureInfo
struct TextureInfo_t2535341730;
// PolyToolkitInternal.Gltf2Material
struct Gltf2Material_t2703071627;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// PolyToolkitInternal.Gltf1Material
struct Gltf1Material_t2703072588;
// PolyToolkitInternal.Gltf1Program
struct Gltf1Program_t3399901429;
// PolyToolkitInternal.Gltf1Shader
struct Gltf1Shader_t1854243311;
// System.Net.HttpListener
struct HttpListener_t988452056;
// PolyToolkitInternal.entitlement.OAuth2Identity/<StartHttpListener>c__AnonStorey5
struct U3CStartHttpListenerU3Ec__AnonStorey5_t808655612;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Action`2<PolyToolkit.PolyStatus,PolyToolkit.PolyAsset>
struct Action_2_t2738134841;
// PolyToolkitInternal.api_clients.poly_client.PolyClient
struct PolyClient_t2759672288;
// PolyToolkit.PolyRequest
struct PolyRequest_t2374976743;
// System.Action`2<PolyToolkit.PolyStatus,PolyToolkit.PolyListAssetsResult>
struct Action_2_t1974849383;
// System.Text.RegularExpressions.Regex
struct Regex_t3657309853;
// System.Collections.Generic.List`1<UnityEngine.Material>
struct List_1_t1812449865;
// System.Collections.Generic.Dictionary`2<PolyToolkitInternal.GltfMaterialBase,PolyToolkitInternal.GltfMaterialConverter/UnityMaterial>
struct Dictionary_2_t1359220952;
// PolyToolkitInternal.GltfRootBase
struct GltfRootBase_t3065879770;
// System.Collections.Generic.IEnumerator`1<PolyToolkitInternal.GltfMaterialBase>
struct IEnumerator_1_t4039770520;
// PolyToolkitInternal.GltfMaterialBase
struct GltfMaterialBase_t3607200052;
// System.Collections.Generic.IEnumerator`1<PolyToolkitInternal.GltfTextureBase>
struct IEnumerator_1_t3631689205;
// PolyToolkitInternal.IUriLoader
struct IUriLoader_t4098220267;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.IDisposable
struct IDisposable_t3640265483;
// System.Collections.Generic.List`1<UnityEngine.Texture2D>
struct List_1_t1017553631;
// System.Collections.Generic.IEnumerator`1<PolyToolkitInternal.GltfImageBase>
struct IEnumerator_1_t280215335;
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t3089334924;
// CFX_ShurikenThreadFix
struct CFX_ShurikenThreadFix_t3300879551;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// PolyToolkitInternal.api_clients.poly_client.PolyClientUtils/GetRawFileDataTextCallback
struct GetRawFileDataTextCallback_t2904487790;
// System.Action`1<PolyToolkit.PolyStatus>
struct Action_1_t3317841535;
// PolyToolkitInternal.api_clients.poly_client.PolyClientUtils/GetRawFileDataBytesCallback
struct GetRawFileDataBytesCallback_t2880187408;
// PolyToolkitInternal.IBufferReader
struct IBufferReader_t3938825628;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t934056436;
// System.Array[]
struct ArrayU5BU5D_t2896390326;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// PolyToolkitInternal.Gltf2Image
struct Gltf2Image_t3036181135;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf1Node>
struct List_1_t2567860186;
// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf2Primitive>
struct List_1_t1060975091;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf2Node>
struct List_1_t2567860153;
// PolyToolkitInternal.Gltf2Buffer
struct Gltf2Buffer_t1264135663;
// UnityEngine.Material
struct Material_t340375123;
// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf1Primitive>
struct List_1_t1060976052;
// PolyToolkitInternal.Gltf1Primitive
struct Gltf1Primitive_t3883868606;
// PolyToolkitInternal.Gltf1Node
struct Gltf1Node_t1095785444;
// PolyToolkitInternal.Gltf2Primitive
struct Gltf2Primitive_t3883867645;
// PolyToolkitInternal.Gltf2Node
struct Gltf2Node_t1095785411;
// System.Void
struct Void_t1185182177;
// PolyToolkitInternal.TiltBrushGltf1PbrValues
struct TiltBrushGltf1PbrValues_t1054533882;
// PolyToolkitInternal.Gltf1Technique
struct Gltf1Technique_t503673868;
// System.Char[]
struct CharU5BU5D_t3528271667;
// PolyToolkitInternal.Gltf1Image
struct Gltf1Image_t3036181168;
// PolyToolkitInternal.Gltf1Buffer
struct Gltf1Buffer_t1264135694;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// PolyToolkitInternal.Gltf2Material/PbrMetallicRoughness
struct PbrMetallicRoughness_t2508879888;
// PolyToolkitInternal.Gltf1Scene
struct Gltf1Scene_t747889833;
// PolyToolkitInternal.GltfNodeBase
struct GltfNodeBase_t3005766873;
// PolyToolkit.PolyAsset
struct PolyAsset_t1814153511;
// PolyToolkitInternal.Gltf1Mesh
struct Gltf1Mesh_t1560025;
// PolyToolkitInternal.GltfPrimitiveBase
struct GltfPrimitiveBase_t1810376431;
// PolyToolkitInternal.Gltf2Mesh
struct Gltf2Mesh_t1559994;
// PolyToolkitInternal.Gltf2Scene
struct Gltf2Scene_t747889802;
// PolyToolkit.PolyListAssetsResult
struct PolyListAssetsResult_t1050868053;
// PolyToolkitInternal.Gltf1Texture
struct Gltf1Texture_t3945546188;
// PolyToolkitInternal.GltfAsset
struct GltfAsset_t1389371343;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t2869341516;
// PolyToolkitInternal.caching.PersistentBlobCache/CacheReadCallback
struct CacheReadCallback_t2055645553;
// System.Collections.Generic.List`1<PolyToolkitInternal.MeshPrecursor>
struct List_1_t1895342774;
// System.Collections.Generic.List`1<UnityEngine.Mesh>
struct List_1_t826071730;
// Newtonsoft.Json.Serialization.IContractResolver
struct IContractResolver_t4193385603;
// Newtonsoft.Json.JsonConverter[]
struct JsonConverterU5BU5D_t155849004;
// Newtonsoft.Json.Serialization.DefaultContractResolverState
struct DefaultContractResolverState_t3045992855;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// PolyToolkitInternal.AsyncImporter
struct AsyncImporter_t2267690090;
// PolyToolkit.PolyFormat
struct PolyFormat_t1880249796;
// PolyToolkitInternal.AsyncImporter/AsyncImportCallback
struct AsyncImportCallback_t3623524474;
// PolyToolkitInternal.ImportGltf/ImportState
struct ImportState_t482765084;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Collections.IEnumerable
struct IEnumerable_t1941168011;
// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf1Mesh>
struct List_1_t1473634767;
// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf2Buffer>
struct List_1_t2736210405;
// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf2Accessor>
struct List_1_t287795851;
// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf2BufferView>
struct List_1_t3267605692;
// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf2Mesh>
struct List_1_t1473634736;
// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf2Material>
struct List_1_t4175146369;
// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf2Scene>
struct List_1_t2219964544;
// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf2Texture>
struct List_1_t1122653603;
// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf2Image>
struct List_1_t213288581;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>,System.String>
struct Func_2_t2105257824;
// PolyToolkitInternal.Gltf2BufferView
struct Gltf2BufferView_t1795530950;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf2Accessor>
struct Dictionary_2_t2895944704;
// PolyToolkitInternal.Gltf2Accessor
struct Gltf2Accessor_t3110688405;
// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1Accessor>
struct Dictionary_2_t2895945665;
// PolyToolkitInternal.Gltf1Accessor
struct Gltf1Accessor_t3110689366;
// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1Buffer>
struct Dictionary_2_t1049391993;
// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1BufferView>
struct Dictionary_2_t1580788274;
// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1Mesh>
struct Dictionary_2_t4081783620;
// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1Shader>
struct Dictionary_2_t1639499610;
// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1Program>
struct Dictionary_2_t3185157728;
// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1Technique>
struct Dictionary_2_t288930167;
// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1Image>
struct Dictionary_2_t2821437467;
// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1Texture>
struct Dictionary_2_t3730802487;
// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1Material>
struct Dictionary_2_t2488328887;
// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1Node>
struct Dictionary_2_t881041743;
// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1Scene>
struct Dictionary_2_t533146132;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String>
struct Func_2_t3001800792;
// PolyToolkitInternal.Gltf1BufferView
struct Gltf1BufferView_t1795531975;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.GameObject>>
struct Dictionary_2_t1474424692;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1839659084;
// UnityEngine.UI.Text
struct Text_t1901882714;
// System.Security.Cryptography.MD5
struct MD5_t3177620429;
// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.caching.PersistentBlobCache/CacheEntry>
struct Dictionary_2_t3747976592;
// PolyToolkitInternal.model.util.ConcurrentQueue`1<PolyToolkitInternal.caching.PersistentBlobCache/CacheRequest>
struct ConcurrentQueue_1_t441838195;
// System.Func`2<PolyToolkitInternal.caching.PersistentBlobCache/CacheEntry,System.Int64>
struct Func_2_t1163785221;
// System.Collections.Generic.Dictionary`2<PolyToolkit.PolyCategory,System.String>
struct Dictionary_2_t3025884774;
// System.Collections.Generic.Dictionary`2<PolyToolkit.PolyOrderBy,System.String>
struct Dictionary_2_t247703358;
// System.Collections.Generic.Dictionary`2<PolyToolkit.PolyFormatFilter,System.String>
struct Dictionary_2_t3377995330;
// System.Collections.Generic.Dictionary`2<PolyToolkit.PolyVisibilityFilter,System.String>
struct Dictionary_2_t1991905614;
// System.Collections.Generic.Dictionary`2<PolyToolkit.PolyMaxComplexityFilter,System.String>
struct Dictionary_2_t2835058718;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t3014364904;
// System.Collections.Generic.Queue`1<PolyToolkitInternal.AsyncImporter/ImportOperation>
struct Queue_1_t1676077817;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CLOADPROFILEICONU3EC__ITERATOR3_T474336387_H
#define U3CLOADPROFILEICONU3EC__ITERATOR3_T474336387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.entitlement.OAuth2Identity/<LoadProfileIcon>c__Iterator3
struct  U3CLoadProfileIconU3Ec__Iterator3_t474336387  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest PolyToolkitInternal.entitlement.OAuth2Identity/<LoadProfileIcon>c__Iterator3::<www>__1
	UnityWebRequest_t463507806 * ___U3CwwwU3E__1_0;
	// System.String PolyToolkitInternal.entitlement.OAuth2Identity/<LoadProfileIcon>c__Iterator3::uri
	String_t* ___uri_1;
	// PolyToolkitInternal.entitlement.OAuth2Identity PolyToolkitInternal.entitlement.OAuth2Identity/<LoadProfileIcon>c__Iterator3::$this
	OAuth2Identity_t1039733419 * ___U24this_2;
	// System.Object PolyToolkitInternal.entitlement.OAuth2Identity/<LoadProfileIcon>c__Iterator3::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean PolyToolkitInternal.entitlement.OAuth2Identity/<LoadProfileIcon>c__Iterator3::$disposing
	bool ___U24disposing_4;
	// System.Int32 PolyToolkitInternal.entitlement.OAuth2Identity/<LoadProfileIcon>c__Iterator3::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__1_0() { return static_cast<int32_t>(offsetof(U3CLoadProfileIconU3Ec__Iterator3_t474336387, ___U3CwwwU3E__1_0)); }
	inline UnityWebRequest_t463507806 * get_U3CwwwU3E__1_0() const { return ___U3CwwwU3E__1_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwwwU3E__1_0() { return &___U3CwwwU3E__1_0; }
	inline void set_U3CwwwU3E__1_0(UnityWebRequest_t463507806 * value)
	{
		___U3CwwwU3E__1_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__1_0), value);
	}

	inline static int32_t get_offset_of_uri_1() { return static_cast<int32_t>(offsetof(U3CLoadProfileIconU3Ec__Iterator3_t474336387, ___uri_1)); }
	inline String_t* get_uri_1() const { return ___uri_1; }
	inline String_t** get_address_of_uri_1() { return &___uri_1; }
	inline void set_uri_1(String_t* value)
	{
		___uri_1 = value;
		Il2CppCodeGenWriteBarrier((&___uri_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CLoadProfileIconU3Ec__Iterator3_t474336387, ___U24this_2)); }
	inline OAuth2Identity_t1039733419 * get_U24this_2() const { return ___U24this_2; }
	inline OAuth2Identity_t1039733419 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(OAuth2Identity_t1039733419 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CLoadProfileIconU3Ec__Iterator3_t474336387, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CLoadProfileIconU3Ec__Iterator3_t474336387, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CLoadProfileIconU3Ec__Iterator3_t474336387, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADPROFILEICONU3EC__ITERATOR3_T474336387_H
#ifndef U3CGETUSERINFOU3EC__ITERATOR0_T2243535225_H
#define U3CGETUSERINFOU3EC__ITERATOR0_T2243535225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.entitlement.OAuth2Identity/<GetUserInfo>c__Iterator0
struct  U3CGetUserInfoU3Ec__Iterator0_t2243535225  : public RuntimeObject
{
public:
	// PolyToolkitInternal.entitlement.OAuth2Identity/UserInfo PolyToolkitInternal.entitlement.OAuth2Identity/<GetUserInfo>c__Iterator0::<user>__0
	UserInfo_t3564753064 * ___U3CuserU3E__0_0;
	// System.Int32 PolyToolkitInternal.entitlement.OAuth2Identity/<GetUserInfo>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_1;
	// UnityEngine.Networking.UnityWebRequest PolyToolkitInternal.entitlement.OAuth2Identity/<GetUserInfo>c__Iterator0::<www>__2
	UnityWebRequest_t463507806 * ___U3CwwwU3E__2_2;
	// Newtonsoft.Json.Linq.JObject PolyToolkitInternal.entitlement.OAuth2Identity/<GetUserInfo>c__Iterator0::<json>__3
	JObject_t2059125928 * ___U3CjsonU3E__3_3;
	// System.String PolyToolkitInternal.entitlement.OAuth2Identity/<GetUserInfo>c__Iterator0::<iconUri>__3
	String_t* ___U3CiconUriU3E__3_4;
	// PolyToolkitInternal.entitlement.OAuth2Identity PolyToolkitInternal.entitlement.OAuth2Identity/<GetUserInfo>c__Iterator0::$this
	OAuth2Identity_t1039733419 * ___U24this_5;
	// System.Object PolyToolkitInternal.entitlement.OAuth2Identity/<GetUserInfo>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean PolyToolkitInternal.entitlement.OAuth2Identity/<GetUserInfo>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 PolyToolkitInternal.entitlement.OAuth2Identity/<GetUserInfo>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CuserU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetUserInfoU3Ec__Iterator0_t2243535225, ___U3CuserU3E__0_0)); }
	inline UserInfo_t3564753064 * get_U3CuserU3E__0_0() const { return ___U3CuserU3E__0_0; }
	inline UserInfo_t3564753064 ** get_address_of_U3CuserU3E__0_0() { return &___U3CuserU3E__0_0; }
	inline void set_U3CuserU3E__0_0(UserInfo_t3564753064 * value)
	{
		___U3CuserU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CuserU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CiU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetUserInfoU3Ec__Iterator0_t2243535225, ___U3CiU3E__1_1)); }
	inline int32_t get_U3CiU3E__1_1() const { return ___U3CiU3E__1_1; }
	inline int32_t* get_address_of_U3CiU3E__1_1() { return &___U3CiU3E__1_1; }
	inline void set_U3CiU3E__1_1(int32_t value)
	{
		___U3CiU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CwwwU3E__2_2() { return static_cast<int32_t>(offsetof(U3CGetUserInfoU3Ec__Iterator0_t2243535225, ___U3CwwwU3E__2_2)); }
	inline UnityWebRequest_t463507806 * get_U3CwwwU3E__2_2() const { return ___U3CwwwU3E__2_2; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwwwU3E__2_2() { return &___U3CwwwU3E__2_2; }
	inline void set_U3CwwwU3E__2_2(UnityWebRequest_t463507806 * value)
	{
		___U3CwwwU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__2_2), value);
	}

	inline static int32_t get_offset_of_U3CjsonU3E__3_3() { return static_cast<int32_t>(offsetof(U3CGetUserInfoU3Ec__Iterator0_t2243535225, ___U3CjsonU3E__3_3)); }
	inline JObject_t2059125928 * get_U3CjsonU3E__3_3() const { return ___U3CjsonU3E__3_3; }
	inline JObject_t2059125928 ** get_address_of_U3CjsonU3E__3_3() { return &___U3CjsonU3E__3_3; }
	inline void set_U3CjsonU3E__3_3(JObject_t2059125928 * value)
	{
		___U3CjsonU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonU3E__3_3), value);
	}

	inline static int32_t get_offset_of_U3CiconUriU3E__3_4() { return static_cast<int32_t>(offsetof(U3CGetUserInfoU3Ec__Iterator0_t2243535225, ___U3CiconUriU3E__3_4)); }
	inline String_t* get_U3CiconUriU3E__3_4() const { return ___U3CiconUriU3E__3_4; }
	inline String_t** get_address_of_U3CiconUriU3E__3_4() { return &___U3CiconUriU3E__3_4; }
	inline void set_U3CiconUriU3E__3_4(String_t* value)
	{
		___U3CiconUriU3E__3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CiconUriU3E__3_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CGetUserInfoU3Ec__Iterator0_t2243535225, ___U24this_5)); }
	inline OAuth2Identity_t1039733419 * get_U24this_5() const { return ___U24this_5; }
	inline OAuth2Identity_t1039733419 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(OAuth2Identity_t1039733419 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CGetUserInfoU3Ec__Iterator0_t2243535225, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CGetUserInfoU3Ec__Iterator0_t2243535225, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CGetUserInfoU3Ec__Iterator0_t2243535225, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

struct U3CGetUserInfoU3Ec__Iterator0_t2243535225_StaticFields
{
public:
	// System.Action PolyToolkitInternal.entitlement.OAuth2Identity/<GetUserInfo>c__Iterator0::<>f__am$cache0
	Action_t1264377477 * ___U3CU3Ef__amU24cache0_9;
	// System.Action`1<System.String> PolyToolkitInternal.entitlement.OAuth2Identity/<GetUserInfo>c__Iterator0::<>f__am$cache1
	Action_1_t2019918284 * ___U3CU3Ef__amU24cache1_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(U3CGetUserInfoU3Ec__Iterator0_t2243535225_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_10() { return static_cast<int32_t>(offsetof(U3CGetUserInfoU3Ec__Iterator0_t2243535225_StaticFields, ___U3CU3Ef__amU24cache1_10)); }
	inline Action_1_t2019918284 * get_U3CU3Ef__amU24cache1_10() const { return ___U3CU3Ef__amU24cache1_10; }
	inline Action_1_t2019918284 ** get_address_of_U3CU3Ef__amU24cache1_10() { return &___U3CU3Ef__amU24cache1_10; }
	inline void set_U3CU3Ef__amU24cache1_10(Action_1_t2019918284 * value)
	{
		___U3CU3Ef__amU24cache1_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETUSERINFOU3EC__ITERATOR0_T2243535225_H
#ifndef USERINFO_T3564753064_H
#define USERINFO_T3564753064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.entitlement.OAuth2Identity/UserInfo
struct  UserInfo_t3564753064  : public RuntimeObject
{
public:
	// System.String PolyToolkitInternal.entitlement.OAuth2Identity/UserInfo::id
	String_t* ___id_0;
	// System.String PolyToolkitInternal.entitlement.OAuth2Identity/UserInfo::name
	String_t* ___name_1;
	// System.String PolyToolkitInternal.entitlement.OAuth2Identity/UserInfo::email
	String_t* ___email_2;
	// System.String PolyToolkitInternal.entitlement.OAuth2Identity/UserInfo::location
	String_t* ___location_3;
	// UnityEngine.Sprite PolyToolkitInternal.entitlement.OAuth2Identity/UserInfo::icon
	Sprite_t280657092 * ___icon_4;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(UserInfo_t3564753064, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(UserInfo_t3564753064, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_email_2() { return static_cast<int32_t>(offsetof(UserInfo_t3564753064, ___email_2)); }
	inline String_t* get_email_2() const { return ___email_2; }
	inline String_t** get_address_of_email_2() { return &___email_2; }
	inline void set_email_2(String_t* value)
	{
		___email_2 = value;
		Il2CppCodeGenWriteBarrier((&___email_2), value);
	}

	inline static int32_t get_offset_of_location_3() { return static_cast<int32_t>(offsetof(UserInfo_t3564753064, ___location_3)); }
	inline String_t* get_location_3() const { return ___location_3; }
	inline String_t** get_address_of_location_3() { return &___location_3; }
	inline void set_location_3(String_t* value)
	{
		___location_3 = value;
		Il2CppCodeGenWriteBarrier((&___location_3), value);
	}

	inline static int32_t get_offset_of_icon_4() { return static_cast<int32_t>(offsetof(UserInfo_t3564753064, ___icon_4)); }
	inline Sprite_t280657092 * get_icon_4() const { return ___icon_4; }
	inline Sprite_t280657092 ** get_address_of_icon_4() { return &___icon_4; }
	inline void set_icon_4(Sprite_t280657092 * value)
	{
		___icon_4 = value;
		Il2CppCodeGenWriteBarrier((&___icon_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERINFO_T3564753064_H
#ifndef GLTFIMAGEBASE_T4142612163_H
#define GLTFIMAGEBASE_T4142612163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfImageBase
struct  GltfImageBase_t4142612163  : public RuntimeObject
{
public:
	// System.String PolyToolkitInternal.GltfImageBase::uri
	String_t* ___uri_0;
	// PolyToolkitInternal.RawImage PolyToolkitInternal.GltfImageBase::data
	RawImage_t3303103980 * ___data_1;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(GltfImageBase_t4142612163, ___uri_0)); }
	inline String_t* get_uri_0() const { return ___uri_0; }
	inline String_t** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(String_t* value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(GltfImageBase_t4142612163, ___data_1)); }
	inline RawImage_t3303103980 * get_data_1() const { return ___data_1; }
	inline RawImage_t3303103980 ** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(RawImage_t3303103980 * value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFIMAGEBASE_T4142612163_H
#ifndef GLTFTEXTUREBASE_T3199118737_H
#define GLTFTEXTUREBASE_T3199118737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfTextureBase
struct  GltfTextureBase_t3199118737  : public RuntimeObject
{
public:
	// UnityEngine.Texture2D PolyToolkitInternal.GltfTextureBase::unityTexture
	Texture2D_t3840446185 * ___unityTexture_0;

public:
	inline static int32_t get_offset_of_unityTexture_0() { return static_cast<int32_t>(offsetof(GltfTextureBase_t3199118737, ___unityTexture_0)); }
	inline Texture2D_t3840446185 * get_unityTexture_0() const { return ___unityTexture_0; }
	inline Texture2D_t3840446185 ** get_address_of_unityTexture_0() { return &___unityTexture_0; }
	inline void set_unityTexture_0(Texture2D_t3840446185 * value)
	{
		___unityTexture_0 = value;
		Il2CppCodeGenWriteBarrier((&___unityTexture_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFTEXTUREBASE_T3199118737_H
#ifndef TEXTUREINFO_T2535341730_H
#define TEXTUREINFO_T2535341730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf2Material/TextureInfo
struct  TextureInfo_t2535341730  : public RuntimeObject
{
public:
	// System.Int32 PolyToolkitInternal.Gltf2Material/TextureInfo::index
	int32_t ___index_0;
	// System.Int32 PolyToolkitInternal.Gltf2Material/TextureInfo::texCoord
	int32_t ___texCoord_1;
	// PolyToolkitInternal.GltfTextureBase PolyToolkitInternal.Gltf2Material/TextureInfo::texture
	GltfTextureBase_t3199118737 * ___texture_2;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(TextureInfo_t2535341730, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_texCoord_1() { return static_cast<int32_t>(offsetof(TextureInfo_t2535341730, ___texCoord_1)); }
	inline int32_t get_texCoord_1() const { return ___texCoord_1; }
	inline int32_t* get_address_of_texCoord_1() { return &___texCoord_1; }
	inline void set_texCoord_1(int32_t value)
	{
		___texCoord_1 = value;
	}

	inline static int32_t get_offset_of_texture_2() { return static_cast<int32_t>(offsetof(TextureInfo_t2535341730, ___texture_2)); }
	inline GltfTextureBase_t3199118737 * get_texture_2() const { return ___texture_2; }
	inline GltfTextureBase_t3199118737 ** get_address_of_texture_2() { return &___texture_2; }
	inline void set_texture_2(GltfTextureBase_t3199118737 * value)
	{
		___texture_2 = value;
		Il2CppCodeGenWriteBarrier((&___texture_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREINFO_T2535341730_H
#ifndef U3CU3EC__ITERATOR1_T3170205985_H
#define U3CU3EC__ITERATOR1_T3170205985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf2Material/<>c__Iterator1
struct  U3CU3Ec__Iterator1_t3170205985  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerator`1<PolyToolkitInternal.Gltf2Material/TextureInfo> PolyToolkitInternal.Gltf2Material/<>c__Iterator1::$locvar0
	RuntimeObject* ___U24locvar0_0;
	// PolyToolkitInternal.Gltf2Material/TextureInfo PolyToolkitInternal.Gltf2Material/<>c__Iterator1::<ti>__1
	TextureInfo_t2535341730 * ___U3CtiU3E__1_1;
	// PolyToolkitInternal.Gltf2Material PolyToolkitInternal.Gltf2Material/<>c__Iterator1::$this
	Gltf2Material_t2703071627 * ___U24this_2;
	// PolyToolkitInternal.GltfTextureBase PolyToolkitInternal.Gltf2Material/<>c__Iterator1::$current
	GltfTextureBase_t3199118737 * ___U24current_3;
	// System.Boolean PolyToolkitInternal.Gltf2Material/<>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 PolyToolkitInternal.Gltf2Material/<>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t3170205985, ___U24locvar0_0)); }
	inline RuntimeObject* get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline RuntimeObject** get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(RuntimeObject* value)
	{
		___U24locvar0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_0), value);
	}

	inline static int32_t get_offset_of_U3CtiU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t3170205985, ___U3CtiU3E__1_1)); }
	inline TextureInfo_t2535341730 * get_U3CtiU3E__1_1() const { return ___U3CtiU3E__1_1; }
	inline TextureInfo_t2535341730 ** get_address_of_U3CtiU3E__1_1() { return &___U3CtiU3E__1_1; }
	inline void set_U3CtiU3E__1_1(TextureInfo_t2535341730 * value)
	{
		___U3CtiU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtiU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t3170205985, ___U24this_2)); }
	inline Gltf2Material_t2703071627 * get_U24this_2() const { return ___U24this_2; }
	inline Gltf2Material_t2703071627 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(Gltf2Material_t2703071627 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t3170205985, ___U24current_3)); }
	inline GltfTextureBase_t3199118737 * get_U24current_3() const { return ___U24current_3; }
	inline GltfTextureBase_t3199118737 ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(GltfTextureBase_t3199118737 * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t3170205985, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t3170205985, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR1_T3170205985_H
#ifndef GLTFMESHBASE_T3664755873_H
#define GLTFMESHBASE_T3664755873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfMeshBase
struct  GltfMeshBase_t3664755873  : public RuntimeObject
{
public:
	// System.String PolyToolkitInternal.GltfMeshBase::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(GltfMeshBase_t3664755873, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFMESHBASE_T3664755873_H
#ifndef CACHEENTRY_T3962720293_H
#define CACHEENTRY_T3962720293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.caching.PersistentBlobCache/CacheEntry
struct  CacheEntry_t3962720293  : public RuntimeObject
{
public:
	// System.String PolyToolkitInternal.caching.PersistentBlobCache/CacheEntry::hash
	String_t* ___hash_0;
	// System.Int64 PolyToolkitInternal.caching.PersistentBlobCache/CacheEntry::fileSize
	int64_t ___fileSize_1;
	// System.Int64 PolyToolkitInternal.caching.PersistentBlobCache/CacheEntry::writeTimestampMillis
	int64_t ___writeTimestampMillis_2;
	// System.Int64 PolyToolkitInternal.caching.PersistentBlobCache/CacheEntry::readTimestampMillis
	int64_t ___readTimestampMillis_3;

public:
	inline static int32_t get_offset_of_hash_0() { return static_cast<int32_t>(offsetof(CacheEntry_t3962720293, ___hash_0)); }
	inline String_t* get_hash_0() const { return ___hash_0; }
	inline String_t** get_address_of_hash_0() { return &___hash_0; }
	inline void set_hash_0(String_t* value)
	{
		___hash_0 = value;
		Il2CppCodeGenWriteBarrier((&___hash_0), value);
	}

	inline static int32_t get_offset_of_fileSize_1() { return static_cast<int32_t>(offsetof(CacheEntry_t3962720293, ___fileSize_1)); }
	inline int64_t get_fileSize_1() const { return ___fileSize_1; }
	inline int64_t* get_address_of_fileSize_1() { return &___fileSize_1; }
	inline void set_fileSize_1(int64_t value)
	{
		___fileSize_1 = value;
	}

	inline static int32_t get_offset_of_writeTimestampMillis_2() { return static_cast<int32_t>(offsetof(CacheEntry_t3962720293, ___writeTimestampMillis_2)); }
	inline int64_t get_writeTimestampMillis_2() const { return ___writeTimestampMillis_2; }
	inline int64_t* get_address_of_writeTimestampMillis_2() { return &___writeTimestampMillis_2; }
	inline void set_writeTimestampMillis_2(int64_t value)
	{
		___writeTimestampMillis_2 = value;
	}

	inline static int32_t get_offset_of_readTimestampMillis_3() { return static_cast<int32_t>(offsetof(CacheEntry_t3962720293, ___readTimestampMillis_3)); }
	inline int64_t get_readTimestampMillis_3() const { return ___readTimestampMillis_3; }
	inline int64_t* get_address_of_readTimestampMillis_3() { return &___readTimestampMillis_3; }
	inline void set_readTimestampMillis_3(int64_t value)
	{
		___readTimestampMillis_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEENTRY_T3962720293_H
#ifndef GLTFMATERIALBASE_T3607200052_H
#define GLTFMATERIALBASE_T3607200052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfMaterialBase
struct  GltfMaterialBase_t3607200052  : public RuntimeObject
{
public:
	// System.String PolyToolkitInternal.GltfMaterialBase::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(GltfMaterialBase_t3607200052, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFMATERIALBASE_T3607200052_H
#ifndef U3CU3EC__ITERATOR0_T831553825_H
#define U3CU3EC__ITERATOR0_T831553825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf2Material/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t831553825  : public RuntimeObject
{
public:
	// PolyToolkitInternal.Gltf2Material PolyToolkitInternal.Gltf2Material/<>c__Iterator0::$this
	Gltf2Material_t2703071627 * ___U24this_0;
	// PolyToolkitInternal.Gltf2Material/TextureInfo PolyToolkitInternal.Gltf2Material/<>c__Iterator0::$current
	TextureInfo_t2535341730 * ___U24current_1;
	// System.Boolean PolyToolkitInternal.Gltf2Material/<>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 PolyToolkitInternal.Gltf2Material/<>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t831553825, ___U24this_0)); }
	inline Gltf2Material_t2703071627 * get_U24this_0() const { return ___U24this_0; }
	inline Gltf2Material_t2703071627 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Gltf2Material_t2703071627 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t831553825, ___U24current_1)); }
	inline TextureInfo_t2535341730 * get_U24current_1() const { return ___U24current_1; }
	inline TextureInfo_t2535341730 ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(TextureInfo_t2535341730 * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t831553825, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t831553825, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T831553825_H
#ifndef COROUTINERUNNER_T64893240_H
#define COROUTINERUNNER_T64893240_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.CoroutineRunner
struct  CoroutineRunner_t64893240  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COROUTINERUNNER_T64893240_H
#ifndef U3CREAUTHORIZEU3EC__ITERATOR1_T739728412_H
#define U3CREAUTHORIZEU3EC__ITERATOR1_T739728412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.entitlement.OAuth2Identity/<Reauthorize>c__Iterator1
struct  U3CReauthorizeU3Ec__Iterator1_t739728412  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PolyToolkitInternal.entitlement.OAuth2Identity/<Reauthorize>c__Iterator1::<parameters>__1
	Dictionary_2_t1632706988 * ___U3CparametersU3E__1_0;
	// UnityEngine.Networking.UnityWebRequest PolyToolkitInternal.entitlement.OAuth2Identity/<Reauthorize>c__Iterator1::<www>__2
	UnityWebRequest_t463507806 * ___U3CwwwU3E__2_1;
	// System.Action`1<System.String> PolyToolkitInternal.entitlement.OAuth2Identity/<Reauthorize>c__Iterator1::failureCallback
	Action_1_t2019918284 * ___failureCallback_2;
	// System.Action PolyToolkitInternal.entitlement.OAuth2Identity/<Reauthorize>c__Iterator1::successCallback
	Action_t1264377477 * ___successCallback_3;
	// PolyToolkitInternal.entitlement.OAuth2Identity PolyToolkitInternal.entitlement.OAuth2Identity/<Reauthorize>c__Iterator1::$this
	OAuth2Identity_t1039733419 * ___U24this_4;
	// System.Object PolyToolkitInternal.entitlement.OAuth2Identity/<Reauthorize>c__Iterator1::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean PolyToolkitInternal.entitlement.OAuth2Identity/<Reauthorize>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 PolyToolkitInternal.entitlement.OAuth2Identity/<Reauthorize>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CparametersU3E__1_0() { return static_cast<int32_t>(offsetof(U3CReauthorizeU3Ec__Iterator1_t739728412, ___U3CparametersU3E__1_0)); }
	inline Dictionary_2_t1632706988 * get_U3CparametersU3E__1_0() const { return ___U3CparametersU3E__1_0; }
	inline Dictionary_2_t1632706988 ** get_address_of_U3CparametersU3E__1_0() { return &___U3CparametersU3E__1_0; }
	inline void set_U3CparametersU3E__1_0(Dictionary_2_t1632706988 * value)
	{
		___U3CparametersU3E__1_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CparametersU3E__1_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__2_1() { return static_cast<int32_t>(offsetof(U3CReauthorizeU3Ec__Iterator1_t739728412, ___U3CwwwU3E__2_1)); }
	inline UnityWebRequest_t463507806 * get_U3CwwwU3E__2_1() const { return ___U3CwwwU3E__2_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwwwU3E__2_1() { return &___U3CwwwU3E__2_1; }
	inline void set_U3CwwwU3E__2_1(UnityWebRequest_t463507806 * value)
	{
		___U3CwwwU3E__2_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__2_1), value);
	}

	inline static int32_t get_offset_of_failureCallback_2() { return static_cast<int32_t>(offsetof(U3CReauthorizeU3Ec__Iterator1_t739728412, ___failureCallback_2)); }
	inline Action_1_t2019918284 * get_failureCallback_2() const { return ___failureCallback_2; }
	inline Action_1_t2019918284 ** get_address_of_failureCallback_2() { return &___failureCallback_2; }
	inline void set_failureCallback_2(Action_1_t2019918284 * value)
	{
		___failureCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___failureCallback_2), value);
	}

	inline static int32_t get_offset_of_successCallback_3() { return static_cast<int32_t>(offsetof(U3CReauthorizeU3Ec__Iterator1_t739728412, ___successCallback_3)); }
	inline Action_t1264377477 * get_successCallback_3() const { return ___successCallback_3; }
	inline Action_t1264377477 ** get_address_of_successCallback_3() { return &___successCallback_3; }
	inline void set_successCallback_3(Action_t1264377477 * value)
	{
		___successCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___successCallback_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CReauthorizeU3Ec__Iterator1_t739728412, ___U24this_4)); }
	inline OAuth2Identity_t1039733419 * get_U24this_4() const { return ___U24this_4; }
	inline OAuth2Identity_t1039733419 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(OAuth2Identity_t1039733419 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CReauthorizeU3Ec__Iterator1_t739728412, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CReauthorizeU3Ec__Iterator1_t739728412, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CReauthorizeU3Ec__Iterator1_t739728412, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREAUTHORIZEU3EC__ITERATOR1_T739728412_H
#ifndef U3CU3EC__ITERATOR0_T446849591_H
#define U3CU3EC__ITERATOR0_T446849591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf1Material/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t446849591  : public RuntimeObject
{
public:
	// PolyToolkitInternal.Gltf1Material PolyToolkitInternal.Gltf1Material/<>c__Iterator0::$this
	Gltf1Material_t2703072588 * ___U24this_0;
	// PolyToolkitInternal.GltfTextureBase PolyToolkitInternal.Gltf1Material/<>c__Iterator0::$current
	GltfTextureBase_t3199118737 * ___U24current_1;
	// System.Boolean PolyToolkitInternal.Gltf1Material/<>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 PolyToolkitInternal.Gltf1Material/<>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t446849591, ___U24this_0)); }
	inline Gltf1Material_t2703072588 * get_U24this_0() const { return ___U24this_0; }
	inline Gltf1Material_t2703072588 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Gltf1Material_t2703072588 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t446849591, ___U24current_1)); }
	inline GltfTextureBase_t3199118737 * get_U24current_1() const { return ___U24current_1; }
	inline GltfTextureBase_t3199118737 ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(GltfTextureBase_t3199118737 * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t446849591, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t446849591, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T446849591_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef GLTF1TECHNIQUE_T503673868_H
#define GLTF1TECHNIQUE_T503673868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf1Technique
struct  Gltf1Technique_t503673868  : public RuntimeObject
{
public:
	// System.String PolyToolkitInternal.Gltf1Technique::program
	String_t* ___program_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PolyToolkitInternal.Gltf1Technique::extras
	Dictionary_2_t1632706988 * ___extras_1;
	// System.String PolyToolkitInternal.Gltf1Technique::gltfId
	String_t* ___gltfId_2;
	// PolyToolkitInternal.Gltf1Program PolyToolkitInternal.Gltf1Technique::programPtr
	Gltf1Program_t3399901429 * ___programPtr_3;

public:
	inline static int32_t get_offset_of_program_0() { return static_cast<int32_t>(offsetof(Gltf1Technique_t503673868, ___program_0)); }
	inline String_t* get_program_0() const { return ___program_0; }
	inline String_t** get_address_of_program_0() { return &___program_0; }
	inline void set_program_0(String_t* value)
	{
		___program_0 = value;
		Il2CppCodeGenWriteBarrier((&___program_0), value);
	}

	inline static int32_t get_offset_of_extras_1() { return static_cast<int32_t>(offsetof(Gltf1Technique_t503673868, ___extras_1)); }
	inline Dictionary_2_t1632706988 * get_extras_1() const { return ___extras_1; }
	inline Dictionary_2_t1632706988 ** get_address_of_extras_1() { return &___extras_1; }
	inline void set_extras_1(Dictionary_2_t1632706988 * value)
	{
		___extras_1 = value;
		Il2CppCodeGenWriteBarrier((&___extras_1), value);
	}

	inline static int32_t get_offset_of_gltfId_2() { return static_cast<int32_t>(offsetof(Gltf1Technique_t503673868, ___gltfId_2)); }
	inline String_t* get_gltfId_2() const { return ___gltfId_2; }
	inline String_t** get_address_of_gltfId_2() { return &___gltfId_2; }
	inline void set_gltfId_2(String_t* value)
	{
		___gltfId_2 = value;
		Il2CppCodeGenWriteBarrier((&___gltfId_2), value);
	}

	inline static int32_t get_offset_of_programPtr_3() { return static_cast<int32_t>(offsetof(Gltf1Technique_t503673868, ___programPtr_3)); }
	inline Gltf1Program_t3399901429 * get_programPtr_3() const { return ___programPtr_3; }
	inline Gltf1Program_t3399901429 ** get_address_of_programPtr_3() { return &___programPtr_3; }
	inline void set_programPtr_3(Gltf1Program_t3399901429 * value)
	{
		___programPtr_3 = value;
		Il2CppCodeGenWriteBarrier((&___programPtr_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF1TECHNIQUE_T503673868_H
#ifndef GLTF1PROGRAM_T3399901429_H
#define GLTF1PROGRAM_T3399901429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf1Program
struct  Gltf1Program_t3399901429  : public RuntimeObject
{
public:
	// System.String PolyToolkitInternal.Gltf1Program::vertexShader
	String_t* ___vertexShader_0;
	// System.String PolyToolkitInternal.Gltf1Program::fragmentShader
	String_t* ___fragmentShader_1;
	// System.String PolyToolkitInternal.Gltf1Program::gltfId
	String_t* ___gltfId_2;
	// PolyToolkitInternal.Gltf1Shader PolyToolkitInternal.Gltf1Program::vertexShaderPtr
	Gltf1Shader_t1854243311 * ___vertexShaderPtr_3;
	// PolyToolkitInternal.Gltf1Shader PolyToolkitInternal.Gltf1Program::fragmentShaderPtr
	Gltf1Shader_t1854243311 * ___fragmentShaderPtr_4;

public:
	inline static int32_t get_offset_of_vertexShader_0() { return static_cast<int32_t>(offsetof(Gltf1Program_t3399901429, ___vertexShader_0)); }
	inline String_t* get_vertexShader_0() const { return ___vertexShader_0; }
	inline String_t** get_address_of_vertexShader_0() { return &___vertexShader_0; }
	inline void set_vertexShader_0(String_t* value)
	{
		___vertexShader_0 = value;
		Il2CppCodeGenWriteBarrier((&___vertexShader_0), value);
	}

	inline static int32_t get_offset_of_fragmentShader_1() { return static_cast<int32_t>(offsetof(Gltf1Program_t3399901429, ___fragmentShader_1)); }
	inline String_t* get_fragmentShader_1() const { return ___fragmentShader_1; }
	inline String_t** get_address_of_fragmentShader_1() { return &___fragmentShader_1; }
	inline void set_fragmentShader_1(String_t* value)
	{
		___fragmentShader_1 = value;
		Il2CppCodeGenWriteBarrier((&___fragmentShader_1), value);
	}

	inline static int32_t get_offset_of_gltfId_2() { return static_cast<int32_t>(offsetof(Gltf1Program_t3399901429, ___gltfId_2)); }
	inline String_t* get_gltfId_2() const { return ___gltfId_2; }
	inline String_t** get_address_of_gltfId_2() { return &___gltfId_2; }
	inline void set_gltfId_2(String_t* value)
	{
		___gltfId_2 = value;
		Il2CppCodeGenWriteBarrier((&___gltfId_2), value);
	}

	inline static int32_t get_offset_of_vertexShaderPtr_3() { return static_cast<int32_t>(offsetof(Gltf1Program_t3399901429, ___vertexShaderPtr_3)); }
	inline Gltf1Shader_t1854243311 * get_vertexShaderPtr_3() const { return ___vertexShaderPtr_3; }
	inline Gltf1Shader_t1854243311 ** get_address_of_vertexShaderPtr_3() { return &___vertexShaderPtr_3; }
	inline void set_vertexShaderPtr_3(Gltf1Shader_t1854243311 * value)
	{
		___vertexShaderPtr_3 = value;
		Il2CppCodeGenWriteBarrier((&___vertexShaderPtr_3), value);
	}

	inline static int32_t get_offset_of_fragmentShaderPtr_4() { return static_cast<int32_t>(offsetof(Gltf1Program_t3399901429, ___fragmentShaderPtr_4)); }
	inline Gltf1Shader_t1854243311 * get_fragmentShaderPtr_4() const { return ___fragmentShaderPtr_4; }
	inline Gltf1Shader_t1854243311 ** get_address_of_fragmentShaderPtr_4() { return &___fragmentShaderPtr_4; }
	inline void set_fragmentShaderPtr_4(Gltf1Shader_t1854243311 * value)
	{
		___fragmentShaderPtr_4 = value;
		Il2CppCodeGenWriteBarrier((&___fragmentShaderPtr_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF1PROGRAM_T3399901429_H
#ifndef GLTF1SHADER_T1854243311_H
#define GLTF1SHADER_T1854243311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf1Shader
struct  Gltf1Shader_t1854243311  : public RuntimeObject
{
public:
	// System.String PolyToolkitInternal.Gltf1Shader::gltfId
	String_t* ___gltfId_2;
	// System.String PolyToolkitInternal.Gltf1Shader::uri
	String_t* ___uri_3;
	// System.Int32 PolyToolkitInternal.Gltf1Shader::type
	int32_t ___type_4;

public:
	inline static int32_t get_offset_of_gltfId_2() { return static_cast<int32_t>(offsetof(Gltf1Shader_t1854243311, ___gltfId_2)); }
	inline String_t* get_gltfId_2() const { return ___gltfId_2; }
	inline String_t** get_address_of_gltfId_2() { return &___gltfId_2; }
	inline void set_gltfId_2(String_t* value)
	{
		___gltfId_2 = value;
		Il2CppCodeGenWriteBarrier((&___gltfId_2), value);
	}

	inline static int32_t get_offset_of_uri_3() { return static_cast<int32_t>(offsetof(Gltf1Shader_t1854243311, ___uri_3)); }
	inline String_t* get_uri_3() const { return ___uri_3; }
	inline String_t** get_address_of_uri_3() { return &___uri_3; }
	inline void set_uri_3(String_t* value)
	{
		___uri_3 = value;
		Il2CppCodeGenWriteBarrier((&___uri_3), value);
	}

	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(Gltf1Shader_t1854243311, ___type_4)); }
	inline int32_t get_type_4() const { return ___type_4; }
	inline int32_t* get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(int32_t value)
	{
		___type_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF1SHADER_T1854243311_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef FEATURES_T1280010604_H
#define FEATURES_T1280010604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Features
struct  Features_t1280010604  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEATURES_T1280010604_H
#ifndef GLTFBUFFERVIEWBASE_T1504165784_H
#define GLTFBUFFERVIEWBASE_T1504165784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfBufferViewBase
struct  GltfBufferViewBase_t1504165784  : public RuntimeObject
{
public:
	// System.Int32 PolyToolkitInternal.GltfBufferViewBase::byteLength
	int32_t ___byteLength_0;
	// System.Int32 PolyToolkitInternal.GltfBufferViewBase::byteOffset
	int32_t ___byteOffset_1;
	// System.Int32 PolyToolkitInternal.GltfBufferViewBase::target
	int32_t ___target_2;

public:
	inline static int32_t get_offset_of_byteLength_0() { return static_cast<int32_t>(offsetof(GltfBufferViewBase_t1504165784, ___byteLength_0)); }
	inline int32_t get_byteLength_0() const { return ___byteLength_0; }
	inline int32_t* get_address_of_byteLength_0() { return &___byteLength_0; }
	inline void set_byteLength_0(int32_t value)
	{
		___byteLength_0 = value;
	}

	inline static int32_t get_offset_of_byteOffset_1() { return static_cast<int32_t>(offsetof(GltfBufferViewBase_t1504165784, ___byteOffset_1)); }
	inline int32_t get_byteOffset_1() const { return ___byteOffset_1; }
	inline int32_t* get_address_of_byteOffset_1() { return &___byteOffset_1; }
	inline void set_byteOffset_1(int32_t value)
	{
		___byteOffset_1 = value;
	}

	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(GltfBufferViewBase_t1504165784, ___target_2)); }
	inline int32_t get_target_2() const { return ___target_2; }
	inline int32_t* get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(int32_t value)
	{
		___target_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFBUFFERVIEWBASE_T1504165784_H
#ifndef U3CSTARTHTTPLISTENERU3EC__ANONSTOREY4_T3147307772_H
#define U3CSTARTHTTPLISTENERU3EC__ANONSTOREY4_T3147307772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.entitlement.OAuth2Identity/<StartHttpListener>c__AnonStorey4
struct  U3CStartHttpListenerU3Ec__AnonStorey4_t3147307772  : public RuntimeObject
{
public:
	// System.Net.HttpListener PolyToolkitInternal.entitlement.OAuth2Identity/<StartHttpListener>c__AnonStorey4::listener
	HttpListener_t988452056 * ___listener_0;
	// PolyToolkitInternal.entitlement.OAuth2Identity/<StartHttpListener>c__AnonStorey5 PolyToolkitInternal.entitlement.OAuth2Identity/<StartHttpListener>c__AnonStorey4::<>f__ref$5
	U3CStartHttpListenerU3Ec__AnonStorey5_t808655612 * ___U3CU3Ef__refU245_1;

public:
	inline static int32_t get_offset_of_listener_0() { return static_cast<int32_t>(offsetof(U3CStartHttpListenerU3Ec__AnonStorey4_t3147307772, ___listener_0)); }
	inline HttpListener_t988452056 * get_listener_0() const { return ___listener_0; }
	inline HttpListener_t988452056 ** get_address_of_listener_0() { return &___listener_0; }
	inline void set_listener_0(HttpListener_t988452056 * value)
	{
		___listener_0 = value;
		Il2CppCodeGenWriteBarrier((&___listener_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU245_1() { return static_cast<int32_t>(offsetof(U3CStartHttpListenerU3Ec__AnonStorey4_t3147307772, ___U3CU3Ef__refU245_1)); }
	inline U3CStartHttpListenerU3Ec__AnonStorey5_t808655612 * get_U3CU3Ef__refU245_1() const { return ___U3CU3Ef__refU245_1; }
	inline U3CStartHttpListenerU3Ec__AnonStorey5_t808655612 ** get_address_of_U3CU3Ef__refU245_1() { return &___U3CU3Ef__refU245_1; }
	inline void set_U3CU3Ef__refU245_1(U3CStartHttpListenerU3Ec__AnonStorey5_t808655612 * value)
	{
		___U3CU3Ef__refU245_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU245_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTHTTPLISTENERU3EC__ANONSTOREY4_T3147307772_H
#ifndef U3CSTARTHTTPLISTENERU3EC__ANONSTOREY5_T808655612_H
#define U3CSTARTHTTPLISTENERU3EC__ANONSTOREY5_T808655612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.entitlement.OAuth2Identity/<StartHttpListener>c__AnonStorey5
struct  U3CStartHttpListenerU3Ec__AnonStorey5_t808655612  : public RuntimeObject
{
public:
	// System.String PolyToolkitInternal.entitlement.OAuth2Identity/<StartHttpListener>c__AnonStorey5::responseText
	String_t* ___responseText_0;
	// PolyToolkitInternal.entitlement.OAuth2Identity PolyToolkitInternal.entitlement.OAuth2Identity/<StartHttpListener>c__AnonStorey5::$this
	OAuth2Identity_t1039733419 * ___U24this_1;

public:
	inline static int32_t get_offset_of_responseText_0() { return static_cast<int32_t>(offsetof(U3CStartHttpListenerU3Ec__AnonStorey5_t808655612, ___responseText_0)); }
	inline String_t* get_responseText_0() const { return ___responseText_0; }
	inline String_t** get_address_of_responseText_0() { return &___responseText_0; }
	inline void set_responseText_0(String_t* value)
	{
		___responseText_0 = value;
		Il2CppCodeGenWriteBarrier((&___responseText_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartHttpListenerU3Ec__AnonStorey5_t808655612, ___U24this_1)); }
	inline OAuth2Identity_t1039733419 * get_U24this_1() const { return ___U24this_1; }
	inline OAuth2Identity_t1039733419 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(OAuth2Identity_t1039733419 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTHTTPLISTENERU3EC__ANONSTOREY5_T808655612_H
#ifndef U3CAUTHORIZEU3EC__ITERATOR2_T3062002209_H
#define U3CAUTHORIZEU3EC__ITERATOR2_T3062002209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.entitlement.OAuth2Identity/<Authorize>c__Iterator2
struct  U3CAuthorizeU3Ec__Iterator2_t3062002209  : public RuntimeObject
{
public:
	// System.Boolean PolyToolkitInternal.entitlement.OAuth2Identity/<Authorize>c__Iterator2::launchSignInFlowIfNeeded
	bool ___launchSignInFlowIfNeeded_0;
	// System.Action PolyToolkitInternal.entitlement.OAuth2Identity/<Authorize>c__Iterator2::onFailure
	Action_t1264377477 * ___onFailure_1;
	// System.Text.StringBuilder PolyToolkitInternal.entitlement.OAuth2Identity/<Authorize>c__Iterator2::<sb>__1
	StringBuilder_t * ___U3CsbU3E__1_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PolyToolkitInternal.entitlement.OAuth2Identity/<Authorize>c__Iterator2::<parameters>__1
	Dictionary_2_t1632706988 * ___U3CparametersU3E__1_3;
	// UnityEngine.Networking.UnityWebRequest PolyToolkitInternal.entitlement.OAuth2Identity/<Authorize>c__Iterator2::<www>__1
	UnityWebRequest_t463507806 * ___U3CwwwU3E__1_4;
	// Newtonsoft.Json.Linq.JObject PolyToolkitInternal.entitlement.OAuth2Identity/<Authorize>c__Iterator2::<json>__1
	JObject_t2059125928 * ___U3CjsonU3E__1_5;
	// System.Action PolyToolkitInternal.entitlement.OAuth2Identity/<Authorize>c__Iterator2::onSuccess
	Action_t1264377477 * ___onSuccess_6;
	// PolyToolkitInternal.entitlement.OAuth2Identity PolyToolkitInternal.entitlement.OAuth2Identity/<Authorize>c__Iterator2::$this
	OAuth2Identity_t1039733419 * ___U24this_7;
	// System.Object PolyToolkitInternal.entitlement.OAuth2Identity/<Authorize>c__Iterator2::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean PolyToolkitInternal.entitlement.OAuth2Identity/<Authorize>c__Iterator2::$disposing
	bool ___U24disposing_9;
	// System.Int32 PolyToolkitInternal.entitlement.OAuth2Identity/<Authorize>c__Iterator2::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_launchSignInFlowIfNeeded_0() { return static_cast<int32_t>(offsetof(U3CAuthorizeU3Ec__Iterator2_t3062002209, ___launchSignInFlowIfNeeded_0)); }
	inline bool get_launchSignInFlowIfNeeded_0() const { return ___launchSignInFlowIfNeeded_0; }
	inline bool* get_address_of_launchSignInFlowIfNeeded_0() { return &___launchSignInFlowIfNeeded_0; }
	inline void set_launchSignInFlowIfNeeded_0(bool value)
	{
		___launchSignInFlowIfNeeded_0 = value;
	}

	inline static int32_t get_offset_of_onFailure_1() { return static_cast<int32_t>(offsetof(U3CAuthorizeU3Ec__Iterator2_t3062002209, ___onFailure_1)); }
	inline Action_t1264377477 * get_onFailure_1() const { return ___onFailure_1; }
	inline Action_t1264377477 ** get_address_of_onFailure_1() { return &___onFailure_1; }
	inline void set_onFailure_1(Action_t1264377477 * value)
	{
		___onFailure_1 = value;
		Il2CppCodeGenWriteBarrier((&___onFailure_1), value);
	}

	inline static int32_t get_offset_of_U3CsbU3E__1_2() { return static_cast<int32_t>(offsetof(U3CAuthorizeU3Ec__Iterator2_t3062002209, ___U3CsbU3E__1_2)); }
	inline StringBuilder_t * get_U3CsbU3E__1_2() const { return ___U3CsbU3E__1_2; }
	inline StringBuilder_t ** get_address_of_U3CsbU3E__1_2() { return &___U3CsbU3E__1_2; }
	inline void set_U3CsbU3E__1_2(StringBuilder_t * value)
	{
		___U3CsbU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsbU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U3CparametersU3E__1_3() { return static_cast<int32_t>(offsetof(U3CAuthorizeU3Ec__Iterator2_t3062002209, ___U3CparametersU3E__1_3)); }
	inline Dictionary_2_t1632706988 * get_U3CparametersU3E__1_3() const { return ___U3CparametersU3E__1_3; }
	inline Dictionary_2_t1632706988 ** get_address_of_U3CparametersU3E__1_3() { return &___U3CparametersU3E__1_3; }
	inline void set_U3CparametersU3E__1_3(Dictionary_2_t1632706988 * value)
	{
		___U3CparametersU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CparametersU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__1_4() { return static_cast<int32_t>(offsetof(U3CAuthorizeU3Ec__Iterator2_t3062002209, ___U3CwwwU3E__1_4)); }
	inline UnityWebRequest_t463507806 * get_U3CwwwU3E__1_4() const { return ___U3CwwwU3E__1_4; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwwwU3E__1_4() { return &___U3CwwwU3E__1_4; }
	inline void set_U3CwwwU3E__1_4(UnityWebRequest_t463507806 * value)
	{
		___U3CwwwU3E__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__1_4), value);
	}

	inline static int32_t get_offset_of_U3CjsonU3E__1_5() { return static_cast<int32_t>(offsetof(U3CAuthorizeU3Ec__Iterator2_t3062002209, ___U3CjsonU3E__1_5)); }
	inline JObject_t2059125928 * get_U3CjsonU3E__1_5() const { return ___U3CjsonU3E__1_5; }
	inline JObject_t2059125928 ** get_address_of_U3CjsonU3E__1_5() { return &___U3CjsonU3E__1_5; }
	inline void set_U3CjsonU3E__1_5(JObject_t2059125928 * value)
	{
		___U3CjsonU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonU3E__1_5), value);
	}

	inline static int32_t get_offset_of_onSuccess_6() { return static_cast<int32_t>(offsetof(U3CAuthorizeU3Ec__Iterator2_t3062002209, ___onSuccess_6)); }
	inline Action_t1264377477 * get_onSuccess_6() const { return ___onSuccess_6; }
	inline Action_t1264377477 ** get_address_of_onSuccess_6() { return &___onSuccess_6; }
	inline void set_onSuccess_6(Action_t1264377477 * value)
	{
		___onSuccess_6 = value;
		Il2CppCodeGenWriteBarrier((&___onSuccess_6), value);
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CAuthorizeU3Ec__Iterator2_t3062002209, ___U24this_7)); }
	inline OAuth2Identity_t1039733419 * get_U24this_7() const { return ___U24this_7; }
	inline OAuth2Identity_t1039733419 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(OAuth2Identity_t1039733419 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CAuthorizeU3Ec__Iterator2_t3062002209, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CAuthorizeU3Ec__Iterator2_t3062002209, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CAuthorizeU3Ec__Iterator2_t3062002209, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAUTHORIZEU3EC__ITERATOR2_T3062002209_H
#ifndef AUTOSTRINGIFY_T3514337370_H
#define AUTOSTRINGIFY_T3514337370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.AutoStringify
struct  AutoStringify_t3514337370  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOSTRINGIFY_T3514337370_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef JSONCONVERTER_T1047106545_H
#define JSONCONVERTER_T1047106545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonConverter
struct  JsonConverter_t1047106545  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONVERTER_T1047106545_H
#ifndef POLYCLIENTUTILS_T2942259902_H
#define POLYCLIENTUTILS_T2942259902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.api_clients.poly_client.PolyClientUtils
struct  PolyClientUtils_t2942259902  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYCLIENTUTILS_T2942259902_H
#ifndef U3CGETASSETU3EC__ANONSTOREY1_T1596776758_H
#define U3CGETASSETU3EC__ANONSTOREY1_T1596776758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.api_clients.poly_client.PolyClient/<GetAsset>c__AnonStorey1
struct  U3CGetAssetU3Ec__AnonStorey1_t1596776758  : public RuntimeObject
{
public:
	// System.String PolyToolkitInternal.api_clients.poly_client.PolyClient/<GetAsset>c__AnonStorey1::assetId
	String_t* ___assetId_0;
	// System.Boolean PolyToolkitInternal.api_clients.poly_client.PolyClient/<GetAsset>c__AnonStorey1::isRecursion
	bool ___isRecursion_1;
	// System.Action`2<PolyToolkit.PolyStatus,PolyToolkit.PolyAsset> PolyToolkitInternal.api_clients.poly_client.PolyClient/<GetAsset>c__AnonStorey1::callback
	Action_2_t2738134841 * ___callback_2;
	// PolyToolkitInternal.api_clients.poly_client.PolyClient PolyToolkitInternal.api_clients.poly_client.PolyClient/<GetAsset>c__AnonStorey1::$this
	PolyClient_t2759672288 * ___U24this_3;

public:
	inline static int32_t get_offset_of_assetId_0() { return static_cast<int32_t>(offsetof(U3CGetAssetU3Ec__AnonStorey1_t1596776758, ___assetId_0)); }
	inline String_t* get_assetId_0() const { return ___assetId_0; }
	inline String_t** get_address_of_assetId_0() { return &___assetId_0; }
	inline void set_assetId_0(String_t* value)
	{
		___assetId_0 = value;
		Il2CppCodeGenWriteBarrier((&___assetId_0), value);
	}

	inline static int32_t get_offset_of_isRecursion_1() { return static_cast<int32_t>(offsetof(U3CGetAssetU3Ec__AnonStorey1_t1596776758, ___isRecursion_1)); }
	inline bool get_isRecursion_1() const { return ___isRecursion_1; }
	inline bool* get_address_of_isRecursion_1() { return &___isRecursion_1; }
	inline void set_isRecursion_1(bool value)
	{
		___isRecursion_1 = value;
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(U3CGetAssetU3Ec__AnonStorey1_t1596776758, ___callback_2)); }
	inline Action_2_t2738134841 * get_callback_2() const { return ___callback_2; }
	inline Action_2_t2738134841 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Action_2_t2738134841 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CGetAssetU3Ec__AnonStorey1_t1596776758, ___U24this_3)); }
	inline PolyClient_t2759672288 * get_U24this_3() const { return ___U24this_3; }
	inline PolyClient_t2759672288 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(PolyClient_t2759672288 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETASSETU3EC__ANONSTOREY1_T1596776758_H
#ifndef U3CSENDREQUESTU3EC__ANONSTOREY0_T684512095_H
#define U3CSENDREQUESTU3EC__ANONSTOREY0_T684512095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.api_clients.poly_client.PolyClient/<SendRequest>c__AnonStorey0
struct  U3CSendRequestU3Ec__AnonStorey0_t684512095  : public RuntimeObject
{
public:
	// PolyToolkit.PolyRequest PolyToolkitInternal.api_clients.poly_client.PolyClient/<SendRequest>c__AnonStorey0::request
	PolyRequest_t2374976743 * ___request_0;
	// System.Boolean PolyToolkitInternal.api_clients.poly_client.PolyClient/<SendRequest>c__AnonStorey0::isRecursion
	bool ___isRecursion_1;
	// System.Action`2<PolyToolkit.PolyStatus,PolyToolkit.PolyListAssetsResult> PolyToolkitInternal.api_clients.poly_client.PolyClient/<SendRequest>c__AnonStorey0::callback
	Action_2_t1974849383 * ___callback_2;
	// System.Int64 PolyToolkitInternal.api_clients.poly_client.PolyClient/<SendRequest>c__AnonStorey0::maxCacheAge
	int64_t ___maxCacheAge_3;
	// PolyToolkitInternal.api_clients.poly_client.PolyClient PolyToolkitInternal.api_clients.poly_client.PolyClient/<SendRequest>c__AnonStorey0::$this
	PolyClient_t2759672288 * ___U24this_4;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(U3CSendRequestU3Ec__AnonStorey0_t684512095, ___request_0)); }
	inline PolyRequest_t2374976743 * get_request_0() const { return ___request_0; }
	inline PolyRequest_t2374976743 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(PolyRequest_t2374976743 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}

	inline static int32_t get_offset_of_isRecursion_1() { return static_cast<int32_t>(offsetof(U3CSendRequestU3Ec__AnonStorey0_t684512095, ___isRecursion_1)); }
	inline bool get_isRecursion_1() const { return ___isRecursion_1; }
	inline bool* get_address_of_isRecursion_1() { return &___isRecursion_1; }
	inline void set_isRecursion_1(bool value)
	{
		___isRecursion_1 = value;
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(U3CSendRequestU3Ec__AnonStorey0_t684512095, ___callback_2)); }
	inline Action_2_t1974849383 * get_callback_2() const { return ___callback_2; }
	inline Action_2_t1974849383 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Action_2_t1974849383 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}

	inline static int32_t get_offset_of_maxCacheAge_3() { return static_cast<int32_t>(offsetof(U3CSendRequestU3Ec__AnonStorey0_t684512095, ___maxCacheAge_3)); }
	inline int64_t get_maxCacheAge_3() const { return ___maxCacheAge_3; }
	inline int64_t* get_address_of_maxCacheAge_3() { return &___maxCacheAge_3; }
	inline void set_maxCacheAge_3(int64_t value)
	{
		___maxCacheAge_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CSendRequestU3Ec__AnonStorey0_t684512095, ___U24this_4)); }
	inline PolyClient_t2759672288 * get_U24this_4() const { return ___U24this_4; }
	inline PolyClient_t2759672288 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(PolyClient_t2759672288 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDREQUESTU3EC__ANONSTOREY0_T684512095_H
#ifndef GLTFASSET_T1389371343_H
#define GLTFASSET_T1389371343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfAsset
struct  GltfAsset_t1389371343  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PolyToolkitInternal.GltfAsset::extensions
	Dictionary_2_t1632706988 * ___extensions_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PolyToolkitInternal.GltfAsset::extras
	Dictionary_2_t1632706988 * ___extras_1;
	// System.String PolyToolkitInternal.GltfAsset::copyright
	String_t* ___copyright_2;
	// System.String PolyToolkitInternal.GltfAsset::generator
	String_t* ___generator_3;
	// System.Boolean PolyToolkitInternal.GltfAsset::premultipliedAlpha
	bool ___premultipliedAlpha_4;
	// System.String PolyToolkitInternal.GltfAsset::version
	String_t* ___version_5;

public:
	inline static int32_t get_offset_of_extensions_0() { return static_cast<int32_t>(offsetof(GltfAsset_t1389371343, ___extensions_0)); }
	inline Dictionary_2_t1632706988 * get_extensions_0() const { return ___extensions_0; }
	inline Dictionary_2_t1632706988 ** get_address_of_extensions_0() { return &___extensions_0; }
	inline void set_extensions_0(Dictionary_2_t1632706988 * value)
	{
		___extensions_0 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_0), value);
	}

	inline static int32_t get_offset_of_extras_1() { return static_cast<int32_t>(offsetof(GltfAsset_t1389371343, ___extras_1)); }
	inline Dictionary_2_t1632706988 * get_extras_1() const { return ___extras_1; }
	inline Dictionary_2_t1632706988 ** get_address_of_extras_1() { return &___extras_1; }
	inline void set_extras_1(Dictionary_2_t1632706988 * value)
	{
		___extras_1 = value;
		Il2CppCodeGenWriteBarrier((&___extras_1), value);
	}

	inline static int32_t get_offset_of_copyright_2() { return static_cast<int32_t>(offsetof(GltfAsset_t1389371343, ___copyright_2)); }
	inline String_t* get_copyright_2() const { return ___copyright_2; }
	inline String_t** get_address_of_copyright_2() { return &___copyright_2; }
	inline void set_copyright_2(String_t* value)
	{
		___copyright_2 = value;
		Il2CppCodeGenWriteBarrier((&___copyright_2), value);
	}

	inline static int32_t get_offset_of_generator_3() { return static_cast<int32_t>(offsetof(GltfAsset_t1389371343, ___generator_3)); }
	inline String_t* get_generator_3() const { return ___generator_3; }
	inline String_t** get_address_of_generator_3() { return &___generator_3; }
	inline void set_generator_3(String_t* value)
	{
		___generator_3 = value;
		Il2CppCodeGenWriteBarrier((&___generator_3), value);
	}

	inline static int32_t get_offset_of_premultipliedAlpha_4() { return static_cast<int32_t>(offsetof(GltfAsset_t1389371343, ___premultipliedAlpha_4)); }
	inline bool get_premultipliedAlpha_4() const { return ___premultipliedAlpha_4; }
	inline bool* get_address_of_premultipliedAlpha_4() { return &___premultipliedAlpha_4; }
	inline void set_premultipliedAlpha_4(bool value)
	{
		___premultipliedAlpha_4 = value;
	}

	inline static int32_t get_offset_of_version_5() { return static_cast<int32_t>(offsetof(GltfAsset_t1389371343, ___version_5)); }
	inline String_t* get_version_5() const { return ___version_5; }
	inline String_t** get_address_of_version_5() { return &___version_5; }
	inline void set_version_5(String_t* value)
	{
		___version_5 = value;
		Il2CppCodeGenWriteBarrier((&___version_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFASSET_T1389371343_H
#ifndef GLTFMATERIALCONVERTER_T1532388150_H
#define GLTFMATERIALCONVERTER_T1532388150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfMaterialConverter
struct  GltfMaterialConverter_t1532388150  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Material> PolyToolkitInternal.GltfMaterialConverter::newMaterials
	List_1_t1812449865 * ___newMaterials_2;
	// System.Collections.Generic.Dictionary`2<PolyToolkitInternal.GltfMaterialBase,PolyToolkitInternal.GltfMaterialConverter/UnityMaterial> PolyToolkitInternal.GltfMaterialConverter::materials
	Dictionary_2_t1359220952 * ___materials_3;

public:
	inline static int32_t get_offset_of_newMaterials_2() { return static_cast<int32_t>(offsetof(GltfMaterialConverter_t1532388150, ___newMaterials_2)); }
	inline List_1_t1812449865 * get_newMaterials_2() const { return ___newMaterials_2; }
	inline List_1_t1812449865 ** get_address_of_newMaterials_2() { return &___newMaterials_2; }
	inline void set_newMaterials_2(List_1_t1812449865 * value)
	{
		___newMaterials_2 = value;
		Il2CppCodeGenWriteBarrier((&___newMaterials_2), value);
	}

	inline static int32_t get_offset_of_materials_3() { return static_cast<int32_t>(offsetof(GltfMaterialConverter_t1532388150, ___materials_3)); }
	inline Dictionary_2_t1359220952 * get_materials_3() const { return ___materials_3; }
	inline Dictionary_2_t1359220952 ** get_address_of_materials_3() { return &___materials_3; }
	inline void set_materials_3(Dictionary_2_t1359220952 * value)
	{
		___materials_3 = value;
		Il2CppCodeGenWriteBarrier((&___materials_3), value);
	}
};

struct GltfMaterialConverter_t1532388150_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex PolyToolkitInternal.GltfMaterialConverter::kTiltBrushMaterialRegex
	Regex_t3657309853 * ___kTiltBrushMaterialRegex_0;
	// System.Text.RegularExpressions.Regex PolyToolkitInternal.GltfMaterialConverter::kTiltBrushShaderRegex
	Regex_t3657309853 * ___kTiltBrushShaderRegex_1;

public:
	inline static int32_t get_offset_of_kTiltBrushMaterialRegex_0() { return static_cast<int32_t>(offsetof(GltfMaterialConverter_t1532388150_StaticFields, ___kTiltBrushMaterialRegex_0)); }
	inline Regex_t3657309853 * get_kTiltBrushMaterialRegex_0() const { return ___kTiltBrushMaterialRegex_0; }
	inline Regex_t3657309853 ** get_address_of_kTiltBrushMaterialRegex_0() { return &___kTiltBrushMaterialRegex_0; }
	inline void set_kTiltBrushMaterialRegex_0(Regex_t3657309853 * value)
	{
		___kTiltBrushMaterialRegex_0 = value;
		Il2CppCodeGenWriteBarrier((&___kTiltBrushMaterialRegex_0), value);
	}

	inline static int32_t get_offset_of_kTiltBrushShaderRegex_1() { return static_cast<int32_t>(offsetof(GltfMaterialConverter_t1532388150_StaticFields, ___kTiltBrushShaderRegex_1)); }
	inline Regex_t3657309853 * get_kTiltBrushShaderRegex_1() const { return ___kTiltBrushShaderRegex_1; }
	inline Regex_t3657309853 ** get_address_of_kTiltBrushShaderRegex_1() { return &___kTiltBrushShaderRegex_1; }
	inline void set_kTiltBrushShaderRegex_1(Regex_t3657309853 * value)
	{
		___kTiltBrushShaderRegex_1 = value;
		Il2CppCodeGenWriteBarrier((&___kTiltBrushShaderRegex_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFMATERIALCONVERTER_T1532388150_H
#ifndef U3CNECESSARYTEXTURESU3EC__ITERATOR0_T3036043387_H
#define U3CNECESSARYTEXTURESU3EC__ITERATOR0_T3036043387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfMaterialConverter/<NecessaryTextures>c__Iterator0
struct  U3CNecessaryTexturesU3Ec__Iterator0_t3036043387  : public RuntimeObject
{
public:
	// PolyToolkitInternal.GltfRootBase PolyToolkitInternal.GltfMaterialConverter/<NecessaryTextures>c__Iterator0::root
	GltfRootBase_t3065879770 * ___root_0;
	// System.Collections.Generic.IEnumerator`1<PolyToolkitInternal.GltfMaterialBase> PolyToolkitInternal.GltfMaterialConverter/<NecessaryTextures>c__Iterator0::$locvar0
	RuntimeObject* ___U24locvar0_1;
	// PolyToolkitInternal.GltfMaterialBase PolyToolkitInternal.GltfMaterialConverter/<NecessaryTextures>c__Iterator0::<mat>__1
	GltfMaterialBase_t3607200052 * ___U3CmatU3E__1_2;
	// System.Collections.Generic.IEnumerator`1<PolyToolkitInternal.GltfTextureBase> PolyToolkitInternal.GltfMaterialConverter/<NecessaryTextures>c__Iterator0::$locvar1
	RuntimeObject* ___U24locvar1_3;
	// PolyToolkitInternal.GltfTextureBase PolyToolkitInternal.GltfMaterialConverter/<NecessaryTextures>c__Iterator0::<tex>__2
	GltfTextureBase_t3199118737 * ___U3CtexU3E__2_4;
	// PolyToolkitInternal.GltfTextureBase PolyToolkitInternal.GltfMaterialConverter/<NecessaryTextures>c__Iterator0::$current
	GltfTextureBase_t3199118737 * ___U24current_5;
	// System.Boolean PolyToolkitInternal.GltfMaterialConverter/<NecessaryTextures>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 PolyToolkitInternal.GltfMaterialConverter/<NecessaryTextures>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(U3CNecessaryTexturesU3Ec__Iterator0_t3036043387, ___root_0)); }
	inline GltfRootBase_t3065879770 * get_root_0() const { return ___root_0; }
	inline GltfRootBase_t3065879770 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(GltfRootBase_t3065879770 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CNecessaryTexturesU3Ec__Iterator0_t3036043387, ___U24locvar0_1)); }
	inline RuntimeObject* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline RuntimeObject** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(RuntimeObject* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U3CmatU3E__1_2() { return static_cast<int32_t>(offsetof(U3CNecessaryTexturesU3Ec__Iterator0_t3036043387, ___U3CmatU3E__1_2)); }
	inline GltfMaterialBase_t3607200052 * get_U3CmatU3E__1_2() const { return ___U3CmatU3E__1_2; }
	inline GltfMaterialBase_t3607200052 ** get_address_of_U3CmatU3E__1_2() { return &___U3CmatU3E__1_2; }
	inline void set_U3CmatU3E__1_2(GltfMaterialBase_t3607200052 * value)
	{
		___U3CmatU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmatU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U24locvar1_3() { return static_cast<int32_t>(offsetof(U3CNecessaryTexturesU3Ec__Iterator0_t3036043387, ___U24locvar1_3)); }
	inline RuntimeObject* get_U24locvar1_3() const { return ___U24locvar1_3; }
	inline RuntimeObject** get_address_of_U24locvar1_3() { return &___U24locvar1_3; }
	inline void set_U24locvar1_3(RuntimeObject* value)
	{
		___U24locvar1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar1_3), value);
	}

	inline static int32_t get_offset_of_U3CtexU3E__2_4() { return static_cast<int32_t>(offsetof(U3CNecessaryTexturesU3Ec__Iterator0_t3036043387, ___U3CtexU3E__2_4)); }
	inline GltfTextureBase_t3199118737 * get_U3CtexU3E__2_4() const { return ___U3CtexU3E__2_4; }
	inline GltfTextureBase_t3199118737 ** get_address_of_U3CtexU3E__2_4() { return &___U3CtexU3E__2_4; }
	inline void set_U3CtexU3E__2_4(GltfTextureBase_t3199118737 * value)
	{
		___U3CtexU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtexU3E__2_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CNecessaryTexturesU3Ec__Iterator0_t3036043387, ___U24current_5)); }
	inline GltfTextureBase_t3199118737 * get_U24current_5() const { return ___U24current_5; }
	inline GltfTextureBase_t3199118737 ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(GltfTextureBase_t3199118737 * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CNecessaryTexturesU3Ec__Iterator0_t3036043387, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CNecessaryTexturesU3Ec__Iterator0_t3036043387, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CNECESSARYTEXTURESU3EC__ITERATOR0_T3036043387_H
#ifndef U3CLOADTEXTURESCOROUTINEU3EC__ITERATOR1_T514846858_H
#define U3CLOADTEXTURESCOROUTINEU3EC__ITERATOR1_T514846858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfMaterialConverter/<LoadTexturesCoroutine>c__Iterator1
struct  U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858  : public RuntimeObject
{
public:
	// PolyToolkitInternal.GltfRootBase PolyToolkitInternal.GltfMaterialConverter/<LoadTexturesCoroutine>c__Iterator1::root
	GltfRootBase_t3065879770 * ___root_0;
	// System.Collections.Generic.IEnumerator`1<PolyToolkitInternal.GltfTextureBase> PolyToolkitInternal.GltfMaterialConverter/<LoadTexturesCoroutine>c__Iterator1::$locvar0
	RuntimeObject* ___U24locvar0_1;
	// PolyToolkitInternal.GltfTextureBase PolyToolkitInternal.GltfMaterialConverter/<LoadTexturesCoroutine>c__Iterator1::<gltfTexture>__1
	GltfTextureBase_t3199118737 * ___U3CgltfTextureU3E__1_2;
	// PolyToolkitInternal.IUriLoader PolyToolkitInternal.GltfMaterialConverter/<LoadTexturesCoroutine>c__Iterator1::loader
	RuntimeObject* ___loader_3;
	// System.Collections.IEnumerator PolyToolkitInternal.GltfMaterialConverter/<LoadTexturesCoroutine>c__Iterator1::$locvar1
	RuntimeObject* ___U24locvar1_4;
	// System.Object PolyToolkitInternal.GltfMaterialConverter/<LoadTexturesCoroutine>c__Iterator1::<unused>__2
	RuntimeObject * ___U3CunusedU3E__2_5;
	// System.IDisposable PolyToolkitInternal.GltfMaterialConverter/<LoadTexturesCoroutine>c__Iterator1::$locvar2
	RuntimeObject* ___U24locvar2_6;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> PolyToolkitInternal.GltfMaterialConverter/<LoadTexturesCoroutine>c__Iterator1::loaded
	List_1_t1017553631 * ___loaded_7;
	// System.Collections.Generic.IEnumerator`1<PolyToolkitInternal.GltfImageBase> PolyToolkitInternal.GltfMaterialConverter/<LoadTexturesCoroutine>c__Iterator1::$locvar3
	RuntimeObject* ___U24locvar3_8;
	// System.Object PolyToolkitInternal.GltfMaterialConverter/<LoadTexturesCoroutine>c__Iterator1::$current
	RuntimeObject * ___U24current_9;
	// System.Boolean PolyToolkitInternal.GltfMaterialConverter/<LoadTexturesCoroutine>c__Iterator1::$disposing
	bool ___U24disposing_10;
	// System.Int32 PolyToolkitInternal.GltfMaterialConverter/<LoadTexturesCoroutine>c__Iterator1::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858, ___root_0)); }
	inline GltfRootBase_t3065879770 * get_root_0() const { return ___root_0; }
	inline GltfRootBase_t3065879770 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(GltfRootBase_t3065879770 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858, ___U24locvar0_1)); }
	inline RuntimeObject* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline RuntimeObject** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(RuntimeObject* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U3CgltfTextureU3E__1_2() { return static_cast<int32_t>(offsetof(U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858, ___U3CgltfTextureU3E__1_2)); }
	inline GltfTextureBase_t3199118737 * get_U3CgltfTextureU3E__1_2() const { return ___U3CgltfTextureU3E__1_2; }
	inline GltfTextureBase_t3199118737 ** get_address_of_U3CgltfTextureU3E__1_2() { return &___U3CgltfTextureU3E__1_2; }
	inline void set_U3CgltfTextureU3E__1_2(GltfTextureBase_t3199118737 * value)
	{
		___U3CgltfTextureU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgltfTextureU3E__1_2), value);
	}

	inline static int32_t get_offset_of_loader_3() { return static_cast<int32_t>(offsetof(U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858, ___loader_3)); }
	inline RuntimeObject* get_loader_3() const { return ___loader_3; }
	inline RuntimeObject** get_address_of_loader_3() { return &___loader_3; }
	inline void set_loader_3(RuntimeObject* value)
	{
		___loader_3 = value;
		Il2CppCodeGenWriteBarrier((&___loader_3), value);
	}

	inline static int32_t get_offset_of_U24locvar1_4() { return static_cast<int32_t>(offsetof(U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858, ___U24locvar1_4)); }
	inline RuntimeObject* get_U24locvar1_4() const { return ___U24locvar1_4; }
	inline RuntimeObject** get_address_of_U24locvar1_4() { return &___U24locvar1_4; }
	inline void set_U24locvar1_4(RuntimeObject* value)
	{
		___U24locvar1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar1_4), value);
	}

	inline static int32_t get_offset_of_U3CunusedU3E__2_5() { return static_cast<int32_t>(offsetof(U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858, ___U3CunusedU3E__2_5)); }
	inline RuntimeObject * get_U3CunusedU3E__2_5() const { return ___U3CunusedU3E__2_5; }
	inline RuntimeObject ** get_address_of_U3CunusedU3E__2_5() { return &___U3CunusedU3E__2_5; }
	inline void set_U3CunusedU3E__2_5(RuntimeObject * value)
	{
		___U3CunusedU3E__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CunusedU3E__2_5), value);
	}

	inline static int32_t get_offset_of_U24locvar2_6() { return static_cast<int32_t>(offsetof(U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858, ___U24locvar2_6)); }
	inline RuntimeObject* get_U24locvar2_6() const { return ___U24locvar2_6; }
	inline RuntimeObject** get_address_of_U24locvar2_6() { return &___U24locvar2_6; }
	inline void set_U24locvar2_6(RuntimeObject* value)
	{
		___U24locvar2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar2_6), value);
	}

	inline static int32_t get_offset_of_loaded_7() { return static_cast<int32_t>(offsetof(U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858, ___loaded_7)); }
	inline List_1_t1017553631 * get_loaded_7() const { return ___loaded_7; }
	inline List_1_t1017553631 ** get_address_of_loaded_7() { return &___loaded_7; }
	inline void set_loaded_7(List_1_t1017553631 * value)
	{
		___loaded_7 = value;
		Il2CppCodeGenWriteBarrier((&___loaded_7), value);
	}

	inline static int32_t get_offset_of_U24locvar3_8() { return static_cast<int32_t>(offsetof(U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858, ___U24locvar3_8)); }
	inline RuntimeObject* get_U24locvar3_8() const { return ___U24locvar3_8; }
	inline RuntimeObject** get_address_of_U24locvar3_8() { return &___U24locvar3_8; }
	inline void set_U24locvar3_8(RuntimeObject* value)
	{
		___U24locvar3_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar3_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADTEXTURESCOROUTINEU3EC__ITERATOR1_T514846858_H
#ifndef U3CWAITFRAMEU3EC__ITERATOR0_T2962725221_H
#define U3CWAITFRAMEU3EC__ITERATOR0_T2962725221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_ShurikenThreadFix/<WaitFrame>c__Iterator0
struct  U3CWaitFrameU3Ec__Iterator0_t2962725221  : public RuntimeObject
{
public:
	// UnityEngine.ParticleSystem[] CFX_ShurikenThreadFix/<WaitFrame>c__Iterator0::$locvar0
	ParticleSystemU5BU5D_t3089334924* ___U24locvar0_0;
	// System.Int32 CFX_ShurikenThreadFix/<WaitFrame>c__Iterator0::$locvar1
	int32_t ___U24locvar1_1;
	// CFX_ShurikenThreadFix CFX_ShurikenThreadFix/<WaitFrame>c__Iterator0::$this
	CFX_ShurikenThreadFix_t3300879551 * ___U24this_2;
	// System.Object CFX_ShurikenThreadFix/<WaitFrame>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean CFX_ShurikenThreadFix/<WaitFrame>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 CFX_ShurikenThreadFix/<WaitFrame>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CWaitFrameU3Ec__Iterator0_t2962725221, ___U24locvar0_0)); }
	inline ParticleSystemU5BU5D_t3089334924* get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline ParticleSystemU5BU5D_t3089334924** get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(ParticleSystemU5BU5D_t3089334924* value)
	{
		___U24locvar0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar1_1() { return static_cast<int32_t>(offsetof(U3CWaitFrameU3Ec__Iterator0_t2962725221, ___U24locvar1_1)); }
	inline int32_t get_U24locvar1_1() const { return ___U24locvar1_1; }
	inline int32_t* get_address_of_U24locvar1_1() { return &___U24locvar1_1; }
	inline void set_U24locvar1_1(int32_t value)
	{
		___U24locvar1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CWaitFrameU3Ec__Iterator0_t2962725221, ___U24this_2)); }
	inline CFX_ShurikenThreadFix_t3300879551 * get_U24this_2() const { return ___U24this_2; }
	inline CFX_ShurikenThreadFix_t3300879551 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(CFX_ShurikenThreadFix_t3300879551 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CWaitFrameU3Ec__Iterator0_t2962725221, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CWaitFrameU3Ec__Iterator0_t2962725221, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CWaitFrameU3Ec__Iterator0_t2962725221, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFRAMEU3EC__ITERATOR0_T2962725221_H
#ifndef U3CCONVERTTEXTURECOROUTINEU3EC__ITERATOR2_T3482638318_H
#define U3CCONVERTTEXTURECOROUTINEU3EC__ITERATOR2_T3482638318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfMaterialConverter/<ConvertTextureCoroutine>c__Iterator2
struct  U3CConvertTextureCoroutineU3Ec__Iterator2_t3482638318  : public RuntimeObject
{
public:
	// PolyToolkitInternal.GltfTextureBase PolyToolkitInternal.GltfMaterialConverter/<ConvertTextureCoroutine>c__Iterator2::gltfTexture
	GltfTextureBase_t3199118737 * ___gltfTexture_0;
	// PolyToolkitInternal.RawImage PolyToolkitInternal.GltfMaterialConverter/<ConvertTextureCoroutine>c__Iterator2::<data>__1
	RawImage_t3303103980 * ___U3CdataU3E__1_1;
	// UnityEngine.Texture2D PolyToolkitInternal.GltfMaterialConverter/<ConvertTextureCoroutine>c__Iterator2::<tex>__1
	Texture2D_t3840446185 * ___U3CtexU3E__1_2;
	// PolyToolkitInternal.IUriLoader PolyToolkitInternal.GltfMaterialConverter/<ConvertTextureCoroutine>c__Iterator2::loader
	RuntimeObject* ___loader_3;
	// System.Byte[] PolyToolkitInternal.GltfMaterialConverter/<ConvertTextureCoroutine>c__Iterator2::<textureBytes>__2
	ByteU5BU5D_t4116647657* ___U3CtextureBytesU3E__2_4;
	// System.Object PolyToolkitInternal.GltfMaterialConverter/<ConvertTextureCoroutine>c__Iterator2::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean PolyToolkitInternal.GltfMaterialConverter/<ConvertTextureCoroutine>c__Iterator2::$disposing
	bool ___U24disposing_6;
	// System.Int32 PolyToolkitInternal.GltfMaterialConverter/<ConvertTextureCoroutine>c__Iterator2::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_gltfTexture_0() { return static_cast<int32_t>(offsetof(U3CConvertTextureCoroutineU3Ec__Iterator2_t3482638318, ___gltfTexture_0)); }
	inline GltfTextureBase_t3199118737 * get_gltfTexture_0() const { return ___gltfTexture_0; }
	inline GltfTextureBase_t3199118737 ** get_address_of_gltfTexture_0() { return &___gltfTexture_0; }
	inline void set_gltfTexture_0(GltfTextureBase_t3199118737 * value)
	{
		___gltfTexture_0 = value;
		Il2CppCodeGenWriteBarrier((&___gltfTexture_0), value);
	}

	inline static int32_t get_offset_of_U3CdataU3E__1_1() { return static_cast<int32_t>(offsetof(U3CConvertTextureCoroutineU3Ec__Iterator2_t3482638318, ___U3CdataU3E__1_1)); }
	inline RawImage_t3303103980 * get_U3CdataU3E__1_1() const { return ___U3CdataU3E__1_1; }
	inline RawImage_t3303103980 ** get_address_of_U3CdataU3E__1_1() { return &___U3CdataU3E__1_1; }
	inline void set_U3CdataU3E__1_1(RawImage_t3303103980 * value)
	{
		___U3CdataU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdataU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U3CtexU3E__1_2() { return static_cast<int32_t>(offsetof(U3CConvertTextureCoroutineU3Ec__Iterator2_t3482638318, ___U3CtexU3E__1_2)); }
	inline Texture2D_t3840446185 * get_U3CtexU3E__1_2() const { return ___U3CtexU3E__1_2; }
	inline Texture2D_t3840446185 ** get_address_of_U3CtexU3E__1_2() { return &___U3CtexU3E__1_2; }
	inline void set_U3CtexU3E__1_2(Texture2D_t3840446185 * value)
	{
		___U3CtexU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtexU3E__1_2), value);
	}

	inline static int32_t get_offset_of_loader_3() { return static_cast<int32_t>(offsetof(U3CConvertTextureCoroutineU3Ec__Iterator2_t3482638318, ___loader_3)); }
	inline RuntimeObject* get_loader_3() const { return ___loader_3; }
	inline RuntimeObject** get_address_of_loader_3() { return &___loader_3; }
	inline void set_loader_3(RuntimeObject* value)
	{
		___loader_3 = value;
		Il2CppCodeGenWriteBarrier((&___loader_3), value);
	}

	inline static int32_t get_offset_of_U3CtextureBytesU3E__2_4() { return static_cast<int32_t>(offsetof(U3CConvertTextureCoroutineU3Ec__Iterator2_t3482638318, ___U3CtextureBytesU3E__2_4)); }
	inline ByteU5BU5D_t4116647657* get_U3CtextureBytesU3E__2_4() const { return ___U3CtextureBytesU3E__2_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CtextureBytesU3E__2_4() { return &___U3CtextureBytesU3E__2_4; }
	inline void set_U3CtextureBytesU3E__2_4(ByteU5BU5D_t4116647657* value)
	{
		___U3CtextureBytesU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextureBytesU3E__2_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CConvertTextureCoroutineU3Ec__Iterator2_t3482638318, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CConvertTextureCoroutineU3Ec__Iterator2_t3482638318, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CConvertTextureCoroutineU3Ec__Iterator2_t3482638318, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONVERTTEXTURECOROUTINEU3EC__ITERATOR2_T3482638318_H
#ifndef U3CGETRAWFILETEXTU3EC__ANONSTOREY0_T1419660627_H
#define U3CGETRAWFILETEXTU3EC__ANONSTOREY0_T1419660627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.api_clients.poly_client.PolyClientUtils/<GetRawFileText>c__AnonStorey0
struct  U3CGetRawFileTextU3Ec__AnonStorey0_t1419660627  : public RuntimeObject
{
public:
	// System.String PolyToolkitInternal.api_clients.poly_client.PolyClientUtils/<GetRawFileText>c__AnonStorey0::dataUrl
	String_t* ___dataUrl_0;
	// System.String PolyToolkitInternal.api_clients.poly_client.PolyClientUtils/<GetRawFileText>c__AnonStorey0::accessToken
	String_t* ___accessToken_1;
	// PolyToolkitInternal.api_clients.poly_client.PolyClientUtils/GetRawFileDataTextCallback PolyToolkitInternal.api_clients.poly_client.PolyClientUtils/<GetRawFileText>c__AnonStorey0::callback
	GetRawFileDataTextCallback_t2904487790 * ___callback_2;

public:
	inline static int32_t get_offset_of_dataUrl_0() { return static_cast<int32_t>(offsetof(U3CGetRawFileTextU3Ec__AnonStorey0_t1419660627, ___dataUrl_0)); }
	inline String_t* get_dataUrl_0() const { return ___dataUrl_0; }
	inline String_t** get_address_of_dataUrl_0() { return &___dataUrl_0; }
	inline void set_dataUrl_0(String_t* value)
	{
		___dataUrl_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataUrl_0), value);
	}

	inline static int32_t get_offset_of_accessToken_1() { return static_cast<int32_t>(offsetof(U3CGetRawFileTextU3Ec__AnonStorey0_t1419660627, ___accessToken_1)); }
	inline String_t* get_accessToken_1() const { return ___accessToken_1; }
	inline String_t** get_address_of_accessToken_1() { return &___accessToken_1; }
	inline void set_accessToken_1(String_t* value)
	{
		___accessToken_1 = value;
		Il2CppCodeGenWriteBarrier((&___accessToken_1), value);
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(U3CGetRawFileTextU3Ec__AnonStorey0_t1419660627, ___callback_2)); }
	inline GetRawFileDataTextCallback_t2904487790 * get_callback_2() const { return ___callback_2; }
	inline GetRawFileDataTextCallback_t2904487790 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(GetRawFileDataTextCallback_t2904487790 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETRAWFILETEXTU3EC__ANONSTOREY0_T1419660627_H
#ifndef U3CREAUTHORIZEU3EC__ANONSTOREY2_T535307564_H
#define U3CREAUTHORIZEU3EC__ANONSTOREY2_T535307564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Authenticator/<Reauthorize>c__AnonStorey2
struct  U3CReauthorizeU3Ec__AnonStorey2_t535307564  : public RuntimeObject
{
public:
	// System.Action`1<PolyToolkit.PolyStatus> PolyToolkitInternal.Authenticator/<Reauthorize>c__AnonStorey2::callback
	Action_1_t3317841535 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CReauthorizeU3Ec__AnonStorey2_t535307564, ___callback_0)); }
	inline Action_1_t3317841535 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t3317841535 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t3317841535 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREAUTHORIZEU3EC__ANONSTOREY2_T535307564_H
#ifndef U3CAUTHENTICATEU3EC__ANONSTOREY1_T1316809896_H
#define U3CAUTHENTICATEU3EC__ANONSTOREY1_T1316809896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Authenticator/<Authenticate>c__AnonStorey1
struct  U3CAuthenticateU3Ec__AnonStorey1_t1316809896  : public RuntimeObject
{
public:
	// System.Action`1<PolyToolkit.PolyStatus> PolyToolkitInternal.Authenticator/<Authenticate>c__AnonStorey1::callback
	Action_1_t3317841535 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CAuthenticateU3Ec__AnonStorey1_t1316809896, ___callback_0)); }
	inline Action_1_t3317841535 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t3317841535 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t3317841535 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAUTHENTICATEU3EC__ANONSTOREY1_T1316809896_H
#ifndef U3CAUTHENTICATEU3EC__ANONSTOREY0_T1316744360_H
#define U3CAUTHENTICATEU3EC__ANONSTOREY0_T1316744360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Authenticator/<Authenticate>c__AnonStorey0
struct  U3CAuthenticateU3Ec__AnonStorey0_t1316744360  : public RuntimeObject
{
public:
	// System.Action`1<PolyToolkit.PolyStatus> PolyToolkitInternal.Authenticator/<Authenticate>c__AnonStorey0::callback
	Action_1_t3317841535 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CAuthenticateU3Ec__AnonStorey0_t1316744360, ___callback_0)); }
	inline Action_1_t3317841535 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t3317841535 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t3317841535 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAUTHENTICATEU3EC__ANONSTOREY0_T1316744360_H
#ifndef ATTRIBUTIONGENERATION_T2717336680_H
#define ATTRIBUTIONGENERATION_T2717336680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.AttributionGeneration
struct  AttributionGeneration_t2717336680  : public RuntimeObject
{
public:

public:
};

struct AttributionGeneration_t2717336680_StaticFields
{
public:
	// System.String PolyToolkitInternal.AttributionGeneration::ATTRIB_FILE_NAME
	String_t* ___ATTRIB_FILE_NAME_0;
	// System.String PolyToolkitInternal.AttributionGeneration::FILE_HEADER
	String_t* ___FILE_HEADER_1;
	// System.String PolyToolkitInternal.AttributionGeneration::CC_BY_LICENSE
	String_t* ___CC_BY_LICENSE_2;

public:
	inline static int32_t get_offset_of_ATTRIB_FILE_NAME_0() { return static_cast<int32_t>(offsetof(AttributionGeneration_t2717336680_StaticFields, ___ATTRIB_FILE_NAME_0)); }
	inline String_t* get_ATTRIB_FILE_NAME_0() const { return ___ATTRIB_FILE_NAME_0; }
	inline String_t** get_address_of_ATTRIB_FILE_NAME_0() { return &___ATTRIB_FILE_NAME_0; }
	inline void set_ATTRIB_FILE_NAME_0(String_t* value)
	{
		___ATTRIB_FILE_NAME_0 = value;
		Il2CppCodeGenWriteBarrier((&___ATTRIB_FILE_NAME_0), value);
	}

	inline static int32_t get_offset_of_FILE_HEADER_1() { return static_cast<int32_t>(offsetof(AttributionGeneration_t2717336680_StaticFields, ___FILE_HEADER_1)); }
	inline String_t* get_FILE_HEADER_1() const { return ___FILE_HEADER_1; }
	inline String_t** get_address_of_FILE_HEADER_1() { return &___FILE_HEADER_1; }
	inline void set_FILE_HEADER_1(String_t* value)
	{
		___FILE_HEADER_1 = value;
		Il2CppCodeGenWriteBarrier((&___FILE_HEADER_1), value);
	}

	inline static int32_t get_offset_of_CC_BY_LICENSE_2() { return static_cast<int32_t>(offsetof(AttributionGeneration_t2717336680_StaticFields, ___CC_BY_LICENSE_2)); }
	inline String_t* get_CC_BY_LICENSE_2() const { return ___CC_BY_LICENSE_2; }
	inline String_t** get_address_of_CC_BY_LICENSE_2() { return &___CC_BY_LICENSE_2; }
	inline void set_CC_BY_LICENSE_2(String_t* value)
	{
		___CC_BY_LICENSE_2 = value;
		Il2CppCodeGenWriteBarrier((&___CC_BY_LICENSE_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTIONGENERATION_T2717336680_H
#ifndef U3CGETRAWFILEBYTESU3EC__ANONSTOREY1_T1499323914_H
#define U3CGETRAWFILEBYTESU3EC__ANONSTOREY1_T1499323914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.api_clients.poly_client.PolyClientUtils/<GetRawFileBytes>c__AnonStorey1
struct  U3CGetRawFileBytesU3Ec__AnonStorey1_t1499323914  : public RuntimeObject
{
public:
	// System.String PolyToolkitInternal.api_clients.poly_client.PolyClientUtils/<GetRawFileBytes>c__AnonStorey1::dataUrl
	String_t* ___dataUrl_0;
	// System.String PolyToolkitInternal.api_clients.poly_client.PolyClientUtils/<GetRawFileBytes>c__AnonStorey1::accessToken
	String_t* ___accessToken_1;
	// PolyToolkitInternal.api_clients.poly_client.PolyClientUtils/GetRawFileDataBytesCallback PolyToolkitInternal.api_clients.poly_client.PolyClientUtils/<GetRawFileBytes>c__AnonStorey1::callback
	GetRawFileDataBytesCallback_t2880187408 * ___callback_2;

public:
	inline static int32_t get_offset_of_dataUrl_0() { return static_cast<int32_t>(offsetof(U3CGetRawFileBytesU3Ec__AnonStorey1_t1499323914, ___dataUrl_0)); }
	inline String_t* get_dataUrl_0() const { return ___dataUrl_0; }
	inline String_t** get_address_of_dataUrl_0() { return &___dataUrl_0; }
	inline void set_dataUrl_0(String_t* value)
	{
		___dataUrl_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataUrl_0), value);
	}

	inline static int32_t get_offset_of_accessToken_1() { return static_cast<int32_t>(offsetof(U3CGetRawFileBytesU3Ec__AnonStorey1_t1499323914, ___accessToken_1)); }
	inline String_t* get_accessToken_1() const { return ___accessToken_1; }
	inline String_t** get_address_of_accessToken_1() { return &___accessToken_1; }
	inline void set_accessToken_1(String_t* value)
	{
		___accessToken_1 = value;
		Il2CppCodeGenWriteBarrier((&___accessToken_1), value);
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(U3CGetRawFileBytesU3Ec__AnonStorey1_t1499323914, ___callback_2)); }
	inline GetRawFileDataBytesCallback_t2880187408 * get_callback_2() const { return ___callback_2; }
	inline GetRawFileDataBytesCallback_t2880187408 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(GetRawFileDataBytesCallback_t2880187408 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETRAWFILEBYTESU3EC__ANONSTOREY1_T1499323914_H
#ifndef GLTFBUFFERBASE_T2874699436_H
#define GLTFBUFFERBASE_T2874699436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfBufferBase
struct  GltfBufferBase_t2874699436  : public RuntimeObject
{
public:
	// System.Int64 PolyToolkitInternal.GltfBufferBase::byteLength
	int64_t ___byteLength_0;
	// System.String PolyToolkitInternal.GltfBufferBase::uri
	String_t* ___uri_1;
	// PolyToolkitInternal.IBufferReader PolyToolkitInternal.GltfBufferBase::data
	RuntimeObject* ___data_2;

public:
	inline static int32_t get_offset_of_byteLength_0() { return static_cast<int32_t>(offsetof(GltfBufferBase_t2874699436, ___byteLength_0)); }
	inline int64_t get_byteLength_0() const { return ___byteLength_0; }
	inline int64_t* get_address_of_byteLength_0() { return &___byteLength_0; }
	inline void set_byteLength_0(int64_t value)
	{
		___byteLength_0 = value;
	}

	inline static int32_t get_offset_of_uri_1() { return static_cast<int32_t>(offsetof(GltfBufferBase_t2874699436, ___uri_1)); }
	inline String_t* get_uri_1() const { return ___uri_1; }
	inline String_t** get_address_of_uri_1() { return &___uri_1; }
	inline void set_uri_1(String_t* value)
	{
		___uri_1 = value;
		Il2CppCodeGenWriteBarrier((&___uri_1), value);
	}

	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(GltfBufferBase_t2874699436, ___data_2)); }
	inline RuntimeObject* get_data_2() const { return ___data_2; }
	inline RuntimeObject** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(RuntimeObject* value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((&___data_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFBUFFERBASE_T2874699436_H
#ifndef MESHPRECURSOR_T423268032_H
#define MESHPRECURSOR_T423268032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.MeshPrecursor
struct  MeshPrecursor_t423268032  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] PolyToolkitInternal.MeshPrecursor::vertices
	Vector3U5BU5D_t1718750761* ___vertices_0;
	// UnityEngine.Vector3[] PolyToolkitInternal.MeshPrecursor::normals
	Vector3U5BU5D_t1718750761* ___normals_1;
	// UnityEngine.Color[] PolyToolkitInternal.MeshPrecursor::colors
	ColorU5BU5D_t941916413* ___colors_2;
	// UnityEngine.Vector4[] PolyToolkitInternal.MeshPrecursor::tangents
	Vector4U5BU5D_t934056436* ___tangents_3;
	// System.Array[] PolyToolkitInternal.MeshPrecursor::uvSets
	ArrayU5BU5D_t2896390326* ___uvSets_4;
	// System.Int32[] PolyToolkitInternal.MeshPrecursor::triangles
	Int32U5BU5D_t385246372* ___triangles_5;

public:
	inline static int32_t get_offset_of_vertices_0() { return static_cast<int32_t>(offsetof(MeshPrecursor_t423268032, ___vertices_0)); }
	inline Vector3U5BU5D_t1718750761* get_vertices_0() const { return ___vertices_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_vertices_0() { return &___vertices_0; }
	inline void set_vertices_0(Vector3U5BU5D_t1718750761* value)
	{
		___vertices_0 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_0), value);
	}

	inline static int32_t get_offset_of_normals_1() { return static_cast<int32_t>(offsetof(MeshPrecursor_t423268032, ___normals_1)); }
	inline Vector3U5BU5D_t1718750761* get_normals_1() const { return ___normals_1; }
	inline Vector3U5BU5D_t1718750761** get_address_of_normals_1() { return &___normals_1; }
	inline void set_normals_1(Vector3U5BU5D_t1718750761* value)
	{
		___normals_1 = value;
		Il2CppCodeGenWriteBarrier((&___normals_1), value);
	}

	inline static int32_t get_offset_of_colors_2() { return static_cast<int32_t>(offsetof(MeshPrecursor_t423268032, ___colors_2)); }
	inline ColorU5BU5D_t941916413* get_colors_2() const { return ___colors_2; }
	inline ColorU5BU5D_t941916413** get_address_of_colors_2() { return &___colors_2; }
	inline void set_colors_2(ColorU5BU5D_t941916413* value)
	{
		___colors_2 = value;
		Il2CppCodeGenWriteBarrier((&___colors_2), value);
	}

	inline static int32_t get_offset_of_tangents_3() { return static_cast<int32_t>(offsetof(MeshPrecursor_t423268032, ___tangents_3)); }
	inline Vector4U5BU5D_t934056436* get_tangents_3() const { return ___tangents_3; }
	inline Vector4U5BU5D_t934056436** get_address_of_tangents_3() { return &___tangents_3; }
	inline void set_tangents_3(Vector4U5BU5D_t934056436* value)
	{
		___tangents_3 = value;
		Il2CppCodeGenWriteBarrier((&___tangents_3), value);
	}

	inline static int32_t get_offset_of_uvSets_4() { return static_cast<int32_t>(offsetof(MeshPrecursor_t423268032, ___uvSets_4)); }
	inline ArrayU5BU5D_t2896390326* get_uvSets_4() const { return ___uvSets_4; }
	inline ArrayU5BU5D_t2896390326** get_address_of_uvSets_4() { return &___uvSets_4; }
	inline void set_uvSets_4(ArrayU5BU5D_t2896390326* value)
	{
		___uvSets_4 = value;
		Il2CppCodeGenWriteBarrier((&___uvSets_4), value);
	}

	inline static int32_t get_offset_of_triangles_5() { return static_cast<int32_t>(offsetof(MeshPrecursor_t423268032, ___triangles_5)); }
	inline Int32U5BU5D_t385246372* get_triangles_5() const { return ___triangles_5; }
	inline Int32U5BU5D_t385246372** get_address_of_triangles_5() { return &___triangles_5; }
	inline void set_triangles_5(Int32U5BU5D_t385246372* value)
	{
		___triangles_5 = value;
		Il2CppCodeGenWriteBarrier((&___triangles_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHPRECURSOR_T423268032_H
#ifndef GLTFSCENEBASE_T3309998329_H
#define GLTFSCENEBASE_T3309998329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfSceneBase
struct  GltfSceneBase_t3309998329  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PolyToolkitInternal.GltfSceneBase::extras
	Dictionary_2_t1632706988 * ___extras_0;

public:
	inline static int32_t get_offset_of_extras_0() { return static_cast<int32_t>(offsetof(GltfSceneBase_t3309998329, ___extras_0)); }
	inline Dictionary_2_t1632706988 * get_extras_0() const { return ___extras_0; }
	inline Dictionary_2_t1632706988 ** get_address_of_extras_0() { return &___extras_0; }
	inline void set_extras_0(Dictionary_2_t1632706988 * value)
	{
		___extras_0 = value;
		Il2CppCodeGenWriteBarrier((&___extras_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFSCENEBASE_T3309998329_H
#ifndef GLTF2TEXTURE_T3945546157_H
#define GLTF2TEXTURE_T3945546157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf2Texture
struct  Gltf2Texture_t3945546157  : public GltfTextureBase_t3199118737
{
public:
	// System.Int32 PolyToolkitInternal.Gltf2Texture::gltfIndex
	int32_t ___gltfIndex_1;
	// System.Int32 PolyToolkitInternal.Gltf2Texture::source
	int32_t ___source_2;
	// PolyToolkitInternal.Gltf2Image PolyToolkitInternal.Gltf2Texture::sourcePtr
	Gltf2Image_t3036181135 * ___sourcePtr_3;

public:
	inline static int32_t get_offset_of_gltfIndex_1() { return static_cast<int32_t>(offsetof(Gltf2Texture_t3945546157, ___gltfIndex_1)); }
	inline int32_t get_gltfIndex_1() const { return ___gltfIndex_1; }
	inline int32_t* get_address_of_gltfIndex_1() { return &___gltfIndex_1; }
	inline void set_gltfIndex_1(int32_t value)
	{
		___gltfIndex_1 = value;
	}

	inline static int32_t get_offset_of_source_2() { return static_cast<int32_t>(offsetof(Gltf2Texture_t3945546157, ___source_2)); }
	inline int32_t get_source_2() const { return ___source_2; }
	inline int32_t* get_address_of_source_2() { return &___source_2; }
	inline void set_source_2(int32_t value)
	{
		___source_2 = value;
	}

	inline static int32_t get_offset_of_sourcePtr_3() { return static_cast<int32_t>(offsetof(Gltf2Texture_t3945546157, ___sourcePtr_3)); }
	inline Gltf2Image_t3036181135 * get_sourcePtr_3() const { return ___sourcePtr_3; }
	inline Gltf2Image_t3036181135 ** get_address_of_sourcePtr_3() { return &___sourcePtr_3; }
	inline void set_sourcePtr_3(Gltf2Image_t3036181135 * value)
	{
		___sourcePtr_3 = value;
		Il2CppCodeGenWriteBarrier((&___sourcePtr_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF2TEXTURE_T3945546157_H
#ifndef GLTF1SCENE_T747889833_H
#define GLTF1SCENE_T747889833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf1Scene
struct  Gltf1Scene_t747889833  : public GltfSceneBase_t3309998329
{
public:
	// System.Collections.Generic.List`1<System.String> PolyToolkitInternal.Gltf1Scene::nodes
	List_1_t3319525431 * ___nodes_1;
	// System.String PolyToolkitInternal.Gltf1Scene::gltfId
	String_t* ___gltfId_2;
	// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf1Node> PolyToolkitInternal.Gltf1Scene::nodePtrs
	List_1_t2567860186 * ___nodePtrs_3;

public:
	inline static int32_t get_offset_of_nodes_1() { return static_cast<int32_t>(offsetof(Gltf1Scene_t747889833, ___nodes_1)); }
	inline List_1_t3319525431 * get_nodes_1() const { return ___nodes_1; }
	inline List_1_t3319525431 ** get_address_of_nodes_1() { return &___nodes_1; }
	inline void set_nodes_1(List_1_t3319525431 * value)
	{
		___nodes_1 = value;
		Il2CppCodeGenWriteBarrier((&___nodes_1), value);
	}

	inline static int32_t get_offset_of_gltfId_2() { return static_cast<int32_t>(offsetof(Gltf1Scene_t747889833, ___gltfId_2)); }
	inline String_t* get_gltfId_2() const { return ___gltfId_2; }
	inline String_t** get_address_of_gltfId_2() { return &___gltfId_2; }
	inline void set_gltfId_2(String_t* value)
	{
		___gltfId_2 = value;
		Il2CppCodeGenWriteBarrier((&___gltfId_2), value);
	}

	inline static int32_t get_offset_of_nodePtrs_3() { return static_cast<int32_t>(offsetof(Gltf1Scene_t747889833, ___nodePtrs_3)); }
	inline List_1_t2567860186 * get_nodePtrs_3() const { return ___nodePtrs_3; }
	inline List_1_t2567860186 ** get_address_of_nodePtrs_3() { return &___nodePtrs_3; }
	inline void set_nodePtrs_3(List_1_t2567860186 * value)
	{
		___nodePtrs_3 = value;
		Il2CppCodeGenWriteBarrier((&___nodePtrs_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF1SCENE_T747889833_H
#ifndef GLTF2IMAGE_T3036181135_H
#define GLTF2IMAGE_T3036181135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf2Image
struct  Gltf2Image_t3036181135  : public GltfImageBase_t4142612163
{
public:
	// System.Int32 PolyToolkitInternal.Gltf2Image::gltfIndex
	int32_t ___gltfIndex_2;
	// System.String PolyToolkitInternal.Gltf2Image::name
	String_t* ___name_3;
	// System.String PolyToolkitInternal.Gltf2Image::mimeType
	String_t* ___mimeType_4;

public:
	inline static int32_t get_offset_of_gltfIndex_2() { return static_cast<int32_t>(offsetof(Gltf2Image_t3036181135, ___gltfIndex_2)); }
	inline int32_t get_gltfIndex_2() const { return ___gltfIndex_2; }
	inline int32_t* get_address_of_gltfIndex_2() { return &___gltfIndex_2; }
	inline void set_gltfIndex_2(int32_t value)
	{
		___gltfIndex_2 = value;
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(Gltf2Image_t3036181135, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}

	inline static int32_t get_offset_of_mimeType_4() { return static_cast<int32_t>(offsetof(Gltf2Image_t3036181135, ___mimeType_4)); }
	inline String_t* get_mimeType_4() const { return ___mimeType_4; }
	inline String_t** get_address_of_mimeType_4() { return &___mimeType_4; }
	inline void set_mimeType_4(String_t* value)
	{
		___mimeType_4 = value;
		Il2CppCodeGenWriteBarrier((&___mimeType_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF2IMAGE_T3036181135_H
#ifndef POLYSTATUS_T3145373940_H
#define POLYSTATUS_T3145373940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyStatus
struct  PolyStatus_t3145373940 
{
public:
	// System.Boolean PolyToolkit.PolyStatus::ok
	bool ___ok_0;
	// System.String PolyToolkit.PolyStatus::errorMessage
	String_t* ___errorMessage_1;

public:
	inline static int32_t get_offset_of_ok_0() { return static_cast<int32_t>(offsetof(PolyStatus_t3145373940, ___ok_0)); }
	inline bool get_ok_0() const { return ___ok_0; }
	inline bool* get_address_of_ok_0() { return &___ok_0; }
	inline void set_ok_0(bool value)
	{
		___ok_0 = value;
	}

	inline static int32_t get_offset_of_errorMessage_1() { return static_cast<int32_t>(offsetof(PolyStatus_t3145373940, ___errorMessage_1)); }
	inline String_t* get_errorMessage_1() const { return ___errorMessage_1; }
	inline String_t** get_address_of_errorMessage_1() { return &___errorMessage_1; }
	inline void set_errorMessage_1(String_t* value)
	{
		___errorMessage_1 = value;
		Il2CppCodeGenWriteBarrier((&___errorMessage_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PolyToolkit.PolyStatus
struct PolyStatus_t3145373940_marshaled_pinvoke
{
	int32_t ___ok_0;
	char* ___errorMessage_1;
};
// Native definition for COM marshalling of PolyToolkit.PolyStatus
struct PolyStatus_t3145373940_marshaled_com
{
	int32_t ___ok_0;
	Il2CppChar* ___errorMessage_1;
};
#endif // POLYSTATUS_T3145373940_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef GLTF2MESH_T1559994_H
#define GLTF2MESH_T1559994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf2Mesh
struct  Gltf2Mesh_t1559994  : public GltfMeshBase_t3664755873
{
public:
	// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf2Primitive> PolyToolkitInternal.Gltf2Mesh::primitives
	List_1_t1060975091 * ___primitives_1;
	// System.Int32 PolyToolkitInternal.Gltf2Mesh::gltfIndex
	int32_t ___gltfIndex_2;

public:
	inline static int32_t get_offset_of_primitives_1() { return static_cast<int32_t>(offsetof(Gltf2Mesh_t1559994, ___primitives_1)); }
	inline List_1_t1060975091 * get_primitives_1() const { return ___primitives_1; }
	inline List_1_t1060975091 ** get_address_of_primitives_1() { return &___primitives_1; }
	inline void set_primitives_1(List_1_t1060975091 * value)
	{
		___primitives_1 = value;
		Il2CppCodeGenWriteBarrier((&___primitives_1), value);
	}

	inline static int32_t get_offset_of_gltfIndex_2() { return static_cast<int32_t>(offsetof(Gltf2Mesh_t1559994, ___gltfIndex_2)); }
	inline int32_t get_gltfIndex_2() const { return ___gltfIndex_2; }
	inline int32_t* get_address_of_gltfIndex_2() { return &___gltfIndex_2; }
	inline void set_gltfIndex_2(int32_t value)
	{
		___gltfIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF2MESH_T1559994_H
#ifndef GLTF2SCENE_T747889802_H
#define GLTF2SCENE_T747889802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf2Scene
struct  Gltf2Scene_t747889802  : public GltfSceneBase_t3309998329
{
public:
	// System.Collections.Generic.List`1<System.Int32> PolyToolkitInternal.Gltf2Scene::nodes
	List_1_t128053199 * ___nodes_1;
	// System.Int32 PolyToolkitInternal.Gltf2Scene::gltfIndex
	int32_t ___gltfIndex_2;
	// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf2Node> PolyToolkitInternal.Gltf2Scene::nodePtrs
	List_1_t2567860153 * ___nodePtrs_3;

public:
	inline static int32_t get_offset_of_nodes_1() { return static_cast<int32_t>(offsetof(Gltf2Scene_t747889802, ___nodes_1)); }
	inline List_1_t128053199 * get_nodes_1() const { return ___nodes_1; }
	inline List_1_t128053199 ** get_address_of_nodes_1() { return &___nodes_1; }
	inline void set_nodes_1(List_1_t128053199 * value)
	{
		___nodes_1 = value;
		Il2CppCodeGenWriteBarrier((&___nodes_1), value);
	}

	inline static int32_t get_offset_of_gltfIndex_2() { return static_cast<int32_t>(offsetof(Gltf2Scene_t747889802, ___gltfIndex_2)); }
	inline int32_t get_gltfIndex_2() const { return ___gltfIndex_2; }
	inline int32_t* get_address_of_gltfIndex_2() { return &___gltfIndex_2; }
	inline void set_gltfIndex_2(int32_t value)
	{
		___gltfIndex_2 = value;
	}

	inline static int32_t get_offset_of_nodePtrs_3() { return static_cast<int32_t>(offsetof(Gltf2Scene_t747889802, ___nodePtrs_3)); }
	inline List_1_t2567860153 * get_nodePtrs_3() const { return ___nodePtrs_3; }
	inline List_1_t2567860153 ** get_address_of_nodePtrs_3() { return &___nodePtrs_3; }
	inline void set_nodePtrs_3(List_1_t2567860153 * value)
	{
		___nodePtrs_3 = value;
		Il2CppCodeGenWriteBarrier((&___nodePtrs_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF2SCENE_T747889802_H
#ifndef GLTF2BUFFERVIEW_T1795530950_H
#define GLTF2BUFFERVIEW_T1795530950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf2BufferView
struct  Gltf2BufferView_t1795530950  : public GltfBufferViewBase_t1504165784
{
public:
	// System.Int32 PolyToolkitInternal.Gltf2BufferView::buffer
	int32_t ___buffer_3;
	// System.Int32 PolyToolkitInternal.Gltf2BufferView::gltfIndex
	int32_t ___gltfIndex_4;
	// PolyToolkitInternal.Gltf2Buffer PolyToolkitInternal.Gltf2BufferView::bufferPtr
	Gltf2Buffer_t1264135663 * ___bufferPtr_5;

public:
	inline static int32_t get_offset_of_buffer_3() { return static_cast<int32_t>(offsetof(Gltf2BufferView_t1795530950, ___buffer_3)); }
	inline int32_t get_buffer_3() const { return ___buffer_3; }
	inline int32_t* get_address_of_buffer_3() { return &___buffer_3; }
	inline void set_buffer_3(int32_t value)
	{
		___buffer_3 = value;
	}

	inline static int32_t get_offset_of_gltfIndex_4() { return static_cast<int32_t>(offsetof(Gltf2BufferView_t1795530950, ___gltfIndex_4)); }
	inline int32_t get_gltfIndex_4() const { return ___gltfIndex_4; }
	inline int32_t* get_address_of_gltfIndex_4() { return &___gltfIndex_4; }
	inline void set_gltfIndex_4(int32_t value)
	{
		___gltfIndex_4 = value;
	}

	inline static int32_t get_offset_of_bufferPtr_5() { return static_cast<int32_t>(offsetof(Gltf2BufferView_t1795530950, ___bufferPtr_5)); }
	inline Gltf2Buffer_t1264135663 * get_bufferPtr_5() const { return ___bufferPtr_5; }
	inline Gltf2Buffer_t1264135663 ** get_address_of_bufferPtr_5() { return &___bufferPtr_5; }
	inline void set_bufferPtr_5(Gltf2Buffer_t1264135663 * value)
	{
		___bufferPtr_5 = value;
		Il2CppCodeGenWriteBarrier((&___bufferPtr_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF2BUFFERVIEW_T1795530950_H
#ifndef JSONVECTORCONVERTER_T251778730_H
#define JSONVECTORCONVERTER_T251778730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.JsonVectorConverter
struct  JsonVectorConverter_t251778730  : public JsonConverter_t1047106545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONVECTORCONVERTER_T251778730_H
#ifndef GLTF2BUFFER_T1264135663_H
#define GLTF2BUFFER_T1264135663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf2Buffer
struct  Gltf2Buffer_t1264135663  : public GltfBufferBase_t2874699436
{
public:
	// System.Int32 PolyToolkitInternal.Gltf2Buffer::gltfIndex
	int32_t ___gltfIndex_3;

public:
	inline static int32_t get_offset_of_gltfIndex_3() { return static_cast<int32_t>(offsetof(Gltf2Buffer_t1264135663, ___gltfIndex_3)); }
	inline int32_t get_gltfIndex_3() const { return ___gltfIndex_3; }
	inline int32_t* get_address_of_gltfIndex_3() { return &___gltfIndex_3; }
	inline void set_gltfIndex_3(int32_t value)
	{
		___gltfIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF2BUFFER_T1264135663_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef UNITYMATERIAL_T3949128076_H
#define UNITYMATERIAL_T3949128076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfMaterialConverter/UnityMaterial
struct  UnityMaterial_t3949128076 
{
public:
	// UnityEngine.Material PolyToolkitInternal.GltfMaterialConverter/UnityMaterial::material
	Material_t340375123 * ___material_0;
	// UnityEngine.Material PolyToolkitInternal.GltfMaterialConverter/UnityMaterial::template
	Material_t340375123 * ___template_1;

public:
	inline static int32_t get_offset_of_material_0() { return static_cast<int32_t>(offsetof(UnityMaterial_t3949128076, ___material_0)); }
	inline Material_t340375123 * get_material_0() const { return ___material_0; }
	inline Material_t340375123 ** get_address_of_material_0() { return &___material_0; }
	inline void set_material_0(Material_t340375123 * value)
	{
		___material_0 = value;
		Il2CppCodeGenWriteBarrier((&___material_0), value);
	}

	inline static int32_t get_offset_of_template_1() { return static_cast<int32_t>(offsetof(UnityMaterial_t3949128076, ___template_1)); }
	inline Material_t340375123 * get_template_1() const { return ___template_1; }
	inline Material_t340375123 ** get_address_of_template_1() { return &___template_1; }
	inline void set_template_1(Material_t340375123 * value)
	{
		___template_1 = value;
		Il2CppCodeGenWriteBarrier((&___template_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PolyToolkitInternal.GltfMaterialConverter/UnityMaterial
struct UnityMaterial_t3949128076_marshaled_pinvoke
{
	Material_t340375123 * ___material_0;
	Material_t340375123 * ___template_1;
};
// Native definition for COM marshalling of PolyToolkitInternal.GltfMaterialConverter/UnityMaterial
struct UnityMaterial_t3949128076_marshaled_com
{
	Material_t340375123 * ___material_0;
	Material_t340375123 * ___template_1;
};
#endif // UNITYMATERIAL_T3949128076_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef NULLABLE_1_T3119828856_H
#define NULLABLE_1_T3119828856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Single>
struct  Nullable_1_t3119828856 
{
public:
	// T System.Nullable`1::value
	float ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3119828856, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3119828856, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3119828856_H
#ifndef ENUMERATOR_T2950219929_H
#define ENUMERATOR_T2950219929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<PolyToolkitInternal.Gltf1Primitive>
struct  Enumerator_t2950219929 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1060976052 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Gltf1Primitive_t3883868606 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2950219929, ___l_0)); }
	inline List_1_t1060976052 * get_l_0() const { return ___l_0; }
	inline List_1_t1060976052 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t1060976052 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2950219929, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2950219929, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2950219929, ___current_3)); }
	inline Gltf1Primitive_t3883868606 * get_current_3() const { return ___current_3; }
	inline Gltf1Primitive_t3883868606 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Gltf1Primitive_t3883868606 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2950219929_H
#ifndef AUTOSTRINGIFYABRIDGED_T1725278763_H
#define AUTOSTRINGIFYABRIDGED_T1725278763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.AutoStringifyAbridged
struct  AutoStringifyAbridged_t1725278763  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOSTRINGIFYABRIDGED_T1725278763_H
#ifndef AUTOSTRINGIFIABLE_T1804051496_H
#define AUTOSTRINGIFIABLE_T1804051496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.AutoStringifiable
struct  AutoStringifiable_t1804051496  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOSTRINGIFIABLE_T1804051496_H
#ifndef ENUMERATOR_T162136767_H
#define ENUMERATOR_T162136767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<PolyToolkitInternal.Gltf1Node>
struct  Enumerator_t162136767 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t2567860186 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Gltf1Node_t1095785444 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t162136767, ___l_0)); }
	inline List_1_t2567860186 * get_l_0() const { return ___l_0; }
	inline List_1_t2567860186 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t2567860186 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t162136767, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t162136767, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t162136767, ___current_3)); }
	inline Gltf1Node_t1095785444 * get_current_3() const { return ___current_3; }
	inline Gltf1Node_t1095785444 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Gltf1Node_t1095785444 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T162136767_H
#ifndef ENUMERATOR_T2950218968_H
#define ENUMERATOR_T2950218968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<PolyToolkitInternal.Gltf2Primitive>
struct  Enumerator_t2950218968 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1060975091 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Gltf2Primitive_t3883867645 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2950218968, ___l_0)); }
	inline List_1_t1060975091 * get_l_0() const { return ___l_0; }
	inline List_1_t1060975091 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t1060975091 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2950218968, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2950218968, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2950218968, ___current_3)); }
	inline Gltf2Primitive_t3883867645 * get_current_3() const { return ___current_3; }
	inline Gltf2Primitive_t3883867645 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Gltf2Primitive_t3883867645 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2950218968_H
#ifndef ENUMERATOR_T162136734_H
#define ENUMERATOR_T162136734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<PolyToolkitInternal.Gltf2Node>
struct  Enumerator_t162136734 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t2567860153 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Gltf2Node_t1095785411 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t162136734, ___l_0)); }
	inline List_1_t2567860153 * get_l_0() const { return ___l_0; }
	inline List_1_t2567860153 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t2567860153 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t162136734, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t162136734, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t162136734, ___current_3)); }
	inline Gltf2Node_t1095785411 * get_current_3() const { return ___current_3; }
	inline Gltf2Node_t1095785411 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Gltf2Node_t1095785411 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T162136734_H
#ifndef VERSION_T724142152_H
#define VERSION_T724142152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Version
struct  Version_t724142152 
{
public:
	// System.Int32 PolyToolkitInternal.Version::major
	int32_t ___major_0;
	// System.Int32 PolyToolkitInternal.Version::minor
	int32_t ___minor_1;

public:
	inline static int32_t get_offset_of_major_0() { return static_cast<int32_t>(offsetof(Version_t724142152, ___major_0)); }
	inline int32_t get_major_0() const { return ___major_0; }
	inline int32_t* get_address_of_major_0() { return &___major_0; }
	inline void set_major_0(int32_t value)
	{
		___major_0 = value;
	}

	inline static int32_t get_offset_of_minor_1() { return static_cast<int32_t>(offsetof(Version_t724142152, ___minor_1)); }
	inline int32_t get_minor_1() const { return ___minor_1; }
	inline int32_t* get_address_of_minor_1() { return &___minor_1; }
	inline void set_minor_1(int32_t value)
	{
		___minor_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERSION_T724142152_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef BADJSON_T2001807480_H
#define BADJSON_T2001807480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.BadJson
struct  BadJson_t2001807480  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BADJSON_T2001807480_H
#ifndef GLTF1MESH_T1560025_H
#define GLTF1MESH_T1560025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf1Mesh
struct  Gltf1Mesh_t1560025  : public GltfMeshBase_t3664755873
{
public:
	// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf1Primitive> PolyToolkitInternal.Gltf1Mesh::primitives
	List_1_t1060976052 * ___primitives_1;
	// System.String PolyToolkitInternal.Gltf1Mesh::gltfId
	String_t* ___gltfId_2;

public:
	inline static int32_t get_offset_of_primitives_1() { return static_cast<int32_t>(offsetof(Gltf1Mesh_t1560025, ___primitives_1)); }
	inline List_1_t1060976052 * get_primitives_1() const { return ___primitives_1; }
	inline List_1_t1060976052 ** get_address_of_primitives_1() { return &___primitives_1; }
	inline void set_primitives_1(List_1_t1060976052 * value)
	{
		___primitives_1 = value;
		Il2CppCodeGenWriteBarrier((&___primitives_1), value);
	}

	inline static int32_t get_offset_of_gltfId_2() { return static_cast<int32_t>(offsetof(Gltf1Mesh_t1560025, ___gltfId_2)); }
	inline String_t* get_gltfId_2() const { return ___gltfId_2; }
	inline String_t** get_address_of_gltfId_2() { return &___gltfId_2; }
	inline void set_gltfId_2(String_t* value)
	{
		___gltfId_2 = value;
		Il2CppCodeGenWriteBarrier((&___gltfId_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF1MESH_T1560025_H
#ifndef GLTF1MATERIAL_T2703072588_H
#define GLTF1MATERIAL_T2703072588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf1Material
struct  Gltf1Material_t2703072588  : public GltfMaterialBase_t3607200052
{
public:
	// System.String PolyToolkitInternal.Gltf1Material::technique
	String_t* ___technique_1;
	// PolyToolkitInternal.TiltBrushGltf1PbrValues PolyToolkitInternal.Gltf1Material::values
	TiltBrushGltf1PbrValues_t1054533882 * ___values_2;
	// System.String PolyToolkitInternal.Gltf1Material::gltfId
	String_t* ___gltfId_3;
	// PolyToolkitInternal.Gltf1Technique PolyToolkitInternal.Gltf1Material::techniquePtr
	Gltf1Technique_t503673868 * ___techniquePtr_4;

public:
	inline static int32_t get_offset_of_technique_1() { return static_cast<int32_t>(offsetof(Gltf1Material_t2703072588, ___technique_1)); }
	inline String_t* get_technique_1() const { return ___technique_1; }
	inline String_t** get_address_of_technique_1() { return &___technique_1; }
	inline void set_technique_1(String_t* value)
	{
		___technique_1 = value;
		Il2CppCodeGenWriteBarrier((&___technique_1), value);
	}

	inline static int32_t get_offset_of_values_2() { return static_cast<int32_t>(offsetof(Gltf1Material_t2703072588, ___values_2)); }
	inline TiltBrushGltf1PbrValues_t1054533882 * get_values_2() const { return ___values_2; }
	inline TiltBrushGltf1PbrValues_t1054533882 ** get_address_of_values_2() { return &___values_2; }
	inline void set_values_2(TiltBrushGltf1PbrValues_t1054533882 * value)
	{
		___values_2 = value;
		Il2CppCodeGenWriteBarrier((&___values_2), value);
	}

	inline static int32_t get_offset_of_gltfId_3() { return static_cast<int32_t>(offsetof(Gltf1Material_t2703072588, ___gltfId_3)); }
	inline String_t* get_gltfId_3() const { return ___gltfId_3; }
	inline String_t** get_address_of_gltfId_3() { return &___gltfId_3; }
	inline void set_gltfId_3(String_t* value)
	{
		___gltfId_3 = value;
		Il2CppCodeGenWriteBarrier((&___gltfId_3), value);
	}

	inline static int32_t get_offset_of_techniquePtr_4() { return static_cast<int32_t>(offsetof(Gltf1Material_t2703072588, ___techniquePtr_4)); }
	inline Gltf1Technique_t503673868 * get_techniquePtr_4() const { return ___techniquePtr_4; }
	inline Gltf1Technique_t503673868 ** get_address_of_techniquePtr_4() { return &___techniquePtr_4; }
	inline void set_techniquePtr_4(Gltf1Technique_t503673868 * value)
	{
		___techniquePtr_4 = value;
		Il2CppCodeGenWriteBarrier((&___techniquePtr_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF1MATERIAL_T2703072588_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef GLTF1TEXTURE_T3945546188_H
#define GLTF1TEXTURE_T3945546188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf1Texture
struct  Gltf1Texture_t3945546188  : public GltfTextureBase_t3199118737
{
public:
	// System.String PolyToolkitInternal.Gltf1Texture::gltfId
	String_t* ___gltfId_1;
	// System.String PolyToolkitInternal.Gltf1Texture::source
	String_t* ___source_2;
	// PolyToolkitInternal.Gltf1Image PolyToolkitInternal.Gltf1Texture::sourcePtr
	Gltf1Image_t3036181168 * ___sourcePtr_3;

public:
	inline static int32_t get_offset_of_gltfId_1() { return static_cast<int32_t>(offsetof(Gltf1Texture_t3945546188, ___gltfId_1)); }
	inline String_t* get_gltfId_1() const { return ___gltfId_1; }
	inline String_t** get_address_of_gltfId_1() { return &___gltfId_1; }
	inline void set_gltfId_1(String_t* value)
	{
		___gltfId_1 = value;
		Il2CppCodeGenWriteBarrier((&___gltfId_1), value);
	}

	inline static int32_t get_offset_of_source_2() { return static_cast<int32_t>(offsetof(Gltf1Texture_t3945546188, ___source_2)); }
	inline String_t* get_source_2() const { return ___source_2; }
	inline String_t** get_address_of_source_2() { return &___source_2; }
	inline void set_source_2(String_t* value)
	{
		___source_2 = value;
		Il2CppCodeGenWriteBarrier((&___source_2), value);
	}

	inline static int32_t get_offset_of_sourcePtr_3() { return static_cast<int32_t>(offsetof(Gltf1Texture_t3945546188, ___sourcePtr_3)); }
	inline Gltf1Image_t3036181168 * get_sourcePtr_3() const { return ___sourcePtr_3; }
	inline Gltf1Image_t3036181168 ** get_address_of_sourcePtr_3() { return &___sourcePtr_3; }
	inline void set_sourcePtr_3(Gltf1Image_t3036181168 * value)
	{
		___sourcePtr_3 = value;
		Il2CppCodeGenWriteBarrier((&___sourcePtr_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF1TEXTURE_T3945546188_H
#ifndef GLTF1IMAGE_T3036181168_H
#define GLTF1IMAGE_T3036181168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf1Image
struct  Gltf1Image_t3036181168  : public GltfImageBase_t4142612163
{
public:
	// System.String PolyToolkitInternal.Gltf1Image::gltfId
	String_t* ___gltfId_2;

public:
	inline static int32_t get_offset_of_gltfId_2() { return static_cast<int32_t>(offsetof(Gltf1Image_t3036181168, ___gltfId_2)); }
	inline String_t* get_gltfId_2() const { return ___gltfId_2; }
	inline String_t** get_address_of_gltfId_2() { return &___gltfId_2; }
	inline void set_gltfId_2(String_t* value)
	{
		___gltfId_2 = value;
		Il2CppCodeGenWriteBarrier((&___gltfId_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF1IMAGE_T3036181168_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef GLTF1BUFFER_T1264135694_H
#define GLTF1BUFFER_T1264135694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf1Buffer
struct  Gltf1Buffer_t1264135694  : public GltfBufferBase_t2874699436
{
public:
	// System.String PolyToolkitInternal.Gltf1Buffer::type
	String_t* ___type_3;
	// System.String PolyToolkitInternal.Gltf1Buffer::gltfId
	String_t* ___gltfId_4;

public:
	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(Gltf1Buffer_t1264135694, ___type_3)); }
	inline String_t* get_type_3() const { return ___type_3; }
	inline String_t** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(String_t* value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}

	inline static int32_t get_offset_of_gltfId_4() { return static_cast<int32_t>(offsetof(Gltf1Buffer_t1264135694, ___gltfId_4)); }
	inline String_t* get_gltfId_4() const { return ___gltfId_4; }
	inline String_t** get_address_of_gltfId_4() { return &___gltfId_4; }
	inline void set_gltfId_4(String_t* value)
	{
		___gltfId_4 = value;
		Il2CppCodeGenWriteBarrier((&___gltfId_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF1BUFFER_T1264135694_H
#ifndef GLTF1BUFFERVIEW_T1795531975_H
#define GLTF1BUFFERVIEW_T1795531975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf1BufferView
struct  Gltf1BufferView_t1795531975  : public GltfBufferViewBase_t1504165784
{
public:
	// System.String PolyToolkitInternal.Gltf1BufferView::buffer
	String_t* ___buffer_3;
	// System.String PolyToolkitInternal.Gltf1BufferView::gltfId
	String_t* ___gltfId_4;
	// PolyToolkitInternal.Gltf1Buffer PolyToolkitInternal.Gltf1BufferView::bufferPtr
	Gltf1Buffer_t1264135694 * ___bufferPtr_5;

public:
	inline static int32_t get_offset_of_buffer_3() { return static_cast<int32_t>(offsetof(Gltf1BufferView_t1795531975, ___buffer_3)); }
	inline String_t* get_buffer_3() const { return ___buffer_3; }
	inline String_t** get_address_of_buffer_3() { return &___buffer_3; }
	inline void set_buffer_3(String_t* value)
	{
		___buffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_3), value);
	}

	inline static int32_t get_offset_of_gltfId_4() { return static_cast<int32_t>(offsetof(Gltf1BufferView_t1795531975, ___gltfId_4)); }
	inline String_t* get_gltfId_4() const { return ___gltfId_4; }
	inline String_t** get_address_of_gltfId_4() { return &___gltfId_4; }
	inline void set_gltfId_4(String_t* value)
	{
		___gltfId_4 = value;
		Il2CppCodeGenWriteBarrier((&___gltfId_4), value);
	}

	inline static int32_t get_offset_of_bufferPtr_5() { return static_cast<int32_t>(offsetof(Gltf1BufferView_t1795531975, ___bufferPtr_5)); }
	inline Gltf1Buffer_t1264135694 * get_bufferPtr_5() const { return ___bufferPtr_5; }
	inline Gltf1Buffer_t1264135694 ** get_address_of_bufferPtr_5() { return &___bufferPtr_5; }
	inline void set_bufferPtr_5(Gltf1Buffer_t1264135694 * value)
	{
		___bufferPtr_5 = value;
		Il2CppCodeGenWriteBarrier((&___bufferPtr_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF1BUFFERVIEW_T1795531975_H
#ifndef COMPONENTTYPE_T584972487_H
#define COMPONENTTYPE_T584972487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfAccessorBase/ComponentType
struct  ComponentType_t584972487 
{
public:
	// System.Int32 PolyToolkitInternal.GltfAccessorBase/ComponentType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ComponentType_t584972487, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTTYPE_T584972487_H
#ifndef NULLABLE_1_T4278248406_H
#define NULLABLE_1_T4278248406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Color>
struct  Nullable_1_t4278248406 
{
public:
	// T System.Nullable`1::value
	Color_t2555686324  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t4278248406, ___value_0)); }
	inline Color_t2555686324  get_value_0() const { return ___value_0; }
	inline Color_t2555686324 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Color_t2555686324  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t4278248406, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T4278248406_H
#ifndef MODE_T1780623943_H
#define MODE_T1780623943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfPrimitiveBase/Mode
struct  Mode_t1780623943 
{
public:
	// System.Int32 PolyToolkitInternal.GltfPrimitiveBase/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1780623943, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1780623943_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef NULLABLE_1_T3540463925_H
#define NULLABLE_1_T3540463925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Matrix4x4>
struct  Nullable_1_t3540463925 
{
public:
	// T System.Nullable`1::value
	Matrix4x4_t1817901843  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3540463925, ___value_0)); }
	inline Matrix4x4_t1817901843  get_value_0() const { return ___value_0; }
	inline Matrix4x4_t1817901843 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Matrix4x4_t1817901843  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3540463925, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3540463925_H
#ifndef RESCALINGMODE_T410920913_H
#define RESCALINGMODE_T410920913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyImportOptions/RescalingMode
struct  RescalingMode_t410920913 
{
public:
	// System.Int32 PolyToolkit.PolyImportOptions/RescalingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RescalingMode_t410920913, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESCALINGMODE_T410920913_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef NULLABLE_1_T2446704234_H
#define NULLABLE_1_T2446704234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<PolyToolkitInternal.Version>
struct  Nullable_1_t2446704234 
{
public:
	// T System.Nullable`1::value
	Version_t724142152  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2446704234, ___value_0)); }
	inline Version_t724142152  get_value_0() const { return ___value_0; }
	inline Version_t724142152 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Version_t724142152  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2446704234, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2446704234_H
#ifndef GLTF2MATERIAL_T2703071627_H
#define GLTF2MATERIAL_T2703071627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf2Material
struct  Gltf2Material_t2703071627  : public GltfMaterialBase_t3607200052
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PolyToolkitInternal.Gltf2Material::extras
	Dictionary_2_t1632706988 * ___extras_4;
	// System.Int32 PolyToolkitInternal.Gltf2Material::gltfIndex
	int32_t ___gltfIndex_5;
	// PolyToolkitInternal.Gltf2Material/PbrMetallicRoughness PolyToolkitInternal.Gltf2Material::pbrMetallicRoughness
	PbrMetallicRoughness_t2508879888 * ___pbrMetallicRoughness_6;
	// PolyToolkitInternal.Gltf2Material/TextureInfo PolyToolkitInternal.Gltf2Material::normalTexture
	TextureInfo_t2535341730 * ___normalTexture_7;
	// PolyToolkitInternal.Gltf2Material/TextureInfo PolyToolkitInternal.Gltf2Material::emissiveTexture
	TextureInfo_t2535341730 * ___emissiveTexture_8;
	// UnityEngine.Vector3 PolyToolkitInternal.Gltf2Material::emissiveFactor
	Vector3_t3722313464  ___emissiveFactor_9;
	// System.String PolyToolkitInternal.Gltf2Material::alphaMode
	String_t* ___alphaMode_10;
	// System.Boolean PolyToolkitInternal.Gltf2Material::doubleSided
	bool ___doubleSided_11;

public:
	inline static int32_t get_offset_of_extras_4() { return static_cast<int32_t>(offsetof(Gltf2Material_t2703071627, ___extras_4)); }
	inline Dictionary_2_t1632706988 * get_extras_4() const { return ___extras_4; }
	inline Dictionary_2_t1632706988 ** get_address_of_extras_4() { return &___extras_4; }
	inline void set_extras_4(Dictionary_2_t1632706988 * value)
	{
		___extras_4 = value;
		Il2CppCodeGenWriteBarrier((&___extras_4), value);
	}

	inline static int32_t get_offset_of_gltfIndex_5() { return static_cast<int32_t>(offsetof(Gltf2Material_t2703071627, ___gltfIndex_5)); }
	inline int32_t get_gltfIndex_5() const { return ___gltfIndex_5; }
	inline int32_t* get_address_of_gltfIndex_5() { return &___gltfIndex_5; }
	inline void set_gltfIndex_5(int32_t value)
	{
		___gltfIndex_5 = value;
	}

	inline static int32_t get_offset_of_pbrMetallicRoughness_6() { return static_cast<int32_t>(offsetof(Gltf2Material_t2703071627, ___pbrMetallicRoughness_6)); }
	inline PbrMetallicRoughness_t2508879888 * get_pbrMetallicRoughness_6() const { return ___pbrMetallicRoughness_6; }
	inline PbrMetallicRoughness_t2508879888 ** get_address_of_pbrMetallicRoughness_6() { return &___pbrMetallicRoughness_6; }
	inline void set_pbrMetallicRoughness_6(PbrMetallicRoughness_t2508879888 * value)
	{
		___pbrMetallicRoughness_6 = value;
		Il2CppCodeGenWriteBarrier((&___pbrMetallicRoughness_6), value);
	}

	inline static int32_t get_offset_of_normalTexture_7() { return static_cast<int32_t>(offsetof(Gltf2Material_t2703071627, ___normalTexture_7)); }
	inline TextureInfo_t2535341730 * get_normalTexture_7() const { return ___normalTexture_7; }
	inline TextureInfo_t2535341730 ** get_address_of_normalTexture_7() { return &___normalTexture_7; }
	inline void set_normalTexture_7(TextureInfo_t2535341730 * value)
	{
		___normalTexture_7 = value;
		Il2CppCodeGenWriteBarrier((&___normalTexture_7), value);
	}

	inline static int32_t get_offset_of_emissiveTexture_8() { return static_cast<int32_t>(offsetof(Gltf2Material_t2703071627, ___emissiveTexture_8)); }
	inline TextureInfo_t2535341730 * get_emissiveTexture_8() const { return ___emissiveTexture_8; }
	inline TextureInfo_t2535341730 ** get_address_of_emissiveTexture_8() { return &___emissiveTexture_8; }
	inline void set_emissiveTexture_8(TextureInfo_t2535341730 * value)
	{
		___emissiveTexture_8 = value;
		Il2CppCodeGenWriteBarrier((&___emissiveTexture_8), value);
	}

	inline static int32_t get_offset_of_emissiveFactor_9() { return static_cast<int32_t>(offsetof(Gltf2Material_t2703071627, ___emissiveFactor_9)); }
	inline Vector3_t3722313464  get_emissiveFactor_9() const { return ___emissiveFactor_9; }
	inline Vector3_t3722313464 * get_address_of_emissiveFactor_9() { return &___emissiveFactor_9; }
	inline void set_emissiveFactor_9(Vector3_t3722313464  value)
	{
		___emissiveFactor_9 = value;
	}

	inline static int32_t get_offset_of_alphaMode_10() { return static_cast<int32_t>(offsetof(Gltf2Material_t2703071627, ___alphaMode_10)); }
	inline String_t* get_alphaMode_10() const { return ___alphaMode_10; }
	inline String_t** get_address_of_alphaMode_10() { return &___alphaMode_10; }
	inline void set_alphaMode_10(String_t* value)
	{
		___alphaMode_10 = value;
		Il2CppCodeGenWriteBarrier((&___alphaMode_10), value);
	}

	inline static int32_t get_offset_of_doubleSided_11() { return static_cast<int32_t>(offsetof(Gltf2Material_t2703071627, ___doubleSided_11)); }
	inline bool get_doubleSided_11() const { return ___doubleSided_11; }
	inline bool* get_address_of_doubleSided_11() { return &___doubleSided_11; }
	inline void set_doubleSided_11(bool value)
	{
		___doubleSided_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF2MATERIAL_T2703071627_H
#ifndef U3CU3EC__ITERATOR0_T693557650_H
#define U3CU3EC__ITERATOR0_T693557650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf1Scene/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t693557650  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1/Enumerator<PolyToolkitInternal.Gltf1Node> PolyToolkitInternal.Gltf1Scene/<>c__Iterator0::$locvar0
	Enumerator_t162136767  ___U24locvar0_0;
	// PolyToolkitInternal.Gltf1Node PolyToolkitInternal.Gltf1Scene/<>c__Iterator0::<node>__1
	Gltf1Node_t1095785444 * ___U3CnodeU3E__1_1;
	// PolyToolkitInternal.Gltf1Scene PolyToolkitInternal.Gltf1Scene/<>c__Iterator0::$this
	Gltf1Scene_t747889833 * ___U24this_2;
	// PolyToolkitInternal.GltfNodeBase PolyToolkitInternal.Gltf1Scene/<>c__Iterator0::$current
	GltfNodeBase_t3005766873 * ___U24current_3;
	// System.Boolean PolyToolkitInternal.Gltf1Scene/<>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 PolyToolkitInternal.Gltf1Scene/<>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t693557650, ___U24locvar0_0)); }
	inline Enumerator_t162136767  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t162136767 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t162136767  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CnodeU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t693557650, ___U3CnodeU3E__1_1)); }
	inline Gltf1Node_t1095785444 * get_U3CnodeU3E__1_1() const { return ___U3CnodeU3E__1_1; }
	inline Gltf1Node_t1095785444 ** get_address_of_U3CnodeU3E__1_1() { return &___U3CnodeU3E__1_1; }
	inline void set_U3CnodeU3E__1_1(Gltf1Node_t1095785444 * value)
	{
		___U3CnodeU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t693557650, ___U24this_2)); }
	inline Gltf1Scene_t747889833 * get_U24this_2() const { return ___U24this_2; }
	inline Gltf1Scene_t747889833 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(Gltf1Scene_t747889833 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t693557650, ___U24current_3)); }
	inline GltfNodeBase_t3005766873 * get_U24current_3() const { return ___U24current_3; }
	inline GltfNodeBase_t3005766873 ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(GltfNodeBase_t3005766873 * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t693557650, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t693557650, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T693557650_H
#ifndef PBRMETALLICROUGHNESS_T2508879888_H
#define PBRMETALLICROUGHNESS_T2508879888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf2Material/PbrMetallicRoughness
struct  PbrMetallicRoughness_t2508879888  : public RuntimeObject
{
public:
	// UnityEngine.Color PolyToolkitInternal.Gltf2Material/PbrMetallicRoughness::baseColorFactor
	Color_t2555686324  ___baseColorFactor_0;
	// System.Single PolyToolkitInternal.Gltf2Material/PbrMetallicRoughness::metallicFactor
	float ___metallicFactor_1;
	// System.Single PolyToolkitInternal.Gltf2Material/PbrMetallicRoughness::roughnessFactor
	float ___roughnessFactor_2;
	// PolyToolkitInternal.Gltf2Material/TextureInfo PolyToolkitInternal.Gltf2Material/PbrMetallicRoughness::baseColorTexture
	TextureInfo_t2535341730 * ___baseColorTexture_3;
	// PolyToolkitInternal.Gltf2Material/TextureInfo PolyToolkitInternal.Gltf2Material/PbrMetallicRoughness::metallicRoughnessTexture
	TextureInfo_t2535341730 * ___metallicRoughnessTexture_4;

public:
	inline static int32_t get_offset_of_baseColorFactor_0() { return static_cast<int32_t>(offsetof(PbrMetallicRoughness_t2508879888, ___baseColorFactor_0)); }
	inline Color_t2555686324  get_baseColorFactor_0() const { return ___baseColorFactor_0; }
	inline Color_t2555686324 * get_address_of_baseColorFactor_0() { return &___baseColorFactor_0; }
	inline void set_baseColorFactor_0(Color_t2555686324  value)
	{
		___baseColorFactor_0 = value;
	}

	inline static int32_t get_offset_of_metallicFactor_1() { return static_cast<int32_t>(offsetof(PbrMetallicRoughness_t2508879888, ___metallicFactor_1)); }
	inline float get_metallicFactor_1() const { return ___metallicFactor_1; }
	inline float* get_address_of_metallicFactor_1() { return &___metallicFactor_1; }
	inline void set_metallicFactor_1(float value)
	{
		___metallicFactor_1 = value;
	}

	inline static int32_t get_offset_of_roughnessFactor_2() { return static_cast<int32_t>(offsetof(PbrMetallicRoughness_t2508879888, ___roughnessFactor_2)); }
	inline float get_roughnessFactor_2() const { return ___roughnessFactor_2; }
	inline float* get_address_of_roughnessFactor_2() { return &___roughnessFactor_2; }
	inline void set_roughnessFactor_2(float value)
	{
		___roughnessFactor_2 = value;
	}

	inline static int32_t get_offset_of_baseColorTexture_3() { return static_cast<int32_t>(offsetof(PbrMetallicRoughness_t2508879888, ___baseColorTexture_3)); }
	inline TextureInfo_t2535341730 * get_baseColorTexture_3() const { return ___baseColorTexture_3; }
	inline TextureInfo_t2535341730 ** get_address_of_baseColorTexture_3() { return &___baseColorTexture_3; }
	inline void set_baseColorTexture_3(TextureInfo_t2535341730 * value)
	{
		___baseColorTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___baseColorTexture_3), value);
	}

	inline static int32_t get_offset_of_metallicRoughnessTexture_4() { return static_cast<int32_t>(offsetof(PbrMetallicRoughness_t2508879888, ___metallicRoughnessTexture_4)); }
	inline TextureInfo_t2535341730 * get_metallicRoughnessTexture_4() const { return ___metallicRoughnessTexture_4; }
	inline TextureInfo_t2535341730 ** get_address_of_metallicRoughnessTexture_4() { return &___metallicRoughnessTexture_4; }
	inline void set_metallicRoughnessTexture_4(TextureInfo_t2535341730 * value)
	{
		___metallicRoughnessTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___metallicRoughnessTexture_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PBRMETALLICROUGHNESS_T2508879888_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef U3CU3EC__ITERATOR0_T251376496_H
#define U3CU3EC__ITERATOR0_T251376496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf1Node/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t251376496  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1/Enumerator<PolyToolkitInternal.Gltf1Node> PolyToolkitInternal.Gltf1Node/<>c__Iterator0::$locvar0
	Enumerator_t162136767  ___U24locvar0_0;
	// PolyToolkitInternal.Gltf1Node PolyToolkitInternal.Gltf1Node/<>c__Iterator0::<node>__1
	Gltf1Node_t1095785444 * ___U3CnodeU3E__1_1;
	// PolyToolkitInternal.Gltf1Node PolyToolkitInternal.Gltf1Node/<>c__Iterator0::$this
	Gltf1Node_t1095785444 * ___U24this_2;
	// PolyToolkitInternal.GltfNodeBase PolyToolkitInternal.Gltf1Node/<>c__Iterator0::$current
	GltfNodeBase_t3005766873 * ___U24current_3;
	// System.Boolean PolyToolkitInternal.Gltf1Node/<>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 PolyToolkitInternal.Gltf1Node/<>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t251376496, ___U24locvar0_0)); }
	inline Enumerator_t162136767  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t162136767 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t162136767  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CnodeU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t251376496, ___U3CnodeU3E__1_1)); }
	inline Gltf1Node_t1095785444 * get_U3CnodeU3E__1_1() const { return ___U3CnodeU3E__1_1; }
	inline Gltf1Node_t1095785444 ** get_address_of_U3CnodeU3E__1_1() { return &___U3CnodeU3E__1_1; }
	inline void set_U3CnodeU3E__1_1(Gltf1Node_t1095785444 * value)
	{
		___U3CnodeU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t251376496, ___U24this_2)); }
	inline Gltf1Node_t1095785444 * get_U24this_2() const { return ___U24this_2; }
	inline Gltf1Node_t1095785444 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(Gltf1Node_t1095785444 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t251376496, ___U24current_3)); }
	inline GltfNodeBase_t3005766873 * get_U24current_3() const { return ___U24current_3; }
	inline GltfNodeBase_t3005766873 ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(GltfNodeBase_t3005766873 * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t251376496, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t251376496, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T251376496_H
#ifndef PARSEASSETBACKGROUNDWORK_T3845740228_H
#define PARSEASSETBACKGROUNDWORK_T3845740228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.api_clients.poly_client.ParseAssetBackgroundWork
struct  ParseAssetBackgroundWork_t3845740228  : public RuntimeObject
{
public:
	// System.String PolyToolkitInternal.api_clients.poly_client.ParseAssetBackgroundWork::response
	String_t* ___response_0;
	// System.Action`2<PolyToolkit.PolyStatus,PolyToolkit.PolyAsset> PolyToolkitInternal.api_clients.poly_client.ParseAssetBackgroundWork::callback
	Action_2_t2738134841 * ___callback_1;
	// PolyToolkit.PolyStatus PolyToolkitInternal.api_clients.poly_client.ParseAssetBackgroundWork::status
	PolyStatus_t3145373940  ___status_2;
	// PolyToolkit.PolyAsset PolyToolkitInternal.api_clients.poly_client.ParseAssetBackgroundWork::polyAsset
	PolyAsset_t1814153511 * ___polyAsset_3;

public:
	inline static int32_t get_offset_of_response_0() { return static_cast<int32_t>(offsetof(ParseAssetBackgroundWork_t3845740228, ___response_0)); }
	inline String_t* get_response_0() const { return ___response_0; }
	inline String_t** get_address_of_response_0() { return &___response_0; }
	inline void set_response_0(String_t* value)
	{
		___response_0 = value;
		Il2CppCodeGenWriteBarrier((&___response_0), value);
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(ParseAssetBackgroundWork_t3845740228, ___callback_1)); }
	inline Action_2_t2738134841 * get_callback_1() const { return ___callback_1; }
	inline Action_2_t2738134841 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(Action_2_t2738134841 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___callback_1), value);
	}

	inline static int32_t get_offset_of_status_2() { return static_cast<int32_t>(offsetof(ParseAssetBackgroundWork_t3845740228, ___status_2)); }
	inline PolyStatus_t3145373940  get_status_2() const { return ___status_2; }
	inline PolyStatus_t3145373940 * get_address_of_status_2() { return &___status_2; }
	inline void set_status_2(PolyStatus_t3145373940  value)
	{
		___status_2 = value;
	}

	inline static int32_t get_offset_of_polyAsset_3() { return static_cast<int32_t>(offsetof(ParseAssetBackgroundWork_t3845740228, ___polyAsset_3)); }
	inline PolyAsset_t1814153511 * get_polyAsset_3() const { return ___polyAsset_3; }
	inline PolyAsset_t1814153511 ** get_address_of_polyAsset_3() { return &___polyAsset_3; }
	inline void set_polyAsset_3(PolyAsset_t1814153511 * value)
	{
		___polyAsset_3 = value;
		Il2CppCodeGenWriteBarrier((&___polyAsset_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSEASSETBACKGROUNDWORK_T3845740228_H
#ifndef U3CU3EC__ITERATOR0_T630382070_H
#define U3CU3EC__ITERATOR0_T630382070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf1Mesh/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t630382070  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1/Enumerator<PolyToolkitInternal.Gltf1Primitive> PolyToolkitInternal.Gltf1Mesh/<>c__Iterator0::$locvar0
	Enumerator_t2950219929  ___U24locvar0_0;
	// PolyToolkitInternal.Gltf1Primitive PolyToolkitInternal.Gltf1Mesh/<>c__Iterator0::<prim>__1
	Gltf1Primitive_t3883868606 * ___U3CprimU3E__1_1;
	// PolyToolkitInternal.Gltf1Mesh PolyToolkitInternal.Gltf1Mesh/<>c__Iterator0::$this
	Gltf1Mesh_t1560025 * ___U24this_2;
	// PolyToolkitInternal.GltfPrimitiveBase PolyToolkitInternal.Gltf1Mesh/<>c__Iterator0::$current
	GltfPrimitiveBase_t1810376431 * ___U24current_3;
	// System.Boolean PolyToolkitInternal.Gltf1Mesh/<>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 PolyToolkitInternal.Gltf1Mesh/<>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t630382070, ___U24locvar0_0)); }
	inline Enumerator_t2950219929  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t2950219929 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t2950219929  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CprimU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t630382070, ___U3CprimU3E__1_1)); }
	inline Gltf1Primitive_t3883868606 * get_U3CprimU3E__1_1() const { return ___U3CprimU3E__1_1; }
	inline Gltf1Primitive_t3883868606 ** get_address_of_U3CprimU3E__1_1() { return &___U3CprimU3E__1_1; }
	inline void set_U3CprimU3E__1_1(Gltf1Primitive_t3883868606 * value)
	{
		___U3CprimU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CprimU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t630382070, ___U24this_2)); }
	inline Gltf1Mesh_t1560025 * get_U24this_2() const { return ___U24this_2; }
	inline Gltf1Mesh_t1560025 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(Gltf1Mesh_t1560025 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t630382070, ___U24current_3)); }
	inline GltfPrimitiveBase_t1810376431 * get_U24current_3() const { return ___U24current_3; }
	inline GltfPrimitiveBase_t1810376431 ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(GltfPrimitiveBase_t1810376431 * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t630382070, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t630382070, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T630382070_H
#ifndef U3CU3EC__ITERATOR0_T654162957_H
#define U3CU3EC__ITERATOR0_T654162957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf2Mesh/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t654162957  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1/Enumerator<PolyToolkitInternal.Gltf2Primitive> PolyToolkitInternal.Gltf2Mesh/<>c__Iterator0::$locvar0
	Enumerator_t2950218968  ___U24locvar0_0;
	// PolyToolkitInternal.Gltf2Primitive PolyToolkitInternal.Gltf2Mesh/<>c__Iterator0::<prim>__1
	Gltf2Primitive_t3883867645 * ___U3CprimU3E__1_1;
	// PolyToolkitInternal.Gltf2Mesh PolyToolkitInternal.Gltf2Mesh/<>c__Iterator0::$this
	Gltf2Mesh_t1559994 * ___U24this_2;
	// PolyToolkitInternal.GltfPrimitiveBase PolyToolkitInternal.Gltf2Mesh/<>c__Iterator0::$current
	GltfPrimitiveBase_t1810376431 * ___U24current_3;
	// System.Boolean PolyToolkitInternal.Gltf2Mesh/<>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 PolyToolkitInternal.Gltf2Mesh/<>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t654162957, ___U24locvar0_0)); }
	inline Enumerator_t2950218968  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t2950218968 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t2950218968  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CprimU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t654162957, ___U3CprimU3E__1_1)); }
	inline Gltf2Primitive_t3883867645 * get_U3CprimU3E__1_1() const { return ___U3CprimU3E__1_1; }
	inline Gltf2Primitive_t3883867645 ** get_address_of_U3CprimU3E__1_1() { return &___U3CprimU3E__1_1; }
	inline void set_U3CprimU3E__1_1(Gltf2Primitive_t3883867645 * value)
	{
		___U3CprimU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CprimU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t654162957, ___U24this_2)); }
	inline Gltf2Mesh_t1559994 * get_U24this_2() const { return ___U24this_2; }
	inline Gltf2Mesh_t1559994 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(Gltf2Mesh_t1559994 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t654162957, ___U24current_3)); }
	inline GltfPrimitiveBase_t1810376431 * get_U24current_3() const { return ___U24current_3; }
	inline GltfPrimitiveBase_t1810376431 ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(GltfPrimitiveBase_t1810376431 * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t654162957, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t654162957, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T654162957_H
#ifndef GLTFSCHEMAVERSION_T4047415476_H
#define GLTFSCHEMAVERSION_T4047415476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfSchemaVersion
struct  GltfSchemaVersion_t4047415476 
{
public:
	// System.Int32 PolyToolkitInternal.GltfSchemaVersion::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GltfSchemaVersion_t4047415476, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFSCHEMAVERSION_T4047415476_H
#ifndef U3CU3EC__ITERATOR0_T1227606519_H
#define U3CU3EC__ITERATOR0_T1227606519_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf2Scene/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t1227606519  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1/Enumerator<PolyToolkitInternal.Gltf2Node> PolyToolkitInternal.Gltf2Scene/<>c__Iterator0::$locvar0
	Enumerator_t162136734  ___U24locvar0_0;
	// PolyToolkitInternal.Gltf2Node PolyToolkitInternal.Gltf2Scene/<>c__Iterator0::<node>__1
	Gltf2Node_t1095785411 * ___U3CnodeU3E__1_1;
	// PolyToolkitInternal.Gltf2Scene PolyToolkitInternal.Gltf2Scene/<>c__Iterator0::$this
	Gltf2Scene_t747889802 * ___U24this_2;
	// PolyToolkitInternal.GltfNodeBase PolyToolkitInternal.Gltf2Scene/<>c__Iterator0::$current
	GltfNodeBase_t3005766873 * ___U24current_3;
	// System.Boolean PolyToolkitInternal.Gltf2Scene/<>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 PolyToolkitInternal.Gltf2Scene/<>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1227606519, ___U24locvar0_0)); }
	inline Enumerator_t162136734  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t162136734 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t162136734  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CnodeU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1227606519, ___U3CnodeU3E__1_1)); }
	inline Gltf2Node_t1095785411 * get_U3CnodeU3E__1_1() const { return ___U3CnodeU3E__1_1; }
	inline Gltf2Node_t1095785411 ** get_address_of_U3CnodeU3E__1_1() { return &___U3CnodeU3E__1_1; }
	inline void set_U3CnodeU3E__1_1(Gltf2Node_t1095785411 * value)
	{
		___U3CnodeU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1227606519, ___U24this_2)); }
	inline Gltf2Scene_t747889802 * get_U24this_2() const { return ___U24this_2; }
	inline Gltf2Scene_t747889802 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(Gltf2Scene_t747889802 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1227606519, ___U24current_3)); }
	inline GltfNodeBase_t3005766873 * get_U24current_3() const { return ___U24current_3; }
	inline GltfNodeBase_t3005766873 ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(GltfNodeBase_t3005766873 * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1227606519, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t1227606519, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T1227606519_H
#ifndef DISABLEDPROPERTYATTRIBUTE_T3486517706_H
#define DISABLEDPROPERTYATTRIBUTE_T3486517706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.DisabledPropertyAttribute
struct  DisabledPropertyAttribute_t3486517706  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEDPROPERTYATTRIBUTE_T3486517706_H
#ifndef U3CU3EC__ITERATOR0_T266980529_H
#define U3CU3EC__ITERATOR0_T266980529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf2Node/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t266980529  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1/Enumerator<PolyToolkitInternal.Gltf2Node> PolyToolkitInternal.Gltf2Node/<>c__Iterator0::$locvar0
	Enumerator_t162136734  ___U24locvar0_0;
	// PolyToolkitInternal.Gltf2Node PolyToolkitInternal.Gltf2Node/<>c__Iterator0::<node>__1
	Gltf2Node_t1095785411 * ___U3CnodeU3E__1_1;
	// PolyToolkitInternal.Gltf2Node PolyToolkitInternal.Gltf2Node/<>c__Iterator0::$this
	Gltf2Node_t1095785411 * ___U24this_2;
	// PolyToolkitInternal.GltfNodeBase PolyToolkitInternal.Gltf2Node/<>c__Iterator0::$current
	GltfNodeBase_t3005766873 * ___U24current_3;
	// System.Boolean PolyToolkitInternal.Gltf2Node/<>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 PolyToolkitInternal.Gltf2Node/<>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t266980529, ___U24locvar0_0)); }
	inline Enumerator_t162136734  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t162136734 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t162136734  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CnodeU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t266980529, ___U3CnodeU3E__1_1)); }
	inline Gltf2Node_t1095785411 * get_U3CnodeU3E__1_1() const { return ___U3CnodeU3E__1_1; }
	inline Gltf2Node_t1095785411 ** get_address_of_U3CnodeU3E__1_1() { return &___U3CnodeU3E__1_1; }
	inline void set_U3CnodeU3E__1_1(Gltf2Node_t1095785411 * value)
	{
		___U3CnodeU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnodeU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t266980529, ___U24this_2)); }
	inline Gltf2Node_t1095785411 * get_U24this_2() const { return ___U24this_2; }
	inline Gltf2Node_t1095785411 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(Gltf2Node_t1095785411 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t266980529, ___U24current_3)); }
	inline GltfNodeBase_t3005766873 * get_U24current_3() const { return ___U24current_3; }
	inline GltfNodeBase_t3005766873 ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(GltfNodeBase_t3005766873 * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t266980529, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t266980529, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T266980529_H
#ifndef PARSEASSETSBACKGROUNDWORK_T2464360963_H
#define PARSEASSETSBACKGROUNDWORK_T2464360963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.api_clients.poly_client.ParseAssetsBackgroundWork
struct  ParseAssetsBackgroundWork_t2464360963  : public RuntimeObject
{
public:
	// System.String PolyToolkitInternal.api_clients.poly_client.ParseAssetsBackgroundWork::response
	String_t* ___response_0;
	// PolyToolkit.PolyStatus PolyToolkitInternal.api_clients.poly_client.ParseAssetsBackgroundWork::status
	PolyStatus_t3145373940  ___status_1;
	// System.Action`2<PolyToolkit.PolyStatus,PolyToolkit.PolyListAssetsResult> PolyToolkitInternal.api_clients.poly_client.ParseAssetsBackgroundWork::callback
	Action_2_t1974849383 * ___callback_2;
	// PolyToolkit.PolyListAssetsResult PolyToolkitInternal.api_clients.poly_client.ParseAssetsBackgroundWork::polyListAssetsResult
	PolyListAssetsResult_t1050868053 * ___polyListAssetsResult_3;

public:
	inline static int32_t get_offset_of_response_0() { return static_cast<int32_t>(offsetof(ParseAssetsBackgroundWork_t2464360963, ___response_0)); }
	inline String_t* get_response_0() const { return ___response_0; }
	inline String_t** get_address_of_response_0() { return &___response_0; }
	inline void set_response_0(String_t* value)
	{
		___response_0 = value;
		Il2CppCodeGenWriteBarrier((&___response_0), value);
	}

	inline static int32_t get_offset_of_status_1() { return static_cast<int32_t>(offsetof(ParseAssetsBackgroundWork_t2464360963, ___status_1)); }
	inline PolyStatus_t3145373940  get_status_1() const { return ___status_1; }
	inline PolyStatus_t3145373940 * get_address_of_status_1() { return &___status_1; }
	inline void set_status_1(PolyStatus_t3145373940  value)
	{
		___status_1 = value;
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(ParseAssetsBackgroundWork_t2464360963, ___callback_2)); }
	inline Action_2_t1974849383 * get_callback_2() const { return ___callback_2; }
	inline Action_2_t1974849383 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Action_2_t1974849383 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}

	inline static int32_t get_offset_of_polyListAssetsResult_3() { return static_cast<int32_t>(offsetof(ParseAssetsBackgroundWork_t2464360963, ___polyListAssetsResult_3)); }
	inline PolyListAssetsResult_t1050868053 * get_polyListAssetsResult_3() const { return ___polyListAssetsResult_3; }
	inline PolyListAssetsResult_t1050868053 ** get_address_of_polyListAssetsResult_3() { return &___polyListAssetsResult_3; }
	inline void set_polyListAssetsResult_3(PolyListAssetsResult_t1050868053 * value)
	{
		___polyListAssetsResult_3 = value;
		Il2CppCodeGenWriteBarrier((&___polyListAssetsResult_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSEASSETSBACKGROUNDWORK_T2464360963_H
#ifndef REQUESTTYPE_T2113262704_H
#define REQUESTTYPE_T2113262704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.caching.PersistentBlobCache/RequestType
struct  RequestType_t2113262704 
{
public:
	// System.Int32 PolyToolkitInternal.caching.PersistentBlobCache/RequestType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RequestType_t2113262704, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTTYPE_T2113262704_H
#ifndef POLYIMPORTOPTIONS_T4213423452_H
#define POLYIMPORTOPTIONS_T4213423452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkit.PolyImportOptions
struct  PolyImportOptions_t4213423452 
{
public:
	// PolyToolkit.PolyImportOptions/RescalingMode PolyToolkit.PolyImportOptions::rescalingMode
	int32_t ___rescalingMode_0;
	// System.Single PolyToolkit.PolyImportOptions::scaleFactor
	float ___scaleFactor_1;
	// System.Single PolyToolkit.PolyImportOptions::desiredSize
	float ___desiredSize_2;
	// System.Boolean PolyToolkit.PolyImportOptions::recenter
	bool ___recenter_3;
	// System.Boolean PolyToolkit.PolyImportOptions::clientThrottledMainThread
	bool ___clientThrottledMainThread_4;

public:
	inline static int32_t get_offset_of_rescalingMode_0() { return static_cast<int32_t>(offsetof(PolyImportOptions_t4213423452, ___rescalingMode_0)); }
	inline int32_t get_rescalingMode_0() const { return ___rescalingMode_0; }
	inline int32_t* get_address_of_rescalingMode_0() { return &___rescalingMode_0; }
	inline void set_rescalingMode_0(int32_t value)
	{
		___rescalingMode_0 = value;
	}

	inline static int32_t get_offset_of_scaleFactor_1() { return static_cast<int32_t>(offsetof(PolyImportOptions_t4213423452, ___scaleFactor_1)); }
	inline float get_scaleFactor_1() const { return ___scaleFactor_1; }
	inline float* get_address_of_scaleFactor_1() { return &___scaleFactor_1; }
	inline void set_scaleFactor_1(float value)
	{
		___scaleFactor_1 = value;
	}

	inline static int32_t get_offset_of_desiredSize_2() { return static_cast<int32_t>(offsetof(PolyImportOptions_t4213423452, ___desiredSize_2)); }
	inline float get_desiredSize_2() const { return ___desiredSize_2; }
	inline float* get_address_of_desiredSize_2() { return &___desiredSize_2; }
	inline void set_desiredSize_2(float value)
	{
		___desiredSize_2 = value;
	}

	inline static int32_t get_offset_of_recenter_3() { return static_cast<int32_t>(offsetof(PolyImportOptions_t4213423452, ___recenter_3)); }
	inline bool get_recenter_3() const { return ___recenter_3; }
	inline bool* get_address_of_recenter_3() { return &___recenter_3; }
	inline void set_recenter_3(bool value)
	{
		___recenter_3 = value;
	}

	inline static int32_t get_offset_of_clientThrottledMainThread_4() { return static_cast<int32_t>(offsetof(PolyImportOptions_t4213423452, ___clientThrottledMainThread_4)); }
	inline bool get_clientThrottledMainThread_4() const { return ___clientThrottledMainThread_4; }
	inline bool* get_address_of_clientThrottledMainThread_4() { return &___clientThrottledMainThread_4; }
	inline void set_clientThrottledMainThread_4(bool value)
	{
		___clientThrottledMainThread_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PolyToolkit.PolyImportOptions
struct PolyImportOptions_t4213423452_marshaled_pinvoke
{
	int32_t ___rescalingMode_0;
	float ___scaleFactor_1;
	float ___desiredSize_2;
	int32_t ___recenter_3;
	int32_t ___clientThrottledMainThread_4;
};
// Native definition for COM marshalling of PolyToolkit.PolyImportOptions
struct PolyImportOptions_t4213423452_marshaled_com
{
	int32_t ___rescalingMode_0;
	float ___scaleFactor_1;
	float ___desiredSize_2;
	int32_t ___recenter_3;
	int32_t ___clientThrottledMainThread_4;
};
#endif // POLYIMPORTOPTIONS_T4213423452_H
#ifndef TILTBRUSHGLTF1PBRVALUES_T1054533882_H
#define TILTBRUSHGLTF1PBRVALUES_T1054533882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.TiltBrushGltf1PbrValues
struct  TiltBrushGltf1PbrValues_t1054533882  : public RuntimeObject
{
public:
	// System.Nullable`1<UnityEngine.Color> PolyToolkitInternal.TiltBrushGltf1PbrValues::BaseColorFactor
	Nullable_1_t4278248406  ___BaseColorFactor_0;
	// System.Nullable`1<System.Single> PolyToolkitInternal.TiltBrushGltf1PbrValues::MetallicFactor
	Nullable_1_t3119828856  ___MetallicFactor_1;
	// System.Nullable`1<System.Single> PolyToolkitInternal.TiltBrushGltf1PbrValues::RoughnessFactor
	Nullable_1_t3119828856  ___RoughnessFactor_2;
	// System.String PolyToolkitInternal.TiltBrushGltf1PbrValues::BaseColorTex
	String_t* ___BaseColorTex_3;
	// PolyToolkitInternal.Gltf1Texture PolyToolkitInternal.TiltBrushGltf1PbrValues::BaseColorTexPtr
	Gltf1Texture_t3945546188 * ___BaseColorTexPtr_4;

public:
	inline static int32_t get_offset_of_BaseColorFactor_0() { return static_cast<int32_t>(offsetof(TiltBrushGltf1PbrValues_t1054533882, ___BaseColorFactor_0)); }
	inline Nullable_1_t4278248406  get_BaseColorFactor_0() const { return ___BaseColorFactor_0; }
	inline Nullable_1_t4278248406 * get_address_of_BaseColorFactor_0() { return &___BaseColorFactor_0; }
	inline void set_BaseColorFactor_0(Nullable_1_t4278248406  value)
	{
		___BaseColorFactor_0 = value;
	}

	inline static int32_t get_offset_of_MetallicFactor_1() { return static_cast<int32_t>(offsetof(TiltBrushGltf1PbrValues_t1054533882, ___MetallicFactor_1)); }
	inline Nullable_1_t3119828856  get_MetallicFactor_1() const { return ___MetallicFactor_1; }
	inline Nullable_1_t3119828856 * get_address_of_MetallicFactor_1() { return &___MetallicFactor_1; }
	inline void set_MetallicFactor_1(Nullable_1_t3119828856  value)
	{
		___MetallicFactor_1 = value;
	}

	inline static int32_t get_offset_of_RoughnessFactor_2() { return static_cast<int32_t>(offsetof(TiltBrushGltf1PbrValues_t1054533882, ___RoughnessFactor_2)); }
	inline Nullable_1_t3119828856  get_RoughnessFactor_2() const { return ___RoughnessFactor_2; }
	inline Nullable_1_t3119828856 * get_address_of_RoughnessFactor_2() { return &___RoughnessFactor_2; }
	inline void set_RoughnessFactor_2(Nullable_1_t3119828856  value)
	{
		___RoughnessFactor_2 = value;
	}

	inline static int32_t get_offset_of_BaseColorTex_3() { return static_cast<int32_t>(offsetof(TiltBrushGltf1PbrValues_t1054533882, ___BaseColorTex_3)); }
	inline String_t* get_BaseColorTex_3() const { return ___BaseColorTex_3; }
	inline String_t** get_address_of_BaseColorTex_3() { return &___BaseColorTex_3; }
	inline void set_BaseColorTex_3(String_t* value)
	{
		___BaseColorTex_3 = value;
		Il2CppCodeGenWriteBarrier((&___BaseColorTex_3), value);
	}

	inline static int32_t get_offset_of_BaseColorTexPtr_4() { return static_cast<int32_t>(offsetof(TiltBrushGltf1PbrValues_t1054533882, ___BaseColorTexPtr_4)); }
	inline Gltf1Texture_t3945546188 * get_BaseColorTexPtr_4() const { return ___BaseColorTexPtr_4; }
	inline Gltf1Texture_t3945546188 ** get_address_of_BaseColorTexPtr_4() { return &___BaseColorTexPtr_4; }
	inline void set_BaseColorTexPtr_4(Gltf1Texture_t3945546188 * value)
	{
		___BaseColorTexPtr_4 = value;
		Il2CppCodeGenWriteBarrier((&___BaseColorTexPtr_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILTBRUSHGLTF1PBRVALUES_T1054533882_H
#ifndef GLTFNODEBASE_T3005766873_H
#define GLTFNODEBASE_T3005766873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfNodeBase
struct  GltfNodeBase_t3005766873  : public RuntimeObject
{
public:
	// System.String PolyToolkitInternal.GltfNodeBase::name
	String_t* ___name_0;
	// System.Nullable`1<UnityEngine.Matrix4x4> PolyToolkitInternal.GltfNodeBase::matrix
	Nullable_1_t3540463925  ___matrix_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(GltfNodeBase_t3005766873, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_matrix_1() { return static_cast<int32_t>(offsetof(GltfNodeBase_t3005766873, ___matrix_1)); }
	inline Nullable_1_t3540463925  get_matrix_1() const { return ___matrix_1; }
	inline Nullable_1_t3540463925 * get_address_of_matrix_1() { return &___matrix_1; }
	inline void set_matrix_1(Nullable_1_t3540463925  value)
	{
		___matrix_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFNODEBASE_T3005766873_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GLTFROOTBASE_T3065879770_H
#define GLTFROOTBASE_T3065879770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfRootBase
struct  GltfRootBase_t3065879770  : public RuntimeObject
{
public:
	// PolyToolkitInternal.GltfAsset PolyToolkitInternal.GltfRootBase::asset
	GltfAsset_t1389371343 * ___asset_0;
	// System.Nullable`1<PolyToolkitInternal.Version> PolyToolkitInternal.GltfRootBase::tiltBrushVersion
	Nullable_1_t2446704234  ___tiltBrushVersion_1;
	// System.Nullable`1<PolyToolkitInternal.Version> PolyToolkitInternal.GltfRootBase::blocksVersion
	Nullable_1_t2446704234  ___blocksVersion_2;

public:
	inline static int32_t get_offset_of_asset_0() { return static_cast<int32_t>(offsetof(GltfRootBase_t3065879770, ___asset_0)); }
	inline GltfAsset_t1389371343 * get_asset_0() const { return ___asset_0; }
	inline GltfAsset_t1389371343 ** get_address_of_asset_0() { return &___asset_0; }
	inline void set_asset_0(GltfAsset_t1389371343 * value)
	{
		___asset_0 = value;
		Il2CppCodeGenWriteBarrier((&___asset_0), value);
	}

	inline static int32_t get_offset_of_tiltBrushVersion_1() { return static_cast<int32_t>(offsetof(GltfRootBase_t3065879770, ___tiltBrushVersion_1)); }
	inline Nullable_1_t2446704234  get_tiltBrushVersion_1() const { return ___tiltBrushVersion_1; }
	inline Nullable_1_t2446704234 * get_address_of_tiltBrushVersion_1() { return &___tiltBrushVersion_1; }
	inline void set_tiltBrushVersion_1(Nullable_1_t2446704234  value)
	{
		___tiltBrushVersion_1 = value;
	}

	inline static int32_t get_offset_of_blocksVersion_2() { return static_cast<int32_t>(offsetof(GltfRootBase_t3065879770, ___blocksVersion_2)); }
	inline Nullable_1_t2446704234  get_blocksVersion_2() const { return ___blocksVersion_2; }
	inline Nullable_1_t2446704234 * get_address_of_blocksVersion_2() { return &___blocksVersion_2; }
	inline void set_blocksVersion_2(Nullable_1_t2446704234  value)
	{
		___blocksVersion_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFROOTBASE_T3065879770_H
#ifndef GLTFACCESSORBASE_T2108002982_H
#define GLTFACCESSORBASE_T2108002982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfAccessorBase
struct  GltfAccessorBase_t2108002982  : public RuntimeObject
{
public:
	// System.Int32 PolyToolkitInternal.GltfAccessorBase::byteOffset
	int32_t ___byteOffset_0;
	// System.Int32 PolyToolkitInternal.GltfAccessorBase::byteStride
	int32_t ___byteStride_1;
	// PolyToolkitInternal.GltfAccessorBase/ComponentType PolyToolkitInternal.GltfAccessorBase::componentType
	int32_t ___componentType_2;
	// System.Int32 PolyToolkitInternal.GltfAccessorBase::count
	int32_t ___count_3;
	// System.Collections.Generic.List`1<System.Single> PolyToolkitInternal.GltfAccessorBase::max
	List_1_t2869341516 * ___max_4;
	// System.Collections.Generic.List`1<System.Single> PolyToolkitInternal.GltfAccessorBase::min
	List_1_t2869341516 * ___min_5;
	// System.String PolyToolkitInternal.GltfAccessorBase::type
	String_t* ___type_6;

public:
	inline static int32_t get_offset_of_byteOffset_0() { return static_cast<int32_t>(offsetof(GltfAccessorBase_t2108002982, ___byteOffset_0)); }
	inline int32_t get_byteOffset_0() const { return ___byteOffset_0; }
	inline int32_t* get_address_of_byteOffset_0() { return &___byteOffset_0; }
	inline void set_byteOffset_0(int32_t value)
	{
		___byteOffset_0 = value;
	}

	inline static int32_t get_offset_of_byteStride_1() { return static_cast<int32_t>(offsetof(GltfAccessorBase_t2108002982, ___byteStride_1)); }
	inline int32_t get_byteStride_1() const { return ___byteStride_1; }
	inline int32_t* get_address_of_byteStride_1() { return &___byteStride_1; }
	inline void set_byteStride_1(int32_t value)
	{
		___byteStride_1 = value;
	}

	inline static int32_t get_offset_of_componentType_2() { return static_cast<int32_t>(offsetof(GltfAccessorBase_t2108002982, ___componentType_2)); }
	inline int32_t get_componentType_2() const { return ___componentType_2; }
	inline int32_t* get_address_of_componentType_2() { return &___componentType_2; }
	inline void set_componentType_2(int32_t value)
	{
		___componentType_2 = value;
	}

	inline static int32_t get_offset_of_count_3() { return static_cast<int32_t>(offsetof(GltfAccessorBase_t2108002982, ___count_3)); }
	inline int32_t get_count_3() const { return ___count_3; }
	inline int32_t* get_address_of_count_3() { return &___count_3; }
	inline void set_count_3(int32_t value)
	{
		___count_3 = value;
	}

	inline static int32_t get_offset_of_max_4() { return static_cast<int32_t>(offsetof(GltfAccessorBase_t2108002982, ___max_4)); }
	inline List_1_t2869341516 * get_max_4() const { return ___max_4; }
	inline List_1_t2869341516 ** get_address_of_max_4() { return &___max_4; }
	inline void set_max_4(List_1_t2869341516 * value)
	{
		___max_4 = value;
		Il2CppCodeGenWriteBarrier((&___max_4), value);
	}

	inline static int32_t get_offset_of_min_5() { return static_cast<int32_t>(offsetof(GltfAccessorBase_t2108002982, ___min_5)); }
	inline List_1_t2869341516 * get_min_5() const { return ___min_5; }
	inline List_1_t2869341516 ** get_address_of_min_5() { return &___min_5; }
	inline void set_min_5(List_1_t2869341516 * value)
	{
		___min_5 = value;
		Il2CppCodeGenWriteBarrier((&___min_5), value);
	}

	inline static int32_t get_offset_of_type_6() { return static_cast<int32_t>(offsetof(GltfAccessorBase_t2108002982, ___type_6)); }
	inline String_t* get_type_6() const { return ___type_6; }
	inline String_t** get_address_of_type_6() { return &___type_6; }
	inline void set_type_6(String_t* value)
	{
		___type_6 = value;
		Il2CppCodeGenWriteBarrier((&___type_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFACCESSORBASE_T2108002982_H
#ifndef CACHEREQUEST_T1922999082_H
#define CACHEREQUEST_T1922999082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.caching.PersistentBlobCache/CacheRequest
struct  CacheRequest_t1922999082  : public RuntimeObject
{
public:
	// PolyToolkitInternal.caching.PersistentBlobCache/RequestType PolyToolkitInternal.caching.PersistentBlobCache/CacheRequest::type
	int32_t ___type_0;
	// System.String PolyToolkitInternal.caching.PersistentBlobCache/CacheRequest::key
	String_t* ___key_1;
	// System.String PolyToolkitInternal.caching.PersistentBlobCache/CacheRequest::hash
	String_t* ___hash_2;
	// PolyToolkitInternal.caching.PersistentBlobCache/CacheReadCallback PolyToolkitInternal.caching.PersistentBlobCache/CacheRequest::readCallback
	CacheReadCallback_t2055645553 * ___readCallback_3;
	// System.Byte[] PolyToolkitInternal.caching.PersistentBlobCache/CacheRequest::data
	ByteU5BU5D_t4116647657* ___data_4;
	// System.Int64 PolyToolkitInternal.caching.PersistentBlobCache/CacheRequest::maxAgeMillis
	int64_t ___maxAgeMillis_5;
	// System.Boolean PolyToolkitInternal.caching.PersistentBlobCache/CacheRequest::success
	bool ___success_6;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(CacheRequest_t1922999082, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_key_1() { return static_cast<int32_t>(offsetof(CacheRequest_t1922999082, ___key_1)); }
	inline String_t* get_key_1() const { return ___key_1; }
	inline String_t** get_address_of_key_1() { return &___key_1; }
	inline void set_key_1(String_t* value)
	{
		___key_1 = value;
		Il2CppCodeGenWriteBarrier((&___key_1), value);
	}

	inline static int32_t get_offset_of_hash_2() { return static_cast<int32_t>(offsetof(CacheRequest_t1922999082, ___hash_2)); }
	inline String_t* get_hash_2() const { return ___hash_2; }
	inline String_t** get_address_of_hash_2() { return &___hash_2; }
	inline void set_hash_2(String_t* value)
	{
		___hash_2 = value;
		Il2CppCodeGenWriteBarrier((&___hash_2), value);
	}

	inline static int32_t get_offset_of_readCallback_3() { return static_cast<int32_t>(offsetof(CacheRequest_t1922999082, ___readCallback_3)); }
	inline CacheReadCallback_t2055645553 * get_readCallback_3() const { return ___readCallback_3; }
	inline CacheReadCallback_t2055645553 ** get_address_of_readCallback_3() { return &___readCallback_3; }
	inline void set_readCallback_3(CacheReadCallback_t2055645553 * value)
	{
		___readCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___readCallback_3), value);
	}

	inline static int32_t get_offset_of_data_4() { return static_cast<int32_t>(offsetof(CacheRequest_t1922999082, ___data_4)); }
	inline ByteU5BU5D_t4116647657* get_data_4() const { return ___data_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_data_4() { return &___data_4; }
	inline void set_data_4(ByteU5BU5D_t4116647657* value)
	{
		___data_4 = value;
		Il2CppCodeGenWriteBarrier((&___data_4), value);
	}

	inline static int32_t get_offset_of_maxAgeMillis_5() { return static_cast<int32_t>(offsetof(CacheRequest_t1922999082, ___maxAgeMillis_5)); }
	inline int64_t get_maxAgeMillis_5() const { return ___maxAgeMillis_5; }
	inline int64_t* get_address_of_maxAgeMillis_5() { return &___maxAgeMillis_5; }
	inline void set_maxAgeMillis_5(int64_t value)
	{
		___maxAgeMillis_5 = value;
	}

	inline static int32_t get_offset_of_success_6() { return static_cast<int32_t>(offsetof(CacheRequest_t1922999082, ___success_6)); }
	inline bool get_success_6() const { return ___success_6; }
	inline bool* get_address_of_success_6() { return &___success_6; }
	inline void set_success_6(bool value)
	{
		___success_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEREQUEST_T1922999082_H
#ifndef GLTFPRIMITIVEBASE_T1810376431_H
#define GLTFPRIMITIVEBASE_T1810376431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfPrimitiveBase
struct  GltfPrimitiveBase_t1810376431  : public RuntimeObject
{
public:
	// PolyToolkitInternal.GltfPrimitiveBase/Mode PolyToolkitInternal.GltfPrimitiveBase::mode
	int32_t ___mode_0;
	// System.Collections.Generic.List`1<PolyToolkitInternal.MeshPrecursor> PolyToolkitInternal.GltfPrimitiveBase::precursorMeshes
	List_1_t1895342774 * ___precursorMeshes_1;
	// System.Collections.Generic.List`1<UnityEngine.Mesh> PolyToolkitInternal.GltfPrimitiveBase::unityMeshes
	List_1_t826071730 * ___unityMeshes_2;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(GltfPrimitiveBase_t1810376431, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_precursorMeshes_1() { return static_cast<int32_t>(offsetof(GltfPrimitiveBase_t1810376431, ___precursorMeshes_1)); }
	inline List_1_t1895342774 * get_precursorMeshes_1() const { return ___precursorMeshes_1; }
	inline List_1_t1895342774 ** get_address_of_precursorMeshes_1() { return &___precursorMeshes_1; }
	inline void set_precursorMeshes_1(List_1_t1895342774 * value)
	{
		___precursorMeshes_1 = value;
		Il2CppCodeGenWriteBarrier((&___precursorMeshes_1), value);
	}

	inline static int32_t get_offset_of_unityMeshes_2() { return static_cast<int32_t>(offsetof(GltfPrimitiveBase_t1810376431, ___unityMeshes_2)); }
	inline List_1_t826071730 * get_unityMeshes_2() const { return ___unityMeshes_2; }
	inline List_1_t826071730 ** get_address_of_unityMeshes_2() { return &___unityMeshes_2; }
	inline void set_unityMeshes_2(List_1_t826071730 * value)
	{
		___unityMeshes_2 = value;
		Il2CppCodeGenWriteBarrier((&___unityMeshes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFPRIMITIVEBASE_T1810376431_H
#ifndef DEFAULTCONTRACTRESOLVER_T270250618_H
#define DEFAULTCONTRACTRESOLVER_T270250618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver
struct  DefaultContractResolver_t270250618  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.DefaultContractResolverState Newtonsoft.Json.Serialization.DefaultContractResolver::_instanceState
	DefaultContractResolverState_t3045992855 * ____instanceState_4;
	// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::_sharedCache
	bool ____sharedCache_5;
	// System.Reflection.BindingFlags Newtonsoft.Json.Serialization.DefaultContractResolver::<DefaultMembersSearchFlags>k__BackingField
	int32_t ___U3CDefaultMembersSearchFlagsU3Ek__BackingField_6;
	// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::<SerializeCompilerGeneratedMembers>k__BackingField
	bool ___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7;
	// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::<IgnoreSerializableInterface>k__BackingField
	bool ___U3CIgnoreSerializableInterfaceU3Ek__BackingField_8;
	// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::<IgnoreSerializableAttribute>k__BackingField
	bool ___U3CIgnoreSerializableAttributeU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of__instanceState_4() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t270250618, ____instanceState_4)); }
	inline DefaultContractResolverState_t3045992855 * get__instanceState_4() const { return ____instanceState_4; }
	inline DefaultContractResolverState_t3045992855 ** get_address_of__instanceState_4() { return &____instanceState_4; }
	inline void set__instanceState_4(DefaultContractResolverState_t3045992855 * value)
	{
		____instanceState_4 = value;
		Il2CppCodeGenWriteBarrier((&____instanceState_4), value);
	}

	inline static int32_t get_offset_of__sharedCache_5() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t270250618, ____sharedCache_5)); }
	inline bool get__sharedCache_5() const { return ____sharedCache_5; }
	inline bool* get_address_of__sharedCache_5() { return &____sharedCache_5; }
	inline void set__sharedCache_5(bool value)
	{
		____sharedCache_5 = value;
	}

	inline static int32_t get_offset_of_U3CDefaultMembersSearchFlagsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t270250618, ___U3CDefaultMembersSearchFlagsU3Ek__BackingField_6)); }
	inline int32_t get_U3CDefaultMembersSearchFlagsU3Ek__BackingField_6() const { return ___U3CDefaultMembersSearchFlagsU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CDefaultMembersSearchFlagsU3Ek__BackingField_6() { return &___U3CDefaultMembersSearchFlagsU3Ek__BackingField_6; }
	inline void set_U3CDefaultMembersSearchFlagsU3Ek__BackingField_6(int32_t value)
	{
		___U3CDefaultMembersSearchFlagsU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t270250618, ___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7)); }
	inline bool get_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7() const { return ___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7() { return &___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7; }
	inline void set_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7(bool value)
	{
		___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CIgnoreSerializableInterfaceU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t270250618, ___U3CIgnoreSerializableInterfaceU3Ek__BackingField_8)); }
	inline bool get_U3CIgnoreSerializableInterfaceU3Ek__BackingField_8() const { return ___U3CIgnoreSerializableInterfaceU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIgnoreSerializableInterfaceU3Ek__BackingField_8() { return &___U3CIgnoreSerializableInterfaceU3Ek__BackingField_8; }
	inline void set_U3CIgnoreSerializableInterfaceU3Ek__BackingField_8(bool value)
	{
		___U3CIgnoreSerializableInterfaceU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CIgnoreSerializableAttributeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t270250618, ___U3CIgnoreSerializableAttributeU3Ek__BackingField_9)); }
	inline bool get_U3CIgnoreSerializableAttributeU3Ek__BackingField_9() const { return ___U3CIgnoreSerializableAttributeU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CIgnoreSerializableAttributeU3Ek__BackingField_9() { return &___U3CIgnoreSerializableAttributeU3Ek__BackingField_9; }
	inline void set_U3CIgnoreSerializableAttributeU3Ek__BackingField_9(bool value)
	{
		___U3CIgnoreSerializableAttributeU3Ek__BackingField_9 = value;
	}
};

struct DefaultContractResolver_t270250618_StaticFields
{
public:
	// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.Serialization.DefaultContractResolver::_instance
	RuntimeObject* ____instance_0;
	// Newtonsoft.Json.JsonConverter[] Newtonsoft.Json.Serialization.DefaultContractResolver::BuiltInConverters
	JsonConverterU5BU5D_t155849004* ___BuiltInConverters_1;
	// System.Object Newtonsoft.Json.Serialization.DefaultContractResolver::TypeContractCacheLock
	RuntimeObject * ___TypeContractCacheLock_2;
	// Newtonsoft.Json.Serialization.DefaultContractResolverState Newtonsoft.Json.Serialization.DefaultContractResolver::_sharedState
	DefaultContractResolverState_t3045992855 * ____sharedState_3;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t270250618_StaticFields, ____instance_0)); }
	inline RuntimeObject* get__instance_0() const { return ____instance_0; }
	inline RuntimeObject** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(RuntimeObject* value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}

	inline static int32_t get_offset_of_BuiltInConverters_1() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t270250618_StaticFields, ___BuiltInConverters_1)); }
	inline JsonConverterU5BU5D_t155849004* get_BuiltInConverters_1() const { return ___BuiltInConverters_1; }
	inline JsonConverterU5BU5D_t155849004** get_address_of_BuiltInConverters_1() { return &___BuiltInConverters_1; }
	inline void set_BuiltInConverters_1(JsonConverterU5BU5D_t155849004* value)
	{
		___BuiltInConverters_1 = value;
		Il2CppCodeGenWriteBarrier((&___BuiltInConverters_1), value);
	}

	inline static int32_t get_offset_of_TypeContractCacheLock_2() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t270250618_StaticFields, ___TypeContractCacheLock_2)); }
	inline RuntimeObject * get_TypeContractCacheLock_2() const { return ___TypeContractCacheLock_2; }
	inline RuntimeObject ** get_address_of_TypeContractCacheLock_2() { return &___TypeContractCacheLock_2; }
	inline void set_TypeContractCacheLock_2(RuntimeObject * value)
	{
		___TypeContractCacheLock_2 = value;
		Il2CppCodeGenWriteBarrier((&___TypeContractCacheLock_2), value);
	}

	inline static int32_t get_offset_of__sharedState_3() { return static_cast<int32_t>(offsetof(DefaultContractResolver_t270250618_StaticFields, ____sharedState_3)); }
	inline DefaultContractResolverState_t3045992855 * get__sharedState_3() const { return ____sharedState_3; }
	inline DefaultContractResolverState_t3045992855 ** get_address_of__sharedState_3() { return &____sharedState_3; }
	inline void set__sharedState_3(DefaultContractResolverState_t3045992855 * value)
	{
		____sharedState_3 = value;
		Il2CppCodeGenWriteBarrier((&____sharedState_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCONTRACTRESOLVER_T270250618_H
#ifndef GETRAWFILEDATATEXTCALLBACK_T2904487790_H
#define GETRAWFILEDATATEXTCALLBACK_T2904487790_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.api_clients.poly_client.PolyClientUtils/GetRawFileDataTextCallback
struct  GetRawFileDataTextCallback_t2904487790  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAWFILEDATATEXTCALLBACK_T2904487790_H
#ifndef CACHEREADCALLBACK_T2055645553_H
#define CACHEREADCALLBACK_T2055645553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.caching.PersistentBlobCache/CacheReadCallback
struct  CacheReadCallback_t2055645553  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEREADCALLBACK_T2055645553_H
#ifndef GETRAWFILEDATABYTESCALLBACK_T2880187408_H
#define GETRAWFILEDATABYTESCALLBACK_T2880187408_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.api_clients.poly_client.PolyClientUtils/GetRawFileDataBytesCallback
struct  GetRawFileDataBytesCallback_t2880187408  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAWFILEDATABYTESCALLBACK_T2880187408_H
#ifndef IMPORTOPERATION_T1829818323_H
#define IMPORTOPERATION_T1829818323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.AsyncImporter/ImportOperation
struct  ImportOperation_t1829818323  : public RuntimeObject
{
public:
	// PolyToolkitInternal.AsyncImporter PolyToolkitInternal.AsyncImporter/ImportOperation::instance
	AsyncImporter_t2267690090 * ___instance_0;
	// PolyToolkit.PolyAsset PolyToolkitInternal.AsyncImporter/ImportOperation::asset
	PolyAsset_t1814153511 * ___asset_1;
	// PolyToolkit.PolyFormat PolyToolkitInternal.AsyncImporter/ImportOperation::format
	PolyFormat_t1880249796 * ___format_2;
	// PolyToolkit.PolyImportOptions PolyToolkitInternal.AsyncImporter/ImportOperation::options
	PolyImportOptions_t4213423452  ___options_3;
	// PolyToolkitInternal.AsyncImporter/AsyncImportCallback PolyToolkitInternal.AsyncImporter/ImportOperation::callback
	AsyncImportCallback_t3623524474 * ___callback_4;
	// PolyToolkitInternal.ImportGltf/ImportState PolyToolkitInternal.AsyncImporter/ImportOperation::importState
	ImportState_t482765084 * ___importState_5;
	// PolyToolkitInternal.IUriLoader PolyToolkitInternal.AsyncImporter/ImportOperation::loader
	RuntimeObject* ___loader_6;
	// PolyToolkit.PolyStatus PolyToolkitInternal.AsyncImporter/ImportOperation::status
	PolyStatus_t3145373940  ___status_7;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(ImportOperation_t1829818323, ___instance_0)); }
	inline AsyncImporter_t2267690090 * get_instance_0() const { return ___instance_0; }
	inline AsyncImporter_t2267690090 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(AsyncImporter_t2267690090 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___instance_0), value);
	}

	inline static int32_t get_offset_of_asset_1() { return static_cast<int32_t>(offsetof(ImportOperation_t1829818323, ___asset_1)); }
	inline PolyAsset_t1814153511 * get_asset_1() const { return ___asset_1; }
	inline PolyAsset_t1814153511 ** get_address_of_asset_1() { return &___asset_1; }
	inline void set_asset_1(PolyAsset_t1814153511 * value)
	{
		___asset_1 = value;
		Il2CppCodeGenWriteBarrier((&___asset_1), value);
	}

	inline static int32_t get_offset_of_format_2() { return static_cast<int32_t>(offsetof(ImportOperation_t1829818323, ___format_2)); }
	inline PolyFormat_t1880249796 * get_format_2() const { return ___format_2; }
	inline PolyFormat_t1880249796 ** get_address_of_format_2() { return &___format_2; }
	inline void set_format_2(PolyFormat_t1880249796 * value)
	{
		___format_2 = value;
		Il2CppCodeGenWriteBarrier((&___format_2), value);
	}

	inline static int32_t get_offset_of_options_3() { return static_cast<int32_t>(offsetof(ImportOperation_t1829818323, ___options_3)); }
	inline PolyImportOptions_t4213423452  get_options_3() const { return ___options_3; }
	inline PolyImportOptions_t4213423452 * get_address_of_options_3() { return &___options_3; }
	inline void set_options_3(PolyImportOptions_t4213423452  value)
	{
		___options_3 = value;
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(ImportOperation_t1829818323, ___callback_4)); }
	inline AsyncImportCallback_t3623524474 * get_callback_4() const { return ___callback_4; }
	inline AsyncImportCallback_t3623524474 ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(AsyncImportCallback_t3623524474 * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}

	inline static int32_t get_offset_of_importState_5() { return static_cast<int32_t>(offsetof(ImportOperation_t1829818323, ___importState_5)); }
	inline ImportState_t482765084 * get_importState_5() const { return ___importState_5; }
	inline ImportState_t482765084 ** get_address_of_importState_5() { return &___importState_5; }
	inline void set_importState_5(ImportState_t482765084 * value)
	{
		___importState_5 = value;
		Il2CppCodeGenWriteBarrier((&___importState_5), value);
	}

	inline static int32_t get_offset_of_loader_6() { return static_cast<int32_t>(offsetof(ImportOperation_t1829818323, ___loader_6)); }
	inline RuntimeObject* get_loader_6() const { return ___loader_6; }
	inline RuntimeObject** get_address_of_loader_6() { return &___loader_6; }
	inline void set_loader_6(RuntimeObject* value)
	{
		___loader_6 = value;
		Il2CppCodeGenWriteBarrier((&___loader_6), value);
	}

	inline static int32_t get_offset_of_status_7() { return static_cast<int32_t>(offsetof(ImportOperation_t1829818323, ___status_7)); }
	inline PolyStatus_t3145373940  get_status_7() const { return ___status_7; }
	inline PolyStatus_t3145373940 * get_address_of_status_7() { return &___status_7; }
	inline void set_status_7(PolyStatus_t3145373940  value)
	{
		___status_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMPORTOPERATION_T1829818323_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ASYNCIMPORTCALLBACK_T3623524474_H
#define ASYNCIMPORTCALLBACK_T3623524474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.AsyncImporter/AsyncImportCallback
struct  AsyncImportCallback_t3623524474  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCIMPORTCALLBACK_T3623524474_H
#ifndef GLTF1NODE_T1095785444_H
#define GLTF1NODE_T1095785444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf1Node
struct  Gltf1Node_t1095785444  : public GltfNodeBase_t3005766873
{
public:
	// System.Collections.Generic.List`1<System.String> PolyToolkitInternal.Gltf1Node::children
	List_1_t3319525431 * ___children_2;
	// System.Collections.Generic.List`1<System.String> PolyToolkitInternal.Gltf1Node::meshes
	List_1_t3319525431 * ___meshes_3;
	// System.String PolyToolkitInternal.Gltf1Node::gltfId
	String_t* ___gltfId_4;
	// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf1Mesh> PolyToolkitInternal.Gltf1Node::meshPtrs
	List_1_t1473634767 * ___meshPtrs_5;
	// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf1Node> PolyToolkitInternal.Gltf1Node::childPtrs
	List_1_t2567860186 * ___childPtrs_6;

public:
	inline static int32_t get_offset_of_children_2() { return static_cast<int32_t>(offsetof(Gltf1Node_t1095785444, ___children_2)); }
	inline List_1_t3319525431 * get_children_2() const { return ___children_2; }
	inline List_1_t3319525431 ** get_address_of_children_2() { return &___children_2; }
	inline void set_children_2(List_1_t3319525431 * value)
	{
		___children_2 = value;
		Il2CppCodeGenWriteBarrier((&___children_2), value);
	}

	inline static int32_t get_offset_of_meshes_3() { return static_cast<int32_t>(offsetof(Gltf1Node_t1095785444, ___meshes_3)); }
	inline List_1_t3319525431 * get_meshes_3() const { return ___meshes_3; }
	inline List_1_t3319525431 ** get_address_of_meshes_3() { return &___meshes_3; }
	inline void set_meshes_3(List_1_t3319525431 * value)
	{
		___meshes_3 = value;
		Il2CppCodeGenWriteBarrier((&___meshes_3), value);
	}

	inline static int32_t get_offset_of_gltfId_4() { return static_cast<int32_t>(offsetof(Gltf1Node_t1095785444, ___gltfId_4)); }
	inline String_t* get_gltfId_4() const { return ___gltfId_4; }
	inline String_t** get_address_of_gltfId_4() { return &___gltfId_4; }
	inline void set_gltfId_4(String_t* value)
	{
		___gltfId_4 = value;
		Il2CppCodeGenWriteBarrier((&___gltfId_4), value);
	}

	inline static int32_t get_offset_of_meshPtrs_5() { return static_cast<int32_t>(offsetof(Gltf1Node_t1095785444, ___meshPtrs_5)); }
	inline List_1_t1473634767 * get_meshPtrs_5() const { return ___meshPtrs_5; }
	inline List_1_t1473634767 ** get_address_of_meshPtrs_5() { return &___meshPtrs_5; }
	inline void set_meshPtrs_5(List_1_t1473634767 * value)
	{
		___meshPtrs_5 = value;
		Il2CppCodeGenWriteBarrier((&___meshPtrs_5), value);
	}

	inline static int32_t get_offset_of_childPtrs_6() { return static_cast<int32_t>(offsetof(Gltf1Node_t1095785444, ___childPtrs_6)); }
	inline List_1_t2567860186 * get_childPtrs_6() const { return ___childPtrs_6; }
	inline List_1_t2567860186 ** get_address_of_childPtrs_6() { return &___childPtrs_6; }
	inline void set_childPtrs_6(List_1_t2567860186 * value)
	{
		___childPtrs_6 = value;
		Il2CppCodeGenWriteBarrier((&___childPtrs_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF1NODE_T1095785444_H
#ifndef GLTF2ROOT_T2278775764_H
#define GLTF2ROOT_T2278775764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf2Root
struct  Gltf2Root_t2278775764  : public GltfRootBase_t3065879770
{
public:
	// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf2Buffer> PolyToolkitInternal.Gltf2Root::buffers
	List_1_t2736210405 * ___buffers_3;
	// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf2Accessor> PolyToolkitInternal.Gltf2Root::accessors
	List_1_t287795851 * ___accessors_4;
	// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf2BufferView> PolyToolkitInternal.Gltf2Root::bufferViews
	List_1_t3267605692 * ___bufferViews_5;
	// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf2Mesh> PolyToolkitInternal.Gltf2Root::meshes
	List_1_t1473634736 * ___meshes_6;
	// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf2Material> PolyToolkitInternal.Gltf2Root::materials
	List_1_t4175146369 * ___materials_7;
	// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf2Node> PolyToolkitInternal.Gltf2Root::nodes
	List_1_t2567860153 * ___nodes_8;
	// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf2Scene> PolyToolkitInternal.Gltf2Root::scenes
	List_1_t2219964544 * ___scenes_9;
	// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf2Texture> PolyToolkitInternal.Gltf2Root::textures
	List_1_t1122653603 * ___textures_10;
	// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf2Image> PolyToolkitInternal.Gltf2Root::images
	List_1_t213288581 * ___images_11;
	// System.Int32 PolyToolkitInternal.Gltf2Root::scene
	int32_t ___scene_12;
	// System.Boolean PolyToolkitInternal.Gltf2Root::disposed
	bool ___disposed_13;
	// PolyToolkitInternal.Gltf2Scene PolyToolkitInternal.Gltf2Root::scenePtr
	Gltf2Scene_t747889802 * ___scenePtr_14;

public:
	inline static int32_t get_offset_of_buffers_3() { return static_cast<int32_t>(offsetof(Gltf2Root_t2278775764, ___buffers_3)); }
	inline List_1_t2736210405 * get_buffers_3() const { return ___buffers_3; }
	inline List_1_t2736210405 ** get_address_of_buffers_3() { return &___buffers_3; }
	inline void set_buffers_3(List_1_t2736210405 * value)
	{
		___buffers_3 = value;
		Il2CppCodeGenWriteBarrier((&___buffers_3), value);
	}

	inline static int32_t get_offset_of_accessors_4() { return static_cast<int32_t>(offsetof(Gltf2Root_t2278775764, ___accessors_4)); }
	inline List_1_t287795851 * get_accessors_4() const { return ___accessors_4; }
	inline List_1_t287795851 ** get_address_of_accessors_4() { return &___accessors_4; }
	inline void set_accessors_4(List_1_t287795851 * value)
	{
		___accessors_4 = value;
		Il2CppCodeGenWriteBarrier((&___accessors_4), value);
	}

	inline static int32_t get_offset_of_bufferViews_5() { return static_cast<int32_t>(offsetof(Gltf2Root_t2278775764, ___bufferViews_5)); }
	inline List_1_t3267605692 * get_bufferViews_5() const { return ___bufferViews_5; }
	inline List_1_t3267605692 ** get_address_of_bufferViews_5() { return &___bufferViews_5; }
	inline void set_bufferViews_5(List_1_t3267605692 * value)
	{
		___bufferViews_5 = value;
		Il2CppCodeGenWriteBarrier((&___bufferViews_5), value);
	}

	inline static int32_t get_offset_of_meshes_6() { return static_cast<int32_t>(offsetof(Gltf2Root_t2278775764, ___meshes_6)); }
	inline List_1_t1473634736 * get_meshes_6() const { return ___meshes_6; }
	inline List_1_t1473634736 ** get_address_of_meshes_6() { return &___meshes_6; }
	inline void set_meshes_6(List_1_t1473634736 * value)
	{
		___meshes_6 = value;
		Il2CppCodeGenWriteBarrier((&___meshes_6), value);
	}

	inline static int32_t get_offset_of_materials_7() { return static_cast<int32_t>(offsetof(Gltf2Root_t2278775764, ___materials_7)); }
	inline List_1_t4175146369 * get_materials_7() const { return ___materials_7; }
	inline List_1_t4175146369 ** get_address_of_materials_7() { return &___materials_7; }
	inline void set_materials_7(List_1_t4175146369 * value)
	{
		___materials_7 = value;
		Il2CppCodeGenWriteBarrier((&___materials_7), value);
	}

	inline static int32_t get_offset_of_nodes_8() { return static_cast<int32_t>(offsetof(Gltf2Root_t2278775764, ___nodes_8)); }
	inline List_1_t2567860153 * get_nodes_8() const { return ___nodes_8; }
	inline List_1_t2567860153 ** get_address_of_nodes_8() { return &___nodes_8; }
	inline void set_nodes_8(List_1_t2567860153 * value)
	{
		___nodes_8 = value;
		Il2CppCodeGenWriteBarrier((&___nodes_8), value);
	}

	inline static int32_t get_offset_of_scenes_9() { return static_cast<int32_t>(offsetof(Gltf2Root_t2278775764, ___scenes_9)); }
	inline List_1_t2219964544 * get_scenes_9() const { return ___scenes_9; }
	inline List_1_t2219964544 ** get_address_of_scenes_9() { return &___scenes_9; }
	inline void set_scenes_9(List_1_t2219964544 * value)
	{
		___scenes_9 = value;
		Il2CppCodeGenWriteBarrier((&___scenes_9), value);
	}

	inline static int32_t get_offset_of_textures_10() { return static_cast<int32_t>(offsetof(Gltf2Root_t2278775764, ___textures_10)); }
	inline List_1_t1122653603 * get_textures_10() const { return ___textures_10; }
	inline List_1_t1122653603 ** get_address_of_textures_10() { return &___textures_10; }
	inline void set_textures_10(List_1_t1122653603 * value)
	{
		___textures_10 = value;
		Il2CppCodeGenWriteBarrier((&___textures_10), value);
	}

	inline static int32_t get_offset_of_images_11() { return static_cast<int32_t>(offsetof(Gltf2Root_t2278775764, ___images_11)); }
	inline List_1_t213288581 * get_images_11() const { return ___images_11; }
	inline List_1_t213288581 ** get_address_of_images_11() { return &___images_11; }
	inline void set_images_11(List_1_t213288581 * value)
	{
		___images_11 = value;
		Il2CppCodeGenWriteBarrier((&___images_11), value);
	}

	inline static int32_t get_offset_of_scene_12() { return static_cast<int32_t>(offsetof(Gltf2Root_t2278775764, ___scene_12)); }
	inline int32_t get_scene_12() const { return ___scene_12; }
	inline int32_t* get_address_of_scene_12() { return &___scene_12; }
	inline void set_scene_12(int32_t value)
	{
		___scene_12 = value;
	}

	inline static int32_t get_offset_of_disposed_13() { return static_cast<int32_t>(offsetof(Gltf2Root_t2278775764, ___disposed_13)); }
	inline bool get_disposed_13() const { return ___disposed_13; }
	inline bool* get_address_of_disposed_13() { return &___disposed_13; }
	inline void set_disposed_13(bool value)
	{
		___disposed_13 = value;
	}

	inline static int32_t get_offset_of_scenePtr_14() { return static_cast<int32_t>(offsetof(Gltf2Root_t2278775764, ___scenePtr_14)); }
	inline Gltf2Scene_t747889802 * get_scenePtr_14() const { return ___scenePtr_14; }
	inline Gltf2Scene_t747889802 ** get_address_of_scenePtr_14() { return &___scenePtr_14; }
	inline void set_scenePtr_14(Gltf2Scene_t747889802 * value)
	{
		___scenePtr_14 = value;
		Il2CppCodeGenWriteBarrier((&___scenePtr_14), value);
	}
};

struct Gltf2Root_t2278775764_StaticFields
{
public:
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>,System.String> PolyToolkitInternal.Gltf2Root::<>f__am$cache0
	Func_2_t2105257824 * ___U3CU3Ef__amU24cache0_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_15() { return static_cast<int32_t>(offsetof(Gltf2Root_t2278775764_StaticFields, ___U3CU3Ef__amU24cache0_15)); }
	inline Func_2_t2105257824 * get_U3CU3Ef__amU24cache0_15() const { return ___U3CU3Ef__amU24cache0_15; }
	inline Func_2_t2105257824 ** get_address_of_U3CU3Ef__amU24cache0_15() { return &___U3CU3Ef__amU24cache0_15; }
	inline void set_U3CU3Ef__amU24cache0_15(Func_2_t2105257824 * value)
	{
		___U3CU3Ef__amU24cache0_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF2ROOT_T2278775764_H
#ifndef GLTF2ACCESSOR_T3110688405_H
#define GLTF2ACCESSOR_T3110688405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf2Accessor
struct  Gltf2Accessor_t3110688405  : public GltfAccessorBase_t2108002982
{
public:
	// System.Int32 PolyToolkitInternal.Gltf2Accessor::bufferView
	int32_t ___bufferView_7;
	// System.Int32 PolyToolkitInternal.Gltf2Accessor::gltfIndex
	int32_t ___gltfIndex_8;
	// PolyToolkitInternal.Gltf2BufferView PolyToolkitInternal.Gltf2Accessor::bufferViewPtr
	Gltf2BufferView_t1795530950 * ___bufferViewPtr_9;

public:
	inline static int32_t get_offset_of_bufferView_7() { return static_cast<int32_t>(offsetof(Gltf2Accessor_t3110688405, ___bufferView_7)); }
	inline int32_t get_bufferView_7() const { return ___bufferView_7; }
	inline int32_t* get_address_of_bufferView_7() { return &___bufferView_7; }
	inline void set_bufferView_7(int32_t value)
	{
		___bufferView_7 = value;
	}

	inline static int32_t get_offset_of_gltfIndex_8() { return static_cast<int32_t>(offsetof(Gltf2Accessor_t3110688405, ___gltfIndex_8)); }
	inline int32_t get_gltfIndex_8() const { return ___gltfIndex_8; }
	inline int32_t* get_address_of_gltfIndex_8() { return &___gltfIndex_8; }
	inline void set_gltfIndex_8(int32_t value)
	{
		___gltfIndex_8 = value;
	}

	inline static int32_t get_offset_of_bufferViewPtr_9() { return static_cast<int32_t>(offsetof(Gltf2Accessor_t3110688405, ___bufferViewPtr_9)); }
	inline Gltf2BufferView_t1795530950 * get_bufferViewPtr_9() const { return ___bufferViewPtr_9; }
	inline Gltf2BufferView_t1795530950 ** get_address_of_bufferViewPtr_9() { return &___bufferViewPtr_9; }
	inline void set_bufferViewPtr_9(Gltf2BufferView_t1795530950 * value)
	{
		___bufferViewPtr_9 = value;
		Il2CppCodeGenWriteBarrier((&___bufferViewPtr_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF2ACCESSOR_T3110688405_H
#ifndef GLTF2PRIMITIVE_T3883867645_H
#define GLTF2PRIMITIVE_T3883867645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf2Primitive
struct  Gltf2Primitive_t3883867645  : public GltfPrimitiveBase_t1810376431
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PolyToolkitInternal.Gltf2Primitive::attributes
	Dictionary_2_t2736202052 * ___attributes_3;
	// System.Int32 PolyToolkitInternal.Gltf2Primitive::indices
	int32_t ___indices_4;
	// System.Int32 PolyToolkitInternal.Gltf2Primitive::material
	int32_t ___material_5;
	// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf2Accessor> PolyToolkitInternal.Gltf2Primitive::attributePtrs
	Dictionary_2_t2895944704 * ___attributePtrs_6;
	// PolyToolkitInternal.Gltf2Accessor PolyToolkitInternal.Gltf2Primitive::indicesPtr
	Gltf2Accessor_t3110688405 * ___indicesPtr_7;
	// PolyToolkitInternal.Gltf2Material PolyToolkitInternal.Gltf2Primitive::materialPtr
	Gltf2Material_t2703071627 * ___materialPtr_8;

public:
	inline static int32_t get_offset_of_attributes_3() { return static_cast<int32_t>(offsetof(Gltf2Primitive_t3883867645, ___attributes_3)); }
	inline Dictionary_2_t2736202052 * get_attributes_3() const { return ___attributes_3; }
	inline Dictionary_2_t2736202052 ** get_address_of_attributes_3() { return &___attributes_3; }
	inline void set_attributes_3(Dictionary_2_t2736202052 * value)
	{
		___attributes_3 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_3), value);
	}

	inline static int32_t get_offset_of_indices_4() { return static_cast<int32_t>(offsetof(Gltf2Primitive_t3883867645, ___indices_4)); }
	inline int32_t get_indices_4() const { return ___indices_4; }
	inline int32_t* get_address_of_indices_4() { return &___indices_4; }
	inline void set_indices_4(int32_t value)
	{
		___indices_4 = value;
	}

	inline static int32_t get_offset_of_material_5() { return static_cast<int32_t>(offsetof(Gltf2Primitive_t3883867645, ___material_5)); }
	inline int32_t get_material_5() const { return ___material_5; }
	inline int32_t* get_address_of_material_5() { return &___material_5; }
	inline void set_material_5(int32_t value)
	{
		___material_5 = value;
	}

	inline static int32_t get_offset_of_attributePtrs_6() { return static_cast<int32_t>(offsetof(Gltf2Primitive_t3883867645, ___attributePtrs_6)); }
	inline Dictionary_2_t2895944704 * get_attributePtrs_6() const { return ___attributePtrs_6; }
	inline Dictionary_2_t2895944704 ** get_address_of_attributePtrs_6() { return &___attributePtrs_6; }
	inline void set_attributePtrs_6(Dictionary_2_t2895944704 * value)
	{
		___attributePtrs_6 = value;
		Il2CppCodeGenWriteBarrier((&___attributePtrs_6), value);
	}

	inline static int32_t get_offset_of_indicesPtr_7() { return static_cast<int32_t>(offsetof(Gltf2Primitive_t3883867645, ___indicesPtr_7)); }
	inline Gltf2Accessor_t3110688405 * get_indicesPtr_7() const { return ___indicesPtr_7; }
	inline Gltf2Accessor_t3110688405 ** get_address_of_indicesPtr_7() { return &___indicesPtr_7; }
	inline void set_indicesPtr_7(Gltf2Accessor_t3110688405 * value)
	{
		___indicesPtr_7 = value;
		Il2CppCodeGenWriteBarrier((&___indicesPtr_7), value);
	}

	inline static int32_t get_offset_of_materialPtr_8() { return static_cast<int32_t>(offsetof(Gltf2Primitive_t3883867645, ___materialPtr_8)); }
	inline Gltf2Material_t2703071627 * get_materialPtr_8() const { return ___materialPtr_8; }
	inline Gltf2Material_t2703071627 ** get_address_of_materialPtr_8() { return &___materialPtr_8; }
	inline void set_materialPtr_8(Gltf2Material_t2703071627 * value)
	{
		___materialPtr_8 = value;
		Il2CppCodeGenWriteBarrier((&___materialPtr_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF2PRIMITIVE_T3883867645_H
#ifndef GLTF2NODE_T1095785411_H
#define GLTF2NODE_T1095785411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf2Node
struct  Gltf2Node_t1095785411  : public GltfNodeBase_t3005766873
{
public:
	// System.Collections.Generic.List`1<System.Int32> PolyToolkitInternal.Gltf2Node::children
	List_1_t128053199 * ___children_2;
	// System.Int32 PolyToolkitInternal.Gltf2Node::mesh
	int32_t ___mesh_3;
	// System.Int32 PolyToolkitInternal.Gltf2Node::gltfIndex
	int32_t ___gltfIndex_4;
	// PolyToolkitInternal.Gltf2Mesh PolyToolkitInternal.Gltf2Node::meshPtr
	Gltf2Mesh_t1559994 * ___meshPtr_5;
	// System.Collections.Generic.List`1<PolyToolkitInternal.Gltf2Node> PolyToolkitInternal.Gltf2Node::childPtrs
	List_1_t2567860153 * ___childPtrs_6;

public:
	inline static int32_t get_offset_of_children_2() { return static_cast<int32_t>(offsetof(Gltf2Node_t1095785411, ___children_2)); }
	inline List_1_t128053199 * get_children_2() const { return ___children_2; }
	inline List_1_t128053199 ** get_address_of_children_2() { return &___children_2; }
	inline void set_children_2(List_1_t128053199 * value)
	{
		___children_2 = value;
		Il2CppCodeGenWriteBarrier((&___children_2), value);
	}

	inline static int32_t get_offset_of_mesh_3() { return static_cast<int32_t>(offsetof(Gltf2Node_t1095785411, ___mesh_3)); }
	inline int32_t get_mesh_3() const { return ___mesh_3; }
	inline int32_t* get_address_of_mesh_3() { return &___mesh_3; }
	inline void set_mesh_3(int32_t value)
	{
		___mesh_3 = value;
	}

	inline static int32_t get_offset_of_gltfIndex_4() { return static_cast<int32_t>(offsetof(Gltf2Node_t1095785411, ___gltfIndex_4)); }
	inline int32_t get_gltfIndex_4() const { return ___gltfIndex_4; }
	inline int32_t* get_address_of_gltfIndex_4() { return &___gltfIndex_4; }
	inline void set_gltfIndex_4(int32_t value)
	{
		___gltfIndex_4 = value;
	}

	inline static int32_t get_offset_of_meshPtr_5() { return static_cast<int32_t>(offsetof(Gltf2Node_t1095785411, ___meshPtr_5)); }
	inline Gltf2Mesh_t1559994 * get_meshPtr_5() const { return ___meshPtr_5; }
	inline Gltf2Mesh_t1559994 ** get_address_of_meshPtr_5() { return &___meshPtr_5; }
	inline void set_meshPtr_5(Gltf2Mesh_t1559994 * value)
	{
		___meshPtr_5 = value;
		Il2CppCodeGenWriteBarrier((&___meshPtr_5), value);
	}

	inline static int32_t get_offset_of_childPtrs_6() { return static_cast<int32_t>(offsetof(Gltf2Node_t1095785411, ___childPtrs_6)); }
	inline List_1_t2567860153 * get_childPtrs_6() const { return ___childPtrs_6; }
	inline List_1_t2567860153 ** get_address_of_childPtrs_6() { return &___childPtrs_6; }
	inline void set_childPtrs_6(List_1_t2567860153 * value)
	{
		___childPtrs_6 = value;
		Il2CppCodeGenWriteBarrier((&___childPtrs_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF2NODE_T1095785411_H
#ifndef GLTFJSONCONTRACTRESOLVER_T946412919_H
#define GLTFJSONCONTRACTRESOLVER_T946412919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.GltfJsonContractResolver
struct  GltfJsonContractResolver_t946412919  : public DefaultContractResolver_t270250618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTFJSONCONTRACTRESOLVER_T946412919_H
#ifndef GLTF1PRIMITIVE_T3883868606_H
#define GLTF1PRIMITIVE_T3883868606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf1Primitive
struct  Gltf1Primitive_t3883868606  : public GltfPrimitiveBase_t1810376431
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> PolyToolkitInternal.Gltf1Primitive::attributes
	Dictionary_2_t1632706988 * ___attributes_3;
	// System.String PolyToolkitInternal.Gltf1Primitive::indices
	String_t* ___indices_4;
	// System.String PolyToolkitInternal.Gltf1Primitive::material
	String_t* ___material_5;
	// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1Accessor> PolyToolkitInternal.Gltf1Primitive::attributePtrs
	Dictionary_2_t2895945665 * ___attributePtrs_6;
	// PolyToolkitInternal.Gltf1Accessor PolyToolkitInternal.Gltf1Primitive::indicesPtr
	Gltf1Accessor_t3110689366 * ___indicesPtr_7;
	// PolyToolkitInternal.Gltf1Material PolyToolkitInternal.Gltf1Primitive::materialPtr
	Gltf1Material_t2703072588 * ___materialPtr_8;

public:
	inline static int32_t get_offset_of_attributes_3() { return static_cast<int32_t>(offsetof(Gltf1Primitive_t3883868606, ___attributes_3)); }
	inline Dictionary_2_t1632706988 * get_attributes_3() const { return ___attributes_3; }
	inline Dictionary_2_t1632706988 ** get_address_of_attributes_3() { return &___attributes_3; }
	inline void set_attributes_3(Dictionary_2_t1632706988 * value)
	{
		___attributes_3 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_3), value);
	}

	inline static int32_t get_offset_of_indices_4() { return static_cast<int32_t>(offsetof(Gltf1Primitive_t3883868606, ___indices_4)); }
	inline String_t* get_indices_4() const { return ___indices_4; }
	inline String_t** get_address_of_indices_4() { return &___indices_4; }
	inline void set_indices_4(String_t* value)
	{
		___indices_4 = value;
		Il2CppCodeGenWriteBarrier((&___indices_4), value);
	}

	inline static int32_t get_offset_of_material_5() { return static_cast<int32_t>(offsetof(Gltf1Primitive_t3883868606, ___material_5)); }
	inline String_t* get_material_5() const { return ___material_5; }
	inline String_t** get_address_of_material_5() { return &___material_5; }
	inline void set_material_5(String_t* value)
	{
		___material_5 = value;
		Il2CppCodeGenWriteBarrier((&___material_5), value);
	}

	inline static int32_t get_offset_of_attributePtrs_6() { return static_cast<int32_t>(offsetof(Gltf1Primitive_t3883868606, ___attributePtrs_6)); }
	inline Dictionary_2_t2895945665 * get_attributePtrs_6() const { return ___attributePtrs_6; }
	inline Dictionary_2_t2895945665 ** get_address_of_attributePtrs_6() { return &___attributePtrs_6; }
	inline void set_attributePtrs_6(Dictionary_2_t2895945665 * value)
	{
		___attributePtrs_6 = value;
		Il2CppCodeGenWriteBarrier((&___attributePtrs_6), value);
	}

	inline static int32_t get_offset_of_indicesPtr_7() { return static_cast<int32_t>(offsetof(Gltf1Primitive_t3883868606, ___indicesPtr_7)); }
	inline Gltf1Accessor_t3110689366 * get_indicesPtr_7() const { return ___indicesPtr_7; }
	inline Gltf1Accessor_t3110689366 ** get_address_of_indicesPtr_7() { return &___indicesPtr_7; }
	inline void set_indicesPtr_7(Gltf1Accessor_t3110689366 * value)
	{
		___indicesPtr_7 = value;
		Il2CppCodeGenWriteBarrier((&___indicesPtr_7), value);
	}

	inline static int32_t get_offset_of_materialPtr_8() { return static_cast<int32_t>(offsetof(Gltf1Primitive_t3883868606, ___materialPtr_8)); }
	inline Gltf1Material_t2703072588 * get_materialPtr_8() const { return ___materialPtr_8; }
	inline Gltf1Material_t2703072588 ** get_address_of_materialPtr_8() { return &___materialPtr_8; }
	inline void set_materialPtr_8(Gltf1Material_t2703072588 * value)
	{
		___materialPtr_8 = value;
		Il2CppCodeGenWriteBarrier((&___materialPtr_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF1PRIMITIVE_T3883868606_H
#ifndef EDITTIMEIMPORTOPTIONS_T4030780733_H
#define EDITTIMEIMPORTOPTIONS_T4030780733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.EditTimeImportOptions
struct  EditTimeImportOptions_t4030780733 
{
public:
	// PolyToolkit.PolyImportOptions PolyToolkitInternal.EditTimeImportOptions::baseOptions
	PolyImportOptions_t4213423452  ___baseOptions_0;
	// System.Boolean PolyToolkitInternal.EditTimeImportOptions::alsoInstantiate
	bool ___alsoInstantiate_1;

public:
	inline static int32_t get_offset_of_baseOptions_0() { return static_cast<int32_t>(offsetof(EditTimeImportOptions_t4030780733, ___baseOptions_0)); }
	inline PolyImportOptions_t4213423452  get_baseOptions_0() const { return ___baseOptions_0; }
	inline PolyImportOptions_t4213423452 * get_address_of_baseOptions_0() { return &___baseOptions_0; }
	inline void set_baseOptions_0(PolyImportOptions_t4213423452  value)
	{
		___baseOptions_0 = value;
	}

	inline static int32_t get_offset_of_alsoInstantiate_1() { return static_cast<int32_t>(offsetof(EditTimeImportOptions_t4030780733, ___alsoInstantiate_1)); }
	inline bool get_alsoInstantiate_1() const { return ___alsoInstantiate_1; }
	inline bool* get_address_of_alsoInstantiate_1() { return &___alsoInstantiate_1; }
	inline void set_alsoInstantiate_1(bool value)
	{
		___alsoInstantiate_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PolyToolkitInternal.EditTimeImportOptions
struct EditTimeImportOptions_t4030780733_marshaled_pinvoke
{
	PolyImportOptions_t4213423452_marshaled_pinvoke ___baseOptions_0;
	int32_t ___alsoInstantiate_1;
};
// Native definition for COM marshalling of PolyToolkitInternal.EditTimeImportOptions
struct EditTimeImportOptions_t4030780733_marshaled_com
{
	PolyImportOptions_t4213423452_marshaled_com ___baseOptions_0;
	int32_t ___alsoInstantiate_1;
};
#endif // EDITTIMEIMPORTOPTIONS_T4030780733_H
#ifndef GLTF1ROOT_T2278775795_H
#define GLTF1ROOT_T2278775795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf1Root
struct  Gltf1Root_t2278775795  : public GltfRootBase_t3065879770
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1Buffer> PolyToolkitInternal.Gltf1Root::buffers
	Dictionary_2_t1049391993 * ___buffers_3;
	// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1Accessor> PolyToolkitInternal.Gltf1Root::accessors
	Dictionary_2_t2895945665 * ___accessors_4;
	// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1BufferView> PolyToolkitInternal.Gltf1Root::bufferViews
	Dictionary_2_t1580788274 * ___bufferViews_5;
	// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1Mesh> PolyToolkitInternal.Gltf1Root::meshes
	Dictionary_2_t4081783620 * ___meshes_6;
	// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1Shader> PolyToolkitInternal.Gltf1Root::shaders
	Dictionary_2_t1639499610 * ___shaders_7;
	// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1Program> PolyToolkitInternal.Gltf1Root::programs
	Dictionary_2_t3185157728 * ___programs_8;
	// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1Technique> PolyToolkitInternal.Gltf1Root::techniques
	Dictionary_2_t288930167 * ___techniques_9;
	// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1Image> PolyToolkitInternal.Gltf1Root::images
	Dictionary_2_t2821437467 * ___images_10;
	// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1Texture> PolyToolkitInternal.Gltf1Root::textures
	Dictionary_2_t3730802487 * ___textures_11;
	// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1Material> PolyToolkitInternal.Gltf1Root::materials
	Dictionary_2_t2488328887 * ___materials_12;
	// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1Node> PolyToolkitInternal.Gltf1Root::nodes
	Dictionary_2_t881041743 * ___nodes_13;
	// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.Gltf1Scene> PolyToolkitInternal.Gltf1Root::scenes
	Dictionary_2_t533146132 * ___scenes_14;
	// System.String PolyToolkitInternal.Gltf1Root::scene
	String_t* ___scene_15;
	// System.Boolean PolyToolkitInternal.Gltf1Root::disposed
	bool ___disposed_16;
	// PolyToolkitInternal.Gltf1Scene PolyToolkitInternal.Gltf1Root::scenePtr
	Gltf1Scene_t747889833 * ___scenePtr_17;

public:
	inline static int32_t get_offset_of_buffers_3() { return static_cast<int32_t>(offsetof(Gltf1Root_t2278775795, ___buffers_3)); }
	inline Dictionary_2_t1049391993 * get_buffers_3() const { return ___buffers_3; }
	inline Dictionary_2_t1049391993 ** get_address_of_buffers_3() { return &___buffers_3; }
	inline void set_buffers_3(Dictionary_2_t1049391993 * value)
	{
		___buffers_3 = value;
		Il2CppCodeGenWriteBarrier((&___buffers_3), value);
	}

	inline static int32_t get_offset_of_accessors_4() { return static_cast<int32_t>(offsetof(Gltf1Root_t2278775795, ___accessors_4)); }
	inline Dictionary_2_t2895945665 * get_accessors_4() const { return ___accessors_4; }
	inline Dictionary_2_t2895945665 ** get_address_of_accessors_4() { return &___accessors_4; }
	inline void set_accessors_4(Dictionary_2_t2895945665 * value)
	{
		___accessors_4 = value;
		Il2CppCodeGenWriteBarrier((&___accessors_4), value);
	}

	inline static int32_t get_offset_of_bufferViews_5() { return static_cast<int32_t>(offsetof(Gltf1Root_t2278775795, ___bufferViews_5)); }
	inline Dictionary_2_t1580788274 * get_bufferViews_5() const { return ___bufferViews_5; }
	inline Dictionary_2_t1580788274 ** get_address_of_bufferViews_5() { return &___bufferViews_5; }
	inline void set_bufferViews_5(Dictionary_2_t1580788274 * value)
	{
		___bufferViews_5 = value;
		Il2CppCodeGenWriteBarrier((&___bufferViews_5), value);
	}

	inline static int32_t get_offset_of_meshes_6() { return static_cast<int32_t>(offsetof(Gltf1Root_t2278775795, ___meshes_6)); }
	inline Dictionary_2_t4081783620 * get_meshes_6() const { return ___meshes_6; }
	inline Dictionary_2_t4081783620 ** get_address_of_meshes_6() { return &___meshes_6; }
	inline void set_meshes_6(Dictionary_2_t4081783620 * value)
	{
		___meshes_6 = value;
		Il2CppCodeGenWriteBarrier((&___meshes_6), value);
	}

	inline static int32_t get_offset_of_shaders_7() { return static_cast<int32_t>(offsetof(Gltf1Root_t2278775795, ___shaders_7)); }
	inline Dictionary_2_t1639499610 * get_shaders_7() const { return ___shaders_7; }
	inline Dictionary_2_t1639499610 ** get_address_of_shaders_7() { return &___shaders_7; }
	inline void set_shaders_7(Dictionary_2_t1639499610 * value)
	{
		___shaders_7 = value;
		Il2CppCodeGenWriteBarrier((&___shaders_7), value);
	}

	inline static int32_t get_offset_of_programs_8() { return static_cast<int32_t>(offsetof(Gltf1Root_t2278775795, ___programs_8)); }
	inline Dictionary_2_t3185157728 * get_programs_8() const { return ___programs_8; }
	inline Dictionary_2_t3185157728 ** get_address_of_programs_8() { return &___programs_8; }
	inline void set_programs_8(Dictionary_2_t3185157728 * value)
	{
		___programs_8 = value;
		Il2CppCodeGenWriteBarrier((&___programs_8), value);
	}

	inline static int32_t get_offset_of_techniques_9() { return static_cast<int32_t>(offsetof(Gltf1Root_t2278775795, ___techniques_9)); }
	inline Dictionary_2_t288930167 * get_techniques_9() const { return ___techniques_9; }
	inline Dictionary_2_t288930167 ** get_address_of_techniques_9() { return &___techniques_9; }
	inline void set_techniques_9(Dictionary_2_t288930167 * value)
	{
		___techniques_9 = value;
		Il2CppCodeGenWriteBarrier((&___techniques_9), value);
	}

	inline static int32_t get_offset_of_images_10() { return static_cast<int32_t>(offsetof(Gltf1Root_t2278775795, ___images_10)); }
	inline Dictionary_2_t2821437467 * get_images_10() const { return ___images_10; }
	inline Dictionary_2_t2821437467 ** get_address_of_images_10() { return &___images_10; }
	inline void set_images_10(Dictionary_2_t2821437467 * value)
	{
		___images_10 = value;
		Il2CppCodeGenWriteBarrier((&___images_10), value);
	}

	inline static int32_t get_offset_of_textures_11() { return static_cast<int32_t>(offsetof(Gltf1Root_t2278775795, ___textures_11)); }
	inline Dictionary_2_t3730802487 * get_textures_11() const { return ___textures_11; }
	inline Dictionary_2_t3730802487 ** get_address_of_textures_11() { return &___textures_11; }
	inline void set_textures_11(Dictionary_2_t3730802487 * value)
	{
		___textures_11 = value;
		Il2CppCodeGenWriteBarrier((&___textures_11), value);
	}

	inline static int32_t get_offset_of_materials_12() { return static_cast<int32_t>(offsetof(Gltf1Root_t2278775795, ___materials_12)); }
	inline Dictionary_2_t2488328887 * get_materials_12() const { return ___materials_12; }
	inline Dictionary_2_t2488328887 ** get_address_of_materials_12() { return &___materials_12; }
	inline void set_materials_12(Dictionary_2_t2488328887 * value)
	{
		___materials_12 = value;
		Il2CppCodeGenWriteBarrier((&___materials_12), value);
	}

	inline static int32_t get_offset_of_nodes_13() { return static_cast<int32_t>(offsetof(Gltf1Root_t2278775795, ___nodes_13)); }
	inline Dictionary_2_t881041743 * get_nodes_13() const { return ___nodes_13; }
	inline Dictionary_2_t881041743 ** get_address_of_nodes_13() { return &___nodes_13; }
	inline void set_nodes_13(Dictionary_2_t881041743 * value)
	{
		___nodes_13 = value;
		Il2CppCodeGenWriteBarrier((&___nodes_13), value);
	}

	inline static int32_t get_offset_of_scenes_14() { return static_cast<int32_t>(offsetof(Gltf1Root_t2278775795, ___scenes_14)); }
	inline Dictionary_2_t533146132 * get_scenes_14() const { return ___scenes_14; }
	inline Dictionary_2_t533146132 ** get_address_of_scenes_14() { return &___scenes_14; }
	inline void set_scenes_14(Dictionary_2_t533146132 * value)
	{
		___scenes_14 = value;
		Il2CppCodeGenWriteBarrier((&___scenes_14), value);
	}

	inline static int32_t get_offset_of_scene_15() { return static_cast<int32_t>(offsetof(Gltf1Root_t2278775795, ___scene_15)); }
	inline String_t* get_scene_15() const { return ___scene_15; }
	inline String_t** get_address_of_scene_15() { return &___scene_15; }
	inline void set_scene_15(String_t* value)
	{
		___scene_15 = value;
		Il2CppCodeGenWriteBarrier((&___scene_15), value);
	}

	inline static int32_t get_offset_of_disposed_16() { return static_cast<int32_t>(offsetof(Gltf1Root_t2278775795, ___disposed_16)); }
	inline bool get_disposed_16() const { return ___disposed_16; }
	inline bool* get_address_of_disposed_16() { return &___disposed_16; }
	inline void set_disposed_16(bool value)
	{
		___disposed_16 = value;
	}

	inline static int32_t get_offset_of_scenePtr_17() { return static_cast<int32_t>(offsetof(Gltf1Root_t2278775795, ___scenePtr_17)); }
	inline Gltf1Scene_t747889833 * get_scenePtr_17() const { return ___scenePtr_17; }
	inline Gltf1Scene_t747889833 ** get_address_of_scenePtr_17() { return &___scenePtr_17; }
	inline void set_scenePtr_17(Gltf1Scene_t747889833 * value)
	{
		___scenePtr_17 = value;
		Il2CppCodeGenWriteBarrier((&___scenePtr_17), value);
	}
};

struct Gltf1Root_t2278775795_StaticFields
{
public:
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String> PolyToolkitInternal.Gltf1Root::<>f__am$cache0
	Func_2_t3001800792 * ___U3CU3Ef__amU24cache0_18;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_18() { return static_cast<int32_t>(offsetof(Gltf1Root_t2278775795_StaticFields, ___U3CU3Ef__amU24cache0_18)); }
	inline Func_2_t3001800792 * get_U3CU3Ef__amU24cache0_18() const { return ___U3CU3Ef__amU24cache0_18; }
	inline Func_2_t3001800792 ** get_address_of_U3CU3Ef__amU24cache0_18() { return &___U3CU3Ef__amU24cache0_18; }
	inline void set_U3CU3Ef__amU24cache0_18(Func_2_t3001800792 * value)
	{
		___U3CU3Ef__amU24cache0_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF1ROOT_T2278775795_H
#ifndef GLTF1ACCESSOR_T3110689366_H
#define GLTF1ACCESSOR_T3110689366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Gltf1Accessor
struct  Gltf1Accessor_t3110689366  : public GltfAccessorBase_t2108002982
{
public:
	// System.String PolyToolkitInternal.Gltf1Accessor::bufferView
	String_t* ___bufferView_7;
	// System.String PolyToolkitInternal.Gltf1Accessor::gltfId
	String_t* ___gltfId_8;
	// PolyToolkitInternal.Gltf1BufferView PolyToolkitInternal.Gltf1Accessor::bufferViewPtr
	Gltf1BufferView_t1795531975 * ___bufferViewPtr_9;

public:
	inline static int32_t get_offset_of_bufferView_7() { return static_cast<int32_t>(offsetof(Gltf1Accessor_t3110689366, ___bufferView_7)); }
	inline String_t* get_bufferView_7() const { return ___bufferView_7; }
	inline String_t** get_address_of_bufferView_7() { return &___bufferView_7; }
	inline void set_bufferView_7(String_t* value)
	{
		___bufferView_7 = value;
		Il2CppCodeGenWriteBarrier((&___bufferView_7), value);
	}

	inline static int32_t get_offset_of_gltfId_8() { return static_cast<int32_t>(offsetof(Gltf1Accessor_t3110689366, ___gltfId_8)); }
	inline String_t* get_gltfId_8() const { return ___gltfId_8; }
	inline String_t** get_address_of_gltfId_8() { return &___gltfId_8; }
	inline void set_gltfId_8(String_t* value)
	{
		___gltfId_8 = value;
		Il2CppCodeGenWriteBarrier((&___gltfId_8), value);
	}

	inline static int32_t get_offset_of_bufferViewPtr_9() { return static_cast<int32_t>(offsetof(Gltf1Accessor_t3110689366, ___bufferViewPtr_9)); }
	inline Gltf1BufferView_t1795531975 * get_bufferViewPtr_9() const { return ___bufferViewPtr_9; }
	inline Gltf1BufferView_t1795531975 ** get_address_of_bufferViewPtr_9() { return &___bufferViewPtr_9; }
	inline void set_bufferViewPtr_9(Gltf1BufferView_t1795531975 * value)
	{
		___bufferViewPtr_9 = value;
		Il2CppCodeGenWriteBarrier((&___bufferViewPtr_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLTF1ACCESSOR_T3110689366_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef CFX_SPAWNSYSTEM_T3632654792_H
#define CFX_SPAWNSYSTEM_T3632654792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_SpawnSystem
struct  CFX_SpawnSystem_t3632654792  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] CFX_SpawnSystem::objectsToPreload
	GameObjectU5BU5D_t3328599146* ___objectsToPreload_3;
	// System.Int32[] CFX_SpawnSystem::objectsToPreloadTimes
	Int32U5BU5D_t385246372* ___objectsToPreloadTimes_4;
	// System.Boolean CFX_SpawnSystem::hideObjectsInHierarchy
	bool ___hideObjectsInHierarchy_5;
	// System.Boolean CFX_SpawnSystem::spawnAsChildren
	bool ___spawnAsChildren_6;
	// System.Boolean CFX_SpawnSystem::onlyGetInactiveObjects
	bool ___onlyGetInactiveObjects_7;
	// System.Boolean CFX_SpawnSystem::instantiateIfNeeded
	bool ___instantiateIfNeeded_8;
	// System.Boolean CFX_SpawnSystem::allObjectsLoaded
	bool ___allObjectsLoaded_9;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<UnityEngine.GameObject>> CFX_SpawnSystem::instantiatedObjects
	Dictionary_2_t1474424692 * ___instantiatedObjects_10;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> CFX_SpawnSystem::poolCursors
	Dictionary_2_t1839659084 * ___poolCursors_11;

public:
	inline static int32_t get_offset_of_objectsToPreload_3() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t3632654792, ___objectsToPreload_3)); }
	inline GameObjectU5BU5D_t3328599146* get_objectsToPreload_3() const { return ___objectsToPreload_3; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_objectsToPreload_3() { return &___objectsToPreload_3; }
	inline void set_objectsToPreload_3(GameObjectU5BU5D_t3328599146* value)
	{
		___objectsToPreload_3 = value;
		Il2CppCodeGenWriteBarrier((&___objectsToPreload_3), value);
	}

	inline static int32_t get_offset_of_objectsToPreloadTimes_4() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t3632654792, ___objectsToPreloadTimes_4)); }
	inline Int32U5BU5D_t385246372* get_objectsToPreloadTimes_4() const { return ___objectsToPreloadTimes_4; }
	inline Int32U5BU5D_t385246372** get_address_of_objectsToPreloadTimes_4() { return &___objectsToPreloadTimes_4; }
	inline void set_objectsToPreloadTimes_4(Int32U5BU5D_t385246372* value)
	{
		___objectsToPreloadTimes_4 = value;
		Il2CppCodeGenWriteBarrier((&___objectsToPreloadTimes_4), value);
	}

	inline static int32_t get_offset_of_hideObjectsInHierarchy_5() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t3632654792, ___hideObjectsInHierarchy_5)); }
	inline bool get_hideObjectsInHierarchy_5() const { return ___hideObjectsInHierarchy_5; }
	inline bool* get_address_of_hideObjectsInHierarchy_5() { return &___hideObjectsInHierarchy_5; }
	inline void set_hideObjectsInHierarchy_5(bool value)
	{
		___hideObjectsInHierarchy_5 = value;
	}

	inline static int32_t get_offset_of_spawnAsChildren_6() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t3632654792, ___spawnAsChildren_6)); }
	inline bool get_spawnAsChildren_6() const { return ___spawnAsChildren_6; }
	inline bool* get_address_of_spawnAsChildren_6() { return &___spawnAsChildren_6; }
	inline void set_spawnAsChildren_6(bool value)
	{
		___spawnAsChildren_6 = value;
	}

	inline static int32_t get_offset_of_onlyGetInactiveObjects_7() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t3632654792, ___onlyGetInactiveObjects_7)); }
	inline bool get_onlyGetInactiveObjects_7() const { return ___onlyGetInactiveObjects_7; }
	inline bool* get_address_of_onlyGetInactiveObjects_7() { return &___onlyGetInactiveObjects_7; }
	inline void set_onlyGetInactiveObjects_7(bool value)
	{
		___onlyGetInactiveObjects_7 = value;
	}

	inline static int32_t get_offset_of_instantiateIfNeeded_8() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t3632654792, ___instantiateIfNeeded_8)); }
	inline bool get_instantiateIfNeeded_8() const { return ___instantiateIfNeeded_8; }
	inline bool* get_address_of_instantiateIfNeeded_8() { return &___instantiateIfNeeded_8; }
	inline void set_instantiateIfNeeded_8(bool value)
	{
		___instantiateIfNeeded_8 = value;
	}

	inline static int32_t get_offset_of_allObjectsLoaded_9() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t3632654792, ___allObjectsLoaded_9)); }
	inline bool get_allObjectsLoaded_9() const { return ___allObjectsLoaded_9; }
	inline bool* get_address_of_allObjectsLoaded_9() { return &___allObjectsLoaded_9; }
	inline void set_allObjectsLoaded_9(bool value)
	{
		___allObjectsLoaded_9 = value;
	}

	inline static int32_t get_offset_of_instantiatedObjects_10() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t3632654792, ___instantiatedObjects_10)); }
	inline Dictionary_2_t1474424692 * get_instantiatedObjects_10() const { return ___instantiatedObjects_10; }
	inline Dictionary_2_t1474424692 ** get_address_of_instantiatedObjects_10() { return &___instantiatedObjects_10; }
	inline void set_instantiatedObjects_10(Dictionary_2_t1474424692 * value)
	{
		___instantiatedObjects_10 = value;
		Il2CppCodeGenWriteBarrier((&___instantiatedObjects_10), value);
	}

	inline static int32_t get_offset_of_poolCursors_11() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t3632654792, ___poolCursors_11)); }
	inline Dictionary_2_t1839659084 * get_poolCursors_11() const { return ___poolCursors_11; }
	inline Dictionary_2_t1839659084 ** get_address_of_poolCursors_11() { return &___poolCursors_11; }
	inline void set_poolCursors_11(Dictionary_2_t1839659084 * value)
	{
		___poolCursors_11 = value;
		Il2CppCodeGenWriteBarrier((&___poolCursors_11), value);
	}
};

struct CFX_SpawnSystem_t3632654792_StaticFields
{
public:
	// CFX_SpawnSystem CFX_SpawnSystem::instance
	CFX_SpawnSystem_t3632654792 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(CFX_SpawnSystem_t3632654792_StaticFields, ___instance_2)); }
	inline CFX_SpawnSystem_t3632654792 * get_instance_2() const { return ___instance_2; }
	inline CFX_SpawnSystem_t3632654792 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(CFX_SpawnSystem_t3632654792 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFX_SPAWNSYSTEM_T3632654792_H
#ifndef CFX_SHURIKENTHREADFIX_T3300879551_H
#define CFX_SHURIKENTHREADFIX_T3300879551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_ShurikenThreadFix
struct  CFX_ShurikenThreadFix_t3300879551  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.ParticleSystem[] CFX_ShurikenThreadFix::systems
	ParticleSystemU5BU5D_t3089334924* ___systems_2;

public:
	inline static int32_t get_offset_of_systems_2() { return static_cast<int32_t>(offsetof(CFX_ShurikenThreadFix_t3300879551, ___systems_2)); }
	inline ParticleSystemU5BU5D_t3089334924* get_systems_2() const { return ___systems_2; }
	inline ParticleSystemU5BU5D_t3089334924** get_address_of_systems_2() { return &___systems_2; }
	inline void set_systems_2(ParticleSystemU5BU5D_t3089334924* value)
	{
		___systems_2 = value;
		Il2CppCodeGenWriteBarrier((&___systems_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFX_SHURIKENTHREADFIX_T3300879551_H
#ifndef HELLOPOLY_T535663518_H
#define HELLOPOLY_T535663518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HelloPoly
struct  HelloPoly_t535663518  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text HelloPoly::statusText
	Text_t1901882714 * ___statusText_2;

public:
	inline static int32_t get_offset_of_statusText_2() { return static_cast<int32_t>(offsetof(HelloPoly_t535663518, ___statusText_2)); }
	inline Text_t1901882714 * get_statusText_2() const { return ___statusText_2; }
	inline Text_t1901882714 ** get_address_of_statusText_2() { return &___statusText_2; }
	inline void set_statusText_2(Text_t1901882714 * value)
	{
		___statusText_2 = value;
		Il2CppCodeGenWriteBarrier((&___statusText_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HELLOPOLY_T535663518_H
#ifndef ROTATE_T1850091912_H
#define ROTATE_T1850091912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rotate
struct  Rotate_t1850091912  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 Rotate::axis
	Vector3_t3722313464  ___axis_2;
	// System.Single Rotate::angularSpeed
	float ___angularSpeed_3;

public:
	inline static int32_t get_offset_of_axis_2() { return static_cast<int32_t>(offsetof(Rotate_t1850091912, ___axis_2)); }
	inline Vector3_t3722313464  get_axis_2() const { return ___axis_2; }
	inline Vector3_t3722313464 * get_address_of_axis_2() { return &___axis_2; }
	inline void set_axis_2(Vector3_t3722313464  value)
	{
		___axis_2 = value;
	}

	inline static int32_t get_offset_of_angularSpeed_3() { return static_cast<int32_t>(offsetof(Rotate_t1850091912, ___angularSpeed_3)); }
	inline float get_angularSpeed_3() const { return ___angularSpeed_3; }
	inline float* get_address_of_angularSpeed_3() { return &___angularSpeed_3; }
	inline void set_angularSpeed_3(float value)
	{
		___angularSpeed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATE_T1850091912_H
#ifndef SHOWFEATUREDEXAMPLE_T287018513_H
#define SHOWFEATUREDEXAMPLE_T287018513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShowFeaturedExample
struct  ShowFeaturedExample_t287018513  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 ShowFeaturedExample::assetCount
	int32_t ___assetCount_2;
	// UnityEngine.UI.Text ShowFeaturedExample::attributionsText
	Text_t1901882714 * ___attributionsText_3;
	// UnityEngine.UI.Text ShowFeaturedExample::statusText
	Text_t1901882714 * ___statusText_4;

public:
	inline static int32_t get_offset_of_assetCount_2() { return static_cast<int32_t>(offsetof(ShowFeaturedExample_t287018513, ___assetCount_2)); }
	inline int32_t get_assetCount_2() const { return ___assetCount_2; }
	inline int32_t* get_address_of_assetCount_2() { return &___assetCount_2; }
	inline void set_assetCount_2(int32_t value)
	{
		___assetCount_2 = value;
	}

	inline static int32_t get_offset_of_attributionsText_3() { return static_cast<int32_t>(offsetof(ShowFeaturedExample_t287018513, ___attributionsText_3)); }
	inline Text_t1901882714 * get_attributionsText_3() const { return ___attributionsText_3; }
	inline Text_t1901882714 ** get_address_of_attributionsText_3() { return &___attributionsText_3; }
	inline void set_attributionsText_3(Text_t1901882714 * value)
	{
		___attributionsText_3 = value;
		Il2CppCodeGenWriteBarrier((&___attributionsText_3), value);
	}

	inline static int32_t get_offset_of_statusText_4() { return static_cast<int32_t>(offsetof(ShowFeaturedExample_t287018513, ___statusText_4)); }
	inline Text_t1901882714 * get_statusText_4() const { return ___statusText_4; }
	inline Text_t1901882714 ** get_address_of_statusText_4() { return &___statusText_4; }
	inline void set_statusText_4(Text_t1901882714 * value)
	{
		___statusText_4 = value;
		Il2CppCodeGenWriteBarrier((&___statusText_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWFEATUREDEXAMPLE_T287018513_H
#ifndef OAUTH2IDENTITY_T1039733419_H
#define OAUTH2IDENTITY_T1039733419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.entitlement.OAuth2Identity
struct  OAuth2Identity_t1039733419  : public MonoBehaviour_t3962482529
{
public:
	// System.String PolyToolkitInternal.entitlement.OAuth2Identity::m_ServiceName
	String_t* ___m_ServiceName_3;
	// System.String PolyToolkitInternal.entitlement.OAuth2Identity::m_ClientId
	String_t* ___m_ClientId_4;
	// System.String PolyToolkitInternal.entitlement.OAuth2Identity::m_ClientSecret
	String_t* ___m_ClientSecret_5;
	// System.String PolyToolkitInternal.entitlement.OAuth2Identity::m_OAuthScope
	String_t* ___m_OAuthScope_9;
	// System.String PolyToolkitInternal.entitlement.OAuth2Identity::m_CallbackFailedMessage
	String_t* ___m_CallbackFailedMessage_12;
	// System.String PolyToolkitInternal.entitlement.OAuth2Identity::m_PlayerPrefRefreshKey
	String_t* ___m_PlayerPrefRefreshKey_18;
	// System.String PolyToolkitInternal.entitlement.OAuth2Identity::m_AccessToken
	String_t* ___m_AccessToken_20;
	// System.String PolyToolkitInternal.entitlement.OAuth2Identity::m_RefreshToken
	String_t* ___m_RefreshToken_21;
	// PolyToolkitInternal.entitlement.OAuth2Identity/UserInfo PolyToolkitInternal.entitlement.OAuth2Identity::m_User
	UserInfo_t3564753064 * ___m_User_22;
	// System.Net.HttpListener PolyToolkitInternal.entitlement.OAuth2Identity::m_HttpListener
	HttpListener_t988452056 * ___m_HttpListener_23;
	// System.Int32 PolyToolkitInternal.entitlement.OAuth2Identity::m_HttpPort
	int32_t ___m_HttpPort_24;
	// System.Boolean PolyToolkitInternal.entitlement.OAuth2Identity::m_WaitingOnAuthorization
	bool ___m_WaitingOnAuthorization_25;
	// System.String PolyToolkitInternal.entitlement.OAuth2Identity::m_VerificationCode
	String_t* ___m_VerificationCode_26;
	// System.Boolean PolyToolkitInternal.entitlement.OAuth2Identity::m_VerificationError
	bool ___m_VerificationError_27;

public:
	inline static int32_t get_offset_of_m_ServiceName_3() { return static_cast<int32_t>(offsetof(OAuth2Identity_t1039733419, ___m_ServiceName_3)); }
	inline String_t* get_m_ServiceName_3() const { return ___m_ServiceName_3; }
	inline String_t** get_address_of_m_ServiceName_3() { return &___m_ServiceName_3; }
	inline void set_m_ServiceName_3(String_t* value)
	{
		___m_ServiceName_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_ServiceName_3), value);
	}

	inline static int32_t get_offset_of_m_ClientId_4() { return static_cast<int32_t>(offsetof(OAuth2Identity_t1039733419, ___m_ClientId_4)); }
	inline String_t* get_m_ClientId_4() const { return ___m_ClientId_4; }
	inline String_t** get_address_of_m_ClientId_4() { return &___m_ClientId_4; }
	inline void set_m_ClientId_4(String_t* value)
	{
		___m_ClientId_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClientId_4), value);
	}

	inline static int32_t get_offset_of_m_ClientSecret_5() { return static_cast<int32_t>(offsetof(OAuth2Identity_t1039733419, ___m_ClientSecret_5)); }
	inline String_t* get_m_ClientSecret_5() const { return ___m_ClientSecret_5; }
	inline String_t** get_address_of_m_ClientSecret_5() { return &___m_ClientSecret_5; }
	inline void set_m_ClientSecret_5(String_t* value)
	{
		___m_ClientSecret_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClientSecret_5), value);
	}

	inline static int32_t get_offset_of_m_OAuthScope_9() { return static_cast<int32_t>(offsetof(OAuth2Identity_t1039733419, ___m_OAuthScope_9)); }
	inline String_t* get_m_OAuthScope_9() const { return ___m_OAuthScope_9; }
	inline String_t** get_address_of_m_OAuthScope_9() { return &___m_OAuthScope_9; }
	inline void set_m_OAuthScope_9(String_t* value)
	{
		___m_OAuthScope_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_OAuthScope_9), value);
	}

	inline static int32_t get_offset_of_m_CallbackFailedMessage_12() { return static_cast<int32_t>(offsetof(OAuth2Identity_t1039733419, ___m_CallbackFailedMessage_12)); }
	inline String_t* get_m_CallbackFailedMessage_12() const { return ___m_CallbackFailedMessage_12; }
	inline String_t** get_address_of_m_CallbackFailedMessage_12() { return &___m_CallbackFailedMessage_12; }
	inline void set_m_CallbackFailedMessage_12(String_t* value)
	{
		___m_CallbackFailedMessage_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_CallbackFailedMessage_12), value);
	}

	inline static int32_t get_offset_of_m_PlayerPrefRefreshKey_18() { return static_cast<int32_t>(offsetof(OAuth2Identity_t1039733419, ___m_PlayerPrefRefreshKey_18)); }
	inline String_t* get_m_PlayerPrefRefreshKey_18() const { return ___m_PlayerPrefRefreshKey_18; }
	inline String_t** get_address_of_m_PlayerPrefRefreshKey_18() { return &___m_PlayerPrefRefreshKey_18; }
	inline void set_m_PlayerPrefRefreshKey_18(String_t* value)
	{
		___m_PlayerPrefRefreshKey_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerPrefRefreshKey_18), value);
	}

	inline static int32_t get_offset_of_m_AccessToken_20() { return static_cast<int32_t>(offsetof(OAuth2Identity_t1039733419, ___m_AccessToken_20)); }
	inline String_t* get_m_AccessToken_20() const { return ___m_AccessToken_20; }
	inline String_t** get_address_of_m_AccessToken_20() { return &___m_AccessToken_20; }
	inline void set_m_AccessToken_20(String_t* value)
	{
		___m_AccessToken_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_AccessToken_20), value);
	}

	inline static int32_t get_offset_of_m_RefreshToken_21() { return static_cast<int32_t>(offsetof(OAuth2Identity_t1039733419, ___m_RefreshToken_21)); }
	inline String_t* get_m_RefreshToken_21() const { return ___m_RefreshToken_21; }
	inline String_t** get_address_of_m_RefreshToken_21() { return &___m_RefreshToken_21; }
	inline void set_m_RefreshToken_21(String_t* value)
	{
		___m_RefreshToken_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_RefreshToken_21), value);
	}

	inline static int32_t get_offset_of_m_User_22() { return static_cast<int32_t>(offsetof(OAuth2Identity_t1039733419, ___m_User_22)); }
	inline UserInfo_t3564753064 * get_m_User_22() const { return ___m_User_22; }
	inline UserInfo_t3564753064 ** get_address_of_m_User_22() { return &___m_User_22; }
	inline void set_m_User_22(UserInfo_t3564753064 * value)
	{
		___m_User_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_User_22), value);
	}

	inline static int32_t get_offset_of_m_HttpListener_23() { return static_cast<int32_t>(offsetof(OAuth2Identity_t1039733419, ___m_HttpListener_23)); }
	inline HttpListener_t988452056 * get_m_HttpListener_23() const { return ___m_HttpListener_23; }
	inline HttpListener_t988452056 ** get_address_of_m_HttpListener_23() { return &___m_HttpListener_23; }
	inline void set_m_HttpListener_23(HttpListener_t988452056 * value)
	{
		___m_HttpListener_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_HttpListener_23), value);
	}

	inline static int32_t get_offset_of_m_HttpPort_24() { return static_cast<int32_t>(offsetof(OAuth2Identity_t1039733419, ___m_HttpPort_24)); }
	inline int32_t get_m_HttpPort_24() const { return ___m_HttpPort_24; }
	inline int32_t* get_address_of_m_HttpPort_24() { return &___m_HttpPort_24; }
	inline void set_m_HttpPort_24(int32_t value)
	{
		___m_HttpPort_24 = value;
	}

	inline static int32_t get_offset_of_m_WaitingOnAuthorization_25() { return static_cast<int32_t>(offsetof(OAuth2Identity_t1039733419, ___m_WaitingOnAuthorization_25)); }
	inline bool get_m_WaitingOnAuthorization_25() const { return ___m_WaitingOnAuthorization_25; }
	inline bool* get_address_of_m_WaitingOnAuthorization_25() { return &___m_WaitingOnAuthorization_25; }
	inline void set_m_WaitingOnAuthorization_25(bool value)
	{
		___m_WaitingOnAuthorization_25 = value;
	}

	inline static int32_t get_offset_of_m_VerificationCode_26() { return static_cast<int32_t>(offsetof(OAuth2Identity_t1039733419, ___m_VerificationCode_26)); }
	inline String_t* get_m_VerificationCode_26() const { return ___m_VerificationCode_26; }
	inline String_t** get_address_of_m_VerificationCode_26() { return &___m_VerificationCode_26; }
	inline void set_m_VerificationCode_26(String_t* value)
	{
		___m_VerificationCode_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerificationCode_26), value);
	}

	inline static int32_t get_offset_of_m_VerificationError_27() { return static_cast<int32_t>(offsetof(OAuth2Identity_t1039733419, ___m_VerificationError_27)); }
	inline bool get_m_VerificationError_27() const { return ___m_VerificationError_27; }
	inline bool* get_address_of_m_VerificationError_27() { return &___m_VerificationError_27; }
	inline void set_m_VerificationError_27(bool value)
	{
		___m_VerificationError_27 = value;
	}
};

struct OAuth2Identity_t1039733419_StaticFields
{
public:
	// UnityEngine.Color PolyToolkitInternal.entitlement.OAuth2Identity::UI_BACKGROUND_COLOR
	Color_t2555686324  ___UI_BACKGROUND_COLOR_14;
	// PolyToolkitInternal.entitlement.OAuth2Identity PolyToolkitInternal.entitlement.OAuth2Identity::Instance
	OAuth2Identity_t1039733419 * ___Instance_15;
	// System.Action PolyToolkitInternal.entitlement.OAuth2Identity::m_OnProfileUpdated
	Action_t1264377477 * ___m_OnProfileUpdated_16;
	// System.String PolyToolkitInternal.entitlement.OAuth2Identity::PLAYER_PREF_REFRESH_KEY_SUFFIX
	String_t* ___PLAYER_PREF_REFRESH_KEY_SUFFIX_17;

public:
	inline static int32_t get_offset_of_UI_BACKGROUND_COLOR_14() { return static_cast<int32_t>(offsetof(OAuth2Identity_t1039733419_StaticFields, ___UI_BACKGROUND_COLOR_14)); }
	inline Color_t2555686324  get_UI_BACKGROUND_COLOR_14() const { return ___UI_BACKGROUND_COLOR_14; }
	inline Color_t2555686324 * get_address_of_UI_BACKGROUND_COLOR_14() { return &___UI_BACKGROUND_COLOR_14; }
	inline void set_UI_BACKGROUND_COLOR_14(Color_t2555686324  value)
	{
		___UI_BACKGROUND_COLOR_14 = value;
	}

	inline static int32_t get_offset_of_Instance_15() { return static_cast<int32_t>(offsetof(OAuth2Identity_t1039733419_StaticFields, ___Instance_15)); }
	inline OAuth2Identity_t1039733419 * get_Instance_15() const { return ___Instance_15; }
	inline OAuth2Identity_t1039733419 ** get_address_of_Instance_15() { return &___Instance_15; }
	inline void set_Instance_15(OAuth2Identity_t1039733419 * value)
	{
		___Instance_15 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_15), value);
	}

	inline static int32_t get_offset_of_m_OnProfileUpdated_16() { return static_cast<int32_t>(offsetof(OAuth2Identity_t1039733419_StaticFields, ___m_OnProfileUpdated_16)); }
	inline Action_t1264377477 * get_m_OnProfileUpdated_16() const { return ___m_OnProfileUpdated_16; }
	inline Action_t1264377477 ** get_address_of_m_OnProfileUpdated_16() { return &___m_OnProfileUpdated_16; }
	inline void set_m_OnProfileUpdated_16(Action_t1264377477 * value)
	{
		___m_OnProfileUpdated_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnProfileUpdated_16), value);
	}

	inline static int32_t get_offset_of_PLAYER_PREF_REFRESH_KEY_SUFFIX_17() { return static_cast<int32_t>(offsetof(OAuth2Identity_t1039733419_StaticFields, ___PLAYER_PREF_REFRESH_KEY_SUFFIX_17)); }
	inline String_t* get_PLAYER_PREF_REFRESH_KEY_SUFFIX_17() const { return ___PLAYER_PREF_REFRESH_KEY_SUFFIX_17; }
	inline String_t** get_address_of_PLAYER_PREF_REFRESH_KEY_SUFFIX_17() { return &___PLAYER_PREF_REFRESH_KEY_SUFFIX_17; }
	inline void set_PLAYER_PREF_REFRESH_KEY_SUFFIX_17(String_t* value)
	{
		___PLAYER_PREF_REFRESH_KEY_SUFFIX_17 = value;
		Il2CppCodeGenWriteBarrier((&___PLAYER_PREF_REFRESH_KEY_SUFFIX_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OAUTH2IDENTITY_T1039733419_H
#ifndef PERSISTENTBLOBCACHE_T1212092596_H
#define PERSISTENTBLOBCACHE_T1212092596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.caching.PersistentBlobCache
struct  PersistentBlobCache_t1212092596  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean PolyToolkitInternal.caching.PersistentBlobCache::setupDone
	bool ___setupDone_3;
	// System.Int32 PolyToolkitInternal.caching.PersistentBlobCache::maxEntries
	int32_t ___maxEntries_4;
	// System.Int64 PolyToolkitInternal.caching.PersistentBlobCache::maxSizeBytes
	int64_t ___maxSizeBytes_5;
	// System.String PolyToolkitInternal.caching.PersistentBlobCache::rootPath
	String_t* ___rootPath_6;
	// System.Security.Cryptography.MD5 PolyToolkitInternal.caching.PersistentBlobCache::md5
	MD5_t3177620429 * ___md5_7;
	// System.Collections.Generic.Dictionary`2<System.String,PolyToolkitInternal.caching.PersistentBlobCache/CacheEntry> PolyToolkitInternal.caching.PersistentBlobCache::cacheEntries
	Dictionary_2_t3747976592 * ___cacheEntries_8;
	// PolyToolkitInternal.model.util.ConcurrentQueue`1<PolyToolkitInternal.caching.PersistentBlobCache/CacheRequest> PolyToolkitInternal.caching.PersistentBlobCache::requestsPendingWork
	ConcurrentQueue_1_t441838195 * ___requestsPendingWork_9;
	// PolyToolkitInternal.model.util.ConcurrentQueue`1<PolyToolkitInternal.caching.PersistentBlobCache/CacheRequest> PolyToolkitInternal.caching.PersistentBlobCache::requestsPendingDelivery
	ConcurrentQueue_1_t441838195 * ___requestsPendingDelivery_10;
	// PolyToolkitInternal.model.util.ConcurrentQueue`1<PolyToolkitInternal.caching.PersistentBlobCache/CacheRequest> PolyToolkitInternal.caching.PersistentBlobCache::requestsRecyclePool
	ConcurrentQueue_1_t441838195 * ___requestsRecyclePool_11;

public:
	inline static int32_t get_offset_of_setupDone_3() { return static_cast<int32_t>(offsetof(PersistentBlobCache_t1212092596, ___setupDone_3)); }
	inline bool get_setupDone_3() const { return ___setupDone_3; }
	inline bool* get_address_of_setupDone_3() { return &___setupDone_3; }
	inline void set_setupDone_3(bool value)
	{
		___setupDone_3 = value;
	}

	inline static int32_t get_offset_of_maxEntries_4() { return static_cast<int32_t>(offsetof(PersistentBlobCache_t1212092596, ___maxEntries_4)); }
	inline int32_t get_maxEntries_4() const { return ___maxEntries_4; }
	inline int32_t* get_address_of_maxEntries_4() { return &___maxEntries_4; }
	inline void set_maxEntries_4(int32_t value)
	{
		___maxEntries_4 = value;
	}

	inline static int32_t get_offset_of_maxSizeBytes_5() { return static_cast<int32_t>(offsetof(PersistentBlobCache_t1212092596, ___maxSizeBytes_5)); }
	inline int64_t get_maxSizeBytes_5() const { return ___maxSizeBytes_5; }
	inline int64_t* get_address_of_maxSizeBytes_5() { return &___maxSizeBytes_5; }
	inline void set_maxSizeBytes_5(int64_t value)
	{
		___maxSizeBytes_5 = value;
	}

	inline static int32_t get_offset_of_rootPath_6() { return static_cast<int32_t>(offsetof(PersistentBlobCache_t1212092596, ___rootPath_6)); }
	inline String_t* get_rootPath_6() const { return ___rootPath_6; }
	inline String_t** get_address_of_rootPath_6() { return &___rootPath_6; }
	inline void set_rootPath_6(String_t* value)
	{
		___rootPath_6 = value;
		Il2CppCodeGenWriteBarrier((&___rootPath_6), value);
	}

	inline static int32_t get_offset_of_md5_7() { return static_cast<int32_t>(offsetof(PersistentBlobCache_t1212092596, ___md5_7)); }
	inline MD5_t3177620429 * get_md5_7() const { return ___md5_7; }
	inline MD5_t3177620429 ** get_address_of_md5_7() { return &___md5_7; }
	inline void set_md5_7(MD5_t3177620429 * value)
	{
		___md5_7 = value;
		Il2CppCodeGenWriteBarrier((&___md5_7), value);
	}

	inline static int32_t get_offset_of_cacheEntries_8() { return static_cast<int32_t>(offsetof(PersistentBlobCache_t1212092596, ___cacheEntries_8)); }
	inline Dictionary_2_t3747976592 * get_cacheEntries_8() const { return ___cacheEntries_8; }
	inline Dictionary_2_t3747976592 ** get_address_of_cacheEntries_8() { return &___cacheEntries_8; }
	inline void set_cacheEntries_8(Dictionary_2_t3747976592 * value)
	{
		___cacheEntries_8 = value;
		Il2CppCodeGenWriteBarrier((&___cacheEntries_8), value);
	}

	inline static int32_t get_offset_of_requestsPendingWork_9() { return static_cast<int32_t>(offsetof(PersistentBlobCache_t1212092596, ___requestsPendingWork_9)); }
	inline ConcurrentQueue_1_t441838195 * get_requestsPendingWork_9() const { return ___requestsPendingWork_9; }
	inline ConcurrentQueue_1_t441838195 ** get_address_of_requestsPendingWork_9() { return &___requestsPendingWork_9; }
	inline void set_requestsPendingWork_9(ConcurrentQueue_1_t441838195 * value)
	{
		___requestsPendingWork_9 = value;
		Il2CppCodeGenWriteBarrier((&___requestsPendingWork_9), value);
	}

	inline static int32_t get_offset_of_requestsPendingDelivery_10() { return static_cast<int32_t>(offsetof(PersistentBlobCache_t1212092596, ___requestsPendingDelivery_10)); }
	inline ConcurrentQueue_1_t441838195 * get_requestsPendingDelivery_10() const { return ___requestsPendingDelivery_10; }
	inline ConcurrentQueue_1_t441838195 ** get_address_of_requestsPendingDelivery_10() { return &___requestsPendingDelivery_10; }
	inline void set_requestsPendingDelivery_10(ConcurrentQueue_1_t441838195 * value)
	{
		___requestsPendingDelivery_10 = value;
		Il2CppCodeGenWriteBarrier((&___requestsPendingDelivery_10), value);
	}

	inline static int32_t get_offset_of_requestsRecyclePool_11() { return static_cast<int32_t>(offsetof(PersistentBlobCache_t1212092596, ___requestsRecyclePool_11)); }
	inline ConcurrentQueue_1_t441838195 * get_requestsRecyclePool_11() const { return ___requestsRecyclePool_11; }
	inline ConcurrentQueue_1_t441838195 ** get_address_of_requestsRecyclePool_11() { return &___requestsRecyclePool_11; }
	inline void set_requestsRecyclePool_11(ConcurrentQueue_1_t441838195 * value)
	{
		___requestsRecyclePool_11 = value;
		Il2CppCodeGenWriteBarrier((&___requestsRecyclePool_11), value);
	}
};

struct PersistentBlobCache_t1212092596_StaticFields
{
public:
	// System.Func`2<PolyToolkitInternal.caching.PersistentBlobCache/CacheEntry,System.Int64> PolyToolkitInternal.caching.PersistentBlobCache::<>f__am$cache0
	Func_2_t1163785221 * ___U3CU3Ef__amU24cache0_12;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_12() { return static_cast<int32_t>(offsetof(PersistentBlobCache_t1212092596_StaticFields, ___U3CU3Ef__amU24cache0_12)); }
	inline Func_2_t1163785221 * get_U3CU3Ef__amU24cache0_12() const { return ___U3CU3Ef__amU24cache0_12; }
	inline Func_2_t1163785221 ** get_address_of_U3CU3Ef__amU24cache0_12() { return &___U3CU3Ef__amU24cache0_12; }
	inline void set_U3CU3Ef__amU24cache0_12(Func_2_t1163785221 * value)
	{
		___U3CU3Ef__amU24cache0_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERSISTENTBLOBCACHE_T1212092596_H
#ifndef POLYCLIENT_T2759672288_H
#define POLYCLIENT_T2759672288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.api_clients.poly_client.PolyClient
struct  PolyClient_t2759672288  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct PolyClient_t2759672288_StaticFields
{
public:
	// System.String PolyToolkitInternal.api_clients.poly_client.PolyClient::BASE_URL
	String_t* ___BASE_URL_3;
	// System.Collections.Generic.Dictionary`2<PolyToolkit.PolyCategory,System.String> PolyToolkitInternal.api_clients.poly_client.PolyClient::CATEGORIES
	Dictionary_2_t3025884774 * ___CATEGORIES_4;
	// System.Collections.Generic.Dictionary`2<PolyToolkit.PolyOrderBy,System.String> PolyToolkitInternal.api_clients.poly_client.PolyClient::ORDER_BY
	Dictionary_2_t247703358 * ___ORDER_BY_5;
	// System.Collections.Generic.Dictionary`2<PolyToolkit.PolyFormatFilter,System.String> PolyToolkitInternal.api_clients.poly_client.PolyClient::FORMAT_FILTER
	Dictionary_2_t3377995330 * ___FORMAT_FILTER_6;
	// System.Collections.Generic.Dictionary`2<PolyToolkit.PolyVisibilityFilter,System.String> PolyToolkitInternal.api_clients.poly_client.PolyClient::VISIBILITY
	Dictionary_2_t1991905614 * ___VISIBILITY_7;
	// System.Collections.Generic.Dictionary`2<PolyToolkit.PolyMaxComplexityFilter,System.String> PolyToolkitInternal.api_clients.poly_client.PolyClient::MAX_COMPLEXITY
	Dictionary_2_t2835058718 * ___MAX_COMPLEXITY_8;

public:
	inline static int32_t get_offset_of_BASE_URL_3() { return static_cast<int32_t>(offsetof(PolyClient_t2759672288_StaticFields, ___BASE_URL_3)); }
	inline String_t* get_BASE_URL_3() const { return ___BASE_URL_3; }
	inline String_t** get_address_of_BASE_URL_3() { return &___BASE_URL_3; }
	inline void set_BASE_URL_3(String_t* value)
	{
		___BASE_URL_3 = value;
		Il2CppCodeGenWriteBarrier((&___BASE_URL_3), value);
	}

	inline static int32_t get_offset_of_CATEGORIES_4() { return static_cast<int32_t>(offsetof(PolyClient_t2759672288_StaticFields, ___CATEGORIES_4)); }
	inline Dictionary_2_t3025884774 * get_CATEGORIES_4() const { return ___CATEGORIES_4; }
	inline Dictionary_2_t3025884774 ** get_address_of_CATEGORIES_4() { return &___CATEGORIES_4; }
	inline void set_CATEGORIES_4(Dictionary_2_t3025884774 * value)
	{
		___CATEGORIES_4 = value;
		Il2CppCodeGenWriteBarrier((&___CATEGORIES_4), value);
	}

	inline static int32_t get_offset_of_ORDER_BY_5() { return static_cast<int32_t>(offsetof(PolyClient_t2759672288_StaticFields, ___ORDER_BY_5)); }
	inline Dictionary_2_t247703358 * get_ORDER_BY_5() const { return ___ORDER_BY_5; }
	inline Dictionary_2_t247703358 ** get_address_of_ORDER_BY_5() { return &___ORDER_BY_5; }
	inline void set_ORDER_BY_5(Dictionary_2_t247703358 * value)
	{
		___ORDER_BY_5 = value;
		Il2CppCodeGenWriteBarrier((&___ORDER_BY_5), value);
	}

	inline static int32_t get_offset_of_FORMAT_FILTER_6() { return static_cast<int32_t>(offsetof(PolyClient_t2759672288_StaticFields, ___FORMAT_FILTER_6)); }
	inline Dictionary_2_t3377995330 * get_FORMAT_FILTER_6() const { return ___FORMAT_FILTER_6; }
	inline Dictionary_2_t3377995330 ** get_address_of_FORMAT_FILTER_6() { return &___FORMAT_FILTER_6; }
	inline void set_FORMAT_FILTER_6(Dictionary_2_t3377995330 * value)
	{
		___FORMAT_FILTER_6 = value;
		Il2CppCodeGenWriteBarrier((&___FORMAT_FILTER_6), value);
	}

	inline static int32_t get_offset_of_VISIBILITY_7() { return static_cast<int32_t>(offsetof(PolyClient_t2759672288_StaticFields, ___VISIBILITY_7)); }
	inline Dictionary_2_t1991905614 * get_VISIBILITY_7() const { return ___VISIBILITY_7; }
	inline Dictionary_2_t1991905614 ** get_address_of_VISIBILITY_7() { return &___VISIBILITY_7; }
	inline void set_VISIBILITY_7(Dictionary_2_t1991905614 * value)
	{
		___VISIBILITY_7 = value;
		Il2CppCodeGenWriteBarrier((&___VISIBILITY_7), value);
	}

	inline static int32_t get_offset_of_MAX_COMPLEXITY_8() { return static_cast<int32_t>(offsetof(PolyClient_t2759672288_StaticFields, ___MAX_COMPLEXITY_8)); }
	inline Dictionary_2_t2835058718 * get_MAX_COMPLEXITY_8() const { return ___MAX_COMPLEXITY_8; }
	inline Dictionary_2_t2835058718 ** get_address_of_MAX_COMPLEXITY_8() { return &___MAX_COMPLEXITY_8; }
	inline void set_MAX_COMPLEXITY_8(Dictionary_2_t2835058718 * value)
	{
		___MAX_COMPLEXITY_8 = value;
		Il2CppCodeGenWriteBarrier((&___MAX_COMPLEXITY_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYCLIENT_T2759672288_H
#ifndef AUTHENTICATOR_T1330886707_H
#define AUTHENTICATOR_T1330886707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.Authenticator
struct  Authenticator_t1330886707  : public MonoBehaviour_t3962482529
{
public:
	// PolyToolkitInternal.entitlement.OAuth2Identity PolyToolkitInternal.Authenticator::oauth2Identity
	OAuth2Identity_t1039733419 * ___oauth2Identity_3;

public:
	inline static int32_t get_offset_of_oauth2Identity_3() { return static_cast<int32_t>(offsetof(Authenticator_t1330886707, ___oauth2Identity_3)); }
	inline OAuth2Identity_t1039733419 * get_oauth2Identity_3() const { return ___oauth2Identity_3; }
	inline OAuth2Identity_t1039733419 ** get_address_of_oauth2Identity_3() { return &___oauth2Identity_3; }
	inline void set_oauth2Identity_3(OAuth2Identity_t1039733419 * value)
	{
		___oauth2Identity_3 = value;
		Il2CppCodeGenWriteBarrier((&___oauth2Identity_3), value);
	}
};

struct Authenticator_t1330886707_StaticFields
{
public:
	// PolyToolkitInternal.Authenticator PolyToolkitInternal.Authenticator::instance
	Authenticator_t1330886707 * ___instance_2;
	// System.Net.Security.RemoteCertificateValidationCallback PolyToolkitInternal.Authenticator::<>f__am$cache0
	RemoteCertificateValidationCallback_t3014364904 * ___U3CU3Ef__amU24cache0_4;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Authenticator_t1330886707_StaticFields, ___instance_2)); }
	inline Authenticator_t1330886707 * get_instance_2() const { return ___instance_2; }
	inline Authenticator_t1330886707 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(Authenticator_t1330886707 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(Authenticator_t1330886707_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline RemoteCertificateValidationCallback_t3014364904 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline RemoteCertificateValidationCallback_t3014364904 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(RemoteCertificateValidationCallback_t3014364904 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATOR_T1330886707_H
#ifndef ASYNCIMPORTER_T2267690090_H
#define ASYNCIMPORTER_T2267690090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyToolkitInternal.AsyncImporter
struct  AsyncImporter_t2267690090  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.Queue`1<PolyToolkitInternal.AsyncImporter/ImportOperation> PolyToolkitInternal.AsyncImporter::finishedOperations
	Queue_1_t1676077817 * ___finishedOperations_2;
	// System.Object PolyToolkitInternal.AsyncImporter::finishedOperationsLock
	RuntimeObject * ___finishedOperationsLock_3;

public:
	inline static int32_t get_offset_of_finishedOperations_2() { return static_cast<int32_t>(offsetof(AsyncImporter_t2267690090, ___finishedOperations_2)); }
	inline Queue_1_t1676077817 * get_finishedOperations_2() const { return ___finishedOperations_2; }
	inline Queue_1_t1676077817 ** get_address_of_finishedOperations_2() { return &___finishedOperations_2; }
	inline void set_finishedOperations_2(Queue_1_t1676077817 * value)
	{
		___finishedOperations_2 = value;
		Il2CppCodeGenWriteBarrier((&___finishedOperations_2), value);
	}

	inline static int32_t get_offset_of_finishedOperationsLock_3() { return static_cast<int32_t>(offsetof(AsyncImporter_t2267690090, ___finishedOperationsLock_3)); }
	inline RuntimeObject * get_finishedOperationsLock_3() const { return ___finishedOperationsLock_3; }
	inline RuntimeObject ** get_address_of_finishedOperationsLock_3() { return &___finishedOperationsLock_3; }
	inline void set_finishedOperationsLock_3(RuntimeObject * value)
	{
		___finishedOperationsLock_3 = value;
		Il2CppCodeGenWriteBarrier((&___finishedOperationsLock_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCIMPORTER_T2267690090_H
#ifndef CFX_LIGHTINTENSITYFADE_T1589272611_H
#define CFX_LIGHTINTENSITYFADE_T1589272611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CFX_LightIntensityFade
struct  CFX_LightIntensityFade_t1589272611  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CFX_LightIntensityFade::duration
	float ___duration_2;
	// System.Single CFX_LightIntensityFade::delay
	float ___delay_3;
	// System.Single CFX_LightIntensityFade::finalIntensity
	float ___finalIntensity_4;
	// System.Single CFX_LightIntensityFade::baseIntensity
	float ___baseIntensity_5;
	// System.Boolean CFX_LightIntensityFade::autodestruct
	bool ___autodestruct_6;
	// System.Single CFX_LightIntensityFade::p_lifetime
	float ___p_lifetime_7;
	// System.Single CFX_LightIntensityFade::p_delay
	float ___p_delay_8;

public:
	inline static int32_t get_offset_of_duration_2() { return static_cast<int32_t>(offsetof(CFX_LightIntensityFade_t1589272611, ___duration_2)); }
	inline float get_duration_2() const { return ___duration_2; }
	inline float* get_address_of_duration_2() { return &___duration_2; }
	inline void set_duration_2(float value)
	{
		___duration_2 = value;
	}

	inline static int32_t get_offset_of_delay_3() { return static_cast<int32_t>(offsetof(CFX_LightIntensityFade_t1589272611, ___delay_3)); }
	inline float get_delay_3() const { return ___delay_3; }
	inline float* get_address_of_delay_3() { return &___delay_3; }
	inline void set_delay_3(float value)
	{
		___delay_3 = value;
	}

	inline static int32_t get_offset_of_finalIntensity_4() { return static_cast<int32_t>(offsetof(CFX_LightIntensityFade_t1589272611, ___finalIntensity_4)); }
	inline float get_finalIntensity_4() const { return ___finalIntensity_4; }
	inline float* get_address_of_finalIntensity_4() { return &___finalIntensity_4; }
	inline void set_finalIntensity_4(float value)
	{
		___finalIntensity_4 = value;
	}

	inline static int32_t get_offset_of_baseIntensity_5() { return static_cast<int32_t>(offsetof(CFX_LightIntensityFade_t1589272611, ___baseIntensity_5)); }
	inline float get_baseIntensity_5() const { return ___baseIntensity_5; }
	inline float* get_address_of_baseIntensity_5() { return &___baseIntensity_5; }
	inline void set_baseIntensity_5(float value)
	{
		___baseIntensity_5 = value;
	}

	inline static int32_t get_offset_of_autodestruct_6() { return static_cast<int32_t>(offsetof(CFX_LightIntensityFade_t1589272611, ___autodestruct_6)); }
	inline bool get_autodestruct_6() const { return ___autodestruct_6; }
	inline bool* get_address_of_autodestruct_6() { return &___autodestruct_6; }
	inline void set_autodestruct_6(bool value)
	{
		___autodestruct_6 = value;
	}

	inline static int32_t get_offset_of_p_lifetime_7() { return static_cast<int32_t>(offsetof(CFX_LightIntensityFade_t1589272611, ___p_lifetime_7)); }
	inline float get_p_lifetime_7() const { return ___p_lifetime_7; }
	inline float* get_address_of_p_lifetime_7() { return &___p_lifetime_7; }
	inline void set_p_lifetime_7(float value)
	{
		___p_lifetime_7 = value;
	}

	inline static int32_t get_offset_of_p_delay_8() { return static_cast<int32_t>(offsetof(CFX_LightIntensityFade_t1589272611, ___p_delay_8)); }
	inline float get_p_delay_8() const { return ___p_delay_8; }
	inline float* get_address_of_p_delay_8() { return &___p_delay_8; }
	inline void set_p_delay_8(float value)
	{
		___p_delay_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CFX_LIGHTINTENSITYFADE_T1589272611_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3500 = { sizeof (CFX_LightIntensityFade_t1589272611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3500[7] = 
{
	CFX_LightIntensityFade_t1589272611::get_offset_of_duration_2(),
	CFX_LightIntensityFade_t1589272611::get_offset_of_delay_3(),
	CFX_LightIntensityFade_t1589272611::get_offset_of_finalIntensity_4(),
	CFX_LightIntensityFade_t1589272611::get_offset_of_baseIntensity_5(),
	CFX_LightIntensityFade_t1589272611::get_offset_of_autodestruct_6(),
	CFX_LightIntensityFade_t1589272611::get_offset_of_p_lifetime_7(),
	CFX_LightIntensityFade_t1589272611::get_offset_of_p_delay_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3501 = { sizeof (CFX_ShurikenThreadFix_t3300879551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3501[1] = 
{
	CFX_ShurikenThreadFix_t3300879551::get_offset_of_systems_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3502 = { sizeof (U3CWaitFrameU3Ec__Iterator0_t2962725221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3502[6] = 
{
	U3CWaitFrameU3Ec__Iterator0_t2962725221::get_offset_of_U24locvar0_0(),
	U3CWaitFrameU3Ec__Iterator0_t2962725221::get_offset_of_U24locvar1_1(),
	U3CWaitFrameU3Ec__Iterator0_t2962725221::get_offset_of_U24this_2(),
	U3CWaitFrameU3Ec__Iterator0_t2962725221::get_offset_of_U24current_3(),
	U3CWaitFrameU3Ec__Iterator0_t2962725221::get_offset_of_U24disposing_4(),
	U3CWaitFrameU3Ec__Iterator0_t2962725221::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3503 = { sizeof (CFX_SpawnSystem_t3632654792), -1, sizeof(CFX_SpawnSystem_t3632654792_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3503[10] = 
{
	CFX_SpawnSystem_t3632654792_StaticFields::get_offset_of_instance_2(),
	CFX_SpawnSystem_t3632654792::get_offset_of_objectsToPreload_3(),
	CFX_SpawnSystem_t3632654792::get_offset_of_objectsToPreloadTimes_4(),
	CFX_SpawnSystem_t3632654792::get_offset_of_hideObjectsInHierarchy_5(),
	CFX_SpawnSystem_t3632654792::get_offset_of_spawnAsChildren_6(),
	CFX_SpawnSystem_t3632654792::get_offset_of_onlyGetInactiveObjects_7(),
	CFX_SpawnSystem_t3632654792::get_offset_of_instantiateIfNeeded_8(),
	CFX_SpawnSystem_t3632654792::get_offset_of_allObjectsLoaded_9(),
	CFX_SpawnSystem_t3632654792::get_offset_of_instantiatedObjects_10(),
	CFX_SpawnSystem_t3632654792::get_offset_of_poolCursors_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3504 = { sizeof (HelloPoly_t535663518), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3504[1] = 
{
	HelloPoly_t535663518::get_offset_of_statusText_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3505 = { sizeof (Rotate_t1850091912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3505[2] = 
{
	Rotate_t1850091912::get_offset_of_axis_2(),
	Rotate_t1850091912::get_offset_of_angularSpeed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3506 = { sizeof (ShowFeaturedExample_t287018513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3506[3] = 
{
	ShowFeaturedExample_t287018513::get_offset_of_assetCount_2(),
	ShowFeaturedExample_t287018513::get_offset_of_attributionsText_3(),
	ShowFeaturedExample_t287018513::get_offset_of_statusText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3507 = { sizeof (ParseAssetsBackgroundWork_t2464360963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3507[4] = 
{
	ParseAssetsBackgroundWork_t2464360963::get_offset_of_response_0(),
	ParseAssetsBackgroundWork_t2464360963::get_offset_of_status_1(),
	ParseAssetsBackgroundWork_t2464360963::get_offset_of_callback_2(),
	ParseAssetsBackgroundWork_t2464360963::get_offset_of_polyListAssetsResult_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3508 = { sizeof (ParseAssetBackgroundWork_t3845740228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3508[4] = 
{
	ParseAssetBackgroundWork_t3845740228::get_offset_of_response_0(),
	ParseAssetBackgroundWork_t3845740228::get_offset_of_callback_1(),
	ParseAssetBackgroundWork_t3845740228::get_offset_of_status_2(),
	ParseAssetBackgroundWork_t3845740228::get_offset_of_polyAsset_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3509 = { sizeof (PolyClient_t2759672288), -1, sizeof(PolyClient_t2759672288_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3509[7] = 
{
	0,
	PolyClient_t2759672288_StaticFields::get_offset_of_BASE_URL_3(),
	PolyClient_t2759672288_StaticFields::get_offset_of_CATEGORIES_4(),
	PolyClient_t2759672288_StaticFields::get_offset_of_ORDER_BY_5(),
	PolyClient_t2759672288_StaticFields::get_offset_of_FORMAT_FILTER_6(),
	PolyClient_t2759672288_StaticFields::get_offset_of_VISIBILITY_7(),
	PolyClient_t2759672288_StaticFields::get_offset_of_MAX_COMPLEXITY_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3510 = { sizeof (U3CSendRequestU3Ec__AnonStorey0_t684512095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3510[5] = 
{
	U3CSendRequestU3Ec__AnonStorey0_t684512095::get_offset_of_request_0(),
	U3CSendRequestU3Ec__AnonStorey0_t684512095::get_offset_of_isRecursion_1(),
	U3CSendRequestU3Ec__AnonStorey0_t684512095::get_offset_of_callback_2(),
	U3CSendRequestU3Ec__AnonStorey0_t684512095::get_offset_of_maxCacheAge_3(),
	U3CSendRequestU3Ec__AnonStorey0_t684512095::get_offset_of_U24this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3511 = { sizeof (U3CGetAssetU3Ec__AnonStorey1_t1596776758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3511[4] = 
{
	U3CGetAssetU3Ec__AnonStorey1_t1596776758::get_offset_of_assetId_0(),
	U3CGetAssetU3Ec__AnonStorey1_t1596776758::get_offset_of_isRecursion_1(),
	U3CGetAssetU3Ec__AnonStorey1_t1596776758::get_offset_of_callback_2(),
	U3CGetAssetU3Ec__AnonStorey1_t1596776758::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3512 = { sizeof (PolyClientUtils_t2942259902), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3513 = { sizeof (GetRawFileDataTextCallback_t2904487790), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3514 = { sizeof (GetRawFileDataBytesCallback_t2880187408), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3515 = { sizeof (U3CGetRawFileTextU3Ec__AnonStorey0_t1419660627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3515[3] = 
{
	U3CGetRawFileTextU3Ec__AnonStorey0_t1419660627::get_offset_of_dataUrl_0(),
	U3CGetRawFileTextU3Ec__AnonStorey0_t1419660627::get_offset_of_accessToken_1(),
	U3CGetRawFileTextU3Ec__AnonStorey0_t1419660627::get_offset_of_callback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3516 = { sizeof (U3CGetRawFileBytesU3Ec__AnonStorey1_t1499323914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3516[3] = 
{
	U3CGetRawFileBytesU3Ec__AnonStorey1_t1499323914::get_offset_of_dataUrl_0(),
	U3CGetRawFileBytesU3Ec__AnonStorey1_t1499323914::get_offset_of_accessToken_1(),
	U3CGetRawFileBytesU3Ec__AnonStorey1_t1499323914::get_offset_of_callback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3517 = { sizeof (AsyncImporter_t2267690090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3517[2] = 
{
	AsyncImporter_t2267690090::get_offset_of_finishedOperations_2(),
	AsyncImporter_t2267690090::get_offset_of_finishedOperationsLock_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3518 = { sizeof (AsyncImportCallback_t3623524474), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3519 = { sizeof (ImportOperation_t1829818323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3519[8] = 
{
	ImportOperation_t1829818323::get_offset_of_instance_0(),
	ImportOperation_t1829818323::get_offset_of_asset_1(),
	ImportOperation_t1829818323::get_offset_of_format_2(),
	ImportOperation_t1829818323::get_offset_of_options_3(),
	ImportOperation_t1829818323::get_offset_of_callback_4(),
	ImportOperation_t1829818323::get_offset_of_importState_5(),
	ImportOperation_t1829818323::get_offset_of_loader_6(),
	ImportOperation_t1829818323::get_offset_of_status_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3520 = { sizeof (AttributionGeneration_t2717336680), -1, sizeof(AttributionGeneration_t2717336680_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3520[3] = 
{
	AttributionGeneration_t2717336680_StaticFields::get_offset_of_ATTRIB_FILE_NAME_0(),
	AttributionGeneration_t2717336680_StaticFields::get_offset_of_FILE_HEADER_1(),
	AttributionGeneration_t2717336680_StaticFields::get_offset_of_CC_BY_LICENSE_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3521 = { sizeof (Authenticator_t1330886707), -1, sizeof(Authenticator_t1330886707_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3521[3] = 
{
	Authenticator_t1330886707_StaticFields::get_offset_of_instance_2(),
	Authenticator_t1330886707::get_offset_of_oauth2Identity_3(),
	Authenticator_t1330886707_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3522 = { sizeof (U3CAuthenticateU3Ec__AnonStorey0_t1316744360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3522[1] = 
{
	U3CAuthenticateU3Ec__AnonStorey0_t1316744360::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3523 = { sizeof (U3CAuthenticateU3Ec__AnonStorey1_t1316809896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3523[1] = 
{
	U3CAuthenticateU3Ec__AnonStorey1_t1316809896::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3524 = { sizeof (U3CReauthorizeU3Ec__AnonStorey2_t535307564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3524[1] = 
{
	U3CReauthorizeU3Ec__AnonStorey2_t535307564::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3525 = { sizeof (AutoStringify_t3514337370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3525[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3526 = { sizeof (AutoStringifiable_t1804051496), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3527 = { sizeof (AutoStringifyAbridged_t1725278763), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3528 = { sizeof (PersistentBlobCache_t1212092596), -1, sizeof(PersistentBlobCache_t1212092596_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3528[11] = 
{
	0,
	PersistentBlobCache_t1212092596::get_offset_of_setupDone_3(),
	PersistentBlobCache_t1212092596::get_offset_of_maxEntries_4(),
	PersistentBlobCache_t1212092596::get_offset_of_maxSizeBytes_5(),
	PersistentBlobCache_t1212092596::get_offset_of_rootPath_6(),
	PersistentBlobCache_t1212092596::get_offset_of_md5_7(),
	PersistentBlobCache_t1212092596::get_offset_of_cacheEntries_8(),
	PersistentBlobCache_t1212092596::get_offset_of_requestsPendingWork_9(),
	PersistentBlobCache_t1212092596::get_offset_of_requestsPendingDelivery_10(),
	PersistentBlobCache_t1212092596::get_offset_of_requestsRecyclePool_11(),
	PersistentBlobCache_t1212092596_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3529 = { sizeof (CacheReadCallback_t2055645553), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3530 = { sizeof (CacheEntry_t3962720293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3530[4] = 
{
	CacheEntry_t3962720293::get_offset_of_hash_0(),
	CacheEntry_t3962720293::get_offset_of_fileSize_1(),
	CacheEntry_t3962720293::get_offset_of_writeTimestampMillis_2(),
	CacheEntry_t3962720293::get_offset_of_readTimestampMillis_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3531 = { sizeof (RequestType_t2113262704)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3531[4] = 
{
	RequestType_t2113262704::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3532 = { sizeof (CacheRequest_t1922999082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3532[7] = 
{
	CacheRequest_t1922999082::get_offset_of_type_0(),
	CacheRequest_t1922999082::get_offset_of_key_1(),
	CacheRequest_t1922999082::get_offset_of_hash_2(),
	CacheRequest_t1922999082::get_offset_of_readCallback_3(),
	CacheRequest_t1922999082::get_offset_of_data_4(),
	CacheRequest_t1922999082::get_offset_of_maxAgeMillis_5(),
	CacheRequest_t1922999082::get_offset_of_success_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3533 = { sizeof (CoroutineRunner_t64893240), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3534 = { sizeof (DisabledPropertyAttribute_t3486517706), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3535 = { sizeof (EditTimeImportOptions_t4030780733)+ sizeof (RuntimeObject), sizeof(EditTimeImportOptions_t4030780733_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3535[2] = 
{
	EditTimeImportOptions_t4030780733::get_offset_of_baseOptions_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EditTimeImportOptions_t4030780733::get_offset_of_alsoInstantiate_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3536 = { sizeof (OAuth2Identity_t1039733419), -1, sizeof(OAuth2Identity_t1039733419_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3536[26] = 
{
	0,
	OAuth2Identity_t1039733419::get_offset_of_m_ServiceName_3(),
	OAuth2Identity_t1039733419::get_offset_of_m_ClientId_4(),
	OAuth2Identity_t1039733419::get_offset_of_m_ClientSecret_5(),
	0,
	0,
	0,
	OAuth2Identity_t1039733419::get_offset_of_m_OAuthScope_9(),
	0,
	0,
	OAuth2Identity_t1039733419::get_offset_of_m_CallbackFailedMessage_12(),
	0,
	OAuth2Identity_t1039733419_StaticFields::get_offset_of_UI_BACKGROUND_COLOR_14(),
	OAuth2Identity_t1039733419_StaticFields::get_offset_of_Instance_15(),
	OAuth2Identity_t1039733419_StaticFields::get_offset_of_m_OnProfileUpdated_16(),
	OAuth2Identity_t1039733419_StaticFields::get_offset_of_PLAYER_PREF_REFRESH_KEY_SUFFIX_17(),
	OAuth2Identity_t1039733419::get_offset_of_m_PlayerPrefRefreshKey_18(),
	0,
	OAuth2Identity_t1039733419::get_offset_of_m_AccessToken_20(),
	OAuth2Identity_t1039733419::get_offset_of_m_RefreshToken_21(),
	OAuth2Identity_t1039733419::get_offset_of_m_User_22(),
	OAuth2Identity_t1039733419::get_offset_of_m_HttpListener_23(),
	OAuth2Identity_t1039733419::get_offset_of_m_HttpPort_24(),
	OAuth2Identity_t1039733419::get_offset_of_m_WaitingOnAuthorization_25(),
	OAuth2Identity_t1039733419::get_offset_of_m_VerificationCode_26(),
	OAuth2Identity_t1039733419::get_offset_of_m_VerificationError_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3537 = { sizeof (UserInfo_t3564753064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3537[5] = 
{
	UserInfo_t3564753064::get_offset_of_id_0(),
	UserInfo_t3564753064::get_offset_of_name_1(),
	UserInfo_t3564753064::get_offset_of_email_2(),
	UserInfo_t3564753064::get_offset_of_location_3(),
	UserInfo_t3564753064::get_offset_of_icon_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3538 = { sizeof (U3CGetUserInfoU3Ec__Iterator0_t2243535225), -1, sizeof(U3CGetUserInfoU3Ec__Iterator0_t2243535225_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3538[11] = 
{
	U3CGetUserInfoU3Ec__Iterator0_t2243535225::get_offset_of_U3CuserU3E__0_0(),
	U3CGetUserInfoU3Ec__Iterator0_t2243535225::get_offset_of_U3CiU3E__1_1(),
	U3CGetUserInfoU3Ec__Iterator0_t2243535225::get_offset_of_U3CwwwU3E__2_2(),
	U3CGetUserInfoU3Ec__Iterator0_t2243535225::get_offset_of_U3CjsonU3E__3_3(),
	U3CGetUserInfoU3Ec__Iterator0_t2243535225::get_offset_of_U3CiconUriU3E__3_4(),
	U3CGetUserInfoU3Ec__Iterator0_t2243535225::get_offset_of_U24this_5(),
	U3CGetUserInfoU3Ec__Iterator0_t2243535225::get_offset_of_U24current_6(),
	U3CGetUserInfoU3Ec__Iterator0_t2243535225::get_offset_of_U24disposing_7(),
	U3CGetUserInfoU3Ec__Iterator0_t2243535225::get_offset_of_U24PC_8(),
	U3CGetUserInfoU3Ec__Iterator0_t2243535225_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
	U3CGetUserInfoU3Ec__Iterator0_t2243535225_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3539 = { sizeof (U3CReauthorizeU3Ec__Iterator1_t739728412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3539[8] = 
{
	U3CReauthorizeU3Ec__Iterator1_t739728412::get_offset_of_U3CparametersU3E__1_0(),
	U3CReauthorizeU3Ec__Iterator1_t739728412::get_offset_of_U3CwwwU3E__2_1(),
	U3CReauthorizeU3Ec__Iterator1_t739728412::get_offset_of_failureCallback_2(),
	U3CReauthorizeU3Ec__Iterator1_t739728412::get_offset_of_successCallback_3(),
	U3CReauthorizeU3Ec__Iterator1_t739728412::get_offset_of_U24this_4(),
	U3CReauthorizeU3Ec__Iterator1_t739728412::get_offset_of_U24current_5(),
	U3CReauthorizeU3Ec__Iterator1_t739728412::get_offset_of_U24disposing_6(),
	U3CReauthorizeU3Ec__Iterator1_t739728412::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3540 = { sizeof (U3CAuthorizeU3Ec__Iterator2_t3062002209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3540[11] = 
{
	U3CAuthorizeU3Ec__Iterator2_t3062002209::get_offset_of_launchSignInFlowIfNeeded_0(),
	U3CAuthorizeU3Ec__Iterator2_t3062002209::get_offset_of_onFailure_1(),
	U3CAuthorizeU3Ec__Iterator2_t3062002209::get_offset_of_U3CsbU3E__1_2(),
	U3CAuthorizeU3Ec__Iterator2_t3062002209::get_offset_of_U3CparametersU3E__1_3(),
	U3CAuthorizeU3Ec__Iterator2_t3062002209::get_offset_of_U3CwwwU3E__1_4(),
	U3CAuthorizeU3Ec__Iterator2_t3062002209::get_offset_of_U3CjsonU3E__1_5(),
	U3CAuthorizeU3Ec__Iterator2_t3062002209::get_offset_of_onSuccess_6(),
	U3CAuthorizeU3Ec__Iterator2_t3062002209::get_offset_of_U24this_7(),
	U3CAuthorizeU3Ec__Iterator2_t3062002209::get_offset_of_U24current_8(),
	U3CAuthorizeU3Ec__Iterator2_t3062002209::get_offset_of_U24disposing_9(),
	U3CAuthorizeU3Ec__Iterator2_t3062002209::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3541 = { sizeof (U3CStartHttpListenerU3Ec__AnonStorey5_t808655612), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3541[2] = 
{
	U3CStartHttpListenerU3Ec__AnonStorey5_t808655612::get_offset_of_responseText_0(),
	U3CStartHttpListenerU3Ec__AnonStorey5_t808655612::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3542 = { sizeof (U3CStartHttpListenerU3Ec__AnonStorey4_t3147307772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3542[2] = 
{
	U3CStartHttpListenerU3Ec__AnonStorey4_t3147307772::get_offset_of_listener_0(),
	U3CStartHttpListenerU3Ec__AnonStorey4_t3147307772::get_offset_of_U3CU3Ef__refU245_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3543 = { sizeof (U3CLoadProfileIconU3Ec__Iterator3_t474336387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3543[6] = 
{
	U3CLoadProfileIconU3Ec__Iterator3_t474336387::get_offset_of_U3CwwwU3E__1_0(),
	U3CLoadProfileIconU3Ec__Iterator3_t474336387::get_offset_of_uri_1(),
	U3CLoadProfileIconU3Ec__Iterator3_t474336387::get_offset_of_U24this_2(),
	U3CLoadProfileIconU3Ec__Iterator3_t474336387::get_offset_of_U24current_3(),
	U3CLoadProfileIconU3Ec__Iterator3_t474336387::get_offset_of_U24disposing_4(),
	U3CLoadProfileIconU3Ec__Iterator3_t474336387::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3544 = { sizeof (Features_t1280010604), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3545 = { sizeof (Gltf1Root_t2278775795), -1, sizeof(Gltf1Root_t2278775795_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3545[16] = 
{
	Gltf1Root_t2278775795::get_offset_of_buffers_3(),
	Gltf1Root_t2278775795::get_offset_of_accessors_4(),
	Gltf1Root_t2278775795::get_offset_of_bufferViews_5(),
	Gltf1Root_t2278775795::get_offset_of_meshes_6(),
	Gltf1Root_t2278775795::get_offset_of_shaders_7(),
	Gltf1Root_t2278775795::get_offset_of_programs_8(),
	Gltf1Root_t2278775795::get_offset_of_techniques_9(),
	Gltf1Root_t2278775795::get_offset_of_images_10(),
	Gltf1Root_t2278775795::get_offset_of_textures_11(),
	Gltf1Root_t2278775795::get_offset_of_materials_12(),
	Gltf1Root_t2278775795::get_offset_of_nodes_13(),
	Gltf1Root_t2278775795::get_offset_of_scenes_14(),
	Gltf1Root_t2278775795::get_offset_of_scene_15(),
	Gltf1Root_t2278775795::get_offset_of_disposed_16(),
	Gltf1Root_t2278775795::get_offset_of_scenePtr_17(),
	Gltf1Root_t2278775795_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3546 = { sizeof (Gltf1Buffer_t1264135694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3546[2] = 
{
	Gltf1Buffer_t1264135694::get_offset_of_type_3(),
	Gltf1Buffer_t1264135694::get_offset_of_gltfId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3547 = { sizeof (Gltf1Accessor_t3110689366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3547[3] = 
{
	Gltf1Accessor_t3110689366::get_offset_of_bufferView_7(),
	Gltf1Accessor_t3110689366::get_offset_of_gltfId_8(),
	Gltf1Accessor_t3110689366::get_offset_of_bufferViewPtr_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3548 = { sizeof (Gltf1BufferView_t1795531975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3548[3] = 
{
	Gltf1BufferView_t1795531975::get_offset_of_buffer_3(),
	Gltf1BufferView_t1795531975::get_offset_of_gltfId_4(),
	Gltf1BufferView_t1795531975::get_offset_of_bufferPtr_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3549 = { sizeof (Gltf1Primitive_t3883868606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3549[6] = 
{
	Gltf1Primitive_t3883868606::get_offset_of_attributes_3(),
	Gltf1Primitive_t3883868606::get_offset_of_indices_4(),
	Gltf1Primitive_t3883868606::get_offset_of_material_5(),
	Gltf1Primitive_t3883868606::get_offset_of_attributePtrs_6(),
	Gltf1Primitive_t3883868606::get_offset_of_indicesPtr_7(),
	Gltf1Primitive_t3883868606::get_offset_of_materialPtr_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3550 = { sizeof (Gltf1Shader_t1854243311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3550[5] = 
{
	0,
	0,
	Gltf1Shader_t1854243311::get_offset_of_gltfId_2(),
	Gltf1Shader_t1854243311::get_offset_of_uri_3(),
	Gltf1Shader_t1854243311::get_offset_of_type_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3551 = { sizeof (Gltf1Program_t3399901429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3551[5] = 
{
	Gltf1Program_t3399901429::get_offset_of_vertexShader_0(),
	Gltf1Program_t3399901429::get_offset_of_fragmentShader_1(),
	Gltf1Program_t3399901429::get_offset_of_gltfId_2(),
	Gltf1Program_t3399901429::get_offset_of_vertexShaderPtr_3(),
	Gltf1Program_t3399901429::get_offset_of_fragmentShaderPtr_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3552 = { sizeof (Gltf1Technique_t503673868), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3552[4] = 
{
	Gltf1Technique_t503673868::get_offset_of_program_0(),
	Gltf1Technique_t503673868::get_offset_of_extras_1(),
	Gltf1Technique_t503673868::get_offset_of_gltfId_2(),
	Gltf1Technique_t503673868::get_offset_of_programPtr_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3553 = { sizeof (Gltf1Image_t3036181168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3553[1] = 
{
	Gltf1Image_t3036181168::get_offset_of_gltfId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3554 = { sizeof (Gltf1Texture_t3945546188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3554[3] = 
{
	Gltf1Texture_t3945546188::get_offset_of_gltfId_1(),
	Gltf1Texture_t3945546188::get_offset_of_source_2(),
	Gltf1Texture_t3945546188::get_offset_of_sourcePtr_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3555 = { sizeof (TiltBrushGltf1PbrValues_t1054533882), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3555[5] = 
{
	TiltBrushGltf1PbrValues_t1054533882::get_offset_of_BaseColorFactor_0(),
	TiltBrushGltf1PbrValues_t1054533882::get_offset_of_MetallicFactor_1(),
	TiltBrushGltf1PbrValues_t1054533882::get_offset_of_RoughnessFactor_2(),
	TiltBrushGltf1PbrValues_t1054533882::get_offset_of_BaseColorTex_3(),
	TiltBrushGltf1PbrValues_t1054533882::get_offset_of_BaseColorTexPtr_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3556 = { sizeof (Gltf1Material_t2703072588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3556[4] = 
{
	Gltf1Material_t2703072588::get_offset_of_technique_1(),
	Gltf1Material_t2703072588::get_offset_of_values_2(),
	Gltf1Material_t2703072588::get_offset_of_gltfId_3(),
	Gltf1Material_t2703072588::get_offset_of_techniquePtr_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3557 = { sizeof (U3CU3Ec__Iterator0_t446849591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3557[4] = 
{
	U3CU3Ec__Iterator0_t446849591::get_offset_of_U24this_0(),
	U3CU3Ec__Iterator0_t446849591::get_offset_of_U24current_1(),
	U3CU3Ec__Iterator0_t446849591::get_offset_of_U24disposing_2(),
	U3CU3Ec__Iterator0_t446849591::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3558 = { sizeof (Gltf1Mesh_t1560025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3558[2] = 
{
	Gltf1Mesh_t1560025::get_offset_of_primitives_1(),
	Gltf1Mesh_t1560025::get_offset_of_gltfId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3559 = { sizeof (U3CU3Ec__Iterator0_t630382070), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3559[6] = 
{
	U3CU3Ec__Iterator0_t630382070::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator0_t630382070::get_offset_of_U3CprimU3E__1_1(),
	U3CU3Ec__Iterator0_t630382070::get_offset_of_U24this_2(),
	U3CU3Ec__Iterator0_t630382070::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator0_t630382070::get_offset_of_U24disposing_4(),
	U3CU3Ec__Iterator0_t630382070::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3560 = { sizeof (Gltf1Node_t1095785444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3560[5] = 
{
	Gltf1Node_t1095785444::get_offset_of_children_2(),
	Gltf1Node_t1095785444::get_offset_of_meshes_3(),
	Gltf1Node_t1095785444::get_offset_of_gltfId_4(),
	Gltf1Node_t1095785444::get_offset_of_meshPtrs_5(),
	Gltf1Node_t1095785444::get_offset_of_childPtrs_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3561 = { sizeof (U3CU3Ec__Iterator0_t251376496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3561[6] = 
{
	U3CU3Ec__Iterator0_t251376496::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator0_t251376496::get_offset_of_U3CnodeU3E__1_1(),
	U3CU3Ec__Iterator0_t251376496::get_offset_of_U24this_2(),
	U3CU3Ec__Iterator0_t251376496::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator0_t251376496::get_offset_of_U24disposing_4(),
	U3CU3Ec__Iterator0_t251376496::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3562 = { sizeof (Gltf1Scene_t747889833), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3562[3] = 
{
	Gltf1Scene_t747889833::get_offset_of_nodes_1(),
	Gltf1Scene_t747889833::get_offset_of_gltfId_2(),
	Gltf1Scene_t747889833::get_offset_of_nodePtrs_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3563 = { sizeof (U3CU3Ec__Iterator0_t693557650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3563[6] = 
{
	U3CU3Ec__Iterator0_t693557650::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator0_t693557650::get_offset_of_U3CnodeU3E__1_1(),
	U3CU3Ec__Iterator0_t693557650::get_offset_of_U24this_2(),
	U3CU3Ec__Iterator0_t693557650::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator0_t693557650::get_offset_of_U24disposing_4(),
	U3CU3Ec__Iterator0_t693557650::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3564 = { sizeof (Gltf2Root_t2278775764), -1, sizeof(Gltf2Root_t2278775764_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3564[13] = 
{
	Gltf2Root_t2278775764::get_offset_of_buffers_3(),
	Gltf2Root_t2278775764::get_offset_of_accessors_4(),
	Gltf2Root_t2278775764::get_offset_of_bufferViews_5(),
	Gltf2Root_t2278775764::get_offset_of_meshes_6(),
	Gltf2Root_t2278775764::get_offset_of_materials_7(),
	Gltf2Root_t2278775764::get_offset_of_nodes_8(),
	Gltf2Root_t2278775764::get_offset_of_scenes_9(),
	Gltf2Root_t2278775764::get_offset_of_textures_10(),
	Gltf2Root_t2278775764::get_offset_of_images_11(),
	Gltf2Root_t2278775764::get_offset_of_scene_12(),
	Gltf2Root_t2278775764::get_offset_of_disposed_13(),
	Gltf2Root_t2278775764::get_offset_of_scenePtr_14(),
	Gltf2Root_t2278775764_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3565 = { sizeof (Gltf2Buffer_t1264135663), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3565[1] = 
{
	Gltf2Buffer_t1264135663::get_offset_of_gltfIndex_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3566 = { sizeof (Gltf2Accessor_t3110688405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3566[3] = 
{
	Gltf2Accessor_t3110688405::get_offset_of_bufferView_7(),
	Gltf2Accessor_t3110688405::get_offset_of_gltfIndex_8(),
	Gltf2Accessor_t3110688405::get_offset_of_bufferViewPtr_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3567 = { sizeof (Gltf2BufferView_t1795530950), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3567[3] = 
{
	Gltf2BufferView_t1795530950::get_offset_of_buffer_3(),
	Gltf2BufferView_t1795530950::get_offset_of_gltfIndex_4(),
	Gltf2BufferView_t1795530950::get_offset_of_bufferPtr_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3568 = { sizeof (Gltf2Primitive_t3883867645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3568[6] = 
{
	Gltf2Primitive_t3883867645::get_offset_of_attributes_3(),
	Gltf2Primitive_t3883867645::get_offset_of_indices_4(),
	Gltf2Primitive_t3883867645::get_offset_of_material_5(),
	Gltf2Primitive_t3883867645::get_offset_of_attributePtrs_6(),
	Gltf2Primitive_t3883867645::get_offset_of_indicesPtr_7(),
	Gltf2Primitive_t3883867645::get_offset_of_materialPtr_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3569 = { sizeof (Gltf2Material_t2703071627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3569[11] = 
{
	0,
	0,
	0,
	Gltf2Material_t2703071627::get_offset_of_extras_4(),
	Gltf2Material_t2703071627::get_offset_of_gltfIndex_5(),
	Gltf2Material_t2703071627::get_offset_of_pbrMetallicRoughness_6(),
	Gltf2Material_t2703071627::get_offset_of_normalTexture_7(),
	Gltf2Material_t2703071627::get_offset_of_emissiveTexture_8(),
	Gltf2Material_t2703071627::get_offset_of_emissiveFactor_9(),
	Gltf2Material_t2703071627::get_offset_of_alphaMode_10(),
	Gltf2Material_t2703071627::get_offset_of_doubleSided_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3570 = { sizeof (PbrMetallicRoughness_t2508879888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3570[5] = 
{
	PbrMetallicRoughness_t2508879888::get_offset_of_baseColorFactor_0(),
	PbrMetallicRoughness_t2508879888::get_offset_of_metallicFactor_1(),
	PbrMetallicRoughness_t2508879888::get_offset_of_roughnessFactor_2(),
	PbrMetallicRoughness_t2508879888::get_offset_of_baseColorTexture_3(),
	PbrMetallicRoughness_t2508879888::get_offset_of_metallicRoughnessTexture_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3571 = { sizeof (TextureInfo_t2535341730), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3571[3] = 
{
	TextureInfo_t2535341730::get_offset_of_index_0(),
	TextureInfo_t2535341730::get_offset_of_texCoord_1(),
	TextureInfo_t2535341730::get_offset_of_texture_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3572 = { sizeof (U3CU3Ec__Iterator0_t831553825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3572[4] = 
{
	U3CU3Ec__Iterator0_t831553825::get_offset_of_U24this_0(),
	U3CU3Ec__Iterator0_t831553825::get_offset_of_U24current_1(),
	U3CU3Ec__Iterator0_t831553825::get_offset_of_U24disposing_2(),
	U3CU3Ec__Iterator0_t831553825::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3573 = { sizeof (U3CU3Ec__Iterator1_t3170205985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3573[6] = 
{
	U3CU3Ec__Iterator1_t3170205985::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator1_t3170205985::get_offset_of_U3CtiU3E__1_1(),
	U3CU3Ec__Iterator1_t3170205985::get_offset_of_U24this_2(),
	U3CU3Ec__Iterator1_t3170205985::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator1_t3170205985::get_offset_of_U24disposing_4(),
	U3CU3Ec__Iterator1_t3170205985::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3574 = { sizeof (Gltf2Texture_t3945546157), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3574[3] = 
{
	Gltf2Texture_t3945546157::get_offset_of_gltfIndex_1(),
	Gltf2Texture_t3945546157::get_offset_of_source_2(),
	Gltf2Texture_t3945546157::get_offset_of_sourcePtr_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3575 = { sizeof (Gltf2Image_t3036181135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3575[3] = 
{
	Gltf2Image_t3036181135::get_offset_of_gltfIndex_2(),
	Gltf2Image_t3036181135::get_offset_of_name_3(),
	Gltf2Image_t3036181135::get_offset_of_mimeType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3576 = { sizeof (Gltf2Mesh_t1559994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3576[2] = 
{
	Gltf2Mesh_t1559994::get_offset_of_primitives_1(),
	Gltf2Mesh_t1559994::get_offset_of_gltfIndex_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3577 = { sizeof (U3CU3Ec__Iterator0_t654162957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3577[6] = 
{
	U3CU3Ec__Iterator0_t654162957::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator0_t654162957::get_offset_of_U3CprimU3E__1_1(),
	U3CU3Ec__Iterator0_t654162957::get_offset_of_U24this_2(),
	U3CU3Ec__Iterator0_t654162957::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator0_t654162957::get_offset_of_U24disposing_4(),
	U3CU3Ec__Iterator0_t654162957::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3578 = { sizeof (Gltf2Node_t1095785411), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3578[5] = 
{
	Gltf2Node_t1095785411::get_offset_of_children_2(),
	Gltf2Node_t1095785411::get_offset_of_mesh_3(),
	Gltf2Node_t1095785411::get_offset_of_gltfIndex_4(),
	Gltf2Node_t1095785411::get_offset_of_meshPtr_5(),
	Gltf2Node_t1095785411::get_offset_of_childPtrs_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3579 = { sizeof (U3CU3Ec__Iterator0_t266980529), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3579[6] = 
{
	U3CU3Ec__Iterator0_t266980529::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator0_t266980529::get_offset_of_U3CnodeU3E__1_1(),
	U3CU3Ec__Iterator0_t266980529::get_offset_of_U24this_2(),
	U3CU3Ec__Iterator0_t266980529::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator0_t266980529::get_offset_of_U24disposing_4(),
	U3CU3Ec__Iterator0_t266980529::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3580 = { sizeof (Gltf2Scene_t747889802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3580[3] = 
{
	Gltf2Scene_t747889802::get_offset_of_nodes_1(),
	Gltf2Scene_t747889802::get_offset_of_gltfIndex_2(),
	Gltf2Scene_t747889802::get_offset_of_nodePtrs_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3581 = { sizeof (U3CU3Ec__Iterator0_t1227606519), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3581[6] = 
{
	U3CU3Ec__Iterator0_t1227606519::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator0_t1227606519::get_offset_of_U3CnodeU3E__1_1(),
	U3CU3Ec__Iterator0_t1227606519::get_offset_of_U24this_2(),
	U3CU3Ec__Iterator0_t1227606519::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator0_t1227606519::get_offset_of_U24disposing_4(),
	U3CU3Ec__Iterator0_t1227606519::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3582 = { sizeof (GltfMaterialConverter_t1532388150), -1, sizeof(GltfMaterialConverter_t1532388150_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3582[4] = 
{
	GltfMaterialConverter_t1532388150_StaticFields::get_offset_of_kTiltBrushMaterialRegex_0(),
	GltfMaterialConverter_t1532388150_StaticFields::get_offset_of_kTiltBrushShaderRegex_1(),
	GltfMaterialConverter_t1532388150::get_offset_of_newMaterials_2(),
	GltfMaterialConverter_t1532388150::get_offset_of_materials_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3583 = { sizeof (UnityMaterial_t3949128076)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3583[2] = 
{
	UnityMaterial_t3949128076::get_offset_of_material_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityMaterial_t3949128076::get_offset_of_template_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3584 = { sizeof (U3CNecessaryTexturesU3Ec__Iterator0_t3036043387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3584[8] = 
{
	U3CNecessaryTexturesU3Ec__Iterator0_t3036043387::get_offset_of_root_0(),
	U3CNecessaryTexturesU3Ec__Iterator0_t3036043387::get_offset_of_U24locvar0_1(),
	U3CNecessaryTexturesU3Ec__Iterator0_t3036043387::get_offset_of_U3CmatU3E__1_2(),
	U3CNecessaryTexturesU3Ec__Iterator0_t3036043387::get_offset_of_U24locvar1_3(),
	U3CNecessaryTexturesU3Ec__Iterator0_t3036043387::get_offset_of_U3CtexU3E__2_4(),
	U3CNecessaryTexturesU3Ec__Iterator0_t3036043387::get_offset_of_U24current_5(),
	U3CNecessaryTexturesU3Ec__Iterator0_t3036043387::get_offset_of_U24disposing_6(),
	U3CNecessaryTexturesU3Ec__Iterator0_t3036043387::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3585 = { sizeof (U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3585[12] = 
{
	U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858::get_offset_of_root_0(),
	U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858::get_offset_of_U24locvar0_1(),
	U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858::get_offset_of_U3CgltfTextureU3E__1_2(),
	U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858::get_offset_of_loader_3(),
	U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858::get_offset_of_U24locvar1_4(),
	U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858::get_offset_of_U3CunusedU3E__2_5(),
	U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858::get_offset_of_U24locvar2_6(),
	U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858::get_offset_of_loaded_7(),
	U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858::get_offset_of_U24locvar3_8(),
	U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858::get_offset_of_U24current_9(),
	U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858::get_offset_of_U24disposing_10(),
	U3CLoadTexturesCoroutineU3Ec__Iterator1_t514846858::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3586 = { sizeof (U3CConvertTextureCoroutineU3Ec__Iterator2_t3482638318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3586[8] = 
{
	U3CConvertTextureCoroutineU3Ec__Iterator2_t3482638318::get_offset_of_gltfTexture_0(),
	U3CConvertTextureCoroutineU3Ec__Iterator2_t3482638318::get_offset_of_U3CdataU3E__1_1(),
	U3CConvertTextureCoroutineU3Ec__Iterator2_t3482638318::get_offset_of_U3CtexU3E__1_2(),
	U3CConvertTextureCoroutineU3Ec__Iterator2_t3482638318::get_offset_of_loader_3(),
	U3CConvertTextureCoroutineU3Ec__Iterator2_t3482638318::get_offset_of_U3CtextureBytesU3E__2_4(),
	U3CConvertTextureCoroutineU3Ec__Iterator2_t3482638318::get_offset_of_U24current_5(),
	U3CConvertTextureCoroutineU3Ec__Iterator2_t3482638318::get_offset_of_U24disposing_6(),
	U3CConvertTextureCoroutineU3Ec__Iterator2_t3482638318::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3587 = { sizeof (GltfSchemaVersion_t4047415476)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3587[3] = 
{
	GltfSchemaVersion_t4047415476::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3588 = { sizeof (BadJson_t2001807480), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3589 = { sizeof (JsonVectorConverter_t251778730), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3590 = { sizeof (GltfJsonContractResolver_t946412919), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3591 = { sizeof (GltfAsset_t1389371343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3591[6] = 
{
	GltfAsset_t1389371343::get_offset_of_extensions_0(),
	GltfAsset_t1389371343::get_offset_of_extras_1(),
	GltfAsset_t1389371343::get_offset_of_copyright_2(),
	GltfAsset_t1389371343::get_offset_of_generator_3(),
	GltfAsset_t1389371343::get_offset_of_premultipliedAlpha_4(),
	GltfAsset_t1389371343::get_offset_of_version_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3592 = { sizeof (MeshPrecursor_t423268032), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3592[6] = 
{
	MeshPrecursor_t423268032::get_offset_of_vertices_0(),
	MeshPrecursor_t423268032::get_offset_of_normals_1(),
	MeshPrecursor_t423268032::get_offset_of_colors_2(),
	MeshPrecursor_t423268032::get_offset_of_tangents_3(),
	MeshPrecursor_t423268032::get_offset_of_uvSets_4(),
	MeshPrecursor_t423268032::get_offset_of_triangles_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3593 = { sizeof (GltfRootBase_t3065879770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3593[3] = 
{
	GltfRootBase_t3065879770::get_offset_of_asset_0(),
	GltfRootBase_t3065879770::get_offset_of_tiltBrushVersion_1(),
	GltfRootBase_t3065879770::get_offset_of_blocksVersion_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3594 = { sizeof (GltfBufferBase_t2874699436), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3594[3] = 
{
	GltfBufferBase_t2874699436::get_offset_of_byteLength_0(),
	GltfBufferBase_t2874699436::get_offset_of_uri_1(),
	GltfBufferBase_t2874699436::get_offset_of_data_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3595 = { sizeof (GltfAccessorBase_t2108002982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3595[7] = 
{
	GltfAccessorBase_t2108002982::get_offset_of_byteOffset_0(),
	GltfAccessorBase_t2108002982::get_offset_of_byteStride_1(),
	GltfAccessorBase_t2108002982::get_offset_of_componentType_2(),
	GltfAccessorBase_t2108002982::get_offset_of_count_3(),
	GltfAccessorBase_t2108002982::get_offset_of_max_4(),
	GltfAccessorBase_t2108002982::get_offset_of_min_5(),
	GltfAccessorBase_t2108002982::get_offset_of_type_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3596 = { sizeof (ComponentType_t584972487)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3596[6] = 
{
	ComponentType_t584972487::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3597 = { sizeof (GltfBufferViewBase_t1504165784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3597[3] = 
{
	GltfBufferViewBase_t1504165784::get_offset_of_byteLength_0(),
	GltfBufferViewBase_t1504165784::get_offset_of_byteOffset_1(),
	GltfBufferViewBase_t1504165784::get_offset_of_target_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3598 = { sizeof (GltfPrimitiveBase_t1810376431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3598[3] = 
{
	GltfPrimitiveBase_t1810376431::get_offset_of_mode_0(),
	GltfPrimitiveBase_t1810376431::get_offset_of_precursorMeshes_1(),
	GltfPrimitiveBase_t1810376431::get_offset_of_unityMeshes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3599 = { sizeof (Mode_t1780623943)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3599[2] = 
{
	Mode_t1780623943::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
