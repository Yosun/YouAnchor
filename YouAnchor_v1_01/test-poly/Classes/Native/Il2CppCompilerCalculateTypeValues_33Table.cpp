﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget>
struct List_1_t235857739;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t3688466362;
// LoadLocation
struct LoadLocation_t1360453159;
// Newtonsoft.Json.Bson.BsonString
struct BsonString_t1135579766;
// Newtonsoft.Json.Bson.BsonToken
struct BsonToken_t3838771636;
// UnityEngine.Object
struct Object_t631007953;
// System.Collections.Generic.List`1<UnityEngine.Analytics.AnalyticsEventParam>
struct List_1_t3952196670;
// UnityEngine.Analytics.AnalyticsEventTracker
struct AnalyticsEventTracker_t2285229262;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// GoogleMaps_Location
struct GoogleMaps_Location_t573291704;
// System.Type
struct Type_t;
// UnityEngine.Analytics.AnalyticsEventParamListContainer
struct AnalyticsEventParamListContainer_t587083383;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule>
struct List_1_t3418373063;
// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode>
struct List_1_t2041514553;
// System.Xml.Linq.XObject
struct XObject_t1119084474;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// GoogleMaps_Result[]
struct GoogleMaps_ResultU5BU5D_t556632001;
// GetLocation
struct GetLocation_t3988867825;
// GoogleMaps_Geometry
struct GoogleMaps_Geometry_t2263647806;
// PolyController
struct PolyController_t1840278131;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.List`1<Newtonsoft.Json.Bson.BsonProperty>
struct List_1_t2602298693;
// System.Void
struct Void_t1185182177;
// System.Collections.Generic.List`1<Newtonsoft.Json.Bson.BsonToken>
struct List_1_t1015879082;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.Analytics.ValueProperty
struct ValueProperty_t1868393739;
// UnityEngine.Analytics.TrackableField
struct TrackableField_t1772682203;
// UnityEngine.Analytics.TriggerListContainer
struct TriggerListContainer_t2032715483;
// UnityEngine.Analytics.EventTrigger/OnTrigger
struct OnTrigger_t4184125570;
// UnityEngine.Analytics.TriggerMethod
struct TriggerMethod_t582536534;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.IO.BinaryWriter
struct BinaryWriter_t3992595042;
// System.Globalization.CultureInfo
struct CultureInfo_t4157843068;
// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>
struct List_1_t4000102456;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// Wrld.Resources.Buildings.Highlight
struct Highlight_t3200169708;
// Newtonsoft.Json.JsonWriter/State[][]
struct StateU5BU5DU5BU5D_t3190318925;
// Newtonsoft.Json.Bson.BsonBinaryWriter
struct BsonBinaryWriter_t671405346;
// System.IO.BinaryReader
struct BinaryReader_t2428077293;
// System.Collections.Generic.List`1<Newtonsoft.Json.Bson.BsonReader/ContainerContext>
struct List_1_t2778069281;
// Newtonsoft.Json.Bson.BsonReader/ContainerContext
struct ContainerContext_t1305994539;
// UnityEngine.Analytics.TrackableProperty
struct TrackableProperty_t3943537984;
// UnityEngine.Analytics.EventTrigger
struct EventTrigger_t2527451695;
// UnityEngine.Analytics.StandardEventPayload
struct StandardEventPayload_t1629891255;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// RT
struct RT_t167452472;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Material
struct Material_t340375123;
// WrldHelper/Highlighter
struct Highlighter_t666689529;
// System.Collections.Generic.List`1<Wrld.Resources.Buildings.Highlight>
struct List_1_t377277154;
// Wrld.Resources.Buildings.BuildingsApi/HighlightReceivedCallback
struct HighlightReceivedCallback_t2001851641;
// UnityEngine.UI.Button
struct Button_t4055032469;
// Colorful.BleachBypass
struct BleachBypass_t2568391998;
// Colorful.Kuwahara
struct Kuwahara_t240745109;
// Colorful.ComicBook
struct ComicBook_t2243422766;
// Colorful.Led
struct Led_t3330730803;
// Colorful.VintageFast
struct VintageFast_t3040349303;
// Colorful.ShadowsMidtonesHighlights
struct ShadowsMidtonesHighlights_t2681697010;
// Colorful.Glitch
struct Glitch_t3656535212;
// UIToggle
struct UIToggle_t4192126258;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t777473367;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.Texture
struct Texture_t3661962703;




#ifndef U3CMODULEU3E_T692745558_H
#define U3CMODULEU3E_T692745558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745558 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745558_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745560_H
#define U3CMODULEU3E_T692745560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745560 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745560_H
#ifndef U3CMODULEU3E_T692745559_H
#define U3CMODULEU3E_T692745559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745559 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745559_H
#ifndef BSONOBJECTID_T3046626253_H
#define BSONOBJECTID_T3046626253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonObjectId
struct  BsonObjectId_t3046626253  : public RuntimeObject
{
public:
	// System.Byte[] Newtonsoft.Json.Bson.BsonObjectId::<Value>k__BackingField
	ByteU5BU5D_t4116647657* ___U3CValueU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BsonObjectId_t3046626253, ___U3CValueU3Ek__BackingField_0)); }
	inline ByteU5BU5D_t4116647657* get_U3CValueU3Ek__BackingField_0() const { return ___U3CValueU3Ek__BackingField_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CValueU3Ek__BackingField_0() { return &___U3CValueU3Ek__BackingField_0; }
	inline void set_U3CValueU3Ek__BackingField_0(ByteU5BU5D_t4116647657* value)
	{
		___U3CValueU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONOBJECTID_T3046626253_H
#ifndef TRACKABLEPROPERTY_T3943537984_H
#define TRACKABLEPROPERTY_T3943537984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty
struct  TrackableProperty_t3943537984  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TrackableProperty/FieldWithTarget> UnityEngine.Analytics.TrackableProperty::m_Fields
	List_1_t235857739 * ___m_Fields_1;

public:
	inline static int32_t get_offset_of_m_Fields_1() { return static_cast<int32_t>(offsetof(TrackableProperty_t3943537984, ___m_Fields_1)); }
	inline List_1_t235857739 * get_m_Fields_1() const { return ___m_Fields_1; }
	inline List_1_t235857739 ** get_address_of_m_Fields_1() { return &___m_Fields_1; }
	inline void set_m_Fields_1(List_1_t235857739 * value)
	{
		___m_Fields_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Fields_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTY_T3943537984_H
#ifndef U3CCALLGOOGLEAPIU3EC__ITERATOR0_T221915320_H
#define U3CCALLGOOGLEAPIU3EC__ITERATOR0_T221915320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadLocation/<CallGoogleAPI>c__Iterator0
struct  U3CCallGoogleAPIU3Ec__Iterator0_t221915320  : public RuntimeObject
{
public:
	// System.String LoadLocation/<CallGoogleAPI>c__Iterator0::address
	String_t* ___address_0;
	// UnityEngine.WWW LoadLocation/<CallGoogleAPI>c__Iterator0::<w>__0
	WWW_t3688466362 * ___U3CwU3E__0_1;
	// LoadLocation LoadLocation/<CallGoogleAPI>c__Iterator0::$this
	LoadLocation_t1360453159 * ___U24this_2;
	// System.Object LoadLocation/<CallGoogleAPI>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean LoadLocation/<CallGoogleAPI>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 LoadLocation/<CallGoogleAPI>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_address_0() { return static_cast<int32_t>(offsetof(U3CCallGoogleAPIU3Ec__Iterator0_t221915320, ___address_0)); }
	inline String_t* get_address_0() const { return ___address_0; }
	inline String_t** get_address_of_address_0() { return &___address_0; }
	inline void set_address_0(String_t* value)
	{
		___address_0 = value;
		Il2CppCodeGenWriteBarrier((&___address_0), value);
	}

	inline static int32_t get_offset_of_U3CwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CCallGoogleAPIU3Ec__Iterator0_t221915320, ___U3CwU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CwU3E__0_1() const { return ___U3CwU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CwU3E__0_1() { return &___U3CwU3E__0_1; }
	inline void set_U3CwU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CCallGoogleAPIU3Ec__Iterator0_t221915320, ___U24this_2)); }
	inline LoadLocation_t1360453159 * get_U24this_2() const { return ___U24this_2; }
	inline LoadLocation_t1360453159 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(LoadLocation_t1360453159 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CCallGoogleAPIU3Ec__Iterator0_t221915320, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CCallGoogleAPIU3Ec__Iterator0_t221915320, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCallGoogleAPIU3Ec__Iterator0_t221915320, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCALLGOOGLEAPIU3EC__ITERATOR0_T221915320_H
#ifndef BSONPROPERTY_T1130223951_H
#define BSONPROPERTY_T1130223951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonProperty
struct  BsonProperty_t1130223951  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Bson.BsonString Newtonsoft.Json.Bson.BsonProperty::<Name>k__BackingField
	BsonString_t1135579766 * ___U3CNameU3Ek__BackingField_0;
	// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonProperty::<Value>k__BackingField
	BsonToken_t3838771636 * ___U3CValueU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BsonProperty_t1130223951, ___U3CNameU3Ek__BackingField_0)); }
	inline BsonString_t1135579766 * get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline BsonString_t1135579766 ** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(BsonString_t1135579766 * value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BsonProperty_t1130223951, ___U3CValueU3Ek__BackingField_1)); }
	inline BsonToken_t3838771636 * get_U3CValueU3Ek__BackingField_1() const { return ___U3CValueU3Ek__BackingField_1; }
	inline BsonToken_t3838771636 ** get_address_of_U3CValueU3Ek__BackingField_1() { return &___U3CValueU3Ek__BackingField_1; }
	inline void set_U3CValueU3Ek__BackingField_1(BsonToken_t3838771636 * value)
	{
		___U3CValueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONPROPERTY_T1130223951_H
#ifndef FIELDWITHTARGET_T3058750293_H
#define FIELDWITHTARGET_T3058750293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableProperty/FieldWithTarget
struct  FieldWithTarget_t3058750293  : public RuntimeObject
{
public:
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_ParamName
	String_t* ___m_ParamName_0;
	// UnityEngine.Object UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_Target
	Object_t631007953 * ___m_Target_1;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_FieldPath
	String_t* ___m_FieldPath_2;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_TypeString
	String_t* ___m_TypeString_3;
	// System.Boolean UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_DoStatic
	bool ___m_DoStatic_4;
	// System.String UnityEngine.Analytics.TrackableProperty/FieldWithTarget::m_StaticString
	String_t* ___m_StaticString_5;

public:
	inline static int32_t get_offset_of_m_ParamName_0() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_ParamName_0)); }
	inline String_t* get_m_ParamName_0() const { return ___m_ParamName_0; }
	inline String_t** get_address_of_m_ParamName_0() { return &___m_ParamName_0; }
	inline void set_m_ParamName_0(String_t* value)
	{
		___m_ParamName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParamName_0), value);
	}

	inline static int32_t get_offset_of_m_Target_1() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_Target_1)); }
	inline Object_t631007953 * get_m_Target_1() const { return ___m_Target_1; }
	inline Object_t631007953 ** get_address_of_m_Target_1() { return &___m_Target_1; }
	inline void set_m_Target_1(Object_t631007953 * value)
	{
		___m_Target_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_1), value);
	}

	inline static int32_t get_offset_of_m_FieldPath_2() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_FieldPath_2)); }
	inline String_t* get_m_FieldPath_2() const { return ___m_FieldPath_2; }
	inline String_t** get_address_of_m_FieldPath_2() { return &___m_FieldPath_2; }
	inline void set_m_FieldPath_2(String_t* value)
	{
		___m_FieldPath_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_FieldPath_2), value);
	}

	inline static int32_t get_offset_of_m_TypeString_3() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_TypeString_3)); }
	inline String_t* get_m_TypeString_3() const { return ___m_TypeString_3; }
	inline String_t** get_address_of_m_TypeString_3() { return &___m_TypeString_3; }
	inline void set_m_TypeString_3(String_t* value)
	{
		___m_TypeString_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeString_3), value);
	}

	inline static int32_t get_offset_of_m_DoStatic_4() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_DoStatic_4)); }
	inline bool get_m_DoStatic_4() const { return ___m_DoStatic_4; }
	inline bool* get_address_of_m_DoStatic_4() { return &___m_DoStatic_4; }
	inline void set_m_DoStatic_4(bool value)
	{
		___m_DoStatic_4 = value;
	}

	inline static int32_t get_offset_of_m_StaticString_5() { return static_cast<int32_t>(offsetof(FieldWithTarget_t3058750293, ___m_StaticString_5)); }
	inline String_t* get_m_StaticString_5() const { return ___m_StaticString_5; }
	inline String_t** get_address_of_m_StaticString_5() { return &___m_StaticString_5; }
	inline void set_m_StaticString_5(String_t* value)
	{
		___m_StaticString_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_StaticString_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDWITHTARGET_T3058750293_H
#ifndef GOOGLEMAPS_LOCATION_T573291704_H
#define GOOGLEMAPS_LOCATION_T573291704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMaps_Location
struct  GoogleMaps_Location_t573291704  : public RuntimeObject
{
public:
	// System.Double GoogleMaps_Location::lat
	double ___lat_0;
	// System.Double GoogleMaps_Location::lng
	double ___lng_1;

public:
	inline static int32_t get_offset_of_lat_0() { return static_cast<int32_t>(offsetof(GoogleMaps_Location_t573291704, ___lat_0)); }
	inline double get_lat_0() const { return ___lat_0; }
	inline double* get_address_of_lat_0() { return &___lat_0; }
	inline void set_lat_0(double value)
	{
		___lat_0 = value;
	}

	inline static int32_t get_offset_of_lng_1() { return static_cast<int32_t>(offsetof(GoogleMaps_Location_t573291704, ___lng_1)); }
	inline double get_lng_1() const { return ___lng_1; }
	inline double* get_address_of_lng_1() { return &___lng_1; }
	inline void set_lng_1(double value)
	{
		___lng_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOOGLEMAPS_LOCATION_T573291704_H
#ifndef API_KEYS_T4177263602_H
#define API_KEYS_T4177263602_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// API_KEYS
struct  API_KEYS_t4177263602  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // API_KEYS_T4177263602_H
#ifndef ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#define ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParamListContainer
struct  AnalyticsEventParamListContainer_t587083383  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.AnalyticsEventParam> UnityEngine.Analytics.AnalyticsEventParamListContainer::m_Parameters
	List_1_t3952196670 * ___m_Parameters_0;

public:
	inline static int32_t get_offset_of_m_Parameters_0() { return static_cast<int32_t>(offsetof(AnalyticsEventParamListContainer_t587083383, ___m_Parameters_0)); }
	inline List_1_t3952196670 * get_m_Parameters_0() const { return ___m_Parameters_0; }
	inline List_1_t3952196670 ** get_address_of_m_Parameters_0() { return &___m_Parameters_0; }
	inline void set_m_Parameters_0(List_1_t3952196670 * value)
	{
		___m_Parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parameters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTPARAMLISTCONTAINER_T587083383_H
#ifndef ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#define ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTrackerSettings
struct  AnalyticsEventTrackerSettings_t480422680  : public RuntimeObject
{
public:

public:
};

struct AnalyticsEventTrackerSettings_t480422680_StaticFields
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTrackerSettings::paramCountMax
	int32_t ___paramCountMax_0;
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTrackerSettings::triggerRuleCountMax
	int32_t ___triggerRuleCountMax_1;

public:
	inline static int32_t get_offset_of_paramCountMax_0() { return static_cast<int32_t>(offsetof(AnalyticsEventTrackerSettings_t480422680_StaticFields, ___paramCountMax_0)); }
	inline int32_t get_paramCountMax_0() const { return ___paramCountMax_0; }
	inline int32_t* get_address_of_paramCountMax_0() { return &___paramCountMax_0; }
	inline void set_paramCountMax_0(int32_t value)
	{
		___paramCountMax_0 = value;
	}

	inline static int32_t get_offset_of_triggerRuleCountMax_1() { return static_cast<int32_t>(offsetof(AnalyticsEventTrackerSettings_t480422680_StaticFields, ___triggerRuleCountMax_1)); }
	inline int32_t get_triggerRuleCountMax_1() const { return ___triggerRuleCountMax_1; }
	inline int32_t* get_address_of_triggerRuleCountMax_1() { return &___triggerRuleCountMax_1; }
	inline void set_triggerRuleCountMax_1(int32_t value)
	{
		___triggerRuleCountMax_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTTRACKERSETTINGS_T480422680_H
#ifndef U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#define U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0
struct  U3CTimedTriggerU3Ec__Iterator0_t3813435494  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.AnalyticsEventTracker UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$this
	AnalyticsEventTracker_t2285229262 * ___U24this_0;
	// System.Object UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.Analytics.AnalyticsEventTracker/<TimedTrigger>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24this_0)); }
	inline AnalyticsEventTracker_t2285229262 * get_U24this_0() const { return ___U24this_0; }
	inline AnalyticsEventTracker_t2285229262 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AnalyticsEventTracker_t2285229262 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CTimedTriggerU3Ec__Iterator0_t3813435494, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMEDTRIGGERU3EC__ITERATOR0_T3813435494_H
#ifndef CLIB_T276046374_H
#define CLIB_T276046374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.CLib
struct  CLib_t276046374  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIB_T276046374_H
#ifndef ANALYTICSEVENT_T4058973021_H
#define ANALYTICSEVENT_T4058973021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEvent
struct  AnalyticsEvent_t4058973021  : public RuntimeObject
{
public:

public:
};

struct AnalyticsEvent_t4058973021_StaticFields
{
public:
	// System.String UnityEngine.Analytics.AnalyticsEvent::k_SdkVersion
	String_t* ___k_SdkVersion_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.AnalyticsEvent::m_EventData
	Dictionary_2_t2865362463 * ___m_EventData_1;
	// System.Boolean UnityEngine.Analytics.AnalyticsEvent::_debugMode
	bool ____debugMode_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.Analytics.AnalyticsEvent::enumRenameTable
	Dictionary_2_t1632706988 * ___enumRenameTable_3;

public:
	inline static int32_t get_offset_of_k_SdkVersion_0() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ___k_SdkVersion_0)); }
	inline String_t* get_k_SdkVersion_0() const { return ___k_SdkVersion_0; }
	inline String_t** get_address_of_k_SdkVersion_0() { return &___k_SdkVersion_0; }
	inline void set_k_SdkVersion_0(String_t* value)
	{
		___k_SdkVersion_0 = value;
		Il2CppCodeGenWriteBarrier((&___k_SdkVersion_0), value);
	}

	inline static int32_t get_offset_of_m_EventData_1() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ___m_EventData_1)); }
	inline Dictionary_2_t2865362463 * get_m_EventData_1() const { return ___m_EventData_1; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_EventData_1() { return &___m_EventData_1; }
	inline void set_m_EventData_1(Dictionary_2_t2865362463 * value)
	{
		___m_EventData_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventData_1), value);
	}

	inline static int32_t get_offset_of__debugMode_2() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ____debugMode_2)); }
	inline bool get__debugMode_2() const { return ____debugMode_2; }
	inline bool* get_address_of__debugMode_2() { return &____debugMode_2; }
	inline void set__debugMode_2(bool value)
	{
		____debugMode_2 = value;
	}

	inline static int32_t get_offset_of_enumRenameTable_3() { return static_cast<int32_t>(offsetof(AnalyticsEvent_t4058973021_StaticFields, ___enumRenameTable_3)); }
	inline Dictionary_2_t1632706988 * get_enumRenameTable_3() const { return ___enumRenameTable_3; }
	inline Dictionary_2_t1632706988 ** get_address_of_enumRenameTable_3() { return &___enumRenameTable_3; }
	inline void set_enumRenameTable_3(Dictionary_2_t1632706988 * value)
	{
		___enumRenameTable_3 = value;
		Il2CppCodeGenWriteBarrier((&___enumRenameTable_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENT_T4058973021_H
#ifndef JSONCONVERTER_T1047106545_H
#define JSONCONVERTER_T1047106545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonConverter
struct  JsonConverter_t1047106545  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONVERTER_T1047106545_H
#ifndef TRACKABLEPROPERTYBASE_T2121532948_H
#define TRACKABLEPROPERTYBASE_T2121532948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackablePropertyBase
struct  TrackablePropertyBase_t2121532948  : public RuntimeObject
{
public:
	// UnityEngine.Object UnityEngine.Analytics.TrackablePropertyBase::m_Target
	Object_t631007953 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackablePropertyBase::m_Path
	String_t* ___m_Path_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackablePropertyBase_t2121532948, ___m_Target_0)); }
	inline Object_t631007953 * get_m_Target_0() const { return ___m_Target_0; }
	inline Object_t631007953 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(Object_t631007953 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(TrackablePropertyBase_t2121532948, ___m_Path_1)); }
	inline String_t* get_m_Path_1() const { return ___m_Path_1; }
	inline String_t** get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(String_t* value)
	{
		___m_Path_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Path_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEPROPERTYBASE_T2121532948_H
#ifndef GOOGLEMAPS_GEOMETRY_T2263647806_H
#define GOOGLEMAPS_GEOMETRY_T2263647806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMaps_Geometry
struct  GoogleMaps_Geometry_t2263647806  : public RuntimeObject
{
public:
	// GoogleMaps_Location GoogleMaps_Geometry::location
	GoogleMaps_Location_t573291704 * ___location_0;
	// System.String GoogleMaps_Geometry::location_type
	String_t* ___location_type_1;

public:
	inline static int32_t get_offset_of_location_0() { return static_cast<int32_t>(offsetof(GoogleMaps_Geometry_t2263647806, ___location_0)); }
	inline GoogleMaps_Location_t573291704 * get_location_0() const { return ___location_0; }
	inline GoogleMaps_Location_t573291704 ** get_address_of_location_0() { return &___location_0; }
	inline void set_location_0(GoogleMaps_Location_t573291704 * value)
	{
		___location_0 = value;
		Il2CppCodeGenWriteBarrier((&___location_0), value);
	}

	inline static int32_t get_offset_of_location_type_1() { return static_cast<int32_t>(offsetof(GoogleMaps_Geometry_t2263647806, ___location_type_1)); }
	inline String_t* get_location_type_1() const { return ___location_type_1; }
	inline String_t** get_address_of_location_type_1() { return &___location_type_1; }
	inline void set_location_type_1(String_t* value)
	{
		___location_type_1 = value;
		Il2CppCodeGenWriteBarrier((&___location_type_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOOGLEMAPS_GEOMETRY_T2263647806_H
#ifndef STANDARDEVENTPAYLOAD_T1629891255_H
#define STANDARDEVENTPAYLOAD_T1629891255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.StandardEventPayload
struct  StandardEventPayload_t1629891255  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.StandardEventPayload::m_IsEventExpanded
	bool ___m_IsEventExpanded_0;
	// System.String UnityEngine.Analytics.StandardEventPayload::m_StandardEventType
	String_t* ___m_StandardEventType_1;
	// System.Type UnityEngine.Analytics.StandardEventPayload::standardEventType
	Type_t * ___standardEventType_2;
	// UnityEngine.Analytics.AnalyticsEventParamListContainer UnityEngine.Analytics.StandardEventPayload::m_Parameters
	AnalyticsEventParamListContainer_t587083383 * ___m_Parameters_3;
	// System.String UnityEngine.Analytics.StandardEventPayload::m_Name
	String_t* ___m_Name_5;

public:
	inline static int32_t get_offset_of_m_IsEventExpanded_0() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_IsEventExpanded_0)); }
	inline bool get_m_IsEventExpanded_0() const { return ___m_IsEventExpanded_0; }
	inline bool* get_address_of_m_IsEventExpanded_0() { return &___m_IsEventExpanded_0; }
	inline void set_m_IsEventExpanded_0(bool value)
	{
		___m_IsEventExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_StandardEventType_1() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_StandardEventType_1)); }
	inline String_t* get_m_StandardEventType_1() const { return ___m_StandardEventType_1; }
	inline String_t** get_address_of_m_StandardEventType_1() { return &___m_StandardEventType_1; }
	inline void set_m_StandardEventType_1(String_t* value)
	{
		___m_StandardEventType_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_StandardEventType_1), value);
	}

	inline static int32_t get_offset_of_standardEventType_2() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___standardEventType_2)); }
	inline Type_t * get_standardEventType_2() const { return ___standardEventType_2; }
	inline Type_t ** get_address_of_standardEventType_2() { return &___standardEventType_2; }
	inline void set_standardEventType_2(Type_t * value)
	{
		___standardEventType_2 = value;
		Il2CppCodeGenWriteBarrier((&___standardEventType_2), value);
	}

	inline static int32_t get_offset_of_m_Parameters_3() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_Parameters_3)); }
	inline AnalyticsEventParamListContainer_t587083383 * get_m_Parameters_3() const { return ___m_Parameters_3; }
	inline AnalyticsEventParamListContainer_t587083383 ** get_address_of_m_Parameters_3() { return &___m_Parameters_3; }
	inline void set_m_Parameters_3(AnalyticsEventParamListContainer_t587083383 * value)
	{
		___m_Parameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Parameters_3), value);
	}

	inline static int32_t get_offset_of_m_Name_5() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255, ___m_Name_5)); }
	inline String_t* get_m_Name_5() const { return ___m_Name_5; }
	inline String_t** get_address_of_m_Name_5() { return &___m_Name_5; }
	inline void set_m_Name_5(String_t* value)
	{
		___m_Name_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_5), value);
	}
};

struct StandardEventPayload_t1629891255_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.StandardEventPayload::m_EventData
	Dictionary_2_t2865362463 * ___m_EventData_4;

public:
	inline static int32_t get_offset_of_m_EventData_4() { return static_cast<int32_t>(offsetof(StandardEventPayload_t1629891255_StaticFields, ___m_EventData_4)); }
	inline Dictionary_2_t2865362463 * get_m_EventData_4() const { return ___m_EventData_4; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_EventData_4() { return &___m_EventData_4; }
	inline void set_m_EventData_4(Dictionary_2_t2865362463 * value)
	{
		___m_EventData_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventData_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDEVENTPAYLOAD_T1629891255_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef U3CWAITFORITU3EC__ITERATOR0_T684802227_H
#define U3CWAITFORITU3EC__ITERATOR0_T684802227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReplayTest/<WaitForIt>c__Iterator0
struct  U3CWaitForItU3Ec__Iterator0_t684802227  : public RuntimeObject
{
public:
	// System.Object ReplayTest/<WaitForIt>c__Iterator0::$current
	RuntimeObject * ___U24current_0;
	// System.Boolean ReplayTest/<WaitForIt>c__Iterator0::$disposing
	bool ___U24disposing_1;
	// System.Int32 ReplayTest/<WaitForIt>c__Iterator0::$PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CWaitForItU3Ec__Iterator0_t684802227, ___U24current_0)); }
	inline RuntimeObject * get_U24current_0() const { return ___U24current_0; }
	inline RuntimeObject ** get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(RuntimeObject * value)
	{
		___U24current_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_0), value);
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CWaitForItU3Ec__Iterator0_t684802227, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CWaitForItU3Ec__Iterator0_t684802227, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORITU3EC__ITERATOR0_T684802227_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef TRIGGERLISTCONTAINER_T2032715483_H
#define TRIGGERLISTCONTAINER_T2032715483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerListContainer
struct  TriggerListContainer_t2032715483  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule> UnityEngine.Analytics.TriggerListContainer::m_Rules
	List_1_t3418373063 * ___m_Rules_0;

public:
	inline static int32_t get_offset_of_m_Rules_0() { return static_cast<int32_t>(offsetof(TriggerListContainer_t2032715483, ___m_Rules_0)); }
	inline List_1_t3418373063 * get_m_Rules_0() const { return ___m_Rules_0; }
	inline List_1_t3418373063 ** get_address_of_m_Rules_0() { return &___m_Rules_0; }
	inline void set_m_Rules_0(List_1_t3418373063 * value)
	{
		___m_Rules_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLISTCONTAINER_T2032715483_H
#ifndef XOBJECTWRAPPER_T2266598274_H
#define XOBJECTWRAPPER_T2266598274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XObjectWrapper
struct  XObjectWrapper_t2266598274  : public RuntimeObject
{
public:
	// System.Xml.Linq.XObject Newtonsoft.Json.Converters.XObjectWrapper::_xmlObject
	XObject_t1119084474 * ____xmlObject_1;

public:
	inline static int32_t get_offset_of__xmlObject_1() { return static_cast<int32_t>(offsetof(XObjectWrapper_t2266598274, ____xmlObject_1)); }
	inline XObject_t1119084474 * get__xmlObject_1() const { return ____xmlObject_1; }
	inline XObject_t1119084474 ** get_address_of__xmlObject_1() { return &____xmlObject_1; }
	inline void set__xmlObject_1(XObject_t1119084474 * value)
	{
		____xmlObject_1 = value;
		Il2CppCodeGenWriteBarrier((&____xmlObject_1), value);
	}
};

struct XObjectWrapper_t2266598274_StaticFields
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XObjectWrapper::EmptyChildNodes
	List_1_t2041514553 * ___EmptyChildNodes_0;

public:
	inline static int32_t get_offset_of_EmptyChildNodes_0() { return static_cast<int32_t>(offsetof(XObjectWrapper_t2266598274_StaticFields, ___EmptyChildNodes_0)); }
	inline List_1_t2041514553 * get_EmptyChildNodes_0() const { return ___EmptyChildNodes_0; }
	inline List_1_t2041514553 ** get_address_of_EmptyChildNodes_0() { return &___EmptyChildNodes_0; }
	inline void set_EmptyChildNodes_0(List_1_t2041514553 * value)
	{
		___EmptyChildNodes_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyChildNodes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XOBJECTWRAPPER_T2266598274_H
#ifndef TRACKABLETRIGGER_T621205209_H
#define TRACKABLETRIGGER_T621205209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableTrigger
struct  TrackableTrigger_t621205209  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.Analytics.TrackableTrigger::m_Target
	GameObject_t1113636619 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackableTrigger::m_MethodPath
	String_t* ___m_MethodPath_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_Target_0)); }
	inline GameObject_t1113636619 * get_m_Target_0() const { return ___m_Target_0; }
	inline GameObject_t1113636619 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(GameObject_t1113636619 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_MethodPath_1() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_MethodPath_1)); }
	inline String_t* get_m_MethodPath_1() const { return ___m_MethodPath_1; }
	inline String_t** get_address_of_m_MethodPath_1() { return &___m_MethodPath_1; }
	inline void set_m_MethodPath_1(String_t* value)
	{
		___m_MethodPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_MethodPath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLETRIGGER_T621205209_H
#ifndef TRIGGERMETHOD_T582536534_H
#define TRIGGERMETHOD_T582536534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerMethod
struct  TriggerMethod_t582536534  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERMETHOD_T582536534_H
#ifndef BSONTOKEN_T3838771636_H
#define BSONTOKEN_T3838771636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonToken
struct  BsonToken_t3838771636  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonToken::<Parent>k__BackingField
	BsonToken_t3838771636 * ___U3CParentU3Ek__BackingField_0;
	// System.Int32 Newtonsoft.Json.Bson.BsonToken::<CalculatedSize>k__BackingField
	int32_t ___U3CCalculatedSizeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CParentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BsonToken_t3838771636, ___U3CParentU3Ek__BackingField_0)); }
	inline BsonToken_t3838771636 * get_U3CParentU3Ek__BackingField_0() const { return ___U3CParentU3Ek__BackingField_0; }
	inline BsonToken_t3838771636 ** get_address_of_U3CParentU3Ek__BackingField_0() { return &___U3CParentU3Ek__BackingField_0; }
	inline void set_U3CParentU3Ek__BackingField_0(BsonToken_t3838771636 * value)
	{
		___U3CParentU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParentU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCalculatedSizeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BsonToken_t3838771636, ___U3CCalculatedSizeU3Ek__BackingField_1)); }
	inline int32_t get_U3CCalculatedSizeU3Ek__BackingField_1() const { return ___U3CCalculatedSizeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CCalculatedSizeU3Ek__BackingField_1() { return &___U3CCalculatedSizeU3Ek__BackingField_1; }
	inline void set_U3CCalculatedSizeU3Ek__BackingField_1(int32_t value)
	{
		___U3CCalculatedSizeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONTOKEN_T3838771636_H
#ifndef GOOGLEMAPS_RESPONSE_T2609494023_H
#define GOOGLEMAPS_RESPONSE_T2609494023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMaps_Response
struct  GoogleMaps_Response_t2609494023  : public RuntimeObject
{
public:
	// GoogleMaps_Result[] GoogleMaps_Response::results
	GoogleMaps_ResultU5BU5D_t556632001* ___results_0;
	// System.String GoogleMaps_Response::status
	String_t* ___status_1;

public:
	inline static int32_t get_offset_of_results_0() { return static_cast<int32_t>(offsetof(GoogleMaps_Response_t2609494023, ___results_0)); }
	inline GoogleMaps_ResultU5BU5D_t556632001* get_results_0() const { return ___results_0; }
	inline GoogleMaps_ResultU5BU5D_t556632001** get_address_of_results_0() { return &___results_0; }
	inline void set_results_0(GoogleMaps_ResultU5BU5D_t556632001* value)
	{
		___results_0 = value;
		Il2CppCodeGenWriteBarrier((&___results_0), value);
	}

	inline static int32_t get_offset_of_status_1() { return static_cast<int32_t>(offsetof(GoogleMaps_Response_t2609494023, ___status_1)); }
	inline String_t* get_status_1() const { return ___status_1; }
	inline String_t** get_address_of_status_1() { return &___status_1; }
	inline void set_status_1(String_t* value)
	{
		___status_1 = value;
		Il2CppCodeGenWriteBarrier((&___status_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOOGLEMAPS_RESPONSE_T2609494023_H
#ifndef U3CLOADITU3EC__ITERATOR0_T1028753935_H
#define U3CLOADITU3EC__ITERATOR0_T1028753935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetLocation/<LoadIt>c__Iterator0
struct  U3CLoadItU3Ec__Iterator0_t1028753935  : public RuntimeObject
{
public:
	// System.Int32 GetLocation/<LoadIt>c__Iterator0::<maxWait>__0
	int32_t ___U3CmaxWaitU3E__0_0;
	// GetLocation GetLocation/<LoadIt>c__Iterator0::$this
	GetLocation_t3988867825 * ___U24this_1;
	// System.Object GetLocation/<LoadIt>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean GetLocation/<LoadIt>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 GetLocation/<LoadIt>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CmaxWaitU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadItU3Ec__Iterator0_t1028753935, ___U3CmaxWaitU3E__0_0)); }
	inline int32_t get_U3CmaxWaitU3E__0_0() const { return ___U3CmaxWaitU3E__0_0; }
	inline int32_t* get_address_of_U3CmaxWaitU3E__0_0() { return &___U3CmaxWaitU3E__0_0; }
	inline void set_U3CmaxWaitU3E__0_0(int32_t value)
	{
		___U3CmaxWaitU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CLoadItU3Ec__Iterator0_t1028753935, ___U24this_1)); }
	inline GetLocation_t3988867825 * get_U24this_1() const { return ___U24this_1; }
	inline GetLocation_t3988867825 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(GetLocation_t3988867825 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CLoadItU3Ec__Iterator0_t1028753935, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CLoadItU3Ec__Iterator0_t1028753935, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CLoadItU3Ec__Iterator0_t1028753935, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADITU3EC__ITERATOR0_T1028753935_H
#ifndef GOOGLEMAPS_RESULT_T3337143104_H
#define GOOGLEMAPS_RESULT_T3337143104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleMaps_Result
struct  GoogleMaps_Result_t3337143104  : public RuntimeObject
{
public:
	// System.String GoogleMaps_Result::formatted_address
	String_t* ___formatted_address_0;
	// GoogleMaps_Geometry GoogleMaps_Result::geometry
	GoogleMaps_Geometry_t2263647806 * ___geometry_1;
	// System.String GoogleMaps_Result::place_id
	String_t* ___place_id_2;

public:
	inline static int32_t get_offset_of_formatted_address_0() { return static_cast<int32_t>(offsetof(GoogleMaps_Result_t3337143104, ___formatted_address_0)); }
	inline String_t* get_formatted_address_0() const { return ___formatted_address_0; }
	inline String_t** get_address_of_formatted_address_0() { return &___formatted_address_0; }
	inline void set_formatted_address_0(String_t* value)
	{
		___formatted_address_0 = value;
		Il2CppCodeGenWriteBarrier((&___formatted_address_0), value);
	}

	inline static int32_t get_offset_of_geometry_1() { return static_cast<int32_t>(offsetof(GoogleMaps_Result_t3337143104, ___geometry_1)); }
	inline GoogleMaps_Geometry_t2263647806 * get_geometry_1() const { return ___geometry_1; }
	inline GoogleMaps_Geometry_t2263647806 ** get_address_of_geometry_1() { return &___geometry_1; }
	inline void set_geometry_1(GoogleMaps_Geometry_t2263647806 * value)
	{
		___geometry_1 = value;
		Il2CppCodeGenWriteBarrier((&___geometry_1), value);
	}

	inline static int32_t get_offset_of_place_id_2() { return static_cast<int32_t>(offsetof(GoogleMaps_Result_t3337143104, ___place_id_2)); }
	inline String_t* get_place_id_2() const { return ___place_id_2; }
	inline String_t** get_address_of_place_id_2() { return &___place_id_2; }
	inline void set_place_id_2(String_t* value)
	{
		___place_id_2 = value;
		Il2CppCodeGenWriteBarrier((&___place_id_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOOGLEMAPS_RESULT_T3337143104_H
#ifndef U3CPULLTEXTUREU3EC__ITERATOR0_T3483955249_H
#define U3CPULLTEXTUREU3EC__ITERATOR0_T3483955249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyController/<PullTexture>c__Iterator0
struct  U3CPullTextureU3Ec__Iterator0_t3483955249  : public RuntimeObject
{
public:
	// System.String PolyController/<PullTexture>c__Iterator0::url
	String_t* ___url_0;
	// UnityEngine.WWW PolyController/<PullTexture>c__Iterator0::<w>__0
	WWW_t3688466362 * ___U3CwU3E__0_1;
	// System.String PolyController/<PullTexture>c__Iterator0::id
	String_t* ___id_2;
	// PolyController PolyController/<PullTexture>c__Iterator0::$this
	PolyController_t1840278131 * ___U24this_3;
	// System.Object PolyController/<PullTexture>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean PolyController/<PullTexture>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 PolyController/<PullTexture>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CPullTextureU3Ec__Iterator0_t3483955249, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_U3CwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CPullTextureU3Ec__Iterator0_t3483955249, ___U3CwU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CwU3E__0_1() const { return ___U3CwU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CwU3E__0_1() { return &___U3CwU3E__0_1; }
	inline void set_U3CwU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(U3CPullTextureU3Ec__Iterator0_t3483955249, ___id_2)); }
	inline String_t* get_id_2() const { return ___id_2; }
	inline String_t** get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(String_t* value)
	{
		___id_2 = value;
		Il2CppCodeGenWriteBarrier((&___id_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CPullTextureU3Ec__Iterator0_t3483955249, ___U24this_3)); }
	inline PolyController_t1840278131 * get_U24this_3() const { return ___U24this_3; }
	inline PolyController_t1840278131 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(PolyController_t1840278131 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CPullTextureU3Ec__Iterator0_t3483955249, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CPullTextureU3Ec__Iterator0_t3483955249, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CPullTextureU3Ec__Iterator0_t3483955249, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPULLTEXTUREU3EC__ITERATOR0_T3483955249_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef TRACKABLEFIELD_T1772682203_H
#define TRACKABLEFIELD_T1772682203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableField
struct  TrackableField_t1772682203  : public TrackablePropertyBase_t2121532948
{
public:
	// System.String[] UnityEngine.Analytics.TrackableField::m_ValidTypeNames
	StringU5BU5D_t1281789340* ___m_ValidTypeNames_2;
	// System.String UnityEngine.Analytics.TrackableField::m_Type
	String_t* ___m_Type_3;
	// System.String UnityEngine.Analytics.TrackableField::m_EnumType
	String_t* ___m_EnumType_4;

public:
	inline static int32_t get_offset_of_m_ValidTypeNames_2() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_ValidTypeNames_2)); }
	inline StringU5BU5D_t1281789340* get_m_ValidTypeNames_2() const { return ___m_ValidTypeNames_2; }
	inline StringU5BU5D_t1281789340** get_address_of_m_ValidTypeNames_2() { return &___m_ValidTypeNames_2; }
	inline void set_m_ValidTypeNames_2(StringU5BU5D_t1281789340* value)
	{
		___m_ValidTypeNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValidTypeNames_2), value);
	}

	inline static int32_t get_offset_of_m_Type_3() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_Type_3)); }
	inline String_t* get_m_Type_3() const { return ___m_Type_3; }
	inline String_t** get_address_of_m_Type_3() { return &___m_Type_3; }
	inline void set_m_Type_3(String_t* value)
	{
		___m_Type_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type_3), value);
	}

	inline static int32_t get_offset_of_m_EnumType_4() { return static_cast<int32_t>(offsetof(TrackableField_t1772682203, ___m_EnumType_4)); }
	inline String_t* get_m_EnumType_4() const { return ___m_EnumType_4; }
	inline String_t** get_address_of_m_EnumType_4() { return &___m_EnumType_4; }
	inline void set_m_EnumType_4(String_t* value)
	{
		___m_EnumType_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EnumType_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEFIELD_T1772682203_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef LATLONG_T2936018554_H
#define LATLONG_T2936018554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Space.LatLong
struct  LatLong_t2936018554 
{
public:
	// System.Double Wrld.Space.LatLong::m_latitudeInDegrees
	double ___m_latitudeInDegrees_0;
	// System.Double Wrld.Space.LatLong::m_longitudeInDegrees
	double ___m_longitudeInDegrees_1;

public:
	inline static int32_t get_offset_of_m_latitudeInDegrees_0() { return static_cast<int32_t>(offsetof(LatLong_t2936018554, ___m_latitudeInDegrees_0)); }
	inline double get_m_latitudeInDegrees_0() const { return ___m_latitudeInDegrees_0; }
	inline double* get_address_of_m_latitudeInDegrees_0() { return &___m_latitudeInDegrees_0; }
	inline void set_m_latitudeInDegrees_0(double value)
	{
		___m_latitudeInDegrees_0 = value;
	}

	inline static int32_t get_offset_of_m_longitudeInDegrees_1() { return static_cast<int32_t>(offsetof(LatLong_t2936018554, ___m_longitudeInDegrees_1)); }
	inline double get_m_longitudeInDegrees_1() const { return ___m_longitudeInDegrees_1; }
	inline double* get_address_of_m_longitudeInDegrees_1() { return &___m_longitudeInDegrees_1; }
	inline void set_m_longitudeInDegrees_1(double value)
	{
		___m_longitudeInDegrees_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LATLONG_T2936018554_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef BSONOBJECT_T3552490343_H
#define BSONOBJECT_T3552490343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonObject
struct  BsonObject_t3552490343  : public BsonToken_t3838771636
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Bson.BsonProperty> Newtonsoft.Json.Bson.BsonObject::_children
	List_1_t2602298693 * ____children_2;

public:
	inline static int32_t get_offset_of__children_2() { return static_cast<int32_t>(offsetof(BsonObject_t3552490343, ____children_2)); }
	inline List_1_t2602298693 * get__children_2() const { return ____children_2; }
	inline List_1_t2602298693 ** get_address_of__children_2() { return &____children_2; }
	inline void set__children_2(List_1_t2602298693 * value)
	{
		____children_2 = value;
		Il2CppCodeGenWriteBarrier((&____children_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONOBJECT_T3552490343_H
#ifndef XMLNODECONVERTER_T181348760_H
#define XMLNODECONVERTER_T181348760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlNodeConverter
struct  XmlNodeConverter_t181348760  : public JsonConverter_t1047106545
{
public:
	// System.String Newtonsoft.Json.Converters.XmlNodeConverter::<DeserializeRootElementName>k__BackingField
	String_t* ___U3CDeserializeRootElementNameU3Ek__BackingField_0;
	// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::<WriteArrayAttribute>k__BackingField
	bool ___U3CWriteArrayAttributeU3Ek__BackingField_1;
	// System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::<OmitRootObject>k__BackingField
	bool ___U3COmitRootObjectU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CDeserializeRootElementNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(XmlNodeConverter_t181348760, ___U3CDeserializeRootElementNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CDeserializeRootElementNameU3Ek__BackingField_0() const { return ___U3CDeserializeRootElementNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CDeserializeRootElementNameU3Ek__BackingField_0() { return &___U3CDeserializeRootElementNameU3Ek__BackingField_0; }
	inline void set_U3CDeserializeRootElementNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CDeserializeRootElementNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeserializeRootElementNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CWriteArrayAttributeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(XmlNodeConverter_t181348760, ___U3CWriteArrayAttributeU3Ek__BackingField_1)); }
	inline bool get_U3CWriteArrayAttributeU3Ek__BackingField_1() const { return ___U3CWriteArrayAttributeU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CWriteArrayAttributeU3Ek__BackingField_1() { return &___U3CWriteArrayAttributeU3Ek__BackingField_1; }
	inline void set_U3CWriteArrayAttributeU3Ek__BackingField_1(bool value)
	{
		___U3CWriteArrayAttributeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3COmitRootObjectU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(XmlNodeConverter_t181348760, ___U3COmitRootObjectU3Ek__BackingField_2)); }
	inline bool get_U3COmitRootObjectU3Ek__BackingField_2() const { return ___U3COmitRootObjectU3Ek__BackingField_2; }
	inline bool* get_address_of_U3COmitRootObjectU3Ek__BackingField_2() { return &___U3COmitRootObjectU3Ek__BackingField_2; }
	inline void set_U3COmitRootObjectU3Ek__BackingField_2(bool value)
	{
		___U3COmitRootObjectU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODECONVERTER_T181348760_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef XATTRIBUTEWRAPPER_T1656890028_H
#define XATTRIBUTEWRAPPER_T1656890028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XAttributeWrapper
struct  XAttributeWrapper_t1656890028  : public XObjectWrapper_t2266598274
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XATTRIBUTEWRAPPER_T1656890028_H
#ifndef XCONTAINERWRAPPER_T1262744989_H
#define XCONTAINERWRAPPER_T1262744989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XContainerWrapper
struct  XContainerWrapper_t1262744989  : public XObjectWrapper_t2266598274
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XContainerWrapper::_childNodes
	List_1_t2041514553 * ____childNodes_2;

public:
	inline static int32_t get_offset_of__childNodes_2() { return static_cast<int32_t>(offsetof(XContainerWrapper_t1262744989, ____childNodes_2)); }
	inline List_1_t2041514553 * get__childNodes_2() const { return ____childNodes_2; }
	inline List_1_t2041514553 ** get_address_of__childNodes_2() { return &____childNodes_2; }
	inline void set__childNodes_2(List_1_t2041514553 * value)
	{
		____childNodes_2 = value;
		Il2CppCodeGenWriteBarrier((&____childNodes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCONTAINERWRAPPER_T1262744989_H
#ifndef XPROCESSINGINSTRUCTIONWRAPPER_T4108965662_H
#define XPROCESSINGINSTRUCTIONWRAPPER_T4108965662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XProcessingInstructionWrapper
struct  XProcessingInstructionWrapper_t4108965662  : public XObjectWrapper_t2266598274
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPROCESSINGINSTRUCTIONWRAPPER_T4108965662_H
#ifndef XCOMMENTWRAPPER_T3809443387_H
#define XCOMMENTWRAPPER_T3809443387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XCommentWrapper
struct  XCommentWrapper_t3809443387  : public XObjectWrapper_t2266598274
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XCOMMENTWRAPPER_T3809443387_H
#ifndef XTEXTWRAPPER_T1757148629_H
#define XTEXTWRAPPER_T1757148629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XTextWrapper
struct  XTextWrapper_t1757148629  : public XObjectWrapper_t2266598274
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XTEXTWRAPPER_T1757148629_H
#ifndef BSONARRAY_T345506913_H
#define BSONARRAY_T345506913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonArray
struct  BsonArray_t345506913  : public BsonToken_t3838771636
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Bson.BsonToken> Newtonsoft.Json.Bson.BsonArray::_children
	List_1_t1015879082 * ____children_2;

public:
	inline static int32_t get_offset_of__children_2() { return static_cast<int32_t>(offsetof(BsonArray_t345506913, ____children_2)); }
	inline List_1_t1015879082 * get__children_2() const { return ____children_2; }
	inline List_1_t1015879082 ** get_address_of__children_2() { return &____children_2; }
	inline void set__children_2(List_1_t1015879082 * value)
	{
		____children_2 = value;
		Il2CppCodeGenWriteBarrier((&____children_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONARRAY_T345506913_H
#ifndef __STATICARRAYINITTYPESIZEU3D52_T2710732173_H
#define __STATICARRAYINITTYPESIZEU3D52_T2710732173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52
struct  __StaticArrayInitTypeSizeU3D52_t2710732173 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D52_t2710732173__padding[52];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D52_T2710732173_H
#ifndef __STATICARRAYINITTYPESIZEU3D28_T1904621871_H
#define __STATICARRAYINITTYPESIZEU3D28_T1904621871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28
struct  __StaticArrayInitTypeSizeU3D28_t1904621871 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D28_t1904621871__padding[28];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D28_T1904621871_H
#ifndef __STATICARRAYINITTYPESIZEU3D12_T2710994317_H
#define __STATICARRAYINITTYPESIZEU3D12_T2710994317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12
struct  __StaticArrayInitTypeSizeU3D12_t2710994317 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t2710994317__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D12_T2710994317_H
#ifndef __STATICARRAYINITTYPESIZEU3D10_T1548194903_H
#define __STATICARRAYINITTYPESIZEU3D10_T1548194903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=10
struct  __StaticArrayInitTypeSizeU3D10_t1548194903 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D10_t1548194903__padding[10];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D10_T1548194903_H
#ifndef NULLABLE_1_T378540539_H
#define NULLABLE_1_T378540539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t378540539 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T378540539_H
#ifndef BSONREGEX_T323777446_H
#define BSONREGEX_T323777446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonRegex
struct  BsonRegex_t323777446  : public BsonToken_t3838771636
{
public:
	// Newtonsoft.Json.Bson.BsonString Newtonsoft.Json.Bson.BsonRegex::<Pattern>k__BackingField
	BsonString_t1135579766 * ___U3CPatternU3Ek__BackingField_2;
	// Newtonsoft.Json.Bson.BsonString Newtonsoft.Json.Bson.BsonRegex::<Options>k__BackingField
	BsonString_t1135579766 * ___U3COptionsU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CPatternU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BsonRegex_t323777446, ___U3CPatternU3Ek__BackingField_2)); }
	inline BsonString_t1135579766 * get_U3CPatternU3Ek__BackingField_2() const { return ___U3CPatternU3Ek__BackingField_2; }
	inline BsonString_t1135579766 ** get_address_of_U3CPatternU3Ek__BackingField_2() { return &___U3CPatternU3Ek__BackingField_2; }
	inline void set_U3CPatternU3Ek__BackingField_2(BsonString_t1135579766 * value)
	{
		___U3CPatternU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPatternU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3COptionsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BsonRegex_t323777446, ___U3COptionsU3Ek__BackingField_3)); }
	inline BsonString_t1135579766 * get_U3COptionsU3Ek__BackingField_3() const { return ___U3COptionsU3Ek__BackingField_3; }
	inline BsonString_t1135579766 ** get_address_of_U3COptionsU3Ek__BackingField_3() { return &___U3COptionsU3Ek__BackingField_3; }
	inline void set_U3COptionsU3Ek__BackingField_3(BsonString_t1135579766 * value)
	{
		___U3COptionsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3COptionsU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONREGEX_T323777446_H
#ifndef MINATTRIBUTE_T1755422200_H
#define MINATTRIBUTE_T1755422200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.MinAttribute
struct  MinAttribute_t1755422200  : public PropertyAttribute_t3677895545
{
public:
	// System.Single Colorful.MinAttribute::Min
	float ___Min_0;

public:
	inline static int32_t get_offset_of_Min_0() { return static_cast<int32_t>(offsetof(MinAttribute_t1755422200, ___Min_0)); }
	inline float get_Min_0() const { return ___Min_0; }
	inline float* get_address_of_Min_0() { return &___Min_0; }
	inline void set_Min_0(float value)
	{
		___Min_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINATTRIBUTE_T1755422200_H
#ifndef BLENDINGMODE_T1651387113_H
#define BLENDINGMODE_T1651387113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Blend/BlendingMode
struct  BlendingMode_t1651387113 
{
public:
	// System.Int32 Colorful.Blend/BlendingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlendingMode_t1651387113, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDINGMODE_T1651387113_H
#ifndef XDOCUMENTWRAPPER_T2289369211_H
#define XDOCUMENTWRAPPER_T2289369211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XDocumentWrapper
struct  XDocumentWrapper_t2289369211  : public XContainerWrapper_t1262744989
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDOCUMENTWRAPPER_T2289369211_H
#ifndef FORMATTING_T2192044929_H
#define FORMATTING_T2192044929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Formatting
struct  Formatting_t2192044929 
{
public:
	// System.Int32 Newtonsoft.Json.Formatting::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Formatting_t2192044929, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTING_T2192044929_H
#ifndef DATEFORMATHANDLING_T1376167855_H
#define DATEFORMATHANDLING_T1376167855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateFormatHandling
struct  DateFormatHandling_t1376167855 
{
public:
	// System.Int32 Newtonsoft.Json.DateFormatHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateFormatHandling_t1376167855, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEFORMATHANDLING_T1376167855_H
#ifndef STRINGESCAPEHANDLING_T4077875565_H
#define STRINGESCAPEHANDLING_T4077875565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.StringEscapeHandling
struct  StringEscapeHandling_t4077875565 
{
public:
	// System.Int32 Newtonsoft.Json.StringEscapeHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StringEscapeHandling_t4077875565, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGESCAPEHANDLING_T4077875565_H
#ifndef FLOATFORMATHANDLING_T562907821_H
#define FLOATFORMATHANDLING_T562907821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.FloatFormatHandling
struct  FloatFormatHandling_t562907821 
{
public:
	// System.Int32 Newtonsoft.Json.FloatFormatHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FloatFormatHandling_t562907821, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATFORMATHANDLING_T562907821_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef JSONCONTAINERTYPE_T3191599701_H
#define JSONCONTAINERTYPE_T3191599701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonContainerType
struct  JsonContainerType_t3191599701 
{
public:
	// System.Int32 Newtonsoft.Json.JsonContainerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JsonContainerType_t3191599701, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTAINERTYPE_T3191599701_H
#ifndef STATE_T2595666649_H
#define STATE_T2595666649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter/State
struct  State_t2595666649 
{
public:
	// System.Int32 Newtonsoft.Json.JsonWriter/State::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(State_t2595666649, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T2595666649_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t1773347010 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline Collider_t1773347010 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t1773347010 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t1773347010 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_pinvoke
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t1056001966_marshaled_com
{
	Vector3_t3722313464  ___m_Point_0;
	Vector3_t3722313464  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2156229523  ___m_UV_4;
	Collider_t1773347010 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T1056001966_H
#ifndef JSONTOKEN_T1917433489_H
#define JSONTOKEN_T1917433489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonToken
struct  JsonToken_t1917433489 
{
public:
	// System.Int32 Newtonsoft.Json.JsonToken::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JsonToken_t1917433489, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTOKEN_T1917433489_H
#ifndef STATE_T879723262_H
#define STATE_T879723262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonReader/State
struct  State_t879723262 
{
public:
	// System.Int32 Newtonsoft.Json.JsonReader/State::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(State_t879723262, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T879723262_H
#ifndef DATETIMEZONEHANDLING_T3002599730_H
#define DATETIMEZONEHANDLING_T3002599730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateTimeZoneHandling
struct  DateTimeZoneHandling_t3002599730 
{
public:
	// System.Int32 Newtonsoft.Json.DateTimeZoneHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeZoneHandling_t3002599730, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEZONEHANDLING_T3002599730_H
#ifndef DATEPARSEHANDLING_T2751701876_H
#define DATEPARSEHANDLING_T2751701876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateParseHandling
struct  DateParseHandling_t2751701876 
{
public:
	// System.Int32 Newtonsoft.Json.DateParseHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateParseHandling_t2751701876, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEPARSEHANDLING_T2751701876_H
#ifndef FLOATPARSEHANDLING_T1006965658_H
#define FLOATPARSEHANDLING_T1006965658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.FloatParseHandling
struct  FloatParseHandling_t1006965658 
{
public:
	// System.Int32 Newtonsoft.Json.FloatParseHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FloatParseHandling_t1006965658, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATPARSEHANDLING_T1006965658_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef REQUIREMENTTYPE_T3584265503_H
#define REQUIREMENTTYPE_T3584265503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParam/RequirementType
struct  RequirementType_t3584265503 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsEventParam/RequirementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RequirementType_t3584265503, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIREMENTTYPE_T3584265503_H
#ifndef PROPERTYTYPE_T4040930247_H
#define PROPERTYTYPE_T4040930247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ValueProperty/PropertyType
struct  PropertyType_t4040930247 
{
public:
	// System.Int32 UnityEngine.Analytics.ValueProperty/PropertyType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PropertyType_t4040930247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYTYPE_T4040930247_H
#ifndef TRIGGER_T4199345191_H
#define TRIGGER_T4199345191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker/Trigger
struct  Trigger_t4199345191 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker/Trigger::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Trigger_t4199345191, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_T4199345191_H
#ifndef TRIGGERBOOL_T501031542_H
#define TRIGGERBOOL_T501031542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerBool
struct  TriggerBool_t501031542 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerBool::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerBool_t501031542, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERBOOL_T501031542_H
#ifndef RT_T167452472_H
#define RT_T167452472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RT
struct  RT_t167452472  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 RT::pos
	Vector3_t3722313464  ___pos_0;
	// UnityEngine.Vector2 RT::anchorsMin
	Vector2_t2156229523  ___anchorsMin_1;
	// UnityEngine.Vector2 RT::anchorsMax
	Vector2_t2156229523  ___anchorsMax_2;
	// UnityEngine.Vector2 RT::pivot
	Vector2_t2156229523  ___pivot_3;
	// UnityEngine.Transform RT::parent
	Transform_t3600365921 * ___parent_4;

public:
	inline static int32_t get_offset_of_pos_0() { return static_cast<int32_t>(offsetof(RT_t167452472, ___pos_0)); }
	inline Vector3_t3722313464  get_pos_0() const { return ___pos_0; }
	inline Vector3_t3722313464 * get_address_of_pos_0() { return &___pos_0; }
	inline void set_pos_0(Vector3_t3722313464  value)
	{
		___pos_0 = value;
	}

	inline static int32_t get_offset_of_anchorsMin_1() { return static_cast<int32_t>(offsetof(RT_t167452472, ___anchorsMin_1)); }
	inline Vector2_t2156229523  get_anchorsMin_1() const { return ___anchorsMin_1; }
	inline Vector2_t2156229523 * get_address_of_anchorsMin_1() { return &___anchorsMin_1; }
	inline void set_anchorsMin_1(Vector2_t2156229523  value)
	{
		___anchorsMin_1 = value;
	}

	inline static int32_t get_offset_of_anchorsMax_2() { return static_cast<int32_t>(offsetof(RT_t167452472, ___anchorsMax_2)); }
	inline Vector2_t2156229523  get_anchorsMax_2() const { return ___anchorsMax_2; }
	inline Vector2_t2156229523 * get_address_of_anchorsMax_2() { return &___anchorsMax_2; }
	inline void set_anchorsMax_2(Vector2_t2156229523  value)
	{
		___anchorsMax_2 = value;
	}

	inline static int32_t get_offset_of_pivot_3() { return static_cast<int32_t>(offsetof(RT_t167452472, ___pivot_3)); }
	inline Vector2_t2156229523  get_pivot_3() const { return ___pivot_3; }
	inline Vector2_t2156229523 * get_address_of_pivot_3() { return &___pivot_3; }
	inline void set_pivot_3(Vector2_t2156229523  value)
	{
		___pivot_3 = value;
	}

	inline static int32_t get_offset_of_parent_4() { return static_cast<int32_t>(offsetof(RT_t167452472, ___parent_4)); }
	inline Transform_t3600365921 * get_parent_4() const { return ___parent_4; }
	inline Transform_t3600365921 ** get_address_of_parent_4() { return &___parent_4; }
	inline void set_parent_4(Transform_t3600365921 * value)
	{
		___parent_4 = value;
		Il2CppCodeGenWriteBarrier((&___parent_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RT_T167452472_H
#ifndef BSONREADERSTATE_T1965113074_H
#define BSONREADERSTATE_T1965113074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonReader/BsonReaderState
struct  BsonReaderState_t1965113074 
{
public:
	// System.Int32 Newtonsoft.Json.Bson.BsonReader/BsonReaderState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BsonReaderState_t1965113074, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONREADERSTATE_T1965113074_H
#ifndef XELEMENTWRAPPER_T2633871282_H
#define XELEMENTWRAPPER_T2633871282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XElementWrapper
struct  XElementWrapper_t2633871282  : public XContainerWrapper_t1262744989
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XElementWrapper::_attributes
	List_1_t2041514553 * ____attributes_3;

public:
	inline static int32_t get_offset_of__attributes_3() { return static_cast<int32_t>(offsetof(XElementWrapper_t2633871282, ____attributes_3)); }
	inline List_1_t2041514553 * get__attributes_3() const { return ____attributes_3; }
	inline List_1_t2041514553 ** get_address_of__attributes_3() { return &____attributes_3; }
	inline void set__attributes_3(List_1_t2041514553 * value)
	{
		____attributes_3 = value;
		Il2CppCodeGenWriteBarrier((&____attributes_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XELEMENTWRAPPER_T2633871282_H
#ifndef TRIGGERTYPE_T105272677_H
#define TRIGGERTYPE_T105272677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerType
struct  TriggerType_t105272677 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerType_t105272677, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERTYPE_T105272677_H
#ifndef BSONBINARYTYPE_T2212550840_H
#define BSONBINARYTYPE_T2212550840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonBinaryType
struct  BsonBinaryType_t2212550840 
{
public:
	// System.Byte Newtonsoft.Json.Bson.BsonBinaryType::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BsonBinaryType_t2212550840, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONBINARYTYPE_T2212550840_H
#ifndef TRIGGEROPERATOR_T3611898925_H
#define TRIGGEROPERATOR_T3611898925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerOperator
struct  TriggerOperator_t3611898925 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerOperator::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerOperator_t3611898925, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEROPERATOR_T3611898925_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255369_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255369  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=28 <PrivateImplementationDetails>::9E31F24F64765FCAA589F589324D17C9FCF6A06D
	__StaticArrayInitTypeSizeU3D28_t1904621871  ___9E31F24F64765FCAA589F589324D17C9FCF6A06D_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=12 <PrivateImplementationDetails>::ADFD2E1C801C825415DD53F4F2F72A13B389313C
	__StaticArrayInitTypeSizeU3D12_t2710994317  ___ADFD2E1C801C825415DD53F4F2F72A13B389313C_1;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=10 <PrivateImplementationDetails>::D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB
	__StaticArrayInitTypeSizeU3D10_t1548194903  ___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::DD3AEFEADB1CD615F3017763F1568179FEE640B0
	__StaticArrayInitTypeSizeU3D52_t2710732173  ___DD3AEFEADB1CD615F3017763F1568179FEE640B0_3;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=52 <PrivateImplementationDetails>::E92B39D8233061927D9ACDE54665E68E7535635A
	__StaticArrayInitTypeSizeU3D52_t2710732173  ___E92B39D8233061927D9ACDE54665E68E7535635A_4;

public:
	inline static int32_t get_offset_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields, ___9E31F24F64765FCAA589F589324D17C9FCF6A06D_0)); }
	inline __StaticArrayInitTypeSizeU3D28_t1904621871  get_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_0() const { return ___9E31F24F64765FCAA589F589324D17C9FCF6A06D_0; }
	inline __StaticArrayInitTypeSizeU3D28_t1904621871 * get_address_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_0() { return &___9E31F24F64765FCAA589F589324D17C9FCF6A06D_0; }
	inline void set_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_0(__StaticArrayInitTypeSizeU3D28_t1904621871  value)
	{
		___9E31F24F64765FCAA589F589324D17C9FCF6A06D_0 = value;
	}

	inline static int32_t get_offset_of_ADFD2E1C801C825415DD53F4F2F72A13B389313C_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields, ___ADFD2E1C801C825415DD53F4F2F72A13B389313C_1)); }
	inline __StaticArrayInitTypeSizeU3D12_t2710994317  get_ADFD2E1C801C825415DD53F4F2F72A13B389313C_1() const { return ___ADFD2E1C801C825415DD53F4F2F72A13B389313C_1; }
	inline __StaticArrayInitTypeSizeU3D12_t2710994317 * get_address_of_ADFD2E1C801C825415DD53F4F2F72A13B389313C_1() { return &___ADFD2E1C801C825415DD53F4F2F72A13B389313C_1; }
	inline void set_ADFD2E1C801C825415DD53F4F2F72A13B389313C_1(__StaticArrayInitTypeSizeU3D12_t2710994317  value)
	{
		___ADFD2E1C801C825415DD53F4F2F72A13B389313C_1 = value;
	}

	inline static int32_t get_offset_of_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields, ___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2)); }
	inline __StaticArrayInitTypeSizeU3D10_t1548194903  get_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2() const { return ___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2; }
	inline __StaticArrayInitTypeSizeU3D10_t1548194903 * get_address_of_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2() { return &___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2; }
	inline void set_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2(__StaticArrayInitTypeSizeU3D10_t1548194903  value)
	{
		___D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2 = value;
	}

	inline static int32_t get_offset_of_DD3AEFEADB1CD615F3017763F1568179FEE640B0_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields, ___DD3AEFEADB1CD615F3017763F1568179FEE640B0_3)); }
	inline __StaticArrayInitTypeSizeU3D52_t2710732173  get_DD3AEFEADB1CD615F3017763F1568179FEE640B0_3() const { return ___DD3AEFEADB1CD615F3017763F1568179FEE640B0_3; }
	inline __StaticArrayInitTypeSizeU3D52_t2710732173 * get_address_of_DD3AEFEADB1CD615F3017763F1568179FEE640B0_3() { return &___DD3AEFEADB1CD615F3017763F1568179FEE640B0_3; }
	inline void set_DD3AEFEADB1CD615F3017763F1568179FEE640B0_3(__StaticArrayInitTypeSizeU3D52_t2710732173  value)
	{
		___DD3AEFEADB1CD615F3017763F1568179FEE640B0_3 = value;
	}

	inline static int32_t get_offset_of_E92B39D8233061927D9ACDE54665E68E7535635A_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields, ___E92B39D8233061927D9ACDE54665E68E7535635A_4)); }
	inline __StaticArrayInitTypeSizeU3D52_t2710732173  get_E92B39D8233061927D9ACDE54665E68E7535635A_4() const { return ___E92B39D8233061927D9ACDE54665E68E7535635A_4; }
	inline __StaticArrayInitTypeSizeU3D52_t2710732173 * get_address_of_E92B39D8233061927D9ACDE54665E68E7535635A_4() { return &___E92B39D8233061927D9ACDE54665E68E7535635A_4; }
	inline void set_E92B39D8233061927D9ACDE54665E68E7535635A_4(__StaticArrayInitTypeSizeU3D52_t2710732173  value)
	{
		___E92B39D8233061927D9ACDE54665E68E7535635A_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255369_H
#ifndef TRIGGERLIFECYCLEEVENT_T3193146760_H
#define TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerLifecycleEvent
struct  TriggerLifecycleEvent_t3193146760 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerLifecycleEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerLifecycleEvent_t3193146760, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifndef BSONTYPE_T1843803290_H
#define BSONTYPE_T1843803290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonType
struct  BsonType_t1843803290 
{
public:
	// System.SByte Newtonsoft.Json.Bson.BsonType::value__
	int8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BsonType_t1843803290, ___value___1)); }
	inline int8_t get_value___1() const { return ___value___1; }
	inline int8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONTYPE_T1843803290_H
#ifndef ANALYTICSEVENTPARAM_T2480121928_H
#define ANALYTICSEVENTPARAM_T2480121928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventParam
struct  AnalyticsEventParam_t2480121928  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.AnalyticsEventParam/RequirementType UnityEngine.Analytics.AnalyticsEventParam::m_RequirementType
	int32_t ___m_RequirementType_0;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_GroupID
	String_t* ___m_GroupID_1;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_Tooltip
	String_t* ___m_Tooltip_2;
	// System.String UnityEngine.Analytics.AnalyticsEventParam::m_Name
	String_t* ___m_Name_3;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.AnalyticsEventParam::m_Value
	ValueProperty_t1868393739 * ___m_Value_4;

public:
	inline static int32_t get_offset_of_m_RequirementType_0() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_RequirementType_0)); }
	inline int32_t get_m_RequirementType_0() const { return ___m_RequirementType_0; }
	inline int32_t* get_address_of_m_RequirementType_0() { return &___m_RequirementType_0; }
	inline void set_m_RequirementType_0(int32_t value)
	{
		___m_RequirementType_0 = value;
	}

	inline static int32_t get_offset_of_m_GroupID_1() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_GroupID_1)); }
	inline String_t* get_m_GroupID_1() const { return ___m_GroupID_1; }
	inline String_t** get_address_of_m_GroupID_1() { return &___m_GroupID_1; }
	inline void set_m_GroupID_1(String_t* value)
	{
		___m_GroupID_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_GroupID_1), value);
	}

	inline static int32_t get_offset_of_m_Tooltip_2() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Tooltip_2)); }
	inline String_t* get_m_Tooltip_2() const { return ___m_Tooltip_2; }
	inline String_t** get_address_of_m_Tooltip_2() { return &___m_Tooltip_2; }
	inline void set_m_Tooltip_2(String_t* value)
	{
		___m_Tooltip_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tooltip_2), value);
	}

	inline static int32_t get_offset_of_m_Name_3() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Name_3)); }
	inline String_t* get_m_Name_3() const { return ___m_Name_3; }
	inline String_t** get_address_of_m_Name_3() { return &___m_Name_3; }
	inline void set_m_Name_3(String_t* value)
	{
		___m_Name_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_3), value);
	}

	inline static int32_t get_offset_of_m_Value_4() { return static_cast<int32_t>(offsetof(AnalyticsEventParam_t2480121928, ___m_Value_4)); }
	inline ValueProperty_t1868393739 * get_m_Value_4() const { return ___m_Value_4; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_4() { return &___m_Value_4; }
	inline void set_m_Value_4(ValueProperty_t1868393739 * value)
	{
		___m_Value_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTPARAM_T2480121928_H
#ifndef VALUEPROPERTY_T1868393739_H
#define VALUEPROPERTY_T1868393739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.ValueProperty
struct  ValueProperty_t1868393739  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_EditingCustomValue
	bool ___m_EditingCustomValue_0;
	// System.Int32 UnityEngine.Analytics.ValueProperty::m_PopupIndex
	int32_t ___m_PopupIndex_1;
	// System.String UnityEngine.Analytics.ValueProperty::m_CustomValue
	String_t* ___m_CustomValue_2;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_FixedType
	bool ___m_FixedType_3;
	// System.String UnityEngine.Analytics.ValueProperty::m_EnumType
	String_t* ___m_EnumType_4;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_EnumTypeIsCustomizable
	bool ___m_EnumTypeIsCustomizable_5;
	// System.Boolean UnityEngine.Analytics.ValueProperty::m_CanDisable
	bool ___m_CanDisable_6;
	// UnityEngine.Analytics.ValueProperty/PropertyType UnityEngine.Analytics.ValueProperty::m_PropertyType
	int32_t ___m_PropertyType_7;
	// System.String UnityEngine.Analytics.ValueProperty::m_ValueType
	String_t* ___m_ValueType_8;
	// System.String UnityEngine.Analytics.ValueProperty::m_Value
	String_t* ___m_Value_9;
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.ValueProperty::m_Target
	TrackableField_t1772682203 * ___m_Target_10;

public:
	inline static int32_t get_offset_of_m_EditingCustomValue_0() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EditingCustomValue_0)); }
	inline bool get_m_EditingCustomValue_0() const { return ___m_EditingCustomValue_0; }
	inline bool* get_address_of_m_EditingCustomValue_0() { return &___m_EditingCustomValue_0; }
	inline void set_m_EditingCustomValue_0(bool value)
	{
		___m_EditingCustomValue_0 = value;
	}

	inline static int32_t get_offset_of_m_PopupIndex_1() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_PopupIndex_1)); }
	inline int32_t get_m_PopupIndex_1() const { return ___m_PopupIndex_1; }
	inline int32_t* get_address_of_m_PopupIndex_1() { return &___m_PopupIndex_1; }
	inline void set_m_PopupIndex_1(int32_t value)
	{
		___m_PopupIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_CustomValue_2() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_CustomValue_2)); }
	inline String_t* get_m_CustomValue_2() const { return ___m_CustomValue_2; }
	inline String_t** get_address_of_m_CustomValue_2() { return &___m_CustomValue_2; }
	inline void set_m_CustomValue_2(String_t* value)
	{
		___m_CustomValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomValue_2), value);
	}

	inline static int32_t get_offset_of_m_FixedType_3() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_FixedType_3)); }
	inline bool get_m_FixedType_3() const { return ___m_FixedType_3; }
	inline bool* get_address_of_m_FixedType_3() { return &___m_FixedType_3; }
	inline void set_m_FixedType_3(bool value)
	{
		___m_FixedType_3 = value;
	}

	inline static int32_t get_offset_of_m_EnumType_4() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EnumType_4)); }
	inline String_t* get_m_EnumType_4() const { return ___m_EnumType_4; }
	inline String_t** get_address_of_m_EnumType_4() { return &___m_EnumType_4; }
	inline void set_m_EnumType_4(String_t* value)
	{
		___m_EnumType_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EnumType_4), value);
	}

	inline static int32_t get_offset_of_m_EnumTypeIsCustomizable_5() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_EnumTypeIsCustomizable_5)); }
	inline bool get_m_EnumTypeIsCustomizable_5() const { return ___m_EnumTypeIsCustomizable_5; }
	inline bool* get_address_of_m_EnumTypeIsCustomizable_5() { return &___m_EnumTypeIsCustomizable_5; }
	inline void set_m_EnumTypeIsCustomizable_5(bool value)
	{
		___m_EnumTypeIsCustomizable_5 = value;
	}

	inline static int32_t get_offset_of_m_CanDisable_6() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_CanDisable_6)); }
	inline bool get_m_CanDisable_6() const { return ___m_CanDisable_6; }
	inline bool* get_address_of_m_CanDisable_6() { return &___m_CanDisable_6; }
	inline void set_m_CanDisable_6(bool value)
	{
		___m_CanDisable_6 = value;
	}

	inline static int32_t get_offset_of_m_PropertyType_7() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_PropertyType_7)); }
	inline int32_t get_m_PropertyType_7() const { return ___m_PropertyType_7; }
	inline int32_t* get_address_of_m_PropertyType_7() { return &___m_PropertyType_7; }
	inline void set_m_PropertyType_7(int32_t value)
	{
		___m_PropertyType_7 = value;
	}

	inline static int32_t get_offset_of_m_ValueType_8() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_ValueType_8)); }
	inline String_t* get_m_ValueType_8() const { return ___m_ValueType_8; }
	inline String_t** get_address_of_m_ValueType_8() { return &___m_ValueType_8; }
	inline void set_m_ValueType_8(String_t* value)
	{
		___m_ValueType_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_ValueType_8), value);
	}

	inline static int32_t get_offset_of_m_Value_9() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_Value_9)); }
	inline String_t* get_m_Value_9() const { return ___m_Value_9; }
	inline String_t** get_address_of_m_Value_9() { return &___m_Value_9; }
	inline void set_m_Value_9(String_t* value)
	{
		___m_Value_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_9), value);
	}

	inline static int32_t get_offset_of_m_Target_10() { return static_cast<int32_t>(offsetof(ValueProperty_t1868393739, ___m_Target_10)); }
	inline TrackableField_t1772682203 * get_m_Target_10() const { return ___m_Target_10; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_10() { return &___m_Target_10; }
	inline void set_m_Target_10(TrackableField_t1772682203 * value)
	{
		___m_Target_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALUEPROPERTY_T1868393739_H
#ifndef EVENTTRIGGER_T2527451695_H
#define EVENTTRIGGER_T2527451695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger
struct  EventTrigger_t2527451695  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_IsTriggerExpanded
	bool ___m_IsTriggerExpanded_0;
	// UnityEngine.Analytics.TriggerType UnityEngine.Analytics.EventTrigger::m_Type
	int32_t ___m_Type_1;
	// UnityEngine.Analytics.TriggerLifecycleEvent UnityEngine.Analytics.EventTrigger::m_LifecycleEvent
	int32_t ___m_LifecycleEvent_2;
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_ApplyRules
	bool ___m_ApplyRules_3;
	// UnityEngine.Analytics.TriggerListContainer UnityEngine.Analytics.EventTrigger::m_Rules
	TriggerListContainer_t2032715483 * ___m_Rules_4;
	// UnityEngine.Analytics.TriggerBool UnityEngine.Analytics.EventTrigger::m_TriggerBool
	int32_t ___m_TriggerBool_5;
	// System.Single UnityEngine.Analytics.EventTrigger::m_InitTime
	float ___m_InitTime_6;
	// System.Single UnityEngine.Analytics.EventTrigger::m_RepeatTime
	float ___m_RepeatTime_7;
	// System.Int32 UnityEngine.Analytics.EventTrigger::m_Repetitions
	int32_t ___m_Repetitions_8;
	// System.Int32 UnityEngine.Analytics.EventTrigger::repetitionCount
	int32_t ___repetitionCount_9;
	// UnityEngine.Analytics.EventTrigger/OnTrigger UnityEngine.Analytics.EventTrigger::m_TriggerFunction
	OnTrigger_t4184125570 * ___m_TriggerFunction_10;
	// UnityEngine.Analytics.TriggerMethod UnityEngine.Analytics.EventTrigger::m_Method
	TriggerMethod_t582536534 * ___m_Method_11;

public:
	inline static int32_t get_offset_of_m_IsTriggerExpanded_0() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_IsTriggerExpanded_0)); }
	inline bool get_m_IsTriggerExpanded_0() const { return ___m_IsTriggerExpanded_0; }
	inline bool* get_address_of_m_IsTriggerExpanded_0() { return &___m_IsTriggerExpanded_0; }
	inline void set_m_IsTriggerExpanded_0(bool value)
	{
		___m_IsTriggerExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_Type_1() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Type_1)); }
	inline int32_t get_m_Type_1() const { return ___m_Type_1; }
	inline int32_t* get_address_of_m_Type_1() { return &___m_Type_1; }
	inline void set_m_Type_1(int32_t value)
	{
		___m_Type_1 = value;
	}

	inline static int32_t get_offset_of_m_LifecycleEvent_2() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_LifecycleEvent_2)); }
	inline int32_t get_m_LifecycleEvent_2() const { return ___m_LifecycleEvent_2; }
	inline int32_t* get_address_of_m_LifecycleEvent_2() { return &___m_LifecycleEvent_2; }
	inline void set_m_LifecycleEvent_2(int32_t value)
	{
		___m_LifecycleEvent_2 = value;
	}

	inline static int32_t get_offset_of_m_ApplyRules_3() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_ApplyRules_3)); }
	inline bool get_m_ApplyRules_3() const { return ___m_ApplyRules_3; }
	inline bool* get_address_of_m_ApplyRules_3() { return &___m_ApplyRules_3; }
	inline void set_m_ApplyRules_3(bool value)
	{
		___m_ApplyRules_3 = value;
	}

	inline static int32_t get_offset_of_m_Rules_4() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Rules_4)); }
	inline TriggerListContainer_t2032715483 * get_m_Rules_4() const { return ___m_Rules_4; }
	inline TriggerListContainer_t2032715483 ** get_address_of_m_Rules_4() { return &___m_Rules_4; }
	inline void set_m_Rules_4(TriggerListContainer_t2032715483 * value)
	{
		___m_Rules_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_4), value);
	}

	inline static int32_t get_offset_of_m_TriggerBool_5() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerBool_5)); }
	inline int32_t get_m_TriggerBool_5() const { return ___m_TriggerBool_5; }
	inline int32_t* get_address_of_m_TriggerBool_5() { return &___m_TriggerBool_5; }
	inline void set_m_TriggerBool_5(int32_t value)
	{
		___m_TriggerBool_5 = value;
	}

	inline static int32_t get_offset_of_m_InitTime_6() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_InitTime_6)); }
	inline float get_m_InitTime_6() const { return ___m_InitTime_6; }
	inline float* get_address_of_m_InitTime_6() { return &___m_InitTime_6; }
	inline void set_m_InitTime_6(float value)
	{
		___m_InitTime_6 = value;
	}

	inline static int32_t get_offset_of_m_RepeatTime_7() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_RepeatTime_7)); }
	inline float get_m_RepeatTime_7() const { return ___m_RepeatTime_7; }
	inline float* get_address_of_m_RepeatTime_7() { return &___m_RepeatTime_7; }
	inline void set_m_RepeatTime_7(float value)
	{
		___m_RepeatTime_7 = value;
	}

	inline static int32_t get_offset_of_m_Repetitions_8() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Repetitions_8)); }
	inline int32_t get_m_Repetitions_8() const { return ___m_Repetitions_8; }
	inline int32_t* get_address_of_m_Repetitions_8() { return &___m_Repetitions_8; }
	inline void set_m_Repetitions_8(int32_t value)
	{
		___m_Repetitions_8 = value;
	}

	inline static int32_t get_offset_of_repetitionCount_9() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___repetitionCount_9)); }
	inline int32_t get_repetitionCount_9() const { return ___repetitionCount_9; }
	inline int32_t* get_address_of_repetitionCount_9() { return &___repetitionCount_9; }
	inline void set_repetitionCount_9(int32_t value)
	{
		___repetitionCount_9 = value;
	}

	inline static int32_t get_offset_of_m_TriggerFunction_10() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerFunction_10)); }
	inline OnTrigger_t4184125570 * get_m_TriggerFunction_10() const { return ___m_TriggerFunction_10; }
	inline OnTrigger_t4184125570 ** get_address_of_m_TriggerFunction_10() { return &___m_TriggerFunction_10; }
	inline void set_m_TriggerFunction_10(OnTrigger_t4184125570 * value)
	{
		___m_TriggerFunction_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TriggerFunction_10), value);
	}

	inline static int32_t get_offset_of_m_Method_11() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Method_11)); }
	inline TriggerMethod_t582536534 * get_m_Method_11() const { return ___m_Method_11; }
	inline TriggerMethod_t582536534 ** get_address_of_m_Method_11() { return &___m_Method_11; }
	inline void set_m_Method_11(TriggerMethod_t582536534 * value)
	{
		___m_Method_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Method_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGER_T2527451695_H
#ifndef TRIGGERRULE_T1946298321_H
#define TRIGGERRULE_T1946298321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerRule
struct  TriggerRule_t1946298321  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.TriggerRule::m_Target
	TrackableField_t1772682203 * ___m_Target_0;
	// UnityEngine.Analytics.TriggerOperator UnityEngine.Analytics.TriggerRule::m_Operator
	int32_t ___m_Operator_1;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value
	ValueProperty_t1868393739 * ___m_Value_2;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value2
	ValueProperty_t1868393739 * ___m_Value2_3;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Target_0)); }
	inline TrackableField_t1772682203 * get_m_Target_0() const { return ___m_Target_0; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(TrackableField_t1772682203 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Operator_1() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Operator_1)); }
	inline int32_t get_m_Operator_1() const { return ___m_Operator_1; }
	inline int32_t* get_address_of_m_Operator_1() { return &___m_Operator_1; }
	inline void set_m_Operator_1(int32_t value)
	{
		___m_Operator_1 = value;
	}

	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value_2)); }
	inline ValueProperty_t1868393739 * get_m_Value_2() const { return ___m_Value_2; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(ValueProperty_t1868393739 * value)
	{
		___m_Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_2), value);
	}

	inline static int32_t get_offset_of_m_Value2_3() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value2_3)); }
	inline ValueProperty_t1868393739 * get_m_Value2_3() const { return ___m_Value2_3; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value2_3() { return &___m_Value2_3; }
	inline void set_m_Value2_3(ValueProperty_t1868393739 * value)
	{
		___m_Value2_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERRULE_T1946298321_H
#ifndef JSONPOSITION_T2528027714_H
#define JSONPOSITION_T2528027714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonPosition
struct  JsonPosition_t2528027714 
{
public:
	// Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonPosition::Type
	int32_t ___Type_1;
	// System.Int32 Newtonsoft.Json.JsonPosition::Position
	int32_t ___Position_2;
	// System.String Newtonsoft.Json.JsonPosition::PropertyName
	String_t* ___PropertyName_3;
	// System.Boolean Newtonsoft.Json.JsonPosition::HasIndex
	bool ___HasIndex_4;

public:
	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(JsonPosition_t2528027714, ___Type_1)); }
	inline int32_t get_Type_1() const { return ___Type_1; }
	inline int32_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(int32_t value)
	{
		___Type_1 = value;
	}

	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(JsonPosition_t2528027714, ___Position_2)); }
	inline int32_t get_Position_2() const { return ___Position_2; }
	inline int32_t* get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(int32_t value)
	{
		___Position_2 = value;
	}

	inline static int32_t get_offset_of_PropertyName_3() { return static_cast<int32_t>(offsetof(JsonPosition_t2528027714, ___PropertyName_3)); }
	inline String_t* get_PropertyName_3() const { return ___PropertyName_3; }
	inline String_t** get_address_of_PropertyName_3() { return &___PropertyName_3; }
	inline void set_PropertyName_3(String_t* value)
	{
		___PropertyName_3 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyName_3), value);
	}

	inline static int32_t get_offset_of_HasIndex_4() { return static_cast<int32_t>(offsetof(JsonPosition_t2528027714, ___HasIndex_4)); }
	inline bool get_HasIndex_4() const { return ___HasIndex_4; }
	inline bool* get_address_of_HasIndex_4() { return &___HasIndex_4; }
	inline void set_HasIndex_4(bool value)
	{
		___HasIndex_4 = value;
	}
};

struct JsonPosition_t2528027714_StaticFields
{
public:
	// System.Char[] Newtonsoft.Json.JsonPosition::SpecialCharacters
	CharU5BU5D_t3528271667* ___SpecialCharacters_0;

public:
	inline static int32_t get_offset_of_SpecialCharacters_0() { return static_cast<int32_t>(offsetof(JsonPosition_t2528027714_StaticFields, ___SpecialCharacters_0)); }
	inline CharU5BU5D_t3528271667* get_SpecialCharacters_0() const { return ___SpecialCharacters_0; }
	inline CharU5BU5D_t3528271667** get_address_of_SpecialCharacters_0() { return &___SpecialCharacters_0; }
	inline void set_SpecialCharacters_0(CharU5BU5D_t3528271667* value)
	{
		___SpecialCharacters_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpecialCharacters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_t2528027714_marshaled_pinvoke
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	char* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
// Native definition for COM marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_t2528027714_marshaled_com
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	Il2CppChar* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
#endif // JSONPOSITION_T2528027714_H
#ifndef BSONBINARYWRITER_T671405346_H
#define BSONBINARYWRITER_T671405346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonBinaryWriter
struct  BsonBinaryWriter_t671405346  : public RuntimeObject
{
public:
	// System.IO.BinaryWriter Newtonsoft.Json.Bson.BsonBinaryWriter::_writer
	BinaryWriter_t3992595042 * ____writer_1;
	// System.Byte[] Newtonsoft.Json.Bson.BsonBinaryWriter::_largeByteBuffer
	ByteU5BU5D_t4116647657* ____largeByteBuffer_2;
	// System.DateTimeKind Newtonsoft.Json.Bson.BsonBinaryWriter::<DateTimeKindHandling>k__BackingField
	int32_t ___U3CDateTimeKindHandlingU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of__writer_1() { return static_cast<int32_t>(offsetof(BsonBinaryWriter_t671405346, ____writer_1)); }
	inline BinaryWriter_t3992595042 * get__writer_1() const { return ____writer_1; }
	inline BinaryWriter_t3992595042 ** get_address_of__writer_1() { return &____writer_1; }
	inline void set__writer_1(BinaryWriter_t3992595042 * value)
	{
		____writer_1 = value;
		Il2CppCodeGenWriteBarrier((&____writer_1), value);
	}

	inline static int32_t get_offset_of__largeByteBuffer_2() { return static_cast<int32_t>(offsetof(BsonBinaryWriter_t671405346, ____largeByteBuffer_2)); }
	inline ByteU5BU5D_t4116647657* get__largeByteBuffer_2() const { return ____largeByteBuffer_2; }
	inline ByteU5BU5D_t4116647657** get_address_of__largeByteBuffer_2() { return &____largeByteBuffer_2; }
	inline void set__largeByteBuffer_2(ByteU5BU5D_t4116647657* value)
	{
		____largeByteBuffer_2 = value;
		Il2CppCodeGenWriteBarrier((&____largeByteBuffer_2), value);
	}

	inline static int32_t get_offset_of_U3CDateTimeKindHandlingU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BsonBinaryWriter_t671405346, ___U3CDateTimeKindHandlingU3Ek__BackingField_3)); }
	inline int32_t get_U3CDateTimeKindHandlingU3Ek__BackingField_3() const { return ___U3CDateTimeKindHandlingU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CDateTimeKindHandlingU3Ek__BackingField_3() { return &___U3CDateTimeKindHandlingU3Ek__BackingField_3; }
	inline void set_U3CDateTimeKindHandlingU3Ek__BackingField_3(int32_t value)
	{
		___U3CDateTimeKindHandlingU3Ek__BackingField_3 = value;
	}
};

struct BsonBinaryWriter_t671405346_StaticFields
{
public:
	// System.Text.Encoding Newtonsoft.Json.Bson.BsonBinaryWriter::Encoding
	Encoding_t1523322056 * ___Encoding_0;

public:
	inline static int32_t get_offset_of_Encoding_0() { return static_cast<int32_t>(offsetof(BsonBinaryWriter_t671405346_StaticFields, ___Encoding_0)); }
	inline Encoding_t1523322056 * get_Encoding_0() const { return ___Encoding_0; }
	inline Encoding_t1523322056 ** get_address_of_Encoding_0() { return &___Encoding_0; }
	inline void set_Encoding_0(Encoding_t1523322056 * value)
	{
		___Encoding_0 = value;
		Il2CppCodeGenWriteBarrier((&___Encoding_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONBINARYWRITER_T671405346_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef CONTAINERCONTEXT_T1305994539_H
#define CONTAINERCONTEXT_T1305994539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonReader/ContainerContext
struct  ContainerContext_t1305994539  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonReader/ContainerContext::Type
	int8_t ___Type_0;
	// System.Int32 Newtonsoft.Json.Bson.BsonReader/ContainerContext::Length
	int32_t ___Length_1;
	// System.Int32 Newtonsoft.Json.Bson.BsonReader/ContainerContext::Position
	int32_t ___Position_2;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(ContainerContext_t1305994539, ___Type_0)); }
	inline int8_t get_Type_0() const { return ___Type_0; }
	inline int8_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int8_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_Length_1() { return static_cast<int32_t>(offsetof(ContainerContext_t1305994539, ___Length_1)); }
	inline int32_t get_Length_1() const { return ___Length_1; }
	inline int32_t* get_address_of_Length_1() { return &___Length_1; }
	inline void set_Length_1(int32_t value)
	{
		___Length_1 = value;
	}

	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(ContainerContext_t1305994539, ___Position_2)); }
	inline int32_t get_Position_2() const { return ___Position_2; }
	inline int32_t* get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(int32_t value)
	{
		___Position_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTAINERCONTEXT_T1305994539_H
#ifndef BSONVALUE_T3067495970_H
#define BSONVALUE_T3067495970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonValue
struct  BsonValue_t3067495970  : public BsonToken_t3838771636
{
public:
	// System.Object Newtonsoft.Json.Bson.BsonValue::_value
	RuntimeObject * ____value_2;
	// Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonValue::_type
	int8_t ____type_3;

public:
	inline static int32_t get_offset_of__value_2() { return static_cast<int32_t>(offsetof(BsonValue_t3067495970, ____value_2)); }
	inline RuntimeObject * get__value_2() const { return ____value_2; }
	inline RuntimeObject ** get_address_of__value_2() { return &____value_2; }
	inline void set__value_2(RuntimeObject * value)
	{
		____value_2 = value;
		Il2CppCodeGenWriteBarrier((&____value_2), value);
	}

	inline static int32_t get_offset_of__type_3() { return static_cast<int32_t>(offsetof(BsonValue_t3067495970, ____type_3)); }
	inline int8_t get__type_3() const { return ____type_3; }
	inline int8_t* get_address_of__type_3() { return &____type_3; }
	inline void set__type_3(int8_t value)
	{
		____type_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONVALUE_T3067495970_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef BSONSTRING_T1135579766_H
#define BSONSTRING_T1135579766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonString
struct  BsonString_t1135579766  : public BsonValue_t3067495970
{
public:
	// System.Int32 Newtonsoft.Json.Bson.BsonString::<ByteCount>k__BackingField
	int32_t ___U3CByteCountU3Ek__BackingField_4;
	// System.Boolean Newtonsoft.Json.Bson.BsonString::<IncludeLength>k__BackingField
	bool ___U3CIncludeLengthU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CByteCountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BsonString_t1135579766, ___U3CByteCountU3Ek__BackingField_4)); }
	inline int32_t get_U3CByteCountU3Ek__BackingField_4() const { return ___U3CByteCountU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CByteCountU3Ek__BackingField_4() { return &___U3CByteCountU3Ek__BackingField_4; }
	inline void set_U3CByteCountU3Ek__BackingField_4(int32_t value)
	{
		___U3CByteCountU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CIncludeLengthU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(BsonString_t1135579766, ___U3CIncludeLengthU3Ek__BackingField_5)); }
	inline bool get_U3CIncludeLengthU3Ek__BackingField_5() const { return ___U3CIncludeLengthU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIncludeLengthU3Ek__BackingField_5() { return &___U3CIncludeLengthU3Ek__BackingField_5; }
	inline void set_U3CIncludeLengthU3Ek__BackingField_5(bool value)
	{
		___U3CIncludeLengthU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONSTRING_T1135579766_H
#ifndef JSONREADER_T2369136700_H
#define JSONREADER_T2369136700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonReader
struct  JsonReader_t2369136700  : public RuntimeObject
{
public:
	// Newtonsoft.Json.JsonToken Newtonsoft.Json.JsonReader::_tokenType
	int32_t ____tokenType_0;
	// System.Object Newtonsoft.Json.JsonReader::_value
	RuntimeObject * ____value_1;
	// System.Char Newtonsoft.Json.JsonReader::_quoteChar
	Il2CppChar ____quoteChar_2;
	// Newtonsoft.Json.JsonReader/State Newtonsoft.Json.JsonReader::_currentState
	int32_t ____currentState_3;
	// Newtonsoft.Json.JsonPosition Newtonsoft.Json.JsonReader::_currentPosition
	JsonPosition_t2528027714  ____currentPosition_4;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonReader::_culture
	CultureInfo_t4157843068 * ____culture_5;
	// Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonReader::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_6;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonReader::_maxDepth
	Nullable_1_t378540539  ____maxDepth_7;
	// System.Boolean Newtonsoft.Json.JsonReader::_hasExceededMaxDepth
	bool ____hasExceededMaxDepth_8;
	// Newtonsoft.Json.DateParseHandling Newtonsoft.Json.JsonReader::_dateParseHandling
	int32_t ____dateParseHandling_9;
	// Newtonsoft.Json.FloatParseHandling Newtonsoft.Json.JsonReader::_floatParseHandling
	int32_t ____floatParseHandling_10;
	// System.String Newtonsoft.Json.JsonReader::_dateFormatString
	String_t* ____dateFormatString_11;
	// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition> Newtonsoft.Json.JsonReader::_stack
	List_1_t4000102456 * ____stack_12;
	// System.Boolean Newtonsoft.Json.JsonReader::<CloseInput>k__BackingField
	bool ___U3CCloseInputU3Ek__BackingField_13;
	// System.Boolean Newtonsoft.Json.JsonReader::<SupportMultipleContent>k__BackingField
	bool ___U3CSupportMultipleContentU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of__tokenType_0() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____tokenType_0)); }
	inline int32_t get__tokenType_0() const { return ____tokenType_0; }
	inline int32_t* get_address_of__tokenType_0() { return &____tokenType_0; }
	inline void set__tokenType_0(int32_t value)
	{
		____tokenType_0 = value;
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____value_1)); }
	inline RuntimeObject * get__value_1() const { return ____value_1; }
	inline RuntimeObject ** get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(RuntimeObject * value)
	{
		____value_1 = value;
		Il2CppCodeGenWriteBarrier((&____value_1), value);
	}

	inline static int32_t get_offset_of__quoteChar_2() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____quoteChar_2)); }
	inline Il2CppChar get__quoteChar_2() const { return ____quoteChar_2; }
	inline Il2CppChar* get_address_of__quoteChar_2() { return &____quoteChar_2; }
	inline void set__quoteChar_2(Il2CppChar value)
	{
		____quoteChar_2 = value;
	}

	inline static int32_t get_offset_of__currentState_3() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____currentState_3)); }
	inline int32_t get__currentState_3() const { return ____currentState_3; }
	inline int32_t* get_address_of__currentState_3() { return &____currentState_3; }
	inline void set__currentState_3(int32_t value)
	{
		____currentState_3 = value;
	}

	inline static int32_t get_offset_of__currentPosition_4() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____currentPosition_4)); }
	inline JsonPosition_t2528027714  get__currentPosition_4() const { return ____currentPosition_4; }
	inline JsonPosition_t2528027714 * get_address_of__currentPosition_4() { return &____currentPosition_4; }
	inline void set__currentPosition_4(JsonPosition_t2528027714  value)
	{
		____currentPosition_4 = value;
	}

	inline static int32_t get_offset_of__culture_5() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____culture_5)); }
	inline CultureInfo_t4157843068 * get__culture_5() const { return ____culture_5; }
	inline CultureInfo_t4157843068 ** get_address_of__culture_5() { return &____culture_5; }
	inline void set__culture_5(CultureInfo_t4157843068 * value)
	{
		____culture_5 = value;
		Il2CppCodeGenWriteBarrier((&____culture_5), value);
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_6() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____dateTimeZoneHandling_6)); }
	inline int32_t get__dateTimeZoneHandling_6() const { return ____dateTimeZoneHandling_6; }
	inline int32_t* get_address_of__dateTimeZoneHandling_6() { return &____dateTimeZoneHandling_6; }
	inline void set__dateTimeZoneHandling_6(int32_t value)
	{
		____dateTimeZoneHandling_6 = value;
	}

	inline static int32_t get_offset_of__maxDepth_7() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____maxDepth_7)); }
	inline Nullable_1_t378540539  get__maxDepth_7() const { return ____maxDepth_7; }
	inline Nullable_1_t378540539 * get_address_of__maxDepth_7() { return &____maxDepth_7; }
	inline void set__maxDepth_7(Nullable_1_t378540539  value)
	{
		____maxDepth_7 = value;
	}

	inline static int32_t get_offset_of__hasExceededMaxDepth_8() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____hasExceededMaxDepth_8)); }
	inline bool get__hasExceededMaxDepth_8() const { return ____hasExceededMaxDepth_8; }
	inline bool* get_address_of__hasExceededMaxDepth_8() { return &____hasExceededMaxDepth_8; }
	inline void set__hasExceededMaxDepth_8(bool value)
	{
		____hasExceededMaxDepth_8 = value;
	}

	inline static int32_t get_offset_of__dateParseHandling_9() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____dateParseHandling_9)); }
	inline int32_t get__dateParseHandling_9() const { return ____dateParseHandling_9; }
	inline int32_t* get_address_of__dateParseHandling_9() { return &____dateParseHandling_9; }
	inline void set__dateParseHandling_9(int32_t value)
	{
		____dateParseHandling_9 = value;
	}

	inline static int32_t get_offset_of__floatParseHandling_10() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____floatParseHandling_10)); }
	inline int32_t get__floatParseHandling_10() const { return ____floatParseHandling_10; }
	inline int32_t* get_address_of__floatParseHandling_10() { return &____floatParseHandling_10; }
	inline void set__floatParseHandling_10(int32_t value)
	{
		____floatParseHandling_10 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_11() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____dateFormatString_11)); }
	inline String_t* get__dateFormatString_11() const { return ____dateFormatString_11; }
	inline String_t** get_address_of__dateFormatString_11() { return &____dateFormatString_11; }
	inline void set__dateFormatString_11(String_t* value)
	{
		____dateFormatString_11 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_11), value);
	}

	inline static int32_t get_offset_of__stack_12() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ____stack_12)); }
	inline List_1_t4000102456 * get__stack_12() const { return ____stack_12; }
	inline List_1_t4000102456 ** get_address_of__stack_12() { return &____stack_12; }
	inline void set__stack_12(List_1_t4000102456 * value)
	{
		____stack_12 = value;
		Il2CppCodeGenWriteBarrier((&____stack_12), value);
	}

	inline static int32_t get_offset_of_U3CCloseInputU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ___U3CCloseInputU3Ek__BackingField_13)); }
	inline bool get_U3CCloseInputU3Ek__BackingField_13() const { return ___U3CCloseInputU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CCloseInputU3Ek__BackingField_13() { return &___U3CCloseInputU3Ek__BackingField_13; }
	inline void set_U3CCloseInputU3Ek__BackingField_13(bool value)
	{
		___U3CCloseInputU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CSupportMultipleContentU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(JsonReader_t2369136700, ___U3CSupportMultipleContentU3Ek__BackingField_14)); }
	inline bool get_U3CSupportMultipleContentU3Ek__BackingField_14() const { return ___U3CSupportMultipleContentU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CSupportMultipleContentU3Ek__BackingField_14() { return &___U3CSupportMultipleContentU3Ek__BackingField_14; }
	inline void set_U3CSupportMultipleContentU3Ek__BackingField_14(bool value)
	{
		___U3CSupportMultipleContentU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONREADER_T2369136700_H
#ifndef ONTRIGGER_T4184125570_H
#define ONTRIGGER_T4184125570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger/OnTrigger
struct  OnTrigger_t4184125570  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTRIGGER_T4184125570_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef HIGHLIGHTER_T666689529_H
#define HIGHLIGHTER_T666689529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WrldHelper/Highlighter
struct  Highlighter_t666689529  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIGHLIGHTER_T666689529_H
#ifndef BSONBINARY_T160198617_H
#define BSONBINARY_T160198617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonBinary
struct  BsonBinary_t160198617  : public BsonValue_t3067495970
{
public:
	// Newtonsoft.Json.Bson.BsonBinaryType Newtonsoft.Json.Bson.BsonBinary::<BinaryType>k__BackingField
	uint8_t ___U3CBinaryTypeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CBinaryTypeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BsonBinary_t160198617, ___U3CBinaryTypeU3Ek__BackingField_4)); }
	inline uint8_t get_U3CBinaryTypeU3Ek__BackingField_4() const { return ___U3CBinaryTypeU3Ek__BackingField_4; }
	inline uint8_t* get_address_of_U3CBinaryTypeU3Ek__BackingField_4() { return &___U3CBinaryTypeU3Ek__BackingField_4; }
	inline void set_U3CBinaryTypeU3Ek__BackingField_4(uint8_t value)
	{
		___U3CBinaryTypeU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONBINARY_T160198617_H
#ifndef JSONWRITER_T1467272295_H
#define JSONWRITER_T1467272295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter
struct  JsonWriter_t1467272295  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition> Newtonsoft.Json.JsonWriter::_stack
	List_1_t4000102456 * ____stack_2;
	// Newtonsoft.Json.JsonPosition Newtonsoft.Json.JsonWriter::_currentPosition
	JsonPosition_t2528027714  ____currentPosition_3;
	// Newtonsoft.Json.JsonWriter/State Newtonsoft.Json.JsonWriter::_currentState
	int32_t ____currentState_4;
	// Newtonsoft.Json.Formatting Newtonsoft.Json.JsonWriter::_formatting
	int32_t ____formatting_5;
	// System.Boolean Newtonsoft.Json.JsonWriter::<CloseOutput>k__BackingField
	bool ___U3CCloseOutputU3Ek__BackingField_6;
	// Newtonsoft.Json.DateFormatHandling Newtonsoft.Json.JsonWriter::_dateFormatHandling
	int32_t ____dateFormatHandling_7;
	// Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonWriter::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_8;
	// Newtonsoft.Json.StringEscapeHandling Newtonsoft.Json.JsonWriter::_stringEscapeHandling
	int32_t ____stringEscapeHandling_9;
	// Newtonsoft.Json.FloatFormatHandling Newtonsoft.Json.JsonWriter::_floatFormatHandling
	int32_t ____floatFormatHandling_10;
	// System.String Newtonsoft.Json.JsonWriter::_dateFormatString
	String_t* ____dateFormatString_11;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonWriter::_culture
	CultureInfo_t4157843068 * ____culture_12;

public:
	inline static int32_t get_offset_of__stack_2() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____stack_2)); }
	inline List_1_t4000102456 * get__stack_2() const { return ____stack_2; }
	inline List_1_t4000102456 ** get_address_of__stack_2() { return &____stack_2; }
	inline void set__stack_2(List_1_t4000102456 * value)
	{
		____stack_2 = value;
		Il2CppCodeGenWriteBarrier((&____stack_2), value);
	}

	inline static int32_t get_offset_of__currentPosition_3() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____currentPosition_3)); }
	inline JsonPosition_t2528027714  get__currentPosition_3() const { return ____currentPosition_3; }
	inline JsonPosition_t2528027714 * get_address_of__currentPosition_3() { return &____currentPosition_3; }
	inline void set__currentPosition_3(JsonPosition_t2528027714  value)
	{
		____currentPosition_3 = value;
	}

	inline static int32_t get_offset_of__currentState_4() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____currentState_4)); }
	inline int32_t get__currentState_4() const { return ____currentState_4; }
	inline int32_t* get_address_of__currentState_4() { return &____currentState_4; }
	inline void set__currentState_4(int32_t value)
	{
		____currentState_4 = value;
	}

	inline static int32_t get_offset_of__formatting_5() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____formatting_5)); }
	inline int32_t get__formatting_5() const { return ____formatting_5; }
	inline int32_t* get_address_of__formatting_5() { return &____formatting_5; }
	inline void set__formatting_5(int32_t value)
	{
		____formatting_5 = value;
	}

	inline static int32_t get_offset_of_U3CCloseOutputU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ___U3CCloseOutputU3Ek__BackingField_6)); }
	inline bool get_U3CCloseOutputU3Ek__BackingField_6() const { return ___U3CCloseOutputU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CCloseOutputU3Ek__BackingField_6() { return &___U3CCloseOutputU3Ek__BackingField_6; }
	inline void set_U3CCloseOutputU3Ek__BackingField_6(bool value)
	{
		___U3CCloseOutputU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_7() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____dateFormatHandling_7)); }
	inline int32_t get__dateFormatHandling_7() const { return ____dateFormatHandling_7; }
	inline int32_t* get_address_of__dateFormatHandling_7() { return &____dateFormatHandling_7; }
	inline void set__dateFormatHandling_7(int32_t value)
	{
		____dateFormatHandling_7 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_8() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____dateTimeZoneHandling_8)); }
	inline int32_t get__dateTimeZoneHandling_8() const { return ____dateTimeZoneHandling_8; }
	inline int32_t* get_address_of__dateTimeZoneHandling_8() { return &____dateTimeZoneHandling_8; }
	inline void set__dateTimeZoneHandling_8(int32_t value)
	{
		____dateTimeZoneHandling_8 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_9() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____stringEscapeHandling_9)); }
	inline int32_t get__stringEscapeHandling_9() const { return ____stringEscapeHandling_9; }
	inline int32_t* get_address_of__stringEscapeHandling_9() { return &____stringEscapeHandling_9; }
	inline void set__stringEscapeHandling_9(int32_t value)
	{
		____stringEscapeHandling_9 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_10() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____floatFormatHandling_10)); }
	inline int32_t get__floatFormatHandling_10() const { return ____floatFormatHandling_10; }
	inline int32_t* get_address_of__floatFormatHandling_10() { return &____floatFormatHandling_10; }
	inline void set__floatFormatHandling_10(int32_t value)
	{
		____floatFormatHandling_10 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_11() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____dateFormatString_11)); }
	inline String_t* get__dateFormatString_11() const { return ____dateFormatString_11; }
	inline String_t** get_address_of__dateFormatString_11() { return &____dateFormatString_11; }
	inline void set__dateFormatString_11(String_t* value)
	{
		____dateFormatString_11 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_11), value);
	}

	inline static int32_t get_offset_of__culture_12() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295, ____culture_12)); }
	inline CultureInfo_t4157843068 * get__culture_12() const { return ____culture_12; }
	inline CultureInfo_t4157843068 ** get_address_of__culture_12() { return &____culture_12; }
	inline void set__culture_12(CultureInfo_t4157843068 * value)
	{
		____culture_12 = value;
		Il2CppCodeGenWriteBarrier((&____culture_12), value);
	}
};

struct JsonWriter_t1467272295_StaticFields
{
public:
	// Newtonsoft.Json.JsonWriter/State[][] Newtonsoft.Json.JsonWriter::StateArray
	StateU5BU5DU5BU5D_t3190318925* ___StateArray_0;
	// Newtonsoft.Json.JsonWriter/State[][] Newtonsoft.Json.JsonWriter::StateArrayTempate
	StateU5BU5DU5BU5D_t3190318925* ___StateArrayTempate_1;

public:
	inline static int32_t get_offset_of_StateArray_0() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295_StaticFields, ___StateArray_0)); }
	inline StateU5BU5DU5BU5D_t3190318925* get_StateArray_0() const { return ___StateArray_0; }
	inline StateU5BU5DU5BU5D_t3190318925** get_address_of_StateArray_0() { return &___StateArray_0; }
	inline void set_StateArray_0(StateU5BU5DU5BU5D_t3190318925* value)
	{
		___StateArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___StateArray_0), value);
	}

	inline static int32_t get_offset_of_StateArrayTempate_1() { return static_cast<int32_t>(offsetof(JsonWriter_t1467272295_StaticFields, ___StateArrayTempate_1)); }
	inline StateU5BU5DU5BU5D_t3190318925* get_StateArrayTempate_1() const { return ___StateArrayTempate_1; }
	inline StateU5BU5DU5BU5D_t3190318925** get_address_of_StateArrayTempate_1() { return &___StateArrayTempate_1; }
	inline void set_StateArrayTempate_1(StateU5BU5DU5BU5D_t3190318925* value)
	{
		___StateArrayTempate_1 = value;
		Il2CppCodeGenWriteBarrier((&___StateArrayTempate_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONWRITER_T1467272295_H
#ifndef BSONWRITER_T3355227794_H
#define BSONWRITER_T3355227794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonWriter
struct  BsonWriter_t3355227794  : public JsonWriter_t1467272295
{
public:
	// Newtonsoft.Json.Bson.BsonBinaryWriter Newtonsoft.Json.Bson.BsonWriter::_writer
	BsonBinaryWriter_t671405346 * ____writer_13;
	// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonWriter::_root
	BsonToken_t3838771636 * ____root_14;
	// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonWriter::_parent
	BsonToken_t3838771636 * ____parent_15;
	// System.String Newtonsoft.Json.Bson.BsonWriter::_propertyName
	String_t* ____propertyName_16;

public:
	inline static int32_t get_offset_of__writer_13() { return static_cast<int32_t>(offsetof(BsonWriter_t3355227794, ____writer_13)); }
	inline BsonBinaryWriter_t671405346 * get__writer_13() const { return ____writer_13; }
	inline BsonBinaryWriter_t671405346 ** get_address_of__writer_13() { return &____writer_13; }
	inline void set__writer_13(BsonBinaryWriter_t671405346 * value)
	{
		____writer_13 = value;
		Il2CppCodeGenWriteBarrier((&____writer_13), value);
	}

	inline static int32_t get_offset_of__root_14() { return static_cast<int32_t>(offsetof(BsonWriter_t3355227794, ____root_14)); }
	inline BsonToken_t3838771636 * get__root_14() const { return ____root_14; }
	inline BsonToken_t3838771636 ** get_address_of__root_14() { return &____root_14; }
	inline void set__root_14(BsonToken_t3838771636 * value)
	{
		____root_14 = value;
		Il2CppCodeGenWriteBarrier((&____root_14), value);
	}

	inline static int32_t get_offset_of__parent_15() { return static_cast<int32_t>(offsetof(BsonWriter_t3355227794, ____parent_15)); }
	inline BsonToken_t3838771636 * get__parent_15() const { return ____parent_15; }
	inline BsonToken_t3838771636 ** get_address_of__parent_15() { return &____parent_15; }
	inline void set__parent_15(BsonToken_t3838771636 * value)
	{
		____parent_15 = value;
		Il2CppCodeGenWriteBarrier((&____parent_15), value);
	}

	inline static int32_t get_offset_of__propertyName_16() { return static_cast<int32_t>(offsetof(BsonWriter_t3355227794, ____propertyName_16)); }
	inline String_t* get__propertyName_16() const { return ____propertyName_16; }
	inline String_t** get_address_of__propertyName_16() { return &____propertyName_16; }
	inline void set__propertyName_16(String_t* value)
	{
		____propertyName_16 = value;
		Il2CppCodeGenWriteBarrier((&____propertyName_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONWRITER_T3355227794_H
#ifndef BSONREADER_T26184191_H
#define BSONREADER_T26184191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Bson.BsonReader
struct  BsonReader_t26184191  : public JsonReader_t2369136700
{
public:
	// System.IO.BinaryReader Newtonsoft.Json.Bson.BsonReader::_reader
	BinaryReader_t2428077293 * ____reader_19;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Bson.BsonReader/ContainerContext> Newtonsoft.Json.Bson.BsonReader::_stack
	List_1_t2778069281 * ____stack_20;
	// System.Byte[] Newtonsoft.Json.Bson.BsonReader::_byteBuffer
	ByteU5BU5D_t4116647657* ____byteBuffer_21;
	// System.Char[] Newtonsoft.Json.Bson.BsonReader::_charBuffer
	CharU5BU5D_t3528271667* ____charBuffer_22;
	// Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonReader::_currentElementType
	int8_t ____currentElementType_23;
	// Newtonsoft.Json.Bson.BsonReader/BsonReaderState Newtonsoft.Json.Bson.BsonReader::_bsonReaderState
	int32_t ____bsonReaderState_24;
	// Newtonsoft.Json.Bson.BsonReader/ContainerContext Newtonsoft.Json.Bson.BsonReader::_currentContext
	ContainerContext_t1305994539 * ____currentContext_25;
	// System.Boolean Newtonsoft.Json.Bson.BsonReader::_readRootValueAsArray
	bool ____readRootValueAsArray_26;
	// System.Boolean Newtonsoft.Json.Bson.BsonReader::_jsonNet35BinaryCompatibility
	bool ____jsonNet35BinaryCompatibility_27;
	// System.DateTimeKind Newtonsoft.Json.Bson.BsonReader::_dateTimeKindHandling
	int32_t ____dateTimeKindHandling_28;

public:
	inline static int32_t get_offset_of__reader_19() { return static_cast<int32_t>(offsetof(BsonReader_t26184191, ____reader_19)); }
	inline BinaryReader_t2428077293 * get__reader_19() const { return ____reader_19; }
	inline BinaryReader_t2428077293 ** get_address_of__reader_19() { return &____reader_19; }
	inline void set__reader_19(BinaryReader_t2428077293 * value)
	{
		____reader_19 = value;
		Il2CppCodeGenWriteBarrier((&____reader_19), value);
	}

	inline static int32_t get_offset_of__stack_20() { return static_cast<int32_t>(offsetof(BsonReader_t26184191, ____stack_20)); }
	inline List_1_t2778069281 * get__stack_20() const { return ____stack_20; }
	inline List_1_t2778069281 ** get_address_of__stack_20() { return &____stack_20; }
	inline void set__stack_20(List_1_t2778069281 * value)
	{
		____stack_20 = value;
		Il2CppCodeGenWriteBarrier((&____stack_20), value);
	}

	inline static int32_t get_offset_of__byteBuffer_21() { return static_cast<int32_t>(offsetof(BsonReader_t26184191, ____byteBuffer_21)); }
	inline ByteU5BU5D_t4116647657* get__byteBuffer_21() const { return ____byteBuffer_21; }
	inline ByteU5BU5D_t4116647657** get_address_of__byteBuffer_21() { return &____byteBuffer_21; }
	inline void set__byteBuffer_21(ByteU5BU5D_t4116647657* value)
	{
		____byteBuffer_21 = value;
		Il2CppCodeGenWriteBarrier((&____byteBuffer_21), value);
	}

	inline static int32_t get_offset_of__charBuffer_22() { return static_cast<int32_t>(offsetof(BsonReader_t26184191, ____charBuffer_22)); }
	inline CharU5BU5D_t3528271667* get__charBuffer_22() const { return ____charBuffer_22; }
	inline CharU5BU5D_t3528271667** get_address_of__charBuffer_22() { return &____charBuffer_22; }
	inline void set__charBuffer_22(CharU5BU5D_t3528271667* value)
	{
		____charBuffer_22 = value;
		Il2CppCodeGenWriteBarrier((&____charBuffer_22), value);
	}

	inline static int32_t get_offset_of__currentElementType_23() { return static_cast<int32_t>(offsetof(BsonReader_t26184191, ____currentElementType_23)); }
	inline int8_t get__currentElementType_23() const { return ____currentElementType_23; }
	inline int8_t* get_address_of__currentElementType_23() { return &____currentElementType_23; }
	inline void set__currentElementType_23(int8_t value)
	{
		____currentElementType_23 = value;
	}

	inline static int32_t get_offset_of__bsonReaderState_24() { return static_cast<int32_t>(offsetof(BsonReader_t26184191, ____bsonReaderState_24)); }
	inline int32_t get__bsonReaderState_24() const { return ____bsonReaderState_24; }
	inline int32_t* get_address_of__bsonReaderState_24() { return &____bsonReaderState_24; }
	inline void set__bsonReaderState_24(int32_t value)
	{
		____bsonReaderState_24 = value;
	}

	inline static int32_t get_offset_of__currentContext_25() { return static_cast<int32_t>(offsetof(BsonReader_t26184191, ____currentContext_25)); }
	inline ContainerContext_t1305994539 * get__currentContext_25() const { return ____currentContext_25; }
	inline ContainerContext_t1305994539 ** get_address_of__currentContext_25() { return &____currentContext_25; }
	inline void set__currentContext_25(ContainerContext_t1305994539 * value)
	{
		____currentContext_25 = value;
		Il2CppCodeGenWriteBarrier((&____currentContext_25), value);
	}

	inline static int32_t get_offset_of__readRootValueAsArray_26() { return static_cast<int32_t>(offsetof(BsonReader_t26184191, ____readRootValueAsArray_26)); }
	inline bool get__readRootValueAsArray_26() const { return ____readRootValueAsArray_26; }
	inline bool* get_address_of__readRootValueAsArray_26() { return &____readRootValueAsArray_26; }
	inline void set__readRootValueAsArray_26(bool value)
	{
		____readRootValueAsArray_26 = value;
	}

	inline static int32_t get_offset_of__jsonNet35BinaryCompatibility_27() { return static_cast<int32_t>(offsetof(BsonReader_t26184191, ____jsonNet35BinaryCompatibility_27)); }
	inline bool get__jsonNet35BinaryCompatibility_27() const { return ____jsonNet35BinaryCompatibility_27; }
	inline bool* get_address_of__jsonNet35BinaryCompatibility_27() { return &____jsonNet35BinaryCompatibility_27; }
	inline void set__jsonNet35BinaryCompatibility_27(bool value)
	{
		____jsonNet35BinaryCompatibility_27 = value;
	}

	inline static int32_t get_offset_of__dateTimeKindHandling_28() { return static_cast<int32_t>(offsetof(BsonReader_t26184191, ____dateTimeKindHandling_28)); }
	inline int32_t get__dateTimeKindHandling_28() const { return ____dateTimeKindHandling_28; }
	inline int32_t* get_address_of__dateTimeKindHandling_28() { return &____dateTimeKindHandling_28; }
	inline void set__dateTimeKindHandling_28(int32_t value)
	{
		____dateTimeKindHandling_28 = value;
	}
};

struct BsonReader_t26184191_StaticFields
{
public:
	// System.Byte[] Newtonsoft.Json.Bson.BsonReader::SeqRange1
	ByteU5BU5D_t4116647657* ___SeqRange1_15;
	// System.Byte[] Newtonsoft.Json.Bson.BsonReader::SeqRange2
	ByteU5BU5D_t4116647657* ___SeqRange2_16;
	// System.Byte[] Newtonsoft.Json.Bson.BsonReader::SeqRange3
	ByteU5BU5D_t4116647657* ___SeqRange3_17;
	// System.Byte[] Newtonsoft.Json.Bson.BsonReader::SeqRange4
	ByteU5BU5D_t4116647657* ___SeqRange4_18;

public:
	inline static int32_t get_offset_of_SeqRange1_15() { return static_cast<int32_t>(offsetof(BsonReader_t26184191_StaticFields, ___SeqRange1_15)); }
	inline ByteU5BU5D_t4116647657* get_SeqRange1_15() const { return ___SeqRange1_15; }
	inline ByteU5BU5D_t4116647657** get_address_of_SeqRange1_15() { return &___SeqRange1_15; }
	inline void set_SeqRange1_15(ByteU5BU5D_t4116647657* value)
	{
		___SeqRange1_15 = value;
		Il2CppCodeGenWriteBarrier((&___SeqRange1_15), value);
	}

	inline static int32_t get_offset_of_SeqRange2_16() { return static_cast<int32_t>(offsetof(BsonReader_t26184191_StaticFields, ___SeqRange2_16)); }
	inline ByteU5BU5D_t4116647657* get_SeqRange2_16() const { return ___SeqRange2_16; }
	inline ByteU5BU5D_t4116647657** get_address_of_SeqRange2_16() { return &___SeqRange2_16; }
	inline void set_SeqRange2_16(ByteU5BU5D_t4116647657* value)
	{
		___SeqRange2_16 = value;
		Il2CppCodeGenWriteBarrier((&___SeqRange2_16), value);
	}

	inline static int32_t get_offset_of_SeqRange3_17() { return static_cast<int32_t>(offsetof(BsonReader_t26184191_StaticFields, ___SeqRange3_17)); }
	inline ByteU5BU5D_t4116647657* get_SeqRange3_17() const { return ___SeqRange3_17; }
	inline ByteU5BU5D_t4116647657** get_address_of_SeqRange3_17() { return &___SeqRange3_17; }
	inline void set_SeqRange3_17(ByteU5BU5D_t4116647657* value)
	{
		___SeqRange3_17 = value;
		Il2CppCodeGenWriteBarrier((&___SeqRange3_17), value);
	}

	inline static int32_t get_offset_of_SeqRange4_18() { return static_cast<int32_t>(offsetof(BsonReader_t26184191_StaticFields, ___SeqRange4_18)); }
	inline ByteU5BU5D_t4116647657* get_SeqRange4_18() const { return ___SeqRange4_18; }
	inline ByteU5BU5D_t4116647657** get_address_of_SeqRange4_18() { return &___SeqRange4_18; }
	inline void set_SeqRange4_18(ByteU5BU5D_t4116647657* value)
	{
		___SeqRange4_18 = value;
		Il2CppCodeGenWriteBarrier((&___SeqRange4_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONREADER_T26184191_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef FACECUSTOMIZER_T3426199831_H
#define FACECUSTOMIZER_T3426199831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FaceCustomizer
struct  FaceCustomizer_t3426199831  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject FaceCustomizer::goUI_Customizable
	GameObject_t1113636619 * ___goUI_Customizable_2;
	// UnityEngine.Transform FaceCustomizer::tSpriteButton_PartContainer
	Transform_t3600365921 * ___tSpriteButton_PartContainer_3;
	// UnityEngine.GameObject FaceCustomizer::goPrefabButton
	GameObject_t1113636619 * ___goPrefabButton_4;
	// System.Single FaceCustomizer::buttonWidth
	float ___buttonWidth_5;
	// UnityEngine.Transform FaceCustomizer::tStartLoad
	Transform_t3600365921 * ___tStartLoad_7;
	// UnityEngine.Transform FaceCustomizer::tSpriteButton_ColorContainer
	Transform_t3600365921 * ___tSpriteButton_ColorContainer_8;
	// UnityEngine.GameObject FaceCustomizer::goPrefabButtonColor
	GameObject_t1113636619 * ___goPrefabButtonColor_9;

public:
	inline static int32_t get_offset_of_goUI_Customizable_2() { return static_cast<int32_t>(offsetof(FaceCustomizer_t3426199831, ___goUI_Customizable_2)); }
	inline GameObject_t1113636619 * get_goUI_Customizable_2() const { return ___goUI_Customizable_2; }
	inline GameObject_t1113636619 ** get_address_of_goUI_Customizable_2() { return &___goUI_Customizable_2; }
	inline void set_goUI_Customizable_2(GameObject_t1113636619 * value)
	{
		___goUI_Customizable_2 = value;
		Il2CppCodeGenWriteBarrier((&___goUI_Customizable_2), value);
	}

	inline static int32_t get_offset_of_tSpriteButton_PartContainer_3() { return static_cast<int32_t>(offsetof(FaceCustomizer_t3426199831, ___tSpriteButton_PartContainer_3)); }
	inline Transform_t3600365921 * get_tSpriteButton_PartContainer_3() const { return ___tSpriteButton_PartContainer_3; }
	inline Transform_t3600365921 ** get_address_of_tSpriteButton_PartContainer_3() { return &___tSpriteButton_PartContainer_3; }
	inline void set_tSpriteButton_PartContainer_3(Transform_t3600365921 * value)
	{
		___tSpriteButton_PartContainer_3 = value;
		Il2CppCodeGenWriteBarrier((&___tSpriteButton_PartContainer_3), value);
	}

	inline static int32_t get_offset_of_goPrefabButton_4() { return static_cast<int32_t>(offsetof(FaceCustomizer_t3426199831, ___goPrefabButton_4)); }
	inline GameObject_t1113636619 * get_goPrefabButton_4() const { return ___goPrefabButton_4; }
	inline GameObject_t1113636619 ** get_address_of_goPrefabButton_4() { return &___goPrefabButton_4; }
	inline void set_goPrefabButton_4(GameObject_t1113636619 * value)
	{
		___goPrefabButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___goPrefabButton_4), value);
	}

	inline static int32_t get_offset_of_buttonWidth_5() { return static_cast<int32_t>(offsetof(FaceCustomizer_t3426199831, ___buttonWidth_5)); }
	inline float get_buttonWidth_5() const { return ___buttonWidth_5; }
	inline float* get_address_of_buttonWidth_5() { return &___buttonWidth_5; }
	inline void set_buttonWidth_5(float value)
	{
		___buttonWidth_5 = value;
	}

	inline static int32_t get_offset_of_tStartLoad_7() { return static_cast<int32_t>(offsetof(FaceCustomizer_t3426199831, ___tStartLoad_7)); }
	inline Transform_t3600365921 * get_tStartLoad_7() const { return ___tStartLoad_7; }
	inline Transform_t3600365921 ** get_address_of_tStartLoad_7() { return &___tStartLoad_7; }
	inline void set_tStartLoad_7(Transform_t3600365921 * value)
	{
		___tStartLoad_7 = value;
		Il2CppCodeGenWriteBarrier((&___tStartLoad_7), value);
	}

	inline static int32_t get_offset_of_tSpriteButton_ColorContainer_8() { return static_cast<int32_t>(offsetof(FaceCustomizer_t3426199831, ___tSpriteButton_ColorContainer_8)); }
	inline Transform_t3600365921 * get_tSpriteButton_ColorContainer_8() const { return ___tSpriteButton_ColorContainer_8; }
	inline Transform_t3600365921 ** get_address_of_tSpriteButton_ColorContainer_8() { return &___tSpriteButton_ColorContainer_8; }
	inline void set_tSpriteButton_ColorContainer_8(Transform_t3600365921 * value)
	{
		___tSpriteButton_ColorContainer_8 = value;
		Il2CppCodeGenWriteBarrier((&___tSpriteButton_ColorContainer_8), value);
	}

	inline static int32_t get_offset_of_goPrefabButtonColor_9() { return static_cast<int32_t>(offsetof(FaceCustomizer_t3426199831, ___goPrefabButtonColor_9)); }
	inline GameObject_t1113636619 * get_goPrefabButtonColor_9() const { return ___goPrefabButtonColor_9; }
	inline GameObject_t1113636619 ** get_address_of_goPrefabButtonColor_9() { return &___goPrefabButtonColor_9; }
	inline void set_goPrefabButtonColor_9(GameObject_t1113636619 * value)
	{
		___goPrefabButtonColor_9 = value;
		Il2CppCodeGenWriteBarrier((&___goPrefabButtonColor_9), value);
	}
};

struct FaceCustomizer_t3426199831_StaticFields
{
public:
	// UnityEngine.Transform FaceCustomizer::currentlyEditing
	Transform_t3600365921 * ___currentlyEditing_6;

public:
	inline static int32_t get_offset_of_currentlyEditing_6() { return static_cast<int32_t>(offsetof(FaceCustomizer_t3426199831_StaticFields, ___currentlyEditing_6)); }
	inline Transform_t3600365921 * get_currentlyEditing_6() const { return ___currentlyEditing_6; }
	inline Transform_t3600365921 ** get_address_of_currentlyEditing_6() { return &___currentlyEditing_6; }
	inline void set_currentlyEditing_6(Transform_t3600365921 * value)
	{
		___currentlyEditing_6 = value;
		Il2CppCodeGenWriteBarrier((&___currentlyEditing_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACECUSTOMIZER_T3426199831_H
#ifndef ANALYTICSTRACKER_T731021378_H
#define ANALYTICSTRACKER_T731021378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsTracker
struct  AnalyticsTracker_t731021378  : public MonoBehaviour_t3962482529
{
public:
	// System.String UnityEngine.Analytics.AnalyticsTracker::m_EventName
	String_t* ___m_EventName_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Analytics.AnalyticsTracker::m_Dict
	Dictionary_2_t2865362463 * ___m_Dict_3;
	// System.Int32 UnityEngine.Analytics.AnalyticsTracker::m_PrevDictHash
	int32_t ___m_PrevDictHash_4;
	// UnityEngine.Analytics.TrackableProperty UnityEngine.Analytics.AnalyticsTracker::m_TrackableProperty
	TrackableProperty_t3943537984 * ___m_TrackableProperty_5;
	// UnityEngine.Analytics.AnalyticsTracker/Trigger UnityEngine.Analytics.AnalyticsTracker::m_Trigger
	int32_t ___m_Trigger_6;

public:
	inline static int32_t get_offset_of_m_EventName_2() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_EventName_2)); }
	inline String_t* get_m_EventName_2() const { return ___m_EventName_2; }
	inline String_t** get_address_of_m_EventName_2() { return &___m_EventName_2; }
	inline void set_m_EventName_2(String_t* value)
	{
		___m_EventName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventName_2), value);
	}

	inline static int32_t get_offset_of_m_Dict_3() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_Dict_3)); }
	inline Dictionary_2_t2865362463 * get_m_Dict_3() const { return ___m_Dict_3; }
	inline Dictionary_2_t2865362463 ** get_address_of_m_Dict_3() { return &___m_Dict_3; }
	inline void set_m_Dict_3(Dictionary_2_t2865362463 * value)
	{
		___m_Dict_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dict_3), value);
	}

	inline static int32_t get_offset_of_m_PrevDictHash_4() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_PrevDictHash_4)); }
	inline int32_t get_m_PrevDictHash_4() const { return ___m_PrevDictHash_4; }
	inline int32_t* get_address_of_m_PrevDictHash_4() { return &___m_PrevDictHash_4; }
	inline void set_m_PrevDictHash_4(int32_t value)
	{
		___m_PrevDictHash_4 = value;
	}

	inline static int32_t get_offset_of_m_TrackableProperty_5() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_TrackableProperty_5)); }
	inline TrackableProperty_t3943537984 * get_m_TrackableProperty_5() const { return ___m_TrackableProperty_5; }
	inline TrackableProperty_t3943537984 ** get_address_of_m_TrackableProperty_5() { return &___m_TrackableProperty_5; }
	inline void set_m_TrackableProperty_5(TrackableProperty_t3943537984 * value)
	{
		___m_TrackableProperty_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackableProperty_5), value);
	}

	inline static int32_t get_offset_of_m_Trigger_6() { return static_cast<int32_t>(offsetof(AnalyticsTracker_t731021378, ___m_Trigger_6)); }
	inline int32_t get_m_Trigger_6() const { return ___m_Trigger_6; }
	inline int32_t* get_address_of_m_Trigger_6() { return &___m_Trigger_6; }
	inline void set_m_Trigger_6(int32_t value)
	{
		___m_Trigger_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSTRACKER_T731021378_H
#ifndef ANALYTICSEVENTTRACKER_T2285229262_H
#define ANALYTICSEVENTTRACKER_T2285229262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsEventTracker
struct  AnalyticsEventTracker_t2285229262  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Analytics.EventTrigger UnityEngine.Analytics.AnalyticsEventTracker::m_Trigger
	EventTrigger_t2527451695 * ___m_Trigger_2;
	// UnityEngine.Analytics.StandardEventPayload UnityEngine.Analytics.AnalyticsEventTracker::m_EventPayload
	StandardEventPayload_t1629891255 * ___m_EventPayload_3;

public:
	inline static int32_t get_offset_of_m_Trigger_2() { return static_cast<int32_t>(offsetof(AnalyticsEventTracker_t2285229262, ___m_Trigger_2)); }
	inline EventTrigger_t2527451695 * get_m_Trigger_2() const { return ___m_Trigger_2; }
	inline EventTrigger_t2527451695 ** get_address_of_m_Trigger_2() { return &___m_Trigger_2; }
	inline void set_m_Trigger_2(EventTrigger_t2527451695 * value)
	{
		___m_Trigger_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Trigger_2), value);
	}

	inline static int32_t get_offset_of_m_EventPayload_3() { return static_cast<int32_t>(offsetof(AnalyticsEventTracker_t2285229262, ___m_EventPayload_3)); }
	inline StandardEventPayload_t1629891255 * get_m_EventPayload_3() const { return ___m_EventPayload_3; }
	inline StandardEventPayload_t1629891255 ** get_address_of_m_EventPayload_3() { return &___m_EventPayload_3; }
	inline void set_m_EventPayload_3(StandardEventPayload_t1629891255 * value)
	{
		___m_EventPayload_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventPayload_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSEVENTTRACKER_T2285229262_H
#ifndef POLYCONTROLLER_T1840278131_H
#define POLYCONTROLLER_T1840278131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolyController
struct  PolyController_t1840278131  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField PolyController::search
	InputField_t3762917431 * ___search_2;
	// UnityEngine.RectTransform PolyController::rt_Settings
	RectTransform_t3704657025 * ___rt_Settings_4;
	// RT PolyController::rtPoly
	RT_t167452472 * ___rtPoly_5;
	// RT PolyController::rtGeo
	RT_t167452472 * ___rtGeo_6;
	// UnityEngine.GameObject[] PolyController::goOnlyInPolyMode
	GameObjectU5BU5D_t3328599146* ___goOnlyInPolyMode_7;
	// UnityEngine.GameObject[] PolyController::goOnlyInGeoMode
	GameObjectU5BU5D_t3328599146* ___goOnlyInGeoMode_8;
	// UnityEngine.Transform PolyController::tScrollContent
	Transform_t3600365921 * ___tScrollContent_9;
	// UnityEngine.GameObject PolyController::goScrollPrefabButton
	GameObject_t1113636619 * ___goScrollPrefabButton_10;

public:
	inline static int32_t get_offset_of_search_2() { return static_cast<int32_t>(offsetof(PolyController_t1840278131, ___search_2)); }
	inline InputField_t3762917431 * get_search_2() const { return ___search_2; }
	inline InputField_t3762917431 ** get_address_of_search_2() { return &___search_2; }
	inline void set_search_2(InputField_t3762917431 * value)
	{
		___search_2 = value;
		Il2CppCodeGenWriteBarrier((&___search_2), value);
	}

	inline static int32_t get_offset_of_rt_Settings_4() { return static_cast<int32_t>(offsetof(PolyController_t1840278131, ___rt_Settings_4)); }
	inline RectTransform_t3704657025 * get_rt_Settings_4() const { return ___rt_Settings_4; }
	inline RectTransform_t3704657025 ** get_address_of_rt_Settings_4() { return &___rt_Settings_4; }
	inline void set_rt_Settings_4(RectTransform_t3704657025 * value)
	{
		___rt_Settings_4 = value;
		Il2CppCodeGenWriteBarrier((&___rt_Settings_4), value);
	}

	inline static int32_t get_offset_of_rtPoly_5() { return static_cast<int32_t>(offsetof(PolyController_t1840278131, ___rtPoly_5)); }
	inline RT_t167452472 * get_rtPoly_5() const { return ___rtPoly_5; }
	inline RT_t167452472 ** get_address_of_rtPoly_5() { return &___rtPoly_5; }
	inline void set_rtPoly_5(RT_t167452472 * value)
	{
		___rtPoly_5 = value;
		Il2CppCodeGenWriteBarrier((&___rtPoly_5), value);
	}

	inline static int32_t get_offset_of_rtGeo_6() { return static_cast<int32_t>(offsetof(PolyController_t1840278131, ___rtGeo_6)); }
	inline RT_t167452472 * get_rtGeo_6() const { return ___rtGeo_6; }
	inline RT_t167452472 ** get_address_of_rtGeo_6() { return &___rtGeo_6; }
	inline void set_rtGeo_6(RT_t167452472 * value)
	{
		___rtGeo_6 = value;
		Il2CppCodeGenWriteBarrier((&___rtGeo_6), value);
	}

	inline static int32_t get_offset_of_goOnlyInPolyMode_7() { return static_cast<int32_t>(offsetof(PolyController_t1840278131, ___goOnlyInPolyMode_7)); }
	inline GameObjectU5BU5D_t3328599146* get_goOnlyInPolyMode_7() const { return ___goOnlyInPolyMode_7; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_goOnlyInPolyMode_7() { return &___goOnlyInPolyMode_7; }
	inline void set_goOnlyInPolyMode_7(GameObjectU5BU5D_t3328599146* value)
	{
		___goOnlyInPolyMode_7 = value;
		Il2CppCodeGenWriteBarrier((&___goOnlyInPolyMode_7), value);
	}

	inline static int32_t get_offset_of_goOnlyInGeoMode_8() { return static_cast<int32_t>(offsetof(PolyController_t1840278131, ___goOnlyInGeoMode_8)); }
	inline GameObjectU5BU5D_t3328599146* get_goOnlyInGeoMode_8() const { return ___goOnlyInGeoMode_8; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_goOnlyInGeoMode_8() { return &___goOnlyInGeoMode_8; }
	inline void set_goOnlyInGeoMode_8(GameObjectU5BU5D_t3328599146* value)
	{
		___goOnlyInGeoMode_8 = value;
		Il2CppCodeGenWriteBarrier((&___goOnlyInGeoMode_8), value);
	}

	inline static int32_t get_offset_of_tScrollContent_9() { return static_cast<int32_t>(offsetof(PolyController_t1840278131, ___tScrollContent_9)); }
	inline Transform_t3600365921 * get_tScrollContent_9() const { return ___tScrollContent_9; }
	inline Transform_t3600365921 ** get_address_of_tScrollContent_9() { return &___tScrollContent_9; }
	inline void set_tScrollContent_9(Transform_t3600365921 * value)
	{
		___tScrollContent_9 = value;
		Il2CppCodeGenWriteBarrier((&___tScrollContent_9), value);
	}

	inline static int32_t get_offset_of_goScrollPrefabButton_10() { return static_cast<int32_t>(offsetof(PolyController_t1840278131, ___goScrollPrefabButton_10)); }
	inline GameObject_t1113636619 * get_goScrollPrefabButton_10() const { return ___goScrollPrefabButton_10; }
	inline GameObject_t1113636619 ** get_address_of_goScrollPrefabButton_10() { return &___goScrollPrefabButton_10; }
	inline void set_goScrollPrefabButton_10(GameObject_t1113636619 * value)
	{
		___goScrollPrefabButton_10 = value;
		Il2CppCodeGenWriteBarrier((&___goScrollPrefabButton_10), value);
	}
};

struct PolyController_t1840278131_StaticFields
{
public:
	// System.Boolean PolyController::polyMode
	bool ___polyMode_3;
	// UnityEngine.GameObject PolyController::currentLoaded
	GameObject_t1113636619 * ___currentLoaded_11;
	// System.Single PolyController::center
	float ___center_12;
	// UnityEngine.UI.Image PolyController::lastBorder
	Image_t2670269651 * ___lastBorder_13;

public:
	inline static int32_t get_offset_of_polyMode_3() { return static_cast<int32_t>(offsetof(PolyController_t1840278131_StaticFields, ___polyMode_3)); }
	inline bool get_polyMode_3() const { return ___polyMode_3; }
	inline bool* get_address_of_polyMode_3() { return &___polyMode_3; }
	inline void set_polyMode_3(bool value)
	{
		___polyMode_3 = value;
	}

	inline static int32_t get_offset_of_currentLoaded_11() { return static_cast<int32_t>(offsetof(PolyController_t1840278131_StaticFields, ___currentLoaded_11)); }
	inline GameObject_t1113636619 * get_currentLoaded_11() const { return ___currentLoaded_11; }
	inline GameObject_t1113636619 ** get_address_of_currentLoaded_11() { return &___currentLoaded_11; }
	inline void set_currentLoaded_11(GameObject_t1113636619 * value)
	{
		___currentLoaded_11 = value;
		Il2CppCodeGenWriteBarrier((&___currentLoaded_11), value);
	}

	inline static int32_t get_offset_of_center_12() { return static_cast<int32_t>(offsetof(PolyController_t1840278131_StaticFields, ___center_12)); }
	inline float get_center_12() const { return ___center_12; }
	inline float* get_address_of_center_12() { return &___center_12; }
	inline void set_center_12(float value)
	{
		___center_12 = value;
	}

	inline static int32_t get_offset_of_lastBorder_13() { return static_cast<int32_t>(offsetof(PolyController_t1840278131_StaticFields, ___lastBorder_13)); }
	inline Image_t2670269651 * get_lastBorder_13() const { return ___lastBorder_13; }
	inline Image_t2670269651 ** get_address_of_lastBorder_13() { return &___lastBorder_13; }
	inline void set_lastBorder_13(Image_t2670269651 * value)
	{
		___lastBorder_13 = value;
		Il2CppCodeGenWriteBarrier((&___lastBorder_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYCONTROLLER_T1840278131_H
#ifndef HITTABLE_T1494261031_H
#define HITTABLE_T1494261031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Hittable
struct  Hittable_t1494261031  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Hittable::go_Spawn
	GameObject_t1113636619 * ___go_Spawn_2;

public:
	inline static int32_t get_offset_of_go_Spawn_2() { return static_cast<int32_t>(offsetof(Hittable_t1494261031, ___go_Spawn_2)); }
	inline GameObject_t1113636619 * get_go_Spawn_2() const { return ___go_Spawn_2; }
	inline GameObject_t1113636619 ** get_address_of_go_Spawn_2() { return &___go_Spawn_2; }
	inline void set_go_Spawn_2(GameObject_t1113636619 * value)
	{
		___go_Spawn_2 = value;
		Il2CppCodeGenWriteBarrier((&___go_Spawn_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HITTABLE_T1494261031_H
#ifndef BASEEFFECT_T1187847871_H
#define BASEEFFECT_T1187847871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.BaseEffect
struct  BaseEffect_t1187847871  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Shader Colorful.BaseEffect::Shader
	Shader_t4151988712 * ___Shader_2;
	// UnityEngine.Material Colorful.BaseEffect::m_Material
	Material_t340375123 * ___m_Material_3;

public:
	inline static int32_t get_offset_of_Shader_2() { return static_cast<int32_t>(offsetof(BaseEffect_t1187847871, ___Shader_2)); }
	inline Shader_t4151988712 * get_Shader_2() const { return ___Shader_2; }
	inline Shader_t4151988712 ** get_address_of_Shader_2() { return &___Shader_2; }
	inline void set_Shader_2(Shader_t4151988712 * value)
	{
		___Shader_2 = value;
		Il2CppCodeGenWriteBarrier((&___Shader_2), value);
	}

	inline static int32_t get_offset_of_m_Material_3() { return static_cast<int32_t>(offsetof(BaseEffect_t1187847871, ___m_Material_3)); }
	inline Material_t340375123 * get_m_Material_3() const { return ___m_Material_3; }
	inline Material_t340375123 ** get_address_of_m_Material_3() { return &___m_Material_3; }
	inline void set_m_Material_3(Material_t340375123 * value)
	{
		___m_Material_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEEFFECT_T1187847871_H
#ifndef MOVER_T2250641681_H
#define MOVER_T2250641681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mover
struct  Mover_t2250641681  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Mover::speed
	float ___speed_2;
	// UnityEngine.Vector3 Mover::MovingDirection
	Vector3_t3722313464  ___MovingDirection_3;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(Mover_t2250641681, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_MovingDirection_3() { return static_cast<int32_t>(offsetof(Mover_t2250641681, ___MovingDirection_3)); }
	inline Vector3_t3722313464  get_MovingDirection_3() const { return ___MovingDirection_3; }
	inline Vector3_t3722313464 * get_address_of_MovingDirection_3() { return &___MovingDirection_3; }
	inline void set_MovingDirection_3(Vector3_t3722313464  value)
	{
		___MovingDirection_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVER_T2250641681_H
#ifndef WRLDHELPER_T4086237380_H
#define WRLDHELPER_T4086237380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WrldHelper
struct  WrldHelper_t4086237380  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material WrldHelper::matHighlightDefault
	Material_t340375123 * ___matHighlightDefault_2;

public:
	inline static int32_t get_offset_of_matHighlightDefault_2() { return static_cast<int32_t>(offsetof(WrldHelper_t4086237380, ___matHighlightDefault_2)); }
	inline Material_t340375123 * get_matHighlightDefault_2() const { return ___matHighlightDefault_2; }
	inline Material_t340375123 ** get_address_of_matHighlightDefault_2() { return &___matHighlightDefault_2; }
	inline void set_matHighlightDefault_2(Material_t340375123 * value)
	{
		___matHighlightDefault_2 = value;
		Il2CppCodeGenWriteBarrier((&___matHighlightDefault_2), value);
	}
};

struct WrldHelper_t4086237380_StaticFields
{
public:
	// UnityEngine.Material WrldHelper::MatHighlightDefault_static
	Material_t340375123 * ___MatHighlightDefault_static_3;
	// WrldHelper/Highlighter WrldHelper::HighlightReturn
	Highlighter_t666689529 * ___HighlightReturn_4;
	// Wrld.Resources.Buildings.Highlight WrldHelper::lastHighlighted
	Highlight_t3200169708 * ___lastHighlighted_5;
	// Wrld.Space.LatLong WrldHelper::currentLatLng
	LatLong_t2936018554  ___currentLatLng_6;
	// System.Collections.Generic.List`1<Wrld.Resources.Buildings.Highlight> WrldHelper::listHighlighted
	List_1_t377277154 * ___listHighlighted_7;
	// Wrld.Resources.Buildings.BuildingsApi/HighlightReceivedCallback WrldHelper::<>f__mg$cache0
	HighlightReceivedCallback_t2001851641 * ___U3CU3Ef__mgU24cache0_8;

public:
	inline static int32_t get_offset_of_MatHighlightDefault_static_3() { return static_cast<int32_t>(offsetof(WrldHelper_t4086237380_StaticFields, ___MatHighlightDefault_static_3)); }
	inline Material_t340375123 * get_MatHighlightDefault_static_3() const { return ___MatHighlightDefault_static_3; }
	inline Material_t340375123 ** get_address_of_MatHighlightDefault_static_3() { return &___MatHighlightDefault_static_3; }
	inline void set_MatHighlightDefault_static_3(Material_t340375123 * value)
	{
		___MatHighlightDefault_static_3 = value;
		Il2CppCodeGenWriteBarrier((&___MatHighlightDefault_static_3), value);
	}

	inline static int32_t get_offset_of_HighlightReturn_4() { return static_cast<int32_t>(offsetof(WrldHelper_t4086237380_StaticFields, ___HighlightReturn_4)); }
	inline Highlighter_t666689529 * get_HighlightReturn_4() const { return ___HighlightReturn_4; }
	inline Highlighter_t666689529 ** get_address_of_HighlightReturn_4() { return &___HighlightReturn_4; }
	inline void set_HighlightReturn_4(Highlighter_t666689529 * value)
	{
		___HighlightReturn_4 = value;
		Il2CppCodeGenWriteBarrier((&___HighlightReturn_4), value);
	}

	inline static int32_t get_offset_of_lastHighlighted_5() { return static_cast<int32_t>(offsetof(WrldHelper_t4086237380_StaticFields, ___lastHighlighted_5)); }
	inline Highlight_t3200169708 * get_lastHighlighted_5() const { return ___lastHighlighted_5; }
	inline Highlight_t3200169708 ** get_address_of_lastHighlighted_5() { return &___lastHighlighted_5; }
	inline void set_lastHighlighted_5(Highlight_t3200169708 * value)
	{
		___lastHighlighted_5 = value;
		Il2CppCodeGenWriteBarrier((&___lastHighlighted_5), value);
	}

	inline static int32_t get_offset_of_currentLatLng_6() { return static_cast<int32_t>(offsetof(WrldHelper_t4086237380_StaticFields, ___currentLatLng_6)); }
	inline LatLong_t2936018554  get_currentLatLng_6() const { return ___currentLatLng_6; }
	inline LatLong_t2936018554 * get_address_of_currentLatLng_6() { return &___currentLatLng_6; }
	inline void set_currentLatLng_6(LatLong_t2936018554  value)
	{
		___currentLatLng_6 = value;
	}

	inline static int32_t get_offset_of_listHighlighted_7() { return static_cast<int32_t>(offsetof(WrldHelper_t4086237380_StaticFields, ___listHighlighted_7)); }
	inline List_1_t377277154 * get_listHighlighted_7() const { return ___listHighlighted_7; }
	inline List_1_t377277154 ** get_address_of_listHighlighted_7() { return &___listHighlighted_7; }
	inline void set_listHighlighted_7(List_1_t377277154 * value)
	{
		___listHighlighted_7 = value;
		Il2CppCodeGenWriteBarrier((&___listHighlighted_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_8() { return static_cast<int32_t>(offsetof(WrldHelper_t4086237380_StaticFields, ___U3CU3Ef__mgU24cache0_8)); }
	inline HighlightReceivedCallback_t2001851641 * get_U3CU3Ef__mgU24cache0_8() const { return ___U3CU3Ef__mgU24cache0_8; }
	inline HighlightReceivedCallback_t2001851641 ** get_address_of_U3CU3Ef__mgU24cache0_8() { return &___U3CU3Ef__mgU24cache0_8; }
	inline void set_U3CU3Ef__mgU24cache0_8(HighlightReceivedCallback_t2001851641 * value)
	{
		___U3CU3Ef__mgU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRLDHELPER_T4086237380_H
#ifndef SHOWVIDEO_T3206053106_H
#define SHOWVIDEO_T3206053106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShowVideo
struct  ShowVideo_t3206053106  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] ShowVideo::goOnlyInMapMode
	GameObjectU5BU5D_t3328599146* ___goOnlyInMapMode_2;
	// UnityEngine.GameObject[] ShowVideo::goInRealCamOnly
	GameObjectU5BU5D_t3328599146* ___goInRealCamOnly_3;

public:
	inline static int32_t get_offset_of_goOnlyInMapMode_2() { return static_cast<int32_t>(offsetof(ShowVideo_t3206053106, ___goOnlyInMapMode_2)); }
	inline GameObjectU5BU5D_t3328599146* get_goOnlyInMapMode_2() const { return ___goOnlyInMapMode_2; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_goOnlyInMapMode_2() { return &___goOnlyInMapMode_2; }
	inline void set_goOnlyInMapMode_2(GameObjectU5BU5D_t3328599146* value)
	{
		___goOnlyInMapMode_2 = value;
		Il2CppCodeGenWriteBarrier((&___goOnlyInMapMode_2), value);
	}

	inline static int32_t get_offset_of_goInRealCamOnly_3() { return static_cast<int32_t>(offsetof(ShowVideo_t3206053106, ___goInRealCamOnly_3)); }
	inline GameObjectU5BU5D_t3328599146* get_goInRealCamOnly_3() const { return ___goInRealCamOnly_3; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_goInRealCamOnly_3() { return &___goInRealCamOnly_3; }
	inline void set_goInRealCamOnly_3(GameObjectU5BU5D_t3328599146* value)
	{
		___goInRealCamOnly_3 = value;
		Il2CppCodeGenWriteBarrier((&___goInRealCamOnly_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWVIDEO_T3206053106_H
#ifndef LOADLOCATION_T1360453159_H
#define LOADLOCATION_T1360453159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadLocation
struct  LoadLocation_t1360453159  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.InputField LoadLocation::text_LatLng
	InputField_t3762917431 * ___text_LatLng_2;

public:
	inline static int32_t get_offset_of_text_LatLng_2() { return static_cast<int32_t>(offsetof(LoadLocation_t1360453159, ___text_LatLng_2)); }
	inline InputField_t3762917431 * get_text_LatLng_2() const { return ___text_LatLng_2; }
	inline InputField_t3762917431 ** get_address_of_text_LatLng_2() { return &___text_LatLng_2; }
	inline void set_text_LatLng_2(InputField_t3762917431 * value)
	{
		___text_LatLng_2 = value;
		Il2CppCodeGenWriteBarrier((&___text_LatLng_2), value);
	}
};

struct LoadLocation_t1360453159_StaticFields
{
public:
	// System.String LoadLocation::url
	String_t* ___url_3;

public:
	inline static int32_t get_offset_of_url_3() { return static_cast<int32_t>(offsetof(LoadLocation_t1360453159_StaticFields, ___url_3)); }
	inline String_t* get_url_3() const { return ___url_3; }
	inline String_t** get_address_of_url_3() { return &___url_3; }
	inline void set_url_3(String_t* value)
	{
		___url_3 = value;
		Il2CppCodeGenWriteBarrier((&___url_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADLOCATION_T1360453159_H
#ifndef GETLOCATION_T3988867825_H
#define GETLOCATION_T3988867825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetLocation
struct  GetLocation_t3988867825  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button GetLocation::disablebutton
	Button_t4055032469 * ___disablebutton_2;
	// LoadLocation GetLocation::ll
	LoadLocation_t1360453159 * ___ll_3;
	// UnityEngine.UI.InputField GetLocation::input
	InputField_t3762917431 * ___input_4;

public:
	inline static int32_t get_offset_of_disablebutton_2() { return static_cast<int32_t>(offsetof(GetLocation_t3988867825, ___disablebutton_2)); }
	inline Button_t4055032469 * get_disablebutton_2() const { return ___disablebutton_2; }
	inline Button_t4055032469 ** get_address_of_disablebutton_2() { return &___disablebutton_2; }
	inline void set_disablebutton_2(Button_t4055032469 * value)
	{
		___disablebutton_2 = value;
		Il2CppCodeGenWriteBarrier((&___disablebutton_2), value);
	}

	inline static int32_t get_offset_of_ll_3() { return static_cast<int32_t>(offsetof(GetLocation_t3988867825, ___ll_3)); }
	inline LoadLocation_t1360453159 * get_ll_3() const { return ___ll_3; }
	inline LoadLocation_t1360453159 ** get_address_of_ll_3() { return &___ll_3; }
	inline void set_ll_3(LoadLocation_t1360453159 * value)
	{
		___ll_3 = value;
		Il2CppCodeGenWriteBarrier((&___ll_3), value);
	}

	inline static int32_t get_offset_of_input_4() { return static_cast<int32_t>(offsetof(GetLocation_t3988867825, ___input_4)); }
	inline InputField_t3762917431 * get_input_4() const { return ___input_4; }
	inline InputField_t3762917431 ** get_address_of_input_4() { return &___input_4; }
	inline void set_input_4(InputField_t3762917431 * value)
	{
		___input_4 = value;
		Il2CppCodeGenWriteBarrier((&___input_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETLOCATION_T3988867825_H
#ifndef BLINK_T1044540832_H
#define BLINK_T1044540832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Blink
struct  Blink_t1044540832  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image Blink::rt
	Image_t2670269651 * ___rt_2;
	// System.Boolean Blink::started
	bool ___started_3;
	// System.Single Blink::blinkMax
	float ___blinkMax_4;
	// System.Single Blink::blinkMin
	float ___blinkMin_5;
	// UnityEngine.Color Blink::colorFinal
	Color_t2555686324  ___colorFinal_6;

public:
	inline static int32_t get_offset_of_rt_2() { return static_cast<int32_t>(offsetof(Blink_t1044540832, ___rt_2)); }
	inline Image_t2670269651 * get_rt_2() const { return ___rt_2; }
	inline Image_t2670269651 ** get_address_of_rt_2() { return &___rt_2; }
	inline void set_rt_2(Image_t2670269651 * value)
	{
		___rt_2 = value;
		Il2CppCodeGenWriteBarrier((&___rt_2), value);
	}

	inline static int32_t get_offset_of_started_3() { return static_cast<int32_t>(offsetof(Blink_t1044540832, ___started_3)); }
	inline bool get_started_3() const { return ___started_3; }
	inline bool* get_address_of_started_3() { return &___started_3; }
	inline void set_started_3(bool value)
	{
		___started_3 = value;
	}

	inline static int32_t get_offset_of_blinkMax_4() { return static_cast<int32_t>(offsetof(Blink_t1044540832, ___blinkMax_4)); }
	inline float get_blinkMax_4() const { return ___blinkMax_4; }
	inline float* get_address_of_blinkMax_4() { return &___blinkMax_4; }
	inline void set_blinkMax_4(float value)
	{
		___blinkMax_4 = value;
	}

	inline static int32_t get_offset_of_blinkMin_5() { return static_cast<int32_t>(offsetof(Blink_t1044540832, ___blinkMin_5)); }
	inline float get_blinkMin_5() const { return ___blinkMin_5; }
	inline float* get_address_of_blinkMin_5() { return &___blinkMin_5; }
	inline void set_blinkMin_5(float value)
	{
		___blinkMin_5 = value;
	}

	inline static int32_t get_offset_of_colorFinal_6() { return static_cast<int32_t>(offsetof(Blink_t1044540832, ___colorFinal_6)); }
	inline Color_t2555686324  get_colorFinal_6() const { return ___colorFinal_6; }
	inline Color_t2555686324 * get_address_of_colorFinal_6() { return &___colorFinal_6; }
	inline void set_colorFinal_6(Color_t2555686324  value)
	{
		___colorFinal_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLINK_T1044540832_H
#ifndef ANCHORAVATAR_T2494489176_H
#define ANCHORAVATAR_T2494489176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnchorAvatar
struct  AnchorAvatar_t2494489176  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHORAVATAR_T2494489176_H
#ifndef KILLME_T1031851152_H
#define KILLME_T1031851152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KillMe
struct  KillMe_t1031851152  : public MonoBehaviour_t3962482529
{
public:
	// System.Single KillMe::secsLeft
	float ___secsLeft_2;

public:
	inline static int32_t get_offset_of_secsLeft_2() { return static_cast<int32_t>(offsetof(KillMe_t1031851152, ___secsLeft_2)); }
	inline float get_secsLeft_2() const { return ___secsLeft_2; }
	inline float* get_address_of_secsLeft_2() { return &___secsLeft_2; }
	inline void set_secsLeft_2(float value)
	{
		___secsLeft_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KILLME_T1031851152_H
#ifndef PULSATE_T1332653514_H
#define PULSATE_T1332653514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pulsate
struct  Pulsate_t1332653514  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform Pulsate::rt
	RectTransform_t3704657025 * ___rt_2;
	// System.Boolean Pulsate::started
	bool ___started_3;

public:
	inline static int32_t get_offset_of_rt_2() { return static_cast<int32_t>(offsetof(Pulsate_t1332653514, ___rt_2)); }
	inline RectTransform_t3704657025 * get_rt_2() const { return ___rt_2; }
	inline RectTransform_t3704657025 ** get_address_of_rt_2() { return &___rt_2; }
	inline void set_rt_2(RectTransform_t3704657025 * value)
	{
		___rt_2 = value;
		Il2CppCodeGenWriteBarrier((&___rt_2), value);
	}

	inline static int32_t get_offset_of_started_3() { return static_cast<int32_t>(offsetof(Pulsate_t1332653514, ___started_3)); }
	inline bool get_started_3() const { return ___started_3; }
	inline bool* get_address_of_started_3() { return &___started_3; }
	inline void set_started_3(bool value)
	{
		___started_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PULSATE_T1332653514_H
#ifndef THEMECONTROLLER_T2603068051_H
#define THEMECONTROLLER_T2603068051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThemeController
struct  ThemeController_t2603068051  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject ThemeController::goUI_ThemeBar
	GameObject_t1113636619 * ___goUI_ThemeBar_2;
	// Colorful.BleachBypass ThemeController::bleach
	BleachBypass_t2568391998 * ___bleach_3;
	// Colorful.Kuwahara ThemeController::kuwahara
	Kuwahara_t240745109 * ___kuwahara_4;
	// Colorful.ComicBook ThemeController::cb
	ComicBook_t2243422766 * ___cb_5;
	// Colorful.Led ThemeController::led
	Led_t3330730803 * ___led_6;
	// Colorful.VintageFast ThemeController::vintage
	VintageFast_t3040349303 * ___vintage_7;
	// Colorful.ShadowsMidtonesHighlights ThemeController::smh
	ShadowsMidtonesHighlights_t2681697010 * ___smh_8;
	// Colorful.Glitch ThemeController::glitch
	Glitch_t3656535212 * ___glitch_9;

public:
	inline static int32_t get_offset_of_goUI_ThemeBar_2() { return static_cast<int32_t>(offsetof(ThemeController_t2603068051, ___goUI_ThemeBar_2)); }
	inline GameObject_t1113636619 * get_goUI_ThemeBar_2() const { return ___goUI_ThemeBar_2; }
	inline GameObject_t1113636619 ** get_address_of_goUI_ThemeBar_2() { return &___goUI_ThemeBar_2; }
	inline void set_goUI_ThemeBar_2(GameObject_t1113636619 * value)
	{
		___goUI_ThemeBar_2 = value;
		Il2CppCodeGenWriteBarrier((&___goUI_ThemeBar_2), value);
	}

	inline static int32_t get_offset_of_bleach_3() { return static_cast<int32_t>(offsetof(ThemeController_t2603068051, ___bleach_3)); }
	inline BleachBypass_t2568391998 * get_bleach_3() const { return ___bleach_3; }
	inline BleachBypass_t2568391998 ** get_address_of_bleach_3() { return &___bleach_3; }
	inline void set_bleach_3(BleachBypass_t2568391998 * value)
	{
		___bleach_3 = value;
		Il2CppCodeGenWriteBarrier((&___bleach_3), value);
	}

	inline static int32_t get_offset_of_kuwahara_4() { return static_cast<int32_t>(offsetof(ThemeController_t2603068051, ___kuwahara_4)); }
	inline Kuwahara_t240745109 * get_kuwahara_4() const { return ___kuwahara_4; }
	inline Kuwahara_t240745109 ** get_address_of_kuwahara_4() { return &___kuwahara_4; }
	inline void set_kuwahara_4(Kuwahara_t240745109 * value)
	{
		___kuwahara_4 = value;
		Il2CppCodeGenWriteBarrier((&___kuwahara_4), value);
	}

	inline static int32_t get_offset_of_cb_5() { return static_cast<int32_t>(offsetof(ThemeController_t2603068051, ___cb_5)); }
	inline ComicBook_t2243422766 * get_cb_5() const { return ___cb_5; }
	inline ComicBook_t2243422766 ** get_address_of_cb_5() { return &___cb_5; }
	inline void set_cb_5(ComicBook_t2243422766 * value)
	{
		___cb_5 = value;
		Il2CppCodeGenWriteBarrier((&___cb_5), value);
	}

	inline static int32_t get_offset_of_led_6() { return static_cast<int32_t>(offsetof(ThemeController_t2603068051, ___led_6)); }
	inline Led_t3330730803 * get_led_6() const { return ___led_6; }
	inline Led_t3330730803 ** get_address_of_led_6() { return &___led_6; }
	inline void set_led_6(Led_t3330730803 * value)
	{
		___led_6 = value;
		Il2CppCodeGenWriteBarrier((&___led_6), value);
	}

	inline static int32_t get_offset_of_vintage_7() { return static_cast<int32_t>(offsetof(ThemeController_t2603068051, ___vintage_7)); }
	inline VintageFast_t3040349303 * get_vintage_7() const { return ___vintage_7; }
	inline VintageFast_t3040349303 ** get_address_of_vintage_7() { return &___vintage_7; }
	inline void set_vintage_7(VintageFast_t3040349303 * value)
	{
		___vintage_7 = value;
		Il2CppCodeGenWriteBarrier((&___vintage_7), value);
	}

	inline static int32_t get_offset_of_smh_8() { return static_cast<int32_t>(offsetof(ThemeController_t2603068051, ___smh_8)); }
	inline ShadowsMidtonesHighlights_t2681697010 * get_smh_8() const { return ___smh_8; }
	inline ShadowsMidtonesHighlights_t2681697010 ** get_address_of_smh_8() { return &___smh_8; }
	inline void set_smh_8(ShadowsMidtonesHighlights_t2681697010 * value)
	{
		___smh_8 = value;
		Il2CppCodeGenWriteBarrier((&___smh_8), value);
	}

	inline static int32_t get_offset_of_glitch_9() { return static_cast<int32_t>(offsetof(ThemeController_t2603068051, ___glitch_9)); }
	inline Glitch_t3656535212 * get_glitch_9() const { return ___glitch_9; }
	inline Glitch_t3656535212 ** get_address_of_glitch_9() { return &___glitch_9; }
	inline void set_glitch_9(Glitch_t3656535212 * value)
	{
		___glitch_9 = value;
		Il2CppCodeGenWriteBarrier((&___glitch_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THEMECONTROLLER_T2603068051_H
#ifndef REPLAYTEST_T92159691_H
#define REPLAYTEST_T92159691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReplayTest
struct  ReplayTest_t92159691  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] ReplayTest::goTurnOffInReplayMode
	GameObjectU5BU5D_t3328599146* ___goTurnOffInReplayMode_2;
	// UIToggle ReplayTest::uitoggle
	UIToggle_t4192126258 * ___uitoggle_3;
	// UnityEngine.AudioSource ReplayTest::aud
	AudioSource_t3935305588 * ___aud_4;

public:
	inline static int32_t get_offset_of_goTurnOffInReplayMode_2() { return static_cast<int32_t>(offsetof(ReplayTest_t92159691, ___goTurnOffInReplayMode_2)); }
	inline GameObjectU5BU5D_t3328599146* get_goTurnOffInReplayMode_2() const { return ___goTurnOffInReplayMode_2; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_goTurnOffInReplayMode_2() { return &___goTurnOffInReplayMode_2; }
	inline void set_goTurnOffInReplayMode_2(GameObjectU5BU5D_t3328599146* value)
	{
		___goTurnOffInReplayMode_2 = value;
		Il2CppCodeGenWriteBarrier((&___goTurnOffInReplayMode_2), value);
	}

	inline static int32_t get_offset_of_uitoggle_3() { return static_cast<int32_t>(offsetof(ReplayTest_t92159691, ___uitoggle_3)); }
	inline UIToggle_t4192126258 * get_uitoggle_3() const { return ___uitoggle_3; }
	inline UIToggle_t4192126258 ** get_address_of_uitoggle_3() { return &___uitoggle_3; }
	inline void set_uitoggle_3(UIToggle_t4192126258 * value)
	{
		___uitoggle_3 = value;
		Il2CppCodeGenWriteBarrier((&___uitoggle_3), value);
	}

	inline static int32_t get_offset_of_aud_4() { return static_cast<int32_t>(offsetof(ReplayTest_t92159691, ___aud_4)); }
	inline AudioSource_t3935305588 * get_aud_4() const { return ___aud_4; }
	inline AudioSource_t3935305588 ** get_address_of_aud_4() { return &___aud_4; }
	inline void set_aud_4(AudioSource_t3935305588 * value)
	{
		___aud_4 = value;
		Il2CppCodeGenWriteBarrier((&___aud_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPLAYTEST_T92159691_H
#ifndef ANCHORTHEME_T4272755557_H
#define ANCHORTHEME_T4272755557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnchorTheme
struct  AnchorTheme_t4272755557  : public MonoBehaviour_t3962482529
{
public:
	// System.String AnchorTheme::ThemeName
	String_t* ___ThemeName_3;
	// UnityEngine.Color AnchorTheme::colorWrld_Highlight
	Color_t2555686324  ___colorWrld_Highlight_4;
	// UnityEngine.GameObject AnchorTheme::goCharacterPrefab
	GameObject_t1113636619 * ___goCharacterPrefab_5;
	// System.String AnchorTheme::Poly_SearchTerm
	String_t* ___Poly_SearchTerm_6;
	// System.Boolean AnchorTheme::makeMeDefault
	bool ___makeMeDefault_7;

public:
	inline static int32_t get_offset_of_ThemeName_3() { return static_cast<int32_t>(offsetof(AnchorTheme_t4272755557, ___ThemeName_3)); }
	inline String_t* get_ThemeName_3() const { return ___ThemeName_3; }
	inline String_t** get_address_of_ThemeName_3() { return &___ThemeName_3; }
	inline void set_ThemeName_3(String_t* value)
	{
		___ThemeName_3 = value;
		Il2CppCodeGenWriteBarrier((&___ThemeName_3), value);
	}

	inline static int32_t get_offset_of_colorWrld_Highlight_4() { return static_cast<int32_t>(offsetof(AnchorTheme_t4272755557, ___colorWrld_Highlight_4)); }
	inline Color_t2555686324  get_colorWrld_Highlight_4() const { return ___colorWrld_Highlight_4; }
	inline Color_t2555686324 * get_address_of_colorWrld_Highlight_4() { return &___colorWrld_Highlight_4; }
	inline void set_colorWrld_Highlight_4(Color_t2555686324  value)
	{
		___colorWrld_Highlight_4 = value;
	}

	inline static int32_t get_offset_of_goCharacterPrefab_5() { return static_cast<int32_t>(offsetof(AnchorTheme_t4272755557, ___goCharacterPrefab_5)); }
	inline GameObject_t1113636619 * get_goCharacterPrefab_5() const { return ___goCharacterPrefab_5; }
	inline GameObject_t1113636619 ** get_address_of_goCharacterPrefab_5() { return &___goCharacterPrefab_5; }
	inline void set_goCharacterPrefab_5(GameObject_t1113636619 * value)
	{
		___goCharacterPrefab_5 = value;
		Il2CppCodeGenWriteBarrier((&___goCharacterPrefab_5), value);
	}

	inline static int32_t get_offset_of_Poly_SearchTerm_6() { return static_cast<int32_t>(offsetof(AnchorTheme_t4272755557, ___Poly_SearchTerm_6)); }
	inline String_t* get_Poly_SearchTerm_6() const { return ___Poly_SearchTerm_6; }
	inline String_t** get_address_of_Poly_SearchTerm_6() { return &___Poly_SearchTerm_6; }
	inline void set_Poly_SearchTerm_6(String_t* value)
	{
		___Poly_SearchTerm_6 = value;
		Il2CppCodeGenWriteBarrier((&___Poly_SearchTerm_6), value);
	}

	inline static int32_t get_offset_of_makeMeDefault_7() { return static_cast<int32_t>(offsetof(AnchorTheme_t4272755557, ___makeMeDefault_7)); }
	inline bool get_makeMeDefault_7() const { return ___makeMeDefault_7; }
	inline bool* get_address_of_makeMeDefault_7() { return &___makeMeDefault_7; }
	inline void set_makeMeDefault_7(bool value)
	{
		___makeMeDefault_7 = value;
	}
};

struct AnchorTheme_t4272755557_StaticFields
{
public:
	// AnchorTheme AnchorTheme::currentTheme
	AnchorTheme_t4272755557 * ___currentTheme_2;

public:
	inline static int32_t get_offset_of_currentTheme_2() { return static_cast<int32_t>(offsetof(AnchorTheme_t4272755557_StaticFields, ___currentTheme_2)); }
	inline AnchorTheme_t4272755557 * get_currentTheme_2() const { return ___currentTheme_2; }
	inline AnchorTheme_t4272755557 ** get_address_of_currentTheme_2() { return &___currentTheme_2; }
	inline void set_currentTheme_2(AnchorTheme_t4272755557 * value)
	{
		___currentTheme_2 = value;
		Il2CppCodeGenWriteBarrier((&___currentTheme_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHORTHEME_T4272755557_H
#ifndef WRLDPLACERPOLY_T2262179182_H
#define WRLDPLACERPOLY_T2262179182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WRLDPlacerPoly
struct  WRLDPlacerPoly_t2262179182  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RaycastHit WRLDPlacerPoly::lastHit
	RaycastHit_t1056001966  ___lastHit_5;
	// UnityEngine.GameObject WRLDPlacerPoly::goUI_PlacingMode
	GameObject_t1113636619 * ___goUI_PlacingMode_6;
	// UnityEngine.UI.Slider WRLDPlacerPoly::slider
	Slider_t3903728902 * ___slider_9;

public:
	inline static int32_t get_offset_of_lastHit_5() { return static_cast<int32_t>(offsetof(WRLDPlacerPoly_t2262179182, ___lastHit_5)); }
	inline RaycastHit_t1056001966  get_lastHit_5() const { return ___lastHit_5; }
	inline RaycastHit_t1056001966 * get_address_of_lastHit_5() { return &___lastHit_5; }
	inline void set_lastHit_5(RaycastHit_t1056001966  value)
	{
		___lastHit_5 = value;
	}

	inline static int32_t get_offset_of_goUI_PlacingMode_6() { return static_cast<int32_t>(offsetof(WRLDPlacerPoly_t2262179182, ___goUI_PlacingMode_6)); }
	inline GameObject_t1113636619 * get_goUI_PlacingMode_6() const { return ___goUI_PlacingMode_6; }
	inline GameObject_t1113636619 ** get_address_of_goUI_PlacingMode_6() { return &___goUI_PlacingMode_6; }
	inline void set_goUI_PlacingMode_6(GameObject_t1113636619 * value)
	{
		___goUI_PlacingMode_6 = value;
		Il2CppCodeGenWriteBarrier((&___goUI_PlacingMode_6), value);
	}

	inline static int32_t get_offset_of_slider_9() { return static_cast<int32_t>(offsetof(WRLDPlacerPoly_t2262179182, ___slider_9)); }
	inline Slider_t3903728902 * get_slider_9() const { return ___slider_9; }
	inline Slider_t3903728902 ** get_address_of_slider_9() { return &___slider_9; }
	inline void set_slider_9(Slider_t3903728902 * value)
	{
		___slider_9 = value;
		Il2CppCodeGenWriteBarrier((&___slider_9), value);
	}
};

struct WRLDPlacerPoly_t2262179182_StaticFields
{
public:
	// UnityEngine.Transform WRLDPlacerPoly::current
	Transform_t3600365921 * ___current_2;
	// UnityEngine.Vector2 WRLDPlacerPoly::sliderBounds
	Vector2_t2156229523  ___sliderBounds_3;
	// System.Boolean WRLDPlacerPoly::placingMode
	bool ___placingMode_4;
	// UnityEngine.GameObject WRLDPlacerPoly::goUI_PlacingModeStatic
	GameObject_t1113636619 * ___goUI_PlacingModeStatic_7;
	// System.Collections.Generic.List`1<UnityEngine.Transform> WRLDPlacerPoly::listPlaced
	List_1_t777473367 * ___listPlaced_8;

public:
	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(WRLDPlacerPoly_t2262179182_StaticFields, ___current_2)); }
	inline Transform_t3600365921 * get_current_2() const { return ___current_2; }
	inline Transform_t3600365921 ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(Transform_t3600365921 * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((&___current_2), value);
	}

	inline static int32_t get_offset_of_sliderBounds_3() { return static_cast<int32_t>(offsetof(WRLDPlacerPoly_t2262179182_StaticFields, ___sliderBounds_3)); }
	inline Vector2_t2156229523  get_sliderBounds_3() const { return ___sliderBounds_3; }
	inline Vector2_t2156229523 * get_address_of_sliderBounds_3() { return &___sliderBounds_3; }
	inline void set_sliderBounds_3(Vector2_t2156229523  value)
	{
		___sliderBounds_3 = value;
	}

	inline static int32_t get_offset_of_placingMode_4() { return static_cast<int32_t>(offsetof(WRLDPlacerPoly_t2262179182_StaticFields, ___placingMode_4)); }
	inline bool get_placingMode_4() const { return ___placingMode_4; }
	inline bool* get_address_of_placingMode_4() { return &___placingMode_4; }
	inline void set_placingMode_4(bool value)
	{
		___placingMode_4 = value;
	}

	inline static int32_t get_offset_of_goUI_PlacingModeStatic_7() { return static_cast<int32_t>(offsetof(WRLDPlacerPoly_t2262179182_StaticFields, ___goUI_PlacingModeStatic_7)); }
	inline GameObject_t1113636619 * get_goUI_PlacingModeStatic_7() const { return ___goUI_PlacingModeStatic_7; }
	inline GameObject_t1113636619 ** get_address_of_goUI_PlacingModeStatic_7() { return &___goUI_PlacingModeStatic_7; }
	inline void set_goUI_PlacingModeStatic_7(GameObject_t1113636619 * value)
	{
		___goUI_PlacingModeStatic_7 = value;
		Il2CppCodeGenWriteBarrier((&___goUI_PlacingModeStatic_7), value);
	}

	inline static int32_t get_offset_of_listPlaced_8() { return static_cast<int32_t>(offsetof(WRLDPlacerPoly_t2262179182_StaticFields, ___listPlaced_8)); }
	inline List_1_t777473367 * get_listPlaced_8() const { return ___listPlaced_8; }
	inline List_1_t777473367 ** get_address_of_listPlaced_8() { return &___listPlaced_8; }
	inline void set_listPlaced_8(List_1_t777473367 * value)
	{
		___listPlaced_8 = value;
		Il2CppCodeGenWriteBarrier((&___listPlaced_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRLDPLACERPOLY_T2262179182_H
#ifndef SCREENSHOTSIMPLE_T3378729939_H
#define SCREENSHOTSIMPLE_T3378729939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenshotSimple
struct  ScreenshotSimple_t3378729939  : public MonoBehaviour_t3962482529
{
public:
	// System.String ScreenshotSimple::filename_noextension
	String_t* ___filename_noextension_2;
	// UnityEngine.GameObject ScreenshotSimple::g
	GameObject_t1113636619 * ___g_3;

public:
	inline static int32_t get_offset_of_filename_noextension_2() { return static_cast<int32_t>(offsetof(ScreenshotSimple_t3378729939, ___filename_noextension_2)); }
	inline String_t* get_filename_noextension_2() const { return ___filename_noextension_2; }
	inline String_t** get_address_of_filename_noextension_2() { return &___filename_noextension_2; }
	inline void set_filename_noextension_2(String_t* value)
	{
		___filename_noextension_2 = value;
		Il2CppCodeGenWriteBarrier((&___filename_noextension_2), value);
	}

	inline static int32_t get_offset_of_g_3() { return static_cast<int32_t>(offsetof(ScreenshotSimple_t3378729939, ___g_3)); }
	inline GameObject_t1113636619 * get_g_3() const { return ___g_3; }
	inline GameObject_t1113636619 ** get_address_of_g_3() { return &___g_3; }
	inline void set_g_3(GameObject_t1113636619 * value)
	{
		___g_3 = value;
		Il2CppCodeGenWriteBarrier((&___g_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSHOTSIMPLE_T3378729939_H
#ifndef ZOMBIE_T2596553718_H
#define ZOMBIE_T2596553718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zombie
struct  Zombie_t2596553718  : public Hittable_t1494261031
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIE_T2596553718_H
#ifndef CHANNELMIXER_T2404774860_H
#define CHANNELMIXER_T2404774860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.ChannelMixer
struct  ChannelMixer_t2404774860  : public BaseEffect_t1187847871
{
public:
	// UnityEngine.Vector3 Colorful.ChannelMixer::Red
	Vector3_t3722313464  ___Red_4;
	// UnityEngine.Vector3 Colorful.ChannelMixer::Green
	Vector3_t3722313464  ___Green_5;
	// UnityEngine.Vector3 Colorful.ChannelMixer::Blue
	Vector3_t3722313464  ___Blue_6;
	// UnityEngine.Vector3 Colorful.ChannelMixer::Constant
	Vector3_t3722313464  ___Constant_7;

public:
	inline static int32_t get_offset_of_Red_4() { return static_cast<int32_t>(offsetof(ChannelMixer_t2404774860, ___Red_4)); }
	inline Vector3_t3722313464  get_Red_4() const { return ___Red_4; }
	inline Vector3_t3722313464 * get_address_of_Red_4() { return &___Red_4; }
	inline void set_Red_4(Vector3_t3722313464  value)
	{
		___Red_4 = value;
	}

	inline static int32_t get_offset_of_Green_5() { return static_cast<int32_t>(offsetof(ChannelMixer_t2404774860, ___Green_5)); }
	inline Vector3_t3722313464  get_Green_5() const { return ___Green_5; }
	inline Vector3_t3722313464 * get_address_of_Green_5() { return &___Green_5; }
	inline void set_Green_5(Vector3_t3722313464  value)
	{
		___Green_5 = value;
	}

	inline static int32_t get_offset_of_Blue_6() { return static_cast<int32_t>(offsetof(ChannelMixer_t2404774860, ___Blue_6)); }
	inline Vector3_t3722313464  get_Blue_6() const { return ___Blue_6; }
	inline Vector3_t3722313464 * get_address_of_Blue_6() { return &___Blue_6; }
	inline void set_Blue_6(Vector3_t3722313464  value)
	{
		___Blue_6 = value;
	}

	inline static int32_t get_offset_of_Constant_7() { return static_cast<int32_t>(offsetof(ChannelMixer_t2404774860, ___Constant_7)); }
	inline Vector3_t3722313464  get_Constant_7() const { return ___Constant_7; }
	inline Vector3_t3722313464 * get_address_of_Constant_7() { return &___Constant_7; }
	inline void set_Constant_7(Vector3_t3722313464  value)
	{
		___Constant_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNELMIXER_T2404774860_H
#ifndef CHANNELCLAMPER_T4227510363_H
#define CHANNELCLAMPER_T4227510363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.ChannelClamper
struct  ChannelClamper_t4227510363  : public BaseEffect_t1187847871
{
public:
	// UnityEngine.Vector2 Colorful.ChannelClamper::Red
	Vector2_t2156229523  ___Red_4;
	// UnityEngine.Vector2 Colorful.ChannelClamper::Green
	Vector2_t2156229523  ___Green_5;
	// UnityEngine.Vector2 Colorful.ChannelClamper::Blue
	Vector2_t2156229523  ___Blue_6;

public:
	inline static int32_t get_offset_of_Red_4() { return static_cast<int32_t>(offsetof(ChannelClamper_t4227510363, ___Red_4)); }
	inline Vector2_t2156229523  get_Red_4() const { return ___Red_4; }
	inline Vector2_t2156229523 * get_address_of_Red_4() { return &___Red_4; }
	inline void set_Red_4(Vector2_t2156229523  value)
	{
		___Red_4 = value;
	}

	inline static int32_t get_offset_of_Green_5() { return static_cast<int32_t>(offsetof(ChannelClamper_t4227510363, ___Green_5)); }
	inline Vector2_t2156229523  get_Green_5() const { return ___Green_5; }
	inline Vector2_t2156229523 * get_address_of_Green_5() { return &___Green_5; }
	inline void set_Green_5(Vector2_t2156229523  value)
	{
		___Green_5 = value;
	}

	inline static int32_t get_offset_of_Blue_6() { return static_cast<int32_t>(offsetof(ChannelClamper_t4227510363, ___Blue_6)); }
	inline Vector2_t2156229523  get_Blue_6() const { return ___Blue_6; }
	inline Vector2_t2156229523 * get_address_of_Blue_6() { return &___Blue_6; }
	inline void set_Blue_6(Vector2_t2156229523  value)
	{
		___Blue_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNELCLAMPER_T4227510363_H
#ifndef BRIGHTNESSCONTRASTGAMMA_T158109514_H
#define BRIGHTNESSCONTRASTGAMMA_T158109514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.BrightnessContrastGamma
struct  BrightnessContrastGamma_t158109514  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.BrightnessContrastGamma::Brightness
	float ___Brightness_4;
	// System.Single Colorful.BrightnessContrastGamma::Contrast
	float ___Contrast_5;
	// UnityEngine.Vector3 Colorful.BrightnessContrastGamma::ContrastCoeff
	Vector3_t3722313464  ___ContrastCoeff_6;
	// System.Single Colorful.BrightnessContrastGamma::Gamma
	float ___Gamma_7;

public:
	inline static int32_t get_offset_of_Brightness_4() { return static_cast<int32_t>(offsetof(BrightnessContrastGamma_t158109514, ___Brightness_4)); }
	inline float get_Brightness_4() const { return ___Brightness_4; }
	inline float* get_address_of_Brightness_4() { return &___Brightness_4; }
	inline void set_Brightness_4(float value)
	{
		___Brightness_4 = value;
	}

	inline static int32_t get_offset_of_Contrast_5() { return static_cast<int32_t>(offsetof(BrightnessContrastGamma_t158109514, ___Contrast_5)); }
	inline float get_Contrast_5() const { return ___Contrast_5; }
	inline float* get_address_of_Contrast_5() { return &___Contrast_5; }
	inline void set_Contrast_5(float value)
	{
		___Contrast_5 = value;
	}

	inline static int32_t get_offset_of_ContrastCoeff_6() { return static_cast<int32_t>(offsetof(BrightnessContrastGamma_t158109514, ___ContrastCoeff_6)); }
	inline Vector3_t3722313464  get_ContrastCoeff_6() const { return ___ContrastCoeff_6; }
	inline Vector3_t3722313464 * get_address_of_ContrastCoeff_6() { return &___ContrastCoeff_6; }
	inline void set_ContrastCoeff_6(Vector3_t3722313464  value)
	{
		___ContrastCoeff_6 = value;
	}

	inline static int32_t get_offset_of_Gamma_7() { return static_cast<int32_t>(offsetof(BrightnessContrastGamma_t158109514, ___Gamma_7)); }
	inline float get_Gamma_7() const { return ___Gamma_7; }
	inline float* get_address_of_Gamma_7() { return &___Gamma_7; }
	inline void set_Gamma_7(float value)
	{
		___Gamma_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BRIGHTNESSCONTRASTGAMMA_T158109514_H
#ifndef BLEND_T936097170_H
#define BLEND_T936097170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.Blend
struct  Blend_t936097170  : public BaseEffect_t1187847871
{
public:
	// UnityEngine.Texture Colorful.Blend::Texture
	Texture_t3661962703 * ___Texture_4;
	// System.Single Colorful.Blend::Amount
	float ___Amount_5;
	// Colorful.Blend/BlendingMode Colorful.Blend::Mode
	int32_t ___Mode_6;

public:
	inline static int32_t get_offset_of_Texture_4() { return static_cast<int32_t>(offsetof(Blend_t936097170, ___Texture_4)); }
	inline Texture_t3661962703 * get_Texture_4() const { return ___Texture_4; }
	inline Texture_t3661962703 ** get_address_of_Texture_4() { return &___Texture_4; }
	inline void set_Texture_4(Texture_t3661962703 * value)
	{
		___Texture_4 = value;
		Il2CppCodeGenWriteBarrier((&___Texture_4), value);
	}

	inline static int32_t get_offset_of_Amount_5() { return static_cast<int32_t>(offsetof(Blend_t936097170, ___Amount_5)); }
	inline float get_Amount_5() const { return ___Amount_5; }
	inline float* get_address_of_Amount_5() { return &___Amount_5; }
	inline void set_Amount_5(float value)
	{
		___Amount_5 = value;
	}

	inline static int32_t get_offset_of_Mode_6() { return static_cast<int32_t>(offsetof(Blend_t936097170, ___Mode_6)); }
	inline int32_t get_Mode_6() const { return ___Mode_6; }
	inline int32_t* get_address_of_Mode_6() { return &___Mode_6; }
	inline void set_Mode_6(int32_t value)
	{
		___Mode_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLEND_T936097170_H
#ifndef BLEACHBYPASS_T2568391998_H
#define BLEACHBYPASS_T2568391998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.BleachBypass
struct  BleachBypass_t2568391998  : public BaseEffect_t1187847871
{
public:
	// System.Single Colorful.BleachBypass::Amount
	float ___Amount_4;

public:
	inline static int32_t get_offset_of_Amount_4() { return static_cast<int32_t>(offsetof(BleachBypass_t2568391998, ___Amount_4)); }
	inline float get_Amount_4() const { return ___Amount_4; }
	inline float* get_address_of_Amount_4() { return &___Amount_4; }
	inline void set_Amount_4(float value)
	{
		___Amount_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLEACHBYPASS_T2568391998_H
#ifndef BILATERALGAUSSIANBLUR_T938878669_H
#define BILATERALGAUSSIANBLUR_T938878669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.BilateralGaussianBlur
struct  BilateralGaussianBlur_t938878669  : public BaseEffect_t1187847871
{
public:
	// System.Int32 Colorful.BilateralGaussianBlur::Passes
	int32_t ___Passes_4;
	// System.Single Colorful.BilateralGaussianBlur::Threshold
	float ___Threshold_5;
	// System.Single Colorful.BilateralGaussianBlur::Amount
	float ___Amount_6;

public:
	inline static int32_t get_offset_of_Passes_4() { return static_cast<int32_t>(offsetof(BilateralGaussianBlur_t938878669, ___Passes_4)); }
	inline int32_t get_Passes_4() const { return ___Passes_4; }
	inline int32_t* get_address_of_Passes_4() { return &___Passes_4; }
	inline void set_Passes_4(int32_t value)
	{
		___Passes_4 = value;
	}

	inline static int32_t get_offset_of_Threshold_5() { return static_cast<int32_t>(offsetof(BilateralGaussianBlur_t938878669, ___Threshold_5)); }
	inline float get_Threshold_5() const { return ___Threshold_5; }
	inline float* get_address_of_Threshold_5() { return &___Threshold_5; }
	inline void set_Threshold_5(float value)
	{
		___Threshold_5 = value;
	}

	inline static int32_t get_offset_of_Amount_6() { return static_cast<int32_t>(offsetof(BilateralGaussianBlur_t938878669, ___Amount_6)); }
	inline float get_Amount_6() const { return ___Amount_6; }
	inline float* get_address_of_Amount_6() { return &___Amount_6; }
	inline void set_Amount_6(float value)
	{
		___Amount_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BILATERALGAUSSIANBLUR_T938878669_H
#ifndef ANALOGTV_T1398652140_H
#define ANALOGTV_T1398652140_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Colorful.AnalogTV
struct  AnalogTV_t1398652140  : public BaseEffect_t1187847871
{
public:
	// System.Boolean Colorful.AnalogTV::AutomaticPhase
	bool ___AutomaticPhase_4;
	// System.Single Colorful.AnalogTV::Phase
	float ___Phase_5;
	// System.Boolean Colorful.AnalogTV::ConvertToGrayscale
	bool ___ConvertToGrayscale_6;
	// System.Single Colorful.AnalogTV::NoiseIntensity
	float ___NoiseIntensity_7;
	// System.Single Colorful.AnalogTV::ScanlinesIntensity
	float ___ScanlinesIntensity_8;
	// System.Int32 Colorful.AnalogTV::ScanlinesCount
	int32_t ___ScanlinesCount_9;
	// System.Single Colorful.AnalogTV::ScanlinesOffset
	float ___ScanlinesOffset_10;
	// System.Boolean Colorful.AnalogTV::VerticalScanlines
	bool ___VerticalScanlines_11;
	// System.Single Colorful.AnalogTV::Distortion
	float ___Distortion_12;
	// System.Single Colorful.AnalogTV::CubicDistortion
	float ___CubicDistortion_13;
	// System.Single Colorful.AnalogTV::Scale
	float ___Scale_14;

public:
	inline static int32_t get_offset_of_AutomaticPhase_4() { return static_cast<int32_t>(offsetof(AnalogTV_t1398652140, ___AutomaticPhase_4)); }
	inline bool get_AutomaticPhase_4() const { return ___AutomaticPhase_4; }
	inline bool* get_address_of_AutomaticPhase_4() { return &___AutomaticPhase_4; }
	inline void set_AutomaticPhase_4(bool value)
	{
		___AutomaticPhase_4 = value;
	}

	inline static int32_t get_offset_of_Phase_5() { return static_cast<int32_t>(offsetof(AnalogTV_t1398652140, ___Phase_5)); }
	inline float get_Phase_5() const { return ___Phase_5; }
	inline float* get_address_of_Phase_5() { return &___Phase_5; }
	inline void set_Phase_5(float value)
	{
		___Phase_5 = value;
	}

	inline static int32_t get_offset_of_ConvertToGrayscale_6() { return static_cast<int32_t>(offsetof(AnalogTV_t1398652140, ___ConvertToGrayscale_6)); }
	inline bool get_ConvertToGrayscale_6() const { return ___ConvertToGrayscale_6; }
	inline bool* get_address_of_ConvertToGrayscale_6() { return &___ConvertToGrayscale_6; }
	inline void set_ConvertToGrayscale_6(bool value)
	{
		___ConvertToGrayscale_6 = value;
	}

	inline static int32_t get_offset_of_NoiseIntensity_7() { return static_cast<int32_t>(offsetof(AnalogTV_t1398652140, ___NoiseIntensity_7)); }
	inline float get_NoiseIntensity_7() const { return ___NoiseIntensity_7; }
	inline float* get_address_of_NoiseIntensity_7() { return &___NoiseIntensity_7; }
	inline void set_NoiseIntensity_7(float value)
	{
		___NoiseIntensity_7 = value;
	}

	inline static int32_t get_offset_of_ScanlinesIntensity_8() { return static_cast<int32_t>(offsetof(AnalogTV_t1398652140, ___ScanlinesIntensity_8)); }
	inline float get_ScanlinesIntensity_8() const { return ___ScanlinesIntensity_8; }
	inline float* get_address_of_ScanlinesIntensity_8() { return &___ScanlinesIntensity_8; }
	inline void set_ScanlinesIntensity_8(float value)
	{
		___ScanlinesIntensity_8 = value;
	}

	inline static int32_t get_offset_of_ScanlinesCount_9() { return static_cast<int32_t>(offsetof(AnalogTV_t1398652140, ___ScanlinesCount_9)); }
	inline int32_t get_ScanlinesCount_9() const { return ___ScanlinesCount_9; }
	inline int32_t* get_address_of_ScanlinesCount_9() { return &___ScanlinesCount_9; }
	inline void set_ScanlinesCount_9(int32_t value)
	{
		___ScanlinesCount_9 = value;
	}

	inline static int32_t get_offset_of_ScanlinesOffset_10() { return static_cast<int32_t>(offsetof(AnalogTV_t1398652140, ___ScanlinesOffset_10)); }
	inline float get_ScanlinesOffset_10() const { return ___ScanlinesOffset_10; }
	inline float* get_address_of_ScanlinesOffset_10() { return &___ScanlinesOffset_10; }
	inline void set_ScanlinesOffset_10(float value)
	{
		___ScanlinesOffset_10 = value;
	}

	inline static int32_t get_offset_of_VerticalScanlines_11() { return static_cast<int32_t>(offsetof(AnalogTV_t1398652140, ___VerticalScanlines_11)); }
	inline bool get_VerticalScanlines_11() const { return ___VerticalScanlines_11; }
	inline bool* get_address_of_VerticalScanlines_11() { return &___VerticalScanlines_11; }
	inline void set_VerticalScanlines_11(bool value)
	{
		___VerticalScanlines_11 = value;
	}

	inline static int32_t get_offset_of_Distortion_12() { return static_cast<int32_t>(offsetof(AnalogTV_t1398652140, ___Distortion_12)); }
	inline float get_Distortion_12() const { return ___Distortion_12; }
	inline float* get_address_of_Distortion_12() { return &___Distortion_12; }
	inline void set_Distortion_12(float value)
	{
		___Distortion_12 = value;
	}

	inline static int32_t get_offset_of_CubicDistortion_13() { return static_cast<int32_t>(offsetof(AnalogTV_t1398652140, ___CubicDistortion_13)); }
	inline float get_CubicDistortion_13() const { return ___CubicDistortion_13; }
	inline float* get_address_of_CubicDistortion_13() { return &___CubicDistortion_13; }
	inline void set_CubicDistortion_13(float value)
	{
		___CubicDistortion_13 = value;
	}

	inline static int32_t get_offset_of_Scale_14() { return static_cast<int32_t>(offsetof(AnalogTV_t1398652140, ___Scale_14)); }
	inline float get_Scale_14() const { return ___Scale_14; }
	inline float* get_address_of_Scale_14() { return &___Scale_14; }
	inline void set_Scale_14(float value)
	{
		___Scale_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALOGTV_T1398652140_H
#ifndef OCTOPUS_T187370362_H
#define OCTOPUS_T187370362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Octopus
struct  Octopus_t187370362  : public Hittable_t1494261031
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCTOPUS_T187370362_H
#ifndef CUSTOMIZEFACEEXTENSION_T1668187399_H
#define CUSTOMIZEFACEEXTENSION_T1668187399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomizeFaceExtension
struct  CustomizeFaceExtension_t1668187399  : public FaceCustomizer_t3426199831
{
public:
	// UnityEngine.GameObject CustomizeFaceExtension::goExitCustomizeModeButton
	GameObject_t1113636619 * ___goExitCustomizeModeButton_10;
	// UnityEngine.UI.Button CustomizeFaceExtension::goDisableInCustomizeMode
	Button_t4055032469 * ___goDisableInCustomizeMode_11;

public:
	inline static int32_t get_offset_of_goExitCustomizeModeButton_10() { return static_cast<int32_t>(offsetof(CustomizeFaceExtension_t1668187399, ___goExitCustomizeModeButton_10)); }
	inline GameObject_t1113636619 * get_goExitCustomizeModeButton_10() const { return ___goExitCustomizeModeButton_10; }
	inline GameObject_t1113636619 ** get_address_of_goExitCustomizeModeButton_10() { return &___goExitCustomizeModeButton_10; }
	inline void set_goExitCustomizeModeButton_10(GameObject_t1113636619 * value)
	{
		___goExitCustomizeModeButton_10 = value;
		Il2CppCodeGenWriteBarrier((&___goExitCustomizeModeButton_10), value);
	}

	inline static int32_t get_offset_of_goDisableInCustomizeMode_11() { return static_cast<int32_t>(offsetof(CustomizeFaceExtension_t1668187399, ___goDisableInCustomizeMode_11)); }
	inline Button_t4055032469 * get_goDisableInCustomizeMode_11() const { return ___goDisableInCustomizeMode_11; }
	inline Button_t4055032469 ** get_address_of_goDisableInCustomizeMode_11() { return &___goDisableInCustomizeMode_11; }
	inline void set_goDisableInCustomizeMode_11(Button_t4055032469 * value)
	{
		___goDisableInCustomizeMode_11 = value;
		Il2CppCodeGenWriteBarrier((&___goDisableInCustomizeMode_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMIZEFACEEXTENSION_T1668187399_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3300 = { sizeof (XDocumentWrapper_t2289369211), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3301 = { sizeof (XTextWrapper_t1757148629), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3302 = { sizeof (XCommentWrapper_t3809443387), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3303 = { sizeof (XProcessingInstructionWrapper_t4108965662), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3304 = { sizeof (XContainerWrapper_t1262744989), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3304[1] = 
{
	XContainerWrapper_t1262744989::get_offset_of__childNodes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3305 = { sizeof (XObjectWrapper_t2266598274), -1, sizeof(XObjectWrapper_t2266598274_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3305[2] = 
{
	XObjectWrapper_t2266598274_StaticFields::get_offset_of_EmptyChildNodes_0(),
	XObjectWrapper_t2266598274::get_offset_of__xmlObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3306 = { sizeof (XAttributeWrapper_t1656890028), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3307 = { sizeof (XElementWrapper_t2633871282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3307[1] = 
{
	XElementWrapper_t2633871282::get_offset_of__attributes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3308 = { sizeof (XmlNodeConverter_t181348760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3308[3] = 
{
	XmlNodeConverter_t181348760::get_offset_of_U3CDeserializeRootElementNameU3Ek__BackingField_0(),
	XmlNodeConverter_t181348760::get_offset_of_U3CWriteArrayAttributeU3Ek__BackingField_1(),
	XmlNodeConverter_t181348760::get_offset_of_U3COmitRootObjectU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3309 = { sizeof (BsonBinaryType_t2212550840)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3309[8] = 
{
	BsonBinaryType_t2212550840::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3310 = { sizeof (BsonBinaryWriter_t671405346), -1, sizeof(BsonBinaryWriter_t671405346_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3310[4] = 
{
	BsonBinaryWriter_t671405346_StaticFields::get_offset_of_Encoding_0(),
	BsonBinaryWriter_t671405346::get_offset_of__writer_1(),
	BsonBinaryWriter_t671405346::get_offset_of__largeByteBuffer_2(),
	BsonBinaryWriter_t671405346::get_offset_of_U3CDateTimeKindHandlingU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3311 = { sizeof (BsonReader_t26184191), -1, sizeof(BsonReader_t26184191_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3311[14] = 
{
	BsonReader_t26184191_StaticFields::get_offset_of_SeqRange1_15(),
	BsonReader_t26184191_StaticFields::get_offset_of_SeqRange2_16(),
	BsonReader_t26184191_StaticFields::get_offset_of_SeqRange3_17(),
	BsonReader_t26184191_StaticFields::get_offset_of_SeqRange4_18(),
	BsonReader_t26184191::get_offset_of__reader_19(),
	BsonReader_t26184191::get_offset_of__stack_20(),
	BsonReader_t26184191::get_offset_of__byteBuffer_21(),
	BsonReader_t26184191::get_offset_of__charBuffer_22(),
	BsonReader_t26184191::get_offset_of__currentElementType_23(),
	BsonReader_t26184191::get_offset_of__bsonReaderState_24(),
	BsonReader_t26184191::get_offset_of__currentContext_25(),
	BsonReader_t26184191::get_offset_of__readRootValueAsArray_26(),
	BsonReader_t26184191::get_offset_of__jsonNet35BinaryCompatibility_27(),
	BsonReader_t26184191::get_offset_of__dateTimeKindHandling_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3312 = { sizeof (BsonReaderState_t1965113074)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3312[10] = 
{
	BsonReaderState_t1965113074::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3313 = { sizeof (ContainerContext_t1305994539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3313[3] = 
{
	ContainerContext_t1305994539::get_offset_of_Type_0(),
	ContainerContext_t1305994539::get_offset_of_Length_1(),
	ContainerContext_t1305994539::get_offset_of_Position_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3314 = { sizeof (BsonToken_t3838771636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3314[2] = 
{
	BsonToken_t3838771636::get_offset_of_U3CParentU3Ek__BackingField_0(),
	BsonToken_t3838771636::get_offset_of_U3CCalculatedSizeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3315 = { sizeof (BsonObject_t3552490343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3315[1] = 
{
	BsonObject_t3552490343::get_offset_of__children_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3316 = { sizeof (BsonArray_t345506913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3316[1] = 
{
	BsonArray_t345506913::get_offset_of__children_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3317 = { sizeof (BsonValue_t3067495970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3317[2] = 
{
	BsonValue_t3067495970::get_offset_of__value_2(),
	BsonValue_t3067495970::get_offset_of__type_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3318 = { sizeof (BsonString_t1135579766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3318[2] = 
{
	BsonString_t1135579766::get_offset_of_U3CByteCountU3Ek__BackingField_4(),
	BsonString_t1135579766::get_offset_of_U3CIncludeLengthU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3319 = { sizeof (BsonBinary_t160198617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3319[1] = 
{
	BsonBinary_t160198617::get_offset_of_U3CBinaryTypeU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3320 = { sizeof (BsonRegex_t323777446), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3320[2] = 
{
	BsonRegex_t323777446::get_offset_of_U3CPatternU3Ek__BackingField_2(),
	BsonRegex_t323777446::get_offset_of_U3COptionsU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3321 = { sizeof (BsonProperty_t1130223951), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3321[2] = 
{
	BsonProperty_t1130223951::get_offset_of_U3CNameU3Ek__BackingField_0(),
	BsonProperty_t1130223951::get_offset_of_U3CValueU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3322 = { sizeof (BsonType_t1843803290)+ sizeof (RuntimeObject), sizeof(int8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3322[21] = 
{
	BsonType_t1843803290::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3323 = { sizeof (BsonWriter_t3355227794), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3323[4] = 
{
	BsonWriter_t3355227794::get_offset_of__writer_13(),
	BsonWriter_t3355227794::get_offset_of__root_14(),
	BsonWriter_t3355227794::get_offset_of__parent_15(),
	BsonWriter_t3355227794::get_offset_of__propertyName_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3324 = { sizeof (BsonObjectId_t3046626253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3324[1] = 
{
	BsonObjectId_t3046626253::get_offset_of_U3CValueU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3325 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255369), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3325[5] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_U39E31F24F64765FCAA589F589324D17C9FCF6A06D_0(),
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_ADFD2E1C801C825415DD53F4F2F72A13B389313C_1(),
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_D40004AB0E92BF6C8DFE481B56BE3D04ABDA76EB_2(),
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_DD3AEFEADB1CD615F3017763F1568179FEE640B0_3(),
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_E92B39D8233061927D9ACDE54665E68E7535635A_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3326 = { sizeof (__StaticArrayInitTypeSizeU3D10_t1548194903)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D10_t1548194903 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3327 = { sizeof (__StaticArrayInitTypeSizeU3D12_t2710994317)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_t2710994317 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3328 = { sizeof (__StaticArrayInitTypeSizeU3D28_t1904621871)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D28_t1904621871 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3329 = { sizeof (__StaticArrayInitTypeSizeU3D52_t2710732173)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D52_t2710732173 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3330 = { sizeof (U3CModuleU3E_t692745558), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3331 = { sizeof (AnalyticsEvent_t4058973021), -1, sizeof(AnalyticsEvent_t4058973021_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3331[4] = 
{
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of_k_SdkVersion_0(),
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of_m_EventData_1(),
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of__debugMode_2(),
	AnalyticsEvent_t4058973021_StaticFields::get_offset_of_enumRenameTable_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3332 = { sizeof (U3CModuleU3E_t692745559), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3333 = { sizeof (AnalyticsEventTracker_t2285229262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3333[2] = 
{
	AnalyticsEventTracker_t2285229262::get_offset_of_m_Trigger_2(),
	AnalyticsEventTracker_t2285229262::get_offset_of_m_EventPayload_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3334 = { sizeof (U3CTimedTriggerU3Ec__Iterator0_t3813435494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3334[4] = 
{
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24this_0(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24current_1(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24disposing_2(),
	U3CTimedTriggerU3Ec__Iterator0_t3813435494::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3335 = { sizeof (AnalyticsEventTrackerSettings_t480422680), -1, sizeof(AnalyticsEventTrackerSettings_t480422680_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3335[2] = 
{
	AnalyticsEventTrackerSettings_t480422680_StaticFields::get_offset_of_paramCountMax_0(),
	AnalyticsEventTrackerSettings_t480422680_StaticFields::get_offset_of_triggerRuleCountMax_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3336 = { sizeof (AnalyticsEventParam_t2480121928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3336[5] = 
{
	AnalyticsEventParam_t2480121928::get_offset_of_m_RequirementType_0(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_GroupID_1(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Tooltip_2(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Name_3(),
	AnalyticsEventParam_t2480121928::get_offset_of_m_Value_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3337 = { sizeof (RequirementType_t3584265503)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3337[4] = 
{
	RequirementType_t3584265503::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3338 = { sizeof (AnalyticsEventParamListContainer_t587083383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3338[1] = 
{
	AnalyticsEventParamListContainer_t587083383::get_offset_of_m_Parameters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3339 = { sizeof (StandardEventPayload_t1629891255), -1, sizeof(StandardEventPayload_t1629891255_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3339[6] = 
{
	StandardEventPayload_t1629891255::get_offset_of_m_IsEventExpanded_0(),
	StandardEventPayload_t1629891255::get_offset_of_m_StandardEventType_1(),
	StandardEventPayload_t1629891255::get_offset_of_standardEventType_2(),
	StandardEventPayload_t1629891255::get_offset_of_m_Parameters_3(),
	StandardEventPayload_t1629891255_StaticFields::get_offset_of_m_EventData_4(),
	StandardEventPayload_t1629891255::get_offset_of_m_Name_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3340 = { sizeof (TrackableField_t1772682203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3340[3] = 
{
	TrackableField_t1772682203::get_offset_of_m_ValidTypeNames_2(),
	TrackableField_t1772682203::get_offset_of_m_Type_3(),
	TrackableField_t1772682203::get_offset_of_m_EnumType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3341 = { sizeof (TrackablePropertyBase_t2121532948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3341[2] = 
{
	TrackablePropertyBase_t2121532948::get_offset_of_m_Target_0(),
	TrackablePropertyBase_t2121532948::get_offset_of_m_Path_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3342 = { sizeof (ValueProperty_t1868393739), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3342[11] = 
{
	ValueProperty_t1868393739::get_offset_of_m_EditingCustomValue_0(),
	ValueProperty_t1868393739::get_offset_of_m_PopupIndex_1(),
	ValueProperty_t1868393739::get_offset_of_m_CustomValue_2(),
	ValueProperty_t1868393739::get_offset_of_m_FixedType_3(),
	ValueProperty_t1868393739::get_offset_of_m_EnumType_4(),
	ValueProperty_t1868393739::get_offset_of_m_EnumTypeIsCustomizable_5(),
	ValueProperty_t1868393739::get_offset_of_m_CanDisable_6(),
	ValueProperty_t1868393739::get_offset_of_m_PropertyType_7(),
	ValueProperty_t1868393739::get_offset_of_m_ValueType_8(),
	ValueProperty_t1868393739::get_offset_of_m_Value_9(),
	ValueProperty_t1868393739::get_offset_of_m_Target_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3343 = { sizeof (PropertyType_t4040930247)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3343[4] = 
{
	PropertyType_t4040930247::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3344 = { sizeof (AnalyticsTracker_t731021378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3344[5] = 
{
	AnalyticsTracker_t731021378::get_offset_of_m_EventName_2(),
	AnalyticsTracker_t731021378::get_offset_of_m_Dict_3(),
	AnalyticsTracker_t731021378::get_offset_of_m_PrevDictHash_4(),
	AnalyticsTracker_t731021378::get_offset_of_m_TrackableProperty_5(),
	AnalyticsTracker_t731021378::get_offset_of_m_Trigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3345 = { sizeof (Trigger_t4199345191)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3345[8] = 
{
	Trigger_t4199345191::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3346 = { sizeof (TrackableProperty_t3943537984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3346[2] = 
{
	0,
	TrackableProperty_t3943537984::get_offset_of_m_Fields_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3347 = { sizeof (FieldWithTarget_t3058750293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3347[6] = 
{
	FieldWithTarget_t3058750293::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t3058750293::get_offset_of_m_Target_1(),
	FieldWithTarget_t3058750293::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t3058750293::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t3058750293::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t3058750293::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3348 = { sizeof (TriggerBool_t501031542)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3348[4] = 
{
	TriggerBool_t501031542::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3349 = { sizeof (TriggerLifecycleEvent_t3193146760)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3349[9] = 
{
	TriggerLifecycleEvent_t3193146760::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3350 = { sizeof (TriggerOperator_t3611898925)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3350[9] = 
{
	TriggerOperator_t3611898925::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3351 = { sizeof (TriggerType_t105272677)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3351[5] = 
{
	TriggerType_t105272677::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3352 = { sizeof (TriggerListContainer_t2032715483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3352[1] = 
{
	TriggerListContainer_t2032715483::get_offset_of_m_Rules_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3353 = { sizeof (EventTrigger_t2527451695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3353[12] = 
{
	EventTrigger_t2527451695::get_offset_of_m_IsTriggerExpanded_0(),
	EventTrigger_t2527451695::get_offset_of_m_Type_1(),
	EventTrigger_t2527451695::get_offset_of_m_LifecycleEvent_2(),
	EventTrigger_t2527451695::get_offset_of_m_ApplyRules_3(),
	EventTrigger_t2527451695::get_offset_of_m_Rules_4(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerBool_5(),
	EventTrigger_t2527451695::get_offset_of_m_InitTime_6(),
	EventTrigger_t2527451695::get_offset_of_m_RepeatTime_7(),
	EventTrigger_t2527451695::get_offset_of_m_Repetitions_8(),
	EventTrigger_t2527451695::get_offset_of_repetitionCount_9(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerFunction_10(),
	EventTrigger_t2527451695::get_offset_of_m_Method_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3354 = { sizeof (OnTrigger_t4184125570), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3355 = { sizeof (TrackableTrigger_t621205209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3355[2] = 
{
	TrackableTrigger_t621205209::get_offset_of_m_Target_0(),
	TrackableTrigger_t621205209::get_offset_of_m_MethodPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3356 = { sizeof (TriggerMethod_t582536534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3357 = { sizeof (TriggerRule_t1946298321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3357[4] = 
{
	TriggerRule_t1946298321::get_offset_of_m_Target_0(),
	TriggerRule_t1946298321::get_offset_of_m_Operator_1(),
	TriggerRule_t1946298321::get_offset_of_m_Value_2(),
	TriggerRule_t1946298321::get_offset_of_m_Value2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3358 = { sizeof (U3CModuleU3E_t692745560), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3359 = { sizeof (API_KEYS_t4177263602), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3359[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3360 = { sizeof (AnchorAvatar_t2494489176), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3361 = { sizeof (AnchorTheme_t4272755557), -1, sizeof(AnchorTheme_t4272755557_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3361[6] = 
{
	AnchorTheme_t4272755557_StaticFields::get_offset_of_currentTheme_2(),
	AnchorTheme_t4272755557::get_offset_of_ThemeName_3(),
	AnchorTheme_t4272755557::get_offset_of_colorWrld_Highlight_4(),
	AnchorTheme_t4272755557::get_offset_of_goCharacterPrefab_5(),
	AnchorTheme_t4272755557::get_offset_of_Poly_SearchTerm_6(),
	AnchorTheme_t4272755557::get_offset_of_makeMeDefault_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3362 = { sizeof (Hittable_t1494261031), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3362[1] = 
{
	Hittable_t1494261031::get_offset_of_go_Spawn_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3363 = { sizeof (Octopus_t187370362), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3364 = { sizeof (ShowVideo_t3206053106), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3364[2] = 
{
	ShowVideo_t3206053106::get_offset_of_goOnlyInMapMode_2(),
	ShowVideo_t3206053106::get_offset_of_goInRealCamOnly_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3365 = { sizeof (ThemeController_t2603068051), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3365[8] = 
{
	ThemeController_t2603068051::get_offset_of_goUI_ThemeBar_2(),
	ThemeController_t2603068051::get_offset_of_bleach_3(),
	ThemeController_t2603068051::get_offset_of_kuwahara_4(),
	ThemeController_t2603068051::get_offset_of_cb_5(),
	ThemeController_t2603068051::get_offset_of_led_6(),
	ThemeController_t2603068051::get_offset_of_vintage_7(),
	ThemeController_t2603068051::get_offset_of_smh_8(),
	ThemeController_t2603068051::get_offset_of_glitch_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3366 = { sizeof (Zombie_t2596553718), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3367 = { sizeof (CustomizeFaceExtension_t1668187399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3367[2] = 
{
	CustomizeFaceExtension_t1668187399::get_offset_of_goExitCustomizeModeButton_10(),
	CustomizeFaceExtension_t1668187399::get_offset_of_goDisableInCustomizeMode_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3368 = { sizeof (RT_t167452472), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3368[5] = 
{
	RT_t167452472::get_offset_of_pos_0(),
	RT_t167452472::get_offset_of_anchorsMin_1(),
	RT_t167452472::get_offset_of_anchorsMax_2(),
	RT_t167452472::get_offset_of_pivot_3(),
	RT_t167452472::get_offset_of_parent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3369 = { sizeof (PolyController_t1840278131), -1, sizeof(PolyController_t1840278131_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3369[12] = 
{
	PolyController_t1840278131::get_offset_of_search_2(),
	PolyController_t1840278131_StaticFields::get_offset_of_polyMode_3(),
	PolyController_t1840278131::get_offset_of_rt_Settings_4(),
	PolyController_t1840278131::get_offset_of_rtPoly_5(),
	PolyController_t1840278131::get_offset_of_rtGeo_6(),
	PolyController_t1840278131::get_offset_of_goOnlyInPolyMode_7(),
	PolyController_t1840278131::get_offset_of_goOnlyInGeoMode_8(),
	PolyController_t1840278131::get_offset_of_tScrollContent_9(),
	PolyController_t1840278131::get_offset_of_goScrollPrefabButton_10(),
	PolyController_t1840278131_StaticFields::get_offset_of_currentLoaded_11(),
	PolyController_t1840278131_StaticFields::get_offset_of_center_12(),
	PolyController_t1840278131_StaticFields::get_offset_of_lastBorder_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3370 = { sizeof (U3CPullTextureU3Ec__Iterator0_t3483955249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3370[7] = 
{
	U3CPullTextureU3Ec__Iterator0_t3483955249::get_offset_of_url_0(),
	U3CPullTextureU3Ec__Iterator0_t3483955249::get_offset_of_U3CwU3E__0_1(),
	U3CPullTextureU3Ec__Iterator0_t3483955249::get_offset_of_id_2(),
	U3CPullTextureU3Ec__Iterator0_t3483955249::get_offset_of_U24this_3(),
	U3CPullTextureU3Ec__Iterator0_t3483955249::get_offset_of_U24current_4(),
	U3CPullTextureU3Ec__Iterator0_t3483955249::get_offset_of_U24disposing_5(),
	U3CPullTextureU3Ec__Iterator0_t3483955249::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3371 = { sizeof (WRLDPlacerPoly_t2262179182), -1, sizeof(WRLDPlacerPoly_t2262179182_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3371[8] = 
{
	WRLDPlacerPoly_t2262179182_StaticFields::get_offset_of_current_2(),
	WRLDPlacerPoly_t2262179182_StaticFields::get_offset_of_sliderBounds_3(),
	WRLDPlacerPoly_t2262179182_StaticFields::get_offset_of_placingMode_4(),
	WRLDPlacerPoly_t2262179182::get_offset_of_lastHit_5(),
	WRLDPlacerPoly_t2262179182::get_offset_of_goUI_PlacingMode_6(),
	WRLDPlacerPoly_t2262179182_StaticFields::get_offset_of_goUI_PlacingModeStatic_7(),
	WRLDPlacerPoly_t2262179182_StaticFields::get_offset_of_listPlaced_8(),
	WRLDPlacerPoly_t2262179182::get_offset_of_slider_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3372 = { sizeof (Blink_t1044540832), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3372[5] = 
{
	Blink_t1044540832::get_offset_of_rt_2(),
	Blink_t1044540832::get_offset_of_started_3(),
	Blink_t1044540832::get_offset_of_blinkMax_4(),
	Blink_t1044540832::get_offset_of_blinkMin_5(),
	Blink_t1044540832::get_offset_of_colorFinal_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3373 = { sizeof (KillMe_t1031851152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3373[1] = 
{
	KillMe_t1031851152::get_offset_of_secsLeft_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3374 = { sizeof (Pulsate_t1332653514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3374[2] = 
{
	Pulsate_t1332653514::get_offset_of_rt_2(),
	Pulsate_t1332653514::get_offset_of_started_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3375 = { sizeof (ReplayTest_t92159691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3375[3] = 
{
	ReplayTest_t92159691::get_offset_of_goTurnOffInReplayMode_2(),
	ReplayTest_t92159691::get_offset_of_uitoggle_3(),
	ReplayTest_t92159691::get_offset_of_aud_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3376 = { sizeof (U3CWaitForItU3Ec__Iterator0_t684802227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3376[3] = 
{
	U3CWaitForItU3Ec__Iterator0_t684802227::get_offset_of_U24current_0(),
	U3CWaitForItU3Ec__Iterator0_t684802227::get_offset_of_U24disposing_1(),
	U3CWaitForItU3Ec__Iterator0_t684802227::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3377 = { sizeof (ScreenshotSimple_t3378729939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3377[2] = 
{
	ScreenshotSimple_t3378729939::get_offset_of_filename_noextension_2(),
	ScreenshotSimple_t3378729939::get_offset_of_g_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3378 = { sizeof (GetLocation_t3988867825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3378[3] = 
{
	GetLocation_t3988867825::get_offset_of_disablebutton_2(),
	GetLocation_t3988867825::get_offset_of_ll_3(),
	GetLocation_t3988867825::get_offset_of_input_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3379 = { sizeof (U3CLoadItU3Ec__Iterator0_t1028753935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3379[5] = 
{
	U3CLoadItU3Ec__Iterator0_t1028753935::get_offset_of_U3CmaxWaitU3E__0_0(),
	U3CLoadItU3Ec__Iterator0_t1028753935::get_offset_of_U24this_1(),
	U3CLoadItU3Ec__Iterator0_t1028753935::get_offset_of_U24current_2(),
	U3CLoadItU3Ec__Iterator0_t1028753935::get_offset_of_U24disposing_3(),
	U3CLoadItU3Ec__Iterator0_t1028753935::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3380 = { sizeof (GoogleMaps_Response_t2609494023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3380[2] = 
{
	GoogleMaps_Response_t2609494023::get_offset_of_results_0(),
	GoogleMaps_Response_t2609494023::get_offset_of_status_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3381 = { sizeof (GoogleMaps_Result_t3337143104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3381[3] = 
{
	GoogleMaps_Result_t3337143104::get_offset_of_formatted_address_0(),
	GoogleMaps_Result_t3337143104::get_offset_of_geometry_1(),
	GoogleMaps_Result_t3337143104::get_offset_of_place_id_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3382 = { sizeof (GoogleMaps_Geometry_t2263647806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3382[2] = 
{
	GoogleMaps_Geometry_t2263647806::get_offset_of_location_0(),
	GoogleMaps_Geometry_t2263647806::get_offset_of_location_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3383 = { sizeof (GoogleMaps_Location_t573291704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3383[2] = 
{
	GoogleMaps_Location_t573291704::get_offset_of_lat_0(),
	GoogleMaps_Location_t573291704::get_offset_of_lng_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3384 = { sizeof (LoadLocation_t1360453159), -1, sizeof(LoadLocation_t1360453159_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3384[2] = 
{
	LoadLocation_t1360453159::get_offset_of_text_LatLng_2(),
	LoadLocation_t1360453159_StaticFields::get_offset_of_url_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3385 = { sizeof (U3CCallGoogleAPIU3Ec__Iterator0_t221915320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3385[6] = 
{
	U3CCallGoogleAPIU3Ec__Iterator0_t221915320::get_offset_of_address_0(),
	U3CCallGoogleAPIU3Ec__Iterator0_t221915320::get_offset_of_U3CwU3E__0_1(),
	U3CCallGoogleAPIU3Ec__Iterator0_t221915320::get_offset_of_U24this_2(),
	U3CCallGoogleAPIU3Ec__Iterator0_t221915320::get_offset_of_U24current_3(),
	U3CCallGoogleAPIU3Ec__Iterator0_t221915320::get_offset_of_U24disposing_4(),
	U3CCallGoogleAPIU3Ec__Iterator0_t221915320::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3386 = { sizeof (WrldHelper_t4086237380), -1, sizeof(WrldHelper_t4086237380_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3386[7] = 
{
	WrldHelper_t4086237380::get_offset_of_matHighlightDefault_2(),
	WrldHelper_t4086237380_StaticFields::get_offset_of_MatHighlightDefault_static_3(),
	WrldHelper_t4086237380_StaticFields::get_offset_of_HighlightReturn_4(),
	WrldHelper_t4086237380_StaticFields::get_offset_of_lastHighlighted_5(),
	WrldHelper_t4086237380_StaticFields::get_offset_of_currentLatLng_6(),
	WrldHelper_t4086237380_StaticFields::get_offset_of_listHighlighted_7(),
	WrldHelper_t4086237380_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3387 = { sizeof (Highlighter_t666689529), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3388 = { sizeof (Mover_t2250641681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3388[2] = 
{
	Mover_t2250641681::get_offset_of_speed_2(),
	Mover_t2250641681::get_offset_of_MovingDirection_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3389 = { sizeof (MinAttribute_t1755422200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3389[1] = 
{
	MinAttribute_t1755422200::get_offset_of_Min_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3390 = { sizeof (BaseEffect_t1187847871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3390[2] = 
{
	BaseEffect_t1187847871::get_offset_of_Shader_2(),
	BaseEffect_t1187847871::get_offset_of_m_Material_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3391 = { sizeof (CLib_t276046374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3391[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3392 = { sizeof (AnalogTV_t1398652140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3392[11] = 
{
	AnalogTV_t1398652140::get_offset_of_AutomaticPhase_4(),
	AnalogTV_t1398652140::get_offset_of_Phase_5(),
	AnalogTV_t1398652140::get_offset_of_ConvertToGrayscale_6(),
	AnalogTV_t1398652140::get_offset_of_NoiseIntensity_7(),
	AnalogTV_t1398652140::get_offset_of_ScanlinesIntensity_8(),
	AnalogTV_t1398652140::get_offset_of_ScanlinesCount_9(),
	AnalogTV_t1398652140::get_offset_of_ScanlinesOffset_10(),
	AnalogTV_t1398652140::get_offset_of_VerticalScanlines_11(),
	AnalogTV_t1398652140::get_offset_of_Distortion_12(),
	AnalogTV_t1398652140::get_offset_of_CubicDistortion_13(),
	AnalogTV_t1398652140::get_offset_of_Scale_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3393 = { sizeof (BilateralGaussianBlur_t938878669), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3393[3] = 
{
	BilateralGaussianBlur_t938878669::get_offset_of_Passes_4(),
	BilateralGaussianBlur_t938878669::get_offset_of_Threshold_5(),
	BilateralGaussianBlur_t938878669::get_offset_of_Amount_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3394 = { sizeof (BleachBypass_t2568391998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3394[1] = 
{
	BleachBypass_t2568391998::get_offset_of_Amount_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3395 = { sizeof (Blend_t936097170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3395[3] = 
{
	Blend_t936097170::get_offset_of_Texture_4(),
	Blend_t936097170::get_offset_of_Amount_5(),
	Blend_t936097170::get_offset_of_Mode_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3396 = { sizeof (BlendingMode_t1651387113)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3396[22] = 
{
	BlendingMode_t1651387113::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3397 = { sizeof (BrightnessContrastGamma_t158109514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3397[4] = 
{
	BrightnessContrastGamma_t158109514::get_offset_of_Brightness_4(),
	BrightnessContrastGamma_t158109514::get_offset_of_Contrast_5(),
	BrightnessContrastGamma_t158109514::get_offset_of_ContrastCoeff_6(),
	BrightnessContrastGamma_t158109514::get_offset_of_Gamma_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3398 = { sizeof (ChannelClamper_t4227510363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3398[3] = 
{
	ChannelClamper_t4227510363::get_offset_of_Red_4(),
	ChannelClamper_t4227510363::get_offset_of_Green_5(),
	ChannelClamper_t4227510363::get_offset_of_Blue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3399 = { sizeof (ChannelMixer_t2404774860), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3399[4] = 
{
	ChannelMixer_t2404774860::get_offset_of_Red_4(),
	ChannelMixer_t2404774860::get_offset_of_Green_5(),
	ChannelMixer_t2404774860::get_offset_of_Blue_6(),
	ChannelMixer_t2404774860::get_offset_of_Constant_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
