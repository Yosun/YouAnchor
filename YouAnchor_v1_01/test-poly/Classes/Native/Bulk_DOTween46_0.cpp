﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.Camera
struct Camera_t4157153871;
// DG.Tweening.Tweener
struct Tweener_t436044680;
// UnityEngine.CanvasGroup
struct CanvasGroup_t4083511760;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass0_0
struct U3CU3Ec__DisplayClass0_0_t892042862;
// DG.Tweening.Core.DOGetter`1<System.Single>
struct DOGetter_1_t1140062978;
// DG.Tweening.Core.DOSetter`1<System.Single>
struct DOSetter_1_t2447375106;
// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct TweenerCore_3_t884556411;
// UnityEngine.UI.Image
struct Image_t2670269651;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_t1295327389;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Color>
struct DOGetter_1_t2298482528;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Color>
struct DOSetter_1_t3605794656;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct TweenerCore_3_t745532282;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_t2861411330;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass13_0
struct U3CU3Ec__DisplayClass13_0_t2635598378;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>
struct DOGetter_1_t1899025727;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>
struct DOSetter_1_t3206337855;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t4006488229;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass16_0
struct U3CU3Ec__DisplayClass16_0_t1443957290;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>
struct DOGetter_1_t3465109668;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>
struct DOSetter_1_t477454500;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t2944330537;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass22_0
struct U3CU3Ec__DisplayClass22_0_t2245367183;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass23_0
struct U3CU3Ec__DisplayClass23_0_t4201682319;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>
struct TweenerCore_3_t951858585;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass25_0
struct U3CU3Ec__DisplayClass25_0_t289052047;
// UnityEngine.UI.Text
struct Text_t1901882714;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass31_0
struct U3CU3Ec__DisplayClass31_0_t1090461940;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass32_0
struct U3CU3Ec__DisplayClass32_0_t3811451124;
// System.String
struct String_t;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass33_0
struct U3CU3Ec__DisplayClass33_0_t1472798964;
// DG.Tweening.Core.DOGetter`1<System.String>
struct DOGetter_1_t1590246893;
// DG.Tweening.Core.DOGetter`1<System.Object>
struct DOGetter_1_t2822902368;
// DG.Tweening.Core.DOSetter`1<System.String>
struct DOSetter_1_t2897559021;
// DG.Tweening.Core.DOSetter`1<System.Object>
struct DOSetter_1_t4130214496;
// DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>
struct TweenerCore_3_t2915539799;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// DG.Tweening.TweenCallback
struct TweenCallback_t3727756325;
// DG.Tweening.TweenCallback`1<System.Int32>
struct TweenCallback_1_t3009965658;
// DG.Tweening.EaseFunction
struct EaseFunction_t3531141372;
// System.Type
struct Type_t;
// DG.Tweening.Sequence
struct Sequence_t2050373119;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1258266594;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t190067161;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>
struct ABSTweenPlugin_3_t487868235;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct ABSTweenPlugin_3_t281541932;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct ABSTweenPlugin_3_t420566061;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>
struct ABSTweenPlugin_3_t2451549449;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct ABSTweenPlugin_3_t2480340187;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct ABSTweenPlugin_3_t3542497879;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UnityEngine.UI.FontData
struct FontData_t746620069;
// UnityEngine.TextGenerator
struct TextGenerator_t3211863866;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;

extern RuntimeClass* RectTransformUtility_t1743242446_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern const uint32_t DOTweenUtils46_SwitchToRectTransform_m3891122406_MetadataUsageId;
extern RuntimeClass* U3CU3Ec__DisplayClass0_0_t892042862_il2cpp_TypeInfo_var;
extern RuntimeClass* DOGetter_1_t1140062978_il2cpp_TypeInfo_var;
extern RuntimeClass* DOSetter_1_t2447375106_il2cpp_TypeInfo_var;
extern RuntimeClass* DOTween_t2744875806_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m1136460969_RuntimeMethod_var;
extern const RuntimeMethod* DOGetter_1__ctor_m2335505072_RuntimeMethod_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_m737149533_RuntimeMethod_var;
extern const RuntimeMethod* DOSetter_1__ctor_m3423399374_RuntimeMethod_var;
extern const RuntimeMethod* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t884556411_m1318424630_RuntimeMethod_var;
extern const uint32_t ShortcutExtensions46_DOFade_m3189841635_MetadataUsageId;
extern RuntimeClass* U3CU3Ec__DisplayClass3_0_t1295327389_il2cpp_TypeInfo_var;
extern RuntimeClass* DOGetter_1_t2298482528_il2cpp_TypeInfo_var;
extern RuntimeClass* DOSetter_1_t3605794656_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_m2988152837_RuntimeMethod_var;
extern const RuntimeMethod* DOGetter_1__ctor_m1558284636_RuntimeMethod_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_m1674238268_RuntimeMethod_var;
extern const RuntimeMethod* DOSetter_1__ctor_m380718107_RuntimeMethod_var;
extern const RuntimeMethod* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t745532282_m2904780857_RuntimeMethod_var;
extern const uint32_t ShortcutExtensions46_DOColor_m3633025588_MetadataUsageId;
extern RuntimeClass* U3CU3Ec__DisplayClass4_0_t2861411330_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m4078431796_RuntimeMethod_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m1839193484_RuntimeMethod_var;
extern const RuntimeMethod* TweenSettingsExtensions_SetTarget_TisTweener_t436044680_m598017345_RuntimeMethod_var;
extern const uint32_t ShortcutExtensions46_DOFade_m4177432048_MetadataUsageId;
extern RuntimeClass* U3CU3Ec__DisplayClass13_0_t2635598378_il2cpp_TypeInfo_var;
extern RuntimeClass* DOGetter_1_t1899025727_il2cpp_TypeInfo_var;
extern RuntimeClass* DOSetter_1_t3206337855_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__0_m4202884669_RuntimeMethod_var;
extern const RuntimeMethod* DOGetter_1__ctor_m3327751853_RuntimeMethod_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__1_m58758457_RuntimeMethod_var;
extern const RuntimeMethod* DOSetter_1__ctor_m2986952954_RuntimeMethod_var;
extern const uint32_t ShortcutExtensions46_DOAnchorPos_m2186168858_MetadataUsageId;
extern RuntimeClass* U3CU3Ec__DisplayClass16_0_t1443957290_il2cpp_TypeInfo_var;
extern RuntimeClass* DOGetter_1_t3465109668_il2cpp_TypeInfo_var;
extern RuntimeClass* DOSetter_1_t477454500_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_m1327669346_RuntimeMethod_var;
extern const RuntimeMethod* DOGetter_1__ctor_m3773977789_RuntimeMethod_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_m3318326923_RuntimeMethod_var;
extern const RuntimeMethod* DOSetter_1__ctor_m4237574066_RuntimeMethod_var;
extern const uint32_t ShortcutExtensions46_DOAnchorPos3D_m2494478347_MetadataUsageId;
extern RuntimeClass* U3CU3Ec__DisplayClass22_0_t2245367183_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass22_0_U3CDOSizeDeltaU3Eb__0_m194433554_RuntimeMethod_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass22_0_U3CDOSizeDeltaU3Eb__1_m1120729751_RuntimeMethod_var;
extern const uint32_t ShortcutExtensions46_DOSizeDelta_m959638426_MetadataUsageId;
extern RuntimeClass* U3CU3Ec__DisplayClass23_0_t4201682319_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__0_m2475830852_RuntimeMethod_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__1_m2684855292_RuntimeMethod_var;
extern const RuntimeMethod* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t951858585_m2980331507_RuntimeMethod_var;
extern const uint32_t ShortcutExtensions46_DOPunchAnchorPos_m1059198467_MetadataUsageId;
extern RuntimeClass* U3CU3Ec__DisplayClass25_0_t289052047_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__0_m290566680_RuntimeMethod_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__1_m2933663977_RuntimeMethod_var;
extern const RuntimeMethod* Extensions_SetSpecialStartupMode_TisTweenerCore_3_t951858585_m1835023067_RuntimeMethod_var;
extern const uint32_t ShortcutExtensions46_DOShakeAnchorPos_m1729623556_MetadataUsageId;
extern RuntimeClass* U3CU3Ec__DisplayClass31_0_t1090461940_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass31_0_U3CDOColorU3Eb__0_m1133203489_RuntimeMethod_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass31_0_U3CDOColorU3Eb__1_m2068016229_RuntimeMethod_var;
extern const uint32_t ShortcutExtensions46_DOColor_m1801377730_MetadataUsageId;
extern RuntimeClass* U3CU3Ec__DisplayClass32_0_t3811451124_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass32_0_U3CDOFadeU3Eb__0_m36854516_RuntimeMethod_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass32_0_U3CDOFadeU3Eb__1_m3310679632_RuntimeMethod_var;
extern const uint32_t ShortcutExtensions46_DOFade_m1784703499_MetadataUsageId;
extern RuntimeClass* U3CU3Ec__DisplayClass33_0_t1472798964_il2cpp_TypeInfo_var;
extern RuntimeClass* DOGetter_1_t1590246893_il2cpp_TypeInfo_var;
extern RuntimeClass* DOSetter_1_t2897559021_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass33_0_U3CDOTextU3Eb__0_m3254169216_RuntimeMethod_var;
extern const RuntimeMethod* DOGetter_1__ctor_m2531950179_RuntimeMethod_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass33_0_U3CDOTextU3Eb__1_m2475978_RuntimeMethod_var;
extern const RuntimeMethod* DOSetter_1__ctor_m932311154_RuntimeMethod_var;
extern const uint32_t ShortcutExtensions46_DOText_m1289017753_MetadataUsageId;
extern const uint32_t U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__0_m2475830852_MetadataUsageId;
extern const uint32_t U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__1_m2684855292_MetadataUsageId;
extern const uint32_t U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__0_m290566680_MetadataUsageId;
extern const uint32_t U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__1_m2933663977_MetadataUsageId;



#ifndef U3CMODULEU3E_T692745555_H
#define U3CMODULEU3E_T692745555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745555 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745555_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef SHORTCUTEXTENSIONS46_T3362526588_H
#define SHORTCUTEXTENSIONS46_T3362526588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46
struct  ShortcutExtensions46_t3362526588  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHORTCUTEXTENSIONS46_T3362526588_H
#ifndef U3CU3EC__DISPLAYCLASS23_0_T4201682319_H
#define U3CU3EC__DISPLAYCLASS23_0_T4201682319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass23_0
struct  U3CU3Ec__DisplayClass23_0_t4201682319  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.ShortcutExtensions46/<>c__DisplayClass23_0::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass23_0_t4201682319, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS23_0_T4201682319_H
#ifndef U3CU3EC__DISPLAYCLASS22_0_T2245367183_H
#define U3CU3EC__DISPLAYCLASS22_0_T2245367183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass22_0
struct  U3CU3Ec__DisplayClass22_0_t2245367183  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.ShortcutExtensions46/<>c__DisplayClass22_0::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t2245367183, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS22_0_T2245367183_H
#ifndef U3CU3EC__DISPLAYCLASS0_0_T892042862_H
#define U3CU3EC__DISPLAYCLASS0_0_T892042862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_t892042862  : public RuntimeObject
{
public:
	// UnityEngine.CanvasGroup DG.Tweening.ShortcutExtensions46/<>c__DisplayClass0_0::target
	CanvasGroup_t4083511760 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t892042862, ___target_0)); }
	inline CanvasGroup_t4083511760 * get_target_0() const { return ___target_0; }
	inline CanvasGroup_t4083511760 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(CanvasGroup_t4083511760 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS0_0_T892042862_H
#ifndef U3CU3EC__DISPLAYCLASS16_0_T1443957290_H
#define U3CU3EC__DISPLAYCLASS16_0_T1443957290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass16_0
struct  U3CU3Ec__DisplayClass16_0_t1443957290  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.ShortcutExtensions46/<>c__DisplayClass16_0::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_t1443957290, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS16_0_T1443957290_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_T2635598378_H
#define U3CU3EC__DISPLAYCLASS13_0_T2635598378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_t2635598378  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.ShortcutExtensions46/<>c__DisplayClass13_0::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t2635598378, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_T2635598378_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T2861411330_H
#define U3CU3EC__DISPLAYCLASS4_0_T2861411330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t2861411330  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::target
	Image_t2670269651 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t2861411330, ___target_0)); }
	inline Image_t2670269651 * get_target_0() const { return ___target_0; }
	inline Image_t2670269651 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Image_t2670269651 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T2861411330_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T1295327389_H
#define U3CU3EC__DISPLAYCLASS3_0_T1295327389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t1295327389  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image DG.Tweening.ShortcutExtensions46/<>c__DisplayClass3_0::target
	Image_t2670269651 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t1295327389, ___target_0)); }
	inline Image_t2670269651 * get_target_0() const { return ___target_0; }
	inline Image_t2670269651 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Image_t2670269651 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T1295327389_H
#ifndef U3CU3EC__DISPLAYCLASS25_0_T289052047_H
#define U3CU3EC__DISPLAYCLASS25_0_T289052047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass25_0
struct  U3CU3Ec__DisplayClass25_0_t289052047  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.ShortcutExtensions46/<>c__DisplayClass25_0::target
	RectTransform_t3704657025 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass25_0_t289052047, ___target_0)); }
	inline RectTransform_t3704657025 * get_target_0() const { return ___target_0; }
	inline RectTransform_t3704657025 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t3704657025 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS25_0_T289052047_H
#ifndef DOTWEENUTILS46_T543860410_H
#define DOTWEENUTILS46_T543860410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTweenUtils46
struct  DOTweenUtils46_t543860410  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENUTILS46_T543860410_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CU3EC__DISPLAYCLASS33_0_T1472798964_H
#define U3CU3EC__DISPLAYCLASS33_0_T1472798964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass33_0
struct  U3CU3Ec__DisplayClass33_0_t1472798964  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text DG.Tweening.ShortcutExtensions46/<>c__DisplayClass33_0::target
	Text_t1901882714 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t1472798964, ___target_0)); }
	inline Text_t1901882714 * get_target_0() const { return ___target_0; }
	inline Text_t1901882714 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Text_t1901882714 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS33_0_T1472798964_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef U3CU3EC__DISPLAYCLASS31_0_T1090461940_H
#define U3CU3EC__DISPLAYCLASS31_0_T1090461940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass31_0
struct  U3CU3Ec__DisplayClass31_0_t1090461940  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text DG.Tweening.ShortcutExtensions46/<>c__DisplayClass31_0::target
	Text_t1901882714 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_0_t1090461940, ___target_0)); }
	inline Text_t1901882714 * get_target_0() const { return ___target_0; }
	inline Text_t1901882714 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Text_t1901882714 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS31_0_T1090461940_H
#ifndef U3CU3EC__DISPLAYCLASS32_0_T3811451124_H
#define U3CU3EC__DISPLAYCLASS32_0_T3811451124_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass32_0
struct  U3CU3Ec__DisplayClass32_0_t3811451124  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text DG.Tweening.ShortcutExtensions46/<>c__DisplayClass32_0::target
	Text_t1901882714 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass32_0_t3811451124, ___target_0)); }
	inline Text_t1901882714 * get_target_0() const { return ___target_0; }
	inline Text_t1901882714 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Text_t1901882714 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS32_0_T3811451124_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef COLOROPTIONS_T1487297155_H
#define COLOROPTIONS_T1487297155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.ColorOptions
struct  ColorOptions_t1487297155 
{
public:
	// System.Boolean DG.Tweening.Plugins.Options.ColorOptions::alphaOnly
	bool ___alphaOnly_0;

public:
	inline static int32_t get_offset_of_alphaOnly_0() { return static_cast<int32_t>(offsetof(ColorOptions_t1487297155, ___alphaOnly_0)); }
	inline bool get_alphaOnly_0() const { return ___alphaOnly_0; }
	inline bool* get_address_of_alphaOnly_0() { return &___alphaOnly_0; }
	inline void set_alphaOnly_0(bool value)
	{
		___alphaOnly_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.ColorOptions
struct ColorOptions_t1487297155_marshaled_pinvoke
{
	int32_t ___alphaOnly_0;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.ColorOptions
struct ColorOptions_t1487297155_marshaled_com
{
	int32_t ___alphaOnly_0;
};
#endif // COLOROPTIONS_T1487297155_H
#ifndef FLOATOPTIONS_T1203667100_H
#define FLOATOPTIONS_T1203667100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.FloatOptions
struct  FloatOptions_t1203667100 
{
public:
	// System.Boolean DG.Tweening.Plugins.Options.FloatOptions::snapping
	bool ___snapping_0;

public:
	inline static int32_t get_offset_of_snapping_0() { return static_cast<int32_t>(offsetof(FloatOptions_t1203667100, ___snapping_0)); }
	inline bool get_snapping_0() const { return ___snapping_0; }
	inline bool* get_address_of_snapping_0() { return &___snapping_0; }
	inline void set_snapping_0(bool value)
	{
		___snapping_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.FloatOptions
struct FloatOptions_t1203667100_marshaled_pinvoke
{
	int32_t ___snapping_0;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.FloatOptions
struct FloatOptions_t1203667100_marshaled_com
{
	int32_t ___snapping_0;
};
#endif // FLOATOPTIONS_T1203667100_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef SCRAMBLEMODE_T1285273342_H
#define SCRAMBLEMODE_T1285273342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ScrambleMode
struct  ScrambleMode_t1285273342 
{
public:
	// System.Int32 DG.Tweening.ScrambleMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScrambleMode_t1285273342, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRAMBLEMODE_T1285273342_H
#ifndef TWEENTYPE_T1971673186_H
#define TWEENTYPE_T1971673186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.TweenType
struct  TweenType_t1971673186 
{
public:
	// System.Int32 DG.Tweening.TweenType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TweenType_t1971673186, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENTYPE_T1971673186_H
#ifndef AXISCONSTRAINT_T2771958344_H
#define AXISCONSTRAINT_T2771958344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.AxisConstraint
struct  AxisConstraint_t2771958344 
{
public:
	// System.Int32 DG.Tweening.AxisConstraint::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisConstraint_t2771958344, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCONSTRAINT_T2771958344_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef EASE_T4010715394_H
#define EASE_T4010715394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Ease
struct  Ease_t4010715394 
{
public:
	// System.Int32 DG.Tweening.Ease::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Ease_t4010715394, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASE_T4010715394_H
#ifndef LOOPTYPE_T3049802818_H
#define LOOPTYPE_T3049802818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LoopType
struct  LoopType_t3049802818 
{
public:
	// System.Int32 DG.Tweening.LoopType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoopType_t3049802818, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T3049802818_H
#ifndef UPDATETYPE_T3937729206_H
#define UPDATETYPE_T3937729206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.UpdateType
struct  UpdateType_t3937729206 
{
public:
	// System.Int32 DG.Tweening.UpdateType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdateType_t3937729206, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATETYPE_T3937729206_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef FILLMETHOD_T1167457570_H
#define FILLMETHOD_T1167457570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/FillMethod
struct  FillMethod_t1167457570 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillMethod_t1167457570, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T1167457570_H
#ifndef TYPE_T1152881528_H
#define TYPE_T1152881528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/Type
struct  Type_t1152881528 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t1152881528, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T1152881528_H
#ifndef SPECIALSTARTUPMODE_T1644068939_H
#define SPECIALSTARTUPMODE_T1644068939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Enums.SpecialStartupMode
struct  SpecialStartupMode_t1644068939 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.SpecialStartupMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SpecialStartupMode_t1644068939, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALSTARTUPMODE_T1644068939_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef VECTOROPTIONS_T1354903650_H
#define VECTOROPTIONS_T1354903650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.VectorOptions
struct  VectorOptions_t1354903650 
{
public:
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.VectorOptions::axisConstraint
	int32_t ___axisConstraint_0;
	// System.Boolean DG.Tweening.Plugins.Options.VectorOptions::snapping
	bool ___snapping_1;

public:
	inline static int32_t get_offset_of_axisConstraint_0() { return static_cast<int32_t>(offsetof(VectorOptions_t1354903650, ___axisConstraint_0)); }
	inline int32_t get_axisConstraint_0() const { return ___axisConstraint_0; }
	inline int32_t* get_address_of_axisConstraint_0() { return &___axisConstraint_0; }
	inline void set_axisConstraint_0(int32_t value)
	{
		___axisConstraint_0 = value;
	}

	inline static int32_t get_offset_of_snapping_1() { return static_cast<int32_t>(offsetof(VectorOptions_t1354903650, ___snapping_1)); }
	inline bool get_snapping_1() const { return ___snapping_1; }
	inline bool* get_address_of_snapping_1() { return &___snapping_1; }
	inline void set_snapping_1(bool value)
	{
		___snapping_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.VectorOptions
struct VectorOptions_t1354903650_marshaled_pinvoke
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.VectorOptions
struct VectorOptions_t1354903650_marshaled_com
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
};
#endif // VECTOROPTIONS_T1354903650_H
#ifndef VECTOR3ARRAYOPTIONS_T534739431_H
#define VECTOR3ARRAYOPTIONS_T534739431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.Vector3ArrayOptions
struct  Vector3ArrayOptions_t534739431 
{
public:
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.Vector3ArrayOptions::axisConstraint
	int32_t ___axisConstraint_0;
	// System.Boolean DG.Tweening.Plugins.Options.Vector3ArrayOptions::snapping
	bool ___snapping_1;
	// System.Single[] DG.Tweening.Plugins.Options.Vector3ArrayOptions::durations
	SingleU5BU5D_t1444911251* ___durations_2;

public:
	inline static int32_t get_offset_of_axisConstraint_0() { return static_cast<int32_t>(offsetof(Vector3ArrayOptions_t534739431, ___axisConstraint_0)); }
	inline int32_t get_axisConstraint_0() const { return ___axisConstraint_0; }
	inline int32_t* get_address_of_axisConstraint_0() { return &___axisConstraint_0; }
	inline void set_axisConstraint_0(int32_t value)
	{
		___axisConstraint_0 = value;
	}

	inline static int32_t get_offset_of_snapping_1() { return static_cast<int32_t>(offsetof(Vector3ArrayOptions_t534739431, ___snapping_1)); }
	inline bool get_snapping_1() const { return ___snapping_1; }
	inline bool* get_address_of_snapping_1() { return &___snapping_1; }
	inline void set_snapping_1(bool value)
	{
		___snapping_1 = value;
	}

	inline static int32_t get_offset_of_durations_2() { return static_cast<int32_t>(offsetof(Vector3ArrayOptions_t534739431, ___durations_2)); }
	inline SingleU5BU5D_t1444911251* get_durations_2() const { return ___durations_2; }
	inline SingleU5BU5D_t1444911251** get_address_of_durations_2() { return &___durations_2; }
	inline void set_durations_2(SingleU5BU5D_t1444911251* value)
	{
		___durations_2 = value;
		Il2CppCodeGenWriteBarrier((&___durations_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.Vector3ArrayOptions
struct Vector3ArrayOptions_t534739431_marshaled_pinvoke
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
	float* ___durations_2;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.Vector3ArrayOptions
struct Vector3ArrayOptions_t534739431_marshaled_com
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
	float* ___durations_2;
};
#endif // VECTOR3ARRAYOPTIONS_T534739431_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef STRINGOPTIONS_T3992490940_H
#define STRINGOPTIONS_T3992490940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.StringOptions
struct  StringOptions_t3992490940 
{
public:
	// System.Boolean DG.Tweening.Plugins.Options.StringOptions::richTextEnabled
	bool ___richTextEnabled_0;
	// DG.Tweening.ScrambleMode DG.Tweening.Plugins.Options.StringOptions::scrambleMode
	int32_t ___scrambleMode_1;
	// System.Char[] DG.Tweening.Plugins.Options.StringOptions::scrambledChars
	CharU5BU5D_t3528271667* ___scrambledChars_2;
	// System.Int32 DG.Tweening.Plugins.Options.StringOptions::startValueStrippedLength
	int32_t ___startValueStrippedLength_3;
	// System.Int32 DG.Tweening.Plugins.Options.StringOptions::changeValueStrippedLength
	int32_t ___changeValueStrippedLength_4;

public:
	inline static int32_t get_offset_of_richTextEnabled_0() { return static_cast<int32_t>(offsetof(StringOptions_t3992490940, ___richTextEnabled_0)); }
	inline bool get_richTextEnabled_0() const { return ___richTextEnabled_0; }
	inline bool* get_address_of_richTextEnabled_0() { return &___richTextEnabled_0; }
	inline void set_richTextEnabled_0(bool value)
	{
		___richTextEnabled_0 = value;
	}

	inline static int32_t get_offset_of_scrambleMode_1() { return static_cast<int32_t>(offsetof(StringOptions_t3992490940, ___scrambleMode_1)); }
	inline int32_t get_scrambleMode_1() const { return ___scrambleMode_1; }
	inline int32_t* get_address_of_scrambleMode_1() { return &___scrambleMode_1; }
	inline void set_scrambleMode_1(int32_t value)
	{
		___scrambleMode_1 = value;
	}

	inline static int32_t get_offset_of_scrambledChars_2() { return static_cast<int32_t>(offsetof(StringOptions_t3992490940, ___scrambledChars_2)); }
	inline CharU5BU5D_t3528271667* get_scrambledChars_2() const { return ___scrambledChars_2; }
	inline CharU5BU5D_t3528271667** get_address_of_scrambledChars_2() { return &___scrambledChars_2; }
	inline void set_scrambledChars_2(CharU5BU5D_t3528271667* value)
	{
		___scrambledChars_2 = value;
		Il2CppCodeGenWriteBarrier((&___scrambledChars_2), value);
	}

	inline static int32_t get_offset_of_startValueStrippedLength_3() { return static_cast<int32_t>(offsetof(StringOptions_t3992490940, ___startValueStrippedLength_3)); }
	inline int32_t get_startValueStrippedLength_3() const { return ___startValueStrippedLength_3; }
	inline int32_t* get_address_of_startValueStrippedLength_3() { return &___startValueStrippedLength_3; }
	inline void set_startValueStrippedLength_3(int32_t value)
	{
		___startValueStrippedLength_3 = value;
	}

	inline static int32_t get_offset_of_changeValueStrippedLength_4() { return static_cast<int32_t>(offsetof(StringOptions_t3992490940, ___changeValueStrippedLength_4)); }
	inline int32_t get_changeValueStrippedLength_4() const { return ___changeValueStrippedLength_4; }
	inline int32_t* get_address_of_changeValueStrippedLength_4() { return &___changeValueStrippedLength_4; }
	inline void set_changeValueStrippedLength_4(int32_t value)
	{
		___changeValueStrippedLength_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.StringOptions
struct StringOptions_t3992490940_marshaled_pinvoke
{
	int32_t ___richTextEnabled_0;
	int32_t ___scrambleMode_1;
	uint8_t* ___scrambledChars_2;
	int32_t ___startValueStrippedLength_3;
	int32_t ___changeValueStrippedLength_4;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.StringOptions
struct StringOptions_t3992490940_marshaled_com
{
	int32_t ___richTextEnabled_0;
	int32_t ___scrambleMode_1;
	uint8_t* ___scrambledChars_2;
	int32_t ___startValueStrippedLength_3;
	int32_t ___changeValueStrippedLength_4;
};
#endif // STRINGOPTIONS_T3992490940_H
#ifndef ABSSEQUENTIABLE_T3376041011_H
#define ABSSEQUENTIABLE_T3376041011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.ABSSequentiable
struct  ABSSequentiable_t3376041011  : public RuntimeObject
{
public:
	// DG.Tweening.TweenType DG.Tweening.Core.ABSSequentiable::tweenType
	int32_t ___tweenType_0;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedPosition
	float ___sequencedPosition_1;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedEndPosition
	float ___sequencedEndPosition_2;
	// DG.Tweening.TweenCallback DG.Tweening.Core.ABSSequentiable::onStart
	TweenCallback_t3727756325 * ___onStart_3;

public:
	inline static int32_t get_offset_of_tweenType_0() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___tweenType_0)); }
	inline int32_t get_tweenType_0() const { return ___tweenType_0; }
	inline int32_t* get_address_of_tweenType_0() { return &___tweenType_0; }
	inline void set_tweenType_0(int32_t value)
	{
		___tweenType_0 = value;
	}

	inline static int32_t get_offset_of_sequencedPosition_1() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___sequencedPosition_1)); }
	inline float get_sequencedPosition_1() const { return ___sequencedPosition_1; }
	inline float* get_address_of_sequencedPosition_1() { return &___sequencedPosition_1; }
	inline void set_sequencedPosition_1(float value)
	{
		___sequencedPosition_1 = value;
	}

	inline static int32_t get_offset_of_sequencedEndPosition_2() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___sequencedEndPosition_2)); }
	inline float get_sequencedEndPosition_2() const { return ___sequencedEndPosition_2; }
	inline float* get_address_of_sequencedEndPosition_2() { return &___sequencedEndPosition_2; }
	inline void set_sequencedEndPosition_2(float value)
	{
		___sequencedEndPosition_2 = value;
	}

	inline static int32_t get_offset_of_onStart_3() { return static_cast<int32_t>(offsetof(ABSSequentiable_t3376041011, ___onStart_3)); }
	inline TweenCallback_t3727756325 * get_onStart_3() const { return ___onStart_3; }
	inline TweenCallback_t3727756325 ** get_address_of_onStart_3() { return &___onStart_3; }
	inline void set_onStart_3(TweenCallback_t3727756325 * value)
	{
		___onStart_3 = value;
		Il2CppCodeGenWriteBarrier((&___onStart_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSSEQUENTIABLE_T3376041011_H
#ifndef TWEEN_T2342918553_H
#define TWEEN_T2342918553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Tween
struct  Tween_t2342918553  : public ABSSequentiable_t3376041011
{
public:
	// System.Single DG.Tweening.Tween::timeScale
	float ___timeScale_4;
	// System.Boolean DG.Tweening.Tween::isBackwards
	bool ___isBackwards_5;
	// System.Object DG.Tweening.Tween::id
	RuntimeObject * ___id_6;
	// System.Object DG.Tweening.Tween::target
	RuntimeObject * ___target_7;
	// DG.Tweening.UpdateType DG.Tweening.Tween::updateType
	int32_t ___updateType_8;
	// System.Boolean DG.Tweening.Tween::isIndependentUpdate
	bool ___isIndependentUpdate_9;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPlay
	TweenCallback_t3727756325 * ___onPlay_10;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPause
	TweenCallback_t3727756325 * ___onPause_11;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onRewind
	TweenCallback_t3727756325 * ___onRewind_12;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onUpdate
	TweenCallback_t3727756325 * ___onUpdate_13;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onStepComplete
	TweenCallback_t3727756325 * ___onStepComplete_14;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onComplete
	TweenCallback_t3727756325 * ___onComplete_15;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onKill
	TweenCallback_t3727756325 * ___onKill_16;
	// DG.Tweening.TweenCallback`1<System.Int32> DG.Tweening.Tween::onWaypointChange
	TweenCallback_1_t3009965658 * ___onWaypointChange_17;
	// System.Boolean DG.Tweening.Tween::isFrom
	bool ___isFrom_18;
	// System.Boolean DG.Tweening.Tween::isBlendable
	bool ___isBlendable_19;
	// System.Boolean DG.Tweening.Tween::isRecyclable
	bool ___isRecyclable_20;
	// System.Boolean DG.Tweening.Tween::isSpeedBased
	bool ___isSpeedBased_21;
	// System.Boolean DG.Tweening.Tween::autoKill
	bool ___autoKill_22;
	// System.Single DG.Tweening.Tween::duration
	float ___duration_23;
	// System.Int32 DG.Tweening.Tween::loops
	int32_t ___loops_24;
	// DG.Tweening.LoopType DG.Tweening.Tween::loopType
	int32_t ___loopType_25;
	// System.Single DG.Tweening.Tween::delay
	float ___delay_26;
	// System.Boolean DG.Tweening.Tween::isRelative
	bool ___isRelative_27;
	// DG.Tweening.Ease DG.Tweening.Tween::easeType
	int32_t ___easeType_28;
	// DG.Tweening.EaseFunction DG.Tweening.Tween::customEase
	EaseFunction_t3531141372 * ___customEase_29;
	// System.Single DG.Tweening.Tween::easeOvershootOrAmplitude
	float ___easeOvershootOrAmplitude_30;
	// System.Single DG.Tweening.Tween::easePeriod
	float ___easePeriod_31;
	// System.Type DG.Tweening.Tween::typeofT1
	Type_t * ___typeofT1_32;
	// System.Type DG.Tweening.Tween::typeofT2
	Type_t * ___typeofT2_33;
	// System.Type DG.Tweening.Tween::typeofTPlugOptions
	Type_t * ___typeofTPlugOptions_34;
	// System.Boolean DG.Tweening.Tween::active
	bool ___active_35;
	// System.Boolean DG.Tweening.Tween::isSequenced
	bool ___isSequenced_36;
	// DG.Tweening.Sequence DG.Tweening.Tween::sequenceParent
	Sequence_t2050373119 * ___sequenceParent_37;
	// System.Int32 DG.Tweening.Tween::activeId
	int32_t ___activeId_38;
	// DG.Tweening.Core.Enums.SpecialStartupMode DG.Tweening.Tween::specialStartupMode
	int32_t ___specialStartupMode_39;
	// System.Boolean DG.Tweening.Tween::creationLocked
	bool ___creationLocked_40;
	// System.Boolean DG.Tweening.Tween::startupDone
	bool ___startupDone_41;
	// System.Boolean DG.Tweening.Tween::playedOnce
	bool ___playedOnce_42;
	// System.Single DG.Tweening.Tween::position
	float ___position_43;
	// System.Single DG.Tweening.Tween::fullDuration
	float ___fullDuration_44;
	// System.Int32 DG.Tweening.Tween::completedLoops
	int32_t ___completedLoops_45;
	// System.Boolean DG.Tweening.Tween::isPlaying
	bool ___isPlaying_46;
	// System.Boolean DG.Tweening.Tween::isComplete
	bool ___isComplete_47;
	// System.Single DG.Tweening.Tween::elapsedDelay
	float ___elapsedDelay_48;
	// System.Boolean DG.Tweening.Tween::delayComplete
	bool ___delayComplete_49;
	// System.Int32 DG.Tweening.Tween::miscInt
	int32_t ___miscInt_50;

public:
	inline static int32_t get_offset_of_timeScale_4() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___timeScale_4)); }
	inline float get_timeScale_4() const { return ___timeScale_4; }
	inline float* get_address_of_timeScale_4() { return &___timeScale_4; }
	inline void set_timeScale_4(float value)
	{
		___timeScale_4 = value;
	}

	inline static int32_t get_offset_of_isBackwards_5() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isBackwards_5)); }
	inline bool get_isBackwards_5() const { return ___isBackwards_5; }
	inline bool* get_address_of_isBackwards_5() { return &___isBackwards_5; }
	inline void set_isBackwards_5(bool value)
	{
		___isBackwards_5 = value;
	}

	inline static int32_t get_offset_of_id_6() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___id_6)); }
	inline RuntimeObject * get_id_6() const { return ___id_6; }
	inline RuntimeObject ** get_address_of_id_6() { return &___id_6; }
	inline void set_id_6(RuntimeObject * value)
	{
		___id_6 = value;
		Il2CppCodeGenWriteBarrier((&___id_6), value);
	}

	inline static int32_t get_offset_of_target_7() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___target_7)); }
	inline RuntimeObject * get_target_7() const { return ___target_7; }
	inline RuntimeObject ** get_address_of_target_7() { return &___target_7; }
	inline void set_target_7(RuntimeObject * value)
	{
		___target_7 = value;
		Il2CppCodeGenWriteBarrier((&___target_7), value);
	}

	inline static int32_t get_offset_of_updateType_8() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___updateType_8)); }
	inline int32_t get_updateType_8() const { return ___updateType_8; }
	inline int32_t* get_address_of_updateType_8() { return &___updateType_8; }
	inline void set_updateType_8(int32_t value)
	{
		___updateType_8 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_9() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isIndependentUpdate_9)); }
	inline bool get_isIndependentUpdate_9() const { return ___isIndependentUpdate_9; }
	inline bool* get_address_of_isIndependentUpdate_9() { return &___isIndependentUpdate_9; }
	inline void set_isIndependentUpdate_9(bool value)
	{
		___isIndependentUpdate_9 = value;
	}

	inline static int32_t get_offset_of_onPlay_10() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onPlay_10)); }
	inline TweenCallback_t3727756325 * get_onPlay_10() const { return ___onPlay_10; }
	inline TweenCallback_t3727756325 ** get_address_of_onPlay_10() { return &___onPlay_10; }
	inline void set_onPlay_10(TweenCallback_t3727756325 * value)
	{
		___onPlay_10 = value;
		Il2CppCodeGenWriteBarrier((&___onPlay_10), value);
	}

	inline static int32_t get_offset_of_onPause_11() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onPause_11)); }
	inline TweenCallback_t3727756325 * get_onPause_11() const { return ___onPause_11; }
	inline TweenCallback_t3727756325 ** get_address_of_onPause_11() { return &___onPause_11; }
	inline void set_onPause_11(TweenCallback_t3727756325 * value)
	{
		___onPause_11 = value;
		Il2CppCodeGenWriteBarrier((&___onPause_11), value);
	}

	inline static int32_t get_offset_of_onRewind_12() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onRewind_12)); }
	inline TweenCallback_t3727756325 * get_onRewind_12() const { return ___onRewind_12; }
	inline TweenCallback_t3727756325 ** get_address_of_onRewind_12() { return &___onRewind_12; }
	inline void set_onRewind_12(TweenCallback_t3727756325 * value)
	{
		___onRewind_12 = value;
		Il2CppCodeGenWriteBarrier((&___onRewind_12), value);
	}

	inline static int32_t get_offset_of_onUpdate_13() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onUpdate_13)); }
	inline TweenCallback_t3727756325 * get_onUpdate_13() const { return ___onUpdate_13; }
	inline TweenCallback_t3727756325 ** get_address_of_onUpdate_13() { return &___onUpdate_13; }
	inline void set_onUpdate_13(TweenCallback_t3727756325 * value)
	{
		___onUpdate_13 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdate_13), value);
	}

	inline static int32_t get_offset_of_onStepComplete_14() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onStepComplete_14)); }
	inline TweenCallback_t3727756325 * get_onStepComplete_14() const { return ___onStepComplete_14; }
	inline TweenCallback_t3727756325 ** get_address_of_onStepComplete_14() { return &___onStepComplete_14; }
	inline void set_onStepComplete_14(TweenCallback_t3727756325 * value)
	{
		___onStepComplete_14 = value;
		Il2CppCodeGenWriteBarrier((&___onStepComplete_14), value);
	}

	inline static int32_t get_offset_of_onComplete_15() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onComplete_15)); }
	inline TweenCallback_t3727756325 * get_onComplete_15() const { return ___onComplete_15; }
	inline TweenCallback_t3727756325 ** get_address_of_onComplete_15() { return &___onComplete_15; }
	inline void set_onComplete_15(TweenCallback_t3727756325 * value)
	{
		___onComplete_15 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_15), value);
	}

	inline static int32_t get_offset_of_onKill_16() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onKill_16)); }
	inline TweenCallback_t3727756325 * get_onKill_16() const { return ___onKill_16; }
	inline TweenCallback_t3727756325 ** get_address_of_onKill_16() { return &___onKill_16; }
	inline void set_onKill_16(TweenCallback_t3727756325 * value)
	{
		___onKill_16 = value;
		Il2CppCodeGenWriteBarrier((&___onKill_16), value);
	}

	inline static int32_t get_offset_of_onWaypointChange_17() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___onWaypointChange_17)); }
	inline TweenCallback_1_t3009965658 * get_onWaypointChange_17() const { return ___onWaypointChange_17; }
	inline TweenCallback_1_t3009965658 ** get_address_of_onWaypointChange_17() { return &___onWaypointChange_17; }
	inline void set_onWaypointChange_17(TweenCallback_1_t3009965658 * value)
	{
		___onWaypointChange_17 = value;
		Il2CppCodeGenWriteBarrier((&___onWaypointChange_17), value);
	}

	inline static int32_t get_offset_of_isFrom_18() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isFrom_18)); }
	inline bool get_isFrom_18() const { return ___isFrom_18; }
	inline bool* get_address_of_isFrom_18() { return &___isFrom_18; }
	inline void set_isFrom_18(bool value)
	{
		___isFrom_18 = value;
	}

	inline static int32_t get_offset_of_isBlendable_19() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isBlendable_19)); }
	inline bool get_isBlendable_19() const { return ___isBlendable_19; }
	inline bool* get_address_of_isBlendable_19() { return &___isBlendable_19; }
	inline void set_isBlendable_19(bool value)
	{
		___isBlendable_19 = value;
	}

	inline static int32_t get_offset_of_isRecyclable_20() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isRecyclable_20)); }
	inline bool get_isRecyclable_20() const { return ___isRecyclable_20; }
	inline bool* get_address_of_isRecyclable_20() { return &___isRecyclable_20; }
	inline void set_isRecyclable_20(bool value)
	{
		___isRecyclable_20 = value;
	}

	inline static int32_t get_offset_of_isSpeedBased_21() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isSpeedBased_21)); }
	inline bool get_isSpeedBased_21() const { return ___isSpeedBased_21; }
	inline bool* get_address_of_isSpeedBased_21() { return &___isSpeedBased_21; }
	inline void set_isSpeedBased_21(bool value)
	{
		___isSpeedBased_21 = value;
	}

	inline static int32_t get_offset_of_autoKill_22() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___autoKill_22)); }
	inline bool get_autoKill_22() const { return ___autoKill_22; }
	inline bool* get_address_of_autoKill_22() { return &___autoKill_22; }
	inline void set_autoKill_22(bool value)
	{
		___autoKill_22 = value;
	}

	inline static int32_t get_offset_of_duration_23() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___duration_23)); }
	inline float get_duration_23() const { return ___duration_23; }
	inline float* get_address_of_duration_23() { return &___duration_23; }
	inline void set_duration_23(float value)
	{
		___duration_23 = value;
	}

	inline static int32_t get_offset_of_loops_24() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___loops_24)); }
	inline int32_t get_loops_24() const { return ___loops_24; }
	inline int32_t* get_address_of_loops_24() { return &___loops_24; }
	inline void set_loops_24(int32_t value)
	{
		___loops_24 = value;
	}

	inline static int32_t get_offset_of_loopType_25() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___loopType_25)); }
	inline int32_t get_loopType_25() const { return ___loopType_25; }
	inline int32_t* get_address_of_loopType_25() { return &___loopType_25; }
	inline void set_loopType_25(int32_t value)
	{
		___loopType_25 = value;
	}

	inline static int32_t get_offset_of_delay_26() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___delay_26)); }
	inline float get_delay_26() const { return ___delay_26; }
	inline float* get_address_of_delay_26() { return &___delay_26; }
	inline void set_delay_26(float value)
	{
		___delay_26 = value;
	}

	inline static int32_t get_offset_of_isRelative_27() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isRelative_27)); }
	inline bool get_isRelative_27() const { return ___isRelative_27; }
	inline bool* get_address_of_isRelative_27() { return &___isRelative_27; }
	inline void set_isRelative_27(bool value)
	{
		___isRelative_27 = value;
	}

	inline static int32_t get_offset_of_easeType_28() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___easeType_28)); }
	inline int32_t get_easeType_28() const { return ___easeType_28; }
	inline int32_t* get_address_of_easeType_28() { return &___easeType_28; }
	inline void set_easeType_28(int32_t value)
	{
		___easeType_28 = value;
	}

	inline static int32_t get_offset_of_customEase_29() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___customEase_29)); }
	inline EaseFunction_t3531141372 * get_customEase_29() const { return ___customEase_29; }
	inline EaseFunction_t3531141372 ** get_address_of_customEase_29() { return &___customEase_29; }
	inline void set_customEase_29(EaseFunction_t3531141372 * value)
	{
		___customEase_29 = value;
		Il2CppCodeGenWriteBarrier((&___customEase_29), value);
	}

	inline static int32_t get_offset_of_easeOvershootOrAmplitude_30() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___easeOvershootOrAmplitude_30)); }
	inline float get_easeOvershootOrAmplitude_30() const { return ___easeOvershootOrAmplitude_30; }
	inline float* get_address_of_easeOvershootOrAmplitude_30() { return &___easeOvershootOrAmplitude_30; }
	inline void set_easeOvershootOrAmplitude_30(float value)
	{
		___easeOvershootOrAmplitude_30 = value;
	}

	inline static int32_t get_offset_of_easePeriod_31() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___easePeriod_31)); }
	inline float get_easePeriod_31() const { return ___easePeriod_31; }
	inline float* get_address_of_easePeriod_31() { return &___easePeriod_31; }
	inline void set_easePeriod_31(float value)
	{
		___easePeriod_31 = value;
	}

	inline static int32_t get_offset_of_typeofT1_32() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___typeofT1_32)); }
	inline Type_t * get_typeofT1_32() const { return ___typeofT1_32; }
	inline Type_t ** get_address_of_typeofT1_32() { return &___typeofT1_32; }
	inline void set_typeofT1_32(Type_t * value)
	{
		___typeofT1_32 = value;
		Il2CppCodeGenWriteBarrier((&___typeofT1_32), value);
	}

	inline static int32_t get_offset_of_typeofT2_33() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___typeofT2_33)); }
	inline Type_t * get_typeofT2_33() const { return ___typeofT2_33; }
	inline Type_t ** get_address_of_typeofT2_33() { return &___typeofT2_33; }
	inline void set_typeofT2_33(Type_t * value)
	{
		___typeofT2_33 = value;
		Il2CppCodeGenWriteBarrier((&___typeofT2_33), value);
	}

	inline static int32_t get_offset_of_typeofTPlugOptions_34() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___typeofTPlugOptions_34)); }
	inline Type_t * get_typeofTPlugOptions_34() const { return ___typeofTPlugOptions_34; }
	inline Type_t ** get_address_of_typeofTPlugOptions_34() { return &___typeofTPlugOptions_34; }
	inline void set_typeofTPlugOptions_34(Type_t * value)
	{
		___typeofTPlugOptions_34 = value;
		Il2CppCodeGenWriteBarrier((&___typeofTPlugOptions_34), value);
	}

	inline static int32_t get_offset_of_active_35() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___active_35)); }
	inline bool get_active_35() const { return ___active_35; }
	inline bool* get_address_of_active_35() { return &___active_35; }
	inline void set_active_35(bool value)
	{
		___active_35 = value;
	}

	inline static int32_t get_offset_of_isSequenced_36() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isSequenced_36)); }
	inline bool get_isSequenced_36() const { return ___isSequenced_36; }
	inline bool* get_address_of_isSequenced_36() { return &___isSequenced_36; }
	inline void set_isSequenced_36(bool value)
	{
		___isSequenced_36 = value;
	}

	inline static int32_t get_offset_of_sequenceParent_37() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___sequenceParent_37)); }
	inline Sequence_t2050373119 * get_sequenceParent_37() const { return ___sequenceParent_37; }
	inline Sequence_t2050373119 ** get_address_of_sequenceParent_37() { return &___sequenceParent_37; }
	inline void set_sequenceParent_37(Sequence_t2050373119 * value)
	{
		___sequenceParent_37 = value;
		Il2CppCodeGenWriteBarrier((&___sequenceParent_37), value);
	}

	inline static int32_t get_offset_of_activeId_38() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___activeId_38)); }
	inline int32_t get_activeId_38() const { return ___activeId_38; }
	inline int32_t* get_address_of_activeId_38() { return &___activeId_38; }
	inline void set_activeId_38(int32_t value)
	{
		___activeId_38 = value;
	}

	inline static int32_t get_offset_of_specialStartupMode_39() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___specialStartupMode_39)); }
	inline int32_t get_specialStartupMode_39() const { return ___specialStartupMode_39; }
	inline int32_t* get_address_of_specialStartupMode_39() { return &___specialStartupMode_39; }
	inline void set_specialStartupMode_39(int32_t value)
	{
		___specialStartupMode_39 = value;
	}

	inline static int32_t get_offset_of_creationLocked_40() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___creationLocked_40)); }
	inline bool get_creationLocked_40() const { return ___creationLocked_40; }
	inline bool* get_address_of_creationLocked_40() { return &___creationLocked_40; }
	inline void set_creationLocked_40(bool value)
	{
		___creationLocked_40 = value;
	}

	inline static int32_t get_offset_of_startupDone_41() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___startupDone_41)); }
	inline bool get_startupDone_41() const { return ___startupDone_41; }
	inline bool* get_address_of_startupDone_41() { return &___startupDone_41; }
	inline void set_startupDone_41(bool value)
	{
		___startupDone_41 = value;
	}

	inline static int32_t get_offset_of_playedOnce_42() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___playedOnce_42)); }
	inline bool get_playedOnce_42() const { return ___playedOnce_42; }
	inline bool* get_address_of_playedOnce_42() { return &___playedOnce_42; }
	inline void set_playedOnce_42(bool value)
	{
		___playedOnce_42 = value;
	}

	inline static int32_t get_offset_of_position_43() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___position_43)); }
	inline float get_position_43() const { return ___position_43; }
	inline float* get_address_of_position_43() { return &___position_43; }
	inline void set_position_43(float value)
	{
		___position_43 = value;
	}

	inline static int32_t get_offset_of_fullDuration_44() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___fullDuration_44)); }
	inline float get_fullDuration_44() const { return ___fullDuration_44; }
	inline float* get_address_of_fullDuration_44() { return &___fullDuration_44; }
	inline void set_fullDuration_44(float value)
	{
		___fullDuration_44 = value;
	}

	inline static int32_t get_offset_of_completedLoops_45() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___completedLoops_45)); }
	inline int32_t get_completedLoops_45() const { return ___completedLoops_45; }
	inline int32_t* get_address_of_completedLoops_45() { return &___completedLoops_45; }
	inline void set_completedLoops_45(int32_t value)
	{
		___completedLoops_45 = value;
	}

	inline static int32_t get_offset_of_isPlaying_46() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isPlaying_46)); }
	inline bool get_isPlaying_46() const { return ___isPlaying_46; }
	inline bool* get_address_of_isPlaying_46() { return &___isPlaying_46; }
	inline void set_isPlaying_46(bool value)
	{
		___isPlaying_46 = value;
	}

	inline static int32_t get_offset_of_isComplete_47() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___isComplete_47)); }
	inline bool get_isComplete_47() const { return ___isComplete_47; }
	inline bool* get_address_of_isComplete_47() { return &___isComplete_47; }
	inline void set_isComplete_47(bool value)
	{
		___isComplete_47 = value;
	}

	inline static int32_t get_offset_of_elapsedDelay_48() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___elapsedDelay_48)); }
	inline float get_elapsedDelay_48() const { return ___elapsedDelay_48; }
	inline float* get_address_of_elapsedDelay_48() { return &___elapsedDelay_48; }
	inline void set_elapsedDelay_48(float value)
	{
		___elapsedDelay_48 = value;
	}

	inline static int32_t get_offset_of_delayComplete_49() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___delayComplete_49)); }
	inline bool get_delayComplete_49() const { return ___delayComplete_49; }
	inline bool* get_address_of_delayComplete_49() { return &___delayComplete_49; }
	inline void set_delayComplete_49(bool value)
	{
		___delayComplete_49 = value;
	}

	inline static int32_t get_offset_of_miscInt_50() { return static_cast<int32_t>(offsetof(Tween_t2342918553, ___miscInt_50)); }
	inline int32_t get_miscInt_50() const { return ___miscInt_50; }
	inline int32_t* get_address_of_miscInt_50() { return &___miscInt_50; }
	inline void set_miscInt_50(int32_t value)
	{
		___miscInt_50 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEEN_T2342918553_H
#ifndef DOGETTER_1_T2298482528_H
#define DOGETTER_1_T2298482528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOGetter`1<UnityEngine.Color>
struct  DOGetter_1_t2298482528  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOGETTER_1_T2298482528_H
#ifndef DOGETTER_1_T1140062978_H
#define DOGETTER_1_T1140062978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOGetter`1<System.Single>
struct  DOGetter_1_t1140062978  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOGETTER_1_T1140062978_H
#ifndef DOSETTER_1_T3206337855_H
#define DOSETTER_1_T3206337855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>
struct  DOSetter_1_t3206337855  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOSETTER_1_T3206337855_H
#ifndef DOSETTER_1_T3605794656_H
#define DOSETTER_1_T3605794656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOSetter`1<UnityEngine.Color>
struct  DOSetter_1_t3605794656  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOSETTER_1_T3605794656_H
#ifndef DOSETTER_1_T2447375106_H
#define DOSETTER_1_T2447375106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOSetter`1<System.Single>
struct  DOSetter_1_t2447375106  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOSETTER_1_T2447375106_H
#ifndef DOGETTER_1_T1899025727_H
#define DOGETTER_1_T1899025727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>
struct  DOGetter_1_t1899025727  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOGETTER_1_T1899025727_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef CANVASGROUP_T4083511760_H
#define CANVASGROUP_T4083511760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CanvasGroup
struct  CanvasGroup_t4083511760  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASGROUP_T4083511760_H
#ifndef DOGETTER_1_T1590246893_H
#define DOGETTER_1_T1590246893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOGetter`1<System.String>
struct  DOGetter_1_t1590246893  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOGETTER_1_T1590246893_H
#ifndef DOSETTER_1_T477454500_H
#define DOSETTER_1_T477454500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>
struct  DOSetter_1_t477454500  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOSETTER_1_T477454500_H
#ifndef DOSETTER_1_T2897559021_H
#define DOSETTER_1_T2897559021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOSetter`1<System.String>
struct  DOSetter_1_t2897559021  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOSETTER_1_T2897559021_H
#ifndef DOGETTER_1_T3465109668_H
#define DOGETTER_1_T3465109668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>
struct  DOGetter_1_t3465109668  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOGETTER_1_T3465109668_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef RECTTRANSFORM_T3704657025_H
#define RECTTRANSFORM_T3704657025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t3704657025  : public Transform_t3600365921
{
public:

public:
};

struct RectTransform_t3704657025_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1258266594 * ___reapplyDrivenProperties_2;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_2() { return static_cast<int32_t>(offsetof(RectTransform_t3704657025_StaticFields, ___reapplyDrivenProperties_2)); }
	inline ReapplyDrivenProperties_t1258266594 * get_reapplyDrivenProperties_2() const { return ___reapplyDrivenProperties_2; }
	inline ReapplyDrivenProperties_t1258266594 ** get_address_of_reapplyDrivenProperties_2() { return &___reapplyDrivenProperties_2; }
	inline void set_reapplyDrivenProperties_2(ReapplyDrivenProperties_t1258266594 * value)
	{
		___reapplyDrivenProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T3704657025_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef TWEENER_T436044680_H
#define TWEENER_T436044680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Tweener
struct  Tweener_t436044680  : public Tween_t2342918553
{
public:
	// System.Boolean DG.Tweening.Tweener::hasManuallySetStartValue
	bool ___hasManuallySetStartValue_51;
	// System.Boolean DG.Tweening.Tweener::isFromAllowed
	bool ___isFromAllowed_52;

public:
	inline static int32_t get_offset_of_hasManuallySetStartValue_51() { return static_cast<int32_t>(offsetof(Tweener_t436044680, ___hasManuallySetStartValue_51)); }
	inline bool get_hasManuallySetStartValue_51() const { return ___hasManuallySetStartValue_51; }
	inline bool* get_address_of_hasManuallySetStartValue_51() { return &___hasManuallySetStartValue_51; }
	inline void set_hasManuallySetStartValue_51(bool value)
	{
		___hasManuallySetStartValue_51 = value;
	}

	inline static int32_t get_offset_of_isFromAllowed_52() { return static_cast<int32_t>(offsetof(Tweener_t436044680, ___isFromAllowed_52)); }
	inline bool get_isFromAllowed_52() const { return ___isFromAllowed_52; }
	inline bool* get_address_of_isFromAllowed_52() { return &___isFromAllowed_52; }
	inline void set_isFromAllowed_52(bool value)
	{
		___isFromAllowed_52 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENER_T436044680_H
#ifndef CAMERA_T4157153871_H
#define CAMERA_T4157153871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t4157153871  : public Behaviour_t1437897464
{
public:

public:
};

struct Camera_t4157153871_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t190067161 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t190067161 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t190067161 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t190067161 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t190067161 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t190067161 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t190067161 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t190067161 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t190067161 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t190067161 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t190067161 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t190067161 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T4157153871_H
#ifndef TWEENERCORE_3_T951858585_H
#define TWEENERCORE_3_T951858585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>
struct  TweenerCore_3_t951858585  : public Tweener_t436044680
{
public:
	// T2 DG.Tweening.Core.TweenerCore`3::startValue
	Vector3U5BU5D_t1718750761* ___startValue_53;
	// T2 DG.Tweening.Core.TweenerCore`3::endValue
	Vector3U5BU5D_t1718750761* ___endValue_54;
	// T2 DG.Tweening.Core.TweenerCore`3::changeValue
	Vector3U5BU5D_t1718750761* ___changeValue_55;
	// TPlugOptions DG.Tweening.Core.TweenerCore`3::plugOptions
	Vector3ArrayOptions_t534739431  ___plugOptions_56;
	// DG.Tweening.Core.DOGetter`1<T1> DG.Tweening.Core.TweenerCore`3::getter
	DOGetter_1_t3465109668 * ___getter_57;
	// DG.Tweening.Core.DOSetter`1<T1> DG.Tweening.Core.TweenerCore`3::setter
	DOSetter_1_t477454500 * ___setter_58;
	// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions> DG.Tweening.Core.TweenerCore`3::tweenPlugin
	ABSTweenPlugin_3_t487868235 * ___tweenPlugin_59;

public:
	inline static int32_t get_offset_of_startValue_53() { return static_cast<int32_t>(offsetof(TweenerCore_3_t951858585, ___startValue_53)); }
	inline Vector3U5BU5D_t1718750761* get_startValue_53() const { return ___startValue_53; }
	inline Vector3U5BU5D_t1718750761** get_address_of_startValue_53() { return &___startValue_53; }
	inline void set_startValue_53(Vector3U5BU5D_t1718750761* value)
	{
		___startValue_53 = value;
		Il2CppCodeGenWriteBarrier((&___startValue_53), value);
	}

	inline static int32_t get_offset_of_endValue_54() { return static_cast<int32_t>(offsetof(TweenerCore_3_t951858585, ___endValue_54)); }
	inline Vector3U5BU5D_t1718750761* get_endValue_54() const { return ___endValue_54; }
	inline Vector3U5BU5D_t1718750761** get_address_of_endValue_54() { return &___endValue_54; }
	inline void set_endValue_54(Vector3U5BU5D_t1718750761* value)
	{
		___endValue_54 = value;
		Il2CppCodeGenWriteBarrier((&___endValue_54), value);
	}

	inline static int32_t get_offset_of_changeValue_55() { return static_cast<int32_t>(offsetof(TweenerCore_3_t951858585, ___changeValue_55)); }
	inline Vector3U5BU5D_t1718750761* get_changeValue_55() const { return ___changeValue_55; }
	inline Vector3U5BU5D_t1718750761** get_address_of_changeValue_55() { return &___changeValue_55; }
	inline void set_changeValue_55(Vector3U5BU5D_t1718750761* value)
	{
		___changeValue_55 = value;
		Il2CppCodeGenWriteBarrier((&___changeValue_55), value);
	}

	inline static int32_t get_offset_of_plugOptions_56() { return static_cast<int32_t>(offsetof(TweenerCore_3_t951858585, ___plugOptions_56)); }
	inline Vector3ArrayOptions_t534739431  get_plugOptions_56() const { return ___plugOptions_56; }
	inline Vector3ArrayOptions_t534739431 * get_address_of_plugOptions_56() { return &___plugOptions_56; }
	inline void set_plugOptions_56(Vector3ArrayOptions_t534739431  value)
	{
		___plugOptions_56 = value;
	}

	inline static int32_t get_offset_of_getter_57() { return static_cast<int32_t>(offsetof(TweenerCore_3_t951858585, ___getter_57)); }
	inline DOGetter_1_t3465109668 * get_getter_57() const { return ___getter_57; }
	inline DOGetter_1_t3465109668 ** get_address_of_getter_57() { return &___getter_57; }
	inline void set_getter_57(DOGetter_1_t3465109668 * value)
	{
		___getter_57 = value;
		Il2CppCodeGenWriteBarrier((&___getter_57), value);
	}

	inline static int32_t get_offset_of_setter_58() { return static_cast<int32_t>(offsetof(TweenerCore_3_t951858585, ___setter_58)); }
	inline DOSetter_1_t477454500 * get_setter_58() const { return ___setter_58; }
	inline DOSetter_1_t477454500 ** get_address_of_setter_58() { return &___setter_58; }
	inline void set_setter_58(DOSetter_1_t477454500 * value)
	{
		___setter_58 = value;
		Il2CppCodeGenWriteBarrier((&___setter_58), value);
	}

	inline static int32_t get_offset_of_tweenPlugin_59() { return static_cast<int32_t>(offsetof(TweenerCore_3_t951858585, ___tweenPlugin_59)); }
	inline ABSTweenPlugin_3_t487868235 * get_tweenPlugin_59() const { return ___tweenPlugin_59; }
	inline ABSTweenPlugin_3_t487868235 ** get_address_of_tweenPlugin_59() { return &___tweenPlugin_59; }
	inline void set_tweenPlugin_59(ABSTweenPlugin_3_t487868235 * value)
	{
		___tweenPlugin_59 = value;
		Il2CppCodeGenWriteBarrier((&___tweenPlugin_59), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENERCORE_3_T951858585_H
#ifndef TWEENERCORE_3_T745532282_H
#define TWEENERCORE_3_T745532282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct  TweenerCore_3_t745532282  : public Tweener_t436044680
{
public:
	// T2 DG.Tweening.Core.TweenerCore`3::startValue
	Color_t2555686324  ___startValue_53;
	// T2 DG.Tweening.Core.TweenerCore`3::endValue
	Color_t2555686324  ___endValue_54;
	// T2 DG.Tweening.Core.TweenerCore`3::changeValue
	Color_t2555686324  ___changeValue_55;
	// TPlugOptions DG.Tweening.Core.TweenerCore`3::plugOptions
	ColorOptions_t1487297155  ___plugOptions_56;
	// DG.Tweening.Core.DOGetter`1<T1> DG.Tweening.Core.TweenerCore`3::getter
	DOGetter_1_t2298482528 * ___getter_57;
	// DG.Tweening.Core.DOSetter`1<T1> DG.Tweening.Core.TweenerCore`3::setter
	DOSetter_1_t3605794656 * ___setter_58;
	// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions> DG.Tweening.Core.TweenerCore`3::tweenPlugin
	ABSTweenPlugin_3_t281541932 * ___tweenPlugin_59;

public:
	inline static int32_t get_offset_of_startValue_53() { return static_cast<int32_t>(offsetof(TweenerCore_3_t745532282, ___startValue_53)); }
	inline Color_t2555686324  get_startValue_53() const { return ___startValue_53; }
	inline Color_t2555686324 * get_address_of_startValue_53() { return &___startValue_53; }
	inline void set_startValue_53(Color_t2555686324  value)
	{
		___startValue_53 = value;
	}

	inline static int32_t get_offset_of_endValue_54() { return static_cast<int32_t>(offsetof(TweenerCore_3_t745532282, ___endValue_54)); }
	inline Color_t2555686324  get_endValue_54() const { return ___endValue_54; }
	inline Color_t2555686324 * get_address_of_endValue_54() { return &___endValue_54; }
	inline void set_endValue_54(Color_t2555686324  value)
	{
		___endValue_54 = value;
	}

	inline static int32_t get_offset_of_changeValue_55() { return static_cast<int32_t>(offsetof(TweenerCore_3_t745532282, ___changeValue_55)); }
	inline Color_t2555686324  get_changeValue_55() const { return ___changeValue_55; }
	inline Color_t2555686324 * get_address_of_changeValue_55() { return &___changeValue_55; }
	inline void set_changeValue_55(Color_t2555686324  value)
	{
		___changeValue_55 = value;
	}

	inline static int32_t get_offset_of_plugOptions_56() { return static_cast<int32_t>(offsetof(TweenerCore_3_t745532282, ___plugOptions_56)); }
	inline ColorOptions_t1487297155  get_plugOptions_56() const { return ___plugOptions_56; }
	inline ColorOptions_t1487297155 * get_address_of_plugOptions_56() { return &___plugOptions_56; }
	inline void set_plugOptions_56(ColorOptions_t1487297155  value)
	{
		___plugOptions_56 = value;
	}

	inline static int32_t get_offset_of_getter_57() { return static_cast<int32_t>(offsetof(TweenerCore_3_t745532282, ___getter_57)); }
	inline DOGetter_1_t2298482528 * get_getter_57() const { return ___getter_57; }
	inline DOGetter_1_t2298482528 ** get_address_of_getter_57() { return &___getter_57; }
	inline void set_getter_57(DOGetter_1_t2298482528 * value)
	{
		___getter_57 = value;
		Il2CppCodeGenWriteBarrier((&___getter_57), value);
	}

	inline static int32_t get_offset_of_setter_58() { return static_cast<int32_t>(offsetof(TweenerCore_3_t745532282, ___setter_58)); }
	inline DOSetter_1_t3605794656 * get_setter_58() const { return ___setter_58; }
	inline DOSetter_1_t3605794656 ** get_address_of_setter_58() { return &___setter_58; }
	inline void set_setter_58(DOSetter_1_t3605794656 * value)
	{
		___setter_58 = value;
		Il2CppCodeGenWriteBarrier((&___setter_58), value);
	}

	inline static int32_t get_offset_of_tweenPlugin_59() { return static_cast<int32_t>(offsetof(TweenerCore_3_t745532282, ___tweenPlugin_59)); }
	inline ABSTweenPlugin_3_t281541932 * get_tweenPlugin_59() const { return ___tweenPlugin_59; }
	inline ABSTweenPlugin_3_t281541932 ** get_address_of_tweenPlugin_59() { return &___tweenPlugin_59; }
	inline void set_tweenPlugin_59(ABSTweenPlugin_3_t281541932 * value)
	{
		___tweenPlugin_59 = value;
		Il2CppCodeGenWriteBarrier((&___tweenPlugin_59), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENERCORE_3_T745532282_H
#ifndef TWEENERCORE_3_T884556411_H
#define TWEENERCORE_3_T884556411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct  TweenerCore_3_t884556411  : public Tweener_t436044680
{
public:
	// T2 DG.Tweening.Core.TweenerCore`3::startValue
	float ___startValue_53;
	// T2 DG.Tweening.Core.TweenerCore`3::endValue
	float ___endValue_54;
	// T2 DG.Tweening.Core.TweenerCore`3::changeValue
	float ___changeValue_55;
	// TPlugOptions DG.Tweening.Core.TweenerCore`3::plugOptions
	FloatOptions_t1203667100  ___plugOptions_56;
	// DG.Tweening.Core.DOGetter`1<T1> DG.Tweening.Core.TweenerCore`3::getter
	DOGetter_1_t1140062978 * ___getter_57;
	// DG.Tweening.Core.DOSetter`1<T1> DG.Tweening.Core.TweenerCore`3::setter
	DOSetter_1_t2447375106 * ___setter_58;
	// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions> DG.Tweening.Core.TweenerCore`3::tweenPlugin
	ABSTweenPlugin_3_t420566061 * ___tweenPlugin_59;

public:
	inline static int32_t get_offset_of_startValue_53() { return static_cast<int32_t>(offsetof(TweenerCore_3_t884556411, ___startValue_53)); }
	inline float get_startValue_53() const { return ___startValue_53; }
	inline float* get_address_of_startValue_53() { return &___startValue_53; }
	inline void set_startValue_53(float value)
	{
		___startValue_53 = value;
	}

	inline static int32_t get_offset_of_endValue_54() { return static_cast<int32_t>(offsetof(TweenerCore_3_t884556411, ___endValue_54)); }
	inline float get_endValue_54() const { return ___endValue_54; }
	inline float* get_address_of_endValue_54() { return &___endValue_54; }
	inline void set_endValue_54(float value)
	{
		___endValue_54 = value;
	}

	inline static int32_t get_offset_of_changeValue_55() { return static_cast<int32_t>(offsetof(TweenerCore_3_t884556411, ___changeValue_55)); }
	inline float get_changeValue_55() const { return ___changeValue_55; }
	inline float* get_address_of_changeValue_55() { return &___changeValue_55; }
	inline void set_changeValue_55(float value)
	{
		___changeValue_55 = value;
	}

	inline static int32_t get_offset_of_plugOptions_56() { return static_cast<int32_t>(offsetof(TweenerCore_3_t884556411, ___plugOptions_56)); }
	inline FloatOptions_t1203667100  get_plugOptions_56() const { return ___plugOptions_56; }
	inline FloatOptions_t1203667100 * get_address_of_plugOptions_56() { return &___plugOptions_56; }
	inline void set_plugOptions_56(FloatOptions_t1203667100  value)
	{
		___plugOptions_56 = value;
	}

	inline static int32_t get_offset_of_getter_57() { return static_cast<int32_t>(offsetof(TweenerCore_3_t884556411, ___getter_57)); }
	inline DOGetter_1_t1140062978 * get_getter_57() const { return ___getter_57; }
	inline DOGetter_1_t1140062978 ** get_address_of_getter_57() { return &___getter_57; }
	inline void set_getter_57(DOGetter_1_t1140062978 * value)
	{
		___getter_57 = value;
		Il2CppCodeGenWriteBarrier((&___getter_57), value);
	}

	inline static int32_t get_offset_of_setter_58() { return static_cast<int32_t>(offsetof(TweenerCore_3_t884556411, ___setter_58)); }
	inline DOSetter_1_t2447375106 * get_setter_58() const { return ___setter_58; }
	inline DOSetter_1_t2447375106 ** get_address_of_setter_58() { return &___setter_58; }
	inline void set_setter_58(DOSetter_1_t2447375106 * value)
	{
		___setter_58 = value;
		Il2CppCodeGenWriteBarrier((&___setter_58), value);
	}

	inline static int32_t get_offset_of_tweenPlugin_59() { return static_cast<int32_t>(offsetof(TweenerCore_3_t884556411, ___tweenPlugin_59)); }
	inline ABSTweenPlugin_3_t420566061 * get_tweenPlugin_59() const { return ___tweenPlugin_59; }
	inline ABSTweenPlugin_3_t420566061 ** get_address_of_tweenPlugin_59() { return &___tweenPlugin_59; }
	inline void set_tweenPlugin_59(ABSTweenPlugin_3_t420566061 * value)
	{
		___tweenPlugin_59 = value;
		Il2CppCodeGenWriteBarrier((&___tweenPlugin_59), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENERCORE_3_T884556411_H
#ifndef TWEENERCORE_3_T2915539799_H
#define TWEENERCORE_3_T2915539799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>
struct  TweenerCore_3_t2915539799  : public Tweener_t436044680
{
public:
	// T2 DG.Tweening.Core.TweenerCore`3::startValue
	String_t* ___startValue_53;
	// T2 DG.Tweening.Core.TweenerCore`3::endValue
	String_t* ___endValue_54;
	// T2 DG.Tweening.Core.TweenerCore`3::changeValue
	String_t* ___changeValue_55;
	// TPlugOptions DG.Tweening.Core.TweenerCore`3::plugOptions
	StringOptions_t3992490940  ___plugOptions_56;
	// DG.Tweening.Core.DOGetter`1<T1> DG.Tweening.Core.TweenerCore`3::getter
	DOGetter_1_t1590246893 * ___getter_57;
	// DG.Tweening.Core.DOSetter`1<T1> DG.Tweening.Core.TweenerCore`3::setter
	DOSetter_1_t2897559021 * ___setter_58;
	// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions> DG.Tweening.Core.TweenerCore`3::tweenPlugin
	ABSTweenPlugin_3_t2451549449 * ___tweenPlugin_59;

public:
	inline static int32_t get_offset_of_startValue_53() { return static_cast<int32_t>(offsetof(TweenerCore_3_t2915539799, ___startValue_53)); }
	inline String_t* get_startValue_53() const { return ___startValue_53; }
	inline String_t** get_address_of_startValue_53() { return &___startValue_53; }
	inline void set_startValue_53(String_t* value)
	{
		___startValue_53 = value;
		Il2CppCodeGenWriteBarrier((&___startValue_53), value);
	}

	inline static int32_t get_offset_of_endValue_54() { return static_cast<int32_t>(offsetof(TweenerCore_3_t2915539799, ___endValue_54)); }
	inline String_t* get_endValue_54() const { return ___endValue_54; }
	inline String_t** get_address_of_endValue_54() { return &___endValue_54; }
	inline void set_endValue_54(String_t* value)
	{
		___endValue_54 = value;
		Il2CppCodeGenWriteBarrier((&___endValue_54), value);
	}

	inline static int32_t get_offset_of_changeValue_55() { return static_cast<int32_t>(offsetof(TweenerCore_3_t2915539799, ___changeValue_55)); }
	inline String_t* get_changeValue_55() const { return ___changeValue_55; }
	inline String_t** get_address_of_changeValue_55() { return &___changeValue_55; }
	inline void set_changeValue_55(String_t* value)
	{
		___changeValue_55 = value;
		Il2CppCodeGenWriteBarrier((&___changeValue_55), value);
	}

	inline static int32_t get_offset_of_plugOptions_56() { return static_cast<int32_t>(offsetof(TweenerCore_3_t2915539799, ___plugOptions_56)); }
	inline StringOptions_t3992490940  get_plugOptions_56() const { return ___plugOptions_56; }
	inline StringOptions_t3992490940 * get_address_of_plugOptions_56() { return &___plugOptions_56; }
	inline void set_plugOptions_56(StringOptions_t3992490940  value)
	{
		___plugOptions_56 = value;
	}

	inline static int32_t get_offset_of_getter_57() { return static_cast<int32_t>(offsetof(TweenerCore_3_t2915539799, ___getter_57)); }
	inline DOGetter_1_t1590246893 * get_getter_57() const { return ___getter_57; }
	inline DOGetter_1_t1590246893 ** get_address_of_getter_57() { return &___getter_57; }
	inline void set_getter_57(DOGetter_1_t1590246893 * value)
	{
		___getter_57 = value;
		Il2CppCodeGenWriteBarrier((&___getter_57), value);
	}

	inline static int32_t get_offset_of_setter_58() { return static_cast<int32_t>(offsetof(TweenerCore_3_t2915539799, ___setter_58)); }
	inline DOSetter_1_t2897559021 * get_setter_58() const { return ___setter_58; }
	inline DOSetter_1_t2897559021 ** get_address_of_setter_58() { return &___setter_58; }
	inline void set_setter_58(DOSetter_1_t2897559021 * value)
	{
		___setter_58 = value;
		Il2CppCodeGenWriteBarrier((&___setter_58), value);
	}

	inline static int32_t get_offset_of_tweenPlugin_59() { return static_cast<int32_t>(offsetof(TweenerCore_3_t2915539799, ___tweenPlugin_59)); }
	inline ABSTweenPlugin_3_t2451549449 * get_tweenPlugin_59() const { return ___tweenPlugin_59; }
	inline ABSTweenPlugin_3_t2451549449 ** get_address_of_tweenPlugin_59() { return &___tweenPlugin_59; }
	inline void set_tweenPlugin_59(ABSTweenPlugin_3_t2451549449 * value)
	{
		___tweenPlugin_59 = value;
		Il2CppCodeGenWriteBarrier((&___tweenPlugin_59), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENERCORE_3_T2915539799_H
#ifndef TWEENERCORE_3_T2944330537_H
#define TWEENERCORE_3_T2944330537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct  TweenerCore_3_t2944330537  : public Tweener_t436044680
{
public:
	// T2 DG.Tweening.Core.TweenerCore`3::startValue
	Vector3_t3722313464  ___startValue_53;
	// T2 DG.Tweening.Core.TweenerCore`3::endValue
	Vector3_t3722313464  ___endValue_54;
	// T2 DG.Tweening.Core.TweenerCore`3::changeValue
	Vector3_t3722313464  ___changeValue_55;
	// TPlugOptions DG.Tweening.Core.TweenerCore`3::plugOptions
	VectorOptions_t1354903650  ___plugOptions_56;
	// DG.Tweening.Core.DOGetter`1<T1> DG.Tweening.Core.TweenerCore`3::getter
	DOGetter_1_t3465109668 * ___getter_57;
	// DG.Tweening.Core.DOSetter`1<T1> DG.Tweening.Core.TweenerCore`3::setter
	DOSetter_1_t477454500 * ___setter_58;
	// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions> DG.Tweening.Core.TweenerCore`3::tweenPlugin
	ABSTweenPlugin_3_t2480340187 * ___tweenPlugin_59;

public:
	inline static int32_t get_offset_of_startValue_53() { return static_cast<int32_t>(offsetof(TweenerCore_3_t2944330537, ___startValue_53)); }
	inline Vector3_t3722313464  get_startValue_53() const { return ___startValue_53; }
	inline Vector3_t3722313464 * get_address_of_startValue_53() { return &___startValue_53; }
	inline void set_startValue_53(Vector3_t3722313464  value)
	{
		___startValue_53 = value;
	}

	inline static int32_t get_offset_of_endValue_54() { return static_cast<int32_t>(offsetof(TweenerCore_3_t2944330537, ___endValue_54)); }
	inline Vector3_t3722313464  get_endValue_54() const { return ___endValue_54; }
	inline Vector3_t3722313464 * get_address_of_endValue_54() { return &___endValue_54; }
	inline void set_endValue_54(Vector3_t3722313464  value)
	{
		___endValue_54 = value;
	}

	inline static int32_t get_offset_of_changeValue_55() { return static_cast<int32_t>(offsetof(TweenerCore_3_t2944330537, ___changeValue_55)); }
	inline Vector3_t3722313464  get_changeValue_55() const { return ___changeValue_55; }
	inline Vector3_t3722313464 * get_address_of_changeValue_55() { return &___changeValue_55; }
	inline void set_changeValue_55(Vector3_t3722313464  value)
	{
		___changeValue_55 = value;
	}

	inline static int32_t get_offset_of_plugOptions_56() { return static_cast<int32_t>(offsetof(TweenerCore_3_t2944330537, ___plugOptions_56)); }
	inline VectorOptions_t1354903650  get_plugOptions_56() const { return ___plugOptions_56; }
	inline VectorOptions_t1354903650 * get_address_of_plugOptions_56() { return &___plugOptions_56; }
	inline void set_plugOptions_56(VectorOptions_t1354903650  value)
	{
		___plugOptions_56 = value;
	}

	inline static int32_t get_offset_of_getter_57() { return static_cast<int32_t>(offsetof(TweenerCore_3_t2944330537, ___getter_57)); }
	inline DOGetter_1_t3465109668 * get_getter_57() const { return ___getter_57; }
	inline DOGetter_1_t3465109668 ** get_address_of_getter_57() { return &___getter_57; }
	inline void set_getter_57(DOGetter_1_t3465109668 * value)
	{
		___getter_57 = value;
		Il2CppCodeGenWriteBarrier((&___getter_57), value);
	}

	inline static int32_t get_offset_of_setter_58() { return static_cast<int32_t>(offsetof(TweenerCore_3_t2944330537, ___setter_58)); }
	inline DOSetter_1_t477454500 * get_setter_58() const { return ___setter_58; }
	inline DOSetter_1_t477454500 ** get_address_of_setter_58() { return &___setter_58; }
	inline void set_setter_58(DOSetter_1_t477454500 * value)
	{
		___setter_58 = value;
		Il2CppCodeGenWriteBarrier((&___setter_58), value);
	}

	inline static int32_t get_offset_of_tweenPlugin_59() { return static_cast<int32_t>(offsetof(TweenerCore_3_t2944330537, ___tweenPlugin_59)); }
	inline ABSTweenPlugin_3_t2480340187 * get_tweenPlugin_59() const { return ___tweenPlugin_59; }
	inline ABSTweenPlugin_3_t2480340187 ** get_address_of_tweenPlugin_59() { return &___tweenPlugin_59; }
	inline void set_tweenPlugin_59(ABSTweenPlugin_3_t2480340187 * value)
	{
		___tweenPlugin_59 = value;
		Il2CppCodeGenWriteBarrier((&___tweenPlugin_59), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENERCORE_3_T2944330537_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef TWEENERCORE_3_T4006488229_H
#define TWEENERCORE_3_T4006488229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct  TweenerCore_3_t4006488229  : public Tweener_t436044680
{
public:
	// T2 DG.Tweening.Core.TweenerCore`3::startValue
	Vector2_t2156229523  ___startValue_53;
	// T2 DG.Tweening.Core.TweenerCore`3::endValue
	Vector2_t2156229523  ___endValue_54;
	// T2 DG.Tweening.Core.TweenerCore`3::changeValue
	Vector2_t2156229523  ___changeValue_55;
	// TPlugOptions DG.Tweening.Core.TweenerCore`3::plugOptions
	VectorOptions_t1354903650  ___plugOptions_56;
	// DG.Tweening.Core.DOGetter`1<T1> DG.Tweening.Core.TweenerCore`3::getter
	DOGetter_1_t1899025727 * ___getter_57;
	// DG.Tweening.Core.DOSetter`1<T1> DG.Tweening.Core.TweenerCore`3::setter
	DOSetter_1_t3206337855 * ___setter_58;
	// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions> DG.Tweening.Core.TweenerCore`3::tweenPlugin
	ABSTweenPlugin_3_t3542497879 * ___tweenPlugin_59;

public:
	inline static int32_t get_offset_of_startValue_53() { return static_cast<int32_t>(offsetof(TweenerCore_3_t4006488229, ___startValue_53)); }
	inline Vector2_t2156229523  get_startValue_53() const { return ___startValue_53; }
	inline Vector2_t2156229523 * get_address_of_startValue_53() { return &___startValue_53; }
	inline void set_startValue_53(Vector2_t2156229523  value)
	{
		___startValue_53 = value;
	}

	inline static int32_t get_offset_of_endValue_54() { return static_cast<int32_t>(offsetof(TweenerCore_3_t4006488229, ___endValue_54)); }
	inline Vector2_t2156229523  get_endValue_54() const { return ___endValue_54; }
	inline Vector2_t2156229523 * get_address_of_endValue_54() { return &___endValue_54; }
	inline void set_endValue_54(Vector2_t2156229523  value)
	{
		___endValue_54 = value;
	}

	inline static int32_t get_offset_of_changeValue_55() { return static_cast<int32_t>(offsetof(TweenerCore_3_t4006488229, ___changeValue_55)); }
	inline Vector2_t2156229523  get_changeValue_55() const { return ___changeValue_55; }
	inline Vector2_t2156229523 * get_address_of_changeValue_55() { return &___changeValue_55; }
	inline void set_changeValue_55(Vector2_t2156229523  value)
	{
		___changeValue_55 = value;
	}

	inline static int32_t get_offset_of_plugOptions_56() { return static_cast<int32_t>(offsetof(TweenerCore_3_t4006488229, ___plugOptions_56)); }
	inline VectorOptions_t1354903650  get_plugOptions_56() const { return ___plugOptions_56; }
	inline VectorOptions_t1354903650 * get_address_of_plugOptions_56() { return &___plugOptions_56; }
	inline void set_plugOptions_56(VectorOptions_t1354903650  value)
	{
		___plugOptions_56 = value;
	}

	inline static int32_t get_offset_of_getter_57() { return static_cast<int32_t>(offsetof(TweenerCore_3_t4006488229, ___getter_57)); }
	inline DOGetter_1_t1899025727 * get_getter_57() const { return ___getter_57; }
	inline DOGetter_1_t1899025727 ** get_address_of_getter_57() { return &___getter_57; }
	inline void set_getter_57(DOGetter_1_t1899025727 * value)
	{
		___getter_57 = value;
		Il2CppCodeGenWriteBarrier((&___getter_57), value);
	}

	inline static int32_t get_offset_of_setter_58() { return static_cast<int32_t>(offsetof(TweenerCore_3_t4006488229, ___setter_58)); }
	inline DOSetter_1_t3206337855 * get_setter_58() const { return ___setter_58; }
	inline DOSetter_1_t3206337855 ** get_address_of_setter_58() { return &___setter_58; }
	inline void set_setter_58(DOSetter_1_t3206337855 * value)
	{
		___setter_58 = value;
		Il2CppCodeGenWriteBarrier((&___setter_58), value);
	}

	inline static int32_t get_offset_of_tweenPlugin_59() { return static_cast<int32_t>(offsetof(TweenerCore_3_t4006488229, ___tweenPlugin_59)); }
	inline ABSTweenPlugin_3_t3542497879 * get_tweenPlugin_59() const { return ___tweenPlugin_59; }
	inline ABSTweenPlugin_3_t3542497879 ** get_address_of_tweenPlugin_59() { return &___tweenPlugin_59; }
	inline void set_tweenPlugin_59(ABSTweenPlugin_3_t3542497879 * value)
	{
		___tweenPlugin_59 = value;
		Il2CppCodeGenWriteBarrier((&___tweenPlugin_59), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENERCORE_3_T4006488229_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t2598313366 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_5)); }
	inline Color_t2555686324  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2555686324 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2555686324  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_7)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_9)); }
	inline Canvas_t3310196443 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t3310196443 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t340375123 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t340375123 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t3648964284 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t3648964284 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_20)); }
	inline Material_t340375123 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t340375123 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_21)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef IMAGE_T2670269651_H
#define IMAGE_T2670269651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image
struct  Image_t2670269651  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t280657092 * ___m_Sprite_29;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t280657092 * ___m_OverrideSprite_30;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_31;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_32;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_33;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_34;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_35;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_36;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_37;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_38;

public:
	inline static int32_t get_offset_of_m_Sprite_29() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Sprite_29)); }
	inline Sprite_t280657092 * get_m_Sprite_29() const { return ___m_Sprite_29; }
	inline Sprite_t280657092 ** get_address_of_m_Sprite_29() { return &___m_Sprite_29; }
	inline void set_m_Sprite_29(Sprite_t280657092 * value)
	{
		___m_Sprite_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sprite_29), value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_30() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_OverrideSprite_30)); }
	inline Sprite_t280657092 * get_m_OverrideSprite_30() const { return ___m_OverrideSprite_30; }
	inline Sprite_t280657092 ** get_address_of_m_OverrideSprite_30() { return &___m_OverrideSprite_30; }
	inline void set_m_OverrideSprite_30(Sprite_t280657092 * value)
	{
		___m_OverrideSprite_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_OverrideSprite_30), value);
	}

	inline static int32_t get_offset_of_m_Type_31() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Type_31)); }
	inline int32_t get_m_Type_31() const { return ___m_Type_31; }
	inline int32_t* get_address_of_m_Type_31() { return &___m_Type_31; }
	inline void set_m_Type_31(int32_t value)
	{
		___m_Type_31 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_32() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_PreserveAspect_32)); }
	inline bool get_m_PreserveAspect_32() const { return ___m_PreserveAspect_32; }
	inline bool* get_address_of_m_PreserveAspect_32() { return &___m_PreserveAspect_32; }
	inline void set_m_PreserveAspect_32(bool value)
	{
		___m_PreserveAspect_32 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_33() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillCenter_33)); }
	inline bool get_m_FillCenter_33() const { return ___m_FillCenter_33; }
	inline bool* get_address_of_m_FillCenter_33() { return &___m_FillCenter_33; }
	inline void set_m_FillCenter_33(bool value)
	{
		___m_FillCenter_33 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_34() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillMethod_34)); }
	inline int32_t get_m_FillMethod_34() const { return ___m_FillMethod_34; }
	inline int32_t* get_address_of_m_FillMethod_34() { return &___m_FillMethod_34; }
	inline void set_m_FillMethod_34(int32_t value)
	{
		___m_FillMethod_34 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_35() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillAmount_35)); }
	inline float get_m_FillAmount_35() const { return ___m_FillAmount_35; }
	inline float* get_address_of_m_FillAmount_35() { return &___m_FillAmount_35; }
	inline void set_m_FillAmount_35(float value)
	{
		___m_FillAmount_35 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_36() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillClockwise_36)); }
	inline bool get_m_FillClockwise_36() const { return ___m_FillClockwise_36; }
	inline bool* get_address_of_m_FillClockwise_36() { return &___m_FillClockwise_36; }
	inline void set_m_FillClockwise_36(bool value)
	{
		___m_FillClockwise_36 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_37() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillOrigin_37)); }
	inline int32_t get_m_FillOrigin_37() const { return ___m_FillOrigin_37; }
	inline int32_t* get_address_of_m_FillOrigin_37() { return &___m_FillOrigin_37; }
	inline void set_m_FillOrigin_37(int32_t value)
	{
		___m_FillOrigin_37 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_38() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_AlphaHitTestMinimumThreshold_38)); }
	inline float get_m_AlphaHitTestMinimumThreshold_38() const { return ___m_AlphaHitTestMinimumThreshold_38; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_38() { return &___m_AlphaHitTestMinimumThreshold_38; }
	inline void set_m_AlphaHitTestMinimumThreshold_38(float value)
	{
		___m_AlphaHitTestMinimumThreshold_38 = value;
	}
};

struct Image_t2670269651_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t340375123 * ___s_ETC1DefaultUI_28;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_t1457185986* ___s_VertScratch_39;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_t1457185986* ___s_UVScratch_40;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t1718750761* ___s_Xy_41;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t1718750761* ___s_Uv_42;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_28() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_ETC1DefaultUI_28)); }
	inline Material_t340375123 * get_s_ETC1DefaultUI_28() const { return ___s_ETC1DefaultUI_28; }
	inline Material_t340375123 ** get_address_of_s_ETC1DefaultUI_28() { return &___s_ETC1DefaultUI_28; }
	inline void set_s_ETC1DefaultUI_28(Material_t340375123 * value)
	{
		___s_ETC1DefaultUI_28 = value;
		Il2CppCodeGenWriteBarrier((&___s_ETC1DefaultUI_28), value);
	}

	inline static int32_t get_offset_of_s_VertScratch_39() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_VertScratch_39)); }
	inline Vector2U5BU5D_t1457185986* get_s_VertScratch_39() const { return ___s_VertScratch_39; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_VertScratch_39() { return &___s_VertScratch_39; }
	inline void set_s_VertScratch_39(Vector2U5BU5D_t1457185986* value)
	{
		___s_VertScratch_39 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertScratch_39), value);
	}

	inline static int32_t get_offset_of_s_UVScratch_40() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_UVScratch_40)); }
	inline Vector2U5BU5D_t1457185986* get_s_UVScratch_40() const { return ___s_UVScratch_40; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_UVScratch_40() { return &___s_UVScratch_40; }
	inline void set_s_UVScratch_40(Vector2U5BU5D_t1457185986* value)
	{
		___s_UVScratch_40 = value;
		Il2CppCodeGenWriteBarrier((&___s_UVScratch_40), value);
	}

	inline static int32_t get_offset_of_s_Xy_41() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Xy_41)); }
	inline Vector3U5BU5D_t1718750761* get_s_Xy_41() const { return ___s_Xy_41; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Xy_41() { return &___s_Xy_41; }
	inline void set_s_Xy_41(Vector3U5BU5D_t1718750761* value)
	{
		___s_Xy_41 = value;
		Il2CppCodeGenWriteBarrier((&___s_Xy_41), value);
	}

	inline static int32_t get_offset_of_s_Uv_42() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Uv_42)); }
	inline Vector3U5BU5D_t1718750761* get_s_Uv_42() const { return ___s_Uv_42; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Uv_42() { return &___s_Uv_42; }
	inline void set_s_Uv_42(Vector3U5BU5D_t1718750761* value)
	{
		___s_Uv_42 = value;
		Il2CppCodeGenWriteBarrier((&___s_Uv_42), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T2670269651_H
#ifndef TEXT_T1901882714_H
#define TEXT_T1901882714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t1901882714  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t746620069 * ___m_FontData_28;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_29;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t3211863866 * ___m_TextCache_30;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t3211863866 * ___m_TextCacheForLayout_31;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_33;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t1981460040* ___m_TempVerts_34;

public:
	inline static int32_t get_offset_of_m_FontData_28() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_FontData_28)); }
	inline FontData_t746620069 * get_m_FontData_28() const { return ___m_FontData_28; }
	inline FontData_t746620069 ** get_address_of_m_FontData_28() { return &___m_FontData_28; }
	inline void set_m_FontData_28(FontData_t746620069 * value)
	{
		___m_FontData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_28), value);
	}

	inline static int32_t get_offset_of_m_Text_29() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_Text_29)); }
	inline String_t* get_m_Text_29() const { return ___m_Text_29; }
	inline String_t** get_address_of_m_Text_29() { return &___m_Text_29; }
	inline void set_m_Text_29(String_t* value)
	{
		___m_Text_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_29), value);
	}

	inline static int32_t get_offset_of_m_TextCache_30() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCache_30)); }
	inline TextGenerator_t3211863866 * get_m_TextCache_30() const { return ___m_TextCache_30; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCache_30() { return &___m_TextCache_30; }
	inline void set_m_TextCache_30(TextGenerator_t3211863866 * value)
	{
		___m_TextCache_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_30), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_31() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCacheForLayout_31)); }
	inline TextGenerator_t3211863866 * get_m_TextCacheForLayout_31() const { return ___m_TextCacheForLayout_31; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCacheForLayout_31() { return &___m_TextCacheForLayout_31; }
	inline void set_m_TextCacheForLayout_31(TextGenerator_t3211863866 * value)
	{
		___m_TextCacheForLayout_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_31), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_33() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_DisableFontTextureRebuiltCallback_33)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_33() const { return ___m_DisableFontTextureRebuiltCallback_33; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_33() { return &___m_DisableFontTextureRebuiltCallback_33; }
	inline void set_m_DisableFontTextureRebuiltCallback_33(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_33 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_34() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TempVerts_34)); }
	inline UIVertexU5BU5D_t1981460040* get_m_TempVerts_34() const { return ___m_TempVerts_34; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_TempVerts_34() { return &___m_TempVerts_34; }
	inline void set_m_TempVerts_34(UIVertexU5BU5D_t1981460040* value)
	{
		___m_TempVerts_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_34), value);
	}
};

struct Text_t1901882714_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t340375123 * ___s_DefaultText_32;

public:
	inline static int32_t get_offset_of_s_DefaultText_32() { return static_cast<int32_t>(offsetof(Text_t1901882714_StaticFields, ___s_DefaultText_32)); }
	inline Material_t340375123 * get_s_DefaultText_32() const { return ___s_DefaultText_32; }
	inline Material_t340375123 ** get_address_of_s_DefaultText_32() { return &___s_DefaultText_32; }
	inline void set_s_DefaultText_32(Material_t340375123 * value)
	{
		___s_DefaultText_32 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T1901882714_H


// System.Void DG.Tweening.Core.DOGetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2335505072_gshared (DOGetter_1_t1140062978 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m3423399374_gshared (DOSetter_1_t2447375106 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<System.Object>(!!0,System.Object)
extern "C"  RuntimeObject * TweenSettingsExtensions_SetTarget_TisRuntimeObject_m179229169_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m1558284636_gshared (DOGetter_1_t2298482528 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m380718107_gshared (DOSetter_1_t3605794656 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m3327751853_gshared (DOGetter_1_t1899025727 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2986952954_gshared (DOSetter_1_t3206337855 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m3773977789_gshared (DOGetter_1_t3465109668 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m4237574066_gshared (DOSetter_1_t477454500 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// !!0 DG.Tweening.Core.Extensions::SetSpecialStartupMode<System.Object>(!!0,DG.Tweening.Core.Enums.SpecialStartupMode)
extern "C"  RuntimeObject * Extensions_SetSpecialStartupMode_TisRuntimeObject_m2505848042_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, int32_t p1, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOGetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m1915675248_gshared (DOGetter_1_t2822902368 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m1914655859_gshared (DOSetter_1_t4130214496 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);

// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C"  Rect_t2360479859  RectTransform_get_rect_m574169965 (RectTransform_t3704657025 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_width()
extern "C"  float Rect_get_width_m3421484486 (Rect_t2360479859 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_xMin()
extern "C"  float Rect_get_xMin_m581135837 (Rect_t2360479859 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m1358425599 (Rect_t2360479859 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_yMin()
extern "C"  float Rect_get_yMin_m2601414109 (Rect_t2360479859 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3970636864 (Vector2_t2156229523 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3)
extern "C"  Vector2_t2156229523  RectTransformUtility_WorldToScreenPoint_m3914148572 (RuntimeObject * __this /* static, unused */, Camera_t4157153871 * p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2156229523  Vector2_op_Addition_m800700293 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToLocalPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector2&)
extern "C"  bool RectTransformUtility_ScreenPointToLocalPointInRectangle_m2327269187 (RuntimeObject * __this /* static, unused */, RectTransform_t3704657025 * p0, Vector2_t2156229523  p1, Camera_t4157153871 * p2, Vector2_t2156229523 * p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
extern "C"  Vector2_t2156229523  RectTransform_get_anchoredPosition_m1813443094 (RectTransform_t3704657025 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2156229523  Vector2_op_Subtraction_m73004381 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass0_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass0_0__ctor_m1227527790 (U3CU3Ec__DisplayClass0_0_t892042862 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOGetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
#define DOGetter_1__ctor_m2335505072(__this, p0, p1, method) ((  void (*) (DOGetter_1_t1140062978 *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOGetter_1__ctor_m2335505072_gshared)(__this, p0, p1, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
#define DOSetter_1__ctor_m3423399374(__this, p0, p1, method) ((  void (*) (DOSetter_1_t2447375106 *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOSetter_1__ctor_m3423399374_gshared)(__this, p0, p1, method)
// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<System.Single>,DG.Tweening.Core.DOSetter`1<System.Single>,System.Single,System.Single)
extern "C"  TweenerCore_3_t884556411 * DOTween_To_m2106473129 (RuntimeObject * __this /* static, unused */, DOGetter_1_t1140062978 * p0, DOSetter_1_t2447375106 * p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>>(!!0,System.Object)
#define TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t884556411_m1318424630(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t884556411 * (*) (RuntimeObject * /* static, unused */, TweenerCore_3_t884556411 *, RuntimeObject *, const RuntimeMethod*))TweenSettingsExtensions_SetTarget_TisRuntimeObject_m179229169_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass3_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass3_0__ctor_m1227532699 (U3CU3Ec__DisplayClass3_0_t1295327389 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
#define DOGetter_1__ctor_m1558284636(__this, p0, p1, method) ((  void (*) (DOGetter_1_t2298482528 *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOGetter_1__ctor_m1558284636_gshared)(__this, p0, p1, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
#define DOSetter_1__ctor_m380718107(__this, p0, p1, method) ((  void (*) (DOSetter_1_t3605794656 *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOSetter_1__ctor_m380718107_gshared)(__this, p0, p1, method)
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Color>,DG.Tweening.Core.DOSetter`1<UnityEngine.Color>,UnityEngine.Color,System.Single)
extern "C"  TweenerCore_3_t745532282 * DOTween_To_m3257840982 (RuntimeObject * __this /* static, unused */, DOGetter_1_t2298482528 * p0, DOSetter_1_t3605794656 * p1, Color_t2555686324  p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>>(!!0,System.Object)
#define TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t745532282_m2904780857(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t745532282 * (*) (RuntimeObject * /* static, unused */, TweenerCore_3_t745532282 *, RuntimeObject *, const RuntimeMethod*))TweenSettingsExtensions_SetTarget_TisRuntimeObject_m179229169_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass4_0__ctor_m1227523442 (U3CU3Ec__DisplayClass4_0_t2861411330 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.DOTween::ToAlpha(DG.Tweening.Core.DOGetter`1<UnityEngine.Color>,DG.Tweening.Core.DOSetter`1<UnityEngine.Color>,System.Single,System.Single)
extern "C"  Tweener_t436044680 * DOTween_ToAlpha_m1646959083 (RuntimeObject * __this /* static, unused */, DOGetter_1_t2298482528 * p0, DOSetter_1_t3605794656 * p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Tweener>(!!0,System.Object)
#define TweenSettingsExtensions_SetTarget_TisTweener_t436044680_m598017345(__this /* static, unused */, p0, p1, method) ((  Tweener_t436044680 * (*) (RuntimeObject * /* static, unused */, Tweener_t436044680 *, RuntimeObject *, const RuntimeMethod*))TweenSettingsExtensions_SetTarget_TisRuntimeObject_m179229169_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass13_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass13_0__ctor_m4182001830 (U3CU3Ec__DisplayClass13_0_t2635598378 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
#define DOGetter_1__ctor_m3327751853(__this, p0, p1, method) ((  void (*) (DOGetter_1_t1899025727 *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOGetter_1__ctor_m3327751853_gshared)(__this, p0, p1, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
#define DOSetter_1__ctor_m2986952954(__this, p0, p1, method) ((  void (*) (DOSetter_1_t3206337855 *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOSetter_1__ctor_m2986952954_gshared)(__this, p0, p1, method)
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>,UnityEngine.Vector2,System.Single)
extern "C"  TweenerCore_3_t4006488229 * DOTween_To_m3593210828 (RuntimeObject * __this /* static, unused */, DOGetter_1_t1899025727 * p0, DOSetter_1_t3206337855 * p1, Vector2_t2156229523  p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>,System.Boolean)
extern "C"  Tweener_t436044680 * TweenSettingsExtensions_SetOptions_m2879183578 (RuntimeObject * __this /* static, unused */, TweenerCore_3_t4006488229 * p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass16_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass16_0__ctor_m2441169062 (U3CU3Ec__DisplayClass16_0_t1443957290 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
#define DOGetter_1__ctor_m3773977789(__this, p0, p1, method) ((  void (*) (DOGetter_1_t3465109668 *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOGetter_1__ctor_m3773977789_gshared)(__this, p0, p1, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
#define DOSetter_1__ctor_m4237574066(__this, p0, p1, method) ((  void (*) (DOSetter_1_t477454500 *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOSetter_1__ctor_m4237574066_gshared)(__this, p0, p1, method)
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,UnityEngine.Vector3,System.Single)
extern "C"  TweenerCore_3_t2944330537 * DOTween_To_m2364274163 (RuntimeObject * __this /* static, unused */, DOGetter_1_t3465109668 * p0, DOSetter_1_t477454500 * p1, Vector3_t3722313464  p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>,System.Boolean)
extern "C"  Tweener_t436044680 * TweenSettingsExtensions_SetOptions_m1078069392 (RuntimeObject * __this /* static, unused */, TweenerCore_3_t2944330537 * p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass22_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass22_0__ctor_m1558496271 (U3CU3Ec__DisplayClass22_0_t2245367183 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass23_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass23_0__ctor_m4181967889 (U3CU3Ec__DisplayClass23_0_t4201682319 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t3722313464  Vector2_op_Implicit_m1860157806 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions> DG.Tweening.DOTween::Punch(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,UnityEngine.Vector3,System.Single,System.Int32,System.Single)
extern "C"  TweenerCore_3_t951858585 * DOTween_Punch_m807939188 (RuntimeObject * __this /* static, unused */, DOGetter_1_t3465109668 * p0, DOSetter_1_t477454500 * p1, Vector3_t3722313464  p2, float p3, int32_t p4, float p5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>>(!!0,System.Object)
#define TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t951858585_m2980331507(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t951858585 * (*) (RuntimeObject * /* static, unused */, TweenerCore_3_t951858585 *, RuntimeObject *, const RuntimeMethod*))TweenSettingsExtensions_SetTarget_TisRuntimeObject_m179229169_gshared)(__this /* static, unused */, p0, p1, method)
// DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>,System.Boolean)
extern "C"  Tweener_t436044680 * TweenSettingsExtensions_SetOptions_m288553191 (RuntimeObject * __this /* static, unused */, TweenerCore_3_t951858585 * p0, bool p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass25_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass25_0__ctor_m302629890 (U3CU3Ec__DisplayClass25_0_t289052047 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions> DG.Tweening.DOTween::Shake(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,System.Single,UnityEngine.Vector3,System.Int32,System.Single,System.Boolean)
extern "C"  TweenerCore_3_t951858585 * DOTween_Shake_m3409041061 (RuntimeObject * __this /* static, unused */, DOGetter_1_t3465109668 * p0, DOSetter_1_t477454500 * p1, float p2, Vector3_t3722313464  p3, int32_t p4, float p5, bool p6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 DG.Tweening.Core.Extensions::SetSpecialStartupMode<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>>(!!0,DG.Tweening.Core.Enums.SpecialStartupMode)
#define Extensions_SetSpecialStartupMode_TisTweenerCore_3_t951858585_m1835023067(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t951858585 * (*) (RuntimeObject * /* static, unused */, TweenerCore_3_t951858585 *, int32_t, const RuntimeMethod*))Extensions_SetSpecialStartupMode_TisRuntimeObject_m2505848042_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass31_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass31_0__ctor_m631190223 (U3CU3Ec__DisplayClass31_0_t1090461940 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass32_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass32_0__ctor_m1558590154 (U3CU3Ec__DisplayClass32_0_t3811451124 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass33_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass33_0__ctor_m4182061772 (U3CU3Ec__DisplayClass33_0_t1472798964 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOGetter`1<System.String>::.ctor(System.Object,System.IntPtr)
#define DOGetter_1__ctor_m2531950179(__this, p0, p1, method) ((  void (*) (DOGetter_1_t1590246893 *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOGetter_1__ctor_m1915675248_gshared)(__this, p0, p1, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.String>::.ctor(System.Object,System.IntPtr)
#define DOSetter_1__ctor_m932311154(__this, p0, p1, method) ((  void (*) (DOSetter_1_t2897559021 *, RuntimeObject *, intptr_t, const RuntimeMethod*))DOSetter_1__ctor_m1914655859_gshared)(__this, p0, p1, method)
// DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<System.String>,DG.Tweening.Core.DOSetter`1<System.String>,System.String,System.Single)
extern "C"  TweenerCore_3_t2915539799 * DOTween_To_m4191053537 (RuntimeObject * __this /* static, unused */, DOGetter_1_t1590246893 * p0, DOSetter_1_t2897559021 * p1, String_t* p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>,System.Boolean,DG.Tweening.ScrambleMode,System.String)
extern "C"  Tweener_t436044680 * TweenSettingsExtensions_SetOptions_m3956807361 (RuntimeObject * __this /* static, unused */, TweenerCore_3_t2915539799 * p0, bool p1, int32_t p2, String_t* p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.CanvasGroup::get_alpha()
extern "C"  float CanvasGroup_get_alpha_m2918269489 (CanvasGroup_t4083511760 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CanvasGroup::set_alpha(System.Single)
extern "C"  void CanvasGroup_set_alpha_m4032573 (CanvasGroup_t4083511760 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchoredPosition_m4126691837 (RectTransform_t3704657025 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.RectTransform::get_anchoredPosition3D()
extern "C"  Vector3_t3722313464  RectTransform_get_anchoredPosition3D_m3487933542 (RectTransform_t3704657025 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchoredPosition3D(UnityEngine.Vector3)
extern "C"  void RectTransform_set_anchoredPosition3D_m2642675894 (RectTransform_t3704657025 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C"  Vector2_t2156229523  RectTransform_get_sizeDelta_m2183112744 (RectTransform_t3704657025 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C"  void RectTransform_set_sizeDelta_m3462269772 (RectTransform_t3704657025 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t2156229523  Vector2_op_Implicit_m4260192859 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector2 DG.Tweening.DOTweenUtils46::SwitchToRectTransform(UnityEngine.RectTransform,UnityEngine.RectTransform)
extern "C"  Vector2_t2156229523  DOTweenUtils46_SwitchToRectTransform_m3891122406 (RuntimeObject * __this /* static, unused */, RectTransform_t3704657025 * ___from0, RectTransform_t3704657025 * ___to1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenUtils46_SwitchToRectTransform_m3891122406_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2156229523  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2156229523  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t2360479859  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		RectTransform_t3704657025 * L_0 = ___from0;
		NullCheck(L_0);
		Rect_t2360479859  L_1 = RectTransform_get_rect_m574169965(L_0, /*hidden argument*/NULL);
		V_4 = L_1;
		float L_2 = Rect_get_width_m3421484486((&V_4), /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_3 = ___from0;
		NullCheck(L_3);
		Rect_t2360479859  L_4 = RectTransform_get_rect_m574169965(L_3, /*hidden argument*/NULL);
		V_4 = L_4;
		float L_5 = Rect_get_xMin_m581135837((&V_4), /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_6 = ___from0;
		NullCheck(L_6);
		Rect_t2360479859  L_7 = RectTransform_get_rect_m574169965(L_6, /*hidden argument*/NULL);
		V_4 = L_7;
		float L_8 = Rect_get_height_m1358425599((&V_4), /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_9 = ___from0;
		NullCheck(L_9);
		Rect_t2360479859  L_10 = RectTransform_get_rect_m574169965(L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		float L_11 = Rect_get_yMin_m2601414109((&V_4), /*hidden argument*/NULL);
		Vector2__ctor_m3970636864((&V_1), ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_2, (float)(0.5f))), (float)L_5)), ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_8, (float)(0.5f))), (float)L_11)), /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_12 = ___from0;
		NullCheck(L_12);
		Vector3_t3722313464  L_13 = Transform_get_position_m36019626(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1743242446_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_14 = RectTransformUtility_WorldToScreenPoint_m3914148572(NULL /*static, unused*/, (Camera_t4157153871 *)NULL, L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		Vector2_t2156229523  L_15 = V_2;
		Vector2_t2156229523  L_16 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_17 = Vector2_op_Addition_m800700293(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		V_2 = L_17;
		RectTransform_t3704657025 * L_18 = ___to1;
		Vector2_t2156229523  L_19 = V_2;
		RectTransformUtility_ScreenPointToLocalPointInRectangle_m2327269187(NULL /*static, unused*/, L_18, L_19, (Camera_t4157153871 *)NULL, (&V_0), /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_20 = ___to1;
		NullCheck(L_20);
		Rect_t2360479859  L_21 = RectTransform_get_rect_m574169965(L_20, /*hidden argument*/NULL);
		V_4 = L_21;
		float L_22 = Rect_get_width_m3421484486((&V_4), /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_23 = ___to1;
		NullCheck(L_23);
		Rect_t2360479859  L_24 = RectTransform_get_rect_m574169965(L_23, /*hidden argument*/NULL);
		V_4 = L_24;
		float L_25 = Rect_get_xMin_m581135837((&V_4), /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_26 = ___to1;
		NullCheck(L_26);
		Rect_t2360479859  L_27 = RectTransform_get_rect_m574169965(L_26, /*hidden argument*/NULL);
		V_4 = L_27;
		float L_28 = Rect_get_height_m1358425599((&V_4), /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_29 = ___to1;
		NullCheck(L_29);
		Rect_t2360479859  L_30 = RectTransform_get_rect_m574169965(L_29, /*hidden argument*/NULL);
		V_4 = L_30;
		float L_31 = Rect_get_yMin_m2601414109((&V_4), /*hidden argument*/NULL);
		Vector2__ctor_m3970636864((&V_3), ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_22, (float)(0.5f))), (float)L_25)), ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_28, (float)(0.5f))), (float)L_31)), /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_32 = ___to1;
		NullCheck(L_32);
		Vector2_t2156229523  L_33 = RectTransform_get_anchoredPosition_m1813443094(L_32, /*hidden argument*/NULL);
		Vector2_t2156229523  L_34 = V_0;
		Vector2_t2156229523  L_35 = Vector2_op_Addition_m800700293(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		Vector2_t2156229523  L_36 = V_3;
		Vector2_t2156229523  L_37 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		return L_37;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOFade(UnityEngine.CanvasGroup,System.Single,System.Single)
extern "C"  Tweener_t436044680 * ShortcutExtensions46_DOFade_m3189841635 (RuntimeObject * __this /* static, unused */, CanvasGroup_t4083511760 * ___target0, float ___endValue1, float ___duration2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOFade_m3189841635_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass0_0_t892042862 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass0_0_t892042862 * L_0 = (U3CU3Ec__DisplayClass0_0_t892042862 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass0_0_t892042862_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass0_0__ctor_m1227527790(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass0_0_t892042862 * L_1 = V_0;
		CanvasGroup_t4083511760 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass0_0_t892042862 * L_3 = V_0;
		intptr_t L_4 = (intptr_t)U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m1136460969_RuntimeMethod_var;
		DOGetter_1_t1140062978 * L_5 = (DOGetter_1_t1140062978 *)il2cpp_codegen_object_new(DOGetter_1_t1140062978_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m2335505072(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m2335505072_RuntimeMethod_var);
		U3CU3Ec__DisplayClass0_0_t892042862 * L_6 = V_0;
		intptr_t L_7 = (intptr_t)U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_m737149533_RuntimeMethod_var;
		DOSetter_1_t2447375106 * L_8 = (DOSetter_1_t2447375106 *)il2cpp_codegen_object_new(DOSetter_1_t2447375106_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m3423399374(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m3423399374_RuntimeMethod_var);
		float L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2744875806_il2cpp_TypeInfo_var);
		TweenerCore_3_t884556411 * L_11 = DOTween_To_m2106473129(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass0_0_t892042862 * L_12 = V_0;
		NullCheck(L_12);
		CanvasGroup_t4083511760 * L_13 = L_12->get_target_0();
		TweenerCore_3_t884556411 * L_14 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t884556411_m1318424630(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t884556411_m1318424630_RuntimeMethod_var);
		return L_14;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern "C"  Tweener_t436044680 * ShortcutExtensions46_DOColor_m3633025588 (RuntimeObject * __this /* static, unused */, Image_t2670269651 * ___target0, Color_t2555686324  ___endValue1, float ___duration2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOColor_m3633025588_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass3_0_t1295327389 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass3_0_t1295327389 * L_0 = (U3CU3Ec__DisplayClass3_0_t1295327389 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass3_0_t1295327389_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass3_0__ctor_m1227532699(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass3_0_t1295327389 * L_1 = V_0;
		Image_t2670269651 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass3_0_t1295327389 * L_3 = V_0;
		intptr_t L_4 = (intptr_t)U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_m2988152837_RuntimeMethod_var;
		DOGetter_1_t2298482528 * L_5 = (DOGetter_1_t2298482528 *)il2cpp_codegen_object_new(DOGetter_1_t2298482528_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m1558284636(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m1558284636_RuntimeMethod_var);
		U3CU3Ec__DisplayClass3_0_t1295327389 * L_6 = V_0;
		intptr_t L_7 = (intptr_t)U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_m1674238268_RuntimeMethod_var;
		DOSetter_1_t3605794656 * L_8 = (DOSetter_1_t3605794656 *)il2cpp_codegen_object_new(DOSetter_1_t3605794656_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m380718107(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m380718107_RuntimeMethod_var);
		Color_t2555686324  L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2744875806_il2cpp_TypeInfo_var);
		TweenerCore_3_t745532282 * L_11 = DOTween_To_m3257840982(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass3_0_t1295327389 * L_12 = V_0;
		NullCheck(L_12);
		Image_t2670269651 * L_13 = L_12->get_target_0();
		TweenerCore_3_t745532282 * L_14 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t745532282_m2904780857(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t745532282_m2904780857_RuntimeMethod_var);
		return L_14;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOFade(UnityEngine.UI.Image,System.Single,System.Single)
extern "C"  Tweener_t436044680 * ShortcutExtensions46_DOFade_m4177432048 (RuntimeObject * __this /* static, unused */, Image_t2670269651 * ___target0, float ___endValue1, float ___duration2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOFade_m4177432048_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass4_0_t2861411330 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass4_0_t2861411330 * L_0 = (U3CU3Ec__DisplayClass4_0_t2861411330 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass4_0_t2861411330_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass4_0__ctor_m1227523442(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass4_0_t2861411330 * L_1 = V_0;
		Image_t2670269651 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass4_0_t2861411330 * L_3 = V_0;
		intptr_t L_4 = (intptr_t)U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m4078431796_RuntimeMethod_var;
		DOGetter_1_t2298482528 * L_5 = (DOGetter_1_t2298482528 *)il2cpp_codegen_object_new(DOGetter_1_t2298482528_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m1558284636(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m1558284636_RuntimeMethod_var);
		U3CU3Ec__DisplayClass4_0_t2861411330 * L_6 = V_0;
		intptr_t L_7 = (intptr_t)U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m1839193484_RuntimeMethod_var;
		DOSetter_1_t3605794656 * L_8 = (DOSetter_1_t3605794656 *)il2cpp_codegen_object_new(DOSetter_1_t3605794656_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m380718107(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m380718107_RuntimeMethod_var);
		float L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2744875806_il2cpp_TypeInfo_var);
		Tweener_t436044680 * L_11 = DOTween_ToAlpha_m1646959083(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass4_0_t2861411330 * L_12 = V_0;
		NullCheck(L_12);
		Image_t2670269651 * L_13 = L_12->get_target_0();
		Tweener_t436044680 * L_14 = TweenSettingsExtensions_SetTarget_TisTweener_t436044680_m598017345(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t436044680_m598017345_RuntimeMethod_var);
		return L_14;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern "C"  Tweener_t436044680 * ShortcutExtensions46_DOAnchorPos_m2186168858 (RuntimeObject * __this /* static, unused */, RectTransform_t3704657025 * ___target0, Vector2_t2156229523  ___endValue1, float ___duration2, bool ___snapping3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOAnchorPos_m2186168858_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass13_0_t2635598378 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass13_0_t2635598378 * L_0 = (U3CU3Ec__DisplayClass13_0_t2635598378 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass13_0_t2635598378_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass13_0__ctor_m4182001830(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass13_0_t2635598378 * L_1 = V_0;
		RectTransform_t3704657025 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass13_0_t2635598378 * L_3 = V_0;
		intptr_t L_4 = (intptr_t)U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__0_m4202884669_RuntimeMethod_var;
		DOGetter_1_t1899025727 * L_5 = (DOGetter_1_t1899025727 *)il2cpp_codegen_object_new(DOGetter_1_t1899025727_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m3327751853(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m3327751853_RuntimeMethod_var);
		U3CU3Ec__DisplayClass13_0_t2635598378 * L_6 = V_0;
		intptr_t L_7 = (intptr_t)U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__1_m58758457_RuntimeMethod_var;
		DOSetter_1_t3206337855 * L_8 = (DOSetter_1_t3206337855 *)il2cpp_codegen_object_new(DOSetter_1_t3206337855_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m2986952954(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m2986952954_RuntimeMethod_var);
		Vector2_t2156229523  L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2744875806_il2cpp_TypeInfo_var);
		TweenerCore_3_t4006488229 * L_11 = DOTween_To_m3593210828(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		bool L_12 = ___snapping3;
		Tweener_t436044680 * L_13 = TweenSettingsExtensions_SetOptions_m2879183578(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass13_0_t2635598378 * L_14 = V_0;
		NullCheck(L_14);
		RectTransform_t3704657025 * L_15 = L_14->get_target_0();
		Tweener_t436044680 * L_16 = TweenSettingsExtensions_SetTarget_TisTweener_t436044680_m598017345(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t436044680_m598017345_RuntimeMethod_var);
		return L_16;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOAnchorPos3D(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Boolean)
extern "C"  Tweener_t436044680 * ShortcutExtensions46_DOAnchorPos3D_m2494478347 (RuntimeObject * __this /* static, unused */, RectTransform_t3704657025 * ___target0, Vector3_t3722313464  ___endValue1, float ___duration2, bool ___snapping3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOAnchorPos3D_m2494478347_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass16_0_t1443957290 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass16_0_t1443957290 * L_0 = (U3CU3Ec__DisplayClass16_0_t1443957290 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass16_0_t1443957290_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass16_0__ctor_m2441169062(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass16_0_t1443957290 * L_1 = V_0;
		RectTransform_t3704657025 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass16_0_t1443957290 * L_3 = V_0;
		intptr_t L_4 = (intptr_t)U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_m1327669346_RuntimeMethod_var;
		DOGetter_1_t3465109668 * L_5 = (DOGetter_1_t3465109668 *)il2cpp_codegen_object_new(DOGetter_1_t3465109668_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m3773977789(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m3773977789_RuntimeMethod_var);
		U3CU3Ec__DisplayClass16_0_t1443957290 * L_6 = V_0;
		intptr_t L_7 = (intptr_t)U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_m3318326923_RuntimeMethod_var;
		DOSetter_1_t477454500 * L_8 = (DOSetter_1_t477454500 *)il2cpp_codegen_object_new(DOSetter_1_t477454500_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m4237574066(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m4237574066_RuntimeMethod_var);
		Vector3_t3722313464  L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2744875806_il2cpp_TypeInfo_var);
		TweenerCore_3_t2944330537 * L_11 = DOTween_To_m2364274163(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		bool L_12 = ___snapping3;
		Tweener_t436044680 * L_13 = TweenSettingsExtensions_SetOptions_m1078069392(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass16_0_t1443957290 * L_14 = V_0;
		NullCheck(L_14);
		RectTransform_t3704657025 * L_15 = L_14->get_target_0();
		Tweener_t436044680 * L_16 = TweenSettingsExtensions_SetTarget_TisTweener_t436044680_m598017345(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t436044680_m598017345_RuntimeMethod_var);
		return L_16;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOSizeDelta(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern "C"  Tweener_t436044680 * ShortcutExtensions46_DOSizeDelta_m959638426 (RuntimeObject * __this /* static, unused */, RectTransform_t3704657025 * ___target0, Vector2_t2156229523  ___endValue1, float ___duration2, bool ___snapping3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOSizeDelta_m959638426_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass22_0_t2245367183 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass22_0_t2245367183 * L_0 = (U3CU3Ec__DisplayClass22_0_t2245367183 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass22_0_t2245367183_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass22_0__ctor_m1558496271(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass22_0_t2245367183 * L_1 = V_0;
		RectTransform_t3704657025 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass22_0_t2245367183 * L_3 = V_0;
		intptr_t L_4 = (intptr_t)U3CU3Ec__DisplayClass22_0_U3CDOSizeDeltaU3Eb__0_m194433554_RuntimeMethod_var;
		DOGetter_1_t1899025727 * L_5 = (DOGetter_1_t1899025727 *)il2cpp_codegen_object_new(DOGetter_1_t1899025727_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m3327751853(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m3327751853_RuntimeMethod_var);
		U3CU3Ec__DisplayClass22_0_t2245367183 * L_6 = V_0;
		intptr_t L_7 = (intptr_t)U3CU3Ec__DisplayClass22_0_U3CDOSizeDeltaU3Eb__1_m1120729751_RuntimeMethod_var;
		DOSetter_1_t3206337855 * L_8 = (DOSetter_1_t3206337855 *)il2cpp_codegen_object_new(DOSetter_1_t3206337855_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m2986952954(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m2986952954_RuntimeMethod_var);
		Vector2_t2156229523  L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2744875806_il2cpp_TypeInfo_var);
		TweenerCore_3_t4006488229 * L_11 = DOTween_To_m3593210828(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		bool L_12 = ___snapping3;
		Tweener_t436044680 * L_13 = TweenSettingsExtensions_SetOptions_m2879183578(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass22_0_t2245367183 * L_14 = V_0;
		NullCheck(L_14);
		RectTransform_t3704657025 * L_15 = L_14->get_target_0();
		Tweener_t436044680 * L_16 = TweenSettingsExtensions_SetTarget_TisTweener_t436044680_m598017345(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t436044680_m598017345_RuntimeMethod_var);
		return L_16;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOPunchAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern "C"  Tweener_t436044680 * ShortcutExtensions46_DOPunchAnchorPos_m1059198467 (RuntimeObject * __this /* static, unused */, RectTransform_t3704657025 * ___target0, Vector2_t2156229523  ___punch1, float ___duration2, int32_t ___vibrato3, float ___elasticity4, bool ___snapping5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOPunchAnchorPos_m1059198467_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass23_0_t4201682319 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass23_0_t4201682319 * L_0 = (U3CU3Ec__DisplayClass23_0_t4201682319 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass23_0_t4201682319_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass23_0__ctor_m4181967889(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass23_0_t4201682319 * L_1 = V_0;
		RectTransform_t3704657025 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass23_0_t4201682319 * L_3 = V_0;
		intptr_t L_4 = (intptr_t)U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__0_m2475830852_RuntimeMethod_var;
		DOGetter_1_t3465109668 * L_5 = (DOGetter_1_t3465109668 *)il2cpp_codegen_object_new(DOGetter_1_t3465109668_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m3773977789(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m3773977789_RuntimeMethod_var);
		U3CU3Ec__DisplayClass23_0_t4201682319 * L_6 = V_0;
		intptr_t L_7 = (intptr_t)U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__1_m2684855292_RuntimeMethod_var;
		DOSetter_1_t477454500 * L_8 = (DOSetter_1_t477454500 *)il2cpp_codegen_object_new(DOSetter_1_t477454500_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m4237574066(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m4237574066_RuntimeMethod_var);
		Vector2_t2156229523  L_9 = ___punch1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_10 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		float L_11 = ___duration2;
		int32_t L_12 = ___vibrato3;
		float L_13 = ___elasticity4;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2744875806_il2cpp_TypeInfo_var);
		TweenerCore_3_t951858585 * L_14 = DOTween_Punch_m807939188(NULL /*static, unused*/, L_5, L_8, L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass23_0_t4201682319 * L_15 = V_0;
		NullCheck(L_15);
		RectTransform_t3704657025 * L_16 = L_15->get_target_0();
		TweenerCore_3_t951858585 * L_17 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t951858585_m2980331507(NULL /*static, unused*/, L_14, L_16, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t951858585_m2980331507_RuntimeMethod_var);
		bool L_18 = ___snapping5;
		Tweener_t436044680 * L_19 = TweenSettingsExtensions_SetOptions_m288553191(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		return L_19;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,UnityEngine.Vector2,System.Int32,System.Single,System.Boolean,System.Boolean)
extern "C"  Tweener_t436044680 * ShortcutExtensions46_DOShakeAnchorPos_m1729623556 (RuntimeObject * __this /* static, unused */, RectTransform_t3704657025 * ___target0, float ___duration1, Vector2_t2156229523  ___strength2, int32_t ___vibrato3, float ___randomness4, bool ___snapping5, bool ___fadeOut6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOShakeAnchorPos_m1729623556_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass25_0_t289052047 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass25_0_t289052047 * L_0 = (U3CU3Ec__DisplayClass25_0_t289052047 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass25_0_t289052047_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass25_0__ctor_m302629890(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass25_0_t289052047 * L_1 = V_0;
		RectTransform_t3704657025 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass25_0_t289052047 * L_3 = V_0;
		intptr_t L_4 = (intptr_t)U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__0_m290566680_RuntimeMethod_var;
		DOGetter_1_t3465109668 * L_5 = (DOGetter_1_t3465109668 *)il2cpp_codegen_object_new(DOGetter_1_t3465109668_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m3773977789(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m3773977789_RuntimeMethod_var);
		U3CU3Ec__DisplayClass25_0_t289052047 * L_6 = V_0;
		intptr_t L_7 = (intptr_t)U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__1_m2933663977_RuntimeMethod_var;
		DOSetter_1_t477454500 * L_8 = (DOSetter_1_t477454500 *)il2cpp_codegen_object_new(DOSetter_1_t477454500_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m4237574066(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m4237574066_RuntimeMethod_var);
		float L_9 = ___duration1;
		Vector2_t2156229523  L_10 = ___strength2;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_11 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		int32_t L_12 = ___vibrato3;
		float L_13 = ___randomness4;
		bool L_14 = ___fadeOut6;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2744875806_il2cpp_TypeInfo_var);
		TweenerCore_3_t951858585 * L_15 = DOTween_Shake_m3409041061(NULL /*static, unused*/, L_5, L_8, L_9, L_11, L_12, L_13, L_14, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass25_0_t289052047 * L_16 = V_0;
		NullCheck(L_16);
		RectTransform_t3704657025 * L_17 = L_16->get_target_0();
		TweenerCore_3_t951858585 * L_18 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t951858585_m2980331507(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t951858585_m2980331507_RuntimeMethod_var);
		TweenerCore_3_t951858585 * L_19 = Extensions_SetSpecialStartupMode_TisTweenerCore_3_t951858585_m1835023067(NULL /*static, unused*/, L_18, 2, /*hidden argument*/Extensions_SetSpecialStartupMode_TisTweenerCore_3_t951858585_m1835023067_RuntimeMethod_var);
		bool L_20 = ___snapping5;
		Tweener_t436044680 * L_21 = TweenSettingsExtensions_SetOptions_m288553191(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		return L_21;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern "C"  Tweener_t436044680 * ShortcutExtensions46_DOColor_m1801377730 (RuntimeObject * __this /* static, unused */, Text_t1901882714 * ___target0, Color_t2555686324  ___endValue1, float ___duration2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOColor_m1801377730_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass31_0_t1090461940 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass31_0_t1090461940 * L_0 = (U3CU3Ec__DisplayClass31_0_t1090461940 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass31_0_t1090461940_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass31_0__ctor_m631190223(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass31_0_t1090461940 * L_1 = V_0;
		Text_t1901882714 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass31_0_t1090461940 * L_3 = V_0;
		intptr_t L_4 = (intptr_t)U3CU3Ec__DisplayClass31_0_U3CDOColorU3Eb__0_m1133203489_RuntimeMethod_var;
		DOGetter_1_t2298482528 * L_5 = (DOGetter_1_t2298482528 *)il2cpp_codegen_object_new(DOGetter_1_t2298482528_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m1558284636(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m1558284636_RuntimeMethod_var);
		U3CU3Ec__DisplayClass31_0_t1090461940 * L_6 = V_0;
		intptr_t L_7 = (intptr_t)U3CU3Ec__DisplayClass31_0_U3CDOColorU3Eb__1_m2068016229_RuntimeMethod_var;
		DOSetter_1_t3605794656 * L_8 = (DOSetter_1_t3605794656 *)il2cpp_codegen_object_new(DOSetter_1_t3605794656_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m380718107(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m380718107_RuntimeMethod_var);
		Color_t2555686324  L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2744875806_il2cpp_TypeInfo_var);
		TweenerCore_3_t745532282 * L_11 = DOTween_To_m3257840982(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass31_0_t1090461940 * L_12 = V_0;
		NullCheck(L_12);
		Text_t1901882714 * L_13 = L_12->get_target_0();
		TweenerCore_3_t745532282 * L_14 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t745532282_m2904780857(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t745532282_m2904780857_RuntimeMethod_var);
		return L_14;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOFade(UnityEngine.UI.Text,System.Single,System.Single)
extern "C"  Tweener_t436044680 * ShortcutExtensions46_DOFade_m1784703499 (RuntimeObject * __this /* static, unused */, Text_t1901882714 * ___target0, float ___endValue1, float ___duration2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOFade_m1784703499_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass32_0_t3811451124 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass32_0_t3811451124 * L_0 = (U3CU3Ec__DisplayClass32_0_t3811451124 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass32_0_t3811451124_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass32_0__ctor_m1558590154(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass32_0_t3811451124 * L_1 = V_0;
		Text_t1901882714 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass32_0_t3811451124 * L_3 = V_0;
		intptr_t L_4 = (intptr_t)U3CU3Ec__DisplayClass32_0_U3CDOFadeU3Eb__0_m36854516_RuntimeMethod_var;
		DOGetter_1_t2298482528 * L_5 = (DOGetter_1_t2298482528 *)il2cpp_codegen_object_new(DOGetter_1_t2298482528_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m1558284636(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m1558284636_RuntimeMethod_var);
		U3CU3Ec__DisplayClass32_0_t3811451124 * L_6 = V_0;
		intptr_t L_7 = (intptr_t)U3CU3Ec__DisplayClass32_0_U3CDOFadeU3Eb__1_m3310679632_RuntimeMethod_var;
		DOSetter_1_t3605794656 * L_8 = (DOSetter_1_t3605794656 *)il2cpp_codegen_object_new(DOSetter_1_t3605794656_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m380718107(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m380718107_RuntimeMethod_var);
		float L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2744875806_il2cpp_TypeInfo_var);
		Tweener_t436044680 * L_11 = DOTween_ToAlpha_m1646959083(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass32_0_t3811451124 * L_12 = V_0;
		NullCheck(L_12);
		Text_t1901882714 * L_13 = L_12->get_target_0();
		Tweener_t436044680 * L_14 = TweenSettingsExtensions_SetTarget_TisTweener_t436044680_m598017345(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t436044680_m598017345_RuntimeMethod_var);
		return L_14;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOText(UnityEngine.UI.Text,System.String,System.Single,System.Boolean,DG.Tweening.ScrambleMode,System.String)
extern "C"  Tweener_t436044680 * ShortcutExtensions46_DOText_m1289017753 (RuntimeObject * __this /* static, unused */, Text_t1901882714 * ___target0, String_t* ___endValue1, float ___duration2, bool ___richTextEnabled3, int32_t ___scrambleMode4, String_t* ___scrambleChars5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOText_m1289017753_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass33_0_t1472798964 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass33_0_t1472798964 * L_0 = (U3CU3Ec__DisplayClass33_0_t1472798964 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass33_0_t1472798964_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass33_0__ctor_m4182061772(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass33_0_t1472798964 * L_1 = V_0;
		Text_t1901882714 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass33_0_t1472798964 * L_3 = V_0;
		intptr_t L_4 = (intptr_t)U3CU3Ec__DisplayClass33_0_U3CDOTextU3Eb__0_m3254169216_RuntimeMethod_var;
		DOGetter_1_t1590246893 * L_5 = (DOGetter_1_t1590246893 *)il2cpp_codegen_object_new(DOGetter_1_t1590246893_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m2531950179(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m2531950179_RuntimeMethod_var);
		U3CU3Ec__DisplayClass33_0_t1472798964 * L_6 = V_0;
		intptr_t L_7 = (intptr_t)U3CU3Ec__DisplayClass33_0_U3CDOTextU3Eb__1_m2475978_RuntimeMethod_var;
		DOSetter_1_t2897559021 * L_8 = (DOSetter_1_t2897559021 *)il2cpp_codegen_object_new(DOSetter_1_t2897559021_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m932311154(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m932311154_RuntimeMethod_var);
		String_t* L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2744875806_il2cpp_TypeInfo_var);
		TweenerCore_3_t2915539799 * L_11 = DOTween_To_m4191053537(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		bool L_12 = ___richTextEnabled3;
		int32_t L_13 = ___scrambleMode4;
		String_t* L_14 = ___scrambleChars5;
		Tweener_t436044680 * L_15 = TweenSettingsExtensions_SetOptions_m3956807361(NULL /*static, unused*/, L_11, L_12, L_13, L_14, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass33_0_t1472798964 * L_16 = V_0;
		NullCheck(L_16);
		Text_t1901882714 * L_17 = L_16->get_target_0();
		Tweener_t436044680 * L_18 = TweenSettingsExtensions_SetTarget_TisTweener_t436044680_m598017345(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t436044680_m598017345_RuntimeMethod_var);
		return L_18;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass0_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass0_0__ctor_m1227527790 (U3CU3Ec__DisplayClass0_0_t892042862 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single DG.Tweening.ShortcutExtensions46/<>c__DisplayClass0_0::<DOFade>b__0()
extern "C"  float U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m1136460969 (U3CU3Ec__DisplayClass0_0_t892042862 * __this, const RuntimeMethod* method)
{
	{
		CanvasGroup_t4083511760 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		float L_1 = CanvasGroup_get_alpha_m2918269489(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass0_0::<DOFade>b__1(System.Single)
extern "C"  void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_m737149533 (U3CU3Ec__DisplayClass0_0_t892042862 * __this, float ___x0, const RuntimeMethod* method)
{
	{
		CanvasGroup_t4083511760 * L_0 = __this->get_target_0();
		float L_1 = ___x0;
		NullCheck(L_0);
		CanvasGroup_set_alpha_m4032573(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass13_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass13_0__ctor_m4182001830 (U3CU3Ec__DisplayClass13_0_t2635598378 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass13_0::<DOAnchorPos>b__0()
extern "C"  Vector2_t2156229523  U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__0_m4202884669 (U3CU3Ec__DisplayClass13_0_t2635598378 * __this, const RuntimeMethod* method)
{
	{
		RectTransform_t3704657025 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector2_t2156229523  L_1 = RectTransform_get_anchoredPosition_m1813443094(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass13_0::<DOAnchorPos>b__1(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__1_m58758457 (U3CU3Ec__DisplayClass13_0_t2635598378 * __this, Vector2_t2156229523  ___x0, const RuntimeMethod* method)
{
	{
		RectTransform_t3704657025 * L_0 = __this->get_target_0();
		Vector2_t2156229523  L_1 = ___x0;
		NullCheck(L_0);
		RectTransform_set_anchoredPosition_m4126691837(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass16_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass16_0__ctor_m2441169062 (U3CU3Ec__DisplayClass16_0_t1443957290 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass16_0::<DOAnchorPos3D>b__0()
extern "C"  Vector3_t3722313464  U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_m1327669346 (U3CU3Ec__DisplayClass16_0_t1443957290 * __this, const RuntimeMethod* method)
{
	{
		RectTransform_t3704657025 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = RectTransform_get_anchoredPosition3D_m3487933542(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass16_0::<DOAnchorPos3D>b__1(UnityEngine.Vector3)
extern "C"  void U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_m3318326923 (U3CU3Ec__DisplayClass16_0_t1443957290 * __this, Vector3_t3722313464  ___x0, const RuntimeMethod* method)
{
	{
		RectTransform_t3704657025 * L_0 = __this->get_target_0();
		Vector3_t3722313464  L_1 = ___x0;
		NullCheck(L_0);
		RectTransform_set_anchoredPosition3D_m2642675894(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass22_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass22_0__ctor_m1558496271 (U3CU3Ec__DisplayClass22_0_t2245367183 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass22_0::<DOSizeDelta>b__0()
extern "C"  Vector2_t2156229523  U3CU3Ec__DisplayClass22_0_U3CDOSizeDeltaU3Eb__0_m194433554 (U3CU3Ec__DisplayClass22_0_t2245367183 * __this, const RuntimeMethod* method)
{
	{
		RectTransform_t3704657025 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector2_t2156229523  L_1 = RectTransform_get_sizeDelta_m2183112744(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass22_0::<DOSizeDelta>b__1(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass22_0_U3CDOSizeDeltaU3Eb__1_m1120729751 (U3CU3Ec__DisplayClass22_0_t2245367183 * __this, Vector2_t2156229523  ___x0, const RuntimeMethod* method)
{
	{
		RectTransform_t3704657025 * L_0 = __this->get_target_0();
		Vector2_t2156229523  L_1 = ___x0;
		NullCheck(L_0);
		RectTransform_set_sizeDelta_m3462269772(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass23_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass23_0__ctor_m4181967889 (U3CU3Ec__DisplayClass23_0_t4201682319 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass23_0::<DOPunchAnchorPos>b__0()
extern "C"  Vector3_t3722313464  U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__0_m2475830852 (U3CU3Ec__DisplayClass23_0_t4201682319 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__0_m2475830852_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RectTransform_t3704657025 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector2_t2156229523  L_1 = RectTransform_get_anchoredPosition_m1813443094(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass23_0::<DOPunchAnchorPos>b__1(UnityEngine.Vector3)
extern "C"  void U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__1_m2684855292 (U3CU3Ec__DisplayClass23_0_t4201682319 * __this, Vector3_t3722313464  ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__1_m2684855292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RectTransform_t3704657025 * L_0 = __this->get_target_0();
		Vector3_t3722313464  L_1 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_2 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		RectTransform_set_anchoredPosition_m4126691837(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass25_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass25_0__ctor_m302629890 (U3CU3Ec__DisplayClass25_0_t289052047 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass25_0::<DOShakeAnchorPos>b__0()
extern "C"  Vector3_t3722313464  U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__0_m290566680 (U3CU3Ec__DisplayClass25_0_t289052047 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__0_m290566680_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RectTransform_t3704657025 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector2_t2156229523  L_1 = RectTransform_get_anchoredPosition_m1813443094(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_2 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass25_0::<DOShakeAnchorPos>b__1(UnityEngine.Vector3)
extern "C"  void U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__1_m2933663977 (U3CU3Ec__DisplayClass25_0_t289052047 * __this, Vector3_t3722313464  ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__1_m2933663977_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RectTransform_t3704657025 * L_0 = __this->get_target_0();
		Vector3_t3722313464  L_1 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_2 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		RectTransform_set_anchoredPosition_m4126691837(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass3_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass3_0__ctor_m1227532699 (U3CU3Ec__DisplayClass3_0_t1295327389 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass3_0::<DOColor>b__0()
extern "C"  Color_t2555686324  U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_m2988152837 (U3CU3Ec__DisplayClass3_0_t1295327389 * __this, const RuntimeMethod* method)
{
	{
		Image_t2670269651 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Color_t2555686324  L_1 = VirtFuncInvoker0< Color_t2555686324  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_0);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass3_0::<DOColor>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_m1674238268 (U3CU3Ec__DisplayClass3_0_t1295327389 * __this, Color_t2555686324  ___x0, const RuntimeMethod* method)
{
	{
		Image_t2670269651 * L_0 = __this->get_target_0();
		Color_t2555686324  L_1 = ___x0;
		NullCheck(L_0);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass31_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass31_0__ctor_m631190223 (U3CU3Ec__DisplayClass31_0_t1090461940 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass31_0::<DOColor>b__0()
extern "C"  Color_t2555686324  U3CU3Ec__DisplayClass31_0_U3CDOColorU3Eb__0_m1133203489 (U3CU3Ec__DisplayClass31_0_t1090461940 * __this, const RuntimeMethod* method)
{
	{
		Text_t1901882714 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Color_t2555686324  L_1 = VirtFuncInvoker0< Color_t2555686324  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_0);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass31_0::<DOColor>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass31_0_U3CDOColorU3Eb__1_m2068016229 (U3CU3Ec__DisplayClass31_0_t1090461940 * __this, Color_t2555686324  ___x0, const RuntimeMethod* method)
{
	{
		Text_t1901882714 * L_0 = __this->get_target_0();
		Color_t2555686324  L_1 = ___x0;
		NullCheck(L_0);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass32_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass32_0__ctor_m1558590154 (U3CU3Ec__DisplayClass32_0_t3811451124 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass32_0::<DOFade>b__0()
extern "C"  Color_t2555686324  U3CU3Ec__DisplayClass32_0_U3CDOFadeU3Eb__0_m36854516 (U3CU3Ec__DisplayClass32_0_t3811451124 * __this, const RuntimeMethod* method)
{
	{
		Text_t1901882714 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Color_t2555686324  L_1 = VirtFuncInvoker0< Color_t2555686324  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_0);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass32_0::<DOFade>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass32_0_U3CDOFadeU3Eb__1_m3310679632 (U3CU3Ec__DisplayClass32_0_t3811451124 * __this, Color_t2555686324  ___x0, const RuntimeMethod* method)
{
	{
		Text_t1901882714 * L_0 = __this->get_target_0();
		Color_t2555686324  L_1 = ___x0;
		NullCheck(L_0);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass33_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass33_0__ctor_m4182061772 (U3CU3Ec__DisplayClass33_0_t1472798964 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String DG.Tweening.ShortcutExtensions46/<>c__DisplayClass33_0::<DOText>b__0()
extern "C"  String_t* U3CU3Ec__DisplayClass33_0_U3CDOTextU3Eb__0_m3254169216 (U3CU3Ec__DisplayClass33_0_t1472798964 * __this, const RuntimeMethod* method)
{
	{
		Text_t1901882714 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass33_0::<DOText>b__1(System.String)
extern "C"  void U3CU3Ec__DisplayClass33_0_U3CDOTextU3Eb__1_m2475978 (U3CU3Ec__DisplayClass33_0_t1472798964 * __this, String_t* ___x0, const RuntimeMethod* method)
{
	{
		Text_t1901882714 * L_0 = __this->get_target_0();
		String_t* L_1 = ___x0;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass4_0__ctor_m1227523442 (U3CU3Ec__DisplayClass4_0_t2861411330 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::<DOFade>b__0()
extern "C"  Color_t2555686324  U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m4078431796 (U3CU3Ec__DisplayClass4_0_t2861411330 * __this, const RuntimeMethod* method)
{
	{
		Image_t2670269651 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Color_t2555686324  L_1 = VirtFuncInvoker0< Color_t2555686324  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_0);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::<DOFade>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m1839193484 (U3CU3Ec__DisplayClass4_0_t2861411330 * __this, Color_t2555686324  ___x0, const RuntimeMethod* method)
{
	{
		Image_t2670269651 * L_0 = __this->get_target_0();
		Color_t2555686324  L_1 = ___x0;
		NullCheck(L_0);
		VirtActionInvoker1< Color_t2555686324  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
