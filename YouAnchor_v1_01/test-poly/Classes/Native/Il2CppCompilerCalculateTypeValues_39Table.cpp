﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Wrld.MapInput.IUnityInputHandler
struct IUnityInputHandler_t448527725;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,Wrld.Meshes.PreparedMeshRecord>
struct Dictionary_2_t231665766;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// Wrld.Common.Maths.DoubleVector3[]
struct DoubleVector3U5BU5D_t1291174656;
// System.Collections.Generic.List`1<System.Runtime.InteropServices.GCHandle>
struct List_1_t528545633;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32>
struct Func_2_t3962680016;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1839659084;
// Wrld.Meshes.MeshBuilder/<CreatePreparedMeshes>c__AnonStorey0
struct U3CCreatePreparedMeshesU3Ec__AnonStorey0_t942887021;
// Wrld.Meshes.PreparedMeshRepository
struct PreparedMeshRepository_t1998320162;
// System.Collections.Generic.Dictionary`2<System.Int32,Wrld.Resources.Buildings.BuildingsApi/BuildingRequest>
struct Dictionary_2_t487369293;
// System.Collections.Generic.Dictionary`2<System.Int32,Wrld.Resources.Buildings.BuildingsApi/HighlightRequest>
struct Dictionary_2_t305154664;
// Wrld.Streaming.GameObjectStreamer
struct GameObjectStreamer_t3452608707;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// Wrld.Resources.Buildings.BuildingsApi/NativeHighlightMeshUploadCallback
struct NativeHighlightMeshUploadCallback_t608941254;
// Wrld.Resources.Buildings.BuildingsApi/NativeHighlightMeshClearCallback
struct NativeHighlightMeshClearCallback_t813633652;
// Wrld.Resources.Buildings.BuildingsApi/NativeBuildingReceivedCallback
struct NativeBuildingReceivedCallback_t2577383538;
// Wrld.Resources.Buildings.BuildingsApi/NativeHighlightReceivedCallback
struct NativeHighlightReceivedCallback_t2749649652;
// Wrld.Space.UnityWorldSpaceCoordinateFrame
struct UnityWorldSpaceCoordinateFrame_t1382732960;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Collections.Generic.Dictionary`2<System.String,Wrld.Streaming.GameObjectRecord>
struct Dictionary_2_t2657171120;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// Wrld.Materials.MaterialRepository
struct MaterialRepository_t701520604;
// System.Collections.Generic.List`1<Wrld.Space.GeographicTransform>
struct List_1_t3734218024;
// Wrld.Meshes.MeshUploader
struct MeshUploader_t3724330286;
// Wrld.Resources.Terrain.Heights.TerrainHeightProvider
struct TerrainHeightProvider_t517848921;
// Wrld.MapInput.Touch.PanGesture
struct PanGesture_t3882187103;
// Wrld.MapInput.Touch.PinchGesture
struct PinchGesture_t72617885;
// Wrld.MapInput.Touch.RotateGesture
struct RotateGesture_t3703755966;
// Wrld.MapInput.Touch.TouchGesture
struct TouchGesture_t2500129316;
// Wrld.MapInput.Touch.TapGesture
struct TapGesture_t191755233;
// System.Collections.Generic.Dictionary`2<System.UInt32,UnityEngine.Texture>
struct Dictionary_2_t641263069;
// Wrld.Concurrency.ConcurrentQueue`1<Wrld.Materials.TextureLoadHandler/TextureBuffer>
struct ConcurrentQueue_1_t11616060;
// Wrld.Materials.IdGenerator
struct IdGenerator_t1318283159;
// System.Collections.Generic.HashSet`1<UnityEngine.Material>
struct HashSet_1_t3200291893;
// System.Collections.Generic.HashSet`1<System.UInt32>
struct HashSet_1_t1125011452;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// Wrld.MapInput.Mouse.MousePanGesture
struct MousePanGesture_t639745302;
// Wrld.MapInput.Mouse.MouseZoomGesture
struct MouseZoomGesture_t962341082;
// Wrld.MapInput.Mouse.MouseRotateGesture
struct MouseRotateGesture_t1726080705;
// Wrld.MapInput.Mouse.MouseTiltGesture
struct MouseTiltGesture_t4197683903;
// Wrld.MapInput.Mouse.MouseTouchGesture
struct MouseTouchGesture_t2284897039;
// Wrld.MapInput.Mouse.MouseTapGesture
struct MouseTapGesture_t1901348987;
// System.Collections.Generic.Dictionary`2<System.String,Wrld.Materials.MaterialRepository/MaterialRecord>
struct Dictionary_2_t2238724610;
// UnityEngine.Material
struct Material_t340375123;
// Wrld.Materials.TextureLoadHandler
struct TextureLoadHandler_t2080734353;
// Wrld.Resources.Buildings.BuildingsApi/HighlightReceivedCallback
struct HighlightReceivedCallback_t2001851641;
// Wrld.Resources.Buildings.BuildingsApi/BuildingReceivedCallback
struct BuildingReceivedCallback_t1209487313;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.List`1<Wrld.MapInput.Touch.TouchInputPointerEvent>
struct List_1_t2102936381;
// System.Void
struct Void_t1185182177;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// Wrld.MapGameObjectScene
struct MapGameObjectScene_t128928738;
// Wrld.StreamingUpdater
struct StreamingUpdater_t530401876;
// Wrld.Meshes.PreparedMesh[]
struct PreparedMeshU5BU5D_t3261552485;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// Wrld.Streaming.GameObjectRepository
struct GameObjectRepository_t3760691595;
// Wrld.Streaming.GameObjectFactory
struct GameObjectFactory_t2478326784;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// Wrld.Resources.Buildings.Highlight
struct Highlight_t3200169708;
// Wrld.Common.Maths.EcefTangentBasis
struct EcefTangentBasis_t256737685;
// UnityEngine.MeshFilter
struct MeshFilter_t3523625662;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef MOUSETOUCHGESTURE_T2284897039_H
#define MOUSETOUCHGESTURE_T2284897039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.Mouse.MouseTouchGesture
struct  MouseTouchGesture_t2284897039  : public RuntimeObject
{
public:
	// Wrld.MapInput.IUnityInputHandler Wrld.MapInput.Mouse.MouseTouchGesture::m_handler
	RuntimeObject* ___m_handler_0;

public:
	inline static int32_t get_offset_of_m_handler_0() { return static_cast<int32_t>(offsetof(MouseTouchGesture_t2284897039, ___m_handler_0)); }
	inline RuntimeObject* get_m_handler_0() const { return ___m_handler_0; }
	inline RuntimeObject** get_address_of_m_handler_0() { return &___m_handler_0; }
	inline void set_m_handler_0(RuntimeObject* value)
	{
		___m_handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_handler_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSETOUCHGESTURE_T2284897039_H
#ifndef HIGHLIGHT_T3200169708_H
#define HIGHLIGHT_T3200169708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Resources.Buildings.Highlight
struct  Highlight_t3200169708  : public RuntimeObject
{
public:
	// System.String Wrld.Resources.Buildings.Highlight::HighlightId
	String_t* ___HighlightId_0;
	// System.Int32 Wrld.Resources.Buildings.Highlight::HighlightRequestId
	int32_t ___HighlightRequestId_1;

public:
	inline static int32_t get_offset_of_HighlightId_0() { return static_cast<int32_t>(offsetof(Highlight_t3200169708, ___HighlightId_0)); }
	inline String_t* get_HighlightId_0() const { return ___HighlightId_0; }
	inline String_t** get_address_of_HighlightId_0() { return &___HighlightId_0; }
	inline void set_HighlightId_0(String_t* value)
	{
		___HighlightId_0 = value;
		Il2CppCodeGenWriteBarrier((&___HighlightId_0), value);
	}

	inline static int32_t get_offset_of_HighlightRequestId_1() { return static_cast<int32_t>(offsetof(Highlight_t3200169708, ___HighlightRequestId_1)); }
	inline int32_t get_HighlightRequestId_1() const { return ___HighlightRequestId_1; }
	inline int32_t* get_address_of_HighlightRequestId_1() { return &___HighlightRequestId_1; }
	inline void set_HighlightRequestId_1(int32_t value)
	{
		___HighlightRequestId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIGHLIGHT_T3200169708_H
#ifndef PREPAREDMESHREPOSITORY_T1998320162_H
#define PREPAREDMESHREPOSITORY_T1998320162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Meshes.PreparedMeshRepository
struct  PreparedMeshRepository_t1998320162  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Wrld.Meshes.PreparedMeshRecord> Wrld.Meshes.PreparedMeshRepository::m_meshRecords
	Dictionary_2_t231665766 * ___m_meshRecords_0;

public:
	inline static int32_t get_offset_of_m_meshRecords_0() { return static_cast<int32_t>(offsetof(PreparedMeshRepository_t1998320162, ___m_meshRecords_0)); }
	inline Dictionary_2_t231665766 * get_m_meshRecords_0() const { return ___m_meshRecords_0; }
	inline Dictionary_2_t231665766 ** get_address_of_m_meshRecords_0() { return &___m_meshRecords_0; }
	inline void set_m_meshRecords_0(Dictionary_2_t231665766 * value)
	{
		___m_meshRecords_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_meshRecords_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREPAREDMESHREPOSITORY_T1998320162_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef PREPAREDMESH_T3331185004_H
#define PREPAREDMESH_T3331185004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Meshes.PreparedMesh
struct  PreparedMesh_t3331185004  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] Wrld.Meshes.PreparedMesh::m_verts
	Vector3U5BU5D_t1718750761* ___m_verts_0;
	// UnityEngine.Vector2[] Wrld.Meshes.PreparedMesh::m_uvs
	Vector2U5BU5D_t1457185986* ___m_uvs_1;
	// UnityEngine.Vector2[] Wrld.Meshes.PreparedMesh::m_uv2s
	Vector2U5BU5D_t1457185986* ___m_uv2s_2;
	// System.Int32[] Wrld.Meshes.PreparedMesh::m_indices
	Int32U5BU5D_t385246372* ___m_indices_3;
	// System.String Wrld.Meshes.PreparedMesh::m_name
	String_t* ___m_name_4;
	// UnityEngine.Vector3[] Wrld.Meshes.PreparedMesh::m_normals
	Vector3U5BU5D_t1718750761* ___m_normals_5;

public:
	inline static int32_t get_offset_of_m_verts_0() { return static_cast<int32_t>(offsetof(PreparedMesh_t3331185004, ___m_verts_0)); }
	inline Vector3U5BU5D_t1718750761* get_m_verts_0() const { return ___m_verts_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_verts_0() { return &___m_verts_0; }
	inline void set_m_verts_0(Vector3U5BU5D_t1718750761* value)
	{
		___m_verts_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_verts_0), value);
	}

	inline static int32_t get_offset_of_m_uvs_1() { return static_cast<int32_t>(offsetof(PreparedMesh_t3331185004, ___m_uvs_1)); }
	inline Vector2U5BU5D_t1457185986* get_m_uvs_1() const { return ___m_uvs_1; }
	inline Vector2U5BU5D_t1457185986** get_address_of_m_uvs_1() { return &___m_uvs_1; }
	inline void set_m_uvs_1(Vector2U5BU5D_t1457185986* value)
	{
		___m_uvs_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_uvs_1), value);
	}

	inline static int32_t get_offset_of_m_uv2s_2() { return static_cast<int32_t>(offsetof(PreparedMesh_t3331185004, ___m_uv2s_2)); }
	inline Vector2U5BU5D_t1457185986* get_m_uv2s_2() const { return ___m_uv2s_2; }
	inline Vector2U5BU5D_t1457185986** get_address_of_m_uv2s_2() { return &___m_uv2s_2; }
	inline void set_m_uv2s_2(Vector2U5BU5D_t1457185986* value)
	{
		___m_uv2s_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_uv2s_2), value);
	}

	inline static int32_t get_offset_of_m_indices_3() { return static_cast<int32_t>(offsetof(PreparedMesh_t3331185004, ___m_indices_3)); }
	inline Int32U5BU5D_t385246372* get_m_indices_3() const { return ___m_indices_3; }
	inline Int32U5BU5D_t385246372** get_address_of_m_indices_3() { return &___m_indices_3; }
	inline void set_m_indices_3(Int32U5BU5D_t385246372* value)
	{
		___m_indices_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_indices_3), value);
	}

	inline static int32_t get_offset_of_m_name_4() { return static_cast<int32_t>(offsetof(PreparedMesh_t3331185004, ___m_name_4)); }
	inline String_t* get_m_name_4() const { return ___m_name_4; }
	inline String_t** get_address_of_m_name_4() { return &___m_name_4; }
	inline void set_m_name_4(String_t* value)
	{
		___m_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_4), value);
	}

	inline static int32_t get_offset_of_m_normals_5() { return static_cast<int32_t>(offsetof(PreparedMesh_t3331185004, ___m_normals_5)); }
	inline Vector3U5BU5D_t1718750761* get_m_normals_5() const { return ___m_normals_5; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_normals_5() { return &___m_normals_5; }
	inline void set_m_normals_5(Vector3U5BU5D_t1718750761* value)
	{
		___m_normals_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_normals_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREPAREDMESH_T3331185004_H
#ifndef UNPACKEDMESH_T2089855528_H
#define UNPACKEDMESH_T2089855528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Meshes.MeshUploader/UnpackedMesh
struct  UnpackedMesh_t2089855528  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] Wrld.Meshes.MeshUploader/UnpackedMesh::vertices
	Vector3U5BU5D_t1718750761* ___vertices_0;
	// UnityEngine.Vector2[] Wrld.Meshes.MeshUploader/UnpackedMesh::uvs
	Vector2U5BU5D_t1457185986* ___uvs_1;
	// UnityEngine.Vector2[] Wrld.Meshes.MeshUploader/UnpackedMesh::uv2s
	Vector2U5BU5D_t1457185986* ___uv2s_2;
	// System.Int32[] Wrld.Meshes.MeshUploader/UnpackedMesh::indices
	Int32U5BU5D_t385246372* ___indices_3;
	// Wrld.Common.Maths.DoubleVector3[] Wrld.Meshes.MeshUploader/UnpackedMesh::originECEF
	DoubleVector3U5BU5D_t1291174656* ___originECEF_4;
	// System.String Wrld.Meshes.MeshUploader/UnpackedMesh::materialName
	String_t* ___materialName_5;
	// System.String Wrld.Meshes.MeshUploader/UnpackedMesh::name
	String_t* ___name_6;
	// System.Collections.Generic.List`1<System.Runtime.InteropServices.GCHandle> Wrld.Meshes.MeshUploader/UnpackedMesh::gcHandles
	List_1_t528545633 * ___gcHandles_7;

public:
	inline static int32_t get_offset_of_vertices_0() { return static_cast<int32_t>(offsetof(UnpackedMesh_t2089855528, ___vertices_0)); }
	inline Vector3U5BU5D_t1718750761* get_vertices_0() const { return ___vertices_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_vertices_0() { return &___vertices_0; }
	inline void set_vertices_0(Vector3U5BU5D_t1718750761* value)
	{
		___vertices_0 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_0), value);
	}

	inline static int32_t get_offset_of_uvs_1() { return static_cast<int32_t>(offsetof(UnpackedMesh_t2089855528, ___uvs_1)); }
	inline Vector2U5BU5D_t1457185986* get_uvs_1() const { return ___uvs_1; }
	inline Vector2U5BU5D_t1457185986** get_address_of_uvs_1() { return &___uvs_1; }
	inline void set_uvs_1(Vector2U5BU5D_t1457185986* value)
	{
		___uvs_1 = value;
		Il2CppCodeGenWriteBarrier((&___uvs_1), value);
	}

	inline static int32_t get_offset_of_uv2s_2() { return static_cast<int32_t>(offsetof(UnpackedMesh_t2089855528, ___uv2s_2)); }
	inline Vector2U5BU5D_t1457185986* get_uv2s_2() const { return ___uv2s_2; }
	inline Vector2U5BU5D_t1457185986** get_address_of_uv2s_2() { return &___uv2s_2; }
	inline void set_uv2s_2(Vector2U5BU5D_t1457185986* value)
	{
		___uv2s_2 = value;
		Il2CppCodeGenWriteBarrier((&___uv2s_2), value);
	}

	inline static int32_t get_offset_of_indices_3() { return static_cast<int32_t>(offsetof(UnpackedMesh_t2089855528, ___indices_3)); }
	inline Int32U5BU5D_t385246372* get_indices_3() const { return ___indices_3; }
	inline Int32U5BU5D_t385246372** get_address_of_indices_3() { return &___indices_3; }
	inline void set_indices_3(Int32U5BU5D_t385246372* value)
	{
		___indices_3 = value;
		Il2CppCodeGenWriteBarrier((&___indices_3), value);
	}

	inline static int32_t get_offset_of_originECEF_4() { return static_cast<int32_t>(offsetof(UnpackedMesh_t2089855528, ___originECEF_4)); }
	inline DoubleVector3U5BU5D_t1291174656* get_originECEF_4() const { return ___originECEF_4; }
	inline DoubleVector3U5BU5D_t1291174656** get_address_of_originECEF_4() { return &___originECEF_4; }
	inline void set_originECEF_4(DoubleVector3U5BU5D_t1291174656* value)
	{
		___originECEF_4 = value;
		Il2CppCodeGenWriteBarrier((&___originECEF_4), value);
	}

	inline static int32_t get_offset_of_materialName_5() { return static_cast<int32_t>(offsetof(UnpackedMesh_t2089855528, ___materialName_5)); }
	inline String_t* get_materialName_5() const { return ___materialName_5; }
	inline String_t** get_address_of_materialName_5() { return &___materialName_5; }
	inline void set_materialName_5(String_t* value)
	{
		___materialName_5 = value;
		Il2CppCodeGenWriteBarrier((&___materialName_5), value);
	}

	inline static int32_t get_offset_of_name_6() { return static_cast<int32_t>(offsetof(UnpackedMesh_t2089855528, ___name_6)); }
	inline String_t* get_name_6() const { return ___name_6; }
	inline String_t** get_address_of_name_6() { return &___name_6; }
	inline void set_name_6(String_t* value)
	{
		___name_6 = value;
		Il2CppCodeGenWriteBarrier((&___name_6), value);
	}

	inline static int32_t get_offset_of_gcHandles_7() { return static_cast<int32_t>(offsetof(UnpackedMesh_t2089855528, ___gcHandles_7)); }
	inline List_1_t528545633 * get_gcHandles_7() const { return ___gcHandles_7; }
	inline List_1_t528545633 ** get_address_of_gcHandles_7() { return &___gcHandles_7; }
	inline void set_gcHandles_7(List_1_t528545633 * value)
	{
		___gcHandles_7 = value;
		Il2CppCodeGenWriteBarrier((&___gcHandles_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNPACKEDMESH_T2089855528_H
#ifndef MATRIX4X4EXTENSIONS_T1177881985_H
#define MATRIX4X4EXTENSIONS_T1177881985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Common.Maths.Matrix4x4Extensions
struct  Matrix4x4Extensions_t1177881985  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4EXTENSIONS_T1177881985_H
#ifndef QUATERNIONEXTENSIONS_T2180248493_H
#define QUATERNIONEXTENSIONS_T2180248493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Common.Maths.QuaternionExtensions
struct  QuaternionExtensions_t2180248493  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONEXTENSIONS_T2180248493_H
#ifndef MESHBUILDER_T1786676944_H
#define MESHBUILDER_T1786676944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Meshes.MeshBuilder
struct  MeshBuilder_t1786676944  : public RuntimeObject
{
public:

public:
};

struct MeshBuilder_t1786676944_StaticFields
{
public:
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32> Wrld.Meshes.MeshBuilder::<>f__am$cache0
	Func_2_t3962680016 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>,System.Int32> Wrld.Meshes.MeshBuilder::<>f__am$cache1
	Func_2_t3962680016 * ___U3CU3Ef__amU24cache1_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(MeshBuilder_t1786676944_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t3962680016 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t3962680016 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t3962680016 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(MeshBuilder_t1786676944_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t3962680016 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t3962680016 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t3962680016 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHBUILDER_T1786676944_H
#ifndef U3CCREATEPREPAREDMESHESU3EC__ANONSTOREY0_T942887021_H
#define U3CCREATEPREPAREDMESHESU3EC__ANONSTOREY0_T942887021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Meshes.MeshBuilder/<CreatePreparedMeshes>c__AnonStorey0
struct  U3CCreatePreparedMeshesU3Ec__AnonStorey0_t942887021  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] Wrld.Meshes.MeshBuilder/<CreatePreparedMeshes>c__AnonStorey0::verts
	Vector3U5BU5D_t1718750761* ___verts_0;
	// UnityEngine.Vector2[] Wrld.Meshes.MeshBuilder/<CreatePreparedMeshes>c__AnonStorey0::uvs
	Vector2U5BU5D_t1457185986* ___uvs_1;
	// UnityEngine.Vector2[] Wrld.Meshes.MeshBuilder/<CreatePreparedMeshes>c__AnonStorey0::uv2s
	Vector2U5BU5D_t1457185986* ___uv2s_2;

public:
	inline static int32_t get_offset_of_verts_0() { return static_cast<int32_t>(offsetof(U3CCreatePreparedMeshesU3Ec__AnonStorey0_t942887021, ___verts_0)); }
	inline Vector3U5BU5D_t1718750761* get_verts_0() const { return ___verts_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_verts_0() { return &___verts_0; }
	inline void set_verts_0(Vector3U5BU5D_t1718750761* value)
	{
		___verts_0 = value;
		Il2CppCodeGenWriteBarrier((&___verts_0), value);
	}

	inline static int32_t get_offset_of_uvs_1() { return static_cast<int32_t>(offsetof(U3CCreatePreparedMeshesU3Ec__AnonStorey0_t942887021, ___uvs_1)); }
	inline Vector2U5BU5D_t1457185986* get_uvs_1() const { return ___uvs_1; }
	inline Vector2U5BU5D_t1457185986** get_address_of_uvs_1() { return &___uvs_1; }
	inline void set_uvs_1(Vector2U5BU5D_t1457185986* value)
	{
		___uvs_1 = value;
		Il2CppCodeGenWriteBarrier((&___uvs_1), value);
	}

	inline static int32_t get_offset_of_uv2s_2() { return static_cast<int32_t>(offsetof(U3CCreatePreparedMeshesU3Ec__AnonStorey0_t942887021, ___uv2s_2)); }
	inline Vector2U5BU5D_t1457185986* get_uv2s_2() const { return ___uv2s_2; }
	inline Vector2U5BU5D_t1457185986** get_address_of_uv2s_2() { return &___uv2s_2; }
	inline void set_uv2s_2(Vector2U5BU5D_t1457185986* value)
	{
		___uv2s_2 = value;
		Il2CppCodeGenWriteBarrier((&___uv2s_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATEPREPAREDMESHESU3EC__ANONSTOREY0_T942887021_H
#ifndef U3CCREATEPREPAREDMESHESU3EC__ANONSTOREY1_T3281539181_H
#define U3CCREATEPREPAREDMESHESU3EC__ANONSTOREY1_T3281539181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Meshes.MeshBuilder/<CreatePreparedMeshes>c__AnonStorey1
struct  U3CCreatePreparedMeshesU3Ec__AnonStorey1_t3281539181  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> Wrld.Meshes.MeshBuilder/<CreatePreparedMeshes>c__AnonStorey1::reversedRemapping
	Dictionary_2_t1839659084 * ___reversedRemapping_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> Wrld.Meshes.MeshBuilder/<CreatePreparedMeshes>c__AnonStorey1::indexRemapper
	Dictionary_2_t1839659084 * ___indexRemapper_1;
	// Wrld.Meshes.MeshBuilder/<CreatePreparedMeshes>c__AnonStorey0 Wrld.Meshes.MeshBuilder/<CreatePreparedMeshes>c__AnonStorey1::<>f__ref$0
	U3CCreatePreparedMeshesU3Ec__AnonStorey0_t942887021 * ___U3CU3Ef__refU240_2;

public:
	inline static int32_t get_offset_of_reversedRemapping_0() { return static_cast<int32_t>(offsetof(U3CCreatePreparedMeshesU3Ec__AnonStorey1_t3281539181, ___reversedRemapping_0)); }
	inline Dictionary_2_t1839659084 * get_reversedRemapping_0() const { return ___reversedRemapping_0; }
	inline Dictionary_2_t1839659084 ** get_address_of_reversedRemapping_0() { return &___reversedRemapping_0; }
	inline void set_reversedRemapping_0(Dictionary_2_t1839659084 * value)
	{
		___reversedRemapping_0 = value;
		Il2CppCodeGenWriteBarrier((&___reversedRemapping_0), value);
	}

	inline static int32_t get_offset_of_indexRemapper_1() { return static_cast<int32_t>(offsetof(U3CCreatePreparedMeshesU3Ec__AnonStorey1_t3281539181, ___indexRemapper_1)); }
	inline Dictionary_2_t1839659084 * get_indexRemapper_1() const { return ___indexRemapper_1; }
	inline Dictionary_2_t1839659084 ** get_address_of_indexRemapper_1() { return &___indexRemapper_1; }
	inline void set_indexRemapper_1(Dictionary_2_t1839659084 * value)
	{
		___indexRemapper_1 = value;
		Il2CppCodeGenWriteBarrier((&___indexRemapper_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_2() { return static_cast<int32_t>(offsetof(U3CCreatePreparedMeshesU3Ec__AnonStorey1_t3281539181, ___U3CU3Ef__refU240_2)); }
	inline U3CCreatePreparedMeshesU3Ec__AnonStorey0_t942887021 * get_U3CU3Ef__refU240_2() const { return ___U3CU3Ef__refU240_2; }
	inline U3CCreatePreparedMeshesU3Ec__AnonStorey0_t942887021 ** get_address_of_U3CU3Ef__refU240_2() { return &___U3CU3Ef__refU240_2; }
	inline void set_U3CU3Ef__refU240_2(U3CCreatePreparedMeshesU3Ec__AnonStorey0_t942887021 * value)
	{
		___U3CU3Ef__refU240_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATEPREPAREDMESHESU3EC__ANONSTOREY1_T3281539181_H
#ifndef MESHUPLOADER_T3724330286_H
#define MESHUPLOADER_T3724330286_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Meshes.MeshUploader
struct  MeshUploader_t3724330286  : public RuntimeObject
{
public:

public:
};

struct MeshUploader_t3724330286_StaticFields
{
public:
	// Wrld.Meshes.PreparedMeshRepository Wrld.Meshes.MeshUploader::m_preparedMeshes
	PreparedMeshRepository_t1998320162 * ___m_preparedMeshes_0;

public:
	inline static int32_t get_offset_of_m_preparedMeshes_0() { return static_cast<int32_t>(offsetof(MeshUploader_t3724330286_StaticFields, ___m_preparedMeshes_0)); }
	inline PreparedMeshRepository_t1998320162 * get_m_preparedMeshes_0() const { return ___m_preparedMeshes_0; }
	inline PreparedMeshRepository_t1998320162 ** get_address_of_m_preparedMeshes_0() { return &___m_preparedMeshes_0; }
	inline void set_m_preparedMeshes_0(PreparedMeshRepository_t1998320162 * value)
	{
		___m_preparedMeshes_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_preparedMeshes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHUPLOADER_T3724330286_H
#ifndef BUILDINGSAPI_T3854622187_H
#define BUILDINGSAPI_T3854622187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Resources.Buildings.BuildingsApi
struct  BuildingsApi_t3854622187  : public RuntimeObject
{
public:
	// System.Int32 Wrld.Resources.Buildings.BuildingsApi::m_nextBuildingRequestId
	int32_t ___m_nextBuildingRequestId_4;
	// System.Int32 Wrld.Resources.Buildings.BuildingsApi::m_nextHighlightRequestId
	int32_t ___m_nextHighlightRequestId_5;

public:
	inline static int32_t get_offset_of_m_nextBuildingRequestId_4() { return static_cast<int32_t>(offsetof(BuildingsApi_t3854622187, ___m_nextBuildingRequestId_4)); }
	inline int32_t get_m_nextBuildingRequestId_4() const { return ___m_nextBuildingRequestId_4; }
	inline int32_t* get_address_of_m_nextBuildingRequestId_4() { return &___m_nextBuildingRequestId_4; }
	inline void set_m_nextBuildingRequestId_4(int32_t value)
	{
		___m_nextBuildingRequestId_4 = value;
	}

	inline static int32_t get_offset_of_m_nextHighlightRequestId_5() { return static_cast<int32_t>(offsetof(BuildingsApi_t3854622187, ___m_nextHighlightRequestId_5)); }
	inline int32_t get_m_nextHighlightRequestId_5() const { return ___m_nextHighlightRequestId_5; }
	inline int32_t* get_address_of_m_nextHighlightRequestId_5() { return &___m_nextHighlightRequestId_5; }
	inline void set_m_nextHighlightRequestId_5(int32_t value)
	{
		___m_nextHighlightRequestId_5 = value;
	}
};

struct BuildingsApi_t3854622187_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,Wrld.Resources.Buildings.BuildingsApi/BuildingRequest> Wrld.Resources.Buildings.BuildingsApi::BuildingRequests
	Dictionary_2_t487369293 * ___BuildingRequests_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,Wrld.Resources.Buildings.BuildingsApi/HighlightRequest> Wrld.Resources.Buildings.BuildingsApi::HighlightRequests
	Dictionary_2_t305154664 * ___HighlightRequests_1;
	// Wrld.Streaming.GameObjectStreamer Wrld.Resources.Buildings.BuildingsApi::HighlightStreamer
	GameObjectStreamer_t3452608707 * ___HighlightStreamer_2;
	// System.Collections.Generic.List`1<System.String> Wrld.Resources.Buildings.BuildingsApi::HighlightIdsToAdd
	List_1_t3319525431 * ___HighlightIdsToAdd_3;
	// Wrld.Resources.Buildings.BuildingsApi/NativeHighlightMeshUploadCallback Wrld.Resources.Buildings.BuildingsApi::<>f__mg$cache0
	NativeHighlightMeshUploadCallback_t608941254 * ___U3CU3Ef__mgU24cache0_6;
	// Wrld.Resources.Buildings.BuildingsApi/NativeHighlightMeshClearCallback Wrld.Resources.Buildings.BuildingsApi::<>f__mg$cache1
	NativeHighlightMeshClearCallback_t813633652 * ___U3CU3Ef__mgU24cache1_7;
	// Wrld.Resources.Buildings.BuildingsApi/NativeBuildingReceivedCallback Wrld.Resources.Buildings.BuildingsApi::<>f__mg$cache2
	NativeBuildingReceivedCallback_t2577383538 * ___U3CU3Ef__mgU24cache2_8;
	// Wrld.Resources.Buildings.BuildingsApi/NativeHighlightReceivedCallback Wrld.Resources.Buildings.BuildingsApi::<>f__mg$cache3
	NativeHighlightReceivedCallback_t2749649652 * ___U3CU3Ef__mgU24cache3_9;

public:
	inline static int32_t get_offset_of_BuildingRequests_0() { return static_cast<int32_t>(offsetof(BuildingsApi_t3854622187_StaticFields, ___BuildingRequests_0)); }
	inline Dictionary_2_t487369293 * get_BuildingRequests_0() const { return ___BuildingRequests_0; }
	inline Dictionary_2_t487369293 ** get_address_of_BuildingRequests_0() { return &___BuildingRequests_0; }
	inline void set_BuildingRequests_0(Dictionary_2_t487369293 * value)
	{
		___BuildingRequests_0 = value;
		Il2CppCodeGenWriteBarrier((&___BuildingRequests_0), value);
	}

	inline static int32_t get_offset_of_HighlightRequests_1() { return static_cast<int32_t>(offsetof(BuildingsApi_t3854622187_StaticFields, ___HighlightRequests_1)); }
	inline Dictionary_2_t305154664 * get_HighlightRequests_1() const { return ___HighlightRequests_1; }
	inline Dictionary_2_t305154664 ** get_address_of_HighlightRequests_1() { return &___HighlightRequests_1; }
	inline void set_HighlightRequests_1(Dictionary_2_t305154664 * value)
	{
		___HighlightRequests_1 = value;
		Il2CppCodeGenWriteBarrier((&___HighlightRequests_1), value);
	}

	inline static int32_t get_offset_of_HighlightStreamer_2() { return static_cast<int32_t>(offsetof(BuildingsApi_t3854622187_StaticFields, ___HighlightStreamer_2)); }
	inline GameObjectStreamer_t3452608707 * get_HighlightStreamer_2() const { return ___HighlightStreamer_2; }
	inline GameObjectStreamer_t3452608707 ** get_address_of_HighlightStreamer_2() { return &___HighlightStreamer_2; }
	inline void set_HighlightStreamer_2(GameObjectStreamer_t3452608707 * value)
	{
		___HighlightStreamer_2 = value;
		Il2CppCodeGenWriteBarrier((&___HighlightStreamer_2), value);
	}

	inline static int32_t get_offset_of_HighlightIdsToAdd_3() { return static_cast<int32_t>(offsetof(BuildingsApi_t3854622187_StaticFields, ___HighlightIdsToAdd_3)); }
	inline List_1_t3319525431 * get_HighlightIdsToAdd_3() const { return ___HighlightIdsToAdd_3; }
	inline List_1_t3319525431 ** get_address_of_HighlightIdsToAdd_3() { return &___HighlightIdsToAdd_3; }
	inline void set_HighlightIdsToAdd_3(List_1_t3319525431 * value)
	{
		___HighlightIdsToAdd_3 = value;
		Il2CppCodeGenWriteBarrier((&___HighlightIdsToAdd_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_6() { return static_cast<int32_t>(offsetof(BuildingsApi_t3854622187_StaticFields, ___U3CU3Ef__mgU24cache0_6)); }
	inline NativeHighlightMeshUploadCallback_t608941254 * get_U3CU3Ef__mgU24cache0_6() const { return ___U3CU3Ef__mgU24cache0_6; }
	inline NativeHighlightMeshUploadCallback_t608941254 ** get_address_of_U3CU3Ef__mgU24cache0_6() { return &___U3CU3Ef__mgU24cache0_6; }
	inline void set_U3CU3Ef__mgU24cache0_6(NativeHighlightMeshUploadCallback_t608941254 * value)
	{
		___U3CU3Ef__mgU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_7() { return static_cast<int32_t>(offsetof(BuildingsApi_t3854622187_StaticFields, ___U3CU3Ef__mgU24cache1_7)); }
	inline NativeHighlightMeshClearCallback_t813633652 * get_U3CU3Ef__mgU24cache1_7() const { return ___U3CU3Ef__mgU24cache1_7; }
	inline NativeHighlightMeshClearCallback_t813633652 ** get_address_of_U3CU3Ef__mgU24cache1_7() { return &___U3CU3Ef__mgU24cache1_7; }
	inline void set_U3CU3Ef__mgU24cache1_7(NativeHighlightMeshClearCallback_t813633652 * value)
	{
		___U3CU3Ef__mgU24cache1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_8() { return static_cast<int32_t>(offsetof(BuildingsApi_t3854622187_StaticFields, ___U3CU3Ef__mgU24cache2_8)); }
	inline NativeBuildingReceivedCallback_t2577383538 * get_U3CU3Ef__mgU24cache2_8() const { return ___U3CU3Ef__mgU24cache2_8; }
	inline NativeBuildingReceivedCallback_t2577383538 ** get_address_of_U3CU3Ef__mgU24cache2_8() { return &___U3CU3Ef__mgU24cache2_8; }
	inline void set_U3CU3Ef__mgU24cache2_8(NativeBuildingReceivedCallback_t2577383538 * value)
	{
		___U3CU3Ef__mgU24cache2_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_9() { return static_cast<int32_t>(offsetof(BuildingsApi_t3854622187_StaticFields, ___U3CU3Ef__mgU24cache3_9)); }
	inline NativeHighlightReceivedCallback_t2749649652 * get_U3CU3Ef__mgU24cache3_9() const { return ___U3CU3Ef__mgU24cache3_9; }
	inline NativeHighlightReceivedCallback_t2749649652 ** get_address_of_U3CU3Ef__mgU24cache3_9() { return &___U3CU3Ef__mgU24cache3_9; }
	inline void set_U3CU3Ef__mgU24cache3_9(NativeHighlightReceivedCallback_t2749649652 * value)
	{
		___U3CU3Ef__mgU24cache3_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDINGSAPI_T3854622187_H
#ifndef UNITYWORLDSPACETRANSFORMUPDATESTRATEGY_T501792395_H
#define UNITYWORLDSPACETRANSFORMUPDATESTRATEGY_T501792395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Space.UnityWorldSpaceTransformUpdateStrategy
struct  UnityWorldSpaceTransformUpdateStrategy_t501792395  : public RuntimeObject
{
public:
	// Wrld.Space.UnityWorldSpaceCoordinateFrame Wrld.Space.UnityWorldSpaceTransformUpdateStrategy::m_frame
	UnityWorldSpaceCoordinateFrame_t1382732960 * ___m_frame_0;

public:
	inline static int32_t get_offset_of_m_frame_0() { return static_cast<int32_t>(offsetof(UnityWorldSpaceTransformUpdateStrategy_t501792395, ___m_frame_0)); }
	inline UnityWorldSpaceCoordinateFrame_t1382732960 * get_m_frame_0() const { return ___m_frame_0; }
	inline UnityWorldSpaceCoordinateFrame_t1382732960 ** get_address_of_m_frame_0() { return &___m_frame_0; }
	inline void set_m_frame_0(UnityWorldSpaceCoordinateFrame_t1382732960 * value)
	{
		___m_frame_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_frame_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYWORLDSPACETRANSFORMUPDATESTRATEGY_T501792395_H
#ifndef GAMEOBJECTFACTORY_T2478326784_H
#define GAMEOBJECTFACTORY_T2478326784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Streaming.GameObjectFactory
struct  GameObjectFactory_t2478326784  : public RuntimeObject
{
public:
	// UnityEngine.Transform Wrld.Streaming.GameObjectFactory::m_parentTransform
	Transform_t3600365921 * ___m_parentTransform_0;

public:
	inline static int32_t get_offset_of_m_parentTransform_0() { return static_cast<int32_t>(offsetof(GameObjectFactory_t2478326784, ___m_parentTransform_0)); }
	inline Transform_t3600365921 * get_m_parentTransform_0() const { return ___m_parentTransform_0; }
	inline Transform_t3600365921 ** get_address_of_m_parentTransform_0() { return &___m_parentTransform_0; }
	inline void set_m_parentTransform_0(Transform_t3600365921 * value)
	{
		___m_parentTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_parentTransform_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTFACTORY_T2478326784_H
#ifndef COORDINATECONVERSIONS_T2160508553_H
#define COORDINATECONVERSIONS_T2160508553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Space.CoordinateConversions
struct  CoordinateConversions_t2160508553  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COORDINATECONVERSIONS_T2160508553_H
#ifndef GAMEOBJECTREPOSITORY_T3760691595_H
#define GAMEOBJECTREPOSITORY_T3760691595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Streaming.GameObjectRepository
struct  GameObjectRepository_t3760691595  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Wrld.Streaming.GameObjectRecord> Wrld.Streaming.GameObjectRepository::m_gameObjectsById
	Dictionary_2_t2657171120 * ___m_gameObjectsById_0;
	// UnityEngine.GameObject Wrld.Streaming.GameObjectRepository::m_root
	GameObject_t1113636619 * ___m_root_1;
	// Wrld.Materials.MaterialRepository Wrld.Streaming.GameObjectRepository::m_materialRepository
	MaterialRepository_t701520604 * ___m_materialRepository_2;

public:
	inline static int32_t get_offset_of_m_gameObjectsById_0() { return static_cast<int32_t>(offsetof(GameObjectRepository_t3760691595, ___m_gameObjectsById_0)); }
	inline Dictionary_2_t2657171120 * get_m_gameObjectsById_0() const { return ___m_gameObjectsById_0; }
	inline Dictionary_2_t2657171120 ** get_address_of_m_gameObjectsById_0() { return &___m_gameObjectsById_0; }
	inline void set_m_gameObjectsById_0(Dictionary_2_t2657171120 * value)
	{
		___m_gameObjectsById_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_gameObjectsById_0), value);
	}

	inline static int32_t get_offset_of_m_root_1() { return static_cast<int32_t>(offsetof(GameObjectRepository_t3760691595, ___m_root_1)); }
	inline GameObject_t1113636619 * get_m_root_1() const { return ___m_root_1; }
	inline GameObject_t1113636619 ** get_address_of_m_root_1() { return &___m_root_1; }
	inline void set_m_root_1(GameObject_t1113636619 * value)
	{
		___m_root_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_root_1), value);
	}

	inline static int32_t get_offset_of_m_materialRepository_2() { return static_cast<int32_t>(offsetof(GameObjectRepository_t3760691595, ___m_materialRepository_2)); }
	inline MaterialRepository_t701520604 * get_m_materialRepository_2() const { return ___m_materialRepository_2; }
	inline MaterialRepository_t701520604 ** get_address_of_m_materialRepository_2() { return &___m_materialRepository_2; }
	inline void set_m_materialRepository_2(MaterialRepository_t701520604 * value)
	{
		___m_materialRepository_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialRepository_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTREPOSITORY_T3760691595_H
#ifndef GEOGRAPHICAPI_T2934948604_H
#define GEOGRAPHICAPI_T2934948604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Space.GeographicApi
struct  GeographicApi_t2934948604  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Wrld.Space.GeographicTransform> Wrld.Space.GeographicApi::m_geographicTransforms
	List_1_t3734218024 * ___m_geographicTransforms_0;

public:
	inline static int32_t get_offset_of_m_geographicTransforms_0() { return static_cast<int32_t>(offsetof(GeographicApi_t2934948604, ___m_geographicTransforms_0)); }
	inline List_1_t3734218024 * get_m_geographicTransforms_0() const { return ___m_geographicTransforms_0; }
	inline List_1_t3734218024 ** get_address_of_m_geographicTransforms_0() { return &___m_geographicTransforms_0; }
	inline void set_m_geographicTransforms_0(List_1_t3734218024 * value)
	{
		___m_geographicTransforms_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_geographicTransforms_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GEOGRAPHICAPI_T2934948604_H
#ifndef MAPGAMEOBJECTSCENE_T128928738_H
#define MAPGAMEOBJECTSCENE_T128928738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapGameObjectScene
struct  MapGameObjectScene_t128928738  : public RuntimeObject
{
public:
	// Wrld.Streaming.GameObjectStreamer Wrld.MapGameObjectScene::m_terrainStreamer
	GameObjectStreamer_t3452608707 * ___m_terrainStreamer_0;
	// Wrld.Streaming.GameObjectStreamer Wrld.MapGameObjectScene::m_roadStreamer
	GameObjectStreamer_t3452608707 * ___m_roadStreamer_1;
	// Wrld.Streaming.GameObjectStreamer Wrld.MapGameObjectScene::m_buildingStreamer
	GameObjectStreamer_t3452608707 * ___m_buildingStreamer_2;
	// Wrld.Streaming.GameObjectStreamer Wrld.MapGameObjectScene::m_highlightStreamer
	GameObjectStreamer_t3452608707 * ___m_highlightStreamer_3;
	// Wrld.Meshes.MeshUploader Wrld.MapGameObjectScene::m_meshUploader
	MeshUploader_t3724330286 * ___m_meshUploader_4;
	// System.Boolean Wrld.MapGameObjectScene::m_enabled
	bool ___m_enabled_5;

public:
	inline static int32_t get_offset_of_m_terrainStreamer_0() { return static_cast<int32_t>(offsetof(MapGameObjectScene_t128928738, ___m_terrainStreamer_0)); }
	inline GameObjectStreamer_t3452608707 * get_m_terrainStreamer_0() const { return ___m_terrainStreamer_0; }
	inline GameObjectStreamer_t3452608707 ** get_address_of_m_terrainStreamer_0() { return &___m_terrainStreamer_0; }
	inline void set_m_terrainStreamer_0(GameObjectStreamer_t3452608707 * value)
	{
		___m_terrainStreamer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_terrainStreamer_0), value);
	}

	inline static int32_t get_offset_of_m_roadStreamer_1() { return static_cast<int32_t>(offsetof(MapGameObjectScene_t128928738, ___m_roadStreamer_1)); }
	inline GameObjectStreamer_t3452608707 * get_m_roadStreamer_1() const { return ___m_roadStreamer_1; }
	inline GameObjectStreamer_t3452608707 ** get_address_of_m_roadStreamer_1() { return &___m_roadStreamer_1; }
	inline void set_m_roadStreamer_1(GameObjectStreamer_t3452608707 * value)
	{
		___m_roadStreamer_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_roadStreamer_1), value);
	}

	inline static int32_t get_offset_of_m_buildingStreamer_2() { return static_cast<int32_t>(offsetof(MapGameObjectScene_t128928738, ___m_buildingStreamer_2)); }
	inline GameObjectStreamer_t3452608707 * get_m_buildingStreamer_2() const { return ___m_buildingStreamer_2; }
	inline GameObjectStreamer_t3452608707 ** get_address_of_m_buildingStreamer_2() { return &___m_buildingStreamer_2; }
	inline void set_m_buildingStreamer_2(GameObjectStreamer_t3452608707 * value)
	{
		___m_buildingStreamer_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_buildingStreamer_2), value);
	}

	inline static int32_t get_offset_of_m_highlightStreamer_3() { return static_cast<int32_t>(offsetof(MapGameObjectScene_t128928738, ___m_highlightStreamer_3)); }
	inline GameObjectStreamer_t3452608707 * get_m_highlightStreamer_3() const { return ___m_highlightStreamer_3; }
	inline GameObjectStreamer_t3452608707 ** get_address_of_m_highlightStreamer_3() { return &___m_highlightStreamer_3; }
	inline void set_m_highlightStreamer_3(GameObjectStreamer_t3452608707 * value)
	{
		___m_highlightStreamer_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_highlightStreamer_3), value);
	}

	inline static int32_t get_offset_of_m_meshUploader_4() { return static_cast<int32_t>(offsetof(MapGameObjectScene_t128928738, ___m_meshUploader_4)); }
	inline MeshUploader_t3724330286 * get_m_meshUploader_4() const { return ___m_meshUploader_4; }
	inline MeshUploader_t3724330286 ** get_address_of_m_meshUploader_4() { return &___m_meshUploader_4; }
	inline void set_m_meshUploader_4(MeshUploader_t3724330286 * value)
	{
		___m_meshUploader_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_meshUploader_4), value);
	}

	inline static int32_t get_offset_of_m_enabled_5() { return static_cast<int32_t>(offsetof(MapGameObjectScene_t128928738, ___m_enabled_5)); }
	inline bool get_m_enabled_5() const { return ___m_enabled_5; }
	inline bool* get_address_of_m_enabled_5() { return &___m_enabled_5; }
	inline void set_m_enabled_5(bool value)
	{
		___m_enabled_5 = value;
	}
};

struct MapGameObjectScene_t128928738_StaticFields
{
public:
	// Wrld.MapGameObjectScene Wrld.MapGameObjectScene::ms_instance
	MapGameObjectScene_t128928738 * ___ms_instance_6;

public:
	inline static int32_t get_offset_of_ms_instance_6() { return static_cast<int32_t>(offsetof(MapGameObjectScene_t128928738_StaticFields, ___ms_instance_6)); }
	inline MapGameObjectScene_t128928738 * get_ms_instance_6() const { return ___ms_instance_6; }
	inline MapGameObjectScene_t128928738 ** get_address_of_ms_instance_6() { return &___ms_instance_6; }
	inline void set_ms_instance_6(MapGameObjectScene_t128928738 * value)
	{
		___ms_instance_6 = value;
		Il2CppCodeGenWriteBarrier((&___ms_instance_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPGAMEOBJECTSCENE_T128928738_H
#ifndef ASSERTHANDLER_T1732545153_H
#define ASSERTHANDLER_T1732545153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.AssertHandler
struct  AssertHandler_t1732545153  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSERTHANDLER_T1732545153_H
#ifndef APIKEYHELPERS_T4091981690_H
#define APIKEYHELPERS_T4091981690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Scripts.Utilities.APIKeyHelpers
struct  APIKeyHelpers_t4091981690  : public RuntimeObject
{
public:

public:
};

struct APIKeyHelpers_t4091981690_StaticFields
{
public:
	// System.String Wrld.Scripts.Utilities.APIKeyHelpers::ms_cachedApiKey
	String_t* ___ms_cachedApiKey_0;

public:
	inline static int32_t get_offset_of_ms_cachedApiKey_0() { return static_cast<int32_t>(offsetof(APIKeyHelpers_t4091981690_StaticFields, ___ms_cachedApiKey_0)); }
	inline String_t* get_ms_cachedApiKey_0() const { return ___ms_cachedApiKey_0; }
	inline String_t** get_address_of_ms_cachedApiKey_0() { return &___ms_cachedApiKey_0; }
	inline void set_ms_cachedApiKey_0(String_t* value)
	{
		___ms_cachedApiKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___ms_cachedApiKey_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APIKEYHELPERS_T4091981690_H
#ifndef STREAMINGUPDATER_T530401876_H
#define STREAMINGUPDATER_T530401876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.StreamingUpdater
struct  StreamingUpdater_t530401876  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGUPDATER_T530401876_H
#ifndef RESOURCECEILINGPROVIDER_T2554508737_H
#define RESOURCECEILINGPROVIDER_T2554508737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Streaming.ResourceCeilingProvider
struct  ResourceCeilingProvider_t2554508737  : public RuntimeObject
{
public:
	// Wrld.Resources.Terrain.Heights.TerrainHeightProvider Wrld.Streaming.ResourceCeilingProvider::m_terrainHeightProvider
	TerrainHeightProvider_t517848921 * ___m_terrainHeightProvider_0;

public:
	inline static int32_t get_offset_of_m_terrainHeightProvider_0() { return static_cast<int32_t>(offsetof(ResourceCeilingProvider_t2554508737, ___m_terrainHeightProvider_0)); }
	inline TerrainHeightProvider_t517848921 * get_m_terrainHeightProvider_0() const { return ___m_terrainHeightProvider_0; }
	inline TerrainHeightProvider_t517848921 ** get_address_of_m_terrainHeightProvider_0() { return &___m_terrainHeightProvider_0; }
	inline void set_m_terrainHeightProvider_0(TerrainHeightProvider_t517848921 * value)
	{
		___m_terrainHeightProvider_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_terrainHeightProvider_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCECEILINGPROVIDER_T2554508737_H
#ifndef TERRAINHEIGHTPROVIDER_T517848921_H
#define TERRAINHEIGHTPROVIDER_T517848921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Resources.Terrain.Heights.TerrainHeightProvider
struct  TerrainHeightProvider_t517848921  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERRAINHEIGHTPROVIDER_T517848921_H
#ifndef EARTHCONSTANTS_T444733090_H
#define EARTHCONSTANTS_T444733090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Space.EarthConstants
struct  EarthConstants_t444733090  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EARTHCONSTANTS_T444733090_H
#ifndef MATHSHELPERS_T2198872558_H
#define MATHSHELPERS_T2198872558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Helpers.MathsHelpers
struct  MathsHelpers_t2198872558  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHSHELPERS_T2198872558_H
#ifndef TAPGESTURE_T191755233_H
#define TAPGESTURE_T191755233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.Touch.TapGesture
struct  TapGesture_t191755233  : public RuntimeObject
{
public:
	// System.Int32 Wrld.MapInput.Touch.TapGesture::m_tapDownCount
	int32_t ___m_tapDownCount_0;
	// System.Int32 Wrld.MapInput.Touch.TapGesture::m_tapUpCount
	int32_t ___m_tapUpCount_1;
	// System.Int32 Wrld.MapInput.Touch.TapGesture::m_currentPointerTrackingStack
	int32_t ___m_currentPointerTrackingStack_2;
	// System.Single Wrld.MapInput.Touch.TapGesture::m_tapAnchorX
	float ___m_tapAnchorX_3;
	// System.Single Wrld.MapInput.Touch.TapGesture::m_tapAnchorY
	float ___m_tapAnchorY_4;
	// System.Int64 Wrld.MapInput.Touch.TapGesture::m_tapUnixTime
	int64_t ___m_tapUnixTime_5;
	// Wrld.MapInput.IUnityInputHandler Wrld.MapInput.Touch.TapGesture::m_tapHandler
	RuntimeObject* ___m_tapHandler_9;

public:
	inline static int32_t get_offset_of_m_tapDownCount_0() { return static_cast<int32_t>(offsetof(TapGesture_t191755233, ___m_tapDownCount_0)); }
	inline int32_t get_m_tapDownCount_0() const { return ___m_tapDownCount_0; }
	inline int32_t* get_address_of_m_tapDownCount_0() { return &___m_tapDownCount_0; }
	inline void set_m_tapDownCount_0(int32_t value)
	{
		___m_tapDownCount_0 = value;
	}

	inline static int32_t get_offset_of_m_tapUpCount_1() { return static_cast<int32_t>(offsetof(TapGesture_t191755233, ___m_tapUpCount_1)); }
	inline int32_t get_m_tapUpCount_1() const { return ___m_tapUpCount_1; }
	inline int32_t* get_address_of_m_tapUpCount_1() { return &___m_tapUpCount_1; }
	inline void set_m_tapUpCount_1(int32_t value)
	{
		___m_tapUpCount_1 = value;
	}

	inline static int32_t get_offset_of_m_currentPointerTrackingStack_2() { return static_cast<int32_t>(offsetof(TapGesture_t191755233, ___m_currentPointerTrackingStack_2)); }
	inline int32_t get_m_currentPointerTrackingStack_2() const { return ___m_currentPointerTrackingStack_2; }
	inline int32_t* get_address_of_m_currentPointerTrackingStack_2() { return &___m_currentPointerTrackingStack_2; }
	inline void set_m_currentPointerTrackingStack_2(int32_t value)
	{
		___m_currentPointerTrackingStack_2 = value;
	}

	inline static int32_t get_offset_of_m_tapAnchorX_3() { return static_cast<int32_t>(offsetof(TapGesture_t191755233, ___m_tapAnchorX_3)); }
	inline float get_m_tapAnchorX_3() const { return ___m_tapAnchorX_3; }
	inline float* get_address_of_m_tapAnchorX_3() { return &___m_tapAnchorX_3; }
	inline void set_m_tapAnchorX_3(float value)
	{
		___m_tapAnchorX_3 = value;
	}

	inline static int32_t get_offset_of_m_tapAnchorY_4() { return static_cast<int32_t>(offsetof(TapGesture_t191755233, ___m_tapAnchorY_4)); }
	inline float get_m_tapAnchorY_4() const { return ___m_tapAnchorY_4; }
	inline float* get_address_of_m_tapAnchorY_4() { return &___m_tapAnchorY_4; }
	inline void set_m_tapAnchorY_4(float value)
	{
		___m_tapAnchorY_4 = value;
	}

	inline static int32_t get_offset_of_m_tapUnixTime_5() { return static_cast<int32_t>(offsetof(TapGesture_t191755233, ___m_tapUnixTime_5)); }
	inline int64_t get_m_tapUnixTime_5() const { return ___m_tapUnixTime_5; }
	inline int64_t* get_address_of_m_tapUnixTime_5() { return &___m_tapUnixTime_5; }
	inline void set_m_tapUnixTime_5(int64_t value)
	{
		___m_tapUnixTime_5 = value;
	}

	inline static int32_t get_offset_of_m_tapHandler_9() { return static_cast<int32_t>(offsetof(TapGesture_t191755233, ___m_tapHandler_9)); }
	inline RuntimeObject* get_m_tapHandler_9() const { return ___m_tapHandler_9; }
	inline RuntimeObject** get_address_of_m_tapHandler_9() { return &___m_tapHandler_9; }
	inline void set_m_tapHandler_9(RuntimeObject* value)
	{
		___m_tapHandler_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_tapHandler_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAPGESTURE_T191755233_H
#ifndef TOUCHGESTURE_T2500129316_H
#define TOUCHGESTURE_T2500129316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.Touch.TouchGesture
struct  TouchGesture_t2500129316  : public RuntimeObject
{
public:
	// Wrld.MapInput.IUnityInputHandler Wrld.MapInput.Touch.TouchGesture::m_handler
	RuntimeObject* ___m_handler_0;

public:
	inline static int32_t get_offset_of_m_handler_0() { return static_cast<int32_t>(offsetof(TouchGesture_t2500129316, ___m_handler_0)); }
	inline RuntimeObject* get_m_handler_0() const { return ___m_handler_0; }
	inline RuntimeObject** get_address_of_m_handler_0() { return &___m_handler_0; }
	inline void set_m_handler_0(RuntimeObject* value)
	{
		___m_handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_handler_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHGESTURE_T2500129316_H
#ifndef UNITYTOUCHINPUTPROCESSOR_T158491272_H
#define UNITYTOUCHINPUTPROCESSOR_T158491272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.Touch.UnityTouchInputProcessor
struct  UnityTouchInputProcessor_t158491272  : public RuntimeObject
{
public:
	// Wrld.MapInput.Touch.PanGesture Wrld.MapInput.Touch.UnityTouchInputProcessor::m_pan
	PanGesture_t3882187103 * ___m_pan_0;
	// Wrld.MapInput.Touch.PinchGesture Wrld.MapInput.Touch.UnityTouchInputProcessor::m_pinch
	PinchGesture_t72617885 * ___m_pinch_1;
	// Wrld.MapInput.Touch.RotateGesture Wrld.MapInput.Touch.UnityTouchInputProcessor::m_rotate
	RotateGesture_t3703755966 * ___m_rotate_2;
	// Wrld.MapInput.Touch.TouchGesture Wrld.MapInput.Touch.UnityTouchInputProcessor::m_touch
	TouchGesture_t2500129316 * ___m_touch_3;
	// Wrld.MapInput.Touch.TapGesture Wrld.MapInput.Touch.UnityTouchInputProcessor::m_tap
	TapGesture_t191755233 * ___m_tap_4;

public:
	inline static int32_t get_offset_of_m_pan_0() { return static_cast<int32_t>(offsetof(UnityTouchInputProcessor_t158491272, ___m_pan_0)); }
	inline PanGesture_t3882187103 * get_m_pan_0() const { return ___m_pan_0; }
	inline PanGesture_t3882187103 ** get_address_of_m_pan_0() { return &___m_pan_0; }
	inline void set_m_pan_0(PanGesture_t3882187103 * value)
	{
		___m_pan_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_pan_0), value);
	}

	inline static int32_t get_offset_of_m_pinch_1() { return static_cast<int32_t>(offsetof(UnityTouchInputProcessor_t158491272, ___m_pinch_1)); }
	inline PinchGesture_t72617885 * get_m_pinch_1() const { return ___m_pinch_1; }
	inline PinchGesture_t72617885 ** get_address_of_m_pinch_1() { return &___m_pinch_1; }
	inline void set_m_pinch_1(PinchGesture_t72617885 * value)
	{
		___m_pinch_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_pinch_1), value);
	}

	inline static int32_t get_offset_of_m_rotate_2() { return static_cast<int32_t>(offsetof(UnityTouchInputProcessor_t158491272, ___m_rotate_2)); }
	inline RotateGesture_t3703755966 * get_m_rotate_2() const { return ___m_rotate_2; }
	inline RotateGesture_t3703755966 ** get_address_of_m_rotate_2() { return &___m_rotate_2; }
	inline void set_m_rotate_2(RotateGesture_t3703755966 * value)
	{
		___m_rotate_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_rotate_2), value);
	}

	inline static int32_t get_offset_of_m_touch_3() { return static_cast<int32_t>(offsetof(UnityTouchInputProcessor_t158491272, ___m_touch_3)); }
	inline TouchGesture_t2500129316 * get_m_touch_3() const { return ___m_touch_3; }
	inline TouchGesture_t2500129316 ** get_address_of_m_touch_3() { return &___m_touch_3; }
	inline void set_m_touch_3(TouchGesture_t2500129316 * value)
	{
		___m_touch_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_touch_3), value);
	}

	inline static int32_t get_offset_of_m_tap_4() { return static_cast<int32_t>(offsetof(UnityTouchInputProcessor_t158491272, ___m_tap_4)); }
	inline TapGesture_t191755233 * get_m_tap_4() const { return ___m_tap_4; }
	inline TapGesture_t191755233 ** get_address_of_m_tap_4() { return &___m_tap_4; }
	inline void set_m_tap_4(TapGesture_t191755233 * value)
	{
		___m_tap_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_tap_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYTOUCHINPUTPROCESSOR_T158491272_H
#ifndef TEXTURELOADHANDLER_T2080734353_H
#define TEXTURELOADHANDLER_T2080734353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Materials.TextureLoadHandler
struct  TextureLoadHandler_t2080734353  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.UInt32,UnityEngine.Texture> Wrld.Materials.TextureLoadHandler::m_builtTextures
	Dictionary_2_t641263069 * ___m_builtTextures_1;
	// Wrld.Concurrency.ConcurrentQueue`1<Wrld.Materials.TextureLoadHandler/TextureBuffer> Wrld.Materials.TextureLoadHandler::m_processQueue
	ConcurrentQueue_1_t11616060 * ___m_processQueue_2;
	// Wrld.Materials.IdGenerator Wrld.Materials.TextureLoadHandler::m_idGenerator
	IdGenerator_t1318283159 * ___m_idGenerator_3;

public:
	inline static int32_t get_offset_of_m_builtTextures_1() { return static_cast<int32_t>(offsetof(TextureLoadHandler_t2080734353, ___m_builtTextures_1)); }
	inline Dictionary_2_t641263069 * get_m_builtTextures_1() const { return ___m_builtTextures_1; }
	inline Dictionary_2_t641263069 ** get_address_of_m_builtTextures_1() { return &___m_builtTextures_1; }
	inline void set_m_builtTextures_1(Dictionary_2_t641263069 * value)
	{
		___m_builtTextures_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_builtTextures_1), value);
	}

	inline static int32_t get_offset_of_m_processQueue_2() { return static_cast<int32_t>(offsetof(TextureLoadHandler_t2080734353, ___m_processQueue_2)); }
	inline ConcurrentQueue_1_t11616060 * get_m_processQueue_2() const { return ___m_processQueue_2; }
	inline ConcurrentQueue_1_t11616060 ** get_address_of_m_processQueue_2() { return &___m_processQueue_2; }
	inline void set_m_processQueue_2(ConcurrentQueue_1_t11616060 * value)
	{
		___m_processQueue_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_processQueue_2), value);
	}

	inline static int32_t get_offset_of_m_idGenerator_3() { return static_cast<int32_t>(offsetof(TextureLoadHandler_t2080734353, ___m_idGenerator_3)); }
	inline IdGenerator_t1318283159 * get_m_idGenerator_3() const { return ___m_idGenerator_3; }
	inline IdGenerator_t1318283159 ** get_address_of_m_idGenerator_3() { return &___m_idGenerator_3; }
	inline void set_m_idGenerator_3(IdGenerator_t1318283159 * value)
	{
		___m_idGenerator_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_idGenerator_3), value);
	}
};

struct TextureLoadHandler_t2080734353_StaticFields
{
public:
	// Wrld.Materials.TextureLoadHandler Wrld.Materials.TextureLoadHandler::ms_instance
	TextureLoadHandler_t2080734353 * ___ms_instance_4;

public:
	inline static int32_t get_offset_of_ms_instance_4() { return static_cast<int32_t>(offsetof(TextureLoadHandler_t2080734353_StaticFields, ___ms_instance_4)); }
	inline TextureLoadHandler_t2080734353 * get_ms_instance_4() const { return ___ms_instance_4; }
	inline TextureLoadHandler_t2080734353 ** get_address_of_ms_instance_4() { return &___ms_instance_4; }
	inline void set_ms_instance_4(TextureLoadHandler_t2080734353 * value)
	{
		___ms_instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___ms_instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURELOADHANDLER_T2080734353_H
#ifndef U3CUPDATEU3EC__ANONSTOREY0_T2884843461_H
#define U3CUPDATEU3EC__ANONSTOREY0_T2884843461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Materials.MaterialRepository/<Update>c__AnonStorey0
struct  U3CUpdateU3Ec__AnonStorey0_t2884843461  : public RuntimeObject
{
public:
	// System.Collections.Generic.HashSet`1<UnityEngine.Material> Wrld.Materials.MaterialRepository/<Update>c__AnonStorey0::toRemove
	HashSet_1_t3200291893 * ___toRemove_0;

public:
	inline static int32_t get_offset_of_toRemove_0() { return static_cast<int32_t>(offsetof(U3CUpdateU3Ec__AnonStorey0_t2884843461, ___toRemove_0)); }
	inline HashSet_1_t3200291893 * get_toRemove_0() const { return ___toRemove_0; }
	inline HashSet_1_t3200291893 ** get_address_of_toRemove_0() { return &___toRemove_0; }
	inline void set_toRemove_0(HashSet_1_t3200291893 * value)
	{
		___toRemove_0 = value;
		Il2CppCodeGenWriteBarrier((&___toRemove_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEU3EC__ANONSTOREY0_T2884843461_H
#ifndef IDGENERATOR_T1318283159_H
#define IDGENERATOR_T1318283159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Materials.IdGenerator
struct  IdGenerator_t1318283159  : public RuntimeObject
{
public:
	// System.UInt32 Wrld.Materials.IdGenerator::m_idGenerator
	uint32_t ___m_idGenerator_0;
	// System.Collections.Generic.HashSet`1<System.UInt32> Wrld.Materials.IdGenerator::m_ids
	HashSet_1_t1125011452 * ___m_ids_1;

public:
	inline static int32_t get_offset_of_m_idGenerator_0() { return static_cast<int32_t>(offsetof(IdGenerator_t1318283159, ___m_idGenerator_0)); }
	inline uint32_t get_m_idGenerator_0() const { return ___m_idGenerator_0; }
	inline uint32_t* get_address_of_m_idGenerator_0() { return &___m_idGenerator_0; }
	inline void set_m_idGenerator_0(uint32_t value)
	{
		___m_idGenerator_0 = value;
	}

	inline static int32_t get_offset_of_m_ids_1() { return static_cast<int32_t>(offsetof(IdGenerator_t1318283159, ___m_ids_1)); }
	inline HashSet_1_t1125011452 * get_m_ids_1() const { return ___m_ids_1; }
	inline HashSet_1_t1125011452 ** get_address_of_m_ids_1() { return &___m_ids_1; }
	inline void set_m_ids_1(HashSet_1_t1125011452 * value)
	{
		___m_ids_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_ids_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDGENERATOR_T1318283159_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef MOUSEZOOMGESTURE_T962341082_H
#define MOUSEZOOMGESTURE_T962341082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.Mouse.MouseZoomGesture
struct  MouseZoomGesture_t962341082  : public RuntimeObject
{
public:
	// Wrld.MapInput.IUnityInputHandler Wrld.MapInput.Mouse.MouseZoomGesture::m_handler
	RuntimeObject* ___m_handler_0;
	// System.Single Wrld.MapInput.Mouse.MouseZoomGesture::m_sensitivity
	float ___m_sensitivity_1;
	// System.Single Wrld.MapInput.Mouse.MouseZoomGesture::m_maxZoomDelta
	float ___m_maxZoomDelta_2;
	// System.Single Wrld.MapInput.Mouse.MouseZoomGesture::m_zoomAccumulator
	float ___m_zoomAccumulator_3;

public:
	inline static int32_t get_offset_of_m_handler_0() { return static_cast<int32_t>(offsetof(MouseZoomGesture_t962341082, ___m_handler_0)); }
	inline RuntimeObject* get_m_handler_0() const { return ___m_handler_0; }
	inline RuntimeObject** get_address_of_m_handler_0() { return &___m_handler_0; }
	inline void set_m_handler_0(RuntimeObject* value)
	{
		___m_handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_handler_0), value);
	}

	inline static int32_t get_offset_of_m_sensitivity_1() { return static_cast<int32_t>(offsetof(MouseZoomGesture_t962341082, ___m_sensitivity_1)); }
	inline float get_m_sensitivity_1() const { return ___m_sensitivity_1; }
	inline float* get_address_of_m_sensitivity_1() { return &___m_sensitivity_1; }
	inline void set_m_sensitivity_1(float value)
	{
		___m_sensitivity_1 = value;
	}

	inline static int32_t get_offset_of_m_maxZoomDelta_2() { return static_cast<int32_t>(offsetof(MouseZoomGesture_t962341082, ___m_maxZoomDelta_2)); }
	inline float get_m_maxZoomDelta_2() const { return ___m_maxZoomDelta_2; }
	inline float* get_address_of_m_maxZoomDelta_2() { return &___m_maxZoomDelta_2; }
	inline void set_m_maxZoomDelta_2(float value)
	{
		___m_maxZoomDelta_2 = value;
	}

	inline static int32_t get_offset_of_m_zoomAccumulator_3() { return static_cast<int32_t>(offsetof(MouseZoomGesture_t962341082, ___m_zoomAccumulator_3)); }
	inline float get_m_zoomAccumulator_3() const { return ___m_zoomAccumulator_3; }
	inline float* get_address_of_m_zoomAccumulator_3() { return &___m_zoomAccumulator_3; }
	inline void set_m_zoomAccumulator_3(float value)
	{
		___m_zoomAccumulator_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSEZOOMGESTURE_T962341082_H
#ifndef ECEFHELPERS_T3574471672_H
#define ECEFHELPERS_T3574471672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Common.Maths.EcefHelpers
struct  EcefHelpers_t3574471672  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECEFHELPERS_T3574471672_H
#ifndef DOUBLEVECTOR3EXTENSIONS_T851847462_H
#define DOUBLEVECTOR3EXTENSIONS_T851847462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Common.Maths.DoubleVector3Extensions
struct  DoubleVector3Extensions_t851847462  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLEVECTOR3EXTENSIONS_T851847462_H
#ifndef UNITYMOUSEINPUTPROCESSOR_T4249415975_H
#define UNITYMOUSEINPUTPROCESSOR_T4249415975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.Mouse.UnityMouseInputProcessor
struct  UnityMouseInputProcessor_t4249415975  : public RuntimeObject
{
public:
	// Wrld.MapInput.Mouse.MousePanGesture Wrld.MapInput.Mouse.UnityMouseInputProcessor::m_pan
	MousePanGesture_t639745302 * ___m_pan_0;
	// Wrld.MapInput.Mouse.MouseZoomGesture Wrld.MapInput.Mouse.UnityMouseInputProcessor::m_zoom
	MouseZoomGesture_t962341082 * ___m_zoom_1;
	// Wrld.MapInput.Mouse.MouseRotateGesture Wrld.MapInput.Mouse.UnityMouseInputProcessor::m_rotate
	MouseRotateGesture_t1726080705 * ___m_rotate_2;
	// Wrld.MapInput.Mouse.MouseTiltGesture Wrld.MapInput.Mouse.UnityMouseInputProcessor::m_tilt
	MouseTiltGesture_t4197683903 * ___m_tilt_3;
	// Wrld.MapInput.Mouse.MouseTouchGesture Wrld.MapInput.Mouse.UnityMouseInputProcessor::m_touch
	MouseTouchGesture_t2284897039 * ___m_touch_4;
	// Wrld.MapInput.Mouse.MouseTapGesture Wrld.MapInput.Mouse.UnityMouseInputProcessor::m_tap
	MouseTapGesture_t1901348987 * ___m_tap_5;

public:
	inline static int32_t get_offset_of_m_pan_0() { return static_cast<int32_t>(offsetof(UnityMouseInputProcessor_t4249415975, ___m_pan_0)); }
	inline MousePanGesture_t639745302 * get_m_pan_0() const { return ___m_pan_0; }
	inline MousePanGesture_t639745302 ** get_address_of_m_pan_0() { return &___m_pan_0; }
	inline void set_m_pan_0(MousePanGesture_t639745302 * value)
	{
		___m_pan_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_pan_0), value);
	}

	inline static int32_t get_offset_of_m_zoom_1() { return static_cast<int32_t>(offsetof(UnityMouseInputProcessor_t4249415975, ___m_zoom_1)); }
	inline MouseZoomGesture_t962341082 * get_m_zoom_1() const { return ___m_zoom_1; }
	inline MouseZoomGesture_t962341082 ** get_address_of_m_zoom_1() { return &___m_zoom_1; }
	inline void set_m_zoom_1(MouseZoomGesture_t962341082 * value)
	{
		___m_zoom_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_zoom_1), value);
	}

	inline static int32_t get_offset_of_m_rotate_2() { return static_cast<int32_t>(offsetof(UnityMouseInputProcessor_t4249415975, ___m_rotate_2)); }
	inline MouseRotateGesture_t1726080705 * get_m_rotate_2() const { return ___m_rotate_2; }
	inline MouseRotateGesture_t1726080705 ** get_address_of_m_rotate_2() { return &___m_rotate_2; }
	inline void set_m_rotate_2(MouseRotateGesture_t1726080705 * value)
	{
		___m_rotate_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_rotate_2), value);
	}

	inline static int32_t get_offset_of_m_tilt_3() { return static_cast<int32_t>(offsetof(UnityMouseInputProcessor_t4249415975, ___m_tilt_3)); }
	inline MouseTiltGesture_t4197683903 * get_m_tilt_3() const { return ___m_tilt_3; }
	inline MouseTiltGesture_t4197683903 ** get_address_of_m_tilt_3() { return &___m_tilt_3; }
	inline void set_m_tilt_3(MouseTiltGesture_t4197683903 * value)
	{
		___m_tilt_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_tilt_3), value);
	}

	inline static int32_t get_offset_of_m_touch_4() { return static_cast<int32_t>(offsetof(UnityMouseInputProcessor_t4249415975, ___m_touch_4)); }
	inline MouseTouchGesture_t2284897039 * get_m_touch_4() const { return ___m_touch_4; }
	inline MouseTouchGesture_t2284897039 ** get_address_of_m_touch_4() { return &___m_touch_4; }
	inline void set_m_touch_4(MouseTouchGesture_t2284897039 * value)
	{
		___m_touch_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_touch_4), value);
	}

	inline static int32_t get_offset_of_m_tap_5() { return static_cast<int32_t>(offsetof(UnityMouseInputProcessor_t4249415975, ___m_tap_5)); }
	inline MouseTapGesture_t1901348987 * get_m_tap_5() const { return ___m_tap_5; }
	inline MouseTapGesture_t1901348987 ** get_address_of_m_tap_5() { return &___m_tap_5; }
	inline void set_m_tap_5(MouseTapGesture_t1901348987 * value)
	{
		___m_tap_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_tap_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYMOUSEINPUTPROCESSOR_T4249415975_H
#ifndef MATERIALREPOSITORY_T701520604_H
#define MATERIALREPOSITORY_T701520604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Materials.MaterialRepository
struct  MaterialRepository_t701520604  : public RuntimeObject
{
public:
	// System.Collections.Generic.HashSet`1<UnityEngine.Material> Wrld.Materials.MaterialRepository::m_materialsRequiringTexture
	HashSet_1_t3200291893 * ___m_materialsRequiringTexture_0;
	// System.Collections.Generic.Dictionary`2<System.String,Wrld.Materials.MaterialRepository/MaterialRecord> Wrld.Materials.MaterialRepository::m_materials
	Dictionary_2_t2238724610 * ___m_materials_1;
	// UnityEngine.Material Wrld.Materials.MaterialRepository::m_defaultMaterial
	Material_t340375123 * ___m_defaultMaterial_2;
	// UnityEngine.Material Wrld.Materials.MaterialRepository::m_defaultRasterTerrainMaterial
	Material_t340375123 * ___m_defaultRasterTerrainMaterial_3;
	// Wrld.Materials.TextureLoadHandler Wrld.Materials.MaterialRepository::m_textureLoadHandler
	TextureLoadHandler_t2080734353 * ___m_textureLoadHandler_4;
	// System.String Wrld.Materials.MaterialRepository::m_materialDirectory
	String_t* ___m_materialDirectory_5;

public:
	inline static int32_t get_offset_of_m_materialsRequiringTexture_0() { return static_cast<int32_t>(offsetof(MaterialRepository_t701520604, ___m_materialsRequiringTexture_0)); }
	inline HashSet_1_t3200291893 * get_m_materialsRequiringTexture_0() const { return ___m_materialsRequiringTexture_0; }
	inline HashSet_1_t3200291893 ** get_address_of_m_materialsRequiringTexture_0() { return &___m_materialsRequiringTexture_0; }
	inline void set_m_materialsRequiringTexture_0(HashSet_1_t3200291893 * value)
	{
		___m_materialsRequiringTexture_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialsRequiringTexture_0), value);
	}

	inline static int32_t get_offset_of_m_materials_1() { return static_cast<int32_t>(offsetof(MaterialRepository_t701520604, ___m_materials_1)); }
	inline Dictionary_2_t2238724610 * get_m_materials_1() const { return ___m_materials_1; }
	inline Dictionary_2_t2238724610 ** get_address_of_m_materials_1() { return &___m_materials_1; }
	inline void set_m_materials_1(Dictionary_2_t2238724610 * value)
	{
		___m_materials_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_materials_1), value);
	}

	inline static int32_t get_offset_of_m_defaultMaterial_2() { return static_cast<int32_t>(offsetof(MaterialRepository_t701520604, ___m_defaultMaterial_2)); }
	inline Material_t340375123 * get_m_defaultMaterial_2() const { return ___m_defaultMaterial_2; }
	inline Material_t340375123 ** get_address_of_m_defaultMaterial_2() { return &___m_defaultMaterial_2; }
	inline void set_m_defaultMaterial_2(Material_t340375123 * value)
	{
		___m_defaultMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultMaterial_2), value);
	}

	inline static int32_t get_offset_of_m_defaultRasterTerrainMaterial_3() { return static_cast<int32_t>(offsetof(MaterialRepository_t701520604, ___m_defaultRasterTerrainMaterial_3)); }
	inline Material_t340375123 * get_m_defaultRasterTerrainMaterial_3() const { return ___m_defaultRasterTerrainMaterial_3; }
	inline Material_t340375123 ** get_address_of_m_defaultRasterTerrainMaterial_3() { return &___m_defaultRasterTerrainMaterial_3; }
	inline void set_m_defaultRasterTerrainMaterial_3(Material_t340375123 * value)
	{
		___m_defaultRasterTerrainMaterial_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultRasterTerrainMaterial_3), value);
	}

	inline static int32_t get_offset_of_m_textureLoadHandler_4() { return static_cast<int32_t>(offsetof(MaterialRepository_t701520604, ___m_textureLoadHandler_4)); }
	inline TextureLoadHandler_t2080734353 * get_m_textureLoadHandler_4() const { return ___m_textureLoadHandler_4; }
	inline TextureLoadHandler_t2080734353 ** get_address_of_m_textureLoadHandler_4() { return &___m_textureLoadHandler_4; }
	inline void set_m_textureLoadHandler_4(TextureLoadHandler_t2080734353 * value)
	{
		___m_textureLoadHandler_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_textureLoadHandler_4), value);
	}

	inline static int32_t get_offset_of_m_materialDirectory_5() { return static_cast<int32_t>(offsetof(MaterialRepository_t701520604, ___m_materialDirectory_5)); }
	inline String_t* get_m_materialDirectory_5() const { return ___m_materialDirectory_5; }
	inline String_t** get_address_of_m_materialDirectory_5() { return &___m_materialDirectory_5; }
	inline void set_m_materialDirectory_5(String_t* value)
	{
		___m_materialDirectory_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialDirectory_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALREPOSITORY_T701520604_H
#ifndef PINCHGESTURE_T72617885_H
#define PINCHGESTURE_T72617885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.Touch.PinchGesture
struct  PinchGesture_t72617885  : public RuntimeObject
{
public:
	// Wrld.MapInput.IUnityInputHandler Wrld.MapInput.Touch.PinchGesture::m_handler
	RuntimeObject* ___m_handler_0;
	// System.Boolean Wrld.MapInput.Touch.PinchGesture::pinching
	bool ___pinching_1;
	// System.Single Wrld.MapInput.Touch.PinchGesture::previousDistance
	float ___previousDistance_2;
	// System.Single Wrld.MapInput.Touch.PinchGesture::majorScreenDimension
	float ___majorScreenDimension_3;

public:
	inline static int32_t get_offset_of_m_handler_0() { return static_cast<int32_t>(offsetof(PinchGesture_t72617885, ___m_handler_0)); }
	inline RuntimeObject* get_m_handler_0() const { return ___m_handler_0; }
	inline RuntimeObject** get_address_of_m_handler_0() { return &___m_handler_0; }
	inline void set_m_handler_0(RuntimeObject* value)
	{
		___m_handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_handler_0), value);
	}

	inline static int32_t get_offset_of_pinching_1() { return static_cast<int32_t>(offsetof(PinchGesture_t72617885, ___pinching_1)); }
	inline bool get_pinching_1() const { return ___pinching_1; }
	inline bool* get_address_of_pinching_1() { return &___pinching_1; }
	inline void set_pinching_1(bool value)
	{
		___pinching_1 = value;
	}

	inline static int32_t get_offset_of_previousDistance_2() { return static_cast<int32_t>(offsetof(PinchGesture_t72617885, ___previousDistance_2)); }
	inline float get_previousDistance_2() const { return ___previousDistance_2; }
	inline float* get_address_of_previousDistance_2() { return &___previousDistance_2; }
	inline void set_previousDistance_2(float value)
	{
		___previousDistance_2 = value;
	}

	inline static int32_t get_offset_of_majorScreenDimension_3() { return static_cast<int32_t>(offsetof(PinchGesture_t72617885, ___majorScreenDimension_3)); }
	inline float get_majorScreenDimension_3() const { return ___majorScreenDimension_3; }
	inline float* get_address_of_majorScreenDimension_3() { return &___majorScreenDimension_3; }
	inline void set_majorScreenDimension_3(float value)
	{
		___majorScreenDimension_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PINCHGESTURE_T72617885_H
#ifndef MATERIALRECORD_T2453468311_H
#define MATERIALRECORD_T2453468311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Materials.MaterialRepository/MaterialRecord
struct  MaterialRecord_t2453468311  : public RuntimeObject
{
public:
	// UnityEngine.Material Wrld.Materials.MaterialRepository/MaterialRecord::<Material>k__BackingField
	Material_t340375123 * ___U3CMaterialU3Ek__BackingField_0;
	// System.UInt32 Wrld.Materials.MaterialRepository/MaterialRecord::<ReferenceCount>k__BackingField
	uint32_t ___U3CReferenceCountU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMaterialU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MaterialRecord_t2453468311, ___U3CMaterialU3Ek__BackingField_0)); }
	inline Material_t340375123 * get_U3CMaterialU3Ek__BackingField_0() const { return ___U3CMaterialU3Ek__BackingField_0; }
	inline Material_t340375123 ** get_address_of_U3CMaterialU3Ek__BackingField_0() { return &___U3CMaterialU3Ek__BackingField_0; }
	inline void set_U3CMaterialU3Ek__BackingField_0(Material_t340375123 * value)
	{
		___U3CMaterialU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMaterialU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CReferenceCountU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MaterialRecord_t2453468311, ___U3CReferenceCountU3Ek__BackingField_1)); }
	inline uint32_t get_U3CReferenceCountU3Ek__BackingField_1() const { return ___U3CReferenceCountU3Ek__BackingField_1; }
	inline uint32_t* get_address_of_U3CReferenceCountU3Ek__BackingField_1() { return &___U3CReferenceCountU3Ek__BackingField_1; }
	inline void set_U3CReferenceCountU3Ek__BackingField_1(uint32_t value)
	{
		___U3CReferenceCountU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALRECORD_T2453468311_H
#ifndef HIGHLIGHTREQUEST_T1416441333_H
#define HIGHLIGHTREQUEST_T1416441333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Resources.Buildings.BuildingsApi/HighlightRequest
struct  HighlightRequest_t1416441333 
{
public:
	// UnityEngine.Material Wrld.Resources.Buildings.BuildingsApi/HighlightRequest::material
	Material_t340375123 * ___material_0;
	// Wrld.Resources.Buildings.BuildingsApi/HighlightReceivedCallback Wrld.Resources.Buildings.BuildingsApi/HighlightRequest::callback
	HighlightReceivedCallback_t2001851641 * ___callback_1;

public:
	inline static int32_t get_offset_of_material_0() { return static_cast<int32_t>(offsetof(HighlightRequest_t1416441333, ___material_0)); }
	inline Material_t340375123 * get_material_0() const { return ___material_0; }
	inline Material_t340375123 ** get_address_of_material_0() { return &___material_0; }
	inline void set_material_0(Material_t340375123 * value)
	{
		___material_0 = value;
		Il2CppCodeGenWriteBarrier((&___material_0), value);
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(HighlightRequest_t1416441333, ___callback_1)); }
	inline HighlightReceivedCallback_t2001851641 * get_callback_1() const { return ___callback_1; }
	inline HighlightReceivedCallback_t2001851641 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(HighlightReceivedCallback_t2001851641 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Wrld.Resources.Buildings.BuildingsApi/HighlightRequest
struct HighlightRequest_t1416441333_marshaled_pinvoke
{
	Material_t340375123 * ___material_0;
	Il2CppMethodPointer ___callback_1;
};
// Native definition for COM marshalling of Wrld.Resources.Buildings.BuildingsApi/HighlightRequest
struct HighlightRequest_t1416441333_marshaled_com
{
	Material_t340375123 * ___material_0;
	Il2CppMethodPointer ___callback_1;
};
#endif // HIGHLIGHTREQUEST_T1416441333_H
#ifndef BUILDINGREQUEST_T1598655962_H
#define BUILDINGREQUEST_T1598655962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Resources.Buildings.BuildingsApi/BuildingRequest
struct  BuildingRequest_t1598655962 
{
public:
	// Wrld.Resources.Buildings.BuildingsApi/BuildingReceivedCallback Wrld.Resources.Buildings.BuildingsApi/BuildingRequest::callback
	BuildingReceivedCallback_t1209487313 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(BuildingRequest_t1598655962, ___callback_0)); }
	inline BuildingReceivedCallback_t1209487313 * get_callback_0() const { return ___callback_0; }
	inline BuildingReceivedCallback_t1209487313 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(BuildingReceivedCallback_t1209487313 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Wrld.Resources.Buildings.BuildingsApi/BuildingRequest
struct BuildingRequest_t1598655962_marshaled_pinvoke
{
	Il2CppMethodPointer ___callback_0;
};
// Native definition for COM marshalling of Wrld.Resources.Buildings.BuildingsApi/BuildingRequest
struct BuildingRequest_t1598655962_marshaled_com
{
	Il2CppMethodPointer ___callback_0;
};
#endif // BUILDINGREQUEST_T1598655962_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef TOUCHINPUTEVENT_T172715690_H
#define TOUCHINPUTEVENT_T172715690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.Touch.TouchInputEvent
struct  TouchInputEvent_t172715690 
{
public:
	// System.Boolean Wrld.MapInput.Touch.TouchInputEvent::isPointerUpEvent
	bool ___isPointerUpEvent_0;
	// System.Boolean Wrld.MapInput.Touch.TouchInputEvent::isPointerDownEvent
	bool ___isPointerDownEvent_1;
	// System.Int32 Wrld.MapInput.Touch.TouchInputEvent::primaryActionIndex
	int32_t ___primaryActionIndex_2;
	// System.Int32 Wrld.MapInput.Touch.TouchInputEvent::primaryActionIdentifier
	int32_t ___primaryActionIdentifier_3;
	// System.Collections.Generic.List`1<Wrld.MapInput.Touch.TouchInputPointerEvent> Wrld.MapInput.Touch.TouchInputEvent::pointerEvents
	List_1_t2102936381 * ___pointerEvents_4;

public:
	inline static int32_t get_offset_of_isPointerUpEvent_0() { return static_cast<int32_t>(offsetof(TouchInputEvent_t172715690, ___isPointerUpEvent_0)); }
	inline bool get_isPointerUpEvent_0() const { return ___isPointerUpEvent_0; }
	inline bool* get_address_of_isPointerUpEvent_0() { return &___isPointerUpEvent_0; }
	inline void set_isPointerUpEvent_0(bool value)
	{
		___isPointerUpEvent_0 = value;
	}

	inline static int32_t get_offset_of_isPointerDownEvent_1() { return static_cast<int32_t>(offsetof(TouchInputEvent_t172715690, ___isPointerDownEvent_1)); }
	inline bool get_isPointerDownEvent_1() const { return ___isPointerDownEvent_1; }
	inline bool* get_address_of_isPointerDownEvent_1() { return &___isPointerDownEvent_1; }
	inline void set_isPointerDownEvent_1(bool value)
	{
		___isPointerDownEvent_1 = value;
	}

	inline static int32_t get_offset_of_primaryActionIndex_2() { return static_cast<int32_t>(offsetof(TouchInputEvent_t172715690, ___primaryActionIndex_2)); }
	inline int32_t get_primaryActionIndex_2() const { return ___primaryActionIndex_2; }
	inline int32_t* get_address_of_primaryActionIndex_2() { return &___primaryActionIndex_2; }
	inline void set_primaryActionIndex_2(int32_t value)
	{
		___primaryActionIndex_2 = value;
	}

	inline static int32_t get_offset_of_primaryActionIdentifier_3() { return static_cast<int32_t>(offsetof(TouchInputEvent_t172715690, ___primaryActionIdentifier_3)); }
	inline int32_t get_primaryActionIdentifier_3() const { return ___primaryActionIdentifier_3; }
	inline int32_t* get_address_of_primaryActionIdentifier_3() { return &___primaryActionIdentifier_3; }
	inline void set_primaryActionIdentifier_3(int32_t value)
	{
		___primaryActionIdentifier_3 = value;
	}

	inline static int32_t get_offset_of_pointerEvents_4() { return static_cast<int32_t>(offsetof(TouchInputEvent_t172715690, ___pointerEvents_4)); }
	inline List_1_t2102936381 * get_pointerEvents_4() const { return ___pointerEvents_4; }
	inline List_1_t2102936381 ** get_address_of_pointerEvents_4() { return &___pointerEvents_4; }
	inline void set_pointerEvents_4(List_1_t2102936381 * value)
	{
		___pointerEvents_4 = value;
		Il2CppCodeGenWriteBarrier((&___pointerEvents_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Wrld.MapInput.Touch.TouchInputEvent
struct TouchInputEvent_t172715690_marshaled_pinvoke
{
	int32_t ___isPointerUpEvent_0;
	int32_t ___isPointerDownEvent_1;
	int32_t ___primaryActionIndex_2;
	int32_t ___primaryActionIdentifier_3;
	List_1_t2102936381 * ___pointerEvents_4;
};
// Native definition for COM marshalling of Wrld.MapInput.Touch.TouchInputEvent
struct TouchInputEvent_t172715690_marshaled_com
{
	int32_t ___isPointerUpEvent_0;
	int32_t ___isPointerDownEvent_1;
	int32_t ___primaryActionIndex_2;
	int32_t ___primaryActionIdentifier_3;
	List_1_t2102936381 * ___pointerEvents_4;
};
#endif // TOUCHINPUTEVENT_T172715690_H
#ifndef TOUCHINPUTPOINTEREVENT_T630861639_H
#define TOUCHINPUTPOINTEREVENT_T630861639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.Touch.TouchInputPointerEvent
struct  TouchInputPointerEvent_t630861639 
{
public:
	// System.Single Wrld.MapInput.Touch.TouchInputPointerEvent::x
	float ___x_0;
	// System.Single Wrld.MapInput.Touch.TouchInputPointerEvent::y
	float ___y_1;
	// System.Int32 Wrld.MapInput.Touch.TouchInputPointerEvent::pointerIdentity
	int32_t ___pointerIdentity_2;
	// System.Int32 Wrld.MapInput.Touch.TouchInputPointerEvent::pointerIndex
	int32_t ___pointerIndex_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(TouchInputPointerEvent_t630861639, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(TouchInputPointerEvent_t630861639, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_pointerIdentity_2() { return static_cast<int32_t>(offsetof(TouchInputPointerEvent_t630861639, ___pointerIdentity_2)); }
	inline int32_t get_pointerIdentity_2() const { return ___pointerIdentity_2; }
	inline int32_t* get_address_of_pointerIdentity_2() { return &___pointerIdentity_2; }
	inline void set_pointerIdentity_2(int32_t value)
	{
		___pointerIdentity_2 = value;
	}

	inline static int32_t get_offset_of_pointerIndex_3() { return static_cast<int32_t>(offsetof(TouchInputPointerEvent_t630861639, ___pointerIndex_3)); }
	inline int32_t get_pointerIndex_3() const { return ___pointerIndex_3; }
	inline int32_t* get_address_of_pointerIndex_3() { return &___pointerIndex_3; }
	inline void set_pointerIndex_3(int32_t value)
	{
		___pointerIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHINPUTPOINTEREVENT_T630861639_H
#ifndef LATLONG_T2936018554_H
#define LATLONG_T2936018554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Space.LatLong
struct  LatLong_t2936018554 
{
public:
	// System.Double Wrld.Space.LatLong::m_latitudeInDegrees
	double ___m_latitudeInDegrees_0;
	// System.Double Wrld.Space.LatLong::m_longitudeInDegrees
	double ___m_longitudeInDegrees_1;

public:
	inline static int32_t get_offset_of_m_latitudeInDegrees_0() { return static_cast<int32_t>(offsetof(LatLong_t2936018554, ___m_latitudeInDegrees_0)); }
	inline double get_m_latitudeInDegrees_0() const { return ___m_latitudeInDegrees_0; }
	inline double* get_address_of_m_latitudeInDegrees_0() { return &___m_latitudeInDegrees_0; }
	inline void set_m_latitudeInDegrees_0(double value)
	{
		___m_latitudeInDegrees_0 = value;
	}

	inline static int32_t get_offset_of_m_longitudeInDegrees_1() { return static_cast<int32_t>(offsetof(LatLong_t2936018554, ___m_longitudeInDegrees_1)); }
	inline double get_m_longitudeInDegrees_1() const { return ___m_longitudeInDegrees_1; }
	inline double* get_address_of_m_longitudeInDegrees_1() { return &___m_longitudeInDegrees_1; }
	inline void set_m_longitudeInDegrees_1(double value)
	{
		___m_longitudeInDegrees_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LATLONG_T2936018554_H
#ifndef INVALIDAPIKEYEXCEPTION_T175683051_H
#define INVALIDAPIKEYEXCEPTION_T175683051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Scripts.Utilities.InvalidApiKeyException
struct  InvalidApiKeyException_t175683051  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDAPIKEYEXCEPTION_T175683051_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef UINT64_T4134040092_H
#define UINT64_T4134040092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt64
struct  UInt64_t4134040092 
{
public:
	// System.UInt64 System.UInt64::m_value
	uint64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt64_t4134040092, ___m_value_0)); }
	inline uint64_t get_m_value_0() const { return ___m_value_0; }
	inline uint64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT64_T4134040092_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef DOUBLEVECTOR3_T761704365_H
#define DOUBLEVECTOR3_T761704365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Common.Maths.DoubleVector3
struct  DoubleVector3_t761704365 
{
public:
	// System.Double Wrld.Common.Maths.DoubleVector3::x
	double ___x_3;
	// System.Double Wrld.Common.Maths.DoubleVector3::y
	double ___y_4;
	// System.Double Wrld.Common.Maths.DoubleVector3::z
	double ___z_5;

public:
	inline static int32_t get_offset_of_x_3() { return static_cast<int32_t>(offsetof(DoubleVector3_t761704365, ___x_3)); }
	inline double get_x_3() const { return ___x_3; }
	inline double* get_address_of_x_3() { return &___x_3; }
	inline void set_x_3(double value)
	{
		___x_3 = value;
	}

	inline static int32_t get_offset_of_y_4() { return static_cast<int32_t>(offsetof(DoubleVector3_t761704365, ___y_4)); }
	inline double get_y_4() const { return ___y_4; }
	inline double* get_address_of_y_4() { return &___y_4; }
	inline void set_y_4(double value)
	{
		___y_4 = value;
	}

	inline static int32_t get_offset_of_z_5() { return static_cast<int32_t>(offsetof(DoubleVector3_t761704365, ___z_5)); }
	inline double get_z_5() const { return ___z_5; }
	inline double* get_address_of_z_5() { return &___z_5; }
	inline void set_z_5(double value)
	{
		___z_5 = value;
	}
};

struct DoubleVector3_t761704365_StaticFields
{
public:
	// Wrld.Common.Maths.DoubleVector3 Wrld.Common.Maths.DoubleVector3::zero
	DoubleVector3_t761704365  ___zero_1;
	// Wrld.Common.Maths.DoubleVector3 Wrld.Common.Maths.DoubleVector3::one
	DoubleVector3_t761704365  ___one_2;

public:
	inline static int32_t get_offset_of_zero_1() { return static_cast<int32_t>(offsetof(DoubleVector3_t761704365_StaticFields, ___zero_1)); }
	inline DoubleVector3_t761704365  get_zero_1() const { return ___zero_1; }
	inline DoubleVector3_t761704365 * get_address_of_zero_1() { return &___zero_1; }
	inline void set_zero_1(DoubleVector3_t761704365  value)
	{
		___zero_1 = value;
	}

	inline static int32_t get_offset_of_one_2() { return static_cast<int32_t>(offsetof(DoubleVector3_t761704365_StaticFields, ___one_2)); }
	inline DoubleVector3_t761704365  get_one_2() const { return ___one_2; }
	inline DoubleVector3_t761704365 * get_address_of_one_2() { return &___one_2; }
	inline void set_one_2(DoubleVector3_t761704365  value)
	{
		___one_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLEVECTOR3_T761704365_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef U24ARRAYTYPEU3D208_T3449392908_H
#define U24ARRAYTYPEU3D208_T3449392908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=208
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D208_t3449392908 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D208_t3449392908__padding[208];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D208_T3449392908_H
#ifndef APPLYTEXTURETOMATERIALREQUEST_T638715235_H
#define APPLYTEXTURETOMATERIALREQUEST_T638715235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Materials.MaterialRepository/ApplyTextureToMaterialRequest
struct  ApplyTextureToMaterialRequest_t638715235 
{
public:
	union
	{
		struct
		{
			// System.String Wrld.Materials.MaterialRepository/ApplyTextureToMaterialRequest::<MaterialName>k__BackingField
			String_t* ___U3CMaterialNameU3Ek__BackingField_0;
			// System.UInt32 Wrld.Materials.MaterialRepository/ApplyTextureToMaterialRequest::<TextureID>k__BackingField
			uint32_t ___U3CTextureIDU3Ek__BackingField_1;
		};
		uint8_t ApplyTextureToMaterialRequest_t638715235__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CMaterialNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ApplyTextureToMaterialRequest_t638715235, ___U3CMaterialNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CMaterialNameU3Ek__BackingField_0() const { return ___U3CMaterialNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CMaterialNameU3Ek__BackingField_0() { return &___U3CMaterialNameU3Ek__BackingField_0; }
	inline void set_U3CMaterialNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CMaterialNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMaterialNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CTextureIDU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ApplyTextureToMaterialRequest_t638715235, ___U3CTextureIDU3Ek__BackingField_1)); }
	inline uint32_t get_U3CTextureIDU3Ek__BackingField_1() const { return ___U3CTextureIDU3Ek__BackingField_1; }
	inline uint32_t* get_address_of_U3CTextureIDU3Ek__BackingField_1() { return &___U3CTextureIDU3Ek__BackingField_1; }
	inline void set_U3CTextureIDU3Ek__BackingField_1(uint32_t value)
	{
		___U3CTextureIDU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Wrld.Materials.MaterialRepository/ApplyTextureToMaterialRequest
struct ApplyTextureToMaterialRequest_t638715235_marshaled_pinvoke
{
	union
	{
		struct
		{
			char* ___U3CMaterialNameU3Ek__BackingField_0;
			uint32_t ___U3CTextureIDU3Ek__BackingField_1;
		};
		uint8_t ApplyTextureToMaterialRequest_t638715235__padding[1];
	};
};
// Native definition for COM marshalling of Wrld.Materials.MaterialRepository/ApplyTextureToMaterialRequest
struct ApplyTextureToMaterialRequest_t638715235_marshaled_com
{
	union
	{
		struct
		{
			Il2CppChar* ___U3CMaterialNameU3Ek__BackingField_0;
			uint32_t ___U3CTextureIDU3Ek__BackingField_1;
		};
		uint8_t ApplyTextureToMaterialRequest_t638715235__padding[1];
	};
};
#endif // APPLYTEXTURETOMATERIALREQUEST_T638715235_H
#ifndef U24ARRAYTYPEU3D24_T2467506693_H
#define U24ARRAYTYPEU3D24_T2467506693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=24
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D24_t2467506693 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D24_t2467506693__padding[24];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D24_T2467506693_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255370_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255370  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255370_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-8E7629AD5AF686202B8CB7C014505C432FFE31E6
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0;
	// <PrivateImplementationDetails>/$ArrayType=208 <PrivateImplementationDetails>::$field-811EB71438479767E10832E824E7D58DCDC0FCB8
	U24ArrayTypeU3D208_t3449392908  ___U24fieldU2D811EB71438479767E10832E824E7D58DCDC0FCB8_1;

public:
	inline static int32_t get_offset_of_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255370_StaticFields, ___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0() const { return ___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0() { return &___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0; }
	inline void set_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D811EB71438479767E10832E824E7D58DCDC0FCB8_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255370_StaticFields, ___U24fieldU2D811EB71438479767E10832E824E7D58DCDC0FCB8_1)); }
	inline U24ArrayTypeU3D208_t3449392908  get_U24fieldU2D811EB71438479767E10832E824E7D58DCDC0FCB8_1() const { return ___U24fieldU2D811EB71438479767E10832E824E7D58DCDC0FCB8_1; }
	inline U24ArrayTypeU3D208_t3449392908 * get_address_of_U24fieldU2D811EB71438479767E10832E824E7D58DCDC0FCB8_1() { return &___U24fieldU2D811EB71438479767E10832E824E7D58DCDC0FCB8_1; }
	inline void set_U24fieldU2D811EB71438479767E10832E824E7D58DCDC0FCB8_1(U24ArrayTypeU3D208_t3449392908  value)
	{
		___U24fieldU2D811EB71438479767E10832E824E7D58DCDC0FCB8_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255370_H
#ifndef TEXTUREFORMAT_T2701165832_H
#define TEXTUREFORMAT_T2701165832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t2701165832 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureFormat_t2701165832, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T2701165832_H
#ifndef GAMEOBJECTRECORD_T2871914821_H
#define GAMEOBJECTRECORD_T2871914821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Streaming.GameObjectRecord
struct  GameObjectRecord_t2871914821  : public RuntimeObject
{
public:
	// Wrld.Common.Maths.DoubleVector3 Wrld.Streaming.GameObjectRecord::<OriginECEF>k__BackingField
	DoubleVector3_t761704365  ___U3COriginECEFU3Ek__BackingField_0;
	// UnityEngine.GameObject[] Wrld.Streaming.GameObjectRecord::<GameObjects>k__BackingField
	GameObjectU5BU5D_t3328599146* ___U3CGameObjectsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3COriginECEFU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GameObjectRecord_t2871914821, ___U3COriginECEFU3Ek__BackingField_0)); }
	inline DoubleVector3_t761704365  get_U3COriginECEFU3Ek__BackingField_0() const { return ___U3COriginECEFU3Ek__BackingField_0; }
	inline DoubleVector3_t761704365 * get_address_of_U3COriginECEFU3Ek__BackingField_0() { return &___U3COriginECEFU3Ek__BackingField_0; }
	inline void set_U3COriginECEFU3Ek__BackingField_0(DoubleVector3_t761704365  value)
	{
		___U3COriginECEFU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CGameObjectsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GameObjectRecord_t2871914821, ___U3CGameObjectsU3Ek__BackingField_1)); }
	inline GameObjectU5BU5D_t3328599146* get_U3CGameObjectsU3Ek__BackingField_1() const { return ___U3CGameObjectsU3Ek__BackingField_1; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_U3CGameObjectsU3Ek__BackingField_1() { return &___U3CGameObjectsU3Ek__BackingField_1; }
	inline void set_U3CGameObjectsU3Ek__BackingField_1(GameObjectU5BU5D_t3328599146* value)
	{
		___U3CGameObjectsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameObjectsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTRECORD_T2871914821_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef COLLISIONSTREAMINGTYPE_T2118098865_H
#define COLLISIONSTREAMINGTYPE_T2118098865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Streaming.CollisionStreamingType
struct  CollisionStreamingType_t2118098865 
{
public:
	// System.Int32 Wrld.Streaming.CollisionStreamingType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CollisionStreamingType_t2118098865, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISIONSTREAMINGTYPE_T2118098865_H
#ifndef HIGHLIGHTINTEROP_T2055068794_H
#define HIGHLIGHTINTEROP_T2055068794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Resources.Buildings.HighlightInterop
struct  HighlightInterop_t2055068794 
{
public:
	// System.Double Wrld.Resources.Buildings.HighlightInterop::OriginEcefX
	double ___OriginEcefX_0;
	// System.Double Wrld.Resources.Buildings.HighlightInterop::OriginEcefY
	double ___OriginEcefY_1;
	// System.Double Wrld.Resources.Buildings.HighlightInterop::OriginEcefZ
	double ___OriginEcefZ_2;
	// System.Int32 Wrld.Resources.Buildings.HighlightInterop::VertexCount
	int32_t ___VertexCount_3;
	// System.Int32 Wrld.Resources.Buildings.HighlightInterop::IndexCount
	int32_t ___IndexCount_4;
	// System.IntPtr Wrld.Resources.Buildings.HighlightInterop::VertexPositions
	intptr_t ___VertexPositions_5;
	// System.IntPtr Wrld.Resources.Buildings.HighlightInterop::Indices
	intptr_t ___Indices_6;

public:
	inline static int32_t get_offset_of_OriginEcefX_0() { return static_cast<int32_t>(offsetof(HighlightInterop_t2055068794, ___OriginEcefX_0)); }
	inline double get_OriginEcefX_0() const { return ___OriginEcefX_0; }
	inline double* get_address_of_OriginEcefX_0() { return &___OriginEcefX_0; }
	inline void set_OriginEcefX_0(double value)
	{
		___OriginEcefX_0 = value;
	}

	inline static int32_t get_offset_of_OriginEcefY_1() { return static_cast<int32_t>(offsetof(HighlightInterop_t2055068794, ___OriginEcefY_1)); }
	inline double get_OriginEcefY_1() const { return ___OriginEcefY_1; }
	inline double* get_address_of_OriginEcefY_1() { return &___OriginEcefY_1; }
	inline void set_OriginEcefY_1(double value)
	{
		___OriginEcefY_1 = value;
	}

	inline static int32_t get_offset_of_OriginEcefZ_2() { return static_cast<int32_t>(offsetof(HighlightInterop_t2055068794, ___OriginEcefZ_2)); }
	inline double get_OriginEcefZ_2() const { return ___OriginEcefZ_2; }
	inline double* get_address_of_OriginEcefZ_2() { return &___OriginEcefZ_2; }
	inline void set_OriginEcefZ_2(double value)
	{
		___OriginEcefZ_2 = value;
	}

	inline static int32_t get_offset_of_VertexCount_3() { return static_cast<int32_t>(offsetof(HighlightInterop_t2055068794, ___VertexCount_3)); }
	inline int32_t get_VertexCount_3() const { return ___VertexCount_3; }
	inline int32_t* get_address_of_VertexCount_3() { return &___VertexCount_3; }
	inline void set_VertexCount_3(int32_t value)
	{
		___VertexCount_3 = value;
	}

	inline static int32_t get_offset_of_IndexCount_4() { return static_cast<int32_t>(offsetof(HighlightInterop_t2055068794, ___IndexCount_4)); }
	inline int32_t get_IndexCount_4() const { return ___IndexCount_4; }
	inline int32_t* get_address_of_IndexCount_4() { return &___IndexCount_4; }
	inline void set_IndexCount_4(int32_t value)
	{
		___IndexCount_4 = value;
	}

	inline static int32_t get_offset_of_VertexPositions_5() { return static_cast<int32_t>(offsetof(HighlightInterop_t2055068794, ___VertexPositions_5)); }
	inline intptr_t get_VertexPositions_5() const { return ___VertexPositions_5; }
	inline intptr_t* get_address_of_VertexPositions_5() { return &___VertexPositions_5; }
	inline void set_VertexPositions_5(intptr_t value)
	{
		___VertexPositions_5 = value;
	}

	inline static int32_t get_offset_of_Indices_6() { return static_cast<int32_t>(offsetof(HighlightInterop_t2055068794, ___Indices_6)); }
	inline intptr_t get_Indices_6() const { return ___Indices_6; }
	inline intptr_t* get_address_of_Indices_6() { return &___Indices_6; }
	inline void set_Indices_6(intptr_t value)
	{
		___Indices_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIGHLIGHTINTEROP_T2055068794_H
#ifndef BUILDINGINTEROP_T2102364785_H
#define BUILDINGINTEROP_T2102364785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Resources.Buildings.BuildingInterop
struct  BuildingInterop_t2102364785 
{
public:
	// System.Double Wrld.Resources.Buildings.BuildingInterop::BaseAltitude
	double ___BaseAltitude_0;
	// System.Double Wrld.Resources.Buildings.BuildingInterop::TopAltitude
	double ___TopAltitude_1;
	// System.Double Wrld.Resources.Buildings.BuildingInterop::CentroidLatitude
	double ___CentroidLatitude_2;
	// System.Double Wrld.Resources.Buildings.BuildingInterop::CentroidLongitude
	double ___CentroidLongitude_3;
	// System.IntPtr Wrld.Resources.Buildings.BuildingInterop::StringIdPtr
	intptr_t ___StringIdPtr_4;

public:
	inline static int32_t get_offset_of_BaseAltitude_0() { return static_cast<int32_t>(offsetof(BuildingInterop_t2102364785, ___BaseAltitude_0)); }
	inline double get_BaseAltitude_0() const { return ___BaseAltitude_0; }
	inline double* get_address_of_BaseAltitude_0() { return &___BaseAltitude_0; }
	inline void set_BaseAltitude_0(double value)
	{
		___BaseAltitude_0 = value;
	}

	inline static int32_t get_offset_of_TopAltitude_1() { return static_cast<int32_t>(offsetof(BuildingInterop_t2102364785, ___TopAltitude_1)); }
	inline double get_TopAltitude_1() const { return ___TopAltitude_1; }
	inline double* get_address_of_TopAltitude_1() { return &___TopAltitude_1; }
	inline void set_TopAltitude_1(double value)
	{
		___TopAltitude_1 = value;
	}

	inline static int32_t get_offset_of_CentroidLatitude_2() { return static_cast<int32_t>(offsetof(BuildingInterop_t2102364785, ___CentroidLatitude_2)); }
	inline double get_CentroidLatitude_2() const { return ___CentroidLatitude_2; }
	inline double* get_address_of_CentroidLatitude_2() { return &___CentroidLatitude_2; }
	inline void set_CentroidLatitude_2(double value)
	{
		___CentroidLatitude_2 = value;
	}

	inline static int32_t get_offset_of_CentroidLongitude_3() { return static_cast<int32_t>(offsetof(BuildingInterop_t2102364785, ___CentroidLongitude_3)); }
	inline double get_CentroidLongitude_3() const { return ___CentroidLongitude_3; }
	inline double* get_address_of_CentroidLongitude_3() { return &___CentroidLongitude_3; }
	inline void set_CentroidLongitude_3(double value)
	{
		___CentroidLongitude_3 = value;
	}

	inline static int32_t get_offset_of_StringIdPtr_4() { return static_cast<int32_t>(offsetof(BuildingInterop_t2102364785, ___StringIdPtr_4)); }
	inline intptr_t get_StringIdPtr_4() const { return ___StringIdPtr_4; }
	inline intptr_t* get_address_of_StringIdPtr_4() { return &___StringIdPtr_4; }
	inline void set_StringIdPtr_4(intptr_t value)
	{
		___StringIdPtr_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDINGINTEROP_T2102364785_H
#ifndef BUILDING_T1491635829_H
#define BUILDING_T1491635829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Resources.Buildings.Building
struct  Building_t1491635829 
{
public:
	// System.String Wrld.Resources.Buildings.Building::BuildingId
	String_t* ___BuildingId_0;
	// System.Double Wrld.Resources.Buildings.Building::BaseAltitude
	double ___BaseAltitude_1;
	// System.Double Wrld.Resources.Buildings.Building::TopAltitude
	double ___TopAltitude_2;
	// Wrld.Space.LatLong Wrld.Resources.Buildings.Building::Centroid
	LatLong_t2936018554  ___Centroid_3;

public:
	inline static int32_t get_offset_of_BuildingId_0() { return static_cast<int32_t>(offsetof(Building_t1491635829, ___BuildingId_0)); }
	inline String_t* get_BuildingId_0() const { return ___BuildingId_0; }
	inline String_t** get_address_of_BuildingId_0() { return &___BuildingId_0; }
	inline void set_BuildingId_0(String_t* value)
	{
		___BuildingId_0 = value;
		Il2CppCodeGenWriteBarrier((&___BuildingId_0), value);
	}

	inline static int32_t get_offset_of_BaseAltitude_1() { return static_cast<int32_t>(offsetof(Building_t1491635829, ___BaseAltitude_1)); }
	inline double get_BaseAltitude_1() const { return ___BaseAltitude_1; }
	inline double* get_address_of_BaseAltitude_1() { return &___BaseAltitude_1; }
	inline void set_BaseAltitude_1(double value)
	{
		___BaseAltitude_1 = value;
	}

	inline static int32_t get_offset_of_TopAltitude_2() { return static_cast<int32_t>(offsetof(Building_t1491635829, ___TopAltitude_2)); }
	inline double get_TopAltitude_2() const { return ___TopAltitude_2; }
	inline double* get_address_of_TopAltitude_2() { return &___TopAltitude_2; }
	inline void set_TopAltitude_2(double value)
	{
		___TopAltitude_2 = value;
	}

	inline static int32_t get_offset_of_Centroid_3() { return static_cast<int32_t>(offsetof(Building_t1491635829, ___Centroid_3)); }
	inline LatLong_t2936018554  get_Centroid_3() const { return ___Centroid_3; }
	inline LatLong_t2936018554 * get_address_of_Centroid_3() { return &___Centroid_3; }
	inline void set_Centroid_3(LatLong_t2936018554  value)
	{
		___Centroid_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Wrld.Resources.Buildings.Building
struct Building_t1491635829_marshaled_pinvoke
{
	char* ___BuildingId_0;
	double ___BaseAltitude_1;
	double ___TopAltitude_2;
	LatLong_t2936018554  ___Centroid_3;
};
// Native definition for COM marshalling of Wrld.Resources.Buildings.Building
struct Building_t1491635829_marshaled_com
{
	Il2CppChar* ___BuildingId_0;
	double ___BaseAltitude_1;
	double ___TopAltitude_2;
	LatLong_t2936018554  ___Centroid_3;
};
#endif // BUILDING_T1491635829_H
#ifndef NATIVEPLUGINRUNNER_T3528041536_H
#define NATIVEPLUGINRUNNER_T3528041536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.NativePluginRunner
struct  NativePluginRunner_t3528041536  : public RuntimeObject
{
public:
	// Wrld.Materials.MaterialRepository Wrld.NativePluginRunner::m_materialRepository
	MaterialRepository_t701520604 * ___m_materialRepository_2;
	// Wrld.Materials.TextureLoadHandler Wrld.NativePluginRunner::m_textureLoadHandler
	TextureLoadHandler_t2080734353 * ___m_textureLoadHandler_3;
	// Wrld.MapGameObjectScene Wrld.NativePluginRunner::m_mapGameObjectScene
	MapGameObjectScene_t128928738 * ___m_mapGameObjectScene_4;
	// Wrld.StreamingUpdater Wrld.NativePluginRunner::m_streamingUpdater
	StreamingUpdater_t530401876 * ___m_streamingUpdater_5;
	// System.Boolean Wrld.NativePluginRunner::m_isRunning
	bool ___m_isRunning_6;

public:
	inline static int32_t get_offset_of_m_materialRepository_2() { return static_cast<int32_t>(offsetof(NativePluginRunner_t3528041536, ___m_materialRepository_2)); }
	inline MaterialRepository_t701520604 * get_m_materialRepository_2() const { return ___m_materialRepository_2; }
	inline MaterialRepository_t701520604 ** get_address_of_m_materialRepository_2() { return &___m_materialRepository_2; }
	inline void set_m_materialRepository_2(MaterialRepository_t701520604 * value)
	{
		___m_materialRepository_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialRepository_2), value);
	}

	inline static int32_t get_offset_of_m_textureLoadHandler_3() { return static_cast<int32_t>(offsetof(NativePluginRunner_t3528041536, ___m_textureLoadHandler_3)); }
	inline TextureLoadHandler_t2080734353 * get_m_textureLoadHandler_3() const { return ___m_textureLoadHandler_3; }
	inline TextureLoadHandler_t2080734353 ** get_address_of_m_textureLoadHandler_3() { return &___m_textureLoadHandler_3; }
	inline void set_m_textureLoadHandler_3(TextureLoadHandler_t2080734353 * value)
	{
		___m_textureLoadHandler_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_textureLoadHandler_3), value);
	}

	inline static int32_t get_offset_of_m_mapGameObjectScene_4() { return static_cast<int32_t>(offsetof(NativePluginRunner_t3528041536, ___m_mapGameObjectScene_4)); }
	inline MapGameObjectScene_t128928738 * get_m_mapGameObjectScene_4() const { return ___m_mapGameObjectScene_4; }
	inline MapGameObjectScene_t128928738 ** get_address_of_m_mapGameObjectScene_4() { return &___m_mapGameObjectScene_4; }
	inline void set_m_mapGameObjectScene_4(MapGameObjectScene_t128928738 * value)
	{
		___m_mapGameObjectScene_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_mapGameObjectScene_4), value);
	}

	inline static int32_t get_offset_of_m_streamingUpdater_5() { return static_cast<int32_t>(offsetof(NativePluginRunner_t3528041536, ___m_streamingUpdater_5)); }
	inline StreamingUpdater_t530401876 * get_m_streamingUpdater_5() const { return ___m_streamingUpdater_5; }
	inline StreamingUpdater_t530401876 ** get_address_of_m_streamingUpdater_5() { return &___m_streamingUpdater_5; }
	inline void set_m_streamingUpdater_5(StreamingUpdater_t530401876 * value)
	{
		___m_streamingUpdater_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_streamingUpdater_5), value);
	}

	inline static int32_t get_offset_of_m_isRunning_6() { return static_cast<int32_t>(offsetof(NativePluginRunner_t3528041536, ___m_isRunning_6)); }
	inline bool get_m_isRunning_6() const { return ___m_isRunning_6; }
	inline bool* get_address_of_m_isRunning_6() { return &___m_isRunning_6; }
	inline void set_m_isRunning_6(bool value)
	{
		___m_isRunning_6 = value;
	}
};

struct NativePluginRunner_t3528041536_StaticFields
{
public:
	// System.IntPtr Wrld.NativePluginRunner::API
	intptr_t ___API_1;

public:
	inline static int32_t get_offset_of_API_1() { return static_cast<int32_t>(offsetof(NativePluginRunner_t3528041536_StaticFields, ___API_1)); }
	inline intptr_t get_API_1() const { return ___API_1; }
	inline intptr_t* get_address_of_API_1() { return &___API_1; }
	inline void set_API_1(intptr_t value)
	{
		___API_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEPLUGINRUNNER_T3528041536_H
#ifndef PREPAREDMESHRECORD_T446409467_H
#define PREPAREDMESHRECORD_T446409467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Meshes.PreparedMeshRecord
struct  PreparedMeshRecord_t446409467  : public RuntimeObject
{
public:
	// Wrld.Meshes.PreparedMesh[] Wrld.Meshes.PreparedMeshRecord::<Meshes>k__BackingField
	PreparedMeshU5BU5D_t3261552485* ___U3CMeshesU3Ek__BackingField_0;
	// Wrld.Common.Maths.DoubleVector3 Wrld.Meshes.PreparedMeshRecord::<OriginECEF>k__BackingField
	DoubleVector3_t761704365  ___U3COriginECEFU3Ek__BackingField_1;
	// System.String Wrld.Meshes.PreparedMeshRecord::<MaterialName>k__BackingField
	String_t* ___U3CMaterialNameU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CMeshesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PreparedMeshRecord_t446409467, ___U3CMeshesU3Ek__BackingField_0)); }
	inline PreparedMeshU5BU5D_t3261552485* get_U3CMeshesU3Ek__BackingField_0() const { return ___U3CMeshesU3Ek__BackingField_0; }
	inline PreparedMeshU5BU5D_t3261552485** get_address_of_U3CMeshesU3Ek__BackingField_0() { return &___U3CMeshesU3Ek__BackingField_0; }
	inline void set_U3CMeshesU3Ek__BackingField_0(PreparedMeshU5BU5D_t3261552485* value)
	{
		___U3CMeshesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMeshesU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3COriginECEFU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PreparedMeshRecord_t446409467, ___U3COriginECEFU3Ek__BackingField_1)); }
	inline DoubleVector3_t761704365  get_U3COriginECEFU3Ek__BackingField_1() const { return ___U3COriginECEFU3Ek__BackingField_1; }
	inline DoubleVector3_t761704365 * get_address_of_U3COriginECEFU3Ek__BackingField_1() { return &___U3COriginECEFU3Ek__BackingField_1; }
	inline void set_U3COriginECEFU3Ek__BackingField_1(DoubleVector3_t761704365  value)
	{
		___U3COriginECEFU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CMaterialNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PreparedMeshRecord_t446409467, ___U3CMaterialNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CMaterialNameU3Ek__BackingField_2() const { return ___U3CMaterialNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CMaterialNameU3Ek__BackingField_2() { return &___U3CMaterialNameU3Ek__BackingField_2; }
	inline void set_U3CMaterialNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CMaterialNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMaterialNameU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREPAREDMESHRECORD_T446409467_H
#ifndef MARSHALLEDMESH_T2163999146_H
#define MARSHALLEDMESH_T2163999146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Meshes.MeshUploader/MarshalledMesh
struct  MarshalledMesh_t2163999146 
{
public:
	// System.IntPtr Wrld.Meshes.MeshUploader/MarshalledMesh::vertices
	intptr_t ___vertices_0;
	// System.Int32 Wrld.Meshes.MeshUploader/MarshalledMesh::vertexCount
	int32_t ___vertexCount_1;
	// System.IntPtr Wrld.Meshes.MeshUploader/MarshalledMesh::uvs
	intptr_t ___uvs_2;
	// System.Int32 Wrld.Meshes.MeshUploader/MarshalledMesh::uvCount
	int32_t ___uvCount_3;
	// System.IntPtr Wrld.Meshes.MeshUploader/MarshalledMesh::uv2s
	intptr_t ___uv2s_4;
	// System.Int32 Wrld.Meshes.MeshUploader/MarshalledMesh::uv2Count
	int32_t ___uv2Count_5;
	// System.IntPtr Wrld.Meshes.MeshUploader/MarshalledMesh::indices
	intptr_t ___indices_6;
	// System.Int32 Wrld.Meshes.MeshUploader/MarshalledMesh::indexCount
	int32_t ___indexCount_7;
	// System.IntPtr Wrld.Meshes.MeshUploader/MarshalledMesh::originEcef
	intptr_t ___originEcef_8;
	// System.IntPtr Wrld.Meshes.MeshUploader/MarshalledMesh::name
	intptr_t ___name_9;
	// System.IntPtr Wrld.Meshes.MeshUploader/MarshalledMesh::unpackedMeshHandle
	intptr_t ___unpackedMeshHandle_10;

public:
	inline static int32_t get_offset_of_vertices_0() { return static_cast<int32_t>(offsetof(MarshalledMesh_t2163999146, ___vertices_0)); }
	inline intptr_t get_vertices_0() const { return ___vertices_0; }
	inline intptr_t* get_address_of_vertices_0() { return &___vertices_0; }
	inline void set_vertices_0(intptr_t value)
	{
		___vertices_0 = value;
	}

	inline static int32_t get_offset_of_vertexCount_1() { return static_cast<int32_t>(offsetof(MarshalledMesh_t2163999146, ___vertexCount_1)); }
	inline int32_t get_vertexCount_1() const { return ___vertexCount_1; }
	inline int32_t* get_address_of_vertexCount_1() { return &___vertexCount_1; }
	inline void set_vertexCount_1(int32_t value)
	{
		___vertexCount_1 = value;
	}

	inline static int32_t get_offset_of_uvs_2() { return static_cast<int32_t>(offsetof(MarshalledMesh_t2163999146, ___uvs_2)); }
	inline intptr_t get_uvs_2() const { return ___uvs_2; }
	inline intptr_t* get_address_of_uvs_2() { return &___uvs_2; }
	inline void set_uvs_2(intptr_t value)
	{
		___uvs_2 = value;
	}

	inline static int32_t get_offset_of_uvCount_3() { return static_cast<int32_t>(offsetof(MarshalledMesh_t2163999146, ___uvCount_3)); }
	inline int32_t get_uvCount_3() const { return ___uvCount_3; }
	inline int32_t* get_address_of_uvCount_3() { return &___uvCount_3; }
	inline void set_uvCount_3(int32_t value)
	{
		___uvCount_3 = value;
	}

	inline static int32_t get_offset_of_uv2s_4() { return static_cast<int32_t>(offsetof(MarshalledMesh_t2163999146, ___uv2s_4)); }
	inline intptr_t get_uv2s_4() const { return ___uv2s_4; }
	inline intptr_t* get_address_of_uv2s_4() { return &___uv2s_4; }
	inline void set_uv2s_4(intptr_t value)
	{
		___uv2s_4 = value;
	}

	inline static int32_t get_offset_of_uv2Count_5() { return static_cast<int32_t>(offsetof(MarshalledMesh_t2163999146, ___uv2Count_5)); }
	inline int32_t get_uv2Count_5() const { return ___uv2Count_5; }
	inline int32_t* get_address_of_uv2Count_5() { return &___uv2Count_5; }
	inline void set_uv2Count_5(int32_t value)
	{
		___uv2Count_5 = value;
	}

	inline static int32_t get_offset_of_indices_6() { return static_cast<int32_t>(offsetof(MarshalledMesh_t2163999146, ___indices_6)); }
	inline intptr_t get_indices_6() const { return ___indices_6; }
	inline intptr_t* get_address_of_indices_6() { return &___indices_6; }
	inline void set_indices_6(intptr_t value)
	{
		___indices_6 = value;
	}

	inline static int32_t get_offset_of_indexCount_7() { return static_cast<int32_t>(offsetof(MarshalledMesh_t2163999146, ___indexCount_7)); }
	inline int32_t get_indexCount_7() const { return ___indexCount_7; }
	inline int32_t* get_address_of_indexCount_7() { return &___indexCount_7; }
	inline void set_indexCount_7(int32_t value)
	{
		___indexCount_7 = value;
	}

	inline static int32_t get_offset_of_originEcef_8() { return static_cast<int32_t>(offsetof(MarshalledMesh_t2163999146, ___originEcef_8)); }
	inline intptr_t get_originEcef_8() const { return ___originEcef_8; }
	inline intptr_t* get_address_of_originEcef_8() { return &___originEcef_8; }
	inline void set_originEcef_8(intptr_t value)
	{
		___originEcef_8 = value;
	}

	inline static int32_t get_offset_of_name_9() { return static_cast<int32_t>(offsetof(MarshalledMesh_t2163999146, ___name_9)); }
	inline intptr_t get_name_9() const { return ___name_9; }
	inline intptr_t* get_address_of_name_9() { return &___name_9; }
	inline void set_name_9(intptr_t value)
	{
		___name_9 = value;
	}

	inline static int32_t get_offset_of_unpackedMeshHandle_10() { return static_cast<int32_t>(offsetof(MarshalledMesh_t2163999146, ___unpackedMeshHandle_10)); }
	inline intptr_t get_unpackedMeshHandle_10() const { return ___unpackedMeshHandle_10; }
	inline intptr_t* get_address_of_unpackedMeshHandle_10() { return &___unpackedMeshHandle_10; }
	inline void set_unpackedMeshHandle_10(intptr_t value)
	{
		___unpackedMeshHandle_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALLEDMESH_T2163999146_H
#ifndef ECEFTANGENTBASIS_T256737685_H
#define ECEFTANGENTBASIS_T256737685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Common.Maths.EcefTangentBasis
struct  EcefTangentBasis_t256737685  : public RuntimeObject
{
public:
	// Wrld.Common.Maths.DoubleVector3 Wrld.Common.Maths.EcefTangentBasis::m_pointEcef
	DoubleVector3_t761704365  ___m_pointEcef_0;
	// UnityEngine.Vector3 Wrld.Common.Maths.EcefTangentBasis::m_basisRight
	Vector3_t3722313464  ___m_basisRight_1;
	// UnityEngine.Vector3 Wrld.Common.Maths.EcefTangentBasis::m_basisUp
	Vector3_t3722313464  ___m_basisUp_2;
	// UnityEngine.Vector3 Wrld.Common.Maths.EcefTangentBasis::m_basisForward
	Vector3_t3722313464  ___m_basisForward_3;

public:
	inline static int32_t get_offset_of_m_pointEcef_0() { return static_cast<int32_t>(offsetof(EcefTangentBasis_t256737685, ___m_pointEcef_0)); }
	inline DoubleVector3_t761704365  get_m_pointEcef_0() const { return ___m_pointEcef_0; }
	inline DoubleVector3_t761704365 * get_address_of_m_pointEcef_0() { return &___m_pointEcef_0; }
	inline void set_m_pointEcef_0(DoubleVector3_t761704365  value)
	{
		___m_pointEcef_0 = value;
	}

	inline static int32_t get_offset_of_m_basisRight_1() { return static_cast<int32_t>(offsetof(EcefTangentBasis_t256737685, ___m_basisRight_1)); }
	inline Vector3_t3722313464  get_m_basisRight_1() const { return ___m_basisRight_1; }
	inline Vector3_t3722313464 * get_address_of_m_basisRight_1() { return &___m_basisRight_1; }
	inline void set_m_basisRight_1(Vector3_t3722313464  value)
	{
		___m_basisRight_1 = value;
	}

	inline static int32_t get_offset_of_m_basisUp_2() { return static_cast<int32_t>(offsetof(EcefTangentBasis_t256737685, ___m_basisUp_2)); }
	inline Vector3_t3722313464  get_m_basisUp_2() const { return ___m_basisUp_2; }
	inline Vector3_t3722313464 * get_address_of_m_basisUp_2() { return &___m_basisUp_2; }
	inline void set_m_basisUp_2(Vector3_t3722313464  value)
	{
		___m_basisUp_2 = value;
	}

	inline static int32_t get_offset_of_m_basisForward_3() { return static_cast<int32_t>(offsetof(EcefTangentBasis_t256737685, ___m_basisForward_3)); }
	inline Vector3_t3722313464  get_m_basisForward_3() const { return ___m_basisForward_3; }
	inline Vector3_t3722313464 * get_address_of_m_basisForward_3() { return &___m_basisForward_3; }
	inline void set_m_basisForward_3(Vector3_t3722313464  value)
	{
		___m_basisForward_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECEFTANGENTBASIS_T256737685_H
#ifndef MARSHALLEDTEXTUREBUFFER_T4250577980_H
#define MARSHALLEDTEXTUREBUFFER_T4250577980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Materials.TextureLoadHandler/MarshalledTextureBuffer
struct  MarshalledTextureBuffer_t4250577980 
{
public:
	// System.IntPtr Wrld.Materials.TextureLoadHandler/MarshalledTextureBuffer::data
	intptr_t ___data_0;
	// System.Int32 Wrld.Materials.TextureLoadHandler/MarshalledTextureBuffer::sizeInBytes
	int32_t ___sizeInBytes_1;
	// System.Int32 Wrld.Materials.TextureLoadHandler/MarshalledTextureBuffer::width
	int32_t ___width_2;
	// System.Int32 Wrld.Materials.TextureLoadHandler/MarshalledTextureBuffer::height
	int32_t ___height_3;
	// System.Int32 Wrld.Materials.TextureLoadHandler/MarshalledTextureBuffer::format
	int32_t ___format_4;
	// System.Byte Wrld.Materials.TextureLoadHandler/MarshalledTextureBuffer::_mipMaps
	uint8_t ____mipMaps_5;
	// System.IntPtr Wrld.Materials.TextureLoadHandler/MarshalledTextureBuffer::textureBufferHandle
	intptr_t ___textureBufferHandle_6;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(MarshalledTextureBuffer_t4250577980, ___data_0)); }
	inline intptr_t get_data_0() const { return ___data_0; }
	inline intptr_t* get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(intptr_t value)
	{
		___data_0 = value;
	}

	inline static int32_t get_offset_of_sizeInBytes_1() { return static_cast<int32_t>(offsetof(MarshalledTextureBuffer_t4250577980, ___sizeInBytes_1)); }
	inline int32_t get_sizeInBytes_1() const { return ___sizeInBytes_1; }
	inline int32_t* get_address_of_sizeInBytes_1() { return &___sizeInBytes_1; }
	inline void set_sizeInBytes_1(int32_t value)
	{
		___sizeInBytes_1 = value;
	}

	inline static int32_t get_offset_of_width_2() { return static_cast<int32_t>(offsetof(MarshalledTextureBuffer_t4250577980, ___width_2)); }
	inline int32_t get_width_2() const { return ___width_2; }
	inline int32_t* get_address_of_width_2() { return &___width_2; }
	inline void set_width_2(int32_t value)
	{
		___width_2 = value;
	}

	inline static int32_t get_offset_of_height_3() { return static_cast<int32_t>(offsetof(MarshalledTextureBuffer_t4250577980, ___height_3)); }
	inline int32_t get_height_3() const { return ___height_3; }
	inline int32_t* get_address_of_height_3() { return &___height_3; }
	inline void set_height_3(int32_t value)
	{
		___height_3 = value;
	}

	inline static int32_t get_offset_of_format_4() { return static_cast<int32_t>(offsetof(MarshalledTextureBuffer_t4250577980, ___format_4)); }
	inline int32_t get_format_4() const { return ___format_4; }
	inline int32_t* get_address_of_format_4() { return &___format_4; }
	inline void set_format_4(int32_t value)
	{
		___format_4 = value;
	}

	inline static int32_t get_offset_of__mipMaps_5() { return static_cast<int32_t>(offsetof(MarshalledTextureBuffer_t4250577980, ____mipMaps_5)); }
	inline uint8_t get__mipMaps_5() const { return ____mipMaps_5; }
	inline uint8_t* get_address_of__mipMaps_5() { return &____mipMaps_5; }
	inline void set__mipMaps_5(uint8_t value)
	{
		____mipMaps_5 = value;
	}

	inline static int32_t get_offset_of_textureBufferHandle_6() { return static_cast<int32_t>(offsetof(MarshalledTextureBuffer_t4250577980, ___textureBufferHandle_6)); }
	inline intptr_t get_textureBufferHandle_6() const { return ___textureBufferHandle_6; }
	inline intptr_t* get_address_of_textureBufferHandle_6() { return &___textureBufferHandle_6; }
	inline void set_textureBufferHandle_6(intptr_t value)
	{
		___textureBufferHandle_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALLEDTEXTUREBUFFER_T4250577980_H
#ifndef UNITYINPUTHANDLER_T2924012501_H
#define UNITYINPUTHANDLER_T2924012501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.UnityInputHandler
struct  UnityInputHandler_t2924012501  : public RuntimeObject
{
public:
	// System.IntPtr Wrld.MapInput.UnityInputHandler::m_apiPtr
	intptr_t ___m_apiPtr_1;

public:
	inline static int32_t get_offset_of_m_apiPtr_1() { return static_cast<int32_t>(offsetof(UnityInputHandler_t2924012501, ___m_apiPtr_1)); }
	inline intptr_t get_m_apiPtr_1() const { return ___m_apiPtr_1; }
	inline intptr_t* get_address_of_m_apiPtr_1() { return &___m_apiPtr_1; }
	inline void set_m_apiPtr_1(intptr_t value)
	{
		___m_apiPtr_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYINPUTHANDLER_T2924012501_H
#ifndef ROTATEGESTURE_T3703755966_H
#define ROTATEGESTURE_T3703755966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.Touch.RotateGesture
struct  RotateGesture_t3703755966  : public RuntimeObject
{
public:
	// Wrld.MapInput.IUnityInputHandler Wrld.MapInput.Touch.RotateGesture::m_handler
	RuntimeObject* ___m_handler_0;
	// System.Boolean Wrld.MapInput.Touch.RotateGesture::rotating
	bool ___rotating_1;
	// UnityEngine.Vector2[] Wrld.MapInput.Touch.RotateGesture::lastPointer
	Vector2U5BU5D_t1457185986* ___lastPointer_2;
	// System.Single Wrld.MapInput.Touch.RotateGesture::lastRotationRadians
	float ___lastRotationRadians_3;
	// System.Single Wrld.MapInput.Touch.RotateGesture::m_totalRotation
	float ___m_totalRotation_4;
	// System.Single Wrld.MapInput.Touch.RotateGesture::m_previousRotationDelta
	float ___m_previousRotationDelta_5;
	// System.Boolean Wrld.MapInput.Touch.RotateGesture::needNewBaseline
	bool ___needNewBaseline_6;
	// UnityEngine.Vector2 Wrld.MapInput.Touch.RotateGesture::m_baselineDirection
	Vector2_t2156229523  ___m_baselineDirection_7;

public:
	inline static int32_t get_offset_of_m_handler_0() { return static_cast<int32_t>(offsetof(RotateGesture_t3703755966, ___m_handler_0)); }
	inline RuntimeObject* get_m_handler_0() const { return ___m_handler_0; }
	inline RuntimeObject** get_address_of_m_handler_0() { return &___m_handler_0; }
	inline void set_m_handler_0(RuntimeObject* value)
	{
		___m_handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_handler_0), value);
	}

	inline static int32_t get_offset_of_rotating_1() { return static_cast<int32_t>(offsetof(RotateGesture_t3703755966, ___rotating_1)); }
	inline bool get_rotating_1() const { return ___rotating_1; }
	inline bool* get_address_of_rotating_1() { return &___rotating_1; }
	inline void set_rotating_1(bool value)
	{
		___rotating_1 = value;
	}

	inline static int32_t get_offset_of_lastPointer_2() { return static_cast<int32_t>(offsetof(RotateGesture_t3703755966, ___lastPointer_2)); }
	inline Vector2U5BU5D_t1457185986* get_lastPointer_2() const { return ___lastPointer_2; }
	inline Vector2U5BU5D_t1457185986** get_address_of_lastPointer_2() { return &___lastPointer_2; }
	inline void set_lastPointer_2(Vector2U5BU5D_t1457185986* value)
	{
		___lastPointer_2 = value;
		Il2CppCodeGenWriteBarrier((&___lastPointer_2), value);
	}

	inline static int32_t get_offset_of_lastRotationRadians_3() { return static_cast<int32_t>(offsetof(RotateGesture_t3703755966, ___lastRotationRadians_3)); }
	inline float get_lastRotationRadians_3() const { return ___lastRotationRadians_3; }
	inline float* get_address_of_lastRotationRadians_3() { return &___lastRotationRadians_3; }
	inline void set_lastRotationRadians_3(float value)
	{
		___lastRotationRadians_3 = value;
	}

	inline static int32_t get_offset_of_m_totalRotation_4() { return static_cast<int32_t>(offsetof(RotateGesture_t3703755966, ___m_totalRotation_4)); }
	inline float get_m_totalRotation_4() const { return ___m_totalRotation_4; }
	inline float* get_address_of_m_totalRotation_4() { return &___m_totalRotation_4; }
	inline void set_m_totalRotation_4(float value)
	{
		___m_totalRotation_4 = value;
	}

	inline static int32_t get_offset_of_m_previousRotationDelta_5() { return static_cast<int32_t>(offsetof(RotateGesture_t3703755966, ___m_previousRotationDelta_5)); }
	inline float get_m_previousRotationDelta_5() const { return ___m_previousRotationDelta_5; }
	inline float* get_address_of_m_previousRotationDelta_5() { return &___m_previousRotationDelta_5; }
	inline void set_m_previousRotationDelta_5(float value)
	{
		___m_previousRotationDelta_5 = value;
	}

	inline static int32_t get_offset_of_needNewBaseline_6() { return static_cast<int32_t>(offsetof(RotateGesture_t3703755966, ___needNewBaseline_6)); }
	inline bool get_needNewBaseline_6() const { return ___needNewBaseline_6; }
	inline bool* get_address_of_needNewBaseline_6() { return &___needNewBaseline_6; }
	inline void set_needNewBaseline_6(bool value)
	{
		___needNewBaseline_6 = value;
	}

	inline static int32_t get_offset_of_m_baselineDirection_7() { return static_cast<int32_t>(offsetof(RotateGesture_t3703755966, ___m_baselineDirection_7)); }
	inline Vector2_t2156229523  get_m_baselineDirection_7() const { return ___m_baselineDirection_7; }
	inline Vector2_t2156229523 * get_address_of_m_baselineDirection_7() { return &___m_baselineDirection_7; }
	inline void set_m_baselineDirection_7(Vector2_t2156229523  value)
	{
		___m_baselineDirection_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEGESTURE_T3703755966_H
#ifndef PANGESTURE_T3882187103_H
#define PANGESTURE_T3882187103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapInput.Touch.PanGesture
struct  PanGesture_t3882187103  : public RuntimeObject
{
public:
	// Wrld.MapInput.IUnityInputHandler Wrld.MapInput.Touch.PanGesture::m_handler
	RuntimeObject* ___m_handler_0;
	// System.Boolean Wrld.MapInput.Touch.PanGesture::panning
	bool ___panning_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Wrld.MapInput.Touch.PanGesture::inputs
	List_1_t3628304265 * ___inputs_2;
	// UnityEngine.Vector2 Wrld.MapInput.Touch.PanGesture::panCenter
	Vector2_t2156229523  ___panCenter_3;
	// UnityEngine.Vector2 Wrld.MapInput.Touch.PanGesture::panAnchor
	Vector2_t2156229523  ___panAnchor_4;
	// System.Single Wrld.MapInput.Touch.PanGesture::majorScreenDimension
	float ___majorScreenDimension_5;

public:
	inline static int32_t get_offset_of_m_handler_0() { return static_cast<int32_t>(offsetof(PanGesture_t3882187103, ___m_handler_0)); }
	inline RuntimeObject* get_m_handler_0() const { return ___m_handler_0; }
	inline RuntimeObject** get_address_of_m_handler_0() { return &___m_handler_0; }
	inline void set_m_handler_0(RuntimeObject* value)
	{
		___m_handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_handler_0), value);
	}

	inline static int32_t get_offset_of_panning_1() { return static_cast<int32_t>(offsetof(PanGesture_t3882187103, ___panning_1)); }
	inline bool get_panning_1() const { return ___panning_1; }
	inline bool* get_address_of_panning_1() { return &___panning_1; }
	inline void set_panning_1(bool value)
	{
		___panning_1 = value;
	}

	inline static int32_t get_offset_of_inputs_2() { return static_cast<int32_t>(offsetof(PanGesture_t3882187103, ___inputs_2)); }
	inline List_1_t3628304265 * get_inputs_2() const { return ___inputs_2; }
	inline List_1_t3628304265 ** get_address_of_inputs_2() { return &___inputs_2; }
	inline void set_inputs_2(List_1_t3628304265 * value)
	{
		___inputs_2 = value;
		Il2CppCodeGenWriteBarrier((&___inputs_2), value);
	}

	inline static int32_t get_offset_of_panCenter_3() { return static_cast<int32_t>(offsetof(PanGesture_t3882187103, ___panCenter_3)); }
	inline Vector2_t2156229523  get_panCenter_3() const { return ___panCenter_3; }
	inline Vector2_t2156229523 * get_address_of_panCenter_3() { return &___panCenter_3; }
	inline void set_panCenter_3(Vector2_t2156229523  value)
	{
		___panCenter_3 = value;
	}

	inline static int32_t get_offset_of_panAnchor_4() { return static_cast<int32_t>(offsetof(PanGesture_t3882187103, ___panAnchor_4)); }
	inline Vector2_t2156229523  get_panAnchor_4() const { return ___panAnchor_4; }
	inline Vector2_t2156229523 * get_address_of_panAnchor_4() { return &___panAnchor_4; }
	inline void set_panAnchor_4(Vector2_t2156229523  value)
	{
		___panAnchor_4 = value;
	}

	inline static int32_t get_offset_of_majorScreenDimension_5() { return static_cast<int32_t>(offsetof(PanGesture_t3882187103, ___majorScreenDimension_5)); }
	inline float get_majorScreenDimension_5() const { return ___majorScreenDimension_5; }
	inline float* get_address_of_majorScreenDimension_5() { return &___majorScreenDimension_5; }
	inline void set_majorScreenDimension_5(float value)
	{
		___majorScreenDimension_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PANGESTURE_T3882187103_H
#ifndef UNITYWORLDSPACECOORDINATEFRAME_T1382732960_H
#define UNITYWORLDSPACECOORDINATEFRAME_T1382732960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Space.UnityWorldSpaceCoordinateFrame
struct  UnityWorldSpaceCoordinateFrame_t1382732960  : public RuntimeObject
{
public:
	// UnityEngine.Quaternion Wrld.Space.UnityWorldSpaceCoordinateFrame::<ECEFToLocalRotation>k__BackingField
	Quaternion_t2301928331  ___U3CECEFToLocalRotationU3Ek__BackingField_0;
	// UnityEngine.Quaternion Wrld.Space.UnityWorldSpaceCoordinateFrame::<LocalToECEFRotation>k__BackingField
	Quaternion_t2301928331  ___U3CLocalToECEFRotationU3Ek__BackingField_1;
	// Wrld.Common.Maths.DoubleVector3 Wrld.Space.UnityWorldSpaceCoordinateFrame::m_originECEF
	DoubleVector3_t761704365  ___m_originECEF_2;
	// UnityEngine.Vector3 Wrld.Space.UnityWorldSpaceCoordinateFrame::m_upECEF
	Vector3_t3722313464  ___m_upECEF_3;
	// UnityEngine.Vector3 Wrld.Space.UnityWorldSpaceCoordinateFrame::m_rightECEF
	Vector3_t3722313464  ___m_rightECEF_4;
	// UnityEngine.Vector3 Wrld.Space.UnityWorldSpaceCoordinateFrame::m_forwardECEF
	Vector3_t3722313464  ___m_forwardECEF_5;

public:
	inline static int32_t get_offset_of_U3CECEFToLocalRotationU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UnityWorldSpaceCoordinateFrame_t1382732960, ___U3CECEFToLocalRotationU3Ek__BackingField_0)); }
	inline Quaternion_t2301928331  get_U3CECEFToLocalRotationU3Ek__BackingField_0() const { return ___U3CECEFToLocalRotationU3Ek__BackingField_0; }
	inline Quaternion_t2301928331 * get_address_of_U3CECEFToLocalRotationU3Ek__BackingField_0() { return &___U3CECEFToLocalRotationU3Ek__BackingField_0; }
	inline void set_U3CECEFToLocalRotationU3Ek__BackingField_0(Quaternion_t2301928331  value)
	{
		___U3CECEFToLocalRotationU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CLocalToECEFRotationU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UnityWorldSpaceCoordinateFrame_t1382732960, ___U3CLocalToECEFRotationU3Ek__BackingField_1)); }
	inline Quaternion_t2301928331  get_U3CLocalToECEFRotationU3Ek__BackingField_1() const { return ___U3CLocalToECEFRotationU3Ek__BackingField_1; }
	inline Quaternion_t2301928331 * get_address_of_U3CLocalToECEFRotationU3Ek__BackingField_1() { return &___U3CLocalToECEFRotationU3Ek__BackingField_1; }
	inline void set_U3CLocalToECEFRotationU3Ek__BackingField_1(Quaternion_t2301928331  value)
	{
		___U3CLocalToECEFRotationU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_m_originECEF_2() { return static_cast<int32_t>(offsetof(UnityWorldSpaceCoordinateFrame_t1382732960, ___m_originECEF_2)); }
	inline DoubleVector3_t761704365  get_m_originECEF_2() const { return ___m_originECEF_2; }
	inline DoubleVector3_t761704365 * get_address_of_m_originECEF_2() { return &___m_originECEF_2; }
	inline void set_m_originECEF_2(DoubleVector3_t761704365  value)
	{
		___m_originECEF_2 = value;
	}

	inline static int32_t get_offset_of_m_upECEF_3() { return static_cast<int32_t>(offsetof(UnityWorldSpaceCoordinateFrame_t1382732960, ___m_upECEF_3)); }
	inline Vector3_t3722313464  get_m_upECEF_3() const { return ___m_upECEF_3; }
	inline Vector3_t3722313464 * get_address_of_m_upECEF_3() { return &___m_upECEF_3; }
	inline void set_m_upECEF_3(Vector3_t3722313464  value)
	{
		___m_upECEF_3 = value;
	}

	inline static int32_t get_offset_of_m_rightECEF_4() { return static_cast<int32_t>(offsetof(UnityWorldSpaceCoordinateFrame_t1382732960, ___m_rightECEF_4)); }
	inline Vector3_t3722313464  get_m_rightECEF_4() const { return ___m_rightECEF_4; }
	inline Vector3_t3722313464 * get_address_of_m_rightECEF_4() { return &___m_rightECEF_4; }
	inline void set_m_rightECEF_4(Vector3_t3722313464  value)
	{
		___m_rightECEF_4 = value;
	}

	inline static int32_t get_offset_of_m_forwardECEF_5() { return static_cast<int32_t>(offsetof(UnityWorldSpaceCoordinateFrame_t1382732960, ___m_forwardECEF_5)); }
	inline Vector3_t3722313464  get_m_forwardECEF_5() const { return ___m_forwardECEF_5; }
	inline Vector3_t3722313464 * get_address_of_m_forwardECEF_5() { return &___m_forwardECEF_5; }
	inline void set_m_forwardECEF_5(Vector3_t3722313464  value)
	{
		___m_forwardECEF_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYWORLDSPACECOORDINATEFRAME_T1382732960_H
#ifndef ECEFTRANSFORMUPDATESTRATEGY_T443103155_H
#define ECEFTRANSFORMUPDATESTRATEGY_T443103155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Space.ECEFTransformUpdateStrategy
struct  ECEFTransformUpdateStrategy_t443103155  : public RuntimeObject
{
public:
	// Wrld.Common.Maths.DoubleVector3 Wrld.Space.ECEFTransformUpdateStrategy::m_cameraPositionECEF
	DoubleVector3_t761704365  ___m_cameraPositionECEF_0;
	// UnityEngine.Vector3 Wrld.Space.ECEFTransformUpdateStrategy::m_up
	Vector3_t3722313464  ___m_up_1;

public:
	inline static int32_t get_offset_of_m_cameraPositionECEF_0() { return static_cast<int32_t>(offsetof(ECEFTransformUpdateStrategy_t443103155, ___m_cameraPositionECEF_0)); }
	inline DoubleVector3_t761704365  get_m_cameraPositionECEF_0() const { return ___m_cameraPositionECEF_0; }
	inline DoubleVector3_t761704365 * get_address_of_m_cameraPositionECEF_0() { return &___m_cameraPositionECEF_0; }
	inline void set_m_cameraPositionECEF_0(DoubleVector3_t761704365  value)
	{
		___m_cameraPositionECEF_0 = value;
	}

	inline static int32_t get_offset_of_m_up_1() { return static_cast<int32_t>(offsetof(ECEFTransformUpdateStrategy_t443103155, ___m_up_1)); }
	inline Vector3_t3722313464  get_m_up_1() const { return ___m_up_1; }
	inline Vector3_t3722313464 * get_address_of_m_up_1() { return &___m_up_1; }
	inline void set_m_up_1(Vector3_t3722313464  value)
	{
		___m_up_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECEFTRANSFORMUPDATESTRATEGY_T443103155_H
#ifndef LATLONGALTITUDE_T944891001_H
#define LATLONGALTITUDE_T944891001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Space.LatLongAltitude
struct  LatLongAltitude_t944891001 
{
public:
	// Wrld.Space.LatLong Wrld.Space.LatLongAltitude::m_latLong
	LatLong_t2936018554  ___m_latLong_0;
	// System.Double Wrld.Space.LatLongAltitude::m_altitude
	double ___m_altitude_1;

public:
	inline static int32_t get_offset_of_m_latLong_0() { return static_cast<int32_t>(offsetof(LatLongAltitude_t944891001, ___m_latLong_0)); }
	inline LatLong_t2936018554  get_m_latLong_0() const { return ___m_latLong_0; }
	inline LatLong_t2936018554 * get_address_of_m_latLong_0() { return &___m_latLong_0; }
	inline void set_m_latLong_0(LatLong_t2936018554  value)
	{
		___m_latLong_0 = value;
	}

	inline static int32_t get_offset_of_m_altitude_1() { return static_cast<int32_t>(offsetof(LatLongAltitude_t944891001, ___m_altitude_1)); }
	inline double get_m_altitude_1() const { return ___m_altitude_1; }
	inline double* get_address_of_m_altitude_1() { return &___m_altitude_1; }
	inline void set_m_altitude_1(double value)
	{
		___m_altitude_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LATLONGALTITUDE_T944891001_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GAMEOBJECTSTREAMER_T3452608707_H
#define GAMEOBJECTSTREAMER_T3452608707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Streaming.GameObjectStreamer
struct  GameObjectStreamer_t3452608707  : public RuntimeObject
{
public:
	// Wrld.Streaming.GameObjectRepository Wrld.Streaming.GameObjectStreamer::m_gameObjectRepository
	GameObjectRepository_t3760691595 * ___m_gameObjectRepository_0;
	// Wrld.Materials.MaterialRepository Wrld.Streaming.GameObjectStreamer::m_materialRepository
	MaterialRepository_t701520604 * ___m_materialRepository_1;
	// Wrld.Streaming.GameObjectFactory Wrld.Streaming.GameObjectStreamer::m_gameObjectCreator
	GameObjectFactory_t2478326784 * ___m_gameObjectCreator_2;
	// Wrld.Streaming.CollisionStreamingType Wrld.Streaming.GameObjectStreamer::m_collisions
	int32_t ___m_collisions_3;

public:
	inline static int32_t get_offset_of_m_gameObjectRepository_0() { return static_cast<int32_t>(offsetof(GameObjectStreamer_t3452608707, ___m_gameObjectRepository_0)); }
	inline GameObjectRepository_t3760691595 * get_m_gameObjectRepository_0() const { return ___m_gameObjectRepository_0; }
	inline GameObjectRepository_t3760691595 ** get_address_of_m_gameObjectRepository_0() { return &___m_gameObjectRepository_0; }
	inline void set_m_gameObjectRepository_0(GameObjectRepository_t3760691595 * value)
	{
		___m_gameObjectRepository_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_gameObjectRepository_0), value);
	}

	inline static int32_t get_offset_of_m_materialRepository_1() { return static_cast<int32_t>(offsetof(GameObjectStreamer_t3452608707, ___m_materialRepository_1)); }
	inline MaterialRepository_t701520604 * get_m_materialRepository_1() const { return ___m_materialRepository_1; }
	inline MaterialRepository_t701520604 ** get_address_of_m_materialRepository_1() { return &___m_materialRepository_1; }
	inline void set_m_materialRepository_1(MaterialRepository_t701520604 * value)
	{
		___m_materialRepository_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialRepository_1), value);
	}

	inline static int32_t get_offset_of_m_gameObjectCreator_2() { return static_cast<int32_t>(offsetof(GameObjectStreamer_t3452608707, ___m_gameObjectCreator_2)); }
	inline GameObjectFactory_t2478326784 * get_m_gameObjectCreator_2() const { return ___m_gameObjectCreator_2; }
	inline GameObjectFactory_t2478326784 ** get_address_of_m_gameObjectCreator_2() { return &___m_gameObjectCreator_2; }
	inline void set_m_gameObjectCreator_2(GameObjectFactory_t2478326784 * value)
	{
		___m_gameObjectCreator_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_gameObjectCreator_2), value);
	}

	inline static int32_t get_offset_of_m_collisions_3() { return static_cast<int32_t>(offsetof(GameObjectStreamer_t3452608707, ___m_collisions_3)); }
	inline int32_t get_m_collisions_3() const { return ___m_collisions_3; }
	inline int32_t* get_address_of_m_collisions_3() { return &___m_collisions_3; }
	inline void set_m_collisions_3(int32_t value)
	{
		___m_collisions_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTSTREAMER_T3452608707_H
#ifndef TEXTUREBUFFER_T2704239211_H
#define TEXTUREBUFFER_T2704239211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Materials.TextureLoadHandler/TextureBuffer
struct  TextureBuffer_t2704239211  : public RuntimeObject
{
public:
	// System.Byte[] Wrld.Materials.TextureLoadHandler/TextureBuffer::allocatedBuffer
	ByteU5BU5D_t4116647657* ___allocatedBuffer_0;
	// System.Int32 Wrld.Materials.TextureLoadHandler/TextureBuffer::width
	int32_t ___width_1;
	// System.Int32 Wrld.Materials.TextureLoadHandler/TextureBuffer::height
	int32_t ___height_2;
	// UnityEngine.TextureFormat Wrld.Materials.TextureLoadHandler/TextureBuffer::format
	int32_t ___format_3;
	// System.Boolean Wrld.Materials.TextureLoadHandler/TextureBuffer::mipMaps
	bool ___mipMaps_4;
	// System.UInt32 Wrld.Materials.TextureLoadHandler/TextureBuffer::id
	uint32_t ___id_5;
	// System.Collections.Generic.List`1<System.Runtime.InteropServices.GCHandle> Wrld.Materials.TextureLoadHandler/TextureBuffer::gcHandles
	List_1_t528545633 * ___gcHandles_6;

public:
	inline static int32_t get_offset_of_allocatedBuffer_0() { return static_cast<int32_t>(offsetof(TextureBuffer_t2704239211, ___allocatedBuffer_0)); }
	inline ByteU5BU5D_t4116647657* get_allocatedBuffer_0() const { return ___allocatedBuffer_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_allocatedBuffer_0() { return &___allocatedBuffer_0; }
	inline void set_allocatedBuffer_0(ByteU5BU5D_t4116647657* value)
	{
		___allocatedBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___allocatedBuffer_0), value);
	}

	inline static int32_t get_offset_of_width_1() { return static_cast<int32_t>(offsetof(TextureBuffer_t2704239211, ___width_1)); }
	inline int32_t get_width_1() const { return ___width_1; }
	inline int32_t* get_address_of_width_1() { return &___width_1; }
	inline void set_width_1(int32_t value)
	{
		___width_1 = value;
	}

	inline static int32_t get_offset_of_height_2() { return static_cast<int32_t>(offsetof(TextureBuffer_t2704239211, ___height_2)); }
	inline int32_t get_height_2() const { return ___height_2; }
	inline int32_t* get_address_of_height_2() { return &___height_2; }
	inline void set_height_2(int32_t value)
	{
		___height_2 = value;
	}

	inline static int32_t get_offset_of_format_3() { return static_cast<int32_t>(offsetof(TextureBuffer_t2704239211, ___format_3)); }
	inline int32_t get_format_3() const { return ___format_3; }
	inline int32_t* get_address_of_format_3() { return &___format_3; }
	inline void set_format_3(int32_t value)
	{
		___format_3 = value;
	}

	inline static int32_t get_offset_of_mipMaps_4() { return static_cast<int32_t>(offsetof(TextureBuffer_t2704239211, ___mipMaps_4)); }
	inline bool get_mipMaps_4() const { return ___mipMaps_4; }
	inline bool* get_address_of_mipMaps_4() { return &___mipMaps_4; }
	inline void set_mipMaps_4(bool value)
	{
		___mipMaps_4 = value;
	}

	inline static int32_t get_offset_of_id_5() { return static_cast<int32_t>(offsetof(TextureBuffer_t2704239211, ___id_5)); }
	inline uint32_t get_id_5() const { return ___id_5; }
	inline uint32_t* get_address_of_id_5() { return &___id_5; }
	inline void set_id_5(uint32_t value)
	{
		___id_5 = value;
	}

	inline static int32_t get_offset_of_gcHandles_6() { return static_cast<int32_t>(offsetof(TextureBuffer_t2704239211, ___gcHandles_6)); }
	inline List_1_t528545633 * get_gcHandles_6() const { return ___gcHandles_6; }
	inline List_1_t528545633 ** get_address_of_gcHandles_6() { return &___gcHandles_6; }
	inline void set_gcHandles_6(List_1_t528545633 * value)
	{
		___gcHandles_6 = value;
		Il2CppCodeGenWriteBarrier((&___gcHandles_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREBUFFER_T2704239211_H
#ifndef ADDMESHCALLBACK_T1037702503_H
#define ADDMESHCALLBACK_T1037702503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapGameObjectScene/AddMeshCallback
struct  AddMeshCallback_t1037702503  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDMESHCALLBACK_T1037702503_H
#ifndef NATIVEHIGHLIGHTMESHCLEARCALLBACK_T813633652_H
#define NATIVEHIGHLIGHTMESHCLEARCALLBACK_T813633652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Resources.Buildings.BuildingsApi/NativeHighlightMeshClearCallback
struct  NativeHighlightMeshClearCallback_t813633652  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEHIGHLIGHTMESHCLEARCALLBACK_T813633652_H
#ifndef RELEASETEXTURECALLBACK_T388032167_H
#define RELEASETEXTURECALLBACK_T388032167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Materials.TextureLoadHandler/ReleaseTextureCallback
struct  ReleaseTextureCallback_t388032167  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RELEASETEXTURECALLBACK_T388032167_H
#ifndef BEGINUPLOADTEXTUREBUFFERCALLBACK_T3016670851_H
#define BEGINUPLOADTEXTUREBUFFERCALLBACK_T3016670851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Materials.TextureLoadHandler/BeginUploadTextureBufferCallback
struct  BeginUploadTextureBufferCallback_t3016670851  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEGINUPLOADTEXTUREBUFFERCALLBACK_T3016670851_H
#ifndef ALLOCATETEXTUREBUFFERCALLBACK_T109657048_H
#define ALLOCATETEXTUREBUFFERCALLBACK_T109657048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Materials.TextureLoadHandler/AllocateTextureBufferCallback
struct  AllocateTextureBufferCallback_t109657048  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLOCATETEXTUREBUFFERCALLBACK_T109657048_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ALLOCATEUNPACKEDMESHCALLBACK_T3754430302_H
#define ALLOCATEUNPACKEDMESHCALLBACK_T3754430302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Meshes.MeshUploader/AllocateUnpackedMeshCallback
struct  AllocateUnpackedMeshCallback_t3754430302  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALLOCATEUNPACKEDMESHCALLBACK_T3754430302_H
#ifndef NATIVEHIGHLIGHTMESHUPLOADCALLBACK_T608941254_H
#define NATIVEHIGHLIGHTMESHUPLOADCALLBACK_T608941254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Resources.Buildings.BuildingsApi/NativeHighlightMeshUploadCallback
struct  NativeHighlightMeshUploadCallback_t608941254  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEHIGHLIGHTMESHUPLOADCALLBACK_T608941254_H
#ifndef HANDLEASSERTCALLBACK_T2932343000_H
#define HANDLEASSERTCALLBACK_T2932343000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.AssertHandler/HandleAssertCallback
struct  HandleAssertCallback_t2932343000  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDLEASSERTCALLBACK_T2932343000_H
#ifndef HIGHLIGHTRECEIVEDCALLBACK_T2001851641_H
#define HIGHLIGHTRECEIVEDCALLBACK_T2001851641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Resources.Buildings.BuildingsApi/HighlightReceivedCallback
struct  HighlightReceivedCallback_t2001851641  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIGHLIGHTRECEIVEDCALLBACK_T2001851641_H
#ifndef NATIVEBUILDINGRECEIVEDCALLBACK_T2577383538_H
#define NATIVEBUILDINGRECEIVEDCALLBACK_T2577383538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Resources.Buildings.BuildingsApi/NativeBuildingReceivedCallback
struct  NativeBuildingReceivedCallback_t2577383538  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEBUILDINGRECEIVEDCALLBACK_T2577383538_H
#ifndef BUILDINGRECEIVEDCALLBACK_T1209487313_H
#define BUILDINGRECEIVEDCALLBACK_T1209487313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Resources.Buildings.BuildingsApi/BuildingReceivedCallback
struct  BuildingReceivedCallback_t1209487313  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDINGRECEIVEDCALLBACK_T1209487313_H
#ifndef UPLOADUNPACKEDMESHCALLBACK_T3790783597_H
#define UPLOADUNPACKEDMESHCALLBACK_T3790783597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Meshes.MeshUploader/UploadUnpackedMeshCallback
struct  UploadUnpackedMeshCallback_t3790783597  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADUNPACKEDMESHCALLBACK_T3790783597_H
#ifndef NATIVEHIGHLIGHTRECEIVEDCALLBACK_T2749649652_H
#define NATIVEHIGHLIGHTRECEIVEDCALLBACK_T2749649652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Resources.Buildings.BuildingsApi/NativeHighlightReceivedCallback
struct  NativeHighlightReceivedCallback_t2749649652  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEHIGHLIGHTRECEIVEDCALLBACK_T2749649652_H
#ifndef VISIBILITYCALLBACK_T2021865822_H
#define VISIBILITYCALLBACK_T2021865822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapGameObjectScene/VisibilityCallback
struct  VisibilityCallback_t2021865822  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISIBILITYCALLBACK_T2021865822_H
#ifndef DELETEMESHCALLBACK_T4229142016_H
#define DELETEMESHCALLBACK_T4229142016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapGameObjectScene/DeleteMeshCallback
struct  DeleteMeshCallback_t4229142016  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETEMESHCALLBACK_T4229142016_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ENDPROGRAM_T1276848342_H
#define ENDPROGRAM_T1276848342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EndProgram
struct  EndProgram_t1276848342  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDPROGRAM_T1276848342_H
#ifndef ERRORMESSAGE_T4118551105_H
#define ERRORMESSAGE_T4118551105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ErrorMessage
struct  ErrorMessage_t4118551105  : public MonoBehaviour_t3962482529
{
public:
	// System.String ErrorMessage::<Title>k__BackingField
	String_t* ___U3CTitleU3Ek__BackingField_2;
	// System.String ErrorMessage::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CTitleU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ErrorMessage_t4118551105, ___U3CTitleU3Ek__BackingField_2)); }
	inline String_t* get_U3CTitleU3Ek__BackingField_2() const { return ___U3CTitleU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CTitleU3Ek__BackingField_2() { return &___U3CTitleU3Ek__BackingField_2; }
	inline void set_U3CTitleU3Ek__BackingField_2(String_t* value)
	{
		___U3CTitleU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTitleU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ErrorMessage_t4118551105, ___U3CTextU3Ek__BackingField_3)); }
	inline String_t* get_U3CTextU3Ek__BackingField_3() const { return ___U3CTextU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_3() { return &___U3CTextU3Ek__BackingField_3; }
	inline void set_U3CTextU3Ek__BackingField_3(String_t* value)
	{
		___U3CTextU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORMESSAGE_T4118551105_H
#ifndef GEOGRAPHICTRANSFORM_T2262143282_H
#define GEOGRAPHICTRANSFORM_T2262143282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.Space.GeographicTransform
struct  GeographicTransform_t2262143282  : public MonoBehaviour_t3962482529
{
public:
	// System.Double Wrld.Space.GeographicTransform::InitialLatitude
	double ___InitialLatitude_2;
	// System.Double Wrld.Space.GeographicTransform::InitialLongitude
	double ___InitialLongitude_3;
	// System.Single Wrld.Space.GeographicTransform::InitialHeadingInDegrees
	float ___InitialHeadingInDegrees_4;
	// Wrld.Common.Maths.EcefTangentBasis Wrld.Space.GeographicTransform::<TangentBasis>k__BackingField
	EcefTangentBasis_t256737685 * ___U3CTangentBasisU3Ek__BackingField_5;
	// System.Boolean Wrld.Space.GeographicTransform::m_hasEverBeenRegistered
	bool ___m_hasEverBeenRegistered_6;

public:
	inline static int32_t get_offset_of_InitialLatitude_2() { return static_cast<int32_t>(offsetof(GeographicTransform_t2262143282, ___InitialLatitude_2)); }
	inline double get_InitialLatitude_2() const { return ___InitialLatitude_2; }
	inline double* get_address_of_InitialLatitude_2() { return &___InitialLatitude_2; }
	inline void set_InitialLatitude_2(double value)
	{
		___InitialLatitude_2 = value;
	}

	inline static int32_t get_offset_of_InitialLongitude_3() { return static_cast<int32_t>(offsetof(GeographicTransform_t2262143282, ___InitialLongitude_3)); }
	inline double get_InitialLongitude_3() const { return ___InitialLongitude_3; }
	inline double* get_address_of_InitialLongitude_3() { return &___InitialLongitude_3; }
	inline void set_InitialLongitude_3(double value)
	{
		___InitialLongitude_3 = value;
	}

	inline static int32_t get_offset_of_InitialHeadingInDegrees_4() { return static_cast<int32_t>(offsetof(GeographicTransform_t2262143282, ___InitialHeadingInDegrees_4)); }
	inline float get_InitialHeadingInDegrees_4() const { return ___InitialHeadingInDegrees_4; }
	inline float* get_address_of_InitialHeadingInDegrees_4() { return &___InitialHeadingInDegrees_4; }
	inline void set_InitialHeadingInDegrees_4(float value)
	{
		___InitialHeadingInDegrees_4 = value;
	}

	inline static int32_t get_offset_of_U3CTangentBasisU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GeographicTransform_t2262143282, ___U3CTangentBasisU3Ek__BackingField_5)); }
	inline EcefTangentBasis_t256737685 * get_U3CTangentBasisU3Ek__BackingField_5() const { return ___U3CTangentBasisU3Ek__BackingField_5; }
	inline EcefTangentBasis_t256737685 ** get_address_of_U3CTangentBasisU3Ek__BackingField_5() { return &___U3CTangentBasisU3Ek__BackingField_5; }
	inline void set_U3CTangentBasisU3Ek__BackingField_5(EcefTangentBasis_t256737685 * value)
	{
		___U3CTangentBasisU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTangentBasisU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_m_hasEverBeenRegistered_6() { return static_cast<int32_t>(offsetof(GeographicTransform_t2262143282, ___m_hasEverBeenRegistered_6)); }
	inline bool get_m_hasEverBeenRegistered_6() const { return ___m_hasEverBeenRegistered_6; }
	inline bool* get_address_of_m_hasEverBeenRegistered_6() { return &___m_hasEverBeenRegistered_6; }
	inline void set_m_hasEverBeenRegistered_6(bool value)
	{
		___m_hasEverBeenRegistered_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GEOGRAPHICTRANSFORM_T2262143282_H
#ifndef STREAMINGBEHAVIOUR_T1089570205_H
#define STREAMINGBEHAVIOUR_T1089570205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wrld.MapCamera.StreamingBehaviour
struct  StreamingBehaviour_t1089570205  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGBEHAVIOUR_T1089570205_H
#ifndef VIEWNORMALS_T1335920405_H
#define VIEWNORMALS_T1335920405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ViewNormals
struct  ViewNormals_t1335920405  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.MeshFilter ViewNormals::objectMesh
	MeshFilter_t3523625662 * ___objectMesh_2;

public:
	inline static int32_t get_offset_of_objectMesh_2() { return static_cast<int32_t>(offsetof(ViewNormals_t1335920405, ___objectMesh_2)); }
	inline MeshFilter_t3523625662 * get_objectMesh_2() const { return ___objectMesh_2; }
	inline MeshFilter_t3523625662 ** get_address_of_objectMesh_2() { return &___objectMesh_2; }
	inline void set_objectMesh_2(MeshFilter_t3523625662 * value)
	{
		___objectMesh_2 = value;
		Il2CppCodeGenWriteBarrier((&___objectMesh_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWNORMALS_T1335920405_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3900 = { sizeof (MouseTouchGesture_t2284897039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3900[1] = 
{
	MouseTouchGesture_t2284897039::get_offset_of_m_handler_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3901 = { sizeof (MouseZoomGesture_t962341082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3901[4] = 
{
	MouseZoomGesture_t962341082::get_offset_of_m_handler_0(),
	MouseZoomGesture_t962341082::get_offset_of_m_sensitivity_1(),
	MouseZoomGesture_t962341082::get_offset_of_m_maxZoomDelta_2(),
	MouseZoomGesture_t962341082::get_offset_of_m_zoomAccumulator_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3902 = { sizeof (UnityMouseInputProcessor_t4249415975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3902[6] = 
{
	UnityMouseInputProcessor_t4249415975::get_offset_of_m_pan_0(),
	UnityMouseInputProcessor_t4249415975::get_offset_of_m_zoom_1(),
	UnityMouseInputProcessor_t4249415975::get_offset_of_m_rotate_2(),
	UnityMouseInputProcessor_t4249415975::get_offset_of_m_tilt_3(),
	UnityMouseInputProcessor_t4249415975::get_offset_of_m_touch_4(),
	UnityMouseInputProcessor_t4249415975::get_offset_of_m_tap_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3903 = { sizeof (PanGesture_t3882187103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3903[9] = 
{
	PanGesture_t3882187103::get_offset_of_m_handler_0(),
	PanGesture_t3882187103::get_offset_of_panning_1(),
	PanGesture_t3882187103::get_offset_of_inputs_2(),
	PanGesture_t3882187103::get_offset_of_panCenter_3(),
	PanGesture_t3882187103::get_offset_of_panAnchor_4(),
	PanGesture_t3882187103::get_offset_of_majorScreenDimension_5(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3904 = { sizeof (PinchGesture_t72617885), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3904[4] = 
{
	PinchGesture_t72617885::get_offset_of_m_handler_0(),
	PinchGesture_t72617885::get_offset_of_pinching_1(),
	PinchGesture_t72617885::get_offset_of_previousDistance_2(),
	PinchGesture_t72617885::get_offset_of_majorScreenDimension_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3905 = { sizeof (RotateGesture_t3703755966), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3905[8] = 
{
	RotateGesture_t3703755966::get_offset_of_m_handler_0(),
	RotateGesture_t3703755966::get_offset_of_rotating_1(),
	RotateGesture_t3703755966::get_offset_of_lastPointer_2(),
	RotateGesture_t3703755966::get_offset_of_lastRotationRadians_3(),
	RotateGesture_t3703755966::get_offset_of_m_totalRotation_4(),
	RotateGesture_t3703755966::get_offset_of_m_previousRotationDelta_5(),
	RotateGesture_t3703755966::get_offset_of_needNewBaseline_6(),
	RotateGesture_t3703755966::get_offset_of_m_baselineDirection_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3906 = { sizeof (TapGesture_t191755233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3906[10] = 
{
	TapGesture_t191755233::get_offset_of_m_tapDownCount_0(),
	TapGesture_t191755233::get_offset_of_m_tapUpCount_1(),
	TapGesture_t191755233::get_offset_of_m_currentPointerTrackingStack_2(),
	TapGesture_t191755233::get_offset_of_m_tapAnchorX_3(),
	TapGesture_t191755233::get_offset_of_m_tapAnchorY_4(),
	TapGesture_t191755233::get_offset_of_m_tapUnixTime_5(),
	0,
	0,
	0,
	TapGesture_t191755233::get_offset_of_m_tapHandler_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3907 = { sizeof (TouchInputPointerEvent_t630861639)+ sizeof (RuntimeObject), sizeof(TouchInputPointerEvent_t630861639 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3907[4] = 
{
	TouchInputPointerEvent_t630861639::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInputPointerEvent_t630861639::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInputPointerEvent_t630861639::get_offset_of_pointerIdentity_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInputPointerEvent_t630861639::get_offset_of_pointerIndex_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3908 = { sizeof (TouchInputEvent_t172715690)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3908[5] = 
{
	TouchInputEvent_t172715690::get_offset_of_isPointerUpEvent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInputEvent_t172715690::get_offset_of_isPointerDownEvent_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInputEvent_t172715690::get_offset_of_primaryActionIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInputEvent_t172715690::get_offset_of_primaryActionIdentifier_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TouchInputEvent_t172715690::get_offset_of_pointerEvents_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3909 = { sizeof (TouchGesture_t2500129316), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3909[1] = 
{
	TouchGesture_t2500129316::get_offset_of_m_handler_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3910 = { sizeof (UnityTouchInputProcessor_t158491272), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3910[5] = 
{
	UnityTouchInputProcessor_t158491272::get_offset_of_m_pan_0(),
	UnityTouchInputProcessor_t158491272::get_offset_of_m_pinch_1(),
	UnityTouchInputProcessor_t158491272::get_offset_of_m_rotate_2(),
	UnityTouchInputProcessor_t158491272::get_offset_of_m_touch_3(),
	UnityTouchInputProcessor_t158491272::get_offset_of_m_tap_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3911 = { sizeof (UnityInputHandler_t2924012501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3911[2] = 
{
	0,
	UnityInputHandler_t2924012501::get_offset_of_m_apiPtr_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3912 = { sizeof (IdGenerator_t1318283159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3912[2] = 
{
	IdGenerator_t1318283159::get_offset_of_m_idGenerator_0(),
	IdGenerator_t1318283159::get_offset_of_m_ids_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3913 = { sizeof (MaterialRepository_t701520604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3913[6] = 
{
	MaterialRepository_t701520604::get_offset_of_m_materialsRequiringTexture_0(),
	MaterialRepository_t701520604::get_offset_of_m_materials_1(),
	MaterialRepository_t701520604::get_offset_of_m_defaultMaterial_2(),
	MaterialRepository_t701520604::get_offset_of_m_defaultRasterTerrainMaterial_3(),
	MaterialRepository_t701520604::get_offset_of_m_textureLoadHandler_4(),
	MaterialRepository_t701520604::get_offset_of_m_materialDirectory_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3914 = { sizeof (ApplyTextureToMaterialRequest_t638715235)+ sizeof (RuntimeObject), sizeof(ApplyTextureToMaterialRequest_t638715235_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3914[2] = 
{
	ApplyTextureToMaterialRequest_t638715235::get_offset_of_U3CMaterialNameU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ApplyTextureToMaterialRequest_t638715235::get_offset_of_U3CTextureIDU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3915 = { sizeof (MaterialRecord_t2453468311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3915[2] = 
{
	MaterialRecord_t2453468311::get_offset_of_U3CMaterialU3Ek__BackingField_0(),
	MaterialRecord_t2453468311::get_offset_of_U3CReferenceCountU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3916 = { sizeof (U3CUpdateU3Ec__AnonStorey0_t2884843461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3916[1] = 
{
	U3CUpdateU3Ec__AnonStorey0_t2884843461::get_offset_of_toRemove_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3917 = { sizeof (TextureLoadHandler_t2080734353), -1, sizeof(TextureLoadHandler_t2080734353_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3917[5] = 
{
	0,
	TextureLoadHandler_t2080734353::get_offset_of_m_builtTextures_1(),
	TextureLoadHandler_t2080734353::get_offset_of_m_processQueue_2(),
	TextureLoadHandler_t2080734353::get_offset_of_m_idGenerator_3(),
	TextureLoadHandler_t2080734353_StaticFields::get_offset_of_ms_instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3918 = { sizeof (TextureBuffer_t2704239211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3918[7] = 
{
	TextureBuffer_t2704239211::get_offset_of_allocatedBuffer_0(),
	TextureBuffer_t2704239211::get_offset_of_width_1(),
	TextureBuffer_t2704239211::get_offset_of_height_2(),
	TextureBuffer_t2704239211::get_offset_of_format_3(),
	TextureBuffer_t2704239211::get_offset_of_mipMaps_4(),
	TextureBuffer_t2704239211::get_offset_of_id_5(),
	TextureBuffer_t2704239211::get_offset_of_gcHandles_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3919 = { sizeof (MarshalledTextureBuffer_t4250577980)+ sizeof (RuntimeObject), sizeof(MarshalledTextureBuffer_t4250577980 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3919[7] = 
{
	MarshalledTextureBuffer_t4250577980::get_offset_of_data_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MarshalledTextureBuffer_t4250577980::get_offset_of_sizeInBytes_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MarshalledTextureBuffer_t4250577980::get_offset_of_width_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MarshalledTextureBuffer_t4250577980::get_offset_of_height_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MarshalledTextureBuffer_t4250577980::get_offset_of_format_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MarshalledTextureBuffer_t4250577980::get_offset_of__mipMaps_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MarshalledTextureBuffer_t4250577980::get_offset_of_textureBufferHandle_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3920 = { sizeof (AllocateTextureBufferCallback_t109657048), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3921 = { sizeof (BeginUploadTextureBufferCallback_t3016670851), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3922 = { sizeof (ReleaseTextureCallback_t388032167), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3923 = { sizeof (DoubleVector3_t761704365)+ sizeof (RuntimeObject), sizeof(DoubleVector3_t761704365 ), sizeof(DoubleVector3_t761704365_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3923[6] = 
{
	0,
	DoubleVector3_t761704365_StaticFields::get_offset_of_zero_1(),
	DoubleVector3_t761704365_StaticFields::get_offset_of_one_2(),
	DoubleVector3_t761704365::get_offset_of_x_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DoubleVector3_t761704365::get_offset_of_y_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DoubleVector3_t761704365::get_offset_of_z_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3924 = { sizeof (DoubleVector3Extensions_t851847462), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3925 = { sizeof (EcefHelpers_t3574471672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3925[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3926 = { sizeof (EcefTangentBasis_t256737685), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3926[4] = 
{
	EcefTangentBasis_t256737685::get_offset_of_m_pointEcef_0(),
	EcefTangentBasis_t256737685::get_offset_of_m_basisRight_1(),
	EcefTangentBasis_t256737685::get_offset_of_m_basisUp_2(),
	EcefTangentBasis_t256737685::get_offset_of_m_basisForward_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3927 = { sizeof (MathsHelpers_t2198872558), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3928 = { sizeof (Matrix4x4Extensions_t1177881985), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3929 = { sizeof (QuaternionExtensions_t2180248493), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3930 = { sizeof (MeshBuilder_t1786676944), -1, sizeof(MeshBuilder_t1786676944_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3930[2] = 
{
	MeshBuilder_t1786676944_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	MeshBuilder_t1786676944_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3931 = { sizeof (U3CCreatePreparedMeshesU3Ec__AnonStorey0_t942887021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3931[3] = 
{
	U3CCreatePreparedMeshesU3Ec__AnonStorey0_t942887021::get_offset_of_verts_0(),
	U3CCreatePreparedMeshesU3Ec__AnonStorey0_t942887021::get_offset_of_uvs_1(),
	U3CCreatePreparedMeshesU3Ec__AnonStorey0_t942887021::get_offset_of_uv2s_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3932 = { sizeof (U3CCreatePreparedMeshesU3Ec__AnonStorey1_t3281539181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3932[3] = 
{
	U3CCreatePreparedMeshesU3Ec__AnonStorey1_t3281539181::get_offset_of_reversedRemapping_0(),
	U3CCreatePreparedMeshesU3Ec__AnonStorey1_t3281539181::get_offset_of_indexRemapper_1(),
	U3CCreatePreparedMeshesU3Ec__AnonStorey1_t3281539181::get_offset_of_U3CU3Ef__refU240_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3933 = { sizeof (MeshUploader_t3724330286), -1, sizeof(MeshUploader_t3724330286_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3933[1] = 
{
	MeshUploader_t3724330286_StaticFields::get_offset_of_m_preparedMeshes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3934 = { sizeof (UnpackedMesh_t2089855528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3934[8] = 
{
	UnpackedMesh_t2089855528::get_offset_of_vertices_0(),
	UnpackedMesh_t2089855528::get_offset_of_uvs_1(),
	UnpackedMesh_t2089855528::get_offset_of_uv2s_2(),
	UnpackedMesh_t2089855528::get_offset_of_indices_3(),
	UnpackedMesh_t2089855528::get_offset_of_originECEF_4(),
	UnpackedMesh_t2089855528::get_offset_of_materialName_5(),
	UnpackedMesh_t2089855528::get_offset_of_name_6(),
	UnpackedMesh_t2089855528::get_offset_of_gcHandles_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3935 = { sizeof (MarshalledMesh_t2163999146)+ sizeof (RuntimeObject), sizeof(MarshalledMesh_t2163999146 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3935[11] = 
{
	MarshalledMesh_t2163999146::get_offset_of_vertices_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MarshalledMesh_t2163999146::get_offset_of_vertexCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MarshalledMesh_t2163999146::get_offset_of_uvs_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MarshalledMesh_t2163999146::get_offset_of_uvCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MarshalledMesh_t2163999146::get_offset_of_uv2s_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MarshalledMesh_t2163999146::get_offset_of_uv2Count_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MarshalledMesh_t2163999146::get_offset_of_indices_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MarshalledMesh_t2163999146::get_offset_of_indexCount_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MarshalledMesh_t2163999146::get_offset_of_originEcef_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MarshalledMesh_t2163999146::get_offset_of_name_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MarshalledMesh_t2163999146::get_offset_of_unpackedMeshHandle_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3936 = { sizeof (AllocateUnpackedMeshCallback_t3754430302), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3937 = { sizeof (UploadUnpackedMeshCallback_t3790783597), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3938 = { sizeof (PreparedMesh_t3331185004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3938[6] = 
{
	PreparedMesh_t3331185004::get_offset_of_m_verts_0(),
	PreparedMesh_t3331185004::get_offset_of_m_uvs_1(),
	PreparedMesh_t3331185004::get_offset_of_m_uv2s_2(),
	PreparedMesh_t3331185004::get_offset_of_m_indices_3(),
	PreparedMesh_t3331185004::get_offset_of_m_name_4(),
	PreparedMesh_t3331185004::get_offset_of_m_normals_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3939 = { sizeof (PreparedMeshRecord_t446409467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3939[3] = 
{
	PreparedMeshRecord_t446409467::get_offset_of_U3CMeshesU3Ek__BackingField_0(),
	PreparedMeshRecord_t446409467::get_offset_of_U3COriginECEFU3Ek__BackingField_1(),
	PreparedMeshRecord_t446409467::get_offset_of_U3CMaterialNameU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3940 = { sizeof (PreparedMeshRepository_t1998320162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3940[1] = 
{
	PreparedMeshRepository_t1998320162::get_offset_of_m_meshRecords_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3941 = { sizeof (NativePluginRunner_t3528041536), -1, sizeof(NativePluginRunner_t3528041536_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3941[7] = 
{
	0,
	NativePluginRunner_t3528041536_StaticFields::get_offset_of_API_1(),
	NativePluginRunner_t3528041536::get_offset_of_m_materialRepository_2(),
	NativePluginRunner_t3528041536::get_offset_of_m_textureLoadHandler_3(),
	NativePluginRunner_t3528041536::get_offset_of_m_mapGameObjectScene_4(),
	NativePluginRunner_t3528041536::get_offset_of_m_streamingUpdater_5(),
	NativePluginRunner_t3528041536::get_offset_of_m_isRunning_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3942 = { sizeof (Building_t1491635829)+ sizeof (RuntimeObject), sizeof(Building_t1491635829_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3942[4] = 
{
	Building_t1491635829::get_offset_of_BuildingId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Building_t1491635829::get_offset_of_BaseAltitude_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Building_t1491635829::get_offset_of_TopAltitude_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Building_t1491635829::get_offset_of_Centroid_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3943 = { sizeof (Highlight_t3200169708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3943[2] = 
{
	Highlight_t3200169708::get_offset_of_HighlightId_0(),
	Highlight_t3200169708::get_offset_of_HighlightRequestId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3944 = { sizeof (BuildingInterop_t2102364785)+ sizeof (RuntimeObject), sizeof(BuildingInterop_t2102364785 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3944[5] = 
{
	BuildingInterop_t2102364785::get_offset_of_BaseAltitude_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BuildingInterop_t2102364785::get_offset_of_TopAltitude_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BuildingInterop_t2102364785::get_offset_of_CentroidLatitude_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BuildingInterop_t2102364785::get_offset_of_CentroidLongitude_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BuildingInterop_t2102364785::get_offset_of_StringIdPtr_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3945 = { sizeof (HighlightInterop_t2055068794)+ sizeof (RuntimeObject), sizeof(HighlightInterop_t2055068794 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3945[7] = 
{
	HighlightInterop_t2055068794::get_offset_of_OriginEcefX_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HighlightInterop_t2055068794::get_offset_of_OriginEcefY_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HighlightInterop_t2055068794::get_offset_of_OriginEcefZ_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HighlightInterop_t2055068794::get_offset_of_VertexCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HighlightInterop_t2055068794::get_offset_of_IndexCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HighlightInterop_t2055068794::get_offset_of_VertexPositions_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HighlightInterop_t2055068794::get_offset_of_Indices_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3946 = { sizeof (BuildingsApi_t3854622187), -1, sizeof(BuildingsApi_t3854622187_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3946[10] = 
{
	BuildingsApi_t3854622187_StaticFields::get_offset_of_BuildingRequests_0(),
	BuildingsApi_t3854622187_StaticFields::get_offset_of_HighlightRequests_1(),
	BuildingsApi_t3854622187_StaticFields::get_offset_of_HighlightStreamer_2(),
	BuildingsApi_t3854622187_StaticFields::get_offset_of_HighlightIdsToAdd_3(),
	BuildingsApi_t3854622187::get_offset_of_m_nextBuildingRequestId_4(),
	BuildingsApi_t3854622187::get_offset_of_m_nextHighlightRequestId_5(),
	BuildingsApi_t3854622187_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_6(),
	BuildingsApi_t3854622187_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_7(),
	BuildingsApi_t3854622187_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_8(),
	BuildingsApi_t3854622187_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3947 = { sizeof (BuildingReceivedCallback_t1209487313), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3948 = { sizeof (HighlightReceivedCallback_t2001851641), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3949 = { sizeof (BuildingRequest_t1598655962)+ sizeof (RuntimeObject), sizeof(BuildingRequest_t1598655962_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable3949[1] = 
{
	BuildingRequest_t1598655962::get_offset_of_callback_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3950 = { sizeof (HighlightRequest_t1416441333)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3950[2] = 
{
	HighlightRequest_t1416441333::get_offset_of_material_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HighlightRequest_t1416441333::get_offset_of_callback_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3951 = { sizeof (NativeHighlightMeshUploadCallback_t608941254), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3952 = { sizeof (NativeHighlightMeshClearCallback_t813633652), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3953 = { sizeof (NativeBuildingReceivedCallback_t2577383538), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3954 = { sizeof (NativeHighlightReceivedCallback_t2749649652), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3955 = { sizeof (TerrainHeightProvider_t517848921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3955[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3956 = { sizeof (EarthConstants_t444733090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3956[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3957 = { sizeof (ECEFTransformUpdateStrategy_t443103155), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3957[2] = 
{
	ECEFTransformUpdateStrategy_t443103155::get_offset_of_m_cameraPositionECEF_0(),
	ECEFTransformUpdateStrategy_t443103155::get_offset_of_m_up_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3958 = { sizeof (GeographicApi_t2934948604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3958[1] = 
{
	GeographicApi_t2934948604::get_offset_of_m_geographicTransforms_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3959 = { sizeof (GeographicTransform_t2262143282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3959[5] = 
{
	GeographicTransform_t2262143282::get_offset_of_InitialLatitude_2(),
	GeographicTransform_t2262143282::get_offset_of_InitialLongitude_3(),
	GeographicTransform_t2262143282::get_offset_of_InitialHeadingInDegrees_4(),
	GeographicTransform_t2262143282::get_offset_of_U3CTangentBasisU3Ek__BackingField_5(),
	GeographicTransform_t2262143282::get_offset_of_m_hasEverBeenRegistered_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3960 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3961 = { sizeof (CoordinateConversions_t2160508553), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3962 = { sizeof (LatLong_t2936018554)+ sizeof (RuntimeObject), sizeof(LatLong_t2936018554 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3962[2] = 
{
	LatLong_t2936018554::get_offset_of_m_latitudeInDegrees_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LatLong_t2936018554::get_offset_of_m_longitudeInDegrees_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3963 = { sizeof (LatLongAltitude_t944891001)+ sizeof (RuntimeObject), sizeof(LatLongAltitude_t944891001 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3963[2] = 
{
	LatLongAltitude_t944891001::get_offset_of_m_latLong_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LatLongAltitude_t944891001::get_offset_of_m_altitude_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3964 = { sizeof (UnityWorldSpaceCoordinateFrame_t1382732960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3964[6] = 
{
	UnityWorldSpaceCoordinateFrame_t1382732960::get_offset_of_U3CECEFToLocalRotationU3Ek__BackingField_0(),
	UnityWorldSpaceCoordinateFrame_t1382732960::get_offset_of_U3CLocalToECEFRotationU3Ek__BackingField_1(),
	UnityWorldSpaceCoordinateFrame_t1382732960::get_offset_of_m_originECEF_2(),
	UnityWorldSpaceCoordinateFrame_t1382732960::get_offset_of_m_upECEF_3(),
	UnityWorldSpaceCoordinateFrame_t1382732960::get_offset_of_m_rightECEF_4(),
	UnityWorldSpaceCoordinateFrame_t1382732960::get_offset_of_m_forwardECEF_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3965 = { sizeof (UnityWorldSpaceTransformUpdateStrategy_t501792395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3965[1] = 
{
	UnityWorldSpaceTransformUpdateStrategy_t501792395::get_offset_of_m_frame_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3966 = { sizeof (CollisionStreamingType_t2118098865)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3966[4] = 
{
	CollisionStreamingType_t2118098865::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3967 = { sizeof (GameObjectFactory_t2478326784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3967[1] = 
{
	GameObjectFactory_t2478326784::get_offset_of_m_parentTransform_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3968 = { sizeof (GameObjectRecord_t2871914821), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3968[2] = 
{
	GameObjectRecord_t2871914821::get_offset_of_U3COriginECEFU3Ek__BackingField_0(),
	GameObjectRecord_t2871914821::get_offset_of_U3CGameObjectsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3969 = { sizeof (GameObjectRepository_t3760691595), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3969[3] = 
{
	GameObjectRepository_t3760691595::get_offset_of_m_gameObjectsById_0(),
	GameObjectRepository_t3760691595::get_offset_of_m_root_1(),
	GameObjectRepository_t3760691595::get_offset_of_m_materialRepository_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3970 = { sizeof (GameObjectStreamer_t3452608707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3970[4] = 
{
	GameObjectStreamer_t3452608707::get_offset_of_m_gameObjectRepository_0(),
	GameObjectStreamer_t3452608707::get_offset_of_m_materialRepository_1(),
	GameObjectStreamer_t3452608707::get_offset_of_m_gameObjectCreator_2(),
	GameObjectStreamer_t3452608707::get_offset_of_m_collisions_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3971 = { sizeof (MapGameObjectScene_t128928738), -1, sizeof(MapGameObjectScene_t128928738_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3971[7] = 
{
	MapGameObjectScene_t128928738::get_offset_of_m_terrainStreamer_0(),
	MapGameObjectScene_t128928738::get_offset_of_m_roadStreamer_1(),
	MapGameObjectScene_t128928738::get_offset_of_m_buildingStreamer_2(),
	MapGameObjectScene_t128928738::get_offset_of_m_highlightStreamer_3(),
	MapGameObjectScene_t128928738::get_offset_of_m_meshUploader_4(),
	MapGameObjectScene_t128928738::get_offset_of_m_enabled_5(),
	MapGameObjectScene_t128928738_StaticFields::get_offset_of_ms_instance_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3972 = { sizeof (AddMeshCallback_t1037702503), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3973 = { sizeof (DeleteMeshCallback_t4229142016), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3974 = { sizeof (VisibilityCallback_t2021865822), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3975 = { sizeof (ResourceCeilingProvider_t2554508737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3975[1] = 
{
	ResourceCeilingProvider_t2554508737::get_offset_of_m_terrainHeightProvider_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3976 = { sizeof (StreamingBehaviour_t1089570205), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3977 = { sizeof (StreamingUpdater_t530401876), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3978 = { sizeof (APIKeyHelpers_t4091981690), -1, sizeof(APIKeyHelpers_t4091981690_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3978[1] = 
{
	APIKeyHelpers_t4091981690_StaticFields::get_offset_of_ms_cachedApiKey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3979 = { sizeof (AssertHandler_t1732545153), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3980 = { sizeof (HandleAssertCallback_t2932343000), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3981 = { sizeof (EndProgram_t1276848342), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3982 = { sizeof (ErrorMessage_t4118551105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3982[4] = 
{
	ErrorMessage_t4118551105::get_offset_of_U3CTitleU3Ek__BackingField_2(),
	ErrorMessage_t4118551105::get_offset_of_U3CTextU3Ek__BackingField_3(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3983 = { sizeof (InvalidApiKeyException_t175683051), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3984 = { sizeof (ViewNormals_t1335920405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3984[1] = 
{
	ViewNormals_t1335920405::get_offset_of_objectMesh_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3985 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255370), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255370_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3985[2] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255370_StaticFields::get_offset_of_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0(),
	U3CPrivateImplementationDetailsU3E_t3057255370_StaticFields::get_offset_of_U24fieldU2D811EB71438479767E10832E824E7D58DCDC0FCB8_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3986 = { sizeof (U24ArrayTypeU3D24_t2467506693)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D24_t2467506693 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3987 = { sizeof (U24ArrayTypeU3D208_t3449392908)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D208_t3449392908 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
