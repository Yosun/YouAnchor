﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.WeakReference
struct WeakReference_t1334886716;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Diagnostics.TraceListenerCollection
struct TraceListenerCollection_t1347122889;
// System.Diagnostics.CorrelationManager
struct CorrelationManager_t2688752967;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.String
struct String_t;
// System.Text.RegularExpressions.Regex
struct Regex_t3657309853;
// System.Net.CookieCollection
struct CookieCollection_t3881042616;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Net.HttpStreamAsyncResult
struct HttpStreamAsyncResult_t1178010344;
// System.Net.IAuthenticationModule
struct IAuthenticationModule_t3112562943;
// System.Net.ICredentialPolicy
struct ICredentialPolicy_t1002784953;
// System.Collections.Generic.List`1<System.Net.Cookie>
struct List_1_t2465948139;
// System.Net.CookieCollection/CookieCollectionComparer
struct CookieCollectionComparer_t1373927847;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t2342208608;
// System.Diagnostics.TraceImplSettings
struct TraceImplSettings_t2527703222;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// System.Configuration.IConfigurationSystem
struct IConfigurationSystem_t1184708823;
// System.Configuration.ConfigurationSectionCollection
struct ConfigurationSectionCollection_t2786897858;
// System.Configuration.ConfigurationSectionGroupCollection
struct ConfigurationSectionGroupCollection_t1151641153;
// System.Configuration.Configuration
struct Configuration_t2529364143;
// System.Configuration.SectionGroupInfo
struct SectionGroupInfo_t687896682;
// System.Configuration.ElementMap
struct ElementMap_t2160633803;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t2852175726;
// System.Configuration.ConfigurationElementCollection
struct ConfigurationElementCollection_t446763386;
// System.Configuration.ElementInformation
struct ElementInformation_t2651568025;
// System.Configuration.ConfigurationLockCollection
struct ConfigurationLockCollection_t4066281341;
// System.Collections.Stack
struct Stack_t2329662280;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t3590861854;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.ComponentModel.EventHandlerList
struct EventHandlerList_t1108123056;
// System.ComponentModel.ISite
struct ISite_t4006303512;
// System.Configuration.SectionInformation
struct SectionInformation_t2821611020;
// System.Configuration.IConfigurationSectionHandler
struct IConfigurationSectionHandler_t3614337894;
// System.Collections.IComparer
struct IComparer_t1540313114;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IO.Stream
struct Stream_t1273022909;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t451242010;
// System.Exception
struct Exception_t;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Diagnostics.Process
struct Process_t3774297411;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Text.Encoding
struct Encoding_t1523322056;
// System.Diagnostics.Process/AsyncReadHandler
struct AsyncReadHandler_t1188682440;
// Microsoft.Win32.SafeHandles.SafeWaitHandle
struct SafeWaitHandle_t1972936122;
// System.IO.Compression.DeflateStream
struct DeflateStream_t4175168077;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_t1942268960;
// System.Net.ChunkStream
struct ChunkStream_t2634567336;
// System.Net.HttpListenerContext
struct HttpListenerContext_t424880822;
// System.ComponentModel.ISynchronizeInvoke
struct ISynchronizeInvoke_t1357618335;
// System.EventHandler
struct EventHandler_t1348719766;
// System.Diagnostics.ProcessModuleCollection
struct ProcessModuleCollection_t3446348346;
// System.IO.StreamReader
struct StreamReader_t4009935899;
// System.IO.StreamWriter
struct StreamWriter_t1266378904;
// System.Diagnostics.ProcessStartInfo
struct ProcessStartInfo_t2184852744;
// System.Diagnostics.Process/ProcessAsyncReader
struct ProcessAsyncReader_t337580163;
// System.Diagnostics.DataReceivedEventHandler
struct DataReceivedEventHandler_t2795960821;
// System.IO.Compression.DeflateStream/UnmanagedReadOrWrite
struct UnmanagedReadOrWrite_t876388624;
// System.Collections.Specialized.StringDictionary
struct StringDictionary_t120437468;
// System.Diagnostics.TraceFilter
struct TraceFilter_t4153521180;
// System.Collections.Specialized.ProcessStringDictionary
struct ProcessStringDictionary_t2107791454;
// System.Security.SecureString
struct SecureString_t3041467854;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Uri
struct Uri_t100236324;
// System.Xml.XmlNode
struct XmlNode_t3767805227;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef WEAKOBJECTWRAPPER_T827463650_H
#define WEAKOBJECTWRAPPER_T827463650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.WeakObjectWrapper
struct  WeakObjectWrapper_t827463650  : public RuntimeObject
{
public:
	// System.Int32 System.ComponentModel.WeakObjectWrapper::<TargetHashCode>k__BackingField
	int32_t ___U3CTargetHashCodeU3Ek__BackingField_0;
	// System.WeakReference System.ComponentModel.WeakObjectWrapper::<Weak>k__BackingField
	WeakReference_t1334886716 * ___U3CWeakU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CTargetHashCodeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WeakObjectWrapper_t827463650, ___U3CTargetHashCodeU3Ek__BackingField_0)); }
	inline int32_t get_U3CTargetHashCodeU3Ek__BackingField_0() const { return ___U3CTargetHashCodeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CTargetHashCodeU3Ek__BackingField_0() { return &___U3CTargetHashCodeU3Ek__BackingField_0; }
	inline void set_U3CTargetHashCodeU3Ek__BackingField_0(int32_t value)
	{
		___U3CTargetHashCodeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CWeakU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WeakObjectWrapper_t827463650, ___U3CWeakU3Ek__BackingField_1)); }
	inline WeakReference_t1334886716 * get_U3CWeakU3Ek__BackingField_1() const { return ___U3CWeakU3Ek__BackingField_1; }
	inline WeakReference_t1334886716 ** get_address_of_U3CWeakU3Ek__BackingField_1() { return &___U3CWeakU3Ek__BackingField_1; }
	inline void set_U3CWeakU3Ek__BackingField_1(WeakReference_t1334886716 * value)
	{
		___U3CWeakU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWeakU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEAKOBJECTWRAPPER_T827463650_H
#ifndef EQUALITYCOMPARER_1_T3292203282_H
#define EQUALITYCOMPARER_1_T3292203282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1<System.ComponentModel.WeakObjectWrapper>
struct  EqualityComparer_1_t3292203282  : public RuntimeObject
{
public:

public:
};

struct EqualityComparer_1_t3292203282_StaticFields
{
public:
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1::_default
	EqualityComparer_1_t3292203282 * ____default_0;

public:
	inline static int32_t get_offset_of__default_0() { return static_cast<int32_t>(offsetof(EqualityComparer_1_t3292203282_StaticFields, ____default_0)); }
	inline EqualityComparer_1_t3292203282 * get__default_0() const { return ____default_0; }
	inline EqualityComparer_1_t3292203282 ** get_address_of__default_0() { return &____default_0; }
	inline void set__default_0(EqualityComparer_1_t3292203282 * value)
	{
		____default_0 = value;
		Il2CppCodeGenWriteBarrier((&____default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EQUALITYCOMPARER_1_T3292203282_H
#ifndef TRACELISTENERCOLLECTION_T1347122889_H
#define TRACELISTENERCOLLECTION_T1347122889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceListenerCollection
struct  TraceListenerCollection_t1347122889  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Diagnostics.TraceListenerCollection::listeners
	ArrayList_t2718874744 * ___listeners_0;

public:
	inline static int32_t get_offset_of_listeners_0() { return static_cast<int32_t>(offsetof(TraceListenerCollection_t1347122889, ___listeners_0)); }
	inline ArrayList_t2718874744 * get_listeners_0() const { return ___listeners_0; }
	inline ArrayList_t2718874744 ** get_address_of_listeners_0() { return &___listeners_0; }
	inline void set_listeners_0(ArrayList_t2718874744 * value)
	{
		___listeners_0 = value;
		Il2CppCodeGenWriteBarrier((&___listeners_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACELISTENERCOLLECTION_T1347122889_H
#ifndef TRACEIMPL_T1390982446_H
#define TRACEIMPL_T1390982446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceImpl
struct  TraceImpl_t1390982446  : public RuntimeObject
{
public:

public:
};

struct TraceImpl_t1390982446_StaticFields
{
public:
	// System.Object System.Diagnostics.TraceImpl::initLock
	RuntimeObject * ___initLock_0;
	// System.Boolean System.Diagnostics.TraceImpl::autoFlush
	bool ___autoFlush_1;
	// System.Diagnostics.TraceListenerCollection System.Diagnostics.TraceImpl::listeners
	TraceListenerCollection_t1347122889 * ___listeners_4;
	// System.Diagnostics.CorrelationManager System.Diagnostics.TraceImpl::correlation_manager
	CorrelationManager_t2688752967 * ___correlation_manager_5;

public:
	inline static int32_t get_offset_of_initLock_0() { return static_cast<int32_t>(offsetof(TraceImpl_t1390982446_StaticFields, ___initLock_0)); }
	inline RuntimeObject * get_initLock_0() const { return ___initLock_0; }
	inline RuntimeObject ** get_address_of_initLock_0() { return &___initLock_0; }
	inline void set_initLock_0(RuntimeObject * value)
	{
		___initLock_0 = value;
		Il2CppCodeGenWriteBarrier((&___initLock_0), value);
	}

	inline static int32_t get_offset_of_autoFlush_1() { return static_cast<int32_t>(offsetof(TraceImpl_t1390982446_StaticFields, ___autoFlush_1)); }
	inline bool get_autoFlush_1() const { return ___autoFlush_1; }
	inline bool* get_address_of_autoFlush_1() { return &___autoFlush_1; }
	inline void set_autoFlush_1(bool value)
	{
		___autoFlush_1 = value;
	}

	inline static int32_t get_offset_of_listeners_4() { return static_cast<int32_t>(offsetof(TraceImpl_t1390982446_StaticFields, ___listeners_4)); }
	inline TraceListenerCollection_t1347122889 * get_listeners_4() const { return ___listeners_4; }
	inline TraceListenerCollection_t1347122889 ** get_address_of_listeners_4() { return &___listeners_4; }
	inline void set_listeners_4(TraceListenerCollection_t1347122889 * value)
	{
		___listeners_4 = value;
		Il2CppCodeGenWriteBarrier((&___listeners_4), value);
	}

	inline static int32_t get_offset_of_correlation_manager_5() { return static_cast<int32_t>(offsetof(TraceImpl_t1390982446_StaticFields, ___correlation_manager_5)); }
	inline CorrelationManager_t2688752967 * get_correlation_manager_5() const { return ___correlation_manager_5; }
	inline CorrelationManager_t2688752967 ** get_address_of_correlation_manager_5() { return &___correlation_manager_5; }
	inline void set_correlation_manager_5(CorrelationManager_t2688752967 * value)
	{
		___correlation_manager_5 = value;
		Il2CppCodeGenWriteBarrier((&___correlation_manager_5), value);
	}
};

struct TraceImpl_t1390982446_ThreadStaticFields
{
public:
	// System.Int32 System.Diagnostics.TraceImpl::indentLevel
	int32_t ___indentLevel_2;
	// System.Int32 System.Diagnostics.TraceImpl::indentSize
	int32_t ___indentSize_3;

public:
	inline static int32_t get_offset_of_indentLevel_2() { return static_cast<int32_t>(offsetof(TraceImpl_t1390982446_ThreadStaticFields, ___indentLevel_2)); }
	inline int32_t get_indentLevel_2() const { return ___indentLevel_2; }
	inline int32_t* get_address_of_indentLevel_2() { return &___indentLevel_2; }
	inline void set_indentLevel_2(int32_t value)
	{
		___indentLevel_2 = value;
	}

	inline static int32_t get_offset_of_indentSize_3() { return static_cast<int32_t>(offsetof(TraceImpl_t1390982446_ThreadStaticFields, ___indentSize_3)); }
	inline int32_t get_indentSize_3() const { return ___indentSize_3; }
	inline int32_t* get_address_of_indentSize_3() { return &___indentSize_3; }
	inline void set_indentSize_3(int32_t value)
	{
		___indentSize_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACEIMPL_T1390982446_H
#ifndef TRACEIMPLSETTINGS_T2527703222_H
#define TRACEIMPLSETTINGS_T2527703222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceImplSettings
struct  TraceImplSettings_t2527703222  : public RuntimeObject
{
public:
	// System.Boolean System.Diagnostics.TraceImplSettings::AutoFlush
	bool ___AutoFlush_0;
	// System.Int32 System.Diagnostics.TraceImplSettings::IndentLevel
	int32_t ___IndentLevel_1;
	// System.Int32 System.Diagnostics.TraceImplSettings::IndentSize
	int32_t ___IndentSize_2;
	// System.Diagnostics.TraceListenerCollection System.Diagnostics.TraceImplSettings::Listeners
	TraceListenerCollection_t1347122889 * ___Listeners_3;

public:
	inline static int32_t get_offset_of_AutoFlush_0() { return static_cast<int32_t>(offsetof(TraceImplSettings_t2527703222, ___AutoFlush_0)); }
	inline bool get_AutoFlush_0() const { return ___AutoFlush_0; }
	inline bool* get_address_of_AutoFlush_0() { return &___AutoFlush_0; }
	inline void set_AutoFlush_0(bool value)
	{
		___AutoFlush_0 = value;
	}

	inline static int32_t get_offset_of_IndentLevel_1() { return static_cast<int32_t>(offsetof(TraceImplSettings_t2527703222, ___IndentLevel_1)); }
	inline int32_t get_IndentLevel_1() const { return ___IndentLevel_1; }
	inline int32_t* get_address_of_IndentLevel_1() { return &___IndentLevel_1; }
	inline void set_IndentLevel_1(int32_t value)
	{
		___IndentLevel_1 = value;
	}

	inline static int32_t get_offset_of_IndentSize_2() { return static_cast<int32_t>(offsetof(TraceImplSettings_t2527703222, ___IndentSize_2)); }
	inline int32_t get_IndentSize_2() const { return ___IndentSize_2; }
	inline int32_t* get_address_of_IndentSize_2() { return &___IndentSize_2; }
	inline void set_IndentSize_2(int32_t value)
	{
		___IndentSize_2 = value;
	}

	inline static int32_t get_offset_of_Listeners_3() { return static_cast<int32_t>(offsetof(TraceImplSettings_t2527703222, ___Listeners_3)); }
	inline TraceListenerCollection_t1347122889 * get_Listeners_3() const { return ___Listeners_3; }
	inline TraceListenerCollection_t1347122889 ** get_address_of_Listeners_3() { return &___Listeners_3; }
	inline void set_Listeners_3(TraceListenerCollection_t1347122889 * value)
	{
		___Listeners_3 = value;
		Il2CppCodeGenWriteBarrier((&___Listeners_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACEIMPLSETTINGS_T2527703222_H
#ifndef TRACEFILTER_T4153521180_H
#define TRACEFILTER_T4153521180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceFilter
struct  TraceFilter_t4153521180  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACEFILTER_T4153521180_H
#ifndef URIPARSER_T3890150400_H
#define URIPARSER_T3890150400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriParser
struct  UriParser_t3890150400  : public RuntimeObject
{
public:
	// System.String System.UriParser::scheme_name
	String_t* ___scheme_name_2;
	// System.Int32 System.UriParser::default_port
	int32_t ___default_port_3;

public:
	inline static int32_t get_offset_of_scheme_name_2() { return static_cast<int32_t>(offsetof(UriParser_t3890150400, ___scheme_name_2)); }
	inline String_t* get_scheme_name_2() const { return ___scheme_name_2; }
	inline String_t** get_address_of_scheme_name_2() { return &___scheme_name_2; }
	inline void set_scheme_name_2(String_t* value)
	{
		___scheme_name_2 = value;
		Il2CppCodeGenWriteBarrier((&___scheme_name_2), value);
	}

	inline static int32_t get_offset_of_default_port_3() { return static_cast<int32_t>(offsetof(UriParser_t3890150400, ___default_port_3)); }
	inline int32_t get_default_port_3() const { return ___default_port_3; }
	inline int32_t* get_address_of_default_port_3() { return &___default_port_3; }
	inline void set_default_port_3(int32_t value)
	{
		___default_port_3 = value;
	}
};

struct UriParser_t3890150400_StaticFields
{
public:
	// System.Object System.UriParser::lock_object
	RuntimeObject * ___lock_object_0;
	// System.Collections.Hashtable System.UriParser::table
	Hashtable_t1853889766 * ___table_1;
	// System.Text.RegularExpressions.Regex System.UriParser::uri_regex
	Regex_t3657309853 * ___uri_regex_4;
	// System.Text.RegularExpressions.Regex System.UriParser::auth_regex
	Regex_t3657309853 * ___auth_regex_5;

public:
	inline static int32_t get_offset_of_lock_object_0() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___lock_object_0)); }
	inline RuntimeObject * get_lock_object_0() const { return ___lock_object_0; }
	inline RuntimeObject ** get_address_of_lock_object_0() { return &___lock_object_0; }
	inline void set_lock_object_0(RuntimeObject * value)
	{
		___lock_object_0 = value;
		Il2CppCodeGenWriteBarrier((&___lock_object_0), value);
	}

	inline static int32_t get_offset_of_table_1() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___table_1)); }
	inline Hashtable_t1853889766 * get_table_1() const { return ___table_1; }
	inline Hashtable_t1853889766 ** get_address_of_table_1() { return &___table_1; }
	inline void set_table_1(Hashtable_t1853889766 * value)
	{
		___table_1 = value;
		Il2CppCodeGenWriteBarrier((&___table_1), value);
	}

	inline static int32_t get_offset_of_uri_regex_4() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___uri_regex_4)); }
	inline Regex_t3657309853 * get_uri_regex_4() const { return ___uri_regex_4; }
	inline Regex_t3657309853 ** get_address_of_uri_regex_4() { return &___uri_regex_4; }
	inline void set_uri_regex_4(Regex_t3657309853 * value)
	{
		___uri_regex_4 = value;
		Il2CppCodeGenWriteBarrier((&___uri_regex_4), value);
	}

	inline static int32_t get_offset_of_auth_regex_5() { return static_cast<int32_t>(offsetof(UriParser_t3890150400_StaticFields, ___auth_regex_5)); }
	inline Regex_t3657309853 * get_auth_regex_5() const { return ___auth_regex_5; }
	inline Regex_t3657309853 ** get_address_of_auth_regex_5() { return &___auth_regex_5; }
	inline void set_auth_regex_5(Regex_t3657309853 * value)
	{
		___auth_regex_5 = value;
		Il2CppCodeGenWriteBarrier((&___auth_regex_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIPARSER_T3890150400_H
#ifndef EVENTARGS_T3591816995_H
#define EVENTARGS_T3591816995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3591816995  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3591816995_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3591816995 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3591816995_StaticFields, ___Empty_0)); }
	inline EventArgs_t3591816995 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3591816995 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3591816995 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3591816995_H
#ifndef TRACE_T2517910945_H
#define TRACE_T2517910945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.Trace
struct  Trace_t2517910945  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACE_T2517910945_H
#ifndef STOPWATCH_T305734070_H
#define STOPWATCH_T305734070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.Stopwatch
struct  Stopwatch_t305734070  : public RuntimeObject
{
public:

public:
};

struct Stopwatch_t305734070_StaticFields
{
public:
	// System.Int64 System.Diagnostics.Stopwatch::Frequency
	int64_t ___Frequency_0;
	// System.Boolean System.Diagnostics.Stopwatch::IsHighResolution
	bool ___IsHighResolution_1;

public:
	inline static int32_t get_offset_of_Frequency_0() { return static_cast<int32_t>(offsetof(Stopwatch_t305734070_StaticFields, ___Frequency_0)); }
	inline int64_t get_Frequency_0() const { return ___Frequency_0; }
	inline int64_t* get_address_of_Frequency_0() { return &___Frequency_0; }
	inline void set_Frequency_0(int64_t value)
	{
		___Frequency_0 = value;
	}

	inline static int32_t get_offset_of_IsHighResolution_1() { return static_cast<int32_t>(offsetof(Stopwatch_t305734070_StaticFields, ___IsHighResolution_1)); }
	inline bool get_IsHighResolution_1() const { return ___IsHighResolution_1; }
	inline bool* get_address_of_IsHighResolution_1() { return &___IsHighResolution_1; }
	inline void set_IsHighResolution_1(bool value)
	{
		___IsHighResolution_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STOPWATCH_T305734070_H
#ifndef COOKIECONTAINER_T2331592909_H
#define COOKIECONTAINER_T2331592909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieContainer
struct  CookieContainer_t2331592909  : public RuntimeObject
{
public:
	// System.Int32 System.Net.CookieContainer::capacity
	int32_t ___capacity_0;
	// System.Int32 System.Net.CookieContainer::perDomainCapacity
	int32_t ___perDomainCapacity_1;
	// System.Int32 System.Net.CookieContainer::maxCookieSize
	int32_t ___maxCookieSize_2;
	// System.Net.CookieCollection System.Net.CookieContainer::cookies
	CookieCollection_t3881042616 * ___cookies_3;

public:
	inline static int32_t get_offset_of_capacity_0() { return static_cast<int32_t>(offsetof(CookieContainer_t2331592909, ___capacity_0)); }
	inline int32_t get_capacity_0() const { return ___capacity_0; }
	inline int32_t* get_address_of_capacity_0() { return &___capacity_0; }
	inline void set_capacity_0(int32_t value)
	{
		___capacity_0 = value;
	}

	inline static int32_t get_offset_of_perDomainCapacity_1() { return static_cast<int32_t>(offsetof(CookieContainer_t2331592909, ___perDomainCapacity_1)); }
	inline int32_t get_perDomainCapacity_1() const { return ___perDomainCapacity_1; }
	inline int32_t* get_address_of_perDomainCapacity_1() { return &___perDomainCapacity_1; }
	inline void set_perDomainCapacity_1(int32_t value)
	{
		___perDomainCapacity_1 = value;
	}

	inline static int32_t get_offset_of_maxCookieSize_2() { return static_cast<int32_t>(offsetof(CookieContainer_t2331592909, ___maxCookieSize_2)); }
	inline int32_t get_maxCookieSize_2() const { return ___maxCookieSize_2; }
	inline int32_t* get_address_of_maxCookieSize_2() { return &___maxCookieSize_2; }
	inline void set_maxCookieSize_2(int32_t value)
	{
		___maxCookieSize_2 = value;
	}

	inline static int32_t get_offset_of_cookies_3() { return static_cast<int32_t>(offsetof(CookieContainer_t2331592909, ___cookies_3)); }
	inline CookieCollection_t3881042616 * get_cookies_3() const { return ___cookies_3; }
	inline CookieCollection_t3881042616 ** get_address_of_cookies_3() { return &___cookies_3; }
	inline void set_cookies_3(CookieCollection_t3881042616 * value)
	{
		___cookies_3 = value;
		Il2CppCodeGenWriteBarrier((&___cookies_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIECONTAINER_T2331592909_H
#ifndef CONNECTIONMANAGEMENTDATA_T2003128658_H
#define CONNECTIONMANAGEMENTDATA_T2003128658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.ConnectionManagementData
struct  ConnectionManagementData_t2003128658  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Net.Configuration.ConnectionManagementData::data
	Hashtable_t1853889766 * ___data_0;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(ConnectionManagementData_t2003128658, ___data_0)); }
	inline Hashtable_t1853889766 * get_data_0() const { return ___data_0; }
	inline Hashtable_t1853889766 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(Hashtable_t1853889766 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONMANAGEMENTDATA_T2003128658_H
#ifndef HANDLERSUTIL_T2371960855_H
#define HANDLERSUTIL_T2371960855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.HandlersUtil
struct  HandlersUtil_t2371960855  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDLERSUTIL_T2371960855_H
#ifndef CHUNK_T1455545707_H
#define CHUNK_T1455545707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ChunkStream/Chunk
struct  Chunk_t1455545707  : public RuntimeObject
{
public:
	// System.Byte[] System.Net.ChunkStream/Chunk::Bytes
	ByteU5BU5D_t4116647657* ___Bytes_0;
	// System.Int32 System.Net.ChunkStream/Chunk::Offset
	int32_t ___Offset_1;

public:
	inline static int32_t get_offset_of_Bytes_0() { return static_cast<int32_t>(offsetof(Chunk_t1455545707, ___Bytes_0)); }
	inline ByteU5BU5D_t4116647657* get_Bytes_0() const { return ___Bytes_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_Bytes_0() { return &___Bytes_0; }
	inline void set_Bytes_0(ByteU5BU5D_t4116647657* value)
	{
		___Bytes_0 = value;
		Il2CppCodeGenWriteBarrier((&___Bytes_0), value);
	}

	inline static int32_t get_offset_of_Offset_1() { return static_cast<int32_t>(offsetof(Chunk_t1455545707, ___Offset_1)); }
	inline int32_t get_Offset_1() const { return ___Offset_1; }
	inline int32_t* get_address_of_Offset_1() { return &___Offset_1; }
	inline void set_Offset_1(int32_t value)
	{
		___Offset_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHUNK_T1455545707_H
#ifndef READBUFFERSTATE_T2902666188_H
#define READBUFFERSTATE_T2902666188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ChunkedInputStream/ReadBufferState
struct  ReadBufferState_t2902666188  : public RuntimeObject
{
public:
	// System.Byte[] System.Net.ChunkedInputStream/ReadBufferState::Buffer
	ByteU5BU5D_t4116647657* ___Buffer_0;
	// System.Int32 System.Net.ChunkedInputStream/ReadBufferState::Offset
	int32_t ___Offset_1;
	// System.Int32 System.Net.ChunkedInputStream/ReadBufferState::Count
	int32_t ___Count_2;
	// System.Int32 System.Net.ChunkedInputStream/ReadBufferState::InitialCount
	int32_t ___InitialCount_3;
	// System.Net.HttpStreamAsyncResult System.Net.ChunkedInputStream/ReadBufferState::Ares
	HttpStreamAsyncResult_t1178010344 * ___Ares_4;

public:
	inline static int32_t get_offset_of_Buffer_0() { return static_cast<int32_t>(offsetof(ReadBufferState_t2902666188, ___Buffer_0)); }
	inline ByteU5BU5D_t4116647657* get_Buffer_0() const { return ___Buffer_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_Buffer_0() { return &___Buffer_0; }
	inline void set_Buffer_0(ByteU5BU5D_t4116647657* value)
	{
		___Buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___Buffer_0), value);
	}

	inline static int32_t get_offset_of_Offset_1() { return static_cast<int32_t>(offsetof(ReadBufferState_t2902666188, ___Offset_1)); }
	inline int32_t get_Offset_1() const { return ___Offset_1; }
	inline int32_t* get_address_of_Offset_1() { return &___Offset_1; }
	inline void set_Offset_1(int32_t value)
	{
		___Offset_1 = value;
	}

	inline static int32_t get_offset_of_Count_2() { return static_cast<int32_t>(offsetof(ReadBufferState_t2902666188, ___Count_2)); }
	inline int32_t get_Count_2() const { return ___Count_2; }
	inline int32_t* get_address_of_Count_2() { return &___Count_2; }
	inline void set_Count_2(int32_t value)
	{
		___Count_2 = value;
	}

	inline static int32_t get_offset_of_InitialCount_3() { return static_cast<int32_t>(offsetof(ReadBufferState_t2902666188, ___InitialCount_3)); }
	inline int32_t get_InitialCount_3() const { return ___InitialCount_3; }
	inline int32_t* get_address_of_InitialCount_3() { return &___InitialCount_3; }
	inline void set_InitialCount_3(int32_t value)
	{
		___InitialCount_3 = value;
	}

	inline static int32_t get_offset_of_Ares_4() { return static_cast<int32_t>(offsetof(ReadBufferState_t2902666188, ___Ares_4)); }
	inline HttpStreamAsyncResult_t1178010344 * get_Ares_4() const { return ___Ares_4; }
	inline HttpStreamAsyncResult_t1178010344 ** get_address_of_Ares_4() { return &___Ares_4; }
	inline void set_Ares_4(HttpStreamAsyncResult_t1178010344 * value)
	{
		___Ares_4 = value;
		Il2CppCodeGenWriteBarrier((&___Ares_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READBUFFERSTATE_T2902666188_H
#ifndef AUTHORIZATION_T542416582_H
#define AUTHORIZATION_T542416582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Authorization
struct  Authorization_t542416582  : public RuntimeObject
{
public:
	// System.String System.Net.Authorization::token
	String_t* ___token_0;
	// System.Boolean System.Net.Authorization::complete
	bool ___complete_1;
	// System.Net.IAuthenticationModule System.Net.Authorization::module
	RuntimeObject* ___module_2;

public:
	inline static int32_t get_offset_of_token_0() { return static_cast<int32_t>(offsetof(Authorization_t542416582, ___token_0)); }
	inline String_t* get_token_0() const { return ___token_0; }
	inline String_t** get_address_of_token_0() { return &___token_0; }
	inline void set_token_0(String_t* value)
	{
		___token_0 = value;
		Il2CppCodeGenWriteBarrier((&___token_0), value);
	}

	inline static int32_t get_offset_of_complete_1() { return static_cast<int32_t>(offsetof(Authorization_t542416582, ___complete_1)); }
	inline bool get_complete_1() const { return ___complete_1; }
	inline bool* get_address_of_complete_1() { return &___complete_1; }
	inline void set_complete_1(bool value)
	{
		___complete_1 = value;
	}

	inline static int32_t get_offset_of_module_2() { return static_cast<int32_t>(offsetof(Authorization_t542416582, ___module_2)); }
	inline RuntimeObject* get_module_2() const { return ___module_2; }
	inline RuntimeObject** get_address_of_module_2() { return &___module_2; }
	inline void set_module_2(RuntimeObject* value)
	{
		___module_2 = value;
		Il2CppCodeGenWriteBarrier((&___module_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHORIZATION_T542416582_H
#ifndef AUTHENTICATIONMANAGER_T2084001809_H
#define AUTHENTICATIONMANAGER_T2084001809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.AuthenticationManager
struct  AuthenticationManager_t2084001809  : public RuntimeObject
{
public:

public:
};

struct AuthenticationManager_t2084001809_StaticFields
{
public:
	// System.Collections.ArrayList System.Net.AuthenticationManager::modules
	ArrayList_t2718874744 * ___modules_0;
	// System.Object System.Net.AuthenticationManager::locker
	RuntimeObject * ___locker_1;
	// System.Net.ICredentialPolicy System.Net.AuthenticationManager::credential_policy
	RuntimeObject* ___credential_policy_2;

public:
	inline static int32_t get_offset_of_modules_0() { return static_cast<int32_t>(offsetof(AuthenticationManager_t2084001809_StaticFields, ___modules_0)); }
	inline ArrayList_t2718874744 * get_modules_0() const { return ___modules_0; }
	inline ArrayList_t2718874744 ** get_address_of_modules_0() { return &___modules_0; }
	inline void set_modules_0(ArrayList_t2718874744 * value)
	{
		___modules_0 = value;
		Il2CppCodeGenWriteBarrier((&___modules_0), value);
	}

	inline static int32_t get_offset_of_locker_1() { return static_cast<int32_t>(offsetof(AuthenticationManager_t2084001809_StaticFields, ___locker_1)); }
	inline RuntimeObject * get_locker_1() const { return ___locker_1; }
	inline RuntimeObject ** get_address_of_locker_1() { return &___locker_1; }
	inline void set_locker_1(RuntimeObject * value)
	{
		___locker_1 = value;
		Il2CppCodeGenWriteBarrier((&___locker_1), value);
	}

	inline static int32_t get_offset_of_credential_policy_2() { return static_cast<int32_t>(offsetof(AuthenticationManager_t2084001809_StaticFields, ___credential_policy_2)); }
	inline RuntimeObject* get_credential_policy_2() const { return ___credential_policy_2; }
	inline RuntimeObject** get_address_of_credential_policy_2() { return &___credential_policy_2; }
	inline void set_credential_policy_2(RuntimeObject* value)
	{
		___credential_policy_2 = value;
		Il2CppCodeGenWriteBarrier((&___credential_policy_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONMANAGER_T2084001809_H
#ifndef NETCONFIGURATIONHANDLER_T3348259332_H
#define NETCONFIGURATIONHANDLER_T3348259332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.NetConfigurationHandler
struct  NetConfigurationHandler_t3348259332  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETCONFIGURATIONHANDLER_T3348259332_H
#ifndef MONOIO_T2601436416_H
#define MONOIO_T2601436416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MonoIO
struct  MonoIO_t2601436416  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOIO_T2601436416_H
#ifndef WEBREQUESTMODULEHANDLER_T914399239_H
#define WEBREQUESTMODULEHANDLER_T914399239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.WebRequestModuleHandler
struct  WebRequestModuleHandler_t914399239  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTMODULEHANDLER_T914399239_H
#ifndef COOKIECOLLECTION_T3881042616_H
#define COOKIECOLLECTION_T3881042616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieCollection
struct  CookieCollection_t3881042616  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Net.Cookie> System.Net.CookieCollection::list
	List_1_t2465948139 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(CookieCollection_t3881042616, ___list_0)); }
	inline List_1_t2465948139 * get_list_0() const { return ___list_0; }
	inline List_1_t2465948139 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t2465948139 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

struct CookieCollection_t3881042616_StaticFields
{
public:
	// System.Net.CookieCollection/CookieCollectionComparer System.Net.CookieCollection::Comparer
	CookieCollectionComparer_t1373927847 * ___Comparer_1;

public:
	inline static int32_t get_offset_of_Comparer_1() { return static_cast<int32_t>(offsetof(CookieCollection_t3881042616_StaticFields, ___Comparer_1)); }
	inline CookieCollectionComparer_t1373927847 * get_Comparer_1() const { return ___Comparer_1; }
	inline CookieCollectionComparer_t1373927847 ** get_address_of_Comparer_1() { return &___Comparer_1; }
	inline void set_Comparer_1(CookieCollectionComparer_t1373927847 * value)
	{
		___Comparer_1 = value;
		Il2CppCodeGenWriteBarrier((&___Comparer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIECOLLECTION_T3881042616_H
#ifndef COOKIECOLLECTIONCOMPARER_T1373927847_H
#define COOKIECOLLECTIONCOMPARER_T1373927847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieCollection/CookieCollectionComparer
struct  CookieCollectionComparer_t1373927847  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIECOLLECTIONCOMPARER_T1373927847_H
#ifndef MARSHALBYREFOBJECT_T2760389100_H
#define MARSHALBYREFOBJECT_T2760389100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t2760389100  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t2342208608 * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t2760389100, ____identity_0)); }
	inline ServerIdentity_t2342208608 * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t2342208608 ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t2342208608 * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALBYREFOBJECT_T2760389100_H
#ifndef DIAGNOSTICSCONFIGURATION_T1159239597_H
#define DIAGNOSTICSCONFIGURATION_T1159239597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.DiagnosticsConfiguration
struct  DiagnosticsConfiguration_t1159239597  : public RuntimeObject
{
public:

public:
};

struct DiagnosticsConfiguration_t1159239597_StaticFields
{
public:
	// System.Object System.Diagnostics.DiagnosticsConfiguration::settings
	RuntimeObject * ___settings_0;

public:
	inline static int32_t get_offset_of_settings_0() { return static_cast<int32_t>(offsetof(DiagnosticsConfiguration_t1159239597_StaticFields, ___settings_0)); }
	inline RuntimeObject * get_settings_0() const { return ___settings_0; }
	inline RuntimeObject ** get_address_of_settings_0() { return &___settings_0; }
	inline void set_settings_0(RuntimeObject * value)
	{
		___settings_0 = value;
		Il2CppCodeGenWriteBarrier((&___settings_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIAGNOSTICSCONFIGURATION_T1159239597_H
#ifndef DIAGNOSTICSCONFIGURATIONHANDLER_T2074484573_H
#define DIAGNOSTICSCONFIGURATIONHANDLER_T2074484573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.DiagnosticsConfigurationHandler
struct  DiagnosticsConfigurationHandler_t2074484573  : public RuntimeObject
{
public:
	// System.Diagnostics.TraceImplSettings System.Diagnostics.DiagnosticsConfigurationHandler::configValues
	TraceImplSettings_t2527703222 * ___configValues_0;
	// System.Collections.IDictionary System.Diagnostics.DiagnosticsConfigurationHandler::elementHandlers
	RuntimeObject* ___elementHandlers_1;

public:
	inline static int32_t get_offset_of_configValues_0() { return static_cast<int32_t>(offsetof(DiagnosticsConfigurationHandler_t2074484573, ___configValues_0)); }
	inline TraceImplSettings_t2527703222 * get_configValues_0() const { return ___configValues_0; }
	inline TraceImplSettings_t2527703222 ** get_address_of_configValues_0() { return &___configValues_0; }
	inline void set_configValues_0(TraceImplSettings_t2527703222 * value)
	{
		___configValues_0 = value;
		Il2CppCodeGenWriteBarrier((&___configValues_0), value);
	}

	inline static int32_t get_offset_of_elementHandlers_1() { return static_cast<int32_t>(offsetof(DiagnosticsConfigurationHandler_t2074484573, ___elementHandlers_1)); }
	inline RuntimeObject* get_elementHandlers_1() const { return ___elementHandlers_1; }
	inline RuntimeObject** get_address_of_elementHandlers_1() { return &___elementHandlers_1; }
	inline void set_elementHandlers_1(RuntimeObject* value)
	{
		___elementHandlers_1 = value;
		Il2CppCodeGenWriteBarrier((&___elementHandlers_1), value);
	}
};

struct DiagnosticsConfigurationHandler_t2074484573_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Diagnostics.DiagnosticsConfigurationHandler::<>f__switch$map5
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map5_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Diagnostics.DiagnosticsConfigurationHandler::<>f__switch$map6
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map6_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Diagnostics.DiagnosticsConfigurationHandler::<>f__switch$map7
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map7_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map5_2() { return static_cast<int32_t>(offsetof(DiagnosticsConfigurationHandler_t2074484573_StaticFields, ___U3CU3Ef__switchU24map5_2)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map5_2() const { return ___U3CU3Ef__switchU24map5_2; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map5_2() { return &___U3CU3Ef__switchU24map5_2; }
	inline void set_U3CU3Ef__switchU24map5_2(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map5_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map5_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map6_3() { return static_cast<int32_t>(offsetof(DiagnosticsConfigurationHandler_t2074484573_StaticFields, ___U3CU3Ef__switchU24map6_3)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map6_3() const { return ___U3CU3Ef__switchU24map6_3; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map6_3() { return &___U3CU3Ef__switchU24map6_3; }
	inline void set_U3CU3Ef__switchU24map6_3(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map6_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map6_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map7_4() { return static_cast<int32_t>(offsetof(DiagnosticsConfigurationHandler_t2074484573_StaticFields, ___U3CU3Ef__switchU24map7_4)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map7_4() const { return ___U3CU3Ef__switchU24map7_4; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map7_4() { return &___U3CU3Ef__switchU24map7_4; }
	inline void set_U3CU3Ef__switchU24map7_4(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map7_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map7_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIAGNOSTICSCONFIGURATIONHANDLER_T2074484573_H
#ifndef DEFAULTCONFIG_T1013547162_H
#define DEFAULTCONFIG_T1013547162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.DefaultConfig
struct  DefaultConfig_t1013547162  : public RuntimeObject
{
public:

public:
};

struct DefaultConfig_t1013547162_StaticFields
{
public:
	// System.Configuration.DefaultConfig System.Configuration.DefaultConfig::instance
	DefaultConfig_t1013547162 * ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(DefaultConfig_t1013547162_StaticFields, ___instance_0)); }
	inline DefaultConfig_t1013547162 * get_instance_0() const { return ___instance_0; }
	inline DefaultConfig_t1013547162 ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(DefaultConfig_t1013547162 * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCONFIG_T1013547162_H
#ifndef CONFIGURATIONSETTINGS_T822951835_H
#define CONFIGURATIONSETTINGS_T822951835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationSettings
struct  ConfigurationSettings_t822951835  : public RuntimeObject
{
public:

public:
};

struct ConfigurationSettings_t822951835_StaticFields
{
public:
	// System.Configuration.IConfigurationSystem System.Configuration.ConfigurationSettings::config
	RuntimeObject* ___config_0;
	// System.Object System.Configuration.ConfigurationSettings::lockobj
	RuntimeObject * ___lockobj_1;

public:
	inline static int32_t get_offset_of_config_0() { return static_cast<int32_t>(offsetof(ConfigurationSettings_t822951835_StaticFields, ___config_0)); }
	inline RuntimeObject* get_config_0() const { return ___config_0; }
	inline RuntimeObject** get_address_of_config_0() { return &___config_0; }
	inline void set_config_0(RuntimeObject* value)
	{
		___config_0 = value;
		Il2CppCodeGenWriteBarrier((&___config_0), value);
	}

	inline static int32_t get_offset_of_lockobj_1() { return static_cast<int32_t>(offsetof(ConfigurationSettings_t822951835_StaticFields, ___lockobj_1)); }
	inline RuntimeObject * get_lockobj_1() const { return ___lockobj_1; }
	inline RuntimeObject ** get_address_of_lockobj_1() { return &___lockobj_1; }
	inline void set_lockobj_1(RuntimeObject * value)
	{
		___lockobj_1 = value;
		Il2CppCodeGenWriteBarrier((&___lockobj_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONSETTINGS_T822951835_H
#ifndef CONFIGURATIONSECTIONGROUP_T4179402520_H
#define CONFIGURATIONSECTIONGROUP_T4179402520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationSectionGroup
struct  ConfigurationSectionGroup_t4179402520  : public RuntimeObject
{
public:
	// System.Configuration.ConfigurationSectionCollection System.Configuration.ConfigurationSectionGroup::sections
	ConfigurationSectionCollection_t2786897858 * ___sections_0;
	// System.Configuration.ConfigurationSectionGroupCollection System.Configuration.ConfigurationSectionGroup::groups
	ConfigurationSectionGroupCollection_t1151641153 * ___groups_1;
	// System.Configuration.Configuration System.Configuration.ConfigurationSectionGroup::config
	Configuration_t2529364143 * ___config_2;
	// System.Configuration.SectionGroupInfo System.Configuration.ConfigurationSectionGroup::group
	SectionGroupInfo_t687896682 * ___group_3;
	// System.Boolean System.Configuration.ConfigurationSectionGroup::initialized
	bool ___initialized_4;

public:
	inline static int32_t get_offset_of_sections_0() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroup_t4179402520, ___sections_0)); }
	inline ConfigurationSectionCollection_t2786897858 * get_sections_0() const { return ___sections_0; }
	inline ConfigurationSectionCollection_t2786897858 ** get_address_of_sections_0() { return &___sections_0; }
	inline void set_sections_0(ConfigurationSectionCollection_t2786897858 * value)
	{
		___sections_0 = value;
		Il2CppCodeGenWriteBarrier((&___sections_0), value);
	}

	inline static int32_t get_offset_of_groups_1() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroup_t4179402520, ___groups_1)); }
	inline ConfigurationSectionGroupCollection_t1151641153 * get_groups_1() const { return ___groups_1; }
	inline ConfigurationSectionGroupCollection_t1151641153 ** get_address_of_groups_1() { return &___groups_1; }
	inline void set_groups_1(ConfigurationSectionGroupCollection_t1151641153 * value)
	{
		___groups_1 = value;
		Il2CppCodeGenWriteBarrier((&___groups_1), value);
	}

	inline static int32_t get_offset_of_config_2() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroup_t4179402520, ___config_2)); }
	inline Configuration_t2529364143 * get_config_2() const { return ___config_2; }
	inline Configuration_t2529364143 ** get_address_of_config_2() { return &___config_2; }
	inline void set_config_2(Configuration_t2529364143 * value)
	{
		___config_2 = value;
		Il2CppCodeGenWriteBarrier((&___config_2), value);
	}

	inline static int32_t get_offset_of_group_3() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroup_t4179402520, ___group_3)); }
	inline SectionGroupInfo_t687896682 * get_group_3() const { return ___group_3; }
	inline SectionGroupInfo_t687896682 ** get_address_of_group_3() { return &___group_3; }
	inline void set_group_3(SectionGroupInfo_t687896682 * value)
	{
		___group_3 = value;
		Il2CppCodeGenWriteBarrier((&___group_3), value);
	}

	inline static int32_t get_offset_of_initialized_4() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroup_t4179402520, ___initialized_4)); }
	inline bool get_initialized_4() const { return ___initialized_4; }
	inline bool* get_address_of_initialized_4() { return &___initialized_4; }
	inline void set_initialized_4(bool value)
	{
		___initialized_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONSECTIONGROUP_T4179402520_H
#ifndef CONFIGURATIONELEMENT_T3318566633_H
#define CONFIGURATIONELEMENT_T3318566633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationElement
struct  ConfigurationElement_t3318566633  : public RuntimeObject
{
public:
	// System.String System.Configuration.ConfigurationElement::rawXml
	String_t* ___rawXml_0;
	// System.Boolean System.Configuration.ConfigurationElement::modified
	bool ___modified_1;
	// System.Configuration.ElementMap System.Configuration.ConfigurationElement::map
	ElementMap_t2160633803 * ___map_2;
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.ConfigurationElement::keyProps
	ConfigurationPropertyCollection_t2852175726 * ___keyProps_3;
	// System.Configuration.ConfigurationElementCollection System.Configuration.ConfigurationElement::defaultCollection
	ConfigurationElementCollection_t446763386 * ___defaultCollection_4;
	// System.Boolean System.Configuration.ConfigurationElement::readOnly
	bool ___readOnly_5;
	// System.Configuration.ElementInformation System.Configuration.ConfigurationElement::elementInfo
	ElementInformation_t2651568025 * ___elementInfo_6;
	// System.Configuration.Configuration System.Configuration.ConfigurationElement::_configuration
	Configuration_t2529364143 * ____configuration_7;
	// System.Configuration.ConfigurationLockCollection System.Configuration.ConfigurationElement::lockAllAttributesExcept
	ConfigurationLockCollection_t4066281341 * ___lockAllAttributesExcept_8;
	// System.Configuration.ConfigurationLockCollection System.Configuration.ConfigurationElement::lockAllElementsExcept
	ConfigurationLockCollection_t4066281341 * ___lockAllElementsExcept_9;
	// System.Configuration.ConfigurationLockCollection System.Configuration.ConfigurationElement::lockAttributes
	ConfigurationLockCollection_t4066281341 * ___lockAttributes_10;
	// System.Configuration.ConfigurationLockCollection System.Configuration.ConfigurationElement::lockElements
	ConfigurationLockCollection_t4066281341 * ___lockElements_11;
	// System.Boolean System.Configuration.ConfigurationElement::lockItem
	bool ___lockItem_12;

public:
	inline static int32_t get_offset_of_rawXml_0() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ___rawXml_0)); }
	inline String_t* get_rawXml_0() const { return ___rawXml_0; }
	inline String_t** get_address_of_rawXml_0() { return &___rawXml_0; }
	inline void set_rawXml_0(String_t* value)
	{
		___rawXml_0 = value;
		Il2CppCodeGenWriteBarrier((&___rawXml_0), value);
	}

	inline static int32_t get_offset_of_modified_1() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ___modified_1)); }
	inline bool get_modified_1() const { return ___modified_1; }
	inline bool* get_address_of_modified_1() { return &___modified_1; }
	inline void set_modified_1(bool value)
	{
		___modified_1 = value;
	}

	inline static int32_t get_offset_of_map_2() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ___map_2)); }
	inline ElementMap_t2160633803 * get_map_2() const { return ___map_2; }
	inline ElementMap_t2160633803 ** get_address_of_map_2() { return &___map_2; }
	inline void set_map_2(ElementMap_t2160633803 * value)
	{
		___map_2 = value;
		Il2CppCodeGenWriteBarrier((&___map_2), value);
	}

	inline static int32_t get_offset_of_keyProps_3() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ___keyProps_3)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_keyProps_3() const { return ___keyProps_3; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_keyProps_3() { return &___keyProps_3; }
	inline void set_keyProps_3(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___keyProps_3 = value;
		Il2CppCodeGenWriteBarrier((&___keyProps_3), value);
	}

	inline static int32_t get_offset_of_defaultCollection_4() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ___defaultCollection_4)); }
	inline ConfigurationElementCollection_t446763386 * get_defaultCollection_4() const { return ___defaultCollection_4; }
	inline ConfigurationElementCollection_t446763386 ** get_address_of_defaultCollection_4() { return &___defaultCollection_4; }
	inline void set_defaultCollection_4(ConfigurationElementCollection_t446763386 * value)
	{
		___defaultCollection_4 = value;
		Il2CppCodeGenWriteBarrier((&___defaultCollection_4), value);
	}

	inline static int32_t get_offset_of_readOnly_5() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ___readOnly_5)); }
	inline bool get_readOnly_5() const { return ___readOnly_5; }
	inline bool* get_address_of_readOnly_5() { return &___readOnly_5; }
	inline void set_readOnly_5(bool value)
	{
		___readOnly_5 = value;
	}

	inline static int32_t get_offset_of_elementInfo_6() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ___elementInfo_6)); }
	inline ElementInformation_t2651568025 * get_elementInfo_6() const { return ___elementInfo_6; }
	inline ElementInformation_t2651568025 ** get_address_of_elementInfo_6() { return &___elementInfo_6; }
	inline void set_elementInfo_6(ElementInformation_t2651568025 * value)
	{
		___elementInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___elementInfo_6), value);
	}

	inline static int32_t get_offset_of__configuration_7() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ____configuration_7)); }
	inline Configuration_t2529364143 * get__configuration_7() const { return ____configuration_7; }
	inline Configuration_t2529364143 ** get_address_of__configuration_7() { return &____configuration_7; }
	inline void set__configuration_7(Configuration_t2529364143 * value)
	{
		____configuration_7 = value;
		Il2CppCodeGenWriteBarrier((&____configuration_7), value);
	}

	inline static int32_t get_offset_of_lockAllAttributesExcept_8() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ___lockAllAttributesExcept_8)); }
	inline ConfigurationLockCollection_t4066281341 * get_lockAllAttributesExcept_8() const { return ___lockAllAttributesExcept_8; }
	inline ConfigurationLockCollection_t4066281341 ** get_address_of_lockAllAttributesExcept_8() { return &___lockAllAttributesExcept_8; }
	inline void set_lockAllAttributesExcept_8(ConfigurationLockCollection_t4066281341 * value)
	{
		___lockAllAttributesExcept_8 = value;
		Il2CppCodeGenWriteBarrier((&___lockAllAttributesExcept_8), value);
	}

	inline static int32_t get_offset_of_lockAllElementsExcept_9() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ___lockAllElementsExcept_9)); }
	inline ConfigurationLockCollection_t4066281341 * get_lockAllElementsExcept_9() const { return ___lockAllElementsExcept_9; }
	inline ConfigurationLockCollection_t4066281341 ** get_address_of_lockAllElementsExcept_9() { return &___lockAllElementsExcept_9; }
	inline void set_lockAllElementsExcept_9(ConfigurationLockCollection_t4066281341 * value)
	{
		___lockAllElementsExcept_9 = value;
		Il2CppCodeGenWriteBarrier((&___lockAllElementsExcept_9), value);
	}

	inline static int32_t get_offset_of_lockAttributes_10() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ___lockAttributes_10)); }
	inline ConfigurationLockCollection_t4066281341 * get_lockAttributes_10() const { return ___lockAttributes_10; }
	inline ConfigurationLockCollection_t4066281341 ** get_address_of_lockAttributes_10() { return &___lockAttributes_10; }
	inline void set_lockAttributes_10(ConfigurationLockCollection_t4066281341 * value)
	{
		___lockAttributes_10 = value;
		Il2CppCodeGenWriteBarrier((&___lockAttributes_10), value);
	}

	inline static int32_t get_offset_of_lockElements_11() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ___lockElements_11)); }
	inline ConfigurationLockCollection_t4066281341 * get_lockElements_11() const { return ___lockElements_11; }
	inline ConfigurationLockCollection_t4066281341 ** get_address_of_lockElements_11() { return &___lockElements_11; }
	inline void set_lockElements_11(ConfigurationLockCollection_t4066281341 * value)
	{
		___lockElements_11 = value;
		Il2CppCodeGenWriteBarrier((&___lockElements_11), value);
	}

	inline static int32_t get_offset_of_lockItem_12() { return static_cast<int32_t>(offsetof(ConfigurationElement_t3318566633, ___lockItem_12)); }
	inline bool get_lockItem_12() const { return ___lockItem_12; }
	inline bool* get_address_of_lockItem_12() { return &___lockItem_12; }
	inline void set_lockItem_12(bool value)
	{
		___lockItem_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONELEMENT_T3318566633_H
#ifndef CORRELATIONMANAGER_T2688752967_H
#define CORRELATIONMANAGER_T2688752967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.CorrelationManager
struct  CorrelationManager_t2688752967  : public RuntimeObject
{
public:
	// System.Collections.Stack System.Diagnostics.CorrelationManager::op_stack
	Stack_t2329662280 * ___op_stack_0;

public:
	inline static int32_t get_offset_of_op_stack_0() { return static_cast<int32_t>(offsetof(CorrelationManager_t2688752967, ___op_stack_0)); }
	inline Stack_t2329662280 * get_op_stack_0() const { return ___op_stack_0; }
	inline Stack_t2329662280 ** get_address_of_op_stack_0() { return &___op_stack_0; }
	inline void set_op_stack_0(Stack_t2329662280 * value)
	{
		___op_stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___op_stack_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORRELATIONMANAGER_T2688752967_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef READONLYCOLLECTIONBASE_T1836743899_H
#define READONLYCOLLECTIONBASE_T1836743899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ReadOnlyCollectionBase
struct  ReadOnlyCollectionBase_t1836743899  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.ReadOnlyCollectionBase::list
	ArrayList_t2718874744 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(ReadOnlyCollectionBase_t1836743899, ___list_0)); }
	inline ArrayList_t2718874744 * get_list_0() const { return ___list_0; }
	inline ArrayList_t2718874744 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t2718874744 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYCOLLECTIONBASE_T1836743899_H
#ifndef MODULEELEMENT_T3252950656_H
#define MODULEELEMENT_T3252950656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.ModuleElement
struct  ModuleElement_t3252950656  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct ModuleElement_t3252950656_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.ModuleElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.ModuleElement::typeProp
	ConfigurationProperty_t3590861854 * ___typeProp_14;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(ModuleElement_t3252950656_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier((&___properties_13), value);
	}

	inline static int32_t get_offset_of_typeProp_14() { return static_cast<int32_t>(offsetof(ModuleElement_t3252950656_StaticFields, ___typeProp_14)); }
	inline ConfigurationProperty_t3590861854 * get_typeProp_14() const { return ___typeProp_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_typeProp_14() { return &___typeProp_14; }
	inline void set_typeProp_14(ConfigurationProperty_t3590861854 * value)
	{
		___typeProp_14 = value;
		Il2CppCodeGenWriteBarrier((&___typeProp_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODULEELEMENT_T3252950656_H
#ifndef MAILSETTINGSSECTIONGROUP_T1796674833_H
#define MAILSETTINGSSECTIONGROUP_T1796674833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.MailSettingsSectionGroup
struct  MailSettingsSectionGroup_t1796674833  : public ConfigurationSectionGroup_t4179402520
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAILSETTINGSSECTIONGROUP_T1796674833_H
#ifndef NETSECTIONGROUP_T3270122580_H
#define NETSECTIONGROUP_T3270122580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.NetSectionGroup
struct  NetSectionGroup_t3270122580  : public ConfigurationSectionGroup_t4179402520
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETSECTIONGROUP_T3270122580_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef CONNECTIONMANAGEMENTELEMENT_T3857438253_H
#define CONNECTIONMANAGEMENTELEMENT_T3857438253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.ConnectionManagementElement
struct  ConnectionManagementElement_t3857438253  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct ConnectionManagementElement_t3857438253_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.ConnectionManagementElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.ConnectionManagementElement::addressProp
	ConfigurationProperty_t3590861854 * ___addressProp_14;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.ConnectionManagementElement::maxConnectionProp
	ConfigurationProperty_t3590861854 * ___maxConnectionProp_15;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(ConnectionManagementElement_t3857438253_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier((&___properties_13), value);
	}

	inline static int32_t get_offset_of_addressProp_14() { return static_cast<int32_t>(offsetof(ConnectionManagementElement_t3857438253_StaticFields, ___addressProp_14)); }
	inline ConfigurationProperty_t3590861854 * get_addressProp_14() const { return ___addressProp_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_addressProp_14() { return &___addressProp_14; }
	inline void set_addressProp_14(ConfigurationProperty_t3590861854 * value)
	{
		___addressProp_14 = value;
		Il2CppCodeGenWriteBarrier((&___addressProp_14), value);
	}

	inline static int32_t get_offset_of_maxConnectionProp_15() { return static_cast<int32_t>(offsetof(ConnectionManagementElement_t3857438253_StaticFields, ___maxConnectionProp_15)); }
	inline ConfigurationProperty_t3590861854 * get_maxConnectionProp_15() const { return ___maxConnectionProp_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_maxConnectionProp_15() { return &___maxConnectionProp_15; }
	inline void set_maxConnectionProp_15(ConfigurationProperty_t3590861854 * value)
	{
		___maxConnectionProp_15 = value;
		Il2CppCodeGenWriteBarrier((&___maxConnectionProp_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONMANAGEMENTELEMENT_T3857438253_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef FTPCACHEPOLICYELEMENT_T1580201543_H
#define FTPCACHEPOLICYELEMENT_T1580201543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.FtpCachePolicyElement
struct  FtpCachePolicyElement_t1580201543  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct FtpCachePolicyElement_t1580201543_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Net.Configuration.FtpCachePolicyElement::policyLevelProp
	ConfigurationProperty_t3590861854 * ___policyLevelProp_13;
	// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.FtpCachePolicyElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_14;

public:
	inline static int32_t get_offset_of_policyLevelProp_13() { return static_cast<int32_t>(offsetof(FtpCachePolicyElement_t1580201543_StaticFields, ___policyLevelProp_13)); }
	inline ConfigurationProperty_t3590861854 * get_policyLevelProp_13() const { return ___policyLevelProp_13; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_policyLevelProp_13() { return &___policyLevelProp_13; }
	inline void set_policyLevelProp_13(ConfigurationProperty_t3590861854 * value)
	{
		___policyLevelProp_13 = value;
		Il2CppCodeGenWriteBarrier((&___policyLevelProp_13), value);
	}

	inline static int32_t get_offset_of_properties_14() { return static_cast<int32_t>(offsetof(FtpCachePolicyElement_t1580201543_StaticFields, ___properties_14)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_14() const { return ___properties_14; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_14() { return &___properties_14; }
	inline void set_properties_14(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_14 = value;
		Il2CppCodeGenWriteBarrier((&___properties_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FTPCACHEPOLICYELEMENT_T1580201543_H
#ifndef HTTPCACHEPOLICYELEMENT_T409334445_H
#define HTTPCACHEPOLICYELEMENT_T409334445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.HttpCachePolicyElement
struct  HttpCachePolicyElement_t409334445  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct HttpCachePolicyElement_t409334445_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Net.Configuration.HttpCachePolicyElement::maximumAgeProp
	ConfigurationProperty_t3590861854 * ___maximumAgeProp_13;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.HttpCachePolicyElement::maximumStaleProp
	ConfigurationProperty_t3590861854 * ___maximumStaleProp_14;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.HttpCachePolicyElement::minimumFreshProp
	ConfigurationProperty_t3590861854 * ___minimumFreshProp_15;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.HttpCachePolicyElement::policyLevelProp
	ConfigurationProperty_t3590861854 * ___policyLevelProp_16;
	// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.HttpCachePolicyElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_17;

public:
	inline static int32_t get_offset_of_maximumAgeProp_13() { return static_cast<int32_t>(offsetof(HttpCachePolicyElement_t409334445_StaticFields, ___maximumAgeProp_13)); }
	inline ConfigurationProperty_t3590861854 * get_maximumAgeProp_13() const { return ___maximumAgeProp_13; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_maximumAgeProp_13() { return &___maximumAgeProp_13; }
	inline void set_maximumAgeProp_13(ConfigurationProperty_t3590861854 * value)
	{
		___maximumAgeProp_13 = value;
		Il2CppCodeGenWriteBarrier((&___maximumAgeProp_13), value);
	}

	inline static int32_t get_offset_of_maximumStaleProp_14() { return static_cast<int32_t>(offsetof(HttpCachePolicyElement_t409334445_StaticFields, ___maximumStaleProp_14)); }
	inline ConfigurationProperty_t3590861854 * get_maximumStaleProp_14() const { return ___maximumStaleProp_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_maximumStaleProp_14() { return &___maximumStaleProp_14; }
	inline void set_maximumStaleProp_14(ConfigurationProperty_t3590861854 * value)
	{
		___maximumStaleProp_14 = value;
		Il2CppCodeGenWriteBarrier((&___maximumStaleProp_14), value);
	}

	inline static int32_t get_offset_of_minimumFreshProp_15() { return static_cast<int32_t>(offsetof(HttpCachePolicyElement_t409334445_StaticFields, ___minimumFreshProp_15)); }
	inline ConfigurationProperty_t3590861854 * get_minimumFreshProp_15() const { return ___minimumFreshProp_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_minimumFreshProp_15() { return &___minimumFreshProp_15; }
	inline void set_minimumFreshProp_15(ConfigurationProperty_t3590861854 * value)
	{
		___minimumFreshProp_15 = value;
		Il2CppCodeGenWriteBarrier((&___minimumFreshProp_15), value);
	}

	inline static int32_t get_offset_of_policyLevelProp_16() { return static_cast<int32_t>(offsetof(HttpCachePolicyElement_t409334445_StaticFields, ___policyLevelProp_16)); }
	inline ConfigurationProperty_t3590861854 * get_policyLevelProp_16() const { return ___policyLevelProp_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_policyLevelProp_16() { return &___policyLevelProp_16; }
	inline void set_policyLevelProp_16(ConfigurationProperty_t3590861854 * value)
	{
		___policyLevelProp_16 = value;
		Il2CppCodeGenWriteBarrier((&___policyLevelProp_16), value);
	}

	inline static int32_t get_offset_of_properties_17() { return static_cast<int32_t>(offsetof(HttpCachePolicyElement_t409334445_StaticFields, ___properties_17)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_17() const { return ___properties_17; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_17() { return &___properties_17; }
	inline void set_properties_17(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_17 = value;
		Il2CppCodeGenWriteBarrier((&___properties_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCACHEPOLICYELEMENT_T409334445_H
#ifndef HTTPWEBREQUESTELEMENT_T2801692355_H
#define HTTPWEBREQUESTELEMENT_T2801692355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.HttpWebRequestElement
struct  HttpWebRequestElement_t2801692355  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct HttpWebRequestElement_t2801692355_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Net.Configuration.HttpWebRequestElement::maximumErrorResponseLengthProp
	ConfigurationProperty_t3590861854 * ___maximumErrorResponseLengthProp_13;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.HttpWebRequestElement::maximumResponseHeadersLengthProp
	ConfigurationProperty_t3590861854 * ___maximumResponseHeadersLengthProp_14;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.HttpWebRequestElement::maximumUnauthorizedUploadLengthProp
	ConfigurationProperty_t3590861854 * ___maximumUnauthorizedUploadLengthProp_15;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.HttpWebRequestElement::useUnsafeHeaderParsingProp
	ConfigurationProperty_t3590861854 * ___useUnsafeHeaderParsingProp_16;
	// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.HttpWebRequestElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_17;

public:
	inline static int32_t get_offset_of_maximumErrorResponseLengthProp_13() { return static_cast<int32_t>(offsetof(HttpWebRequestElement_t2801692355_StaticFields, ___maximumErrorResponseLengthProp_13)); }
	inline ConfigurationProperty_t3590861854 * get_maximumErrorResponseLengthProp_13() const { return ___maximumErrorResponseLengthProp_13; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_maximumErrorResponseLengthProp_13() { return &___maximumErrorResponseLengthProp_13; }
	inline void set_maximumErrorResponseLengthProp_13(ConfigurationProperty_t3590861854 * value)
	{
		___maximumErrorResponseLengthProp_13 = value;
		Il2CppCodeGenWriteBarrier((&___maximumErrorResponseLengthProp_13), value);
	}

	inline static int32_t get_offset_of_maximumResponseHeadersLengthProp_14() { return static_cast<int32_t>(offsetof(HttpWebRequestElement_t2801692355_StaticFields, ___maximumResponseHeadersLengthProp_14)); }
	inline ConfigurationProperty_t3590861854 * get_maximumResponseHeadersLengthProp_14() const { return ___maximumResponseHeadersLengthProp_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_maximumResponseHeadersLengthProp_14() { return &___maximumResponseHeadersLengthProp_14; }
	inline void set_maximumResponseHeadersLengthProp_14(ConfigurationProperty_t3590861854 * value)
	{
		___maximumResponseHeadersLengthProp_14 = value;
		Il2CppCodeGenWriteBarrier((&___maximumResponseHeadersLengthProp_14), value);
	}

	inline static int32_t get_offset_of_maximumUnauthorizedUploadLengthProp_15() { return static_cast<int32_t>(offsetof(HttpWebRequestElement_t2801692355_StaticFields, ___maximumUnauthorizedUploadLengthProp_15)); }
	inline ConfigurationProperty_t3590861854 * get_maximumUnauthorizedUploadLengthProp_15() const { return ___maximumUnauthorizedUploadLengthProp_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_maximumUnauthorizedUploadLengthProp_15() { return &___maximumUnauthorizedUploadLengthProp_15; }
	inline void set_maximumUnauthorizedUploadLengthProp_15(ConfigurationProperty_t3590861854 * value)
	{
		___maximumUnauthorizedUploadLengthProp_15 = value;
		Il2CppCodeGenWriteBarrier((&___maximumUnauthorizedUploadLengthProp_15), value);
	}

	inline static int32_t get_offset_of_useUnsafeHeaderParsingProp_16() { return static_cast<int32_t>(offsetof(HttpWebRequestElement_t2801692355_StaticFields, ___useUnsafeHeaderParsingProp_16)); }
	inline ConfigurationProperty_t3590861854 * get_useUnsafeHeaderParsingProp_16() const { return ___useUnsafeHeaderParsingProp_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_useUnsafeHeaderParsingProp_16() { return &___useUnsafeHeaderParsingProp_16; }
	inline void set_useUnsafeHeaderParsingProp_16(ConfigurationProperty_t3590861854 * value)
	{
		___useUnsafeHeaderParsingProp_16 = value;
		Il2CppCodeGenWriteBarrier((&___useUnsafeHeaderParsingProp_16), value);
	}

	inline static int32_t get_offset_of_properties_17() { return static_cast<int32_t>(offsetof(HttpWebRequestElement_t2801692355_StaticFields, ___properties_17)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_17() const { return ___properties_17; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_17() { return &___properties_17; }
	inline void set_properties_17(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_17 = value;
		Il2CppCodeGenWriteBarrier((&___properties_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPWEBREQUESTELEMENT_T2801692355_H
#ifndef IPV6ELEMENT_T180053194_H
#define IPV6ELEMENT_T180053194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.Ipv6Element
struct  Ipv6Element_t180053194  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct Ipv6Element_t180053194_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.Ipv6Element::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.Ipv6Element::enabledProp
	ConfigurationProperty_t3590861854 * ___enabledProp_14;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(Ipv6Element_t180053194_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier((&___properties_13), value);
	}

	inline static int32_t get_offset_of_enabledProp_14() { return static_cast<int32_t>(offsetof(Ipv6Element_t180053194_StaticFields, ___enabledProp_14)); }
	inline ConfigurationProperty_t3590861854 * get_enabledProp_14() const { return ___enabledProp_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_enabledProp_14() { return &___enabledProp_14; }
	inline void set_enabledProp_14(ConfigurationProperty_t3590861854 * value)
	{
		___enabledProp_14 = value;
		Il2CppCodeGenWriteBarrier((&___enabledProp_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPV6ELEMENT_T180053194_H
#ifndef PERFORMANCECOUNTERSELEMENT_T4093363992_H
#define PERFORMANCECOUNTERSELEMENT_T4093363992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.PerformanceCountersElement
struct  PerformanceCountersElement_t4093363992  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct PerformanceCountersElement_t4093363992_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Net.Configuration.PerformanceCountersElement::enabledProp
	ConfigurationProperty_t3590861854 * ___enabledProp_13;
	// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.PerformanceCountersElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_14;

public:
	inline static int32_t get_offset_of_enabledProp_13() { return static_cast<int32_t>(offsetof(PerformanceCountersElement_t4093363992_StaticFields, ___enabledProp_13)); }
	inline ConfigurationProperty_t3590861854 * get_enabledProp_13() const { return ___enabledProp_13; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_enabledProp_13() { return &___enabledProp_13; }
	inline void set_enabledProp_13(ConfigurationProperty_t3590861854 * value)
	{
		___enabledProp_13 = value;
		Il2CppCodeGenWriteBarrier((&___enabledProp_13), value);
	}

	inline static int32_t get_offset_of_properties_14() { return static_cast<int32_t>(offsetof(PerformanceCountersElement_t4093363992_StaticFields, ___properties_14)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_14() const { return ___properties_14; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_14() { return &___properties_14; }
	inline void set_properties_14(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_14 = value;
		Il2CppCodeGenWriteBarrier((&___properties_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PERFORMANCECOUNTERSELEMENT_T4093363992_H
#ifndef GCHANDLE_T3351438187_H
#define GCHANDLE_T3351438187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.GCHandle
struct  GCHandle_t3351438187 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t3351438187, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCHANDLE_T3351438187_H
#ifndef STREAM_T1273022909_H
#define STREAM_T1273022909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t1273022909  : public MarshalByRefObject_t2760389100
{
public:

public:
};

struct Stream_t1273022909_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t1273022909 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_t1273022909_StaticFields, ___Null_1)); }
	inline Stream_t1273022909 * get_Null_1() const { return ___Null_1; }
	inline Stream_t1273022909 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_t1273022909 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T1273022909_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COMPONENT_T3620823400_H
#define COMPONENT_T3620823400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Component
struct  Component_t3620823400  : public MarshalByRefObject_t2760389100
{
public:
	// System.ComponentModel.EventHandlerList System.ComponentModel.Component::event_handlers
	EventHandlerList_t1108123056 * ___event_handlers_1;
	// System.ComponentModel.ISite System.ComponentModel.Component::mySite
	RuntimeObject* ___mySite_2;
	// System.Object System.ComponentModel.Component::disposedEvent
	RuntimeObject * ___disposedEvent_3;

public:
	inline static int32_t get_offset_of_event_handlers_1() { return static_cast<int32_t>(offsetof(Component_t3620823400, ___event_handlers_1)); }
	inline EventHandlerList_t1108123056 * get_event_handlers_1() const { return ___event_handlers_1; }
	inline EventHandlerList_t1108123056 ** get_address_of_event_handlers_1() { return &___event_handlers_1; }
	inline void set_event_handlers_1(EventHandlerList_t1108123056 * value)
	{
		___event_handlers_1 = value;
		Il2CppCodeGenWriteBarrier((&___event_handlers_1), value);
	}

	inline static int32_t get_offset_of_mySite_2() { return static_cast<int32_t>(offsetof(Component_t3620823400, ___mySite_2)); }
	inline RuntimeObject* get_mySite_2() const { return ___mySite_2; }
	inline RuntimeObject** get_address_of_mySite_2() { return &___mySite_2; }
	inline void set_mySite_2(RuntimeObject* value)
	{
		___mySite_2 = value;
		Il2CppCodeGenWriteBarrier((&___mySite_2), value);
	}

	inline static int32_t get_offset_of_disposedEvent_3() { return static_cast<int32_t>(offsetof(Component_t3620823400, ___disposedEvent_3)); }
	inline RuntimeObject * get_disposedEvent_3() const { return ___disposedEvent_3; }
	inline RuntimeObject ** get_address_of_disposedEvent_3() { return &___disposedEvent_3; }
	inline void set_disposedEvent_3(RuntimeObject * value)
	{
		___disposedEvent_3 = value;
		Il2CppCodeGenWriteBarrier((&___disposedEvent_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3620823400_H
#ifndef DESCRIPTIONATTRIBUTE_T874390736_H
#define DESCRIPTIONATTRIBUTE_T874390736_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DescriptionAttribute
struct  DescriptionAttribute_t874390736  : public Attribute_t861562559
{
public:
	// System.String System.ComponentModel.DescriptionAttribute::desc
	String_t* ___desc_0;

public:
	inline static int32_t get_offset_of_desc_0() { return static_cast<int32_t>(offsetof(DescriptionAttribute_t874390736, ___desc_0)); }
	inline String_t* get_desc_0() const { return ___desc_0; }
	inline String_t** get_address_of_desc_0() { return &___desc_0; }
	inline void set_desc_0(String_t* value)
	{
		___desc_0 = value;
		Il2CppCodeGenWriteBarrier((&___desc_0), value);
	}
};

struct DescriptionAttribute_t874390736_StaticFields
{
public:
	// System.ComponentModel.DescriptionAttribute System.ComponentModel.DescriptionAttribute::Default
	DescriptionAttribute_t874390736 * ___Default_1;

public:
	inline static int32_t get_offset_of_Default_1() { return static_cast<int32_t>(offsetof(DescriptionAttribute_t874390736_StaticFields, ___Default_1)); }
	inline DescriptionAttribute_t874390736 * get_Default_1() const { return ___Default_1; }
	inline DescriptionAttribute_t874390736 ** get_address_of_Default_1() { return &___Default_1; }
	inline void set_Default_1(DescriptionAttribute_t874390736 * value)
	{
		___Default_1 = value;
		Il2CppCodeGenWriteBarrier((&___Default_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESCRIPTIONATTRIBUTE_T874390736_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef PROXYELEMENT_T3214064751_H
#define PROXYELEMENT_T3214064751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.ProxyElement
struct  ProxyElement_t3214064751  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct ProxyElement_t3214064751_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.ProxyElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.ProxyElement::autoDetectProp
	ConfigurationProperty_t3590861854 * ___autoDetectProp_14;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.ProxyElement::bypassOnLocalProp
	ConfigurationProperty_t3590861854 * ___bypassOnLocalProp_15;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.ProxyElement::proxyAddressProp
	ConfigurationProperty_t3590861854 * ___proxyAddressProp_16;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.ProxyElement::scriptLocationProp
	ConfigurationProperty_t3590861854 * ___scriptLocationProp_17;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.ProxyElement::useSystemDefaultProp
	ConfigurationProperty_t3590861854 * ___useSystemDefaultProp_18;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(ProxyElement_t3214064751_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier((&___properties_13), value);
	}

	inline static int32_t get_offset_of_autoDetectProp_14() { return static_cast<int32_t>(offsetof(ProxyElement_t3214064751_StaticFields, ___autoDetectProp_14)); }
	inline ConfigurationProperty_t3590861854 * get_autoDetectProp_14() const { return ___autoDetectProp_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_autoDetectProp_14() { return &___autoDetectProp_14; }
	inline void set_autoDetectProp_14(ConfigurationProperty_t3590861854 * value)
	{
		___autoDetectProp_14 = value;
		Il2CppCodeGenWriteBarrier((&___autoDetectProp_14), value);
	}

	inline static int32_t get_offset_of_bypassOnLocalProp_15() { return static_cast<int32_t>(offsetof(ProxyElement_t3214064751_StaticFields, ___bypassOnLocalProp_15)); }
	inline ConfigurationProperty_t3590861854 * get_bypassOnLocalProp_15() const { return ___bypassOnLocalProp_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_bypassOnLocalProp_15() { return &___bypassOnLocalProp_15; }
	inline void set_bypassOnLocalProp_15(ConfigurationProperty_t3590861854 * value)
	{
		___bypassOnLocalProp_15 = value;
		Il2CppCodeGenWriteBarrier((&___bypassOnLocalProp_15), value);
	}

	inline static int32_t get_offset_of_proxyAddressProp_16() { return static_cast<int32_t>(offsetof(ProxyElement_t3214064751_StaticFields, ___proxyAddressProp_16)); }
	inline ConfigurationProperty_t3590861854 * get_proxyAddressProp_16() const { return ___proxyAddressProp_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_proxyAddressProp_16() { return &___proxyAddressProp_16; }
	inline void set_proxyAddressProp_16(ConfigurationProperty_t3590861854 * value)
	{
		___proxyAddressProp_16 = value;
		Il2CppCodeGenWriteBarrier((&___proxyAddressProp_16), value);
	}

	inline static int32_t get_offset_of_scriptLocationProp_17() { return static_cast<int32_t>(offsetof(ProxyElement_t3214064751_StaticFields, ___scriptLocationProp_17)); }
	inline ConfigurationProperty_t3590861854 * get_scriptLocationProp_17() const { return ___scriptLocationProp_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_scriptLocationProp_17() { return &___scriptLocationProp_17; }
	inline void set_scriptLocationProp_17(ConfigurationProperty_t3590861854 * value)
	{
		___scriptLocationProp_17 = value;
		Il2CppCodeGenWriteBarrier((&___scriptLocationProp_17), value);
	}

	inline static int32_t get_offset_of_useSystemDefaultProp_18() { return static_cast<int32_t>(offsetof(ProxyElement_t3214064751_StaticFields, ___useSystemDefaultProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_useSystemDefaultProp_18() const { return ___useSystemDefaultProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_useSystemDefaultProp_18() { return &___useSystemDefaultProp_18; }
	inline void set_useSystemDefaultProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___useSystemDefaultProp_18 = value;
		Il2CppCodeGenWriteBarrier((&___useSystemDefaultProp_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROXYELEMENT_T3214064751_H
#ifndef CONFIGURATIONSECTION_T3156163955_H
#define CONFIGURATIONSECTION_T3156163955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationSection
struct  ConfigurationSection_t3156163955  : public ConfigurationElement_t3318566633
{
public:
	// System.Configuration.SectionInformation System.Configuration.ConfigurationSection::sectionInformation
	SectionInformation_t2821611020 * ___sectionInformation_13;
	// System.Configuration.IConfigurationSectionHandler System.Configuration.ConfigurationSection::section_handler
	RuntimeObject* ___section_handler_14;
	// System.String System.Configuration.ConfigurationSection::externalDataXml
	String_t* ___externalDataXml_15;
	// System.Object System.Configuration.ConfigurationSection::_configContext
	RuntimeObject * ____configContext_16;

public:
	inline static int32_t get_offset_of_sectionInformation_13() { return static_cast<int32_t>(offsetof(ConfigurationSection_t3156163955, ___sectionInformation_13)); }
	inline SectionInformation_t2821611020 * get_sectionInformation_13() const { return ___sectionInformation_13; }
	inline SectionInformation_t2821611020 ** get_address_of_sectionInformation_13() { return &___sectionInformation_13; }
	inline void set_sectionInformation_13(SectionInformation_t2821611020 * value)
	{
		___sectionInformation_13 = value;
		Il2CppCodeGenWriteBarrier((&___sectionInformation_13), value);
	}

	inline static int32_t get_offset_of_section_handler_14() { return static_cast<int32_t>(offsetof(ConfigurationSection_t3156163955, ___section_handler_14)); }
	inline RuntimeObject* get_section_handler_14() const { return ___section_handler_14; }
	inline RuntimeObject** get_address_of_section_handler_14() { return &___section_handler_14; }
	inline void set_section_handler_14(RuntimeObject* value)
	{
		___section_handler_14 = value;
		Il2CppCodeGenWriteBarrier((&___section_handler_14), value);
	}

	inline static int32_t get_offset_of_externalDataXml_15() { return static_cast<int32_t>(offsetof(ConfigurationSection_t3156163955, ___externalDataXml_15)); }
	inline String_t* get_externalDataXml_15() const { return ___externalDataXml_15; }
	inline String_t** get_address_of_externalDataXml_15() { return &___externalDataXml_15; }
	inline void set_externalDataXml_15(String_t* value)
	{
		___externalDataXml_15 = value;
		Il2CppCodeGenWriteBarrier((&___externalDataXml_15), value);
	}

	inline static int32_t get_offset_of__configContext_16() { return static_cast<int32_t>(offsetof(ConfigurationSection_t3156163955, ____configContext_16)); }
	inline RuntimeObject * get__configContext_16() const { return ____configContext_16; }
	inline RuntimeObject ** get_address_of__configContext_16() { return &____configContext_16; }
	inline void set__configContext_16(RuntimeObject * value)
	{
		____configContext_16 = value;
		Il2CppCodeGenWriteBarrier((&____configContext_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONSECTION_T3156163955_H
#ifndef SERVICEPOINTMANAGERELEMENT_T2768640361_H
#define SERVICEPOINTMANAGERELEMENT_T2768640361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.ServicePointManagerElement
struct  ServicePointManagerElement_t2768640361  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct ServicePointManagerElement_t2768640361_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.ServicePointManagerElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.ServicePointManagerElement::checkCertificateNameProp
	ConfigurationProperty_t3590861854 * ___checkCertificateNameProp_14;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.ServicePointManagerElement::checkCertificateRevocationListProp
	ConfigurationProperty_t3590861854 * ___checkCertificateRevocationListProp_15;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.ServicePointManagerElement::dnsRefreshTimeoutProp
	ConfigurationProperty_t3590861854 * ___dnsRefreshTimeoutProp_16;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.ServicePointManagerElement::enableDnsRoundRobinProp
	ConfigurationProperty_t3590861854 * ___enableDnsRoundRobinProp_17;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.ServicePointManagerElement::expect100ContinueProp
	ConfigurationProperty_t3590861854 * ___expect100ContinueProp_18;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.ServicePointManagerElement::useNagleAlgorithmProp
	ConfigurationProperty_t3590861854 * ___useNagleAlgorithmProp_19;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(ServicePointManagerElement_t2768640361_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier((&___properties_13), value);
	}

	inline static int32_t get_offset_of_checkCertificateNameProp_14() { return static_cast<int32_t>(offsetof(ServicePointManagerElement_t2768640361_StaticFields, ___checkCertificateNameProp_14)); }
	inline ConfigurationProperty_t3590861854 * get_checkCertificateNameProp_14() const { return ___checkCertificateNameProp_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_checkCertificateNameProp_14() { return &___checkCertificateNameProp_14; }
	inline void set_checkCertificateNameProp_14(ConfigurationProperty_t3590861854 * value)
	{
		___checkCertificateNameProp_14 = value;
		Il2CppCodeGenWriteBarrier((&___checkCertificateNameProp_14), value);
	}

	inline static int32_t get_offset_of_checkCertificateRevocationListProp_15() { return static_cast<int32_t>(offsetof(ServicePointManagerElement_t2768640361_StaticFields, ___checkCertificateRevocationListProp_15)); }
	inline ConfigurationProperty_t3590861854 * get_checkCertificateRevocationListProp_15() const { return ___checkCertificateRevocationListProp_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_checkCertificateRevocationListProp_15() { return &___checkCertificateRevocationListProp_15; }
	inline void set_checkCertificateRevocationListProp_15(ConfigurationProperty_t3590861854 * value)
	{
		___checkCertificateRevocationListProp_15 = value;
		Il2CppCodeGenWriteBarrier((&___checkCertificateRevocationListProp_15), value);
	}

	inline static int32_t get_offset_of_dnsRefreshTimeoutProp_16() { return static_cast<int32_t>(offsetof(ServicePointManagerElement_t2768640361_StaticFields, ___dnsRefreshTimeoutProp_16)); }
	inline ConfigurationProperty_t3590861854 * get_dnsRefreshTimeoutProp_16() const { return ___dnsRefreshTimeoutProp_16; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_dnsRefreshTimeoutProp_16() { return &___dnsRefreshTimeoutProp_16; }
	inline void set_dnsRefreshTimeoutProp_16(ConfigurationProperty_t3590861854 * value)
	{
		___dnsRefreshTimeoutProp_16 = value;
		Il2CppCodeGenWriteBarrier((&___dnsRefreshTimeoutProp_16), value);
	}

	inline static int32_t get_offset_of_enableDnsRoundRobinProp_17() { return static_cast<int32_t>(offsetof(ServicePointManagerElement_t2768640361_StaticFields, ___enableDnsRoundRobinProp_17)); }
	inline ConfigurationProperty_t3590861854 * get_enableDnsRoundRobinProp_17() const { return ___enableDnsRoundRobinProp_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_enableDnsRoundRobinProp_17() { return &___enableDnsRoundRobinProp_17; }
	inline void set_enableDnsRoundRobinProp_17(ConfigurationProperty_t3590861854 * value)
	{
		___enableDnsRoundRobinProp_17 = value;
		Il2CppCodeGenWriteBarrier((&___enableDnsRoundRobinProp_17), value);
	}

	inline static int32_t get_offset_of_expect100ContinueProp_18() { return static_cast<int32_t>(offsetof(ServicePointManagerElement_t2768640361_StaticFields, ___expect100ContinueProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_expect100ContinueProp_18() const { return ___expect100ContinueProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_expect100ContinueProp_18() { return &___expect100ContinueProp_18; }
	inline void set_expect100ContinueProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___expect100ContinueProp_18 = value;
		Il2CppCodeGenWriteBarrier((&___expect100ContinueProp_18), value);
	}

	inline static int32_t get_offset_of_useNagleAlgorithmProp_19() { return static_cast<int32_t>(offsetof(ServicePointManagerElement_t2768640361_StaticFields, ___useNagleAlgorithmProp_19)); }
	inline ConfigurationProperty_t3590861854 * get_useNagleAlgorithmProp_19() const { return ___useNagleAlgorithmProp_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_useNagleAlgorithmProp_19() { return &___useNagleAlgorithmProp_19; }
	inline void set_useNagleAlgorithmProp_19(ConfigurationProperty_t3590861854 * value)
	{
		___useNagleAlgorithmProp_19 = value;
		Il2CppCodeGenWriteBarrier((&___useNagleAlgorithmProp_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVICEPOINTMANAGERELEMENT_T2768640361_H
#ifndef SOCKETELEMENT_T3329874080_H
#define SOCKETELEMENT_T3329874080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.SocketElement
struct  SocketElement_t3329874080  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct SocketElement_t3329874080_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.SocketElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.SocketElement::alwaysUseCompletionPortsForAcceptProp
	ConfigurationProperty_t3590861854 * ___alwaysUseCompletionPortsForAcceptProp_14;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.SocketElement::alwaysUseCompletionPortsForConnectProp
	ConfigurationProperty_t3590861854 * ___alwaysUseCompletionPortsForConnectProp_15;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(SocketElement_t3329874080_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier((&___properties_13), value);
	}

	inline static int32_t get_offset_of_alwaysUseCompletionPortsForAcceptProp_14() { return static_cast<int32_t>(offsetof(SocketElement_t3329874080_StaticFields, ___alwaysUseCompletionPortsForAcceptProp_14)); }
	inline ConfigurationProperty_t3590861854 * get_alwaysUseCompletionPortsForAcceptProp_14() const { return ___alwaysUseCompletionPortsForAcceptProp_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_alwaysUseCompletionPortsForAcceptProp_14() { return &___alwaysUseCompletionPortsForAcceptProp_14; }
	inline void set_alwaysUseCompletionPortsForAcceptProp_14(ConfigurationProperty_t3590861854 * value)
	{
		___alwaysUseCompletionPortsForAcceptProp_14 = value;
		Il2CppCodeGenWriteBarrier((&___alwaysUseCompletionPortsForAcceptProp_14), value);
	}

	inline static int32_t get_offset_of_alwaysUseCompletionPortsForConnectProp_15() { return static_cast<int32_t>(offsetof(SocketElement_t3329874080_StaticFields, ___alwaysUseCompletionPortsForConnectProp_15)); }
	inline ConfigurationProperty_t3590861854 * get_alwaysUseCompletionPortsForConnectProp_15() const { return ___alwaysUseCompletionPortsForConnectProp_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_alwaysUseCompletionPortsForConnectProp_15() { return &___alwaysUseCompletionPortsForConnectProp_15; }
	inline void set_alwaysUseCompletionPortsForConnectProp_15(ConfigurationProperty_t3590861854 * value)
	{
		___alwaysUseCompletionPortsForConnectProp_15 = value;
		Il2CppCodeGenWriteBarrier((&___alwaysUseCompletionPortsForConnectProp_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETELEMENT_T3329874080_H
#ifndef WEBPROXYSCRIPTELEMENT_T477406598_H
#define WEBPROXYSCRIPTELEMENT_T477406598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.WebProxyScriptElement
struct  WebProxyScriptElement_t477406598  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct WebProxyScriptElement_t477406598_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Net.Configuration.WebProxyScriptElement::downloadTimeoutProp
	ConfigurationProperty_t3590861854 * ___downloadTimeoutProp_13;
	// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.WebProxyScriptElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_14;

public:
	inline static int32_t get_offset_of_downloadTimeoutProp_13() { return static_cast<int32_t>(offsetof(WebProxyScriptElement_t477406598_StaticFields, ___downloadTimeoutProp_13)); }
	inline ConfigurationProperty_t3590861854 * get_downloadTimeoutProp_13() const { return ___downloadTimeoutProp_13; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_downloadTimeoutProp_13() { return &___downloadTimeoutProp_13; }
	inline void set_downloadTimeoutProp_13(ConfigurationProperty_t3590861854 * value)
	{
		___downloadTimeoutProp_13 = value;
		Il2CppCodeGenWriteBarrier((&___downloadTimeoutProp_13), value);
	}

	inline static int32_t get_offset_of_properties_14() { return static_cast<int32_t>(offsetof(WebProxyScriptElement_t477406598_StaticFields, ___properties_14)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_14() const { return ___properties_14; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_14() { return &___properties_14; }
	inline void set_properties_14(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_14 = value;
		Il2CppCodeGenWriteBarrier((&___properties_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBPROXYSCRIPTELEMENT_T477406598_H
#ifndef WEBREQUESTMODULEELEMENT_T1406085120_H
#define WEBREQUESTMODULEELEMENT_T1406085120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.WebRequestModuleElement
struct  WebRequestModuleElement_t1406085120  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct WebRequestModuleElement_t1406085120_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.WebRequestModuleElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.WebRequestModuleElement::prefixProp
	ConfigurationProperty_t3590861854 * ___prefixProp_14;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.WebRequestModuleElement::typeProp
	ConfigurationProperty_t3590861854 * ___typeProp_15;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(WebRequestModuleElement_t1406085120_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier((&___properties_13), value);
	}

	inline static int32_t get_offset_of_prefixProp_14() { return static_cast<int32_t>(offsetof(WebRequestModuleElement_t1406085120_StaticFields, ___prefixProp_14)); }
	inline ConfigurationProperty_t3590861854 * get_prefixProp_14() const { return ___prefixProp_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_prefixProp_14() { return &___prefixProp_14; }
	inline void set_prefixProp_14(ConfigurationProperty_t3590861854 * value)
	{
		___prefixProp_14 = value;
		Il2CppCodeGenWriteBarrier((&___prefixProp_14), value);
	}

	inline static int32_t get_offset_of_typeProp_15() { return static_cast<int32_t>(offsetof(WebRequestModuleElement_t1406085120_StaticFields, ___typeProp_15)); }
	inline ConfigurationProperty_t3590861854 * get_typeProp_15() const { return ___typeProp_15; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_typeProp_15() { return &___typeProp_15; }
	inline void set_typeProp_15(ConfigurationProperty_t3590861854 * value)
	{
		___typeProp_15 = value;
		Il2CppCodeGenWriteBarrier((&___typeProp_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTMODULEELEMENT_T1406085120_H
#ifndef CONFIGURATIONELEMENTCOLLECTION_T446763386_H
#define CONFIGURATIONELEMENTCOLLECTION_T446763386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationElementCollection
struct  ConfigurationElementCollection_t446763386  : public ConfigurationElement_t3318566633
{
public:
	// System.Collections.ArrayList System.Configuration.ConfigurationElementCollection::list
	ArrayList_t2718874744 * ___list_13;
	// System.Collections.ArrayList System.Configuration.ConfigurationElementCollection::removed
	ArrayList_t2718874744 * ___removed_14;
	// System.Collections.ArrayList System.Configuration.ConfigurationElementCollection::inherited
	ArrayList_t2718874744 * ___inherited_15;
	// System.Boolean System.Configuration.ConfigurationElementCollection::emitClear
	bool ___emitClear_16;
	// System.Boolean System.Configuration.ConfigurationElementCollection::modified
	bool ___modified_17;
	// System.Collections.IComparer System.Configuration.ConfigurationElementCollection::comparer
	RuntimeObject* ___comparer_18;
	// System.Int32 System.Configuration.ConfigurationElementCollection::inheritedLimitIndex
	int32_t ___inheritedLimitIndex_19;
	// System.String System.Configuration.ConfigurationElementCollection::addElementName
	String_t* ___addElementName_20;
	// System.String System.Configuration.ConfigurationElementCollection::clearElementName
	String_t* ___clearElementName_21;
	// System.String System.Configuration.ConfigurationElementCollection::removeElementName
	String_t* ___removeElementName_22;

public:
	inline static int32_t get_offset_of_list_13() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t446763386, ___list_13)); }
	inline ArrayList_t2718874744 * get_list_13() const { return ___list_13; }
	inline ArrayList_t2718874744 ** get_address_of_list_13() { return &___list_13; }
	inline void set_list_13(ArrayList_t2718874744 * value)
	{
		___list_13 = value;
		Il2CppCodeGenWriteBarrier((&___list_13), value);
	}

	inline static int32_t get_offset_of_removed_14() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t446763386, ___removed_14)); }
	inline ArrayList_t2718874744 * get_removed_14() const { return ___removed_14; }
	inline ArrayList_t2718874744 ** get_address_of_removed_14() { return &___removed_14; }
	inline void set_removed_14(ArrayList_t2718874744 * value)
	{
		___removed_14 = value;
		Il2CppCodeGenWriteBarrier((&___removed_14), value);
	}

	inline static int32_t get_offset_of_inherited_15() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t446763386, ___inherited_15)); }
	inline ArrayList_t2718874744 * get_inherited_15() const { return ___inherited_15; }
	inline ArrayList_t2718874744 ** get_address_of_inherited_15() { return &___inherited_15; }
	inline void set_inherited_15(ArrayList_t2718874744 * value)
	{
		___inherited_15 = value;
		Il2CppCodeGenWriteBarrier((&___inherited_15), value);
	}

	inline static int32_t get_offset_of_emitClear_16() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t446763386, ___emitClear_16)); }
	inline bool get_emitClear_16() const { return ___emitClear_16; }
	inline bool* get_address_of_emitClear_16() { return &___emitClear_16; }
	inline void set_emitClear_16(bool value)
	{
		___emitClear_16 = value;
	}

	inline static int32_t get_offset_of_modified_17() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t446763386, ___modified_17)); }
	inline bool get_modified_17() const { return ___modified_17; }
	inline bool* get_address_of_modified_17() { return &___modified_17; }
	inline void set_modified_17(bool value)
	{
		___modified_17 = value;
	}

	inline static int32_t get_offset_of_comparer_18() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t446763386, ___comparer_18)); }
	inline RuntimeObject* get_comparer_18() const { return ___comparer_18; }
	inline RuntimeObject** get_address_of_comparer_18() { return &___comparer_18; }
	inline void set_comparer_18(RuntimeObject* value)
	{
		___comparer_18 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_18), value);
	}

	inline static int32_t get_offset_of_inheritedLimitIndex_19() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t446763386, ___inheritedLimitIndex_19)); }
	inline int32_t get_inheritedLimitIndex_19() const { return ___inheritedLimitIndex_19; }
	inline int32_t* get_address_of_inheritedLimitIndex_19() { return &___inheritedLimitIndex_19; }
	inline void set_inheritedLimitIndex_19(int32_t value)
	{
		___inheritedLimitIndex_19 = value;
	}

	inline static int32_t get_offset_of_addElementName_20() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t446763386, ___addElementName_20)); }
	inline String_t* get_addElementName_20() const { return ___addElementName_20; }
	inline String_t** get_address_of_addElementName_20() { return &___addElementName_20; }
	inline void set_addElementName_20(String_t* value)
	{
		___addElementName_20 = value;
		Il2CppCodeGenWriteBarrier((&___addElementName_20), value);
	}

	inline static int32_t get_offset_of_clearElementName_21() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t446763386, ___clearElementName_21)); }
	inline String_t* get_clearElementName_21() const { return ___clearElementName_21; }
	inline String_t** get_address_of_clearElementName_21() { return &___clearElementName_21; }
	inline void set_clearElementName_21(String_t* value)
	{
		___clearElementName_21 = value;
		Il2CppCodeGenWriteBarrier((&___clearElementName_21), value);
	}

	inline static int32_t get_offset_of_removeElementName_22() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t446763386, ___removeElementName_22)); }
	inline String_t* get_removeElementName_22() const { return ___removeElementName_22; }
	inline String_t** get_address_of_removeElementName_22() { return &___removeElementName_22; }
	inline void set_removeElementName_22(String_t* value)
	{
		___removeElementName_22 = value;
		Il2CppCodeGenWriteBarrier((&___removeElementName_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONELEMENTCOLLECTION_T446763386_H
#ifndef BYPASSELEMENT_T2358616601_H
#define BYPASSELEMENT_T2358616601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.BypassElement
struct  BypassElement_t2358616601  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct BypassElement_t2358616601_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.BypassElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.BypassElement::addressProp
	ConfigurationProperty_t3590861854 * ___addressProp_14;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(BypassElement_t2358616601_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier((&___properties_13), value);
	}

	inline static int32_t get_offset_of_addressProp_14() { return static_cast<int32_t>(offsetof(BypassElement_t2358616601_StaticFields, ___addressProp_14)); }
	inline ConfigurationProperty_t3590861854 * get_addressProp_14() const { return ___addressProp_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_addressProp_14() { return &___addressProp_14; }
	inline void set_addressProp_14(ConfigurationProperty_t3590861854 * value)
	{
		___addressProp_14 = value;
		Il2CppCodeGenWriteBarrier((&___addressProp_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYPASSELEMENT_T2358616601_H
#ifndef PROCESSMODULECOLLECTION_T3446348346_H
#define PROCESSMODULECOLLECTION_T3446348346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.ProcessModuleCollection
struct  ProcessModuleCollection_t3446348346  : public ReadOnlyCollectionBase_t1836743899
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROCESSMODULECOLLECTION_T3446348346_H
#ifndef DATARECEIVEDEVENTARGS_T2585381898_H
#define DATARECEIVEDEVENTARGS_T2585381898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.DataReceivedEventArgs
struct  DataReceivedEventArgs_t2585381898  : public EventArgs_t3591816995
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATARECEIVEDEVENTARGS_T2585381898_H
#ifndef DEFAULTURIPARSER_T95882050_H
#define DEFAULTURIPARSER_T95882050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DefaultUriParser
struct  DefaultUriParser_t95882050  : public UriParser_t3890150400
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTURIPARSER_T95882050_H
#ifndef GENERICURIPARSER_T1141496137_H
#define GENERICURIPARSER_T1141496137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.GenericUriParser
struct  GenericUriParser_t1141496137  : public UriParser_t3890150400
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICURIPARSER_T1141496137_H
#ifndef AUTHENTICATIONMODULEELEMENT_T2289740666_H
#define AUTHENTICATIONMODULEELEMENT_T2289740666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.AuthenticationModuleElement
struct  AuthenticationModuleElement_t2289740666  : public ConfigurationElement_t3318566633
{
public:

public:
};

struct AuthenticationModuleElement_t2289740666_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.AuthenticationModuleElement::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_13;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.AuthenticationModuleElement::typeProp
	ConfigurationProperty_t3590861854 * ___typeProp_14;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(AuthenticationModuleElement_t2289740666_StaticFields, ___properties_13)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier((&___properties_13), value);
	}

	inline static int32_t get_offset_of_typeProp_14() { return static_cast<int32_t>(offsetof(AuthenticationModuleElement_t2289740666_StaticFields, ___typeProp_14)); }
	inline ConfigurationProperty_t3590861854 * get_typeProp_14() const { return ___typeProp_14; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_typeProp_14() { return &___typeProp_14; }
	inline void set_typeProp_14(ConfigurationProperty_t3590861854 * value)
	{
		___typeProp_14 = value;
		Il2CppCodeGenWriteBarrier((&___typeProp_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONMODULEELEMENT_T2289740666_H
#ifndef WEAKOBJECTWRAPPERCOMPARER_T303980402_H
#define WEAKOBJECTWRAPPERCOMPARER_T303980402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.WeakObjectWrapperComparer
struct  WeakObjectWrapperComparer_t303980402  : public EqualityComparer_1_t3292203282
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEAKOBJECTWRAPPERCOMPARER_T303980402_H
#ifndef TRACELEVEL_T1218108719_H
#define TRACELEVEL_T1218108719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceLevel
struct  TraceLevel_t1218108719 
{
public:
	// System.Int32 System.Diagnostics.TraceLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TraceLevel_t1218108719, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACELEVEL_T1218108719_H
#ifndef TRACEEVENTTYPE_T181971481_H
#define TRACEEVENTTYPE_T181971481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceEventType
struct  TraceEventType_t181971481 
{
public:
	// System.Int32 System.Diagnostics.TraceEventType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TraceEventType_t181971481, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACEEVENTTYPE_T181971481_H
#ifndef SOURCELEVELS_T2339259104_H
#define SOURCELEVELS_T2339259104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.SourceLevels
struct  SourceLevels_t2339259104 
{
public:
	// System.Int32 System.Diagnostics.SourceLevels::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SourceLevels_t2339259104, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOURCELEVELS_T2339259104_H
#ifndef WEBREQUESTMODULESSECTION_T4132732301_H
#define WEBREQUESTMODULESSECTION_T4132732301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.WebRequestModulesSection
struct  WebRequestModulesSection_t4132732301  : public ConfigurationSection_t3156163955
{
public:

public:
};

struct WebRequestModulesSection_t4132732301_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.WebRequestModulesSection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_17;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.WebRequestModulesSection::webRequestModulesProp
	ConfigurationProperty_t3590861854 * ___webRequestModulesProp_18;

public:
	inline static int32_t get_offset_of_properties_17() { return static_cast<int32_t>(offsetof(WebRequestModulesSection_t4132732301_StaticFields, ___properties_17)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_17() const { return ___properties_17; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_17() { return &___properties_17; }
	inline void set_properties_17(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_17 = value;
		Il2CppCodeGenWriteBarrier((&___properties_17), value);
	}

	inline static int32_t get_offset_of_webRequestModulesProp_18() { return static_cast<int32_t>(offsetof(WebRequestModulesSection_t4132732301_StaticFields, ___webRequestModulesProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_webRequestModulesProp_18() const { return ___webRequestModulesProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_webRequestModulesProp_18() { return &___webRequestModulesProp_18; }
	inline void set_webRequestModulesProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___webRequestModulesProp_18 = value;
		Il2CppCodeGenWriteBarrier((&___webRequestModulesProp_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTMODULESSECTION_T4132732301_H
#ifndef EXTERNALEXCEPTION_T3544951457_H
#define EXTERNALEXCEPTION_T3544951457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.ExternalException
struct  ExternalException_t3544951457  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNALEXCEPTION_T3544951457_H
#ifndef TRACEOPTIONS_T3546477972_H
#define TRACEOPTIONS_T3546477972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceOptions
struct  TraceOptions_t3546477972 
{
public:
	// System.Int32 System.Diagnostics.TraceOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TraceOptions_t3546477972, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACEOPTIONS_T3546477972_H
#ifndef DECOMPRESSIONMETHODS_T1612219745_H
#define DECOMPRESSIONMETHODS_T1612219745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DecompressionMethods
struct  DecompressionMethods_t1612219745 
{
public:
	// System.Int32 System.Net.DecompressionMethods::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DecompressionMethods_t1612219745, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECOMPRESSIONMETHODS_T1612219745_H
#ifndef COMPRESSIONMODE_T3714291783_H
#define COMPRESSIONMODE_T3714291783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Compression.CompressionMode
struct  CompressionMode_t3714291783 
{
public:
	// System.Int32 System.IO.Compression.CompressionMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CompressionMode_t3714291783, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONMODE_T3714291783_H
#ifndef FILEACCESS_T1659085276_H
#define FILEACCESS_T1659085276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileAccess
struct  FileAccess_t1659085276 
{
public:
	// System.Int32 System.IO.FileAccess::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FileAccess_t1659085276, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEACCESS_T1659085276_H
#ifndef PROCESSWINDOWSTYLE_T3127335931_H
#define PROCESSWINDOWSTYLE_T3127335931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.ProcessWindowStyle
struct  ProcessWindowStyle_t3127335931 
{
public:
	// System.Int32 System.Diagnostics.ProcessWindowStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ProcessWindowStyle_t3127335931, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROCESSWINDOWSTYLE_T3127335931_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef CONFIGURATIONEXCEPTION_T3515317685_H
#define CONFIGURATIONEXCEPTION_T3515317685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationException
struct  ConfigurationException_t3515317685  : public SystemException_t176217640
{
public:
	// System.String System.Configuration.ConfigurationException::filename
	String_t* ___filename_11;
	// System.Int32 System.Configuration.ConfigurationException::line
	int32_t ___line_12;

public:
	inline static int32_t get_offset_of_filename_11() { return static_cast<int32_t>(offsetof(ConfigurationException_t3515317685, ___filename_11)); }
	inline String_t* get_filename_11() const { return ___filename_11; }
	inline String_t** get_address_of_filename_11() { return &___filename_11; }
	inline void set_filename_11(String_t* value)
	{
		___filename_11 = value;
		Il2CppCodeGenWriteBarrier((&___filename_11), value);
	}

	inline static int32_t get_offset_of_line_12() { return static_cast<int32_t>(offsetof(ConfigurationException_t3515317685, ___line_12)); }
	inline int32_t get_line_12() const { return ___line_12; }
	inline int32_t* get_address_of_line_12() { return &___line_12; }
	inline void set_line_12(int32_t value)
	{
		___line_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONEXCEPTION_T3515317685_H
#ifndef FORMATEXCEPTION_T154580423_H
#define FORMATEXCEPTION_T154580423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.FormatException
struct  FormatException_t154580423  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATEXCEPTION_T154580423_H
#ifndef REQUESTSTREAM_T762880582_H
#define REQUESTSTREAM_T762880582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.RequestStream
struct  RequestStream_t762880582  : public Stream_t1273022909
{
public:
	// System.Byte[] System.Net.RequestStream::buffer
	ByteU5BU5D_t4116647657* ___buffer_2;
	// System.Int32 System.Net.RequestStream::offset
	int32_t ___offset_3;
	// System.Int32 System.Net.RequestStream::length
	int32_t ___length_4;
	// System.Int64 System.Net.RequestStream::remaining_body
	int64_t ___remaining_body_5;
	// System.Boolean System.Net.RequestStream::disposed
	bool ___disposed_6;
	// System.IO.Stream System.Net.RequestStream::stream
	Stream_t1273022909 * ___stream_7;

public:
	inline static int32_t get_offset_of_buffer_2() { return static_cast<int32_t>(offsetof(RequestStream_t762880582, ___buffer_2)); }
	inline ByteU5BU5D_t4116647657* get_buffer_2() const { return ___buffer_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_2() { return &___buffer_2; }
	inline void set_buffer_2(ByteU5BU5D_t4116647657* value)
	{
		___buffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_2), value);
	}

	inline static int32_t get_offset_of_offset_3() { return static_cast<int32_t>(offsetof(RequestStream_t762880582, ___offset_3)); }
	inline int32_t get_offset_3() const { return ___offset_3; }
	inline int32_t* get_address_of_offset_3() { return &___offset_3; }
	inline void set_offset_3(int32_t value)
	{
		___offset_3 = value;
	}

	inline static int32_t get_offset_of_length_4() { return static_cast<int32_t>(offsetof(RequestStream_t762880582, ___length_4)); }
	inline int32_t get_length_4() const { return ___length_4; }
	inline int32_t* get_address_of_length_4() { return &___length_4; }
	inline void set_length_4(int32_t value)
	{
		___length_4 = value;
	}

	inline static int32_t get_offset_of_remaining_body_5() { return static_cast<int32_t>(offsetof(RequestStream_t762880582, ___remaining_body_5)); }
	inline int64_t get_remaining_body_5() const { return ___remaining_body_5; }
	inline int64_t* get_address_of_remaining_body_5() { return &___remaining_body_5; }
	inline void set_remaining_body_5(int64_t value)
	{
		___remaining_body_5 = value;
	}

	inline static int32_t get_offset_of_disposed_6() { return static_cast<int32_t>(offsetof(RequestStream_t762880582, ___disposed_6)); }
	inline bool get_disposed_6() const { return ___disposed_6; }
	inline bool* get_address_of_disposed_6() { return &___disposed_6; }
	inline void set_disposed_6(bool value)
	{
		___disposed_6 = value;
	}

	inline static int32_t get_offset_of_stream_7() { return static_cast<int32_t>(offsetof(RequestStream_t762880582, ___stream_7)); }
	inline Stream_t1273022909 * get_stream_7() const { return ___stream_7; }
	inline Stream_t1273022909 ** get_address_of_stream_7() { return &___stream_7; }
	inline void set_stream_7(Stream_t1273022909 * value)
	{
		___stream_7 = value;
		Il2CppCodeGenWriteBarrier((&___stream_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTSTREAM_T762880582_H
#ifndef MONITORINGDESCRIPTIONATTRIBUTE_T2767822115_H
#define MONITORINGDESCRIPTIONATTRIBUTE_T2767822115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.MonitoringDescriptionAttribute
struct  MonitoringDescriptionAttribute_t2767822115  : public DescriptionAttribute_t874390736
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONITORINGDESCRIPTIONATTRIBUTE_T2767822115_H
#ifndef PROCINFO_T2917059746_H
#define PROCINFO_T2917059746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.Process/ProcInfo
struct  ProcInfo_t2917059746 
{
public:
	// System.IntPtr System.Diagnostics.Process/ProcInfo::process_handle
	intptr_t ___process_handle_0;
	// System.IntPtr System.Diagnostics.Process/ProcInfo::thread_handle
	intptr_t ___thread_handle_1;
	// System.Int32 System.Diagnostics.Process/ProcInfo::pid
	int32_t ___pid_2;
	// System.Int32 System.Diagnostics.Process/ProcInfo::tid
	int32_t ___tid_3;
	// System.String[] System.Diagnostics.Process/ProcInfo::envKeys
	StringU5BU5D_t1281789340* ___envKeys_4;
	// System.String[] System.Diagnostics.Process/ProcInfo::envValues
	StringU5BU5D_t1281789340* ___envValues_5;
	// System.String System.Diagnostics.Process/ProcInfo::UserName
	String_t* ___UserName_6;
	// System.String System.Diagnostics.Process/ProcInfo::Domain
	String_t* ___Domain_7;
	// System.IntPtr System.Diagnostics.Process/ProcInfo::Password
	intptr_t ___Password_8;
	// System.Boolean System.Diagnostics.Process/ProcInfo::LoadUserProfile
	bool ___LoadUserProfile_9;

public:
	inline static int32_t get_offset_of_process_handle_0() { return static_cast<int32_t>(offsetof(ProcInfo_t2917059746, ___process_handle_0)); }
	inline intptr_t get_process_handle_0() const { return ___process_handle_0; }
	inline intptr_t* get_address_of_process_handle_0() { return &___process_handle_0; }
	inline void set_process_handle_0(intptr_t value)
	{
		___process_handle_0 = value;
	}

	inline static int32_t get_offset_of_thread_handle_1() { return static_cast<int32_t>(offsetof(ProcInfo_t2917059746, ___thread_handle_1)); }
	inline intptr_t get_thread_handle_1() const { return ___thread_handle_1; }
	inline intptr_t* get_address_of_thread_handle_1() { return &___thread_handle_1; }
	inline void set_thread_handle_1(intptr_t value)
	{
		___thread_handle_1 = value;
	}

	inline static int32_t get_offset_of_pid_2() { return static_cast<int32_t>(offsetof(ProcInfo_t2917059746, ___pid_2)); }
	inline int32_t get_pid_2() const { return ___pid_2; }
	inline int32_t* get_address_of_pid_2() { return &___pid_2; }
	inline void set_pid_2(int32_t value)
	{
		___pid_2 = value;
	}

	inline static int32_t get_offset_of_tid_3() { return static_cast<int32_t>(offsetof(ProcInfo_t2917059746, ___tid_3)); }
	inline int32_t get_tid_3() const { return ___tid_3; }
	inline int32_t* get_address_of_tid_3() { return &___tid_3; }
	inline void set_tid_3(int32_t value)
	{
		___tid_3 = value;
	}

	inline static int32_t get_offset_of_envKeys_4() { return static_cast<int32_t>(offsetof(ProcInfo_t2917059746, ___envKeys_4)); }
	inline StringU5BU5D_t1281789340* get_envKeys_4() const { return ___envKeys_4; }
	inline StringU5BU5D_t1281789340** get_address_of_envKeys_4() { return &___envKeys_4; }
	inline void set_envKeys_4(StringU5BU5D_t1281789340* value)
	{
		___envKeys_4 = value;
		Il2CppCodeGenWriteBarrier((&___envKeys_4), value);
	}

	inline static int32_t get_offset_of_envValues_5() { return static_cast<int32_t>(offsetof(ProcInfo_t2917059746, ___envValues_5)); }
	inline StringU5BU5D_t1281789340* get_envValues_5() const { return ___envValues_5; }
	inline StringU5BU5D_t1281789340** get_address_of_envValues_5() { return &___envValues_5; }
	inline void set_envValues_5(StringU5BU5D_t1281789340* value)
	{
		___envValues_5 = value;
		Il2CppCodeGenWriteBarrier((&___envValues_5), value);
	}

	inline static int32_t get_offset_of_UserName_6() { return static_cast<int32_t>(offsetof(ProcInfo_t2917059746, ___UserName_6)); }
	inline String_t* get_UserName_6() const { return ___UserName_6; }
	inline String_t** get_address_of_UserName_6() { return &___UserName_6; }
	inline void set_UserName_6(String_t* value)
	{
		___UserName_6 = value;
		Il2CppCodeGenWriteBarrier((&___UserName_6), value);
	}

	inline static int32_t get_offset_of_Domain_7() { return static_cast<int32_t>(offsetof(ProcInfo_t2917059746, ___Domain_7)); }
	inline String_t* get_Domain_7() const { return ___Domain_7; }
	inline String_t** get_address_of_Domain_7() { return &___Domain_7; }
	inline void set_Domain_7(String_t* value)
	{
		___Domain_7 = value;
		Il2CppCodeGenWriteBarrier((&___Domain_7), value);
	}

	inline static int32_t get_offset_of_Password_8() { return static_cast<int32_t>(offsetof(ProcInfo_t2917059746, ___Password_8)); }
	inline intptr_t get_Password_8() const { return ___Password_8; }
	inline intptr_t* get_address_of_Password_8() { return &___Password_8; }
	inline void set_Password_8(intptr_t value)
	{
		___Password_8 = value;
	}

	inline static int32_t get_offset_of_LoadUserProfile_9() { return static_cast<int32_t>(offsetof(ProcInfo_t2917059746, ___LoadUserProfile_9)); }
	inline bool get_LoadUserProfile_9() const { return ___LoadUserProfile_9; }
	inline bool* get_address_of_LoadUserProfile_9() { return &___LoadUserProfile_9; }
	inline void set_LoadUserProfile_9(bool value)
	{
		___LoadUserProfile_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Diagnostics.Process/ProcInfo
struct ProcInfo_t2917059746_marshaled_pinvoke
{
	intptr_t ___process_handle_0;
	intptr_t ___thread_handle_1;
	int32_t ___pid_2;
	int32_t ___tid_3;
	char** ___envKeys_4;
	char** ___envValues_5;
	char* ___UserName_6;
	char* ___Domain_7;
	intptr_t ___Password_8;
	int32_t ___LoadUserProfile_9;
};
// Native definition for COM marshalling of System.Diagnostics.Process/ProcInfo
struct ProcInfo_t2917059746_marshaled_com
{
	intptr_t ___process_handle_0;
	intptr_t ___thread_handle_1;
	int32_t ___pid_2;
	int32_t ___tid_3;
	Il2CppChar** ___envKeys_4;
	Il2CppChar** ___envValues_5;
	Il2CppChar* ___UserName_6;
	Il2CppChar* ___Domain_7;
	intptr_t ___Password_8;
	int32_t ___LoadUserProfile_9;
};
#endif // PROCINFO_T2917059746_H
#ifndef ASYNCMODES_T4207881179_H
#define ASYNCMODES_T4207881179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.Process/AsyncModes
struct  AsyncModes_t4207881179 
{
public:
	// System.Int32 System.Diagnostics.Process/AsyncModes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AsyncModes_t4207881179, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCMODES_T4207881179_H
#ifndef PROCESSASYNCREADER_T337580163_H
#define PROCESSASYNCREADER_T337580163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.Process/ProcessAsyncReader
struct  ProcessAsyncReader_t337580163  : public RuntimeObject
{
public:
	// System.Object System.Diagnostics.Process/ProcessAsyncReader::Sock
	RuntimeObject * ___Sock_0;
	// System.IntPtr System.Diagnostics.Process/ProcessAsyncReader::handle
	intptr_t ___handle_1;
	// System.Object System.Diagnostics.Process/ProcessAsyncReader::state
	RuntimeObject * ___state_2;
	// System.AsyncCallback System.Diagnostics.Process/ProcessAsyncReader::callback
	AsyncCallback_t3962456242 * ___callback_3;
	// System.Threading.ManualResetEvent System.Diagnostics.Process/ProcessAsyncReader::wait_handle
	ManualResetEvent_t451242010 * ___wait_handle_4;
	// System.Exception System.Diagnostics.Process/ProcessAsyncReader::delayedException
	Exception_t * ___delayedException_5;
	// System.Object System.Diagnostics.Process/ProcessAsyncReader::EndPoint
	RuntimeObject * ___EndPoint_6;
	// System.Byte[] System.Diagnostics.Process/ProcessAsyncReader::buffer
	ByteU5BU5D_t4116647657* ___buffer_7;
	// System.Int32 System.Diagnostics.Process/ProcessAsyncReader::Offset
	int32_t ___Offset_8;
	// System.Int32 System.Diagnostics.Process/ProcessAsyncReader::Size
	int32_t ___Size_9;
	// System.Int32 System.Diagnostics.Process/ProcessAsyncReader::SockFlags
	int32_t ___SockFlags_10;
	// System.Object System.Diagnostics.Process/ProcessAsyncReader::AcceptSocket
	RuntimeObject * ___AcceptSocket_11;
	// System.Object[] System.Diagnostics.Process/ProcessAsyncReader::Addresses
	ObjectU5BU5D_t2843939325* ___Addresses_12;
	// System.Int32 System.Diagnostics.Process/ProcessAsyncReader::port
	int32_t ___port_13;
	// System.Object System.Diagnostics.Process/ProcessAsyncReader::Buffers
	RuntimeObject * ___Buffers_14;
	// System.Boolean System.Diagnostics.Process/ProcessAsyncReader::ReuseSocket
	bool ___ReuseSocket_15;
	// System.Object System.Diagnostics.Process/ProcessAsyncReader::acc_socket
	RuntimeObject * ___acc_socket_16;
	// System.Int32 System.Diagnostics.Process/ProcessAsyncReader::total
	int32_t ___total_17;
	// System.Boolean System.Diagnostics.Process/ProcessAsyncReader::completed_sync
	bool ___completed_sync_18;
	// System.Boolean System.Diagnostics.Process/ProcessAsyncReader::completed
	bool ___completed_19;
	// System.Boolean System.Diagnostics.Process/ProcessAsyncReader::err_out
	bool ___err_out_20;
	// System.Int32 System.Diagnostics.Process/ProcessAsyncReader::error
	int32_t ___error_21;
	// System.Int32 System.Diagnostics.Process/ProcessAsyncReader::operation
	int32_t ___operation_22;
	// System.Object System.Diagnostics.Process/ProcessAsyncReader::ares
	RuntimeObject * ___ares_23;
	// System.Int32 System.Diagnostics.Process/ProcessAsyncReader::EndCalled
	int32_t ___EndCalled_24;
	// System.Diagnostics.Process System.Diagnostics.Process/ProcessAsyncReader::process
	Process_t3774297411 * ___process_25;
	// System.IO.Stream System.Diagnostics.Process/ProcessAsyncReader::stream
	Stream_t1273022909 * ___stream_26;
	// System.Text.StringBuilder System.Diagnostics.Process/ProcessAsyncReader::sb
	StringBuilder_t * ___sb_27;
	// System.Text.Encoding System.Diagnostics.Process/ProcessAsyncReader::outputEncoding
	Encoding_t1523322056 * ___outputEncoding_28;
	// System.Diagnostics.Process/AsyncReadHandler System.Diagnostics.Process/ProcessAsyncReader::ReadHandler
	AsyncReadHandler_t1188682440 * ___ReadHandler_29;

public:
	inline static int32_t get_offset_of_Sock_0() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___Sock_0)); }
	inline RuntimeObject * get_Sock_0() const { return ___Sock_0; }
	inline RuntimeObject ** get_address_of_Sock_0() { return &___Sock_0; }
	inline void set_Sock_0(RuntimeObject * value)
	{
		___Sock_0 = value;
		Il2CppCodeGenWriteBarrier((&___Sock_0), value);
	}

	inline static int32_t get_offset_of_handle_1() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___handle_1)); }
	inline intptr_t get_handle_1() const { return ___handle_1; }
	inline intptr_t* get_address_of_handle_1() { return &___handle_1; }
	inline void set_handle_1(intptr_t value)
	{
		___handle_1 = value;
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___state_2)); }
	inline RuntimeObject * get_state_2() const { return ___state_2; }
	inline RuntimeObject ** get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(RuntimeObject * value)
	{
		___state_2 = value;
		Il2CppCodeGenWriteBarrier((&___state_2), value);
	}

	inline static int32_t get_offset_of_callback_3() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___callback_3)); }
	inline AsyncCallback_t3962456242 * get_callback_3() const { return ___callback_3; }
	inline AsyncCallback_t3962456242 ** get_address_of_callback_3() { return &___callback_3; }
	inline void set_callback_3(AsyncCallback_t3962456242 * value)
	{
		___callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___callback_3), value);
	}

	inline static int32_t get_offset_of_wait_handle_4() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___wait_handle_4)); }
	inline ManualResetEvent_t451242010 * get_wait_handle_4() const { return ___wait_handle_4; }
	inline ManualResetEvent_t451242010 ** get_address_of_wait_handle_4() { return &___wait_handle_4; }
	inline void set_wait_handle_4(ManualResetEvent_t451242010 * value)
	{
		___wait_handle_4 = value;
		Il2CppCodeGenWriteBarrier((&___wait_handle_4), value);
	}

	inline static int32_t get_offset_of_delayedException_5() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___delayedException_5)); }
	inline Exception_t * get_delayedException_5() const { return ___delayedException_5; }
	inline Exception_t ** get_address_of_delayedException_5() { return &___delayedException_5; }
	inline void set_delayedException_5(Exception_t * value)
	{
		___delayedException_5 = value;
		Il2CppCodeGenWriteBarrier((&___delayedException_5), value);
	}

	inline static int32_t get_offset_of_EndPoint_6() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___EndPoint_6)); }
	inline RuntimeObject * get_EndPoint_6() const { return ___EndPoint_6; }
	inline RuntimeObject ** get_address_of_EndPoint_6() { return &___EndPoint_6; }
	inline void set_EndPoint_6(RuntimeObject * value)
	{
		___EndPoint_6 = value;
		Il2CppCodeGenWriteBarrier((&___EndPoint_6), value);
	}

	inline static int32_t get_offset_of_buffer_7() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___buffer_7)); }
	inline ByteU5BU5D_t4116647657* get_buffer_7() const { return ___buffer_7; }
	inline ByteU5BU5D_t4116647657** get_address_of_buffer_7() { return &___buffer_7; }
	inline void set_buffer_7(ByteU5BU5D_t4116647657* value)
	{
		___buffer_7 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_7), value);
	}

	inline static int32_t get_offset_of_Offset_8() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___Offset_8)); }
	inline int32_t get_Offset_8() const { return ___Offset_8; }
	inline int32_t* get_address_of_Offset_8() { return &___Offset_8; }
	inline void set_Offset_8(int32_t value)
	{
		___Offset_8 = value;
	}

	inline static int32_t get_offset_of_Size_9() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___Size_9)); }
	inline int32_t get_Size_9() const { return ___Size_9; }
	inline int32_t* get_address_of_Size_9() { return &___Size_9; }
	inline void set_Size_9(int32_t value)
	{
		___Size_9 = value;
	}

	inline static int32_t get_offset_of_SockFlags_10() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___SockFlags_10)); }
	inline int32_t get_SockFlags_10() const { return ___SockFlags_10; }
	inline int32_t* get_address_of_SockFlags_10() { return &___SockFlags_10; }
	inline void set_SockFlags_10(int32_t value)
	{
		___SockFlags_10 = value;
	}

	inline static int32_t get_offset_of_AcceptSocket_11() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___AcceptSocket_11)); }
	inline RuntimeObject * get_AcceptSocket_11() const { return ___AcceptSocket_11; }
	inline RuntimeObject ** get_address_of_AcceptSocket_11() { return &___AcceptSocket_11; }
	inline void set_AcceptSocket_11(RuntimeObject * value)
	{
		___AcceptSocket_11 = value;
		Il2CppCodeGenWriteBarrier((&___AcceptSocket_11), value);
	}

	inline static int32_t get_offset_of_Addresses_12() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___Addresses_12)); }
	inline ObjectU5BU5D_t2843939325* get_Addresses_12() const { return ___Addresses_12; }
	inline ObjectU5BU5D_t2843939325** get_address_of_Addresses_12() { return &___Addresses_12; }
	inline void set_Addresses_12(ObjectU5BU5D_t2843939325* value)
	{
		___Addresses_12 = value;
		Il2CppCodeGenWriteBarrier((&___Addresses_12), value);
	}

	inline static int32_t get_offset_of_port_13() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___port_13)); }
	inline int32_t get_port_13() const { return ___port_13; }
	inline int32_t* get_address_of_port_13() { return &___port_13; }
	inline void set_port_13(int32_t value)
	{
		___port_13 = value;
	}

	inline static int32_t get_offset_of_Buffers_14() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___Buffers_14)); }
	inline RuntimeObject * get_Buffers_14() const { return ___Buffers_14; }
	inline RuntimeObject ** get_address_of_Buffers_14() { return &___Buffers_14; }
	inline void set_Buffers_14(RuntimeObject * value)
	{
		___Buffers_14 = value;
		Il2CppCodeGenWriteBarrier((&___Buffers_14), value);
	}

	inline static int32_t get_offset_of_ReuseSocket_15() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___ReuseSocket_15)); }
	inline bool get_ReuseSocket_15() const { return ___ReuseSocket_15; }
	inline bool* get_address_of_ReuseSocket_15() { return &___ReuseSocket_15; }
	inline void set_ReuseSocket_15(bool value)
	{
		___ReuseSocket_15 = value;
	}

	inline static int32_t get_offset_of_acc_socket_16() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___acc_socket_16)); }
	inline RuntimeObject * get_acc_socket_16() const { return ___acc_socket_16; }
	inline RuntimeObject ** get_address_of_acc_socket_16() { return &___acc_socket_16; }
	inline void set_acc_socket_16(RuntimeObject * value)
	{
		___acc_socket_16 = value;
		Il2CppCodeGenWriteBarrier((&___acc_socket_16), value);
	}

	inline static int32_t get_offset_of_total_17() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___total_17)); }
	inline int32_t get_total_17() const { return ___total_17; }
	inline int32_t* get_address_of_total_17() { return &___total_17; }
	inline void set_total_17(int32_t value)
	{
		___total_17 = value;
	}

	inline static int32_t get_offset_of_completed_sync_18() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___completed_sync_18)); }
	inline bool get_completed_sync_18() const { return ___completed_sync_18; }
	inline bool* get_address_of_completed_sync_18() { return &___completed_sync_18; }
	inline void set_completed_sync_18(bool value)
	{
		___completed_sync_18 = value;
	}

	inline static int32_t get_offset_of_completed_19() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___completed_19)); }
	inline bool get_completed_19() const { return ___completed_19; }
	inline bool* get_address_of_completed_19() { return &___completed_19; }
	inline void set_completed_19(bool value)
	{
		___completed_19 = value;
	}

	inline static int32_t get_offset_of_err_out_20() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___err_out_20)); }
	inline bool get_err_out_20() const { return ___err_out_20; }
	inline bool* get_address_of_err_out_20() { return &___err_out_20; }
	inline void set_err_out_20(bool value)
	{
		___err_out_20 = value;
	}

	inline static int32_t get_offset_of_error_21() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___error_21)); }
	inline int32_t get_error_21() const { return ___error_21; }
	inline int32_t* get_address_of_error_21() { return &___error_21; }
	inline void set_error_21(int32_t value)
	{
		___error_21 = value;
	}

	inline static int32_t get_offset_of_operation_22() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___operation_22)); }
	inline int32_t get_operation_22() const { return ___operation_22; }
	inline int32_t* get_address_of_operation_22() { return &___operation_22; }
	inline void set_operation_22(int32_t value)
	{
		___operation_22 = value;
	}

	inline static int32_t get_offset_of_ares_23() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___ares_23)); }
	inline RuntimeObject * get_ares_23() const { return ___ares_23; }
	inline RuntimeObject ** get_address_of_ares_23() { return &___ares_23; }
	inline void set_ares_23(RuntimeObject * value)
	{
		___ares_23 = value;
		Il2CppCodeGenWriteBarrier((&___ares_23), value);
	}

	inline static int32_t get_offset_of_EndCalled_24() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___EndCalled_24)); }
	inline int32_t get_EndCalled_24() const { return ___EndCalled_24; }
	inline int32_t* get_address_of_EndCalled_24() { return &___EndCalled_24; }
	inline void set_EndCalled_24(int32_t value)
	{
		___EndCalled_24 = value;
	}

	inline static int32_t get_offset_of_process_25() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___process_25)); }
	inline Process_t3774297411 * get_process_25() const { return ___process_25; }
	inline Process_t3774297411 ** get_address_of_process_25() { return &___process_25; }
	inline void set_process_25(Process_t3774297411 * value)
	{
		___process_25 = value;
		Il2CppCodeGenWriteBarrier((&___process_25), value);
	}

	inline static int32_t get_offset_of_stream_26() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___stream_26)); }
	inline Stream_t1273022909 * get_stream_26() const { return ___stream_26; }
	inline Stream_t1273022909 ** get_address_of_stream_26() { return &___stream_26; }
	inline void set_stream_26(Stream_t1273022909 * value)
	{
		___stream_26 = value;
		Il2CppCodeGenWriteBarrier((&___stream_26), value);
	}

	inline static int32_t get_offset_of_sb_27() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___sb_27)); }
	inline StringBuilder_t * get_sb_27() const { return ___sb_27; }
	inline StringBuilder_t ** get_address_of_sb_27() { return &___sb_27; }
	inline void set_sb_27(StringBuilder_t * value)
	{
		___sb_27 = value;
		Il2CppCodeGenWriteBarrier((&___sb_27), value);
	}

	inline static int32_t get_offset_of_outputEncoding_28() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___outputEncoding_28)); }
	inline Encoding_t1523322056 * get_outputEncoding_28() const { return ___outputEncoding_28; }
	inline Encoding_t1523322056 ** get_address_of_outputEncoding_28() { return &___outputEncoding_28; }
	inline void set_outputEncoding_28(Encoding_t1523322056 * value)
	{
		___outputEncoding_28 = value;
		Il2CppCodeGenWriteBarrier((&___outputEncoding_28), value);
	}

	inline static int32_t get_offset_of_ReadHandler_29() { return static_cast<int32_t>(offsetof(ProcessAsyncReader_t337580163, ___ReadHandler_29)); }
	inline AsyncReadHandler_t1188682440 * get_ReadHandler_29() const { return ___ReadHandler_29; }
	inline AsyncReadHandler_t1188682440 ** get_address_of_ReadHandler_29() { return &___ReadHandler_29; }
	inline void set_ReadHandler_29(AsyncReadHandler_t1188682440 * value)
	{
		___ReadHandler_29 = value;
		Il2CppCodeGenWriteBarrier((&___ReadHandler_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Diagnostics.Process/ProcessAsyncReader
struct ProcessAsyncReader_t337580163_marshaled_pinvoke
{
	Il2CppIUnknown* ___Sock_0;
	intptr_t ___handle_1;
	Il2CppIUnknown* ___state_2;
	Il2CppMethodPointer ___callback_3;
	ManualResetEvent_t451242010 * ___wait_handle_4;
	Exception_t * ___delayedException_5;
	Il2CppIUnknown* ___EndPoint_6;
	uint8_t* ___buffer_7;
	int32_t ___Offset_8;
	int32_t ___Size_9;
	int32_t ___SockFlags_10;
	Il2CppIUnknown* ___AcceptSocket_11;
	ObjectU5BU5D_t2843939325* ___Addresses_12;
	int32_t ___port_13;
	Il2CppIUnknown* ___Buffers_14;
	int32_t ___ReuseSocket_15;
	Il2CppIUnknown* ___acc_socket_16;
	int32_t ___total_17;
	int32_t ___completed_sync_18;
	int32_t ___completed_19;
	int32_t ___err_out_20;
	int32_t ___error_21;
	int32_t ___operation_22;
	Il2CppIUnknown* ___ares_23;
	int32_t ___EndCalled_24;
	Process_t3774297411 * ___process_25;
	Stream_t1273022909 * ___stream_26;
	char* ___sb_27;
	Encoding_t1523322056 * ___outputEncoding_28;
	Il2CppMethodPointer ___ReadHandler_29;
};
// Native definition for COM marshalling of System.Diagnostics.Process/ProcessAsyncReader
struct ProcessAsyncReader_t337580163_marshaled_com
{
	Il2CppIUnknown* ___Sock_0;
	intptr_t ___handle_1;
	Il2CppIUnknown* ___state_2;
	Il2CppMethodPointer ___callback_3;
	ManualResetEvent_t451242010 * ___wait_handle_4;
	Exception_t * ___delayedException_5;
	Il2CppIUnknown* ___EndPoint_6;
	uint8_t* ___buffer_7;
	int32_t ___Offset_8;
	int32_t ___Size_9;
	int32_t ___SockFlags_10;
	Il2CppIUnknown* ___AcceptSocket_11;
	ObjectU5BU5D_t2843939325* ___Addresses_12;
	int32_t ___port_13;
	Il2CppIUnknown* ___Buffers_14;
	int32_t ___ReuseSocket_15;
	Il2CppIUnknown* ___acc_socket_16;
	int32_t ___total_17;
	int32_t ___completed_sync_18;
	int32_t ___completed_19;
	int32_t ___err_out_20;
	int32_t ___error_21;
	int32_t ___operation_22;
	Il2CppIUnknown* ___ares_23;
	int32_t ___EndCalled_24;
	Process_t3774297411 * ___process_25;
	Stream_t1273022909 * ___stream_26;
	Il2CppChar* ___sb_27;
	Encoding_t1523322056 * ___outputEncoding_28;
	Il2CppMethodPointer ___ReadHandler_29;
};
#endif // PROCESSASYNCREADER_T337580163_H
#ifndef WAITHANDLE_T1743403487_H
#define WAITHANDLE_T1743403487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.WaitHandle
struct  WaitHandle_t1743403487  : public MarshalByRefObject_t2760389100
{
public:
	// Microsoft.Win32.SafeHandles.SafeWaitHandle System.Threading.WaitHandle::safe_wait_handle
	SafeWaitHandle_t1972936122 * ___safe_wait_handle_2;
	// System.Boolean System.Threading.WaitHandle::disposed
	bool ___disposed_4;

public:
	inline static int32_t get_offset_of_safe_wait_handle_2() { return static_cast<int32_t>(offsetof(WaitHandle_t1743403487, ___safe_wait_handle_2)); }
	inline SafeWaitHandle_t1972936122 * get_safe_wait_handle_2() const { return ___safe_wait_handle_2; }
	inline SafeWaitHandle_t1972936122 ** get_address_of_safe_wait_handle_2() { return &___safe_wait_handle_2; }
	inline void set_safe_wait_handle_2(SafeWaitHandle_t1972936122 * value)
	{
		___safe_wait_handle_2 = value;
		Il2CppCodeGenWriteBarrier((&___safe_wait_handle_2), value);
	}

	inline static int32_t get_offset_of_disposed_4() { return static_cast<int32_t>(offsetof(WaitHandle_t1743403487, ___disposed_4)); }
	inline bool get_disposed_4() const { return ___disposed_4; }
	inline bool* get_address_of_disposed_4() { return &___disposed_4; }
	inline void set_disposed_4(bool value)
	{
		___disposed_4 = value;
	}
};

struct WaitHandle_t1743403487_StaticFields
{
public:
	// System.IntPtr System.Threading.WaitHandle::InvalidHandle
	intptr_t ___InvalidHandle_3;

public:
	inline static int32_t get_offset_of_InvalidHandle_3() { return static_cast<int32_t>(offsetof(WaitHandle_t1743403487_StaticFields, ___InvalidHandle_3)); }
	inline intptr_t get_InvalidHandle_3() const { return ___InvalidHandle_3; }
	inline intptr_t* get_address_of_InvalidHandle_3() { return &___InvalidHandle_3; }
	inline void set_InvalidHandle_3(intptr_t value)
	{
		___InvalidHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITHANDLE_T1743403487_H
#ifndef AUTODETECTVALUES_T1649618618_H
#define AUTODETECTVALUES_T1649618618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.ProxyElement/AutoDetectValues
struct  AutoDetectValues_t1649618618 
{
public:
	// System.Int32 System.Net.Configuration.ProxyElement/AutoDetectValues::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AutoDetectValues_t1649618618, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTODETECTVALUES_T1649618618_H
#ifndef USESYSTEMDEFAULTVALUES_T2711047072_H
#define USESYSTEMDEFAULTVALUES_T2711047072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.ProxyElement/UseSystemDefaultValues
struct  UseSystemDefaultValues_t2711047072 
{
public:
	// System.Int32 System.Net.Configuration.ProxyElement/UseSystemDefaultValues::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UseSystemDefaultValues_t2711047072, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USESYSTEMDEFAULTVALUES_T2711047072_H
#ifndef BYPASSONLOCALVALUES_T945670496_H
#define BYPASSONLOCALVALUES_T945670496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.ProxyElement/BypassOnLocalValues
struct  BypassOnLocalValues_t945670496 
{
public:
	// System.Int32 System.Net.Configuration.ProxyElement/BypassOnLocalValues::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BypassOnLocalValues_t945670496, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYPASSONLOCALVALUES_T945670496_H
#ifndef AUTHENTICATIONMODULESSECTION_T1083221556_H
#define AUTHENTICATIONMODULESSECTION_T1083221556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.AuthenticationModulesSection
struct  AuthenticationModulesSection_t1083221556  : public ConfigurationSection_t3156163955
{
public:

public:
};

struct AuthenticationModulesSection_t1083221556_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.AuthenticationModulesSection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_17;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.AuthenticationModulesSection::authenticationModulesProp
	ConfigurationProperty_t3590861854 * ___authenticationModulesProp_18;

public:
	inline static int32_t get_offset_of_properties_17() { return static_cast<int32_t>(offsetof(AuthenticationModulesSection_t1083221556_StaticFields, ___properties_17)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_17() const { return ___properties_17; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_17() { return &___properties_17; }
	inline void set_properties_17(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_17 = value;
		Il2CppCodeGenWriteBarrier((&___properties_17), value);
	}

	inline static int32_t get_offset_of_authenticationModulesProp_18() { return static_cast<int32_t>(offsetof(AuthenticationModulesSection_t1083221556_StaticFields, ___authenticationModulesProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_authenticationModulesProp_18() const { return ___authenticationModulesProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_authenticationModulesProp_18() { return &___authenticationModulesProp_18; }
	inline void set_authenticationModulesProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___authenticationModulesProp_18 = value;
		Il2CppCodeGenWriteBarrier((&___authenticationModulesProp_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONMODULESSECTION_T1083221556_H
#ifndef REQUESTCACHELEVEL_T1509648360_H
#define REQUESTCACHELEVEL_T1509648360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Cache.RequestCacheLevel
struct  RequestCacheLevel_t1509648360 
{
public:
	// System.Int32 System.Net.Cache.RequestCacheLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RequestCacheLevel_t1509648360, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTCACHELEVEL_T1509648360_H
#ifndef MONOIOERROR_T367894403_H
#define MONOIOERROR_T367894403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MonoIOError
struct  MonoIOError_t367894403 
{
public:
	// System.Int32 System.IO.MonoIOError::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MonoIOError_t367894403, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOIOERROR_T367894403_H
#ifndef AUTHENTICATIONMODULEELEMENTCOLLECTION_T1161221431_H
#define AUTHENTICATIONMODULEELEMENTCOLLECTION_T1161221431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.AuthenticationModuleElementCollection
struct  AuthenticationModuleElementCollection_t1161221431  : public ConfigurationElementCollection_t446763386
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONMODULEELEMENTCOLLECTION_T1161221431_H
#ifndef HTTPREQUESTCACHELEVEL_T1254316655_H
#define HTTPREQUESTCACHELEVEL_T1254316655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Cache.HttpRequestCacheLevel
struct  HttpRequestCacheLevel_t1254316655 
{
public:
	// System.Int32 System.Net.Cache.HttpRequestCacheLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpRequestCacheLevel_t1254316655, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPREQUESTCACHELEVEL_T1254316655_H
#ifndef CONNECTIONMANAGEMENTSECTION_T1603642748_H
#define CONNECTIONMANAGEMENTSECTION_T1603642748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.ConnectionManagementSection
struct  ConnectionManagementSection_t1603642748  : public ConfigurationSection_t3156163955
{
public:

public:
};

struct ConnectionManagementSection_t1603642748_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Net.Configuration.ConnectionManagementSection::connectionManagementProp
	ConfigurationProperty_t3590861854 * ___connectionManagementProp_17;
	// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.ConnectionManagementSection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_18;

public:
	inline static int32_t get_offset_of_connectionManagementProp_17() { return static_cast<int32_t>(offsetof(ConnectionManagementSection_t1603642748_StaticFields, ___connectionManagementProp_17)); }
	inline ConfigurationProperty_t3590861854 * get_connectionManagementProp_17() const { return ___connectionManagementProp_17; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_connectionManagementProp_17() { return &___connectionManagementProp_17; }
	inline void set_connectionManagementProp_17(ConfigurationProperty_t3590861854 * value)
	{
		___connectionManagementProp_17 = value;
		Il2CppCodeGenWriteBarrier((&___connectionManagementProp_17), value);
	}

	inline static int32_t get_offset_of_properties_18() { return static_cast<int32_t>(offsetof(ConnectionManagementSection_t1603642748_StaticFields, ___properties_18)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_18() const { return ___properties_18; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_18() { return &___properties_18; }
	inline void set_properties_18(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_18 = value;
		Il2CppCodeGenWriteBarrier((&___properties_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONMANAGEMENTSECTION_T1603642748_H
#ifndef DEFAULTPROXYSECTION_T4167594595_H
#define DEFAULTPROXYSECTION_T4167594595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.DefaultProxySection
struct  DefaultProxySection_t4167594595  : public ConfigurationSection_t3156163955
{
public:

public:
};

struct DefaultProxySection_t4167594595_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.DefaultProxySection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_17;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.DefaultProxySection::bypassListProp
	ConfigurationProperty_t3590861854 * ___bypassListProp_18;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.DefaultProxySection::enabledProp
	ConfigurationProperty_t3590861854 * ___enabledProp_19;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.DefaultProxySection::moduleProp
	ConfigurationProperty_t3590861854 * ___moduleProp_20;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.DefaultProxySection::proxyProp
	ConfigurationProperty_t3590861854 * ___proxyProp_21;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.DefaultProxySection::useDefaultCredentialsProp
	ConfigurationProperty_t3590861854 * ___useDefaultCredentialsProp_22;

public:
	inline static int32_t get_offset_of_properties_17() { return static_cast<int32_t>(offsetof(DefaultProxySection_t4167594595_StaticFields, ___properties_17)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_17() const { return ___properties_17; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_17() { return &___properties_17; }
	inline void set_properties_17(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_17 = value;
		Il2CppCodeGenWriteBarrier((&___properties_17), value);
	}

	inline static int32_t get_offset_of_bypassListProp_18() { return static_cast<int32_t>(offsetof(DefaultProxySection_t4167594595_StaticFields, ___bypassListProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_bypassListProp_18() const { return ___bypassListProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_bypassListProp_18() { return &___bypassListProp_18; }
	inline void set_bypassListProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___bypassListProp_18 = value;
		Il2CppCodeGenWriteBarrier((&___bypassListProp_18), value);
	}

	inline static int32_t get_offset_of_enabledProp_19() { return static_cast<int32_t>(offsetof(DefaultProxySection_t4167594595_StaticFields, ___enabledProp_19)); }
	inline ConfigurationProperty_t3590861854 * get_enabledProp_19() const { return ___enabledProp_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_enabledProp_19() { return &___enabledProp_19; }
	inline void set_enabledProp_19(ConfigurationProperty_t3590861854 * value)
	{
		___enabledProp_19 = value;
		Il2CppCodeGenWriteBarrier((&___enabledProp_19), value);
	}

	inline static int32_t get_offset_of_moduleProp_20() { return static_cast<int32_t>(offsetof(DefaultProxySection_t4167594595_StaticFields, ___moduleProp_20)); }
	inline ConfigurationProperty_t3590861854 * get_moduleProp_20() const { return ___moduleProp_20; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_moduleProp_20() { return &___moduleProp_20; }
	inline void set_moduleProp_20(ConfigurationProperty_t3590861854 * value)
	{
		___moduleProp_20 = value;
		Il2CppCodeGenWriteBarrier((&___moduleProp_20), value);
	}

	inline static int32_t get_offset_of_proxyProp_21() { return static_cast<int32_t>(offsetof(DefaultProxySection_t4167594595_StaticFields, ___proxyProp_21)); }
	inline ConfigurationProperty_t3590861854 * get_proxyProp_21() const { return ___proxyProp_21; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_proxyProp_21() { return &___proxyProp_21; }
	inline void set_proxyProp_21(ConfigurationProperty_t3590861854 * value)
	{
		___proxyProp_21 = value;
		Il2CppCodeGenWriteBarrier((&___proxyProp_21), value);
	}

	inline static int32_t get_offset_of_useDefaultCredentialsProp_22() { return static_cast<int32_t>(offsetof(DefaultProxySection_t4167594595_StaticFields, ___useDefaultCredentialsProp_22)); }
	inline ConfigurationProperty_t3590861854 * get_useDefaultCredentialsProp_22() const { return ___useDefaultCredentialsProp_22; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_useDefaultCredentialsProp_22() { return &___useDefaultCredentialsProp_22; }
	inline void set_useDefaultCredentialsProp_22(ConfigurationProperty_t3590861854 * value)
	{
		___useDefaultCredentialsProp_22 = value;
		Il2CppCodeGenWriteBarrier((&___useDefaultCredentialsProp_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTPROXYSECTION_T4167594595_H
#ifndef STATE_T4053927353_H
#define STATE_T4053927353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ChunkStream/State
struct  State_t4053927353 
{
public:
	// System.Int32 System.Net.ChunkStream/State::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(State_t4053927353, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T4053927353_H
#ifndef AUTHENTICATIONSCHEMES_T3459406435_H
#define AUTHENTICATIONSCHEMES_T3459406435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.AuthenticationSchemes
struct  AuthenticationSchemes_t3459406435 
{
public:
	// System.Int32 System.Net.AuthenticationSchemes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AuthenticationSchemes_t3459406435, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONSCHEMES_T3459406435_H
#ifndef CONNECTIONMANAGEMENTELEMENTCOLLECTION_T3860227195_H
#define CONNECTIONMANAGEMENTELEMENTCOLLECTION_T3860227195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.ConnectionManagementElementCollection
struct  ConnectionManagementElementCollection_t3860227195  : public ConfigurationElementCollection_t446763386
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECTIONMANAGEMENTELEMENTCOLLECTION_T3860227195_H
#ifndef WEBREQUESTMODULEELEMENTCOLLECTION_T925190782_H
#define WEBREQUESTMODULEELEMENTCOLLECTION_T925190782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.WebRequestModuleElementCollection
struct  WebRequestModuleElementCollection_t925190782  : public ConfigurationElementCollection_t446763386
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTMODULEELEMENTCOLLECTION_T925190782_H
#ifndef SETTINGSSECTION_T1259474535_H
#define SETTINGSSECTION_T1259474535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.SettingsSection
struct  SettingsSection_t1259474535  : public ConfigurationSection_t3156163955
{
public:

public:
};

struct SettingsSection_t1259474535_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.SettingsSection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_17;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.SettingsSection::httpWebRequestProp
	ConfigurationProperty_t3590861854 * ___httpWebRequestProp_18;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.SettingsSection::ipv6Prop
	ConfigurationProperty_t3590861854 * ___ipv6Prop_19;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.SettingsSection::performanceCountersProp
	ConfigurationProperty_t3590861854 * ___performanceCountersProp_20;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.SettingsSection::servicePointManagerProp
	ConfigurationProperty_t3590861854 * ___servicePointManagerProp_21;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.SettingsSection::webProxyScriptProp
	ConfigurationProperty_t3590861854 * ___webProxyScriptProp_22;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.SettingsSection::socketProp
	ConfigurationProperty_t3590861854 * ___socketProp_23;

public:
	inline static int32_t get_offset_of_properties_17() { return static_cast<int32_t>(offsetof(SettingsSection_t1259474535_StaticFields, ___properties_17)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_17() const { return ___properties_17; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_17() { return &___properties_17; }
	inline void set_properties_17(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_17 = value;
		Il2CppCodeGenWriteBarrier((&___properties_17), value);
	}

	inline static int32_t get_offset_of_httpWebRequestProp_18() { return static_cast<int32_t>(offsetof(SettingsSection_t1259474535_StaticFields, ___httpWebRequestProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_httpWebRequestProp_18() const { return ___httpWebRequestProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_httpWebRequestProp_18() { return &___httpWebRequestProp_18; }
	inline void set_httpWebRequestProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___httpWebRequestProp_18 = value;
		Il2CppCodeGenWriteBarrier((&___httpWebRequestProp_18), value);
	}

	inline static int32_t get_offset_of_ipv6Prop_19() { return static_cast<int32_t>(offsetof(SettingsSection_t1259474535_StaticFields, ___ipv6Prop_19)); }
	inline ConfigurationProperty_t3590861854 * get_ipv6Prop_19() const { return ___ipv6Prop_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_ipv6Prop_19() { return &___ipv6Prop_19; }
	inline void set_ipv6Prop_19(ConfigurationProperty_t3590861854 * value)
	{
		___ipv6Prop_19 = value;
		Il2CppCodeGenWriteBarrier((&___ipv6Prop_19), value);
	}

	inline static int32_t get_offset_of_performanceCountersProp_20() { return static_cast<int32_t>(offsetof(SettingsSection_t1259474535_StaticFields, ___performanceCountersProp_20)); }
	inline ConfigurationProperty_t3590861854 * get_performanceCountersProp_20() const { return ___performanceCountersProp_20; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_performanceCountersProp_20() { return &___performanceCountersProp_20; }
	inline void set_performanceCountersProp_20(ConfigurationProperty_t3590861854 * value)
	{
		___performanceCountersProp_20 = value;
		Il2CppCodeGenWriteBarrier((&___performanceCountersProp_20), value);
	}

	inline static int32_t get_offset_of_servicePointManagerProp_21() { return static_cast<int32_t>(offsetof(SettingsSection_t1259474535_StaticFields, ___servicePointManagerProp_21)); }
	inline ConfigurationProperty_t3590861854 * get_servicePointManagerProp_21() const { return ___servicePointManagerProp_21; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_servicePointManagerProp_21() { return &___servicePointManagerProp_21; }
	inline void set_servicePointManagerProp_21(ConfigurationProperty_t3590861854 * value)
	{
		___servicePointManagerProp_21 = value;
		Il2CppCodeGenWriteBarrier((&___servicePointManagerProp_21), value);
	}

	inline static int32_t get_offset_of_webProxyScriptProp_22() { return static_cast<int32_t>(offsetof(SettingsSection_t1259474535_StaticFields, ___webProxyScriptProp_22)); }
	inline ConfigurationProperty_t3590861854 * get_webProxyScriptProp_22() const { return ___webProxyScriptProp_22; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_webProxyScriptProp_22() { return &___webProxyScriptProp_22; }
	inline void set_webProxyScriptProp_22(ConfigurationProperty_t3590861854 * value)
	{
		___webProxyScriptProp_22 = value;
		Il2CppCodeGenWriteBarrier((&___webProxyScriptProp_22), value);
	}

	inline static int32_t get_offset_of_socketProp_23() { return static_cast<int32_t>(offsetof(SettingsSection_t1259474535_StaticFields, ___socketProp_23)); }
	inline ConfigurationProperty_t3590861854 * get_socketProp_23() const { return ___socketProp_23; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_socketProp_23() { return &___socketProp_23; }
	inline void set_socketProp_23(ConfigurationProperty_t3590861854 * value)
	{
		___socketProp_23 = value;
		Il2CppCodeGenWriteBarrier((&___socketProp_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSSECTION_T1259474535_H
#ifndef GZIPSTREAM_T3417139389_H
#define GZIPSTREAM_T3417139389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Compression.GZipStream
struct  GZipStream_t3417139389  : public Stream_t1273022909
{
public:
	// System.IO.Compression.DeflateStream System.IO.Compression.GZipStream::deflateStream
	DeflateStream_t4175168077 * ___deflateStream_2;

public:
	inline static int32_t get_offset_of_deflateStream_2() { return static_cast<int32_t>(offsetof(GZipStream_t3417139389, ___deflateStream_2)); }
	inline DeflateStream_t4175168077 * get_deflateStream_2() const { return ___deflateStream_2; }
	inline DeflateStream_t4175168077 ** get_address_of_deflateStream_2() { return &___deflateStream_2; }
	inline void set_deflateStream_2(DeflateStream_t4175168077 * value)
	{
		___deflateStream_2 = value;
		Il2CppCodeGenWriteBarrier((&___deflateStream_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GZIPSTREAM_T3417139389_H
#ifndef REQUESTCACHINGSECTION_T1693238435_H
#define REQUESTCACHINGSECTION_T1693238435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.RequestCachingSection
struct  RequestCachingSection_t1693238435  : public ConfigurationSection_t3156163955
{
public:

public:
};

struct RequestCachingSection_t1693238435_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Net.Configuration.RequestCachingSection::properties
	ConfigurationPropertyCollection_t2852175726 * ___properties_17;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.RequestCachingSection::defaultFtpCachePolicyProp
	ConfigurationProperty_t3590861854 * ___defaultFtpCachePolicyProp_18;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.RequestCachingSection::defaultHttpCachePolicyProp
	ConfigurationProperty_t3590861854 * ___defaultHttpCachePolicyProp_19;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.RequestCachingSection::defaultPolicyLevelProp
	ConfigurationProperty_t3590861854 * ___defaultPolicyLevelProp_20;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.RequestCachingSection::disableAllCachingProp
	ConfigurationProperty_t3590861854 * ___disableAllCachingProp_21;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.RequestCachingSection::isPrivateCacheProp
	ConfigurationProperty_t3590861854 * ___isPrivateCacheProp_22;
	// System.Configuration.ConfigurationProperty System.Net.Configuration.RequestCachingSection::unspecifiedMaximumAgeProp
	ConfigurationProperty_t3590861854 * ___unspecifiedMaximumAgeProp_23;

public:
	inline static int32_t get_offset_of_properties_17() { return static_cast<int32_t>(offsetof(RequestCachingSection_t1693238435_StaticFields, ___properties_17)); }
	inline ConfigurationPropertyCollection_t2852175726 * get_properties_17() const { return ___properties_17; }
	inline ConfigurationPropertyCollection_t2852175726 ** get_address_of_properties_17() { return &___properties_17; }
	inline void set_properties_17(ConfigurationPropertyCollection_t2852175726 * value)
	{
		___properties_17 = value;
		Il2CppCodeGenWriteBarrier((&___properties_17), value);
	}

	inline static int32_t get_offset_of_defaultFtpCachePolicyProp_18() { return static_cast<int32_t>(offsetof(RequestCachingSection_t1693238435_StaticFields, ___defaultFtpCachePolicyProp_18)); }
	inline ConfigurationProperty_t3590861854 * get_defaultFtpCachePolicyProp_18() const { return ___defaultFtpCachePolicyProp_18; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_defaultFtpCachePolicyProp_18() { return &___defaultFtpCachePolicyProp_18; }
	inline void set_defaultFtpCachePolicyProp_18(ConfigurationProperty_t3590861854 * value)
	{
		___defaultFtpCachePolicyProp_18 = value;
		Il2CppCodeGenWriteBarrier((&___defaultFtpCachePolicyProp_18), value);
	}

	inline static int32_t get_offset_of_defaultHttpCachePolicyProp_19() { return static_cast<int32_t>(offsetof(RequestCachingSection_t1693238435_StaticFields, ___defaultHttpCachePolicyProp_19)); }
	inline ConfigurationProperty_t3590861854 * get_defaultHttpCachePolicyProp_19() const { return ___defaultHttpCachePolicyProp_19; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_defaultHttpCachePolicyProp_19() { return &___defaultHttpCachePolicyProp_19; }
	inline void set_defaultHttpCachePolicyProp_19(ConfigurationProperty_t3590861854 * value)
	{
		___defaultHttpCachePolicyProp_19 = value;
		Il2CppCodeGenWriteBarrier((&___defaultHttpCachePolicyProp_19), value);
	}

	inline static int32_t get_offset_of_defaultPolicyLevelProp_20() { return static_cast<int32_t>(offsetof(RequestCachingSection_t1693238435_StaticFields, ___defaultPolicyLevelProp_20)); }
	inline ConfigurationProperty_t3590861854 * get_defaultPolicyLevelProp_20() const { return ___defaultPolicyLevelProp_20; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_defaultPolicyLevelProp_20() { return &___defaultPolicyLevelProp_20; }
	inline void set_defaultPolicyLevelProp_20(ConfigurationProperty_t3590861854 * value)
	{
		___defaultPolicyLevelProp_20 = value;
		Il2CppCodeGenWriteBarrier((&___defaultPolicyLevelProp_20), value);
	}

	inline static int32_t get_offset_of_disableAllCachingProp_21() { return static_cast<int32_t>(offsetof(RequestCachingSection_t1693238435_StaticFields, ___disableAllCachingProp_21)); }
	inline ConfigurationProperty_t3590861854 * get_disableAllCachingProp_21() const { return ___disableAllCachingProp_21; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_disableAllCachingProp_21() { return &___disableAllCachingProp_21; }
	inline void set_disableAllCachingProp_21(ConfigurationProperty_t3590861854 * value)
	{
		___disableAllCachingProp_21 = value;
		Il2CppCodeGenWriteBarrier((&___disableAllCachingProp_21), value);
	}

	inline static int32_t get_offset_of_isPrivateCacheProp_22() { return static_cast<int32_t>(offsetof(RequestCachingSection_t1693238435_StaticFields, ___isPrivateCacheProp_22)); }
	inline ConfigurationProperty_t3590861854 * get_isPrivateCacheProp_22() const { return ___isPrivateCacheProp_22; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_isPrivateCacheProp_22() { return &___isPrivateCacheProp_22; }
	inline void set_isPrivateCacheProp_22(ConfigurationProperty_t3590861854 * value)
	{
		___isPrivateCacheProp_22 = value;
		Il2CppCodeGenWriteBarrier((&___isPrivateCacheProp_22), value);
	}

	inline static int32_t get_offset_of_unspecifiedMaximumAgeProp_23() { return static_cast<int32_t>(offsetof(RequestCachingSection_t1693238435_StaticFields, ___unspecifiedMaximumAgeProp_23)); }
	inline ConfigurationProperty_t3590861854 * get_unspecifiedMaximumAgeProp_23() const { return ___unspecifiedMaximumAgeProp_23; }
	inline ConfigurationProperty_t3590861854 ** get_address_of_unspecifiedMaximumAgeProp_23() { return &___unspecifiedMaximumAgeProp_23; }
	inline void set_unspecifiedMaximumAgeProp_23(ConfigurationProperty_t3590861854 * value)
	{
		___unspecifiedMaximumAgeProp_23 = value;
		Il2CppCodeGenWriteBarrier((&___unspecifiedMaximumAgeProp_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTCACHINGSECTION_T1693238435_H
#ifndef BYPASSELEMENTCOLLECTION_T47326401_H
#define BYPASSELEMENTCOLLECTION_T47326401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Configuration.BypassElementCollection
struct  BypassElementCollection_t47326401  : public ConfigurationElementCollection_t446763386
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYPASSELEMENTCOLLECTION_T47326401_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_10)); }
	inline TimeSpan_t881159249  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t881159249 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t881159249  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_12)); }
	inline DateTime_t3738529785  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t3738529785  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_13)); }
	inline DateTime_t3738529785  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t3738529785 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t3738529785  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef CHUNKSTREAM_T2634567336_H
#define CHUNKSTREAM_T2634567336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ChunkStream
struct  ChunkStream_t2634567336  : public RuntimeObject
{
public:
	// System.Net.WebHeaderCollection System.Net.ChunkStream::headers
	WebHeaderCollection_t1942268960 * ___headers_0;
	// System.Int32 System.Net.ChunkStream::chunkSize
	int32_t ___chunkSize_1;
	// System.Int32 System.Net.ChunkStream::chunkRead
	int32_t ___chunkRead_2;
	// System.Net.ChunkStream/State System.Net.ChunkStream::state
	int32_t ___state_3;
	// System.Text.StringBuilder System.Net.ChunkStream::saved
	StringBuilder_t * ___saved_4;
	// System.Boolean System.Net.ChunkStream::sawCR
	bool ___sawCR_5;
	// System.Boolean System.Net.ChunkStream::gotit
	bool ___gotit_6;
	// System.Int32 System.Net.ChunkStream::trailerState
	int32_t ___trailerState_7;
	// System.Collections.ArrayList System.Net.ChunkStream::chunks
	ArrayList_t2718874744 * ___chunks_8;

public:
	inline static int32_t get_offset_of_headers_0() { return static_cast<int32_t>(offsetof(ChunkStream_t2634567336, ___headers_0)); }
	inline WebHeaderCollection_t1942268960 * get_headers_0() const { return ___headers_0; }
	inline WebHeaderCollection_t1942268960 ** get_address_of_headers_0() { return &___headers_0; }
	inline void set_headers_0(WebHeaderCollection_t1942268960 * value)
	{
		___headers_0 = value;
		Il2CppCodeGenWriteBarrier((&___headers_0), value);
	}

	inline static int32_t get_offset_of_chunkSize_1() { return static_cast<int32_t>(offsetof(ChunkStream_t2634567336, ___chunkSize_1)); }
	inline int32_t get_chunkSize_1() const { return ___chunkSize_1; }
	inline int32_t* get_address_of_chunkSize_1() { return &___chunkSize_1; }
	inline void set_chunkSize_1(int32_t value)
	{
		___chunkSize_1 = value;
	}

	inline static int32_t get_offset_of_chunkRead_2() { return static_cast<int32_t>(offsetof(ChunkStream_t2634567336, ___chunkRead_2)); }
	inline int32_t get_chunkRead_2() const { return ___chunkRead_2; }
	inline int32_t* get_address_of_chunkRead_2() { return &___chunkRead_2; }
	inline void set_chunkRead_2(int32_t value)
	{
		___chunkRead_2 = value;
	}

	inline static int32_t get_offset_of_state_3() { return static_cast<int32_t>(offsetof(ChunkStream_t2634567336, ___state_3)); }
	inline int32_t get_state_3() const { return ___state_3; }
	inline int32_t* get_address_of_state_3() { return &___state_3; }
	inline void set_state_3(int32_t value)
	{
		___state_3 = value;
	}

	inline static int32_t get_offset_of_saved_4() { return static_cast<int32_t>(offsetof(ChunkStream_t2634567336, ___saved_4)); }
	inline StringBuilder_t * get_saved_4() const { return ___saved_4; }
	inline StringBuilder_t ** get_address_of_saved_4() { return &___saved_4; }
	inline void set_saved_4(StringBuilder_t * value)
	{
		___saved_4 = value;
		Il2CppCodeGenWriteBarrier((&___saved_4), value);
	}

	inline static int32_t get_offset_of_sawCR_5() { return static_cast<int32_t>(offsetof(ChunkStream_t2634567336, ___sawCR_5)); }
	inline bool get_sawCR_5() const { return ___sawCR_5; }
	inline bool* get_address_of_sawCR_5() { return &___sawCR_5; }
	inline void set_sawCR_5(bool value)
	{
		___sawCR_5 = value;
	}

	inline static int32_t get_offset_of_gotit_6() { return static_cast<int32_t>(offsetof(ChunkStream_t2634567336, ___gotit_6)); }
	inline bool get_gotit_6() const { return ___gotit_6; }
	inline bool* get_address_of_gotit_6() { return &___gotit_6; }
	inline void set_gotit_6(bool value)
	{
		___gotit_6 = value;
	}

	inline static int32_t get_offset_of_trailerState_7() { return static_cast<int32_t>(offsetof(ChunkStream_t2634567336, ___trailerState_7)); }
	inline int32_t get_trailerState_7() const { return ___trailerState_7; }
	inline int32_t* get_address_of_trailerState_7() { return &___trailerState_7; }
	inline void set_trailerState_7(int32_t value)
	{
		___trailerState_7 = value;
	}

	inline static int32_t get_offset_of_chunks_8() { return static_cast<int32_t>(offsetof(ChunkStream_t2634567336, ___chunks_8)); }
	inline ArrayList_t2718874744 * get_chunks_8() const { return ___chunks_8; }
	inline ArrayList_t2718874744 ** get_address_of_chunks_8() { return &___chunks_8; }
	inline void set_chunks_8(ArrayList_t2718874744 * value)
	{
		___chunks_8 = value;
		Il2CppCodeGenWriteBarrier((&___chunks_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHUNKSTREAM_T2634567336_H
#ifndef FILESTREAM_T4292183065_H
#define FILESTREAM_T4292183065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.FileStream
struct  FileStream_t4292183065  : public Stream_t1273022909
{
public:
	// System.IO.FileAccess System.IO.FileStream::access
	int32_t ___access_2;
	// System.Boolean System.IO.FileStream::owner
	bool ___owner_3;
	// System.Boolean System.IO.FileStream::async
	bool ___async_4;
	// System.Boolean System.IO.FileStream::canseek
	bool ___canseek_5;
	// System.Int64 System.IO.FileStream::append_startpos
	int64_t ___append_startpos_6;
	// System.Boolean System.IO.FileStream::anonymous
	bool ___anonymous_7;
	// System.Byte[] System.IO.FileStream::buf
	ByteU5BU5D_t4116647657* ___buf_8;
	// System.Int32 System.IO.FileStream::buf_size
	int32_t ___buf_size_9;
	// System.Int32 System.IO.FileStream::buf_length
	int32_t ___buf_length_10;
	// System.Int32 System.IO.FileStream::buf_offset
	int32_t ___buf_offset_11;
	// System.Boolean System.IO.FileStream::buf_dirty
	bool ___buf_dirty_12;
	// System.Int64 System.IO.FileStream::buf_start
	int64_t ___buf_start_13;
	// System.String System.IO.FileStream::name
	String_t* ___name_14;
	// System.IntPtr System.IO.FileStream::handle
	intptr_t ___handle_15;

public:
	inline static int32_t get_offset_of_access_2() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___access_2)); }
	inline int32_t get_access_2() const { return ___access_2; }
	inline int32_t* get_address_of_access_2() { return &___access_2; }
	inline void set_access_2(int32_t value)
	{
		___access_2 = value;
	}

	inline static int32_t get_offset_of_owner_3() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___owner_3)); }
	inline bool get_owner_3() const { return ___owner_3; }
	inline bool* get_address_of_owner_3() { return &___owner_3; }
	inline void set_owner_3(bool value)
	{
		___owner_3 = value;
	}

	inline static int32_t get_offset_of_async_4() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___async_4)); }
	inline bool get_async_4() const { return ___async_4; }
	inline bool* get_address_of_async_4() { return &___async_4; }
	inline void set_async_4(bool value)
	{
		___async_4 = value;
	}

	inline static int32_t get_offset_of_canseek_5() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___canseek_5)); }
	inline bool get_canseek_5() const { return ___canseek_5; }
	inline bool* get_address_of_canseek_5() { return &___canseek_5; }
	inline void set_canseek_5(bool value)
	{
		___canseek_5 = value;
	}

	inline static int32_t get_offset_of_append_startpos_6() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___append_startpos_6)); }
	inline int64_t get_append_startpos_6() const { return ___append_startpos_6; }
	inline int64_t* get_address_of_append_startpos_6() { return &___append_startpos_6; }
	inline void set_append_startpos_6(int64_t value)
	{
		___append_startpos_6 = value;
	}

	inline static int32_t get_offset_of_anonymous_7() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___anonymous_7)); }
	inline bool get_anonymous_7() const { return ___anonymous_7; }
	inline bool* get_address_of_anonymous_7() { return &___anonymous_7; }
	inline void set_anonymous_7(bool value)
	{
		___anonymous_7 = value;
	}

	inline static int32_t get_offset_of_buf_8() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___buf_8)); }
	inline ByteU5BU5D_t4116647657* get_buf_8() const { return ___buf_8; }
	inline ByteU5BU5D_t4116647657** get_address_of_buf_8() { return &___buf_8; }
	inline void set_buf_8(ByteU5BU5D_t4116647657* value)
	{
		___buf_8 = value;
		Il2CppCodeGenWriteBarrier((&___buf_8), value);
	}

	inline static int32_t get_offset_of_buf_size_9() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___buf_size_9)); }
	inline int32_t get_buf_size_9() const { return ___buf_size_9; }
	inline int32_t* get_address_of_buf_size_9() { return &___buf_size_9; }
	inline void set_buf_size_9(int32_t value)
	{
		___buf_size_9 = value;
	}

	inline static int32_t get_offset_of_buf_length_10() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___buf_length_10)); }
	inline int32_t get_buf_length_10() const { return ___buf_length_10; }
	inline int32_t* get_address_of_buf_length_10() { return &___buf_length_10; }
	inline void set_buf_length_10(int32_t value)
	{
		___buf_length_10 = value;
	}

	inline static int32_t get_offset_of_buf_offset_11() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___buf_offset_11)); }
	inline int32_t get_buf_offset_11() const { return ___buf_offset_11; }
	inline int32_t* get_address_of_buf_offset_11() { return &___buf_offset_11; }
	inline void set_buf_offset_11(int32_t value)
	{
		___buf_offset_11 = value;
	}

	inline static int32_t get_offset_of_buf_dirty_12() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___buf_dirty_12)); }
	inline bool get_buf_dirty_12() const { return ___buf_dirty_12; }
	inline bool* get_address_of_buf_dirty_12() { return &___buf_dirty_12; }
	inline void set_buf_dirty_12(bool value)
	{
		___buf_dirty_12 = value;
	}

	inline static int32_t get_offset_of_buf_start_13() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___buf_start_13)); }
	inline int64_t get_buf_start_13() const { return ___buf_start_13; }
	inline int64_t* get_address_of_buf_start_13() { return &___buf_start_13; }
	inline void set_buf_start_13(int64_t value)
	{
		___buf_start_13 = value;
	}

	inline static int32_t get_offset_of_name_14() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___name_14)); }
	inline String_t* get_name_14() const { return ___name_14; }
	inline String_t** get_address_of_name_14() { return &___name_14; }
	inline void set_name_14(String_t* value)
	{
		___name_14 = value;
		Il2CppCodeGenWriteBarrier((&___name_14), value);
	}

	inline static int32_t get_offset_of_handle_15() { return static_cast<int32_t>(offsetof(FileStream_t4292183065, ___handle_15)); }
	inline intptr_t get_handle_15() const { return ___handle_15; }
	inline intptr_t* get_address_of_handle_15() { return &___handle_15; }
	inline void set_handle_15(intptr_t value)
	{
		___handle_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILESTREAM_T4292183065_H
#ifndef CHUNKEDINPUTSTREAM_T1889541612_H
#define CHUNKEDINPUTSTREAM_T1889541612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.ChunkedInputStream
struct  ChunkedInputStream_t1889541612  : public RequestStream_t762880582
{
public:
	// System.Boolean System.Net.ChunkedInputStream::disposed
	bool ___disposed_8;
	// System.Net.ChunkStream System.Net.ChunkedInputStream::decoder
	ChunkStream_t2634567336 * ___decoder_9;
	// System.Net.HttpListenerContext System.Net.ChunkedInputStream::context
	HttpListenerContext_t424880822 * ___context_10;
	// System.Boolean System.Net.ChunkedInputStream::no_more_data
	bool ___no_more_data_11;

public:
	inline static int32_t get_offset_of_disposed_8() { return static_cast<int32_t>(offsetof(ChunkedInputStream_t1889541612, ___disposed_8)); }
	inline bool get_disposed_8() const { return ___disposed_8; }
	inline bool* get_address_of_disposed_8() { return &___disposed_8; }
	inline void set_disposed_8(bool value)
	{
		___disposed_8 = value;
	}

	inline static int32_t get_offset_of_decoder_9() { return static_cast<int32_t>(offsetof(ChunkedInputStream_t1889541612, ___decoder_9)); }
	inline ChunkStream_t2634567336 * get_decoder_9() const { return ___decoder_9; }
	inline ChunkStream_t2634567336 ** get_address_of_decoder_9() { return &___decoder_9; }
	inline void set_decoder_9(ChunkStream_t2634567336 * value)
	{
		___decoder_9 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_9), value);
	}

	inline static int32_t get_offset_of_context_10() { return static_cast<int32_t>(offsetof(ChunkedInputStream_t1889541612, ___context_10)); }
	inline HttpListenerContext_t424880822 * get_context_10() const { return ___context_10; }
	inline HttpListenerContext_t424880822 ** get_address_of_context_10() { return &___context_10; }
	inline void set_context_10(HttpListenerContext_t424880822 * value)
	{
		___context_10 = value;
		Il2CppCodeGenWriteBarrier((&___context_10), value);
	}

	inline static int32_t get_offset_of_no_more_data_11() { return static_cast<int32_t>(offsetof(ChunkedInputStream_t1889541612, ___no_more_data_11)); }
	inline bool get_no_more_data_11() const { return ___no_more_data_11; }
	inline bool* get_address_of_no_more_data_11() { return &___no_more_data_11; }
	inline void set_no_more_data_11(bool value)
	{
		___no_more_data_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHUNKEDINPUTSTREAM_T1889541612_H
#ifndef PROCESS_T3774297411_H
#define PROCESS_T3774297411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.Process
struct  Process_t3774297411  : public Component_t3620823400
{
public:
	// System.IntPtr System.Diagnostics.Process::process_handle
	intptr_t ___process_handle_4;
	// System.Int32 System.Diagnostics.Process::pid
	int32_t ___pid_5;
	// System.Boolean System.Diagnostics.Process::enableRaisingEvents
	bool ___enableRaisingEvents_6;
	// System.Boolean System.Diagnostics.Process::already_waiting
	bool ___already_waiting_7;
	// System.ComponentModel.ISynchronizeInvoke System.Diagnostics.Process::synchronizingObject
	RuntimeObject* ___synchronizingObject_8;
	// System.EventHandler System.Diagnostics.Process::exited_event
	EventHandler_t1348719766 * ___exited_event_9;
	// System.IntPtr System.Diagnostics.Process::stdout_rd
	intptr_t ___stdout_rd_10;
	// System.IntPtr System.Diagnostics.Process::stderr_rd
	intptr_t ___stderr_rd_11;
	// System.Diagnostics.ProcessModuleCollection System.Diagnostics.Process::module_collection
	ProcessModuleCollection_t3446348346 * ___module_collection_12;
	// System.String System.Diagnostics.Process::process_name
	String_t* ___process_name_13;
	// System.IO.StreamReader System.Diagnostics.Process::error_stream
	StreamReader_t4009935899 * ___error_stream_14;
	// System.IO.StreamWriter System.Diagnostics.Process::input_stream
	StreamWriter_t1266378904 * ___input_stream_15;
	// System.IO.StreamReader System.Diagnostics.Process::output_stream
	StreamReader_t4009935899 * ___output_stream_16;
	// System.Diagnostics.ProcessStartInfo System.Diagnostics.Process::start_info
	ProcessStartInfo_t2184852744 * ___start_info_17;
	// System.Diagnostics.Process/AsyncModes System.Diagnostics.Process::async_mode
	int32_t ___async_mode_18;
	// System.Boolean System.Diagnostics.Process::output_canceled
	bool ___output_canceled_19;
	// System.Boolean System.Diagnostics.Process::error_canceled
	bool ___error_canceled_20;
	// System.Diagnostics.Process/ProcessAsyncReader System.Diagnostics.Process::async_output
	ProcessAsyncReader_t337580163 * ___async_output_21;
	// System.Diagnostics.Process/ProcessAsyncReader System.Diagnostics.Process::async_error
	ProcessAsyncReader_t337580163 * ___async_error_22;
	// System.Boolean System.Diagnostics.Process::disposed
	bool ___disposed_23;
	// System.Diagnostics.DataReceivedEventHandler System.Diagnostics.Process::OutputDataReceived
	DataReceivedEventHandler_t2795960821 * ___OutputDataReceived_24;
	// System.Diagnostics.DataReceivedEventHandler System.Diagnostics.Process::ErrorDataReceived
	DataReceivedEventHandler_t2795960821 * ___ErrorDataReceived_25;

public:
	inline static int32_t get_offset_of_process_handle_4() { return static_cast<int32_t>(offsetof(Process_t3774297411, ___process_handle_4)); }
	inline intptr_t get_process_handle_4() const { return ___process_handle_4; }
	inline intptr_t* get_address_of_process_handle_4() { return &___process_handle_4; }
	inline void set_process_handle_4(intptr_t value)
	{
		___process_handle_4 = value;
	}

	inline static int32_t get_offset_of_pid_5() { return static_cast<int32_t>(offsetof(Process_t3774297411, ___pid_5)); }
	inline int32_t get_pid_5() const { return ___pid_5; }
	inline int32_t* get_address_of_pid_5() { return &___pid_5; }
	inline void set_pid_5(int32_t value)
	{
		___pid_5 = value;
	}

	inline static int32_t get_offset_of_enableRaisingEvents_6() { return static_cast<int32_t>(offsetof(Process_t3774297411, ___enableRaisingEvents_6)); }
	inline bool get_enableRaisingEvents_6() const { return ___enableRaisingEvents_6; }
	inline bool* get_address_of_enableRaisingEvents_6() { return &___enableRaisingEvents_6; }
	inline void set_enableRaisingEvents_6(bool value)
	{
		___enableRaisingEvents_6 = value;
	}

	inline static int32_t get_offset_of_already_waiting_7() { return static_cast<int32_t>(offsetof(Process_t3774297411, ___already_waiting_7)); }
	inline bool get_already_waiting_7() const { return ___already_waiting_7; }
	inline bool* get_address_of_already_waiting_7() { return &___already_waiting_7; }
	inline void set_already_waiting_7(bool value)
	{
		___already_waiting_7 = value;
	}

	inline static int32_t get_offset_of_synchronizingObject_8() { return static_cast<int32_t>(offsetof(Process_t3774297411, ___synchronizingObject_8)); }
	inline RuntimeObject* get_synchronizingObject_8() const { return ___synchronizingObject_8; }
	inline RuntimeObject** get_address_of_synchronizingObject_8() { return &___synchronizingObject_8; }
	inline void set_synchronizingObject_8(RuntimeObject* value)
	{
		___synchronizingObject_8 = value;
		Il2CppCodeGenWriteBarrier((&___synchronizingObject_8), value);
	}

	inline static int32_t get_offset_of_exited_event_9() { return static_cast<int32_t>(offsetof(Process_t3774297411, ___exited_event_9)); }
	inline EventHandler_t1348719766 * get_exited_event_9() const { return ___exited_event_9; }
	inline EventHandler_t1348719766 ** get_address_of_exited_event_9() { return &___exited_event_9; }
	inline void set_exited_event_9(EventHandler_t1348719766 * value)
	{
		___exited_event_9 = value;
		Il2CppCodeGenWriteBarrier((&___exited_event_9), value);
	}

	inline static int32_t get_offset_of_stdout_rd_10() { return static_cast<int32_t>(offsetof(Process_t3774297411, ___stdout_rd_10)); }
	inline intptr_t get_stdout_rd_10() const { return ___stdout_rd_10; }
	inline intptr_t* get_address_of_stdout_rd_10() { return &___stdout_rd_10; }
	inline void set_stdout_rd_10(intptr_t value)
	{
		___stdout_rd_10 = value;
	}

	inline static int32_t get_offset_of_stderr_rd_11() { return static_cast<int32_t>(offsetof(Process_t3774297411, ___stderr_rd_11)); }
	inline intptr_t get_stderr_rd_11() const { return ___stderr_rd_11; }
	inline intptr_t* get_address_of_stderr_rd_11() { return &___stderr_rd_11; }
	inline void set_stderr_rd_11(intptr_t value)
	{
		___stderr_rd_11 = value;
	}

	inline static int32_t get_offset_of_module_collection_12() { return static_cast<int32_t>(offsetof(Process_t3774297411, ___module_collection_12)); }
	inline ProcessModuleCollection_t3446348346 * get_module_collection_12() const { return ___module_collection_12; }
	inline ProcessModuleCollection_t3446348346 ** get_address_of_module_collection_12() { return &___module_collection_12; }
	inline void set_module_collection_12(ProcessModuleCollection_t3446348346 * value)
	{
		___module_collection_12 = value;
		Il2CppCodeGenWriteBarrier((&___module_collection_12), value);
	}

	inline static int32_t get_offset_of_process_name_13() { return static_cast<int32_t>(offsetof(Process_t3774297411, ___process_name_13)); }
	inline String_t* get_process_name_13() const { return ___process_name_13; }
	inline String_t** get_address_of_process_name_13() { return &___process_name_13; }
	inline void set_process_name_13(String_t* value)
	{
		___process_name_13 = value;
		Il2CppCodeGenWriteBarrier((&___process_name_13), value);
	}

	inline static int32_t get_offset_of_error_stream_14() { return static_cast<int32_t>(offsetof(Process_t3774297411, ___error_stream_14)); }
	inline StreamReader_t4009935899 * get_error_stream_14() const { return ___error_stream_14; }
	inline StreamReader_t4009935899 ** get_address_of_error_stream_14() { return &___error_stream_14; }
	inline void set_error_stream_14(StreamReader_t4009935899 * value)
	{
		___error_stream_14 = value;
		Il2CppCodeGenWriteBarrier((&___error_stream_14), value);
	}

	inline static int32_t get_offset_of_input_stream_15() { return static_cast<int32_t>(offsetof(Process_t3774297411, ___input_stream_15)); }
	inline StreamWriter_t1266378904 * get_input_stream_15() const { return ___input_stream_15; }
	inline StreamWriter_t1266378904 ** get_address_of_input_stream_15() { return &___input_stream_15; }
	inline void set_input_stream_15(StreamWriter_t1266378904 * value)
	{
		___input_stream_15 = value;
		Il2CppCodeGenWriteBarrier((&___input_stream_15), value);
	}

	inline static int32_t get_offset_of_output_stream_16() { return static_cast<int32_t>(offsetof(Process_t3774297411, ___output_stream_16)); }
	inline StreamReader_t4009935899 * get_output_stream_16() const { return ___output_stream_16; }
	inline StreamReader_t4009935899 ** get_address_of_output_stream_16() { return &___output_stream_16; }
	inline void set_output_stream_16(StreamReader_t4009935899 * value)
	{
		___output_stream_16 = value;
		Il2CppCodeGenWriteBarrier((&___output_stream_16), value);
	}

	inline static int32_t get_offset_of_start_info_17() { return static_cast<int32_t>(offsetof(Process_t3774297411, ___start_info_17)); }
	inline ProcessStartInfo_t2184852744 * get_start_info_17() const { return ___start_info_17; }
	inline ProcessStartInfo_t2184852744 ** get_address_of_start_info_17() { return &___start_info_17; }
	inline void set_start_info_17(ProcessStartInfo_t2184852744 * value)
	{
		___start_info_17 = value;
		Il2CppCodeGenWriteBarrier((&___start_info_17), value);
	}

	inline static int32_t get_offset_of_async_mode_18() { return static_cast<int32_t>(offsetof(Process_t3774297411, ___async_mode_18)); }
	inline int32_t get_async_mode_18() const { return ___async_mode_18; }
	inline int32_t* get_address_of_async_mode_18() { return &___async_mode_18; }
	inline void set_async_mode_18(int32_t value)
	{
		___async_mode_18 = value;
	}

	inline static int32_t get_offset_of_output_canceled_19() { return static_cast<int32_t>(offsetof(Process_t3774297411, ___output_canceled_19)); }
	inline bool get_output_canceled_19() const { return ___output_canceled_19; }
	inline bool* get_address_of_output_canceled_19() { return &___output_canceled_19; }
	inline void set_output_canceled_19(bool value)
	{
		___output_canceled_19 = value;
	}

	inline static int32_t get_offset_of_error_canceled_20() { return static_cast<int32_t>(offsetof(Process_t3774297411, ___error_canceled_20)); }
	inline bool get_error_canceled_20() const { return ___error_canceled_20; }
	inline bool* get_address_of_error_canceled_20() { return &___error_canceled_20; }
	inline void set_error_canceled_20(bool value)
	{
		___error_canceled_20 = value;
	}

	inline static int32_t get_offset_of_async_output_21() { return static_cast<int32_t>(offsetof(Process_t3774297411, ___async_output_21)); }
	inline ProcessAsyncReader_t337580163 * get_async_output_21() const { return ___async_output_21; }
	inline ProcessAsyncReader_t337580163 ** get_address_of_async_output_21() { return &___async_output_21; }
	inline void set_async_output_21(ProcessAsyncReader_t337580163 * value)
	{
		___async_output_21 = value;
		Il2CppCodeGenWriteBarrier((&___async_output_21), value);
	}

	inline static int32_t get_offset_of_async_error_22() { return static_cast<int32_t>(offsetof(Process_t3774297411, ___async_error_22)); }
	inline ProcessAsyncReader_t337580163 * get_async_error_22() const { return ___async_error_22; }
	inline ProcessAsyncReader_t337580163 ** get_address_of_async_error_22() { return &___async_error_22; }
	inline void set_async_error_22(ProcessAsyncReader_t337580163 * value)
	{
		___async_error_22 = value;
		Il2CppCodeGenWriteBarrier((&___async_error_22), value);
	}

	inline static int32_t get_offset_of_disposed_23() { return static_cast<int32_t>(offsetof(Process_t3774297411, ___disposed_23)); }
	inline bool get_disposed_23() const { return ___disposed_23; }
	inline bool* get_address_of_disposed_23() { return &___disposed_23; }
	inline void set_disposed_23(bool value)
	{
		___disposed_23 = value;
	}

	inline static int32_t get_offset_of_OutputDataReceived_24() { return static_cast<int32_t>(offsetof(Process_t3774297411, ___OutputDataReceived_24)); }
	inline DataReceivedEventHandler_t2795960821 * get_OutputDataReceived_24() const { return ___OutputDataReceived_24; }
	inline DataReceivedEventHandler_t2795960821 ** get_address_of_OutputDataReceived_24() { return &___OutputDataReceived_24; }
	inline void set_OutputDataReceived_24(DataReceivedEventHandler_t2795960821 * value)
	{
		___OutputDataReceived_24 = value;
		Il2CppCodeGenWriteBarrier((&___OutputDataReceived_24), value);
	}

	inline static int32_t get_offset_of_ErrorDataReceived_25() { return static_cast<int32_t>(offsetof(Process_t3774297411, ___ErrorDataReceived_25)); }
	inline DataReceivedEventHandler_t2795960821 * get_ErrorDataReceived_25() const { return ___ErrorDataReceived_25; }
	inline DataReceivedEventHandler_t2795960821 ** get_address_of_ErrorDataReceived_25() { return &___ErrorDataReceived_25; }
	inline void set_ErrorDataReceived_25(DataReceivedEventHandler_t2795960821 * value)
	{
		___ErrorDataReceived_25 = value;
		Il2CppCodeGenWriteBarrier((&___ErrorDataReceived_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROCESS_T3774297411_H
#ifndef COOKIEEXCEPTION_T2325395694_H
#define COOKIEEXCEPTION_T2325395694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.CookieException
struct  CookieException_t2325395694  : public FormatException_t154580423
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIEEXCEPTION_T2325395694_H
#ifndef DEFLATESTREAM_T4175168077_H
#define DEFLATESTREAM_T4175168077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Compression.DeflateStream
struct  DeflateStream_t4175168077  : public Stream_t1273022909
{
public:
	// System.IO.Stream System.IO.Compression.DeflateStream::base_stream
	Stream_t1273022909 * ___base_stream_2;
	// System.IO.Compression.CompressionMode System.IO.Compression.DeflateStream::mode
	int32_t ___mode_3;
	// System.Boolean System.IO.Compression.DeflateStream::leaveOpen
	bool ___leaveOpen_4;
	// System.Boolean System.IO.Compression.DeflateStream::disposed
	bool ___disposed_5;
	// System.IO.Compression.DeflateStream/UnmanagedReadOrWrite System.IO.Compression.DeflateStream::feeder
	UnmanagedReadOrWrite_t876388624 * ___feeder_6;
	// System.IntPtr System.IO.Compression.DeflateStream::z_stream
	intptr_t ___z_stream_7;
	// System.Byte[] System.IO.Compression.DeflateStream::io_buffer
	ByteU5BU5D_t4116647657* ___io_buffer_8;
	// System.Runtime.InteropServices.GCHandle System.IO.Compression.DeflateStream::data
	GCHandle_t3351438187  ___data_9;

public:
	inline static int32_t get_offset_of_base_stream_2() { return static_cast<int32_t>(offsetof(DeflateStream_t4175168077, ___base_stream_2)); }
	inline Stream_t1273022909 * get_base_stream_2() const { return ___base_stream_2; }
	inline Stream_t1273022909 ** get_address_of_base_stream_2() { return &___base_stream_2; }
	inline void set_base_stream_2(Stream_t1273022909 * value)
	{
		___base_stream_2 = value;
		Il2CppCodeGenWriteBarrier((&___base_stream_2), value);
	}

	inline static int32_t get_offset_of_mode_3() { return static_cast<int32_t>(offsetof(DeflateStream_t4175168077, ___mode_3)); }
	inline int32_t get_mode_3() const { return ___mode_3; }
	inline int32_t* get_address_of_mode_3() { return &___mode_3; }
	inline void set_mode_3(int32_t value)
	{
		___mode_3 = value;
	}

	inline static int32_t get_offset_of_leaveOpen_4() { return static_cast<int32_t>(offsetof(DeflateStream_t4175168077, ___leaveOpen_4)); }
	inline bool get_leaveOpen_4() const { return ___leaveOpen_4; }
	inline bool* get_address_of_leaveOpen_4() { return &___leaveOpen_4; }
	inline void set_leaveOpen_4(bool value)
	{
		___leaveOpen_4 = value;
	}

	inline static int32_t get_offset_of_disposed_5() { return static_cast<int32_t>(offsetof(DeflateStream_t4175168077, ___disposed_5)); }
	inline bool get_disposed_5() const { return ___disposed_5; }
	inline bool* get_address_of_disposed_5() { return &___disposed_5; }
	inline void set_disposed_5(bool value)
	{
		___disposed_5 = value;
	}

	inline static int32_t get_offset_of_feeder_6() { return static_cast<int32_t>(offsetof(DeflateStream_t4175168077, ___feeder_6)); }
	inline UnmanagedReadOrWrite_t876388624 * get_feeder_6() const { return ___feeder_6; }
	inline UnmanagedReadOrWrite_t876388624 ** get_address_of_feeder_6() { return &___feeder_6; }
	inline void set_feeder_6(UnmanagedReadOrWrite_t876388624 * value)
	{
		___feeder_6 = value;
		Il2CppCodeGenWriteBarrier((&___feeder_6), value);
	}

	inline static int32_t get_offset_of_z_stream_7() { return static_cast<int32_t>(offsetof(DeflateStream_t4175168077, ___z_stream_7)); }
	inline intptr_t get_z_stream_7() const { return ___z_stream_7; }
	inline intptr_t* get_address_of_z_stream_7() { return &___z_stream_7; }
	inline void set_z_stream_7(intptr_t value)
	{
		___z_stream_7 = value;
	}

	inline static int32_t get_offset_of_io_buffer_8() { return static_cast<int32_t>(offsetof(DeflateStream_t4175168077, ___io_buffer_8)); }
	inline ByteU5BU5D_t4116647657* get_io_buffer_8() const { return ___io_buffer_8; }
	inline ByteU5BU5D_t4116647657** get_address_of_io_buffer_8() { return &___io_buffer_8; }
	inline void set_io_buffer_8(ByteU5BU5D_t4116647657* value)
	{
		___io_buffer_8 = value;
		Il2CppCodeGenWriteBarrier((&___io_buffer_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(DeflateStream_t4175168077, ___data_9)); }
	inline GCHandle_t3351438187  get_data_9() const { return ___data_9; }
	inline GCHandle_t3351438187 * get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(GCHandle_t3351438187  value)
	{
		___data_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFLATESTREAM_T4175168077_H
#ifndef TRACESOURCEINFO_T2065837391_H
#define TRACESOURCEINFO_T2065837391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceSourceInfo
struct  TraceSourceInfo_t2065837391  : public RuntimeObject
{
public:
	// System.String System.Diagnostics.TraceSourceInfo::name
	String_t* ___name_0;
	// System.Diagnostics.SourceLevels System.Diagnostics.TraceSourceInfo::levels
	int32_t ___levels_1;
	// System.Diagnostics.TraceListenerCollection System.Diagnostics.TraceSourceInfo::listeners
	TraceListenerCollection_t1347122889 * ___listeners_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(TraceSourceInfo_t2065837391, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_levels_1() { return static_cast<int32_t>(offsetof(TraceSourceInfo_t2065837391, ___levels_1)); }
	inline int32_t get_levels_1() const { return ___levels_1; }
	inline int32_t* get_address_of_levels_1() { return &___levels_1; }
	inline void set_levels_1(int32_t value)
	{
		___levels_1 = value;
	}

	inline static int32_t get_offset_of_listeners_2() { return static_cast<int32_t>(offsetof(TraceSourceInfo_t2065837391, ___listeners_2)); }
	inline TraceListenerCollection_t1347122889 * get_listeners_2() const { return ___listeners_2; }
	inline TraceListenerCollection_t1347122889 ** get_address_of_listeners_2() { return &___listeners_2; }
	inline void set_listeners_2(TraceListenerCollection_t1347122889 * value)
	{
		___listeners_2 = value;
		Il2CppCodeGenWriteBarrier((&___listeners_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACESOURCEINFO_T2065837391_H
#ifndef WIN32EXCEPTION_T3234146298_H
#define WIN32EXCEPTION_T3234146298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Win32Exception
struct  Win32Exception_t3234146298  : public ExternalException_t3544951457
{
public:
	// System.Int32 System.ComponentModel.Win32Exception::native_error_code
	int32_t ___native_error_code_11;

public:
	inline static int32_t get_offset_of_native_error_code_11() { return static_cast<int32_t>(offsetof(Win32Exception_t3234146298, ___native_error_code_11)); }
	inline int32_t get_native_error_code_11() const { return ___native_error_code_11; }
	inline int32_t* get_address_of_native_error_code_11() { return &___native_error_code_11; }
	inline void set_native_error_code_11(int32_t value)
	{
		___native_error_code_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIN32EXCEPTION_T3234146298_H
#ifndef TRACELISTENER_T3975618648_H
#define TRACELISTENER_T3975618648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceListener
struct  TraceListener_t3975618648  : public MarshalByRefObject_t2760389100
{
public:
	// System.Int32 System.Diagnostics.TraceListener::indentLevel
	int32_t ___indentLevel_1;
	// System.Int32 System.Diagnostics.TraceListener::indentSize
	int32_t ___indentSize_2;
	// System.Collections.Specialized.StringDictionary System.Diagnostics.TraceListener::attributes
	StringDictionary_t120437468 * ___attributes_3;
	// System.Diagnostics.TraceFilter System.Diagnostics.TraceListener::filter
	TraceFilter_t4153521180 * ___filter_4;
	// System.Diagnostics.TraceOptions System.Diagnostics.TraceListener::options
	int32_t ___options_5;
	// System.String System.Diagnostics.TraceListener::name
	String_t* ___name_6;
	// System.Boolean System.Diagnostics.TraceListener::needIndent
	bool ___needIndent_7;

public:
	inline static int32_t get_offset_of_indentLevel_1() { return static_cast<int32_t>(offsetof(TraceListener_t3975618648, ___indentLevel_1)); }
	inline int32_t get_indentLevel_1() const { return ___indentLevel_1; }
	inline int32_t* get_address_of_indentLevel_1() { return &___indentLevel_1; }
	inline void set_indentLevel_1(int32_t value)
	{
		___indentLevel_1 = value;
	}

	inline static int32_t get_offset_of_indentSize_2() { return static_cast<int32_t>(offsetof(TraceListener_t3975618648, ___indentSize_2)); }
	inline int32_t get_indentSize_2() const { return ___indentSize_2; }
	inline int32_t* get_address_of_indentSize_2() { return &___indentSize_2; }
	inline void set_indentSize_2(int32_t value)
	{
		___indentSize_2 = value;
	}

	inline static int32_t get_offset_of_attributes_3() { return static_cast<int32_t>(offsetof(TraceListener_t3975618648, ___attributes_3)); }
	inline StringDictionary_t120437468 * get_attributes_3() const { return ___attributes_3; }
	inline StringDictionary_t120437468 ** get_address_of_attributes_3() { return &___attributes_3; }
	inline void set_attributes_3(StringDictionary_t120437468 * value)
	{
		___attributes_3 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_3), value);
	}

	inline static int32_t get_offset_of_filter_4() { return static_cast<int32_t>(offsetof(TraceListener_t3975618648, ___filter_4)); }
	inline TraceFilter_t4153521180 * get_filter_4() const { return ___filter_4; }
	inline TraceFilter_t4153521180 ** get_address_of_filter_4() { return &___filter_4; }
	inline void set_filter_4(TraceFilter_t4153521180 * value)
	{
		___filter_4 = value;
		Il2CppCodeGenWriteBarrier((&___filter_4), value);
	}

	inline static int32_t get_offset_of_options_5() { return static_cast<int32_t>(offsetof(TraceListener_t3975618648, ___options_5)); }
	inline int32_t get_options_5() const { return ___options_5; }
	inline int32_t* get_address_of_options_5() { return &___options_5; }
	inline void set_options_5(int32_t value)
	{
		___options_5 = value;
	}

	inline static int32_t get_offset_of_name_6() { return static_cast<int32_t>(offsetof(TraceListener_t3975618648, ___name_6)); }
	inline String_t* get_name_6() const { return ___name_6; }
	inline String_t** get_address_of_name_6() { return &___name_6; }
	inline void set_name_6(String_t* value)
	{
		___name_6 = value;
		Il2CppCodeGenWriteBarrier((&___name_6), value);
	}

	inline static int32_t get_offset_of_needIndent_7() { return static_cast<int32_t>(offsetof(TraceListener_t3975618648, ___needIndent_7)); }
	inline bool get_needIndent_7() const { return ___needIndent_7; }
	inline bool* get_address_of_needIndent_7() { return &___needIndent_7; }
	inline void set_needIndent_7(bool value)
	{
		___needIndent_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACELISTENER_T3975618648_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef PROCESSSTARTINFO_T2184852744_H
#define PROCESSSTARTINFO_T2184852744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.ProcessStartInfo
struct  ProcessStartInfo_t2184852744  : public RuntimeObject
{
public:
	// System.String System.Diagnostics.ProcessStartInfo::arguments
	String_t* ___arguments_0;
	// System.IntPtr System.Diagnostics.ProcessStartInfo::error_dialog_parent_handle
	intptr_t ___error_dialog_parent_handle_1;
	// System.String System.Diagnostics.ProcessStartInfo::filename
	String_t* ___filename_2;
	// System.String System.Diagnostics.ProcessStartInfo::verb
	String_t* ___verb_3;
	// System.String System.Diagnostics.ProcessStartInfo::working_directory
	String_t* ___working_directory_4;
	// System.Collections.Specialized.ProcessStringDictionary System.Diagnostics.ProcessStartInfo::envVars
	ProcessStringDictionary_t2107791454 * ___envVars_5;
	// System.Boolean System.Diagnostics.ProcessStartInfo::create_no_window
	bool ___create_no_window_6;
	// System.Boolean System.Diagnostics.ProcessStartInfo::error_dialog
	bool ___error_dialog_7;
	// System.Boolean System.Diagnostics.ProcessStartInfo::redirect_standard_error
	bool ___redirect_standard_error_8;
	// System.Boolean System.Diagnostics.ProcessStartInfo::redirect_standard_input
	bool ___redirect_standard_input_9;
	// System.Boolean System.Diagnostics.ProcessStartInfo::redirect_standard_output
	bool ___redirect_standard_output_10;
	// System.Boolean System.Diagnostics.ProcessStartInfo::use_shell_execute
	bool ___use_shell_execute_11;
	// System.Diagnostics.ProcessWindowStyle System.Diagnostics.ProcessStartInfo::window_style
	int32_t ___window_style_12;
	// System.Text.Encoding System.Diagnostics.ProcessStartInfo::encoding_stderr
	Encoding_t1523322056 * ___encoding_stderr_13;
	// System.Text.Encoding System.Diagnostics.ProcessStartInfo::encoding_stdout
	Encoding_t1523322056 * ___encoding_stdout_14;
	// System.String System.Diagnostics.ProcessStartInfo::username
	String_t* ___username_15;
	// System.String System.Diagnostics.ProcessStartInfo::domain
	String_t* ___domain_16;
	// System.Security.SecureString System.Diagnostics.ProcessStartInfo::password
	SecureString_t3041467854 * ___password_17;
	// System.Boolean System.Diagnostics.ProcessStartInfo::load_user_profile
	bool ___load_user_profile_18;

public:
	inline static int32_t get_offset_of_arguments_0() { return static_cast<int32_t>(offsetof(ProcessStartInfo_t2184852744, ___arguments_0)); }
	inline String_t* get_arguments_0() const { return ___arguments_0; }
	inline String_t** get_address_of_arguments_0() { return &___arguments_0; }
	inline void set_arguments_0(String_t* value)
	{
		___arguments_0 = value;
		Il2CppCodeGenWriteBarrier((&___arguments_0), value);
	}

	inline static int32_t get_offset_of_error_dialog_parent_handle_1() { return static_cast<int32_t>(offsetof(ProcessStartInfo_t2184852744, ___error_dialog_parent_handle_1)); }
	inline intptr_t get_error_dialog_parent_handle_1() const { return ___error_dialog_parent_handle_1; }
	inline intptr_t* get_address_of_error_dialog_parent_handle_1() { return &___error_dialog_parent_handle_1; }
	inline void set_error_dialog_parent_handle_1(intptr_t value)
	{
		___error_dialog_parent_handle_1 = value;
	}

	inline static int32_t get_offset_of_filename_2() { return static_cast<int32_t>(offsetof(ProcessStartInfo_t2184852744, ___filename_2)); }
	inline String_t* get_filename_2() const { return ___filename_2; }
	inline String_t** get_address_of_filename_2() { return &___filename_2; }
	inline void set_filename_2(String_t* value)
	{
		___filename_2 = value;
		Il2CppCodeGenWriteBarrier((&___filename_2), value);
	}

	inline static int32_t get_offset_of_verb_3() { return static_cast<int32_t>(offsetof(ProcessStartInfo_t2184852744, ___verb_3)); }
	inline String_t* get_verb_3() const { return ___verb_3; }
	inline String_t** get_address_of_verb_3() { return &___verb_3; }
	inline void set_verb_3(String_t* value)
	{
		___verb_3 = value;
		Il2CppCodeGenWriteBarrier((&___verb_3), value);
	}

	inline static int32_t get_offset_of_working_directory_4() { return static_cast<int32_t>(offsetof(ProcessStartInfo_t2184852744, ___working_directory_4)); }
	inline String_t* get_working_directory_4() const { return ___working_directory_4; }
	inline String_t** get_address_of_working_directory_4() { return &___working_directory_4; }
	inline void set_working_directory_4(String_t* value)
	{
		___working_directory_4 = value;
		Il2CppCodeGenWriteBarrier((&___working_directory_4), value);
	}

	inline static int32_t get_offset_of_envVars_5() { return static_cast<int32_t>(offsetof(ProcessStartInfo_t2184852744, ___envVars_5)); }
	inline ProcessStringDictionary_t2107791454 * get_envVars_5() const { return ___envVars_5; }
	inline ProcessStringDictionary_t2107791454 ** get_address_of_envVars_5() { return &___envVars_5; }
	inline void set_envVars_5(ProcessStringDictionary_t2107791454 * value)
	{
		___envVars_5 = value;
		Il2CppCodeGenWriteBarrier((&___envVars_5), value);
	}

	inline static int32_t get_offset_of_create_no_window_6() { return static_cast<int32_t>(offsetof(ProcessStartInfo_t2184852744, ___create_no_window_6)); }
	inline bool get_create_no_window_6() const { return ___create_no_window_6; }
	inline bool* get_address_of_create_no_window_6() { return &___create_no_window_6; }
	inline void set_create_no_window_6(bool value)
	{
		___create_no_window_6 = value;
	}

	inline static int32_t get_offset_of_error_dialog_7() { return static_cast<int32_t>(offsetof(ProcessStartInfo_t2184852744, ___error_dialog_7)); }
	inline bool get_error_dialog_7() const { return ___error_dialog_7; }
	inline bool* get_address_of_error_dialog_7() { return &___error_dialog_7; }
	inline void set_error_dialog_7(bool value)
	{
		___error_dialog_7 = value;
	}

	inline static int32_t get_offset_of_redirect_standard_error_8() { return static_cast<int32_t>(offsetof(ProcessStartInfo_t2184852744, ___redirect_standard_error_8)); }
	inline bool get_redirect_standard_error_8() const { return ___redirect_standard_error_8; }
	inline bool* get_address_of_redirect_standard_error_8() { return &___redirect_standard_error_8; }
	inline void set_redirect_standard_error_8(bool value)
	{
		___redirect_standard_error_8 = value;
	}

	inline static int32_t get_offset_of_redirect_standard_input_9() { return static_cast<int32_t>(offsetof(ProcessStartInfo_t2184852744, ___redirect_standard_input_9)); }
	inline bool get_redirect_standard_input_9() const { return ___redirect_standard_input_9; }
	inline bool* get_address_of_redirect_standard_input_9() { return &___redirect_standard_input_9; }
	inline void set_redirect_standard_input_9(bool value)
	{
		___redirect_standard_input_9 = value;
	}

	inline static int32_t get_offset_of_redirect_standard_output_10() { return static_cast<int32_t>(offsetof(ProcessStartInfo_t2184852744, ___redirect_standard_output_10)); }
	inline bool get_redirect_standard_output_10() const { return ___redirect_standard_output_10; }
	inline bool* get_address_of_redirect_standard_output_10() { return &___redirect_standard_output_10; }
	inline void set_redirect_standard_output_10(bool value)
	{
		___redirect_standard_output_10 = value;
	}

	inline static int32_t get_offset_of_use_shell_execute_11() { return static_cast<int32_t>(offsetof(ProcessStartInfo_t2184852744, ___use_shell_execute_11)); }
	inline bool get_use_shell_execute_11() const { return ___use_shell_execute_11; }
	inline bool* get_address_of_use_shell_execute_11() { return &___use_shell_execute_11; }
	inline void set_use_shell_execute_11(bool value)
	{
		___use_shell_execute_11 = value;
	}

	inline static int32_t get_offset_of_window_style_12() { return static_cast<int32_t>(offsetof(ProcessStartInfo_t2184852744, ___window_style_12)); }
	inline int32_t get_window_style_12() const { return ___window_style_12; }
	inline int32_t* get_address_of_window_style_12() { return &___window_style_12; }
	inline void set_window_style_12(int32_t value)
	{
		___window_style_12 = value;
	}

	inline static int32_t get_offset_of_encoding_stderr_13() { return static_cast<int32_t>(offsetof(ProcessStartInfo_t2184852744, ___encoding_stderr_13)); }
	inline Encoding_t1523322056 * get_encoding_stderr_13() const { return ___encoding_stderr_13; }
	inline Encoding_t1523322056 ** get_address_of_encoding_stderr_13() { return &___encoding_stderr_13; }
	inline void set_encoding_stderr_13(Encoding_t1523322056 * value)
	{
		___encoding_stderr_13 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_stderr_13), value);
	}

	inline static int32_t get_offset_of_encoding_stdout_14() { return static_cast<int32_t>(offsetof(ProcessStartInfo_t2184852744, ___encoding_stdout_14)); }
	inline Encoding_t1523322056 * get_encoding_stdout_14() const { return ___encoding_stdout_14; }
	inline Encoding_t1523322056 ** get_address_of_encoding_stdout_14() { return &___encoding_stdout_14; }
	inline void set_encoding_stdout_14(Encoding_t1523322056 * value)
	{
		___encoding_stdout_14 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_stdout_14), value);
	}

	inline static int32_t get_offset_of_username_15() { return static_cast<int32_t>(offsetof(ProcessStartInfo_t2184852744, ___username_15)); }
	inline String_t* get_username_15() const { return ___username_15; }
	inline String_t** get_address_of_username_15() { return &___username_15; }
	inline void set_username_15(String_t* value)
	{
		___username_15 = value;
		Il2CppCodeGenWriteBarrier((&___username_15), value);
	}

	inline static int32_t get_offset_of_domain_16() { return static_cast<int32_t>(offsetof(ProcessStartInfo_t2184852744, ___domain_16)); }
	inline String_t* get_domain_16() const { return ___domain_16; }
	inline String_t** get_address_of_domain_16() { return &___domain_16; }
	inline void set_domain_16(String_t* value)
	{
		___domain_16 = value;
		Il2CppCodeGenWriteBarrier((&___domain_16), value);
	}

	inline static int32_t get_offset_of_password_17() { return static_cast<int32_t>(offsetof(ProcessStartInfo_t2184852744, ___password_17)); }
	inline SecureString_t3041467854 * get_password_17() const { return ___password_17; }
	inline SecureString_t3041467854 ** get_address_of_password_17() { return &___password_17; }
	inline void set_password_17(SecureString_t3041467854 * value)
	{
		___password_17 = value;
		Il2CppCodeGenWriteBarrier((&___password_17), value);
	}

	inline static int32_t get_offset_of_load_user_profile_18() { return static_cast<int32_t>(offsetof(ProcessStartInfo_t2184852744, ___load_user_profile_18)); }
	inline bool get_load_user_profile_18() const { return ___load_user_profile_18; }
	inline bool* get_address_of_load_user_profile_18() { return &___load_user_profile_18; }
	inline void set_load_user_profile_18(bool value)
	{
		___load_user_profile_18 = value;
	}
};

struct ProcessStartInfo_t2184852744_StaticFields
{
public:
	// System.String[] System.Diagnostics.ProcessStartInfo::empty
	StringU5BU5D_t1281789340* ___empty_19;

public:
	inline static int32_t get_offset_of_empty_19() { return static_cast<int32_t>(offsetof(ProcessStartInfo_t2184852744_StaticFields, ___empty_19)); }
	inline StringU5BU5D_t1281789340* get_empty_19() const { return ___empty_19; }
	inline StringU5BU5D_t1281789340** get_address_of_empty_19() { return &___empty_19; }
	inline void set_empty_19(StringU5BU5D_t1281789340* value)
	{
		___empty_19 = value;
		Il2CppCodeGenWriteBarrier((&___empty_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROCESSSTARTINFO_T2184852744_H
#ifndef PROCESSWAITHANDLE_T3847593151_H
#define PROCESSWAITHANDLE_T3847593151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.Process/ProcessWaitHandle
struct  ProcessWaitHandle_t3847593151  : public WaitHandle_t1743403487
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROCESSWAITHANDLE_T3847593151_H
#ifndef DEFAULTTRACELISTENER_T2804304119_H
#define DEFAULTTRACELISTENER_T2804304119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.DefaultTraceListener
struct  DefaultTraceListener_t2804304119  : public TraceListener_t3975618648
{
public:
	// System.String System.Diagnostics.DefaultTraceListener::logFileName
	String_t* ___logFileName_11;
	// System.Boolean System.Diagnostics.DefaultTraceListener::assertUiEnabled
	bool ___assertUiEnabled_12;

public:
	inline static int32_t get_offset_of_logFileName_11() { return static_cast<int32_t>(offsetof(DefaultTraceListener_t2804304119, ___logFileName_11)); }
	inline String_t* get_logFileName_11() const { return ___logFileName_11; }
	inline String_t** get_address_of_logFileName_11() { return &___logFileName_11; }
	inline void set_logFileName_11(String_t* value)
	{
		___logFileName_11 = value;
		Il2CppCodeGenWriteBarrier((&___logFileName_11), value);
	}

	inline static int32_t get_offset_of_assertUiEnabled_12() { return static_cast<int32_t>(offsetof(DefaultTraceListener_t2804304119, ___assertUiEnabled_12)); }
	inline bool get_assertUiEnabled_12() const { return ___assertUiEnabled_12; }
	inline bool* get_address_of_assertUiEnabled_12() { return &___assertUiEnabled_12; }
	inline void set_assertUiEnabled_12(bool value)
	{
		___assertUiEnabled_12 = value;
	}
};

struct DefaultTraceListener_t2804304119_StaticFields
{
public:
	// System.Boolean System.Diagnostics.DefaultTraceListener::OnWin32
	bool ___OnWin32_8;
	// System.String System.Diagnostics.DefaultTraceListener::MonoTracePrefix
	String_t* ___MonoTracePrefix_9;
	// System.String System.Diagnostics.DefaultTraceListener::MonoTraceFile
	String_t* ___MonoTraceFile_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Diagnostics.DefaultTraceListener::<>f__switch$map4
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map4_13;

public:
	inline static int32_t get_offset_of_OnWin32_8() { return static_cast<int32_t>(offsetof(DefaultTraceListener_t2804304119_StaticFields, ___OnWin32_8)); }
	inline bool get_OnWin32_8() const { return ___OnWin32_8; }
	inline bool* get_address_of_OnWin32_8() { return &___OnWin32_8; }
	inline void set_OnWin32_8(bool value)
	{
		___OnWin32_8 = value;
	}

	inline static int32_t get_offset_of_MonoTracePrefix_9() { return static_cast<int32_t>(offsetof(DefaultTraceListener_t2804304119_StaticFields, ___MonoTracePrefix_9)); }
	inline String_t* get_MonoTracePrefix_9() const { return ___MonoTracePrefix_9; }
	inline String_t** get_address_of_MonoTracePrefix_9() { return &___MonoTracePrefix_9; }
	inline void set_MonoTracePrefix_9(String_t* value)
	{
		___MonoTracePrefix_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonoTracePrefix_9), value);
	}

	inline static int32_t get_offset_of_MonoTraceFile_10() { return static_cast<int32_t>(offsetof(DefaultTraceListener_t2804304119_StaticFields, ___MonoTraceFile_10)); }
	inline String_t* get_MonoTraceFile_10() const { return ___MonoTraceFile_10; }
	inline String_t** get_address_of_MonoTraceFile_10() { return &___MonoTraceFile_10; }
	inline void set_MonoTraceFile_10(String_t* value)
	{
		___MonoTraceFile_10 = value;
		Il2CppCodeGenWriteBarrier((&___MonoTraceFile_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4_13() { return static_cast<int32_t>(offsetof(DefaultTraceListener_t2804304119_StaticFields, ___U3CU3Ef__switchU24map4_13)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map4_13() const { return ___U3CU3Ef__switchU24map4_13; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map4_13() { return &___U3CU3Ef__switchU24map4_13; }
	inline void set_U3CU3Ef__switchU24map4_13(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map4_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map4_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTRACELISTENER_T2804304119_H
#ifndef READMETHOD_T893206259_H
#define READMETHOD_T893206259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Compression.DeflateStream/ReadMethod
struct  ReadMethod_t893206259  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READMETHOD_T893206259_H
#ifndef WRITEMETHOD_T2538911768_H
#define WRITEMETHOD_T2538911768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Compression.DeflateStream/WriteMethod
struct  WriteMethod_t2538911768  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITEMETHOD_T2538911768_H
#ifndef COOKIE_T993873397_H
#define COOKIE_T993873397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Cookie
struct  Cookie_t993873397  : public RuntimeObject
{
public:
	// System.String System.Net.Cookie::comment
	String_t* ___comment_0;
	// System.Uri System.Net.Cookie::commentUri
	Uri_t100236324 * ___commentUri_1;
	// System.Boolean System.Net.Cookie::discard
	bool ___discard_2;
	// System.String System.Net.Cookie::domain
	String_t* ___domain_3;
	// System.DateTime System.Net.Cookie::expires
	DateTime_t3738529785  ___expires_4;
	// System.Boolean System.Net.Cookie::httpOnly
	bool ___httpOnly_5;
	// System.String System.Net.Cookie::name
	String_t* ___name_6;
	// System.String System.Net.Cookie::path
	String_t* ___path_7;
	// System.String System.Net.Cookie::port
	String_t* ___port_8;
	// System.Int32[] System.Net.Cookie::ports
	Int32U5BU5D_t385246372* ___ports_9;
	// System.Boolean System.Net.Cookie::secure
	bool ___secure_10;
	// System.DateTime System.Net.Cookie::timestamp
	DateTime_t3738529785  ___timestamp_11;
	// System.String System.Net.Cookie::val
	String_t* ___val_12;
	// System.Int32 System.Net.Cookie::version
	int32_t ___version_13;
	// System.Boolean System.Net.Cookie::exact_domain
	bool ___exact_domain_17;

public:
	inline static int32_t get_offset_of_comment_0() { return static_cast<int32_t>(offsetof(Cookie_t993873397, ___comment_0)); }
	inline String_t* get_comment_0() const { return ___comment_0; }
	inline String_t** get_address_of_comment_0() { return &___comment_0; }
	inline void set_comment_0(String_t* value)
	{
		___comment_0 = value;
		Il2CppCodeGenWriteBarrier((&___comment_0), value);
	}

	inline static int32_t get_offset_of_commentUri_1() { return static_cast<int32_t>(offsetof(Cookie_t993873397, ___commentUri_1)); }
	inline Uri_t100236324 * get_commentUri_1() const { return ___commentUri_1; }
	inline Uri_t100236324 ** get_address_of_commentUri_1() { return &___commentUri_1; }
	inline void set_commentUri_1(Uri_t100236324 * value)
	{
		___commentUri_1 = value;
		Il2CppCodeGenWriteBarrier((&___commentUri_1), value);
	}

	inline static int32_t get_offset_of_discard_2() { return static_cast<int32_t>(offsetof(Cookie_t993873397, ___discard_2)); }
	inline bool get_discard_2() const { return ___discard_2; }
	inline bool* get_address_of_discard_2() { return &___discard_2; }
	inline void set_discard_2(bool value)
	{
		___discard_2 = value;
	}

	inline static int32_t get_offset_of_domain_3() { return static_cast<int32_t>(offsetof(Cookie_t993873397, ___domain_3)); }
	inline String_t* get_domain_3() const { return ___domain_3; }
	inline String_t** get_address_of_domain_3() { return &___domain_3; }
	inline void set_domain_3(String_t* value)
	{
		___domain_3 = value;
		Il2CppCodeGenWriteBarrier((&___domain_3), value);
	}

	inline static int32_t get_offset_of_expires_4() { return static_cast<int32_t>(offsetof(Cookie_t993873397, ___expires_4)); }
	inline DateTime_t3738529785  get_expires_4() const { return ___expires_4; }
	inline DateTime_t3738529785 * get_address_of_expires_4() { return &___expires_4; }
	inline void set_expires_4(DateTime_t3738529785  value)
	{
		___expires_4 = value;
	}

	inline static int32_t get_offset_of_httpOnly_5() { return static_cast<int32_t>(offsetof(Cookie_t993873397, ___httpOnly_5)); }
	inline bool get_httpOnly_5() const { return ___httpOnly_5; }
	inline bool* get_address_of_httpOnly_5() { return &___httpOnly_5; }
	inline void set_httpOnly_5(bool value)
	{
		___httpOnly_5 = value;
	}

	inline static int32_t get_offset_of_name_6() { return static_cast<int32_t>(offsetof(Cookie_t993873397, ___name_6)); }
	inline String_t* get_name_6() const { return ___name_6; }
	inline String_t** get_address_of_name_6() { return &___name_6; }
	inline void set_name_6(String_t* value)
	{
		___name_6 = value;
		Il2CppCodeGenWriteBarrier((&___name_6), value);
	}

	inline static int32_t get_offset_of_path_7() { return static_cast<int32_t>(offsetof(Cookie_t993873397, ___path_7)); }
	inline String_t* get_path_7() const { return ___path_7; }
	inline String_t** get_address_of_path_7() { return &___path_7; }
	inline void set_path_7(String_t* value)
	{
		___path_7 = value;
		Il2CppCodeGenWriteBarrier((&___path_7), value);
	}

	inline static int32_t get_offset_of_port_8() { return static_cast<int32_t>(offsetof(Cookie_t993873397, ___port_8)); }
	inline String_t* get_port_8() const { return ___port_8; }
	inline String_t** get_address_of_port_8() { return &___port_8; }
	inline void set_port_8(String_t* value)
	{
		___port_8 = value;
		Il2CppCodeGenWriteBarrier((&___port_8), value);
	}

	inline static int32_t get_offset_of_ports_9() { return static_cast<int32_t>(offsetof(Cookie_t993873397, ___ports_9)); }
	inline Int32U5BU5D_t385246372* get_ports_9() const { return ___ports_9; }
	inline Int32U5BU5D_t385246372** get_address_of_ports_9() { return &___ports_9; }
	inline void set_ports_9(Int32U5BU5D_t385246372* value)
	{
		___ports_9 = value;
		Il2CppCodeGenWriteBarrier((&___ports_9), value);
	}

	inline static int32_t get_offset_of_secure_10() { return static_cast<int32_t>(offsetof(Cookie_t993873397, ___secure_10)); }
	inline bool get_secure_10() const { return ___secure_10; }
	inline bool* get_address_of_secure_10() { return &___secure_10; }
	inline void set_secure_10(bool value)
	{
		___secure_10 = value;
	}

	inline static int32_t get_offset_of_timestamp_11() { return static_cast<int32_t>(offsetof(Cookie_t993873397, ___timestamp_11)); }
	inline DateTime_t3738529785  get_timestamp_11() const { return ___timestamp_11; }
	inline DateTime_t3738529785 * get_address_of_timestamp_11() { return &___timestamp_11; }
	inline void set_timestamp_11(DateTime_t3738529785  value)
	{
		___timestamp_11 = value;
	}

	inline static int32_t get_offset_of_val_12() { return static_cast<int32_t>(offsetof(Cookie_t993873397, ___val_12)); }
	inline String_t* get_val_12() const { return ___val_12; }
	inline String_t** get_address_of_val_12() { return &___val_12; }
	inline void set_val_12(String_t* value)
	{
		___val_12 = value;
		Il2CppCodeGenWriteBarrier((&___val_12), value);
	}

	inline static int32_t get_offset_of_version_13() { return static_cast<int32_t>(offsetof(Cookie_t993873397, ___version_13)); }
	inline int32_t get_version_13() const { return ___version_13; }
	inline int32_t* get_address_of_version_13() { return &___version_13; }
	inline void set_version_13(int32_t value)
	{
		___version_13 = value;
	}

	inline static int32_t get_offset_of_exact_domain_17() { return static_cast<int32_t>(offsetof(Cookie_t993873397, ___exact_domain_17)); }
	inline bool get_exact_domain_17() const { return ___exact_domain_17; }
	inline bool* get_address_of_exact_domain_17() { return &___exact_domain_17; }
	inline void set_exact_domain_17(bool value)
	{
		___exact_domain_17 = value;
	}
};

struct Cookie_t993873397_StaticFields
{
public:
	// System.Char[] System.Net.Cookie::reservedCharsName
	CharU5BU5D_t3528271667* ___reservedCharsName_14;
	// System.Char[] System.Net.Cookie::portSeparators
	CharU5BU5D_t3528271667* ___portSeparators_15;
	// System.String System.Net.Cookie::tspecials
	String_t* ___tspecials_16;

public:
	inline static int32_t get_offset_of_reservedCharsName_14() { return static_cast<int32_t>(offsetof(Cookie_t993873397_StaticFields, ___reservedCharsName_14)); }
	inline CharU5BU5D_t3528271667* get_reservedCharsName_14() const { return ___reservedCharsName_14; }
	inline CharU5BU5D_t3528271667** get_address_of_reservedCharsName_14() { return &___reservedCharsName_14; }
	inline void set_reservedCharsName_14(CharU5BU5D_t3528271667* value)
	{
		___reservedCharsName_14 = value;
		Il2CppCodeGenWriteBarrier((&___reservedCharsName_14), value);
	}

	inline static int32_t get_offset_of_portSeparators_15() { return static_cast<int32_t>(offsetof(Cookie_t993873397_StaticFields, ___portSeparators_15)); }
	inline CharU5BU5D_t3528271667* get_portSeparators_15() const { return ___portSeparators_15; }
	inline CharU5BU5D_t3528271667** get_address_of_portSeparators_15() { return &___portSeparators_15; }
	inline void set_portSeparators_15(CharU5BU5D_t3528271667* value)
	{
		___portSeparators_15 = value;
		Il2CppCodeGenWriteBarrier((&___portSeparators_15), value);
	}

	inline static int32_t get_offset_of_tspecials_16() { return static_cast<int32_t>(offsetof(Cookie_t993873397_StaticFields, ___tspecials_16)); }
	inline String_t* get_tspecials_16() const { return ___tspecials_16; }
	inline String_t** get_address_of_tspecials_16() { return &___tspecials_16; }
	inline void set_tspecials_16(String_t* value)
	{
		___tspecials_16 = value;
		Il2CppCodeGenWriteBarrier((&___tspecials_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIE_T993873397_H
#ifndef TRACEEVENTCACHE_T3113663374_H
#define TRACEEVENTCACHE_T3113663374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceEventCache
struct  TraceEventCache_t3113663374  : public RuntimeObject
{
public:
	// System.DateTime System.Diagnostics.TraceEventCache::started
	DateTime_t3738529785  ___started_0;
	// System.Diagnostics.CorrelationManager System.Diagnostics.TraceEventCache::manager
	CorrelationManager_t2688752967 * ___manager_1;
	// System.String System.Diagnostics.TraceEventCache::callstack
	String_t* ___callstack_2;
	// System.String System.Diagnostics.TraceEventCache::thread
	String_t* ___thread_3;
	// System.Int32 System.Diagnostics.TraceEventCache::process
	int32_t ___process_4;
	// System.Int64 System.Diagnostics.TraceEventCache::timestamp
	int64_t ___timestamp_5;

public:
	inline static int32_t get_offset_of_started_0() { return static_cast<int32_t>(offsetof(TraceEventCache_t3113663374, ___started_0)); }
	inline DateTime_t3738529785  get_started_0() const { return ___started_0; }
	inline DateTime_t3738529785 * get_address_of_started_0() { return &___started_0; }
	inline void set_started_0(DateTime_t3738529785  value)
	{
		___started_0 = value;
	}

	inline static int32_t get_offset_of_manager_1() { return static_cast<int32_t>(offsetof(TraceEventCache_t3113663374, ___manager_1)); }
	inline CorrelationManager_t2688752967 * get_manager_1() const { return ___manager_1; }
	inline CorrelationManager_t2688752967 ** get_address_of_manager_1() { return &___manager_1; }
	inline void set_manager_1(CorrelationManager_t2688752967 * value)
	{
		___manager_1 = value;
		Il2CppCodeGenWriteBarrier((&___manager_1), value);
	}

	inline static int32_t get_offset_of_callstack_2() { return static_cast<int32_t>(offsetof(TraceEventCache_t3113663374, ___callstack_2)); }
	inline String_t* get_callstack_2() const { return ___callstack_2; }
	inline String_t** get_address_of_callstack_2() { return &___callstack_2; }
	inline void set_callstack_2(String_t* value)
	{
		___callstack_2 = value;
		Il2CppCodeGenWriteBarrier((&___callstack_2), value);
	}

	inline static int32_t get_offset_of_thread_3() { return static_cast<int32_t>(offsetof(TraceEventCache_t3113663374, ___thread_3)); }
	inline String_t* get_thread_3() const { return ___thread_3; }
	inline String_t** get_address_of_thread_3() { return &___thread_3; }
	inline void set_thread_3(String_t* value)
	{
		___thread_3 = value;
		Il2CppCodeGenWriteBarrier((&___thread_3), value);
	}

	inline static int32_t get_offset_of_process_4() { return static_cast<int32_t>(offsetof(TraceEventCache_t3113663374, ___process_4)); }
	inline int32_t get_process_4() const { return ___process_4; }
	inline int32_t* get_address_of_process_4() { return &___process_4; }
	inline void set_process_4(int32_t value)
	{
		___process_4 = value;
	}

	inline static int32_t get_offset_of_timestamp_5() { return static_cast<int32_t>(offsetof(TraceEventCache_t3113663374, ___timestamp_5)); }
	inline int64_t get_timestamp_5() const { return ___timestamp_5; }
	inline int64_t* get_address_of_timestamp_5() { return &___timestamp_5; }
	inline void set_timestamp_5(int64_t value)
	{
		___timestamp_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACEEVENTCACHE_T3113663374_H
#ifndef MONOSYNCFILESTREAM_T3452145842_H
#define MONOSYNCFILESTREAM_T3452145842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MonoSyncFileStream
struct  MonoSyncFileStream_t3452145842  : public FileStream_t4292183065
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOSYNCFILESTREAM_T3452145842_H
#ifndef WRITEDELEGATE_T1613340939_H
#define WRITEDELEGATE_T1613340939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MonoSyncFileStream/WriteDelegate
struct  WriteDelegate_t1613340939  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITEDELEGATE_T1613340939_H
#ifndef UNMANAGEDREADORWRITE_T876388624_H
#define UNMANAGEDREADORWRITE_T876388624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Compression.DeflateStream/UnmanagedReadOrWrite
struct  UnmanagedReadOrWrite_t876388624  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNMANAGEDREADORWRITE_T876388624_H
#ifndef READDELEGATE_T2469437439_H
#define READDELEGATE_T2469437439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MonoSyncFileStream/ReadDelegate
struct  ReadDelegate_t2469437439  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READDELEGATE_T2469437439_H
#ifndef ELEMENTHANDLER_T2521990823_H
#define ELEMENTHANDLER_T2521990823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.DiagnosticsConfigurationHandler/ElementHandler
struct  ElementHandler_t2521990823  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTHANDLER_T2521990823_H
#ifndef ASYNCREADHANDLER_T1188682440_H
#define ASYNCREADHANDLER_T1188682440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.Process/AsyncReadHandler
struct  AsyncReadHandler_t1188682440  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCREADHANDLER_T1188682440_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1200 = { sizeof (WeakObjectWrapper_t827463650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1200[2] = 
{
	WeakObjectWrapper_t827463650::get_offset_of_U3CTargetHashCodeU3Ek__BackingField_0(),
	WeakObjectWrapper_t827463650::get_offset_of_U3CWeakU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1201 = { sizeof (WeakObjectWrapperComparer_t303980402), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1202 = { sizeof (Win32Exception_t3234146298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1202[1] = 
{
	Win32Exception_t3234146298::get_offset_of_native_error_code_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1203 = { sizeof (ConfigurationException_t3515317685), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1203[2] = 
{
	ConfigurationException_t3515317685::get_offset_of_filename_11(),
	ConfigurationException_t3515317685::get_offset_of_line_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1204 = { sizeof (ConfigurationSettings_t822951835), -1, sizeof(ConfigurationSettings_t822951835_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1204[2] = 
{
	ConfigurationSettings_t822951835_StaticFields::get_offset_of_config_0(),
	ConfigurationSettings_t822951835_StaticFields::get_offset_of_lockobj_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1205 = { sizeof (DefaultConfig_t1013547162), -1, sizeof(DefaultConfig_t1013547162_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1205[1] = 
{
	DefaultConfig_t1013547162_StaticFields::get_offset_of_instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1206 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1207 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1208 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1209 = { sizeof (DefaultUriParser_t95882050), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1210 = { sizeof (CorrelationManager_t2688752967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1210[1] = 
{
	CorrelationManager_t2688752967::get_offset_of_op_stack_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1211 = { sizeof (DataReceivedEventArgs_t2585381898), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1212 = { sizeof (DefaultTraceListener_t2804304119), -1, sizeof(DefaultTraceListener_t2804304119_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1212[6] = 
{
	DefaultTraceListener_t2804304119_StaticFields::get_offset_of_OnWin32_8(),
	DefaultTraceListener_t2804304119_StaticFields::get_offset_of_MonoTracePrefix_9(),
	DefaultTraceListener_t2804304119_StaticFields::get_offset_of_MonoTraceFile_10(),
	DefaultTraceListener_t2804304119::get_offset_of_logFileName_11(),
	DefaultTraceListener_t2804304119::get_offset_of_assertUiEnabled_12(),
	DefaultTraceListener_t2804304119_StaticFields::get_offset_of_U3CU3Ef__switchU24map4_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1213 = { sizeof (DiagnosticsConfiguration_t1159239597), -1, sizeof(DiagnosticsConfiguration_t1159239597_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1213[1] = 
{
	DiagnosticsConfiguration_t1159239597_StaticFields::get_offset_of_settings_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1214 = { sizeof (DiagnosticsConfigurationHandler_t2074484573), -1, sizeof(DiagnosticsConfigurationHandler_t2074484573_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1214[5] = 
{
	DiagnosticsConfigurationHandler_t2074484573::get_offset_of_configValues_0(),
	DiagnosticsConfigurationHandler_t2074484573::get_offset_of_elementHandlers_1(),
	DiagnosticsConfigurationHandler_t2074484573_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_2(),
	DiagnosticsConfigurationHandler_t2074484573_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_3(),
	DiagnosticsConfigurationHandler_t2074484573_StaticFields::get_offset_of_U3CU3Ef__switchU24map7_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1215 = { sizeof (ElementHandler_t2521990823), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1216 = { sizeof (MonitoringDescriptionAttribute_t2767822115), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1217 = { sizeof (Process_t3774297411), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1217[22] = 
{
	Process_t3774297411::get_offset_of_process_handle_4(),
	Process_t3774297411::get_offset_of_pid_5(),
	Process_t3774297411::get_offset_of_enableRaisingEvents_6(),
	Process_t3774297411::get_offset_of_already_waiting_7(),
	Process_t3774297411::get_offset_of_synchronizingObject_8(),
	Process_t3774297411::get_offset_of_exited_event_9(),
	Process_t3774297411::get_offset_of_stdout_rd_10(),
	Process_t3774297411::get_offset_of_stderr_rd_11(),
	Process_t3774297411::get_offset_of_module_collection_12(),
	Process_t3774297411::get_offset_of_process_name_13(),
	Process_t3774297411::get_offset_of_error_stream_14(),
	Process_t3774297411::get_offset_of_input_stream_15(),
	Process_t3774297411::get_offset_of_output_stream_16(),
	Process_t3774297411::get_offset_of_start_info_17(),
	Process_t3774297411::get_offset_of_async_mode_18(),
	Process_t3774297411::get_offset_of_output_canceled_19(),
	Process_t3774297411::get_offset_of_error_canceled_20(),
	Process_t3774297411::get_offset_of_async_output_21(),
	Process_t3774297411::get_offset_of_async_error_22(),
	Process_t3774297411::get_offset_of_disposed_23(),
	Process_t3774297411::get_offset_of_OutputDataReceived_24(),
	Process_t3774297411::get_offset_of_ErrorDataReceived_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1218 = { sizeof (ProcInfo_t2917059746)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1218[10] = 
{
	ProcInfo_t2917059746::get_offset_of_process_handle_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProcInfo_t2917059746::get_offset_of_thread_handle_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProcInfo_t2917059746::get_offset_of_pid_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProcInfo_t2917059746::get_offset_of_tid_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProcInfo_t2917059746::get_offset_of_envKeys_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProcInfo_t2917059746::get_offset_of_envValues_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProcInfo_t2917059746::get_offset_of_UserName_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProcInfo_t2917059746::get_offset_of_Domain_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProcInfo_t2917059746::get_offset_of_Password_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProcInfo_t2917059746::get_offset_of_LoadUserProfile_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1219 = { sizeof (AsyncModes_t4207881179)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1219[6] = 
{
	AsyncModes_t4207881179::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1220 = { sizeof (ProcessAsyncReader_t337580163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1220[30] = 
{
	ProcessAsyncReader_t337580163::get_offset_of_Sock_0(),
	ProcessAsyncReader_t337580163::get_offset_of_handle_1(),
	ProcessAsyncReader_t337580163::get_offset_of_state_2(),
	ProcessAsyncReader_t337580163::get_offset_of_callback_3(),
	ProcessAsyncReader_t337580163::get_offset_of_wait_handle_4(),
	ProcessAsyncReader_t337580163::get_offset_of_delayedException_5(),
	ProcessAsyncReader_t337580163::get_offset_of_EndPoint_6(),
	ProcessAsyncReader_t337580163::get_offset_of_buffer_7(),
	ProcessAsyncReader_t337580163::get_offset_of_Offset_8(),
	ProcessAsyncReader_t337580163::get_offset_of_Size_9(),
	ProcessAsyncReader_t337580163::get_offset_of_SockFlags_10(),
	ProcessAsyncReader_t337580163::get_offset_of_AcceptSocket_11(),
	ProcessAsyncReader_t337580163::get_offset_of_Addresses_12(),
	ProcessAsyncReader_t337580163::get_offset_of_port_13(),
	ProcessAsyncReader_t337580163::get_offset_of_Buffers_14(),
	ProcessAsyncReader_t337580163::get_offset_of_ReuseSocket_15(),
	ProcessAsyncReader_t337580163::get_offset_of_acc_socket_16(),
	ProcessAsyncReader_t337580163::get_offset_of_total_17(),
	ProcessAsyncReader_t337580163::get_offset_of_completed_sync_18(),
	ProcessAsyncReader_t337580163::get_offset_of_completed_19(),
	ProcessAsyncReader_t337580163::get_offset_of_err_out_20(),
	ProcessAsyncReader_t337580163::get_offset_of_error_21(),
	ProcessAsyncReader_t337580163::get_offset_of_operation_22(),
	ProcessAsyncReader_t337580163::get_offset_of_ares_23(),
	ProcessAsyncReader_t337580163::get_offset_of_EndCalled_24(),
	ProcessAsyncReader_t337580163::get_offset_of_process_25(),
	ProcessAsyncReader_t337580163::get_offset_of_stream_26(),
	ProcessAsyncReader_t337580163::get_offset_of_sb_27(),
	ProcessAsyncReader_t337580163::get_offset_of_outputEncoding_28(),
	ProcessAsyncReader_t337580163::get_offset_of_ReadHandler_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1221 = { sizeof (ProcessWaitHandle_t3847593151), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1222 = { sizeof (AsyncReadHandler_t1188682440), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1223 = { sizeof (ProcessModuleCollection_t3446348346), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1224 = { sizeof (ProcessStartInfo_t2184852744), -1, sizeof(ProcessStartInfo_t2184852744_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1224[20] = 
{
	ProcessStartInfo_t2184852744::get_offset_of_arguments_0(),
	ProcessStartInfo_t2184852744::get_offset_of_error_dialog_parent_handle_1(),
	ProcessStartInfo_t2184852744::get_offset_of_filename_2(),
	ProcessStartInfo_t2184852744::get_offset_of_verb_3(),
	ProcessStartInfo_t2184852744::get_offset_of_working_directory_4(),
	ProcessStartInfo_t2184852744::get_offset_of_envVars_5(),
	ProcessStartInfo_t2184852744::get_offset_of_create_no_window_6(),
	ProcessStartInfo_t2184852744::get_offset_of_error_dialog_7(),
	ProcessStartInfo_t2184852744::get_offset_of_redirect_standard_error_8(),
	ProcessStartInfo_t2184852744::get_offset_of_redirect_standard_input_9(),
	ProcessStartInfo_t2184852744::get_offset_of_redirect_standard_output_10(),
	ProcessStartInfo_t2184852744::get_offset_of_use_shell_execute_11(),
	ProcessStartInfo_t2184852744::get_offset_of_window_style_12(),
	ProcessStartInfo_t2184852744::get_offset_of_encoding_stderr_13(),
	ProcessStartInfo_t2184852744::get_offset_of_encoding_stdout_14(),
	ProcessStartInfo_t2184852744::get_offset_of_username_15(),
	ProcessStartInfo_t2184852744::get_offset_of_domain_16(),
	ProcessStartInfo_t2184852744::get_offset_of_password_17(),
	ProcessStartInfo_t2184852744::get_offset_of_load_user_profile_18(),
	ProcessStartInfo_t2184852744_StaticFields::get_offset_of_empty_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1225 = { sizeof (ProcessWindowStyle_t3127335931)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1225[5] = 
{
	ProcessWindowStyle_t3127335931::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1226 = { sizeof (SourceLevels_t2339259104)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1226[9] = 
{
	SourceLevels_t2339259104::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1227 = { sizeof (Stopwatch_t305734070), -1, sizeof(Stopwatch_t305734070_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1227[2] = 
{
	Stopwatch_t305734070_StaticFields::get_offset_of_Frequency_0(),
	Stopwatch_t305734070_StaticFields::get_offset_of_IsHighResolution_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1228 = { sizeof (Trace_t2517910945), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1229 = { sizeof (TraceEventCache_t3113663374), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1229[6] = 
{
	TraceEventCache_t3113663374::get_offset_of_started_0(),
	TraceEventCache_t3113663374::get_offset_of_manager_1(),
	TraceEventCache_t3113663374::get_offset_of_callstack_2(),
	TraceEventCache_t3113663374::get_offset_of_thread_3(),
	TraceEventCache_t3113663374::get_offset_of_process_4(),
	TraceEventCache_t3113663374::get_offset_of_timestamp_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1230 = { sizeof (TraceEventType_t181971481)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1230[11] = 
{
	TraceEventType_t181971481::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1231 = { sizeof (TraceFilter_t4153521180), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1232 = { sizeof (TraceImplSettings_t2527703222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1232[4] = 
{
	TraceImplSettings_t2527703222::get_offset_of_AutoFlush_0(),
	TraceImplSettings_t2527703222::get_offset_of_IndentLevel_1(),
	TraceImplSettings_t2527703222::get_offset_of_IndentSize_2(),
	TraceImplSettings_t2527703222::get_offset_of_Listeners_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1233 = { sizeof (TraceImpl_t1390982446), -1, sizeof(TraceImpl_t1390982446_StaticFields), sizeof(TraceImpl_t1390982446_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable1233[6] = 
{
	TraceImpl_t1390982446_StaticFields::get_offset_of_initLock_0(),
	TraceImpl_t1390982446_StaticFields::get_offset_of_autoFlush_1(),
	THREAD_STATIC_FIELD_OFFSET,
	THREAD_STATIC_FIELD_OFFSET,
	TraceImpl_t1390982446_StaticFields::get_offset_of_listeners_4(),
	TraceImpl_t1390982446_StaticFields::get_offset_of_correlation_manager_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1234 = { sizeof (TraceLevel_t1218108719)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1234[6] = 
{
	TraceLevel_t1218108719::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1235 = { sizeof (TraceListenerCollection_t1347122889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1235[1] = 
{
	TraceListenerCollection_t1347122889::get_offset_of_listeners_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1236 = { sizeof (TraceListener_t3975618648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1236[7] = 
{
	TraceListener_t3975618648::get_offset_of_indentLevel_1(),
	TraceListener_t3975618648::get_offset_of_indentSize_2(),
	TraceListener_t3975618648::get_offset_of_attributes_3(),
	TraceListener_t3975618648::get_offset_of_filter_4(),
	TraceListener_t3975618648::get_offset_of_options_5(),
	TraceListener_t3975618648::get_offset_of_name_6(),
	TraceListener_t3975618648::get_offset_of_needIndent_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1237 = { sizeof (TraceOptions_t3546477972)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1237[8] = 
{
	TraceOptions_t3546477972::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1238 = { sizeof (TraceSourceInfo_t2065837391), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1238[3] = 
{
	TraceSourceInfo_t2065837391::get_offset_of_name_0(),
	TraceSourceInfo_t2065837391::get_offset_of_levels_1(),
	TraceSourceInfo_t2065837391::get_offset_of_listeners_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1239 = { sizeof (GenericUriParser_t1141496137), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1240 = { sizeof (CompressionMode_t3714291783)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1240[3] = 
{
	CompressionMode_t3714291783::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1241 = { sizeof (DeflateStream_t4175168077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1241[8] = 
{
	DeflateStream_t4175168077::get_offset_of_base_stream_2(),
	DeflateStream_t4175168077::get_offset_of_mode_3(),
	DeflateStream_t4175168077::get_offset_of_leaveOpen_4(),
	DeflateStream_t4175168077::get_offset_of_disposed_5(),
	DeflateStream_t4175168077::get_offset_of_feeder_6(),
	DeflateStream_t4175168077::get_offset_of_z_stream_7(),
	DeflateStream_t4175168077::get_offset_of_io_buffer_8(),
	DeflateStream_t4175168077::get_offset_of_data_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1242 = { sizeof (UnmanagedReadOrWrite_t876388624), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1243 = { sizeof (ReadMethod_t893206259), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1244 = { sizeof (WriteMethod_t2538911768), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1245 = { sizeof (GZipStream_t3417139389), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1245[1] = 
{
	GZipStream_t3417139389::get_offset_of_deflateStream_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1246 = { sizeof (MonoIO_t2601436416), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1247 = { sizeof (MonoIOError_t367894403)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1247[1783] = 
{
	MonoIOError_t367894403::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1248 = { sizeof (MonoSyncFileStream_t3452145842), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1249 = { sizeof (WriteDelegate_t1613340939), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1250 = { sizeof (ReadDelegate_t2469437439), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1251 = { sizeof (AuthenticationManager_t2084001809), -1, sizeof(AuthenticationManager_t2084001809_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1251[3] = 
{
	AuthenticationManager_t2084001809_StaticFields::get_offset_of_modules_0(),
	AuthenticationManager_t2084001809_StaticFields::get_offset_of_locker_1(),
	AuthenticationManager_t2084001809_StaticFields::get_offset_of_credential_policy_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1252 = { sizeof (AuthenticationSchemes_t3459406435)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1252[8] = 
{
	AuthenticationSchemes_t3459406435::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1253 = { sizeof (Authorization_t542416582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1253[3] = 
{
	Authorization_t542416582::get_offset_of_token_0(),
	Authorization_t542416582::get_offset_of_complete_1(),
	Authorization_t542416582::get_offset_of_module_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1254 = { sizeof (HttpRequestCacheLevel_t1254316655)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1254[10] = 
{
	HttpRequestCacheLevel_t1254316655::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1255 = { sizeof (RequestCacheLevel_t1509648360)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1255[8] = 
{
	RequestCacheLevel_t1509648360::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1256 = { sizeof (ChunkedInputStream_t1889541612), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1256[4] = 
{
	ChunkedInputStream_t1889541612::get_offset_of_disposed_8(),
	ChunkedInputStream_t1889541612::get_offset_of_decoder_9(),
	ChunkedInputStream_t1889541612::get_offset_of_context_10(),
	ChunkedInputStream_t1889541612::get_offset_of_no_more_data_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1257 = { sizeof (ReadBufferState_t2902666188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1257[5] = 
{
	ReadBufferState_t2902666188::get_offset_of_Buffer_0(),
	ReadBufferState_t2902666188::get_offset_of_Offset_1(),
	ReadBufferState_t2902666188::get_offset_of_Count_2(),
	ReadBufferState_t2902666188::get_offset_of_InitialCount_3(),
	ReadBufferState_t2902666188::get_offset_of_Ares_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1258 = { sizeof (ChunkStream_t2634567336), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1258[9] = 
{
	ChunkStream_t2634567336::get_offset_of_headers_0(),
	ChunkStream_t2634567336::get_offset_of_chunkSize_1(),
	ChunkStream_t2634567336::get_offset_of_chunkRead_2(),
	ChunkStream_t2634567336::get_offset_of_state_3(),
	ChunkStream_t2634567336::get_offset_of_saved_4(),
	ChunkStream_t2634567336::get_offset_of_sawCR_5(),
	ChunkStream_t2634567336::get_offset_of_gotit_6(),
	ChunkStream_t2634567336::get_offset_of_trailerState_7(),
	ChunkStream_t2634567336::get_offset_of_chunks_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1259 = { sizeof (State_t4053927353)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1259[5] = 
{
	State_t4053927353::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1260 = { sizeof (Chunk_t1455545707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1260[2] = 
{
	Chunk_t1455545707::get_offset_of_Bytes_0(),
	Chunk_t1455545707::get_offset_of_Offset_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1261 = { sizeof (AuthenticationModuleElementCollection_t1161221431), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1262 = { sizeof (AuthenticationModuleElement_t2289740666), -1, sizeof(AuthenticationModuleElement_t2289740666_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1262[2] = 
{
	AuthenticationModuleElement_t2289740666_StaticFields::get_offset_of_properties_13(),
	AuthenticationModuleElement_t2289740666_StaticFields::get_offset_of_typeProp_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1263 = { sizeof (AuthenticationModulesSection_t1083221556), -1, sizeof(AuthenticationModulesSection_t1083221556_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1263[2] = 
{
	AuthenticationModulesSection_t1083221556_StaticFields::get_offset_of_properties_17(),
	AuthenticationModulesSection_t1083221556_StaticFields::get_offset_of_authenticationModulesProp_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1264 = { sizeof (BypassElementCollection_t47326401), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1265 = { sizeof (BypassElement_t2358616601), -1, sizeof(BypassElement_t2358616601_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1265[2] = 
{
	BypassElement_t2358616601_StaticFields::get_offset_of_properties_13(),
	BypassElement_t2358616601_StaticFields::get_offset_of_addressProp_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1266 = { sizeof (ConnectionManagementElementCollection_t3860227195), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1267 = { sizeof (ConnectionManagementElement_t3857438253), -1, sizeof(ConnectionManagementElement_t3857438253_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1267[3] = 
{
	ConnectionManagementElement_t3857438253_StaticFields::get_offset_of_properties_13(),
	ConnectionManagementElement_t3857438253_StaticFields::get_offset_of_addressProp_14(),
	ConnectionManagementElement_t3857438253_StaticFields::get_offset_of_maxConnectionProp_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1268 = { sizeof (ConnectionManagementData_t2003128658), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1268[1] = 
{
	ConnectionManagementData_t2003128658::get_offset_of_data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1269 = { sizeof (HandlersUtil_t2371960855), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1270 = { sizeof (ConnectionManagementSection_t1603642748), -1, sizeof(ConnectionManagementSection_t1603642748_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1270[2] = 
{
	ConnectionManagementSection_t1603642748_StaticFields::get_offset_of_connectionManagementProp_17(),
	ConnectionManagementSection_t1603642748_StaticFields::get_offset_of_properties_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1271 = { sizeof (DefaultProxySection_t4167594595), -1, sizeof(DefaultProxySection_t4167594595_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1271[6] = 
{
	DefaultProxySection_t4167594595_StaticFields::get_offset_of_properties_17(),
	DefaultProxySection_t4167594595_StaticFields::get_offset_of_bypassListProp_18(),
	DefaultProxySection_t4167594595_StaticFields::get_offset_of_enabledProp_19(),
	DefaultProxySection_t4167594595_StaticFields::get_offset_of_moduleProp_20(),
	DefaultProxySection_t4167594595_StaticFields::get_offset_of_proxyProp_21(),
	DefaultProxySection_t4167594595_StaticFields::get_offset_of_useDefaultCredentialsProp_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1272 = { sizeof (FtpCachePolicyElement_t1580201543), -1, sizeof(FtpCachePolicyElement_t1580201543_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1272[2] = 
{
	FtpCachePolicyElement_t1580201543_StaticFields::get_offset_of_policyLevelProp_13(),
	FtpCachePolicyElement_t1580201543_StaticFields::get_offset_of_properties_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1273 = { sizeof (HttpCachePolicyElement_t409334445), -1, sizeof(HttpCachePolicyElement_t409334445_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1273[5] = 
{
	HttpCachePolicyElement_t409334445_StaticFields::get_offset_of_maximumAgeProp_13(),
	HttpCachePolicyElement_t409334445_StaticFields::get_offset_of_maximumStaleProp_14(),
	HttpCachePolicyElement_t409334445_StaticFields::get_offset_of_minimumFreshProp_15(),
	HttpCachePolicyElement_t409334445_StaticFields::get_offset_of_policyLevelProp_16(),
	HttpCachePolicyElement_t409334445_StaticFields::get_offset_of_properties_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1274 = { sizeof (HttpWebRequestElement_t2801692355), -1, sizeof(HttpWebRequestElement_t2801692355_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1274[5] = 
{
	HttpWebRequestElement_t2801692355_StaticFields::get_offset_of_maximumErrorResponseLengthProp_13(),
	HttpWebRequestElement_t2801692355_StaticFields::get_offset_of_maximumResponseHeadersLengthProp_14(),
	HttpWebRequestElement_t2801692355_StaticFields::get_offset_of_maximumUnauthorizedUploadLengthProp_15(),
	HttpWebRequestElement_t2801692355_StaticFields::get_offset_of_useUnsafeHeaderParsingProp_16(),
	HttpWebRequestElement_t2801692355_StaticFields::get_offset_of_properties_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1275 = { sizeof (Ipv6Element_t180053194), -1, sizeof(Ipv6Element_t180053194_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1275[2] = 
{
	Ipv6Element_t180053194_StaticFields::get_offset_of_properties_13(),
	Ipv6Element_t180053194_StaticFields::get_offset_of_enabledProp_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1276 = { sizeof (MailSettingsSectionGroup_t1796674833), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1277 = { sizeof (ModuleElement_t3252950656), -1, sizeof(ModuleElement_t3252950656_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1277[2] = 
{
	ModuleElement_t3252950656_StaticFields::get_offset_of_properties_13(),
	ModuleElement_t3252950656_StaticFields::get_offset_of_typeProp_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1278 = { sizeof (NetConfigurationHandler_t3348259332), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1279 = { sizeof (NetSectionGroup_t3270122580), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1280 = { sizeof (PerformanceCountersElement_t4093363992), -1, sizeof(PerformanceCountersElement_t4093363992_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1280[2] = 
{
	PerformanceCountersElement_t4093363992_StaticFields::get_offset_of_enabledProp_13(),
	PerformanceCountersElement_t4093363992_StaticFields::get_offset_of_properties_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1281 = { sizeof (ProxyElement_t3214064751), -1, sizeof(ProxyElement_t3214064751_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1281[6] = 
{
	ProxyElement_t3214064751_StaticFields::get_offset_of_properties_13(),
	ProxyElement_t3214064751_StaticFields::get_offset_of_autoDetectProp_14(),
	ProxyElement_t3214064751_StaticFields::get_offset_of_bypassOnLocalProp_15(),
	ProxyElement_t3214064751_StaticFields::get_offset_of_proxyAddressProp_16(),
	ProxyElement_t3214064751_StaticFields::get_offset_of_scriptLocationProp_17(),
	ProxyElement_t3214064751_StaticFields::get_offset_of_useSystemDefaultProp_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1282 = { sizeof (BypassOnLocalValues_t945670496)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1282[4] = 
{
	BypassOnLocalValues_t945670496::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1283 = { sizeof (UseSystemDefaultValues_t2711047072)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1283[4] = 
{
	UseSystemDefaultValues_t2711047072::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1284 = { sizeof (AutoDetectValues_t1649618618)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1284[4] = 
{
	AutoDetectValues_t1649618618::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1285 = { sizeof (RequestCachingSection_t1693238435), -1, sizeof(RequestCachingSection_t1693238435_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1285[7] = 
{
	RequestCachingSection_t1693238435_StaticFields::get_offset_of_properties_17(),
	RequestCachingSection_t1693238435_StaticFields::get_offset_of_defaultFtpCachePolicyProp_18(),
	RequestCachingSection_t1693238435_StaticFields::get_offset_of_defaultHttpCachePolicyProp_19(),
	RequestCachingSection_t1693238435_StaticFields::get_offset_of_defaultPolicyLevelProp_20(),
	RequestCachingSection_t1693238435_StaticFields::get_offset_of_disableAllCachingProp_21(),
	RequestCachingSection_t1693238435_StaticFields::get_offset_of_isPrivateCacheProp_22(),
	RequestCachingSection_t1693238435_StaticFields::get_offset_of_unspecifiedMaximumAgeProp_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1286 = { sizeof (ServicePointManagerElement_t2768640361), -1, sizeof(ServicePointManagerElement_t2768640361_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1286[7] = 
{
	ServicePointManagerElement_t2768640361_StaticFields::get_offset_of_properties_13(),
	ServicePointManagerElement_t2768640361_StaticFields::get_offset_of_checkCertificateNameProp_14(),
	ServicePointManagerElement_t2768640361_StaticFields::get_offset_of_checkCertificateRevocationListProp_15(),
	ServicePointManagerElement_t2768640361_StaticFields::get_offset_of_dnsRefreshTimeoutProp_16(),
	ServicePointManagerElement_t2768640361_StaticFields::get_offset_of_enableDnsRoundRobinProp_17(),
	ServicePointManagerElement_t2768640361_StaticFields::get_offset_of_expect100ContinueProp_18(),
	ServicePointManagerElement_t2768640361_StaticFields::get_offset_of_useNagleAlgorithmProp_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1287 = { sizeof (SettingsSection_t1259474535), -1, sizeof(SettingsSection_t1259474535_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1287[7] = 
{
	SettingsSection_t1259474535_StaticFields::get_offset_of_properties_17(),
	SettingsSection_t1259474535_StaticFields::get_offset_of_httpWebRequestProp_18(),
	SettingsSection_t1259474535_StaticFields::get_offset_of_ipv6Prop_19(),
	SettingsSection_t1259474535_StaticFields::get_offset_of_performanceCountersProp_20(),
	SettingsSection_t1259474535_StaticFields::get_offset_of_servicePointManagerProp_21(),
	SettingsSection_t1259474535_StaticFields::get_offset_of_webProxyScriptProp_22(),
	SettingsSection_t1259474535_StaticFields::get_offset_of_socketProp_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1288 = { sizeof (SocketElement_t3329874080), -1, sizeof(SocketElement_t3329874080_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1288[3] = 
{
	SocketElement_t3329874080_StaticFields::get_offset_of_properties_13(),
	SocketElement_t3329874080_StaticFields::get_offset_of_alwaysUseCompletionPortsForAcceptProp_14(),
	SocketElement_t3329874080_StaticFields::get_offset_of_alwaysUseCompletionPortsForConnectProp_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1289 = { sizeof (WebProxyScriptElement_t477406598), -1, sizeof(WebProxyScriptElement_t477406598_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1289[2] = 
{
	WebProxyScriptElement_t477406598_StaticFields::get_offset_of_downloadTimeoutProp_13(),
	WebProxyScriptElement_t477406598_StaticFields::get_offset_of_properties_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1290 = { sizeof (WebRequestModuleElementCollection_t925190782), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1291 = { sizeof (WebRequestModuleElement_t1406085120), -1, sizeof(WebRequestModuleElement_t1406085120_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1291[3] = 
{
	WebRequestModuleElement_t1406085120_StaticFields::get_offset_of_properties_13(),
	WebRequestModuleElement_t1406085120_StaticFields::get_offset_of_prefixProp_14(),
	WebRequestModuleElement_t1406085120_StaticFields::get_offset_of_typeProp_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1292 = { sizeof (WebRequestModuleHandler_t914399239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1293 = { sizeof (WebRequestModulesSection_t4132732301), -1, sizeof(WebRequestModulesSection_t4132732301_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1293[2] = 
{
	WebRequestModulesSection_t4132732301_StaticFields::get_offset_of_properties_17(),
	WebRequestModulesSection_t4132732301_StaticFields::get_offset_of_webRequestModulesProp_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1294 = { sizeof (CookieCollection_t3881042616), -1, sizeof(CookieCollection_t3881042616_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1294[2] = 
{
	CookieCollection_t3881042616::get_offset_of_list_0(),
	CookieCollection_t3881042616_StaticFields::get_offset_of_Comparer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1295 = { sizeof (CookieCollectionComparer_t1373927847), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1296 = { sizeof (CookieContainer_t2331592909), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1296[4] = 
{
	CookieContainer_t2331592909::get_offset_of_capacity_0(),
	CookieContainer_t2331592909::get_offset_of_perDomainCapacity_1(),
	CookieContainer_t2331592909::get_offset_of_maxCookieSize_2(),
	CookieContainer_t2331592909::get_offset_of_cookies_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1297 = { sizeof (Cookie_t993873397), -1, sizeof(Cookie_t993873397_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1297[18] = 
{
	Cookie_t993873397::get_offset_of_comment_0(),
	Cookie_t993873397::get_offset_of_commentUri_1(),
	Cookie_t993873397::get_offset_of_discard_2(),
	Cookie_t993873397::get_offset_of_domain_3(),
	Cookie_t993873397::get_offset_of_expires_4(),
	Cookie_t993873397::get_offset_of_httpOnly_5(),
	Cookie_t993873397::get_offset_of_name_6(),
	Cookie_t993873397::get_offset_of_path_7(),
	Cookie_t993873397::get_offset_of_port_8(),
	Cookie_t993873397::get_offset_of_ports_9(),
	Cookie_t993873397::get_offset_of_secure_10(),
	Cookie_t993873397::get_offset_of_timestamp_11(),
	Cookie_t993873397::get_offset_of_val_12(),
	Cookie_t993873397::get_offset_of_version_13(),
	Cookie_t993873397_StaticFields::get_offset_of_reservedCharsName_14(),
	Cookie_t993873397_StaticFields::get_offset_of_portSeparators_15(),
	Cookie_t993873397_StaticFields::get_offset_of_tspecials_16(),
	Cookie_t993873397::get_offset_of_exact_domain_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1298 = { sizeof (CookieException_t2325395694), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1299 = { sizeof (DecompressionMethods_t1612219745)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1299[4] = 
{
	DecompressionMethods_t1612219745::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
