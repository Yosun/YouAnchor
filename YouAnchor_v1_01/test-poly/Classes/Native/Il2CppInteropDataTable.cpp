﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"









extern "C" void Context_t1744531130_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Context_t1744531130_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Context_t1744531130_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Context_t1744531130_0_0_0;
extern "C" void Escape_t3294788190_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Escape_t3294788190_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Escape_t3294788190_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Escape_t3294788190_0_0_0;
extern "C" void PreviousInfo_t2148130204_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PreviousInfo_t2148130204_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PreviousInfo_t2148130204_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PreviousInfo_t2148130204_0_0_0;
extern "C" void UriScheme_t2867806342_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UriScheme_t2867806342_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UriScheme_t2867806342_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UriScheme_t2867806342_0_0_0;
extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t682969308();
extern const RuntimeType AppDomainInitializer_t682969308_0_0_0;
extern "C" void DelegatePInvokeWrapper_Swapper_t2822380397();
extern const RuntimeType Swapper_t2822380397_0_0_0;
extern "C" void DictionaryEntry_t3123975638_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DictionaryEntry_t3123975638_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DictionaryEntry_t3123975638_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DictionaryEntry_t3123975638_0_0_0;
extern "C" void Slot_t3975888750_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t3975888750_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t3975888750_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t3975888750_0_0_0;
extern "C" void Slot_t384495010_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t384495010_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t384495010_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t384495010_0_0_0;
extern "C" void DelegatePInvokeWrapper_InternalCancelHandler_t872516951();
extern const RuntimeType InternalCancelHandler_t872516951_0_0_0;
extern "C" void ConsoleKeyInfo_t1802691652_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ConsoleKeyInfo_t1802691652_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ConsoleKeyInfo_t1802691652_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ConsoleKeyInfo_t1802691652_0_0_0;
extern "C" void Enum_t4135868527_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Enum_t4135868527_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Enum_t4135868527_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Enum_t4135868527_0_0_0;
extern "C" void InputRecord_t2660212290_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void InputRecord_t2660212290_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void InputRecord_t2660212290_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType InputRecord_t2660212290_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t714865915();
extern const RuntimeType ReadDelegate_t714865915_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t4270993571();
extern const RuntimeType WriteDelegate_t4270993571_0_0_0;
extern "C" void MonoIOStat_t592533987_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoIOStat_t592533987_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoIOStat_t592533987_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoIOStat_t592533987_0_0_0;
extern "C" void MonoEnumInfo_t3694469084_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEnumInfo_t3694469084_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEnumInfo_t3694469084_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEnumInfo_t3694469084_0_0_0;
extern "C" void CustomAttributeNamedArgument_t287865710_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeNamedArgument_t287865710_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeNamedArgument_t287865710_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeNamedArgument_t287865710_0_0_0;
extern "C" void CustomAttributeTypedArgument_t2723150157_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeTypedArgument_t2723150157_0_0_0;
extern "C" void ILTokenInfo_t2325775114_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ILTokenInfo_t2325775114_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ILTokenInfo_t2325775114_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ILTokenInfo_t2325775114_0_0_0;
extern "C" void MonoResource_t4103430009_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoResource_t4103430009_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoResource_t4103430009_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoResource_t4103430009_0_0_0;
extern "C" void MonoWin32Resource_t1904229483_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoWin32Resource_t1904229483_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoWin32Resource_t1904229483_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoWin32Resource_t1904229483_0_0_0;
extern "C" void RefEmitPermissionSet_t484390987_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RefEmitPermissionSet_t484390987_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RefEmitPermissionSet_t484390987_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RefEmitPermissionSet_t484390987_0_0_0;
extern "C" void MonoEventInfo_t346866618_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEventInfo_t346866618_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEventInfo_t346866618_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEventInfo_t346866618_0_0_0;
extern "C" void MonoMethodInfo_t1248819020_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoMethodInfo_t1248819020_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoMethodInfo_t1248819020_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoMethodInfo_t1248819020_0_0_0;
extern "C" void MonoPropertyInfo_t3087356066_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoPropertyInfo_t3087356066_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoPropertyInfo_t3087356066_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoPropertyInfo_t3087356066_0_0_0;
extern "C" void ParameterModifier_t1461694466_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParameterModifier_t1461694466_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParameterModifier_t1461694466_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ParameterModifier_t1461694466_0_0_0;
extern "C" void ResourceCacheItem_t51292791_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceCacheItem_t51292791_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceCacheItem_t51292791_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceCacheItem_t51292791_0_0_0;
extern "C" void ResourceInfo_t2872965302_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceInfo_t2872965302_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceInfo_t2872965302_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceInfo_t2872965302_0_0_0;
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t387175271();
extern const RuntimeType CrossContextDelegate_t387175271_0_0_0;
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t3280319253();
extern const RuntimeType CallbackHandler_t3280319253_0_0_0;
extern "C" void SerializationEntry_t648286436_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SerializationEntry_t648286436_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SerializationEntry_t648286436_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SerializationEntry_t648286436_0_0_0;
extern "C" void StreamingContext_t3711869237_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StreamingContext_t3711869237_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StreamingContext_t3711869237_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType StreamingContext_t3711869237_0_0_0;
extern "C" void DSAParameters_t1885824122_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DSAParameters_t1885824122_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DSAParameters_t1885824122_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DSAParameters_t1885824122_0_0_0;
extern "C" void RSAParameters_t1728406613_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RSAParameters_t1728406613_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RSAParameters_t1728406613_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RSAParameters_t1728406613_0_0_0;
extern "C" void SNIP_t4156255223_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SNIP_t4156255223_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SNIP_t4156255223_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SNIP_t4156255223_0_0_0;
extern "C" void SecurityFrame_t1422462475_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SecurityFrame_t1422462475_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SecurityFrame_t1422462475_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SecurityFrame_t1422462475_0_0_0;
extern "C" void DelegatePInvokeWrapper_ThreadStart_t1006689297();
extern const RuntimeType ThreadStart_t1006689297_0_0_0;
extern "C" void ValueType_t3640485471_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ValueType_t3640485471_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ValueType_t3640485471_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ValueType_t3640485471_0_0_0;
extern "C" void DelegatePInvokeWrapper_AsyncReadHandler_t1188682440();
extern const RuntimeType AsyncReadHandler_t1188682440_0_0_0;
extern "C" void ProcessAsyncReader_t337580163_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ProcessAsyncReader_t337580163_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ProcessAsyncReader_t337580163_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ProcessAsyncReader_t337580163_0_0_0;
extern "C" void ProcInfo_t2917059746_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ProcInfo_t2917059746_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ProcInfo_t2917059746_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ProcInfo_t2917059746_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadMethod_t893206259();
extern const RuntimeType ReadMethod_t893206259_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnmanagedReadOrWrite_t876388624();
extern const RuntimeType UnmanagedReadOrWrite_t876388624_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteMethod_t2538911768();
extern const RuntimeType WriteMethod_t2538911768_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t2469437439();
extern const RuntimeType ReadDelegate_t2469437439_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t1613340939();
extern const RuntimeType WriteDelegate_t1613340939_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t4266946825();
extern const RuntimeType ReadDelegate_t4266946825_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t2016697242();
extern const RuntimeType WriteDelegate_t2016697242_0_0_0;
extern "C" void DelegatePInvokeWrapper_SocketAsyncCall_t1521370843();
extern const RuntimeType SocketAsyncCall_t1521370843_0_0_0;
extern "C" void SocketAsyncResult_t2080034863_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SocketAsyncResult_t2080034863_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SocketAsyncResult_t2080034863_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SocketAsyncResult_t2080034863_0_0_0;
extern "C" void X509ChainStatus_t133602714_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void X509ChainStatus_t133602714_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void X509ChainStatus_t133602714_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType X509ChainStatus_t133602714_0_0_0;
extern "C" void IntStack_t2189327687_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IntStack_t2189327687_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IntStack_t2189327687_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType IntStack_t2189327687_0_0_0;
extern "C" void Interval_t1802865632_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Interval_t1802865632_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Interval_t1802865632_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Interval_t1802865632_0_0_0;
extern "C" void DelegatePInvokeWrapper_CostDelegate_t1722821004();
extern const RuntimeType CostDelegate_t1722821004_0_0_0;
extern "C" void IntStack_t561212167_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IntStack_t561212167_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IntStack_t561212167_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType IntStack_t561212167_0_0_0;
extern "C" void UriScheme_t722425697_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UriScheme_t722425697_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UriScheme_t722425697_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UriScheme_t722425697_0_0_0;
extern "C" void TagName_t2891256255_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TagName_t2891256255_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TagName_t2891256255_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TagName_t2891256255_0_0_0;
extern "C" void QNameValueType_t615604793_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void QNameValueType_t615604793_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void QNameValueType_t615604793_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType QNameValueType_t615604793_0_0_0;
extern "C" void StringArrayValueType_t3147326440_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StringArrayValueType_t3147326440_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StringArrayValueType_t3147326440_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType StringArrayValueType_t3147326440_0_0_0;
extern "C" void StringValueType_t261329552_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StringValueType_t261329552_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StringValueType_t261329552_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType StringValueType_t261329552_0_0_0;
extern "C" void UriValueType_t2866347695_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UriValueType_t2866347695_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UriValueType_t2866347695_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UriValueType_t2866347695_0_0_0;
extern "C" void NsDecl_t3938094415_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void NsDecl_t3938094415_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void NsDecl_t3938094415_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType NsDecl_t3938094415_0_0_0;
extern "C" void NsScope_t3958624705_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void NsScope_t3958624705_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void NsScope_t3958624705_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType NsScope_t3958624705_0_0_0;
extern "C" void DelegatePInvokeWrapper_CharGetter_t1703763694();
extern const RuntimeType CharGetter_t1703763694_0_0_0;
extern "C" void DelegatePInvokeWrapper_Action_t1264377477();
extern const RuntimeType Action_t1264377477_0_0_0;
extern "C" void TimeType_t2507513283_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TimeType_t2507513283_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TimeType_t2507513283_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TimeType_t2507513283_0_0_0;
extern "C" void TransitionTime_t449921781_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TransitionTime_t449921781_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TransitionTime_t449921781_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TransitionTime_t449921781_0_0_0;
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationCurve_t3046754366_0_0_0;
extern "C" void DelegatePInvokeWrapper_LogCallback_t3588208630();
extern const RuntimeType LogCallback_t3588208630_0_0_0;
extern "C" void DelegatePInvokeWrapper_LowMemoryCallback_t4104246196();
extern const RuntimeType LowMemoryCallback_t4104246196_0_0_0;
extern "C" void AssetBundleCreateRequest_t3119663542_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssetBundleCreateRequest_t3119663542_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssetBundleCreateRequest_t3119663542_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AssetBundleCreateRequest_t3119663542_0_0_0;
extern "C" void AssetBundleRequest_t699759206_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssetBundleRequest_t699759206_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssetBundleRequest_t699759206_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AssetBundleRequest_t699759206_0_0_0;
extern "C" void AsyncOperation_t1445031843_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AsyncOperation_t1445031843_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AsyncOperation_t1445031843_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AsyncOperation_t1445031843_0_0_0;
extern "C" void OrderBlock_t1585977831_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void OrderBlock_t1585977831_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void OrderBlock_t1585977831_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType OrderBlock_t1585977831_0_0_0;
extern "C" void Coroutine_t3829159415_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Coroutine_t3829159415_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Coroutine_t3829159415_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Coroutine_t3829159415_0_0_0;
extern "C" void DelegatePInvokeWrapper_CSSMeasureFunc_t1554030124();
extern const RuntimeType CSSMeasureFunc_t1554030124_0_0_0;
extern "C" void CullingGroup_t2096318768_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CullingGroup_t2096318768_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CullingGroup_t2096318768_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CullingGroup_t2096318768_0_0_0;
extern "C" void DelegatePInvokeWrapper_StateChanged_t2136737110();
extern const RuntimeType StateChanged_t2136737110_0_0_0;
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t51287044();
extern const RuntimeType DisplaysUpdatedDelegate_t51287044_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnityAction_t3245792599();
extern const RuntimeType UnityAction_t3245792599_0_0_0;
extern "C" void FailedToLoadScriptObject_t547604379_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FailedToLoadScriptObject_t547604379_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FailedToLoadScriptObject_t547604379_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FailedToLoadScriptObject_t547604379_0_0_0;
extern "C" void Gradient_t3067099924_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Gradient_t3067099924_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Gradient_t3067099924_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Gradient_t3067099924_0_0_0;
extern "C" void Object_t631007953_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Object_t631007953_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Object_t631007953_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Object_t631007953_0_0_0;
extern "C" void PlayableBinding_t354260709_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlayableBinding_t354260709_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlayableBinding_t354260709_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PlayableBinding_t354260709_0_0_0;
extern "C" void RectOffset_t1369453676_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RectOffset_t1369453676_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RectOffset_t1369453676_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RectOffset_t1369453676_0_0_0;
extern "C" void ResourceRequest_t3109103591_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceRequest_t3109103591_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceRequest_t3109103591_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceRequest_t3109103591_0_0_0;
extern "C" void ScriptableObject_t2528358522_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ScriptableObject_t2528358522_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ScriptableObject_t2528358522_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ScriptableObject_t2528358522_0_0_0;
extern "C" void HitInfo_t3229609740_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HitInfo_t3229609740_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HitInfo_t3229609740_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HitInfo_t3229609740_0_0_0;
extern "C" void TrackedReference_t1199777556_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TrackedReference_t1199777556_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TrackedReference_t1199777556_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TrackedReference_t1199777556_0_0_0;
extern "C" void DelegatePInvokeWrapper_RequestAtlasCallback_t3100554279();
extern const RuntimeType RequestAtlasCallback_t3100554279_0_0_0;
extern "C" void WorkRequest_t1354518612_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WorkRequest_t1354518612_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WorkRequest_t1354518612_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WorkRequest_t1354518612_0_0_0;
extern "C" void WaitForSeconds_t1699091251_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WaitForSeconds_t1699091251_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WaitForSeconds_t1699091251_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WaitForSeconds_t1699091251_0_0_0;
extern "C" void YieldInstruction_t403091072_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void YieldInstruction_t403091072_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void YieldInstruction_t403091072_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType YieldInstruction_t403091072_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t1677636661();
extern const RuntimeType PCMReaderCallback_t1677636661_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t1059417452();
extern const RuntimeType PCMSetPositionCallback_t1059417452_0_0_0;
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t2089929874();
extern const RuntimeType AudioConfigurationChangeHandler_t2089929874_0_0_0;
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t2467502454();
extern const RuntimeType FontTextureRebuildCallback_t2467502454_0_0_0;
extern "C" void TextGenerationSettings_t1351628751_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerationSettings_t1351628751_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerationSettings_t1351628751_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerationSettings_t1351628751_0_0_0;
extern "C" void TextGenerator_t3211863866_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerator_t3211863866_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerator_t3211863866_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerator_t3211863866_0_0_0;
extern "C" void DownloadHandler_t2937767557_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DownloadHandler_t2937767557_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DownloadHandler_t2937767557_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DownloadHandler_t2937767557_0_0_0;
extern "C" void DownloadHandlerBuffer_t2928496527_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DownloadHandlerBuffer_t2928496527_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DownloadHandlerBuffer_t2928496527_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DownloadHandlerBuffer_t2928496527_0_0_0;
extern "C" void UnityWebRequest_t463507806_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityWebRequest_t463507806_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityWebRequest_t463507806_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityWebRequest_t463507806_0_0_0;
extern "C" void UnityWebRequestAsyncOperation_t3852015985_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityWebRequestAsyncOperation_t3852015985_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityWebRequestAsyncOperation_t3852015985_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityWebRequestAsyncOperation_t3852015985_0_0_0;
extern "C" void UploadHandler_t2993558019_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UploadHandler_t2993558019_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UploadHandler_t2993558019_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UploadHandler_t2993558019_0_0_0;
extern "C" void UploadHandlerRaw_t25761545_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UploadHandlerRaw_t25761545_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UploadHandlerRaw_t25761545_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UploadHandlerRaw_t25761545_0_0_0;
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationEvent_t1536042487_0_0_0;
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimatorTransitionInfo_t2534804151_0_0_0;
extern "C" void HumanBone_t2465339518_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HumanBone_t2465339518_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HumanBone_t2465339518_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HumanBone_t2465339518_0_0_0;
extern "C" void SkeletonBone_t4134054672_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SkeletonBone_t4134054672_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SkeletonBone_t4134054672_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SkeletonBone_t4134054672_0_0_0;
extern "C" void GcAchievementData_t675222246_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementData_t675222246_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementData_t675222246_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementData_t675222246_0_0_0;
extern "C" void GcAchievementDescriptionData_t643925653_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementDescriptionData_t643925653_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementDescriptionData_t643925653_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementDescriptionData_t643925653_0_0_0;
extern "C" void GcLeaderboard_t4132273028_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcLeaderboard_t4132273028_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcLeaderboard_t4132273028_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcLeaderboard_t4132273028_0_0_0;
extern "C" void GcScoreData_t2125309831_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcScoreData_t2125309831_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcScoreData_t2125309831_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcScoreData_t2125309831_0_0_0;
extern "C" void GcUserProfileData_t2719720026_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcUserProfileData_t2719720026_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcUserProfileData_t2719720026_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcUserProfileData_t2719720026_0_0_0;
extern "C" void Event_t2956885303_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Event_t2956885303_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Event_t2956885303_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Event_t2956885303_0_0_0;
extern "C" void DelegatePInvokeWrapper_WindowFunction_t3146511083();
extern const RuntimeType WindowFunction_t3146511083_0_0_0;
extern "C" void GUIContent_t3050628031_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIContent_t3050628031_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIContent_t3050628031_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIContent_t3050628031_0_0_0;
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t1143955295();
extern const RuntimeType SkinChangedDelegate_t1143955295_0_0_0;
extern "C" void GUIStyle_t3956901511_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyle_t3956901511_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyle_t3956901511_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyle_t3956901511_0_0_0;
extern "C" void GUIStyleState_t1397964415_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyleState_t1397964415_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyleState_t1397964415_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyleState_t1397964415_0_0_0;
extern "C" void Collision2D_t2842956331_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision2D_t2842956331_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision2D_t2842956331_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Collision2D_t2842956331_0_0_0;
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ContactFilter2D_t3805203441_0_0_0;
extern "C" void RaycastHit2D_t2279581989_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit2D_t2279581989_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit2D_t2279581989_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastHit2D_t2279581989_0_0_0;
extern "C" void Collision_t4262080450_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision_t4262080450_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision_t4262080450_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Collision_t4262080450_0_0_0;
extern "C" void ControllerColliderHit_t240592346_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ControllerColliderHit_t240592346_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ControllerColliderHit_t240592346_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ControllerColliderHit_t240592346_0_0_0;
extern "C" void RaycastHit_t1056001966_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit_t1056001966_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit_t1056001966_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastHit_t1056001966_0_0_0;
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t3309123499();
extern const RuntimeType WillRenderCanvases_t3309123499_0_0_0;
extern "C" void CustomEventData_t317522481_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomEventData_t317522481_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomEventData_t317522481_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomEventData_t317522481_0_0_0;
extern "C" void UnityAnalyticsHandler_t3011359618_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityAnalyticsHandler_t3011359618_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityAnalyticsHandler_t3011359618_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityAnalyticsHandler_t3011359618_0_0_0;
extern "C" void DelegatePInvokeWrapper_SessionStateChanged_t3163629820();
extern const RuntimeType SessionStateChanged_t3163629820_0_0_0;
extern "C" void DelegatePInvokeWrapper_UpdatedEventHandler_t1027848393();
extern const RuntimeType UpdatedEventHandler_t1027848393_0_0_0;
extern "C" void DownloadHandlerTexture_t2149801800_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DownloadHandlerTexture_t2149801800_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DownloadHandlerTexture_t2149801800_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DownloadHandlerTexture_t2149801800_0_0_0;
extern "C" void DelegatePInvokeWrapper_EaseFunction_t3531141372();
extern const RuntimeType EaseFunction_t3531141372_0_0_0;
extern "C" void ColorOptions_t1487297155_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ColorOptions_t1487297155_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ColorOptions_t1487297155_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ColorOptions_t1487297155_0_0_0;
extern "C" void FloatOptions_t1203667100_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FloatOptions_t1203667100_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FloatOptions_t1203667100_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FloatOptions_t1203667100_0_0_0;
extern "C" void PathOptions_t2074623791_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PathOptions_t2074623791_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PathOptions_t2074623791_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PathOptions_t2074623791_0_0_0;
extern "C" void RectOptions_t1018205596_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RectOptions_t1018205596_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RectOptions_t1018205596_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RectOptions_t1018205596_0_0_0;
extern "C" void StringOptions_t3992490940_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StringOptions_t3992490940_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StringOptions_t3992490940_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType StringOptions_t3992490940_0_0_0;
extern "C" void UintOptions_t1006674242_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UintOptions_t1006674242_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UintOptions_t1006674242_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UintOptions_t1006674242_0_0_0;
extern "C" void Vector3ArrayOptions_t534739431_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Vector3ArrayOptions_t534739431_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Vector3ArrayOptions_t534739431_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Vector3ArrayOptions_t534739431_0_0_0;
extern "C" void VectorOptions_t1354903650_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void VectorOptions_t1354903650_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void VectorOptions_t1354903650_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType VectorOptions_t1354903650_0_0_0;
extern "C" void DelegatePInvokeWrapper_TweenCallback_t3727756325();
extern const RuntimeType TweenCallback_t3727756325_0_0_0;
extern "C" void RaycastResult_t3360306849_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastResult_t3360306849_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastResult_t3360306849_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastResult_t3360306849_0_0_0;
extern "C" void ColorTween_t809614380_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ColorTween_t809614380_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ColorTween_t809614380_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ColorTween_t809614380_0_0_0;
extern "C" void FloatTween_t1274330004_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FloatTween_t1274330004_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FloatTween_t1274330004_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FloatTween_t1274330004_0_0_0;
extern "C" void Resources_t1597885468_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Resources_t1597885468_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Resources_t1597885468_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Resources_t1597885468_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t2355412304();
extern const RuntimeType OnValidateInput_t2355412304_0_0_0;
extern "C" void Navigation_t3049316579_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Navigation_t3049316579_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Navigation_t3049316579_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Navigation_t3049316579_0_0_0;
extern "C" void SpriteState_t1362986479_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SpriteState_t1362986479_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SpriteState_t1362986479_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SpriteState_t1362986479_0_0_0;
extern "C" void JsonPosition_t2528027714_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void JsonPosition_t2528027714_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void JsonPosition_t2528027714_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType JsonPosition_t2528027714_0_0_0;
extern "C" void TypeNameKey_t2985541961_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TypeNameKey_t2985541961_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TypeNameKey_t2985541961_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TypeNameKey_t2985541961_0_0_0;
extern "C" void ResolverContractKey_t3292851287_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResolverContractKey_t3292851287_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResolverContractKey_t3292851287_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResolverContractKey_t3292851287_0_0_0;
extern "C" void TypeConvertKey_t285306760_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TypeConvertKey_t285306760_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TypeConvertKey_t285306760_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TypeConvertKey_t285306760_0_0_0;
extern "C" void DateTimeParser_t3754458704_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DateTimeParser_t3754458704_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DateTimeParser_t3754458704_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DateTimeParser_t3754458704_0_0_0;
extern "C" void StringBuffer_t2235727887_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StringBuffer_t2235727887_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StringBuffer_t2235727887_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType StringBuffer_t2235727887_0_0_0;
extern "C" void StringReference_t2912309144_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StringReference_t2912309144_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StringReference_t2912309144_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType StringReference_t2912309144_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnTrigger_t4184125570();
extern const RuntimeType OnTrigger_t4184125570_0_0_0;
extern "C" void Example_t2853439971_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Example_t2853439971_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Example_t2853439971_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Example_t2853439971_0_0_0;
extern "C" void PolyAuthConfig_t4147939475_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PolyAuthConfig_t4147939475_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PolyAuthConfig_t4147939475_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PolyAuthConfig_t4147939475_0_0_0;
extern "C" void PolyCacheConfig_t1397553534_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PolyCacheConfig_t1397553534_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PolyCacheConfig_t1397553534_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PolyCacheConfig_t1397553534_0_0_0;
extern "C" void PolyImportOptions_t4213423452_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PolyImportOptions_t4213423452_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PolyImportOptions_t4213423452_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PolyImportOptions_t4213423452_0_0_0;
extern "C" void PolyStatus_t3145373940_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PolyStatus_t3145373940_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PolyStatus_t3145373940_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PolyStatus_t3145373940_0_0_0;
extern "C" void DelegatePInvokeWrapper_GetRawFileDataBytesCallback_t2880187408();
extern const RuntimeType GetRawFileDataBytesCallback_t2880187408_0_0_0;
extern "C" void DelegatePInvokeWrapper_GetRawFileDataTextCallback_t2904487790();
extern const RuntimeType GetRawFileDataTextCallback_t2904487790_0_0_0;
extern "C" void DelegatePInvokeWrapper_CacheReadCallback_t2055645553();
extern const RuntimeType CacheReadCallback_t2055645553_0_0_0;
extern "C" void DelegatePInvokeWrapper_CompletionCallback_t3367319633();
extern const RuntimeType CompletionCallback_t3367319633_0_0_0;
extern "C" void DelegatePInvokeWrapper_CreationCallback_t949890767();
extern const RuntimeType CreationCallback_t949890767_0_0_0;
extern "C" void EditTimeImportOptions_t4030780733_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void EditTimeImportOptions_t4030780733_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void EditTimeImportOptions_t4030780733_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType EditTimeImportOptions_t4030780733_0_0_0;
extern "C" void UnityMaterial_t3949128076_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityMaterial_t3949128076_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityMaterial_t3949128076_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityMaterial_t3949128076_0_0_0;
extern "C" void SaveData_t3667871004_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SaveData_t3667871004_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SaveData_t3667871004_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SaveData_t3667871004_0_0_0;
extern "C" void SurfaceShaderMaterial_t2588431549_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SurfaceShaderMaterial_t2588431549_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SurfaceShaderMaterial_t2588431549_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SurfaceShaderMaterial_t2588431549_0_0_0;
extern "C" void SerializableGuid_t3370039383_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SerializableGuid_t3370039383_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SerializableGuid_t3370039383_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SerializableGuid_t3370039383_0_0_0;
extern "C" void DelegatePInvokeWrapper_StringProcessor_t3163369541();
extern const RuntimeType StringProcessor_t3163369541_0_0_0;
extern "C" void ARAnchor_t362826948_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARAnchor_t362826948_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARAnchor_t362826948_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARAnchor_t362826948_0_0_0;
extern "C" void DelegatePInvokeWrapper_DictionaryVisitorHandler_t414487210();
extern const RuntimeType DictionaryVisitorHandler_t414487210_0_0_0;
extern "C" void ARHitTestResult_t1279023930_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARHitTestResult_t1279023930_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARHitTestResult_t1279023930_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARHitTestResult_t1279023930_0_0_0;
extern "C" void ARKitFaceTrackingConfiguration_t386387352_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARKitFaceTrackingConfiguration_t386387352_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARKitFaceTrackingConfiguration_t386387352_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARKitFaceTrackingConfiguration_t386387352_0_0_0;
extern "C" void ARKitSessionConfiguration_t629136898_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARKitSessionConfiguration_t629136898_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARKitSessionConfiguration_t629136898_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARKitSessionConfiguration_t629136898_0_0_0;
extern "C" void ARKitWorldTrackingSessionConfiguration_t273386347_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARKitWorldTrackingSessionConfiguration_t273386347_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARKitWorldTrackingSessionConfiguration_t273386347_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARKitWorldTrackingSessionConfiguration_t273386347_0_0_0;
extern "C" void ARPlaneAnchor_t2049372221_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARPlaneAnchor_t2049372221_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARPlaneAnchor_t2049372221_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARPlaneAnchor_t2049372221_0_0_0;
extern "C" void ARUserAnchor_t1406831531_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ARUserAnchor_t1406831531_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ARUserAnchor_t1406831531_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ARUserAnchor_t1406831531_0_0_0;
extern "C" void UnityARCamera_t2069150450_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityARCamera_t2069150450_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityARCamera_t2069150450_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityARCamera_t2069150450_0_0_0;
extern "C" void UnityARHitTestResult_t4176230866_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityARHitTestResult_t4176230866_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityARHitTestResult_t4176230866_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityARHitTestResult_t4176230866_0_0_0;
extern "C" void UnityARLightData_t2160616730_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UnityARLightData_t2160616730_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UnityARLightData_t2160616730_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UnityARLightData_t2160616730_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARAnchorAdded_t1608557165();
extern const RuntimeType ARAnchorAdded_t1608557165_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARAnchorRemoved_t4030593004();
extern const RuntimeType ARAnchorRemoved_t4030593004_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARAnchorUpdated_t3113222537();
extern const RuntimeType ARAnchorUpdated_t3113222537_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARSessionCallback_t3772093212();
extern const RuntimeType ARSessionCallback_t3772093212_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARSessionFailed_t2125002991();
extern const RuntimeType ARSessionFailed_t2125002991_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARUserAnchorAdded_t1851120876();
extern const RuntimeType ARUserAnchorAdded_t1851120876_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARUserAnchorRemoved_t23344545();
extern const RuntimeType ARUserAnchorRemoved_t23344545_0_0_0;
extern "C" void DelegatePInvokeWrapper_ARUserAnchorUpdated_t4007601678();
extern const RuntimeType ARUserAnchorUpdated_t4007601678_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARAnchorAdded_t1565083332();
extern const RuntimeType internal_ARAnchorAdded_t1565083332_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARAnchorRemoved_t3371657877();
extern const RuntimeType internal_ARAnchorRemoved_t3371657877_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARAnchorUpdated_t2645242205();
extern const RuntimeType internal_ARAnchorUpdated_t2645242205_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARFaceAnchorAdded_t1021040265();
extern const RuntimeType internal_ARFaceAnchorAdded_t1021040265_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARFaceAnchorRemoved_t2563439402();
extern const RuntimeType internal_ARFaceAnchorRemoved_t2563439402_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARFaceAnchorUpdated_t3423900432();
extern const RuntimeType internal_ARFaceAnchorUpdated_t3423900432_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARFrameUpdate_t3254989823();
extern const RuntimeType internal_ARFrameUpdate_t3254989823_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARSessionTrackingChanged_t1988849735();
extern const RuntimeType internal_ARSessionTrackingChanged_t1988849735_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARUserAnchorAdded_t3285282493();
extern const RuntimeType internal_ARUserAnchorAdded_t3285282493_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARUserAnchorRemoved_t386858594();
extern const RuntimeType internal_ARUserAnchorRemoved_t386858594_0_0_0;
extern "C" void DelegatePInvokeWrapper_internal_ARUserAnchorUpdated_t3964727538();
extern const RuntimeType internal_ARUserAnchorUpdated_t3964727538_0_0_0;
extern "C" void DelegatePInvokeWrapper_HandleAssertCallback_t2932343000();
extern const RuntimeType HandleAssertCallback_t2932343000_0_0_0;
extern "C" void DelegatePInvokeWrapper_CreateThreadDelegate_t2573395908();
extern const RuntimeType CreateThreadDelegate_t2573395908_0_0_0;
extern "C" void DelegatePInvokeWrapper_JoinThreadDelegate_t1262337526();
extern const RuntimeType JoinThreadDelegate_t1262337526_0_0_0;
extern "C" void DelegatePInvokeWrapper_ThreadStartDelegate_t2173555516();
extern const RuntimeType ThreadStartDelegate_t2173555516_0_0_0;
extern "C" void ConfigParams_t592212086_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ConfigParams_t592212086_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ConfigParams_t592212086_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ConfigParams_t592212086_0_0_0;
extern "C" void CollisionConfig_t3653208188_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CollisionConfig_t3653208188_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CollisionConfig_t3653208188_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CollisionConfig_t3653208188_0_0_0;
extern "C" void NativeConfig_t18506486_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void NativeConfig_t18506486_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void NativeConfig_t18506486_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType NativeConfig_t18506486_0_0_0;
extern "C" void DelegatePInvokeWrapper_CameraEventCallback_t1620885257();
extern const RuntimeType CameraEventCallback_t1620885257_0_0_0;
extern "C" void InputFrame_t648463210_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void InputFrame_t648463210_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void InputFrame_t648463210_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType InputFrame_t648463210_0_0_0;
extern "C" void DelegatePInvokeWrapper_SendActionDelegate_t3936809903();
extern const RuntimeType SendActionDelegate_t3936809903_0_0_0;
extern "C" void CameraState_t2128520787_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CameraState_t2128520787_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CameraState_t2128520787_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CameraState_t2128520787_0_0_0;
extern "C" void DelegatePInvokeWrapper_AddMeshCallback_t1037702503();
extern const RuntimeType AddMeshCallback_t1037702503_0_0_0;
extern "C" void DelegatePInvokeWrapper_DeleteMeshCallback_t4229142016();
extern const RuntimeType DeleteMeshCallback_t4229142016_0_0_0;
extern "C" void DelegatePInvokeWrapper_VisibilityCallback_t2021865822();
extern const RuntimeType VisibilityCallback_t2021865822_0_0_0;
extern "C" void KeyboardData_t2354704333_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void KeyboardData_t2354704333_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void KeyboardData_t2354704333_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType KeyboardData_t2354704333_0_0_0;
extern "C" void KeyboardInputEvent_t410871953_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void KeyboardInputEvent_t410871953_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void KeyboardInputEvent_t410871953_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType KeyboardInputEvent_t410871953_0_0_0;
extern "C" void TouchInputEvent_t172715690_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TouchInputEvent_t172715690_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TouchInputEvent_t172715690_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TouchInputEvent_t172715690_0_0_0;
extern "C" void ApplyTextureToMaterialRequest_t638715235_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ApplyTextureToMaterialRequest_t638715235_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ApplyTextureToMaterialRequest_t638715235_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ApplyTextureToMaterialRequest_t638715235_0_0_0;
extern "C" void DelegatePInvokeWrapper_AllocateTextureBufferCallback_t109657048();
extern const RuntimeType AllocateTextureBufferCallback_t109657048_0_0_0;
extern "C" void DelegatePInvokeWrapper_BeginUploadTextureBufferCallback_t3016670851();
extern const RuntimeType BeginUploadTextureBufferCallback_t3016670851_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReleaseTextureCallback_t388032167();
extern const RuntimeType ReleaseTextureCallback_t388032167_0_0_0;
extern "C" void DelegatePInvokeWrapper_AllocateUnpackedMeshCallback_t3754430302();
extern const RuntimeType AllocateUnpackedMeshCallback_t3754430302_0_0_0;
extern "C" void DelegatePInvokeWrapper_UploadUnpackedMeshCallback_t3790783597();
extern const RuntimeType UploadUnpackedMeshCallback_t3790783597_0_0_0;
extern "C" void Building_t1491635829_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Building_t1491635829_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Building_t1491635829_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Building_t1491635829_0_0_0;
extern "C" void DelegatePInvokeWrapper_BuildingReceivedCallback_t1209487313();
extern const RuntimeType BuildingReceivedCallback_t1209487313_0_0_0;
extern "C" void BuildingRequest_t1598655962_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void BuildingRequest_t1598655962_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void BuildingRequest_t1598655962_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType BuildingRequest_t1598655962_0_0_0;
extern "C" void HighlightRequest_t1416441333_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HighlightRequest_t1416441333_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HighlightRequest_t1416441333_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HighlightRequest_t1416441333_0_0_0;
extern "C" void DelegatePInvokeWrapper_NativeBuildingReceivedCallback_t2577383538();
extern const RuntimeType NativeBuildingReceivedCallback_t2577383538_0_0_0;
extern "C" void DelegatePInvokeWrapper_NativeHighlightMeshClearCallback_t813633652();
extern const RuntimeType NativeHighlightMeshClearCallback_t813633652_0_0_0;
extern "C" void DelegatePInvokeWrapper_NativeHighlightMeshUploadCallback_t608941254();
extern const RuntimeType NativeHighlightMeshUploadCallback_t608941254_0_0_0;
extern "C" void DelegatePInvokeWrapper_NativeHighlightReceivedCallback_t2749649652();
extern const RuntimeType NativeHighlightReceivedCallback_t2749649652_0_0_0;
extern Il2CppInteropData g_Il2CppInteropData[236] = 
{
	{ NULL, Context_t1744531130_marshal_pinvoke, Context_t1744531130_marshal_pinvoke_back, Context_t1744531130_marshal_pinvoke_cleanup, NULL, NULL, &Context_t1744531130_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Context */,
	{ NULL, Escape_t3294788190_marshal_pinvoke, Escape_t3294788190_marshal_pinvoke_back, Escape_t3294788190_marshal_pinvoke_cleanup, NULL, NULL, &Escape_t3294788190_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Escape */,
	{ NULL, PreviousInfo_t2148130204_marshal_pinvoke, PreviousInfo_t2148130204_marshal_pinvoke_back, PreviousInfo_t2148130204_marshal_pinvoke_cleanup, NULL, NULL, &PreviousInfo_t2148130204_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/PreviousInfo */,
	{ NULL, UriScheme_t2867806342_marshal_pinvoke, UriScheme_t2867806342_marshal_pinvoke_back, UriScheme_t2867806342_marshal_pinvoke_cleanup, NULL, NULL, &UriScheme_t2867806342_0_0_0 } /* Mono.Security.Uri/UriScheme */,
	{ DelegatePInvokeWrapper_AppDomainInitializer_t682969308, NULL, NULL, NULL, NULL, NULL, &AppDomainInitializer_t682969308_0_0_0 } /* System.AppDomainInitializer */,
	{ DelegatePInvokeWrapper_Swapper_t2822380397, NULL, NULL, NULL, NULL, NULL, &Swapper_t2822380397_0_0_0 } /* System.Array/Swapper */,
	{ NULL, DictionaryEntry_t3123975638_marshal_pinvoke, DictionaryEntry_t3123975638_marshal_pinvoke_back, DictionaryEntry_t3123975638_marshal_pinvoke_cleanup, NULL, NULL, &DictionaryEntry_t3123975638_0_0_0 } /* System.Collections.DictionaryEntry */,
	{ NULL, Slot_t3975888750_marshal_pinvoke, Slot_t3975888750_marshal_pinvoke_back, Slot_t3975888750_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t3975888750_0_0_0 } /* System.Collections.Hashtable/Slot */,
	{ NULL, Slot_t384495010_marshal_pinvoke, Slot_t384495010_marshal_pinvoke_back, Slot_t384495010_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t384495010_0_0_0 } /* System.Collections.SortedList/Slot */,
	{ DelegatePInvokeWrapper_InternalCancelHandler_t872516951, NULL, NULL, NULL, NULL, NULL, &InternalCancelHandler_t872516951_0_0_0 } /* System.Console/InternalCancelHandler */,
	{ NULL, ConsoleKeyInfo_t1802691652_marshal_pinvoke, ConsoleKeyInfo_t1802691652_marshal_pinvoke_back, ConsoleKeyInfo_t1802691652_marshal_pinvoke_cleanup, NULL, NULL, &ConsoleKeyInfo_t1802691652_0_0_0 } /* System.ConsoleKeyInfo */,
	{ NULL, Enum_t4135868527_marshal_pinvoke, Enum_t4135868527_marshal_pinvoke_back, Enum_t4135868527_marshal_pinvoke_cleanup, NULL, NULL, &Enum_t4135868527_0_0_0 } /* System.Enum */,
	{ NULL, InputRecord_t2660212290_marshal_pinvoke, InputRecord_t2660212290_marshal_pinvoke_back, InputRecord_t2660212290_marshal_pinvoke_cleanup, NULL, NULL, &InputRecord_t2660212290_0_0_0 } /* System.InputRecord */,
	{ DelegatePInvokeWrapper_ReadDelegate_t714865915, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t714865915_0_0_0 } /* System.IO.FileStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t4270993571, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t4270993571_0_0_0 } /* System.IO.FileStream/WriteDelegate */,
	{ NULL, MonoIOStat_t592533987_marshal_pinvoke, MonoIOStat_t592533987_marshal_pinvoke_back, MonoIOStat_t592533987_marshal_pinvoke_cleanup, NULL, NULL, &MonoIOStat_t592533987_0_0_0 } /* System.IO.MonoIOStat */,
	{ NULL, MonoEnumInfo_t3694469084_marshal_pinvoke, MonoEnumInfo_t3694469084_marshal_pinvoke_back, MonoEnumInfo_t3694469084_marshal_pinvoke_cleanup, NULL, NULL, &MonoEnumInfo_t3694469084_0_0_0 } /* System.MonoEnumInfo */,
	{ NULL, CustomAttributeNamedArgument_t287865710_marshal_pinvoke, CustomAttributeNamedArgument_t287865710_marshal_pinvoke_back, CustomAttributeNamedArgument_t287865710_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeNamedArgument_t287865710_0_0_0 } /* System.Reflection.CustomAttributeNamedArgument */,
	{ NULL, CustomAttributeTypedArgument_t2723150157_marshal_pinvoke, CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_back, CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeTypedArgument_t2723150157_0_0_0 } /* System.Reflection.CustomAttributeTypedArgument */,
	{ NULL, ILTokenInfo_t2325775114_marshal_pinvoke, ILTokenInfo_t2325775114_marshal_pinvoke_back, ILTokenInfo_t2325775114_marshal_pinvoke_cleanup, NULL, NULL, &ILTokenInfo_t2325775114_0_0_0 } /* System.Reflection.Emit.ILTokenInfo */,
	{ NULL, MonoResource_t4103430009_marshal_pinvoke, MonoResource_t4103430009_marshal_pinvoke_back, MonoResource_t4103430009_marshal_pinvoke_cleanup, NULL, NULL, &MonoResource_t4103430009_0_0_0 } /* System.Reflection.Emit.MonoResource */,
	{ NULL, MonoWin32Resource_t1904229483_marshal_pinvoke, MonoWin32Resource_t1904229483_marshal_pinvoke_back, MonoWin32Resource_t1904229483_marshal_pinvoke_cleanup, NULL, NULL, &MonoWin32Resource_t1904229483_0_0_0 } /* System.Reflection.Emit.MonoWin32Resource */,
	{ NULL, RefEmitPermissionSet_t484390987_marshal_pinvoke, RefEmitPermissionSet_t484390987_marshal_pinvoke_back, RefEmitPermissionSet_t484390987_marshal_pinvoke_cleanup, NULL, NULL, &RefEmitPermissionSet_t484390987_0_0_0 } /* System.Reflection.Emit.RefEmitPermissionSet */,
	{ NULL, MonoEventInfo_t346866618_marshal_pinvoke, MonoEventInfo_t346866618_marshal_pinvoke_back, MonoEventInfo_t346866618_marshal_pinvoke_cleanup, NULL, NULL, &MonoEventInfo_t346866618_0_0_0 } /* System.Reflection.MonoEventInfo */,
	{ NULL, MonoMethodInfo_t1248819020_marshal_pinvoke, MonoMethodInfo_t1248819020_marshal_pinvoke_back, MonoMethodInfo_t1248819020_marshal_pinvoke_cleanup, NULL, NULL, &MonoMethodInfo_t1248819020_0_0_0 } /* System.Reflection.MonoMethodInfo */,
	{ NULL, MonoPropertyInfo_t3087356066_marshal_pinvoke, MonoPropertyInfo_t3087356066_marshal_pinvoke_back, MonoPropertyInfo_t3087356066_marshal_pinvoke_cleanup, NULL, NULL, &MonoPropertyInfo_t3087356066_0_0_0 } /* System.Reflection.MonoPropertyInfo */,
	{ NULL, ParameterModifier_t1461694466_marshal_pinvoke, ParameterModifier_t1461694466_marshal_pinvoke_back, ParameterModifier_t1461694466_marshal_pinvoke_cleanup, NULL, NULL, &ParameterModifier_t1461694466_0_0_0 } /* System.Reflection.ParameterModifier */,
	{ NULL, ResourceCacheItem_t51292791_marshal_pinvoke, ResourceCacheItem_t51292791_marshal_pinvoke_back, ResourceCacheItem_t51292791_marshal_pinvoke_cleanup, NULL, NULL, &ResourceCacheItem_t51292791_0_0_0 } /* System.Resources.ResourceReader/ResourceCacheItem */,
	{ NULL, ResourceInfo_t2872965302_marshal_pinvoke, ResourceInfo_t2872965302_marshal_pinvoke_back, ResourceInfo_t2872965302_marshal_pinvoke_cleanup, NULL, NULL, &ResourceInfo_t2872965302_0_0_0 } /* System.Resources.ResourceReader/ResourceInfo */,
	{ DelegatePInvokeWrapper_CrossContextDelegate_t387175271, NULL, NULL, NULL, NULL, NULL, &CrossContextDelegate_t387175271_0_0_0 } /* System.Runtime.Remoting.Contexts.CrossContextDelegate */,
	{ DelegatePInvokeWrapper_CallbackHandler_t3280319253, NULL, NULL, NULL, NULL, NULL, &CallbackHandler_t3280319253_0_0_0 } /* System.Runtime.Serialization.SerializationCallbacks/CallbackHandler */,
	{ NULL, SerializationEntry_t648286436_marshal_pinvoke, SerializationEntry_t648286436_marshal_pinvoke_back, SerializationEntry_t648286436_marshal_pinvoke_cleanup, NULL, NULL, &SerializationEntry_t648286436_0_0_0 } /* System.Runtime.Serialization.SerializationEntry */,
	{ NULL, StreamingContext_t3711869237_marshal_pinvoke, StreamingContext_t3711869237_marshal_pinvoke_back, StreamingContext_t3711869237_marshal_pinvoke_cleanup, NULL, NULL, &StreamingContext_t3711869237_0_0_0 } /* System.Runtime.Serialization.StreamingContext */,
	{ NULL, DSAParameters_t1885824122_marshal_pinvoke, DSAParameters_t1885824122_marshal_pinvoke_back, DSAParameters_t1885824122_marshal_pinvoke_cleanup, NULL, NULL, &DSAParameters_t1885824122_0_0_0 } /* System.Security.Cryptography.DSAParameters */,
	{ NULL, RSAParameters_t1728406613_marshal_pinvoke, RSAParameters_t1728406613_marshal_pinvoke_back, RSAParameters_t1728406613_marshal_pinvoke_cleanup, NULL, NULL, &RSAParameters_t1728406613_0_0_0 } /* System.Security.Cryptography.RSAParameters */,
	{ NULL, SNIP_t4156255223_marshal_pinvoke, SNIP_t4156255223_marshal_pinvoke_back, SNIP_t4156255223_marshal_pinvoke_cleanup, NULL, NULL, &SNIP_t4156255223_0_0_0 } /* System.Security.Permissions.StrongNameIdentityPermission/SNIP */,
	{ NULL, SecurityFrame_t1422462475_marshal_pinvoke, SecurityFrame_t1422462475_marshal_pinvoke_back, SecurityFrame_t1422462475_marshal_pinvoke_cleanup, NULL, NULL, &SecurityFrame_t1422462475_0_0_0 } /* System.Security.SecurityFrame */,
	{ DelegatePInvokeWrapper_ThreadStart_t1006689297, NULL, NULL, NULL, NULL, NULL, &ThreadStart_t1006689297_0_0_0 } /* System.Threading.ThreadStart */,
	{ NULL, ValueType_t3640485471_marshal_pinvoke, ValueType_t3640485471_marshal_pinvoke_back, ValueType_t3640485471_marshal_pinvoke_cleanup, NULL, NULL, &ValueType_t3640485471_0_0_0 } /* System.ValueType */,
	{ DelegatePInvokeWrapper_AsyncReadHandler_t1188682440, NULL, NULL, NULL, NULL, NULL, &AsyncReadHandler_t1188682440_0_0_0 } /* System.Diagnostics.Process/AsyncReadHandler */,
	{ NULL, ProcessAsyncReader_t337580163_marshal_pinvoke, ProcessAsyncReader_t337580163_marshal_pinvoke_back, ProcessAsyncReader_t337580163_marshal_pinvoke_cleanup, NULL, NULL, &ProcessAsyncReader_t337580163_0_0_0 } /* System.Diagnostics.Process/ProcessAsyncReader */,
	{ NULL, ProcInfo_t2917059746_marshal_pinvoke, ProcInfo_t2917059746_marshal_pinvoke_back, ProcInfo_t2917059746_marshal_pinvoke_cleanup, NULL, NULL, &ProcInfo_t2917059746_0_0_0 } /* System.Diagnostics.Process/ProcInfo */,
	{ DelegatePInvokeWrapper_ReadMethod_t893206259, NULL, NULL, NULL, NULL, NULL, &ReadMethod_t893206259_0_0_0 } /* System.IO.Compression.DeflateStream/ReadMethod */,
	{ DelegatePInvokeWrapper_UnmanagedReadOrWrite_t876388624, NULL, NULL, NULL, NULL, NULL, &UnmanagedReadOrWrite_t876388624_0_0_0 } /* System.IO.Compression.DeflateStream/UnmanagedReadOrWrite */,
	{ DelegatePInvokeWrapper_WriteMethod_t2538911768, NULL, NULL, NULL, NULL, NULL, &WriteMethod_t2538911768_0_0_0 } /* System.IO.Compression.DeflateStream/WriteMethod */,
	{ DelegatePInvokeWrapper_ReadDelegate_t2469437439, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t2469437439_0_0_0 } /* System.IO.MonoSyncFileStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t1613340939, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t1613340939_0_0_0 } /* System.IO.MonoSyncFileStream/WriteDelegate */,
	{ DelegatePInvokeWrapper_ReadDelegate_t4266946825, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t4266946825_0_0_0 } /* System.Net.FtpDataStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t2016697242, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t2016697242_0_0_0 } /* System.Net.FtpDataStream/WriteDelegate */,
	{ DelegatePInvokeWrapper_SocketAsyncCall_t1521370843, NULL, NULL, NULL, NULL, NULL, &SocketAsyncCall_t1521370843_0_0_0 } /* System.Net.Sockets.Socket/SocketAsyncCall */,
	{ NULL, SocketAsyncResult_t2080034863_marshal_pinvoke, SocketAsyncResult_t2080034863_marshal_pinvoke_back, SocketAsyncResult_t2080034863_marshal_pinvoke_cleanup, NULL, NULL, &SocketAsyncResult_t2080034863_0_0_0 } /* System.Net.Sockets.Socket/SocketAsyncResult */,
	{ NULL, X509ChainStatus_t133602714_marshal_pinvoke, X509ChainStatus_t133602714_marshal_pinvoke_back, X509ChainStatus_t133602714_marshal_pinvoke_cleanup, NULL, NULL, &X509ChainStatus_t133602714_0_0_0 } /* System.Security.Cryptography.X509Certificates.X509ChainStatus */,
	{ NULL, IntStack_t2189327687_marshal_pinvoke, IntStack_t2189327687_marshal_pinvoke_back, IntStack_t2189327687_marshal_pinvoke_cleanup, NULL, NULL, &IntStack_t2189327687_0_0_0 } /* System.Text.RegularExpressions.Interpreter/IntStack */,
	{ NULL, Interval_t1802865632_marshal_pinvoke, Interval_t1802865632_marshal_pinvoke_back, Interval_t1802865632_marshal_pinvoke_cleanup, NULL, NULL, &Interval_t1802865632_0_0_0 } /* System.Text.RegularExpressions.Interval */,
	{ DelegatePInvokeWrapper_CostDelegate_t1722821004, NULL, NULL, NULL, NULL, NULL, &CostDelegate_t1722821004_0_0_0 } /* System.Text.RegularExpressions.IntervalCollection/CostDelegate */,
	{ NULL, IntStack_t561212167_marshal_pinvoke, IntStack_t561212167_marshal_pinvoke_back, IntStack_t561212167_marshal_pinvoke_cleanup, NULL, NULL, &IntStack_t561212167_0_0_0 } /* System.Text.RegularExpressions.RxInterpreter/IntStack */,
	{ NULL, UriScheme_t722425697_marshal_pinvoke, UriScheme_t722425697_marshal_pinvoke_back, UriScheme_t722425697_marshal_pinvoke_cleanup, NULL, NULL, &UriScheme_t722425697_0_0_0 } /* System.Uri/UriScheme */,
	{ NULL, TagName_t2891256255_marshal_pinvoke, TagName_t2891256255_marshal_pinvoke_back, TagName_t2891256255_marshal_pinvoke_cleanup, NULL, NULL, &TagName_t2891256255_0_0_0 } /* Mono.Xml2.XmlTextReader/TagName */,
	{ NULL, QNameValueType_t615604793_marshal_pinvoke, QNameValueType_t615604793_marshal_pinvoke_back, QNameValueType_t615604793_marshal_pinvoke_cleanup, NULL, NULL, &QNameValueType_t615604793_0_0_0 } /* System.Xml.Schema.QNameValueType */,
	{ NULL, StringArrayValueType_t3147326440_marshal_pinvoke, StringArrayValueType_t3147326440_marshal_pinvoke_back, StringArrayValueType_t3147326440_marshal_pinvoke_cleanup, NULL, NULL, &StringArrayValueType_t3147326440_0_0_0 } /* System.Xml.Schema.StringArrayValueType */,
	{ NULL, StringValueType_t261329552_marshal_pinvoke, StringValueType_t261329552_marshal_pinvoke_back, StringValueType_t261329552_marshal_pinvoke_cleanup, NULL, NULL, &StringValueType_t261329552_0_0_0 } /* System.Xml.Schema.StringValueType */,
	{ NULL, UriValueType_t2866347695_marshal_pinvoke, UriValueType_t2866347695_marshal_pinvoke_back, UriValueType_t2866347695_marshal_pinvoke_cleanup, NULL, NULL, &UriValueType_t2866347695_0_0_0 } /* System.Xml.Schema.UriValueType */,
	{ NULL, NsDecl_t3938094415_marshal_pinvoke, NsDecl_t3938094415_marshal_pinvoke_back, NsDecl_t3938094415_marshal_pinvoke_cleanup, NULL, NULL, &NsDecl_t3938094415_0_0_0 } /* System.Xml.XmlNamespaceManager/NsDecl */,
	{ NULL, NsScope_t3958624705_marshal_pinvoke, NsScope_t3958624705_marshal_pinvoke_back, NsScope_t3958624705_marshal_pinvoke_cleanup, NULL, NULL, &NsScope_t3958624705_0_0_0 } /* System.Xml.XmlNamespaceManager/NsScope */,
	{ DelegatePInvokeWrapper_CharGetter_t1703763694, NULL, NULL, NULL, NULL, NULL, &CharGetter_t1703763694_0_0_0 } /* System.Xml.XmlReaderBinarySupport/CharGetter */,
	{ DelegatePInvokeWrapper_Action_t1264377477, NULL, NULL, NULL, NULL, NULL, &Action_t1264377477_0_0_0 } /* System.Action */,
	{ NULL, TimeType_t2507513283_marshal_pinvoke, TimeType_t2507513283_marshal_pinvoke_back, TimeType_t2507513283_marshal_pinvoke_cleanup, NULL, NULL, &TimeType_t2507513283_0_0_0 } /* System.TimeZoneInfo/TimeType */,
	{ NULL, TransitionTime_t449921781_marshal_pinvoke, TransitionTime_t449921781_marshal_pinvoke_back, TransitionTime_t449921781_marshal_pinvoke_cleanup, NULL, NULL, &TransitionTime_t449921781_0_0_0 } /* System.TimeZoneInfo/TransitionTime */,
	{ NULL, AnimationCurve_t3046754366_marshal_pinvoke, AnimationCurve_t3046754366_marshal_pinvoke_back, AnimationCurve_t3046754366_marshal_pinvoke_cleanup, NULL, NULL, &AnimationCurve_t3046754366_0_0_0 } /* UnityEngine.AnimationCurve */,
	{ DelegatePInvokeWrapper_LogCallback_t3588208630, NULL, NULL, NULL, NULL, NULL, &LogCallback_t3588208630_0_0_0 } /* UnityEngine.Application/LogCallback */,
	{ DelegatePInvokeWrapper_LowMemoryCallback_t4104246196, NULL, NULL, NULL, NULL, NULL, &LowMemoryCallback_t4104246196_0_0_0 } /* UnityEngine.Application/LowMemoryCallback */,
	{ NULL, AssetBundleCreateRequest_t3119663542_marshal_pinvoke, AssetBundleCreateRequest_t3119663542_marshal_pinvoke_back, AssetBundleCreateRequest_t3119663542_marshal_pinvoke_cleanup, NULL, NULL, &AssetBundleCreateRequest_t3119663542_0_0_0 } /* UnityEngine.AssetBundleCreateRequest */,
	{ NULL, AssetBundleRequest_t699759206_marshal_pinvoke, AssetBundleRequest_t699759206_marshal_pinvoke_back, AssetBundleRequest_t699759206_marshal_pinvoke_cleanup, NULL, NULL, &AssetBundleRequest_t699759206_0_0_0 } /* UnityEngine.AssetBundleRequest */,
	{ NULL, AsyncOperation_t1445031843_marshal_pinvoke, AsyncOperation_t1445031843_marshal_pinvoke_back, AsyncOperation_t1445031843_marshal_pinvoke_cleanup, NULL, NULL, &AsyncOperation_t1445031843_0_0_0 } /* UnityEngine.AsyncOperation */,
	{ NULL, OrderBlock_t1585977831_marshal_pinvoke, OrderBlock_t1585977831_marshal_pinvoke_back, OrderBlock_t1585977831_marshal_pinvoke_cleanup, NULL, NULL, &OrderBlock_t1585977831_0_0_0 } /* UnityEngine.BeforeRenderHelper/OrderBlock */,
	{ NULL, Coroutine_t3829159415_marshal_pinvoke, Coroutine_t3829159415_marshal_pinvoke_back, Coroutine_t3829159415_marshal_pinvoke_cleanup, NULL, NULL, &Coroutine_t3829159415_0_0_0 } /* UnityEngine.Coroutine */,
	{ DelegatePInvokeWrapper_CSSMeasureFunc_t1554030124, NULL, NULL, NULL, NULL, NULL, &CSSMeasureFunc_t1554030124_0_0_0 } /* UnityEngine.CSSLayout.CSSMeasureFunc */,
	{ NULL, CullingGroup_t2096318768_marshal_pinvoke, CullingGroup_t2096318768_marshal_pinvoke_back, CullingGroup_t2096318768_marshal_pinvoke_cleanup, NULL, NULL, &CullingGroup_t2096318768_0_0_0 } /* UnityEngine.CullingGroup */,
	{ DelegatePInvokeWrapper_StateChanged_t2136737110, NULL, NULL, NULL, NULL, NULL, &StateChanged_t2136737110_0_0_0 } /* UnityEngine.CullingGroup/StateChanged */,
	{ DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t51287044, NULL, NULL, NULL, NULL, NULL, &DisplaysUpdatedDelegate_t51287044_0_0_0 } /* UnityEngine.Display/DisplaysUpdatedDelegate */,
	{ DelegatePInvokeWrapper_UnityAction_t3245792599, NULL, NULL, NULL, NULL, NULL, &UnityAction_t3245792599_0_0_0 } /* UnityEngine.Events.UnityAction */,
	{ NULL, FailedToLoadScriptObject_t547604379_marshal_pinvoke, FailedToLoadScriptObject_t547604379_marshal_pinvoke_back, FailedToLoadScriptObject_t547604379_marshal_pinvoke_cleanup, NULL, NULL, &FailedToLoadScriptObject_t547604379_0_0_0 } /* UnityEngine.FailedToLoadScriptObject */,
	{ NULL, Gradient_t3067099924_marshal_pinvoke, Gradient_t3067099924_marshal_pinvoke_back, Gradient_t3067099924_marshal_pinvoke_cleanup, NULL, NULL, &Gradient_t3067099924_0_0_0 } /* UnityEngine.Gradient */,
	{ NULL, Object_t631007953_marshal_pinvoke, Object_t631007953_marshal_pinvoke_back, Object_t631007953_marshal_pinvoke_cleanup, NULL, NULL, &Object_t631007953_0_0_0 } /* UnityEngine.Object */,
	{ NULL, PlayableBinding_t354260709_marshal_pinvoke, PlayableBinding_t354260709_marshal_pinvoke_back, PlayableBinding_t354260709_marshal_pinvoke_cleanup, NULL, NULL, &PlayableBinding_t354260709_0_0_0 } /* UnityEngine.Playables.PlayableBinding */,
	{ NULL, RectOffset_t1369453676_marshal_pinvoke, RectOffset_t1369453676_marshal_pinvoke_back, RectOffset_t1369453676_marshal_pinvoke_cleanup, NULL, NULL, &RectOffset_t1369453676_0_0_0 } /* UnityEngine.RectOffset */,
	{ NULL, ResourceRequest_t3109103591_marshal_pinvoke, ResourceRequest_t3109103591_marshal_pinvoke_back, ResourceRequest_t3109103591_marshal_pinvoke_cleanup, NULL, NULL, &ResourceRequest_t3109103591_0_0_0 } /* UnityEngine.ResourceRequest */,
	{ NULL, ScriptableObject_t2528358522_marshal_pinvoke, ScriptableObject_t2528358522_marshal_pinvoke_back, ScriptableObject_t2528358522_marshal_pinvoke_cleanup, NULL, NULL, &ScriptableObject_t2528358522_0_0_0 } /* UnityEngine.ScriptableObject */,
	{ NULL, HitInfo_t3229609740_marshal_pinvoke, HitInfo_t3229609740_marshal_pinvoke_back, HitInfo_t3229609740_marshal_pinvoke_cleanup, NULL, NULL, &HitInfo_t3229609740_0_0_0 } /* UnityEngine.SendMouseEvents/HitInfo */,
	{ NULL, TrackedReference_t1199777556_marshal_pinvoke, TrackedReference_t1199777556_marshal_pinvoke_back, TrackedReference_t1199777556_marshal_pinvoke_cleanup, NULL, NULL, &TrackedReference_t1199777556_0_0_0 } /* UnityEngine.TrackedReference */,
	{ DelegatePInvokeWrapper_RequestAtlasCallback_t3100554279, NULL, NULL, NULL, NULL, NULL, &RequestAtlasCallback_t3100554279_0_0_0 } /* UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback */,
	{ NULL, WorkRequest_t1354518612_marshal_pinvoke, WorkRequest_t1354518612_marshal_pinvoke_back, WorkRequest_t1354518612_marshal_pinvoke_cleanup, NULL, NULL, &WorkRequest_t1354518612_0_0_0 } /* UnityEngine.UnitySynchronizationContext/WorkRequest */,
	{ NULL, WaitForSeconds_t1699091251_marshal_pinvoke, WaitForSeconds_t1699091251_marshal_pinvoke_back, WaitForSeconds_t1699091251_marshal_pinvoke_cleanup, NULL, NULL, &WaitForSeconds_t1699091251_0_0_0 } /* UnityEngine.WaitForSeconds */,
	{ NULL, YieldInstruction_t403091072_marshal_pinvoke, YieldInstruction_t403091072_marshal_pinvoke_back, YieldInstruction_t403091072_marshal_pinvoke_cleanup, NULL, NULL, &YieldInstruction_t403091072_0_0_0 } /* UnityEngine.YieldInstruction */,
	{ DelegatePInvokeWrapper_PCMReaderCallback_t1677636661, NULL, NULL, NULL, NULL, NULL, &PCMReaderCallback_t1677636661_0_0_0 } /* UnityEngine.AudioClip/PCMReaderCallback */,
	{ DelegatePInvokeWrapper_PCMSetPositionCallback_t1059417452, NULL, NULL, NULL, NULL, NULL, &PCMSetPositionCallback_t1059417452_0_0_0 } /* UnityEngine.AudioClip/PCMSetPositionCallback */,
	{ DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t2089929874, NULL, NULL, NULL, NULL, NULL, &AudioConfigurationChangeHandler_t2089929874_0_0_0 } /* UnityEngine.AudioSettings/AudioConfigurationChangeHandler */,
	{ DelegatePInvokeWrapper_FontTextureRebuildCallback_t2467502454, NULL, NULL, NULL, NULL, NULL, &FontTextureRebuildCallback_t2467502454_0_0_0 } /* UnityEngine.Font/FontTextureRebuildCallback */,
	{ NULL, TextGenerationSettings_t1351628751_marshal_pinvoke, TextGenerationSettings_t1351628751_marshal_pinvoke_back, TextGenerationSettings_t1351628751_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerationSettings_t1351628751_0_0_0 } /* UnityEngine.TextGenerationSettings */,
	{ NULL, TextGenerator_t3211863866_marshal_pinvoke, TextGenerator_t3211863866_marshal_pinvoke_back, TextGenerator_t3211863866_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerator_t3211863866_0_0_0 } /* UnityEngine.TextGenerator */,
	{ NULL, DownloadHandler_t2937767557_marshal_pinvoke, DownloadHandler_t2937767557_marshal_pinvoke_back, DownloadHandler_t2937767557_marshal_pinvoke_cleanup, NULL, NULL, &DownloadHandler_t2937767557_0_0_0 } /* UnityEngine.Networking.DownloadHandler */,
	{ NULL, DownloadHandlerBuffer_t2928496527_marshal_pinvoke, DownloadHandlerBuffer_t2928496527_marshal_pinvoke_back, DownloadHandlerBuffer_t2928496527_marshal_pinvoke_cleanup, NULL, NULL, &DownloadHandlerBuffer_t2928496527_0_0_0 } /* UnityEngine.Networking.DownloadHandlerBuffer */,
	{ NULL, UnityWebRequest_t463507806_marshal_pinvoke, UnityWebRequest_t463507806_marshal_pinvoke_back, UnityWebRequest_t463507806_marshal_pinvoke_cleanup, NULL, NULL, &UnityWebRequest_t463507806_0_0_0 } /* UnityEngine.Networking.UnityWebRequest */,
	{ NULL, UnityWebRequestAsyncOperation_t3852015985_marshal_pinvoke, UnityWebRequestAsyncOperation_t3852015985_marshal_pinvoke_back, UnityWebRequestAsyncOperation_t3852015985_marshal_pinvoke_cleanup, NULL, NULL, &UnityWebRequestAsyncOperation_t3852015985_0_0_0 } /* UnityEngine.Networking.UnityWebRequestAsyncOperation */,
	{ NULL, UploadHandler_t2993558019_marshal_pinvoke, UploadHandler_t2993558019_marshal_pinvoke_back, UploadHandler_t2993558019_marshal_pinvoke_cleanup, NULL, NULL, &UploadHandler_t2993558019_0_0_0 } /* UnityEngine.Networking.UploadHandler */,
	{ NULL, UploadHandlerRaw_t25761545_marshal_pinvoke, UploadHandlerRaw_t25761545_marshal_pinvoke_back, UploadHandlerRaw_t25761545_marshal_pinvoke_cleanup, NULL, NULL, &UploadHandlerRaw_t25761545_0_0_0 } /* UnityEngine.Networking.UploadHandlerRaw */,
	{ NULL, AnimationEvent_t1536042487_marshal_pinvoke, AnimationEvent_t1536042487_marshal_pinvoke_back, AnimationEvent_t1536042487_marshal_pinvoke_cleanup, NULL, NULL, &AnimationEvent_t1536042487_0_0_0 } /* UnityEngine.AnimationEvent */,
	{ NULL, AnimatorTransitionInfo_t2534804151_marshal_pinvoke, AnimatorTransitionInfo_t2534804151_marshal_pinvoke_back, AnimatorTransitionInfo_t2534804151_marshal_pinvoke_cleanup, NULL, NULL, &AnimatorTransitionInfo_t2534804151_0_0_0 } /* UnityEngine.AnimatorTransitionInfo */,
	{ NULL, HumanBone_t2465339518_marshal_pinvoke, HumanBone_t2465339518_marshal_pinvoke_back, HumanBone_t2465339518_marshal_pinvoke_cleanup, NULL, NULL, &HumanBone_t2465339518_0_0_0 } /* UnityEngine.HumanBone */,
	{ NULL, SkeletonBone_t4134054672_marshal_pinvoke, SkeletonBone_t4134054672_marshal_pinvoke_back, SkeletonBone_t4134054672_marshal_pinvoke_cleanup, NULL, NULL, &SkeletonBone_t4134054672_0_0_0 } /* UnityEngine.SkeletonBone */,
	{ NULL, GcAchievementData_t675222246_marshal_pinvoke, GcAchievementData_t675222246_marshal_pinvoke_back, GcAchievementData_t675222246_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementData_t675222246_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementData */,
	{ NULL, GcAchievementDescriptionData_t643925653_marshal_pinvoke, GcAchievementDescriptionData_t643925653_marshal_pinvoke_back, GcAchievementDescriptionData_t643925653_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementDescriptionData_t643925653_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData */,
	{ NULL, GcLeaderboard_t4132273028_marshal_pinvoke, GcLeaderboard_t4132273028_marshal_pinvoke_back, GcLeaderboard_t4132273028_marshal_pinvoke_cleanup, NULL, NULL, &GcLeaderboard_t4132273028_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard */,
	{ NULL, GcScoreData_t2125309831_marshal_pinvoke, GcScoreData_t2125309831_marshal_pinvoke_back, GcScoreData_t2125309831_marshal_pinvoke_cleanup, NULL, NULL, &GcScoreData_t2125309831_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcScoreData */,
	{ NULL, GcUserProfileData_t2719720026_marshal_pinvoke, GcUserProfileData_t2719720026_marshal_pinvoke_back, GcUserProfileData_t2719720026_marshal_pinvoke_cleanup, NULL, NULL, &GcUserProfileData_t2719720026_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData */,
	{ NULL, Event_t2956885303_marshal_pinvoke, Event_t2956885303_marshal_pinvoke_back, Event_t2956885303_marshal_pinvoke_cleanup, NULL, NULL, &Event_t2956885303_0_0_0 } /* UnityEngine.Event */,
	{ DelegatePInvokeWrapper_WindowFunction_t3146511083, NULL, NULL, NULL, NULL, NULL, &WindowFunction_t3146511083_0_0_0 } /* UnityEngine.GUI/WindowFunction */,
	{ NULL, GUIContent_t3050628031_marshal_pinvoke, GUIContent_t3050628031_marshal_pinvoke_back, GUIContent_t3050628031_marshal_pinvoke_cleanup, NULL, NULL, &GUIContent_t3050628031_0_0_0 } /* UnityEngine.GUIContent */,
	{ DelegatePInvokeWrapper_SkinChangedDelegate_t1143955295, NULL, NULL, NULL, NULL, NULL, &SkinChangedDelegate_t1143955295_0_0_0 } /* UnityEngine.GUISkin/SkinChangedDelegate */,
	{ NULL, GUIStyle_t3956901511_marshal_pinvoke, GUIStyle_t3956901511_marshal_pinvoke_back, GUIStyle_t3956901511_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyle_t3956901511_0_0_0 } /* UnityEngine.GUIStyle */,
	{ NULL, GUIStyleState_t1397964415_marshal_pinvoke, GUIStyleState_t1397964415_marshal_pinvoke_back, GUIStyleState_t1397964415_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyleState_t1397964415_0_0_0 } /* UnityEngine.GUIStyleState */,
	{ NULL, Collision2D_t2842956331_marshal_pinvoke, Collision2D_t2842956331_marshal_pinvoke_back, Collision2D_t2842956331_marshal_pinvoke_cleanup, NULL, NULL, &Collision2D_t2842956331_0_0_0 } /* UnityEngine.Collision2D */,
	{ NULL, ContactFilter2D_t3805203441_marshal_pinvoke, ContactFilter2D_t3805203441_marshal_pinvoke_back, ContactFilter2D_t3805203441_marshal_pinvoke_cleanup, NULL, NULL, &ContactFilter2D_t3805203441_0_0_0 } /* UnityEngine.ContactFilter2D */,
	{ NULL, RaycastHit2D_t2279581989_marshal_pinvoke, RaycastHit2D_t2279581989_marshal_pinvoke_back, RaycastHit2D_t2279581989_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit2D_t2279581989_0_0_0 } /* UnityEngine.RaycastHit2D */,
	{ NULL, Collision_t4262080450_marshal_pinvoke, Collision_t4262080450_marshal_pinvoke_back, Collision_t4262080450_marshal_pinvoke_cleanup, NULL, NULL, &Collision_t4262080450_0_0_0 } /* UnityEngine.Collision */,
	{ NULL, ControllerColliderHit_t240592346_marshal_pinvoke, ControllerColliderHit_t240592346_marshal_pinvoke_back, ControllerColliderHit_t240592346_marshal_pinvoke_cleanup, NULL, NULL, &ControllerColliderHit_t240592346_0_0_0 } /* UnityEngine.ControllerColliderHit */,
	{ NULL, RaycastHit_t1056001966_marshal_pinvoke, RaycastHit_t1056001966_marshal_pinvoke_back, RaycastHit_t1056001966_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit_t1056001966_0_0_0 } /* UnityEngine.RaycastHit */,
	{ DelegatePInvokeWrapper_WillRenderCanvases_t3309123499, NULL, NULL, NULL, NULL, NULL, &WillRenderCanvases_t3309123499_0_0_0 } /* UnityEngine.Canvas/WillRenderCanvases */,
	{ NULL, CustomEventData_t317522481_marshal_pinvoke, CustomEventData_t317522481_marshal_pinvoke_back, CustomEventData_t317522481_marshal_pinvoke_cleanup, NULL, NULL, &CustomEventData_t317522481_0_0_0 } /* UnityEngine.Analytics.CustomEventData */,
	{ NULL, UnityAnalyticsHandler_t3011359618_marshal_pinvoke, UnityAnalyticsHandler_t3011359618_marshal_pinvoke_back, UnityAnalyticsHandler_t3011359618_marshal_pinvoke_cleanup, NULL, NULL, &UnityAnalyticsHandler_t3011359618_0_0_0 } /* UnityEngine.Analytics.UnityAnalyticsHandler */,
	{ DelegatePInvokeWrapper_SessionStateChanged_t3163629820, NULL, NULL, NULL, NULL, NULL, &SessionStateChanged_t3163629820_0_0_0 } /* UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged */,
	{ DelegatePInvokeWrapper_UpdatedEventHandler_t1027848393, NULL, NULL, NULL, NULL, NULL, &UpdatedEventHandler_t1027848393_0_0_0 } /* UnityEngine.RemoteSettings/UpdatedEventHandler */,
	{ NULL, DownloadHandlerTexture_t2149801800_marshal_pinvoke, DownloadHandlerTexture_t2149801800_marshal_pinvoke_back, DownloadHandlerTexture_t2149801800_marshal_pinvoke_cleanup, NULL, NULL, &DownloadHandlerTexture_t2149801800_0_0_0 } /* UnityEngine.Networking.DownloadHandlerTexture */,
	{ DelegatePInvokeWrapper_EaseFunction_t3531141372, NULL, NULL, NULL, NULL, NULL, &EaseFunction_t3531141372_0_0_0 } /* DG.Tweening.EaseFunction */,
	{ NULL, ColorOptions_t1487297155_marshal_pinvoke, ColorOptions_t1487297155_marshal_pinvoke_back, ColorOptions_t1487297155_marshal_pinvoke_cleanup, NULL, NULL, &ColorOptions_t1487297155_0_0_0 } /* DG.Tweening.Plugins.Options.ColorOptions */,
	{ NULL, FloatOptions_t1203667100_marshal_pinvoke, FloatOptions_t1203667100_marshal_pinvoke_back, FloatOptions_t1203667100_marshal_pinvoke_cleanup, NULL, NULL, &FloatOptions_t1203667100_0_0_0 } /* DG.Tweening.Plugins.Options.FloatOptions */,
	{ NULL, PathOptions_t2074623791_marshal_pinvoke, PathOptions_t2074623791_marshal_pinvoke_back, PathOptions_t2074623791_marshal_pinvoke_cleanup, NULL, NULL, &PathOptions_t2074623791_0_0_0 } /* DG.Tweening.Plugins.Options.PathOptions */,
	{ NULL, RectOptions_t1018205596_marshal_pinvoke, RectOptions_t1018205596_marshal_pinvoke_back, RectOptions_t1018205596_marshal_pinvoke_cleanup, NULL, NULL, &RectOptions_t1018205596_0_0_0 } /* DG.Tweening.Plugins.Options.RectOptions */,
	{ NULL, StringOptions_t3992490940_marshal_pinvoke, StringOptions_t3992490940_marshal_pinvoke_back, StringOptions_t3992490940_marshal_pinvoke_cleanup, NULL, NULL, &StringOptions_t3992490940_0_0_0 } /* DG.Tweening.Plugins.Options.StringOptions */,
	{ NULL, UintOptions_t1006674242_marshal_pinvoke, UintOptions_t1006674242_marshal_pinvoke_back, UintOptions_t1006674242_marshal_pinvoke_cleanup, NULL, NULL, &UintOptions_t1006674242_0_0_0 } /* DG.Tweening.Plugins.Options.UintOptions */,
	{ NULL, Vector3ArrayOptions_t534739431_marshal_pinvoke, Vector3ArrayOptions_t534739431_marshal_pinvoke_back, Vector3ArrayOptions_t534739431_marshal_pinvoke_cleanup, NULL, NULL, &Vector3ArrayOptions_t534739431_0_0_0 } /* DG.Tweening.Plugins.Options.Vector3ArrayOptions */,
	{ NULL, VectorOptions_t1354903650_marshal_pinvoke, VectorOptions_t1354903650_marshal_pinvoke_back, VectorOptions_t1354903650_marshal_pinvoke_cleanup, NULL, NULL, &VectorOptions_t1354903650_0_0_0 } /* DG.Tweening.Plugins.Options.VectorOptions */,
	{ DelegatePInvokeWrapper_TweenCallback_t3727756325, NULL, NULL, NULL, NULL, NULL, &TweenCallback_t3727756325_0_0_0 } /* DG.Tweening.TweenCallback */,
	{ NULL, RaycastResult_t3360306849_marshal_pinvoke, RaycastResult_t3360306849_marshal_pinvoke_back, RaycastResult_t3360306849_marshal_pinvoke_cleanup, NULL, NULL, &RaycastResult_t3360306849_0_0_0 } /* UnityEngine.EventSystems.RaycastResult */,
	{ NULL, ColorTween_t809614380_marshal_pinvoke, ColorTween_t809614380_marshal_pinvoke_back, ColorTween_t809614380_marshal_pinvoke_cleanup, NULL, NULL, &ColorTween_t809614380_0_0_0 } /* UnityEngine.UI.CoroutineTween.ColorTween */,
	{ NULL, FloatTween_t1274330004_marshal_pinvoke, FloatTween_t1274330004_marshal_pinvoke_back, FloatTween_t1274330004_marshal_pinvoke_cleanup, NULL, NULL, &FloatTween_t1274330004_0_0_0 } /* UnityEngine.UI.CoroutineTween.FloatTween */,
	{ NULL, Resources_t1597885468_marshal_pinvoke, Resources_t1597885468_marshal_pinvoke_back, Resources_t1597885468_marshal_pinvoke_cleanup, NULL, NULL, &Resources_t1597885468_0_0_0 } /* UnityEngine.UI.DefaultControls/Resources */,
	{ DelegatePInvokeWrapper_OnValidateInput_t2355412304, NULL, NULL, NULL, NULL, NULL, &OnValidateInput_t2355412304_0_0_0 } /* UnityEngine.UI.InputField/OnValidateInput */,
	{ NULL, Navigation_t3049316579_marshal_pinvoke, Navigation_t3049316579_marshal_pinvoke_back, Navigation_t3049316579_marshal_pinvoke_cleanup, NULL, NULL, &Navigation_t3049316579_0_0_0 } /* UnityEngine.UI.Navigation */,
	{ NULL, SpriteState_t1362986479_marshal_pinvoke, SpriteState_t1362986479_marshal_pinvoke_back, SpriteState_t1362986479_marshal_pinvoke_cleanup, NULL, NULL, &SpriteState_t1362986479_0_0_0 } /* UnityEngine.UI.SpriteState */,
	{ NULL, JsonPosition_t2528027714_marshal_pinvoke, JsonPosition_t2528027714_marshal_pinvoke_back, JsonPosition_t2528027714_marshal_pinvoke_cleanup, NULL, NULL, &JsonPosition_t2528027714_0_0_0 } /* Newtonsoft.Json.JsonPosition */,
	{ NULL, TypeNameKey_t2985541961_marshal_pinvoke, TypeNameKey_t2985541961_marshal_pinvoke_back, TypeNameKey_t2985541961_marshal_pinvoke_cleanup, NULL, NULL, &TypeNameKey_t2985541961_0_0_0 } /* Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey */,
	{ NULL, ResolverContractKey_t3292851287_marshal_pinvoke, ResolverContractKey_t3292851287_marshal_pinvoke_back, ResolverContractKey_t3292851287_marshal_pinvoke_cleanup, NULL, NULL, &ResolverContractKey_t3292851287_0_0_0 } /* Newtonsoft.Json.Serialization.ResolverContractKey */,
	{ NULL, TypeConvertKey_t285306760_marshal_pinvoke, TypeConvertKey_t285306760_marshal_pinvoke_back, TypeConvertKey_t285306760_marshal_pinvoke_cleanup, NULL, NULL, &TypeConvertKey_t285306760_0_0_0 } /* Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey */,
	{ NULL, DateTimeParser_t3754458704_marshal_pinvoke, DateTimeParser_t3754458704_marshal_pinvoke_back, DateTimeParser_t3754458704_marshal_pinvoke_cleanup, NULL, NULL, &DateTimeParser_t3754458704_0_0_0 } /* Newtonsoft.Json.Utilities.DateTimeParser */,
	{ NULL, StringBuffer_t2235727887_marshal_pinvoke, StringBuffer_t2235727887_marshal_pinvoke_back, StringBuffer_t2235727887_marshal_pinvoke_cleanup, NULL, NULL, &StringBuffer_t2235727887_0_0_0 } /* Newtonsoft.Json.Utilities.StringBuffer */,
	{ NULL, StringReference_t2912309144_marshal_pinvoke, StringReference_t2912309144_marshal_pinvoke_back, StringReference_t2912309144_marshal_pinvoke_cleanup, NULL, NULL, &StringReference_t2912309144_0_0_0 } /* Newtonsoft.Json.Utilities.StringReference */,
	{ DelegatePInvokeWrapper_OnTrigger_t4184125570, NULL, NULL, NULL, NULL, NULL, &OnTrigger_t4184125570_0_0_0 } /* UnityEngine.Analytics.EventTrigger/OnTrigger */,
	{ NULL, Example_t2853439971_marshal_pinvoke, Example_t2853439971_marshal_pinvoke_back, Example_t2853439971_marshal_pinvoke_cleanup, NULL, NULL, &Example_t2853439971_0_0_0 } /* ExampleChanger/Example */,
	{ NULL, PolyAuthConfig_t4147939475_marshal_pinvoke, PolyAuthConfig_t4147939475_marshal_pinvoke_back, PolyAuthConfig_t4147939475_marshal_pinvoke_cleanup, NULL, NULL, &PolyAuthConfig_t4147939475_0_0_0 } /* PolyToolkit.PolyAuthConfig */,
	{ NULL, PolyCacheConfig_t1397553534_marshal_pinvoke, PolyCacheConfig_t1397553534_marshal_pinvoke_back, PolyCacheConfig_t1397553534_marshal_pinvoke_cleanup, NULL, NULL, &PolyCacheConfig_t1397553534_0_0_0 } /* PolyToolkit.PolyCacheConfig */,
	{ NULL, PolyImportOptions_t4213423452_marshal_pinvoke, PolyImportOptions_t4213423452_marshal_pinvoke_back, PolyImportOptions_t4213423452_marshal_pinvoke_cleanup, NULL, NULL, &PolyImportOptions_t4213423452_0_0_0 } /* PolyToolkit.PolyImportOptions */,
	{ NULL, PolyStatus_t3145373940_marshal_pinvoke, PolyStatus_t3145373940_marshal_pinvoke_back, PolyStatus_t3145373940_marshal_pinvoke_cleanup, NULL, NULL, &PolyStatus_t3145373940_0_0_0 } /* PolyToolkit.PolyStatus */,
	{ DelegatePInvokeWrapper_GetRawFileDataBytesCallback_t2880187408, NULL, NULL, NULL, NULL, NULL, &GetRawFileDataBytesCallback_t2880187408_0_0_0 } /* PolyToolkitInternal.api_clients.poly_client.PolyClientUtils/GetRawFileDataBytesCallback */,
	{ DelegatePInvokeWrapper_GetRawFileDataTextCallback_t2904487790, NULL, NULL, NULL, NULL, NULL, &GetRawFileDataTextCallback_t2904487790_0_0_0 } /* PolyToolkitInternal.api_clients.poly_client.PolyClientUtils/GetRawFileDataTextCallback */,
	{ DelegatePInvokeWrapper_CacheReadCallback_t2055645553, NULL, NULL, NULL, NULL, NULL, &CacheReadCallback_t2055645553_0_0_0 } /* PolyToolkitInternal.caching.PersistentBlobCache/CacheReadCallback */,
	{ DelegatePInvokeWrapper_CompletionCallback_t3367319633, NULL, NULL, NULL, NULL, NULL, &CompletionCallback_t3367319633_0_0_0 } /* PolyToolkitInternal.client.model.util.WebRequestManager/CompletionCallback */,
	{ DelegatePInvokeWrapper_CreationCallback_t949890767, NULL, NULL, NULL, NULL, NULL, &CreationCallback_t949890767_0_0_0 } /* PolyToolkitInternal.client.model.util.WebRequestManager/CreationCallback */,
	{ NULL, EditTimeImportOptions_t4030780733_marshal_pinvoke, EditTimeImportOptions_t4030780733_marshal_pinvoke_back, EditTimeImportOptions_t4030780733_marshal_pinvoke_cleanup, NULL, NULL, &EditTimeImportOptions_t4030780733_0_0_0 } /* PolyToolkitInternal.EditTimeImportOptions */,
	{ NULL, UnityMaterial_t3949128076_marshal_pinvoke, UnityMaterial_t3949128076_marshal_pinvoke_back, UnityMaterial_t3949128076_marshal_pinvoke_cleanup, NULL, NULL, &UnityMaterial_t3949128076_0_0_0 } /* PolyToolkitInternal.GltfMaterialConverter/UnityMaterial */,
	{ NULL, SaveData_t3667871004_marshal_pinvoke, SaveData_t3667871004_marshal_pinvoke_back, SaveData_t3667871004_marshal_pinvoke_cleanup, NULL, NULL, &SaveData_t3667871004_0_0_0 } /* PolyToolkitInternal.model.export.SaveData */,
	{ NULL, SurfaceShaderMaterial_t2588431549_marshal_pinvoke, SurfaceShaderMaterial_t2588431549_marshal_pinvoke_back, SurfaceShaderMaterial_t2588431549_marshal_pinvoke_cleanup, NULL, NULL, &SurfaceShaderMaterial_t2588431549_0_0_0 } /* PolyToolkitInternal.PtSettings/SurfaceShaderMaterial */,
	{ NULL, SerializableGuid_t3370039383_marshal_pinvoke, SerializableGuid_t3370039383_marshal_pinvoke_back, SerializableGuid_t3370039383_marshal_pinvoke_cleanup, NULL, NULL, &SerializableGuid_t3370039383_0_0_0 } /* TiltBrushToolkit.SerializableGuid */,
	{ DelegatePInvokeWrapper_StringProcessor_t3163369541, NULL, NULL, NULL, NULL, NULL, &StringProcessor_t3163369541_0_0_0 } /* UIInputWait/StringProcessor */,
	{ NULL, ARAnchor_t362826948_marshal_pinvoke, ARAnchor_t362826948_marshal_pinvoke_back, ARAnchor_t362826948_marshal_pinvoke_cleanup, NULL, NULL, &ARAnchor_t362826948_0_0_0 } /* UnityEngine.XR.iOS.ARAnchor */,
	{ DelegatePInvokeWrapper_DictionaryVisitorHandler_t414487210, NULL, NULL, NULL, NULL, NULL, &DictionaryVisitorHandler_t414487210_0_0_0 } /* UnityEngine.XR.iOS.ARFaceAnchor/DictionaryVisitorHandler */,
	{ NULL, ARHitTestResult_t1279023930_marshal_pinvoke, ARHitTestResult_t1279023930_marshal_pinvoke_back, ARHitTestResult_t1279023930_marshal_pinvoke_cleanup, NULL, NULL, &ARHitTestResult_t1279023930_0_0_0 } /* UnityEngine.XR.iOS.ARHitTestResult */,
	{ NULL, ARKitFaceTrackingConfiguration_t386387352_marshal_pinvoke, ARKitFaceTrackingConfiguration_t386387352_marshal_pinvoke_back, ARKitFaceTrackingConfiguration_t386387352_marshal_pinvoke_cleanup, NULL, NULL, &ARKitFaceTrackingConfiguration_t386387352_0_0_0 } /* UnityEngine.XR.iOS.ARKitFaceTrackingConfiguration */,
	{ NULL, ARKitSessionConfiguration_t629136898_marshal_pinvoke, ARKitSessionConfiguration_t629136898_marshal_pinvoke_back, ARKitSessionConfiguration_t629136898_marshal_pinvoke_cleanup, NULL, NULL, &ARKitSessionConfiguration_t629136898_0_0_0 } /* UnityEngine.XR.iOS.ARKitSessionConfiguration */,
	{ NULL, ARKitWorldTrackingSessionConfiguration_t273386347_marshal_pinvoke, ARKitWorldTrackingSessionConfiguration_t273386347_marshal_pinvoke_back, ARKitWorldTrackingSessionConfiguration_t273386347_marshal_pinvoke_cleanup, NULL, NULL, &ARKitWorldTrackingSessionConfiguration_t273386347_0_0_0 } /* UnityEngine.XR.iOS.ARKitWorldTrackingSessionConfiguration */,
	{ NULL, ARPlaneAnchor_t2049372221_marshal_pinvoke, ARPlaneAnchor_t2049372221_marshal_pinvoke_back, ARPlaneAnchor_t2049372221_marshal_pinvoke_cleanup, NULL, NULL, &ARPlaneAnchor_t2049372221_0_0_0 } /* UnityEngine.XR.iOS.ARPlaneAnchor */,
	{ NULL, ARUserAnchor_t1406831531_marshal_pinvoke, ARUserAnchor_t1406831531_marshal_pinvoke_back, ARUserAnchor_t1406831531_marshal_pinvoke_cleanup, NULL, NULL, &ARUserAnchor_t1406831531_0_0_0 } /* UnityEngine.XR.iOS.ARUserAnchor */,
	{ NULL, UnityARCamera_t2069150450_marshal_pinvoke, UnityARCamera_t2069150450_marshal_pinvoke_back, UnityARCamera_t2069150450_marshal_pinvoke_cleanup, NULL, NULL, &UnityARCamera_t2069150450_0_0_0 } /* UnityEngine.XR.iOS.UnityARCamera */,
	{ NULL, UnityARHitTestResult_t4176230866_marshal_pinvoke, UnityARHitTestResult_t4176230866_marshal_pinvoke_back, UnityARHitTestResult_t4176230866_marshal_pinvoke_cleanup, NULL, NULL, &UnityARHitTestResult_t4176230866_0_0_0 } /* UnityEngine.XR.iOS.UnityARHitTestResult */,
	{ NULL, UnityARLightData_t2160616730_marshal_pinvoke, UnityARLightData_t2160616730_marshal_pinvoke_back, UnityARLightData_t2160616730_marshal_pinvoke_cleanup, NULL, NULL, &UnityARLightData_t2160616730_0_0_0 } /* UnityEngine.XR.iOS.UnityARLightData */,
	{ DelegatePInvokeWrapper_ARAnchorAdded_t1608557165, NULL, NULL, NULL, NULL, NULL, &ARAnchorAdded_t1608557165_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorAdded */,
	{ DelegatePInvokeWrapper_ARAnchorRemoved_t4030593004, NULL, NULL, NULL, NULL, NULL, &ARAnchorRemoved_t4030593004_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorRemoved */,
	{ DelegatePInvokeWrapper_ARAnchorUpdated_t3113222537, NULL, NULL, NULL, NULL, NULL, &ARAnchorUpdated_t3113222537_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARAnchorUpdated */,
	{ DelegatePInvokeWrapper_ARSessionCallback_t3772093212, NULL, NULL, NULL, NULL, NULL, &ARSessionCallback_t3772093212_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionCallback */,
	{ DelegatePInvokeWrapper_ARSessionFailed_t2125002991, NULL, NULL, NULL, NULL, NULL, &ARSessionFailed_t2125002991_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARSessionFailed */,
	{ DelegatePInvokeWrapper_ARUserAnchorAdded_t1851120876, NULL, NULL, NULL, NULL, NULL, &ARUserAnchorAdded_t1851120876_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorAdded */,
	{ DelegatePInvokeWrapper_ARUserAnchorRemoved_t23344545, NULL, NULL, NULL, NULL, NULL, &ARUserAnchorRemoved_t23344545_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorRemoved */,
	{ DelegatePInvokeWrapper_ARUserAnchorUpdated_t4007601678, NULL, NULL, NULL, NULL, NULL, &ARUserAnchorUpdated_t4007601678_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/ARUserAnchorUpdated */,
	{ DelegatePInvokeWrapper_internal_ARAnchorAdded_t1565083332, NULL, NULL, NULL, NULL, NULL, &internal_ARAnchorAdded_t1565083332_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorAdded */,
	{ DelegatePInvokeWrapper_internal_ARAnchorRemoved_t3371657877, NULL, NULL, NULL, NULL, NULL, &internal_ARAnchorRemoved_t3371657877_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorRemoved */,
	{ DelegatePInvokeWrapper_internal_ARAnchorUpdated_t2645242205, NULL, NULL, NULL, NULL, NULL, &internal_ARAnchorUpdated_t2645242205_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARAnchorUpdated */,
	{ DelegatePInvokeWrapper_internal_ARFaceAnchorAdded_t1021040265, NULL, NULL, NULL, NULL, NULL, &internal_ARFaceAnchorAdded_t1021040265_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorAdded */,
	{ DelegatePInvokeWrapper_internal_ARFaceAnchorRemoved_t2563439402, NULL, NULL, NULL, NULL, NULL, &internal_ARFaceAnchorRemoved_t2563439402_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorRemoved */,
	{ DelegatePInvokeWrapper_internal_ARFaceAnchorUpdated_t3423900432, NULL, NULL, NULL, NULL, NULL, &internal_ARFaceAnchorUpdated_t3423900432_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFaceAnchorUpdated */,
	{ DelegatePInvokeWrapper_internal_ARFrameUpdate_t3254989823, NULL, NULL, NULL, NULL, NULL, &internal_ARFrameUpdate_t3254989823_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARFrameUpdate */,
	{ DelegatePInvokeWrapper_internal_ARSessionTrackingChanged_t1988849735, NULL, NULL, NULL, NULL, NULL, &internal_ARSessionTrackingChanged_t1988849735_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARSessionTrackingChanged */,
	{ DelegatePInvokeWrapper_internal_ARUserAnchorAdded_t3285282493, NULL, NULL, NULL, NULL, NULL, &internal_ARUserAnchorAdded_t3285282493_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorAdded */,
	{ DelegatePInvokeWrapper_internal_ARUserAnchorRemoved_t386858594, NULL, NULL, NULL, NULL, NULL, &internal_ARUserAnchorRemoved_t386858594_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorRemoved */,
	{ DelegatePInvokeWrapper_internal_ARUserAnchorUpdated_t3964727538, NULL, NULL, NULL, NULL, NULL, &internal_ARUserAnchorUpdated_t3964727538_0_0_0 } /* UnityEngine.XR.iOS.UnityARSessionNativeInterface/internal_ARUserAnchorUpdated */,
	{ DelegatePInvokeWrapper_HandleAssertCallback_t2932343000, NULL, NULL, NULL, NULL, NULL, &HandleAssertCallback_t2932343000_0_0_0 } /* Wrld.AssertHandler/HandleAssertCallback */,
	{ DelegatePInvokeWrapper_CreateThreadDelegate_t2573395908, NULL, NULL, NULL, NULL, NULL, &CreateThreadDelegate_t2573395908_0_0_0 } /* Wrld.Concurrency.ThreadService/CreateThreadDelegate */,
	{ DelegatePInvokeWrapper_JoinThreadDelegate_t1262337526, NULL, NULL, NULL, NULL, NULL, &JoinThreadDelegate_t1262337526_0_0_0 } /* Wrld.Concurrency.ThreadService/JoinThreadDelegate */,
	{ DelegatePInvokeWrapper_ThreadStartDelegate_t2173555516, NULL, NULL, NULL, NULL, NULL, &ThreadStartDelegate_t2173555516_0_0_0 } /* Wrld.Concurrency.ThreadService/ThreadStartDelegate */,
	{ NULL, ConfigParams_t592212086_marshal_pinvoke, ConfigParams_t592212086_marshal_pinvoke_back, ConfigParams_t592212086_marshal_pinvoke_cleanup, NULL, NULL, &ConfigParams_t592212086_0_0_0 } /* Wrld.ConfigParams */,
	{ NULL, CollisionConfig_t3653208188_marshal_pinvoke, CollisionConfig_t3653208188_marshal_pinvoke_back, CollisionConfig_t3653208188_marshal_pinvoke_cleanup, NULL, NULL, &CollisionConfig_t3653208188_0_0_0 } /* Wrld.ConfigParams/CollisionConfig */,
	{ NULL, NativeConfig_t18506486_marshal_pinvoke, NativeConfig_t18506486_marshal_pinvoke_back, NativeConfig_t18506486_marshal_pinvoke_cleanup, NULL, NULL, &NativeConfig_t18506486_0_0_0 } /* Wrld.ConfigParams/NativeConfig */,
	{ DelegatePInvokeWrapper_CameraEventCallback_t1620885257, NULL, NULL, NULL, NULL, NULL, &CameraEventCallback_t1620885257_0_0_0 } /* Wrld.MapCamera.CameraApi/CameraEventCallback */,
	{ NULL, InputFrame_t648463210_marshal_pinvoke, InputFrame_t648463210_marshal_pinvoke_back, InputFrame_t648463210_marshal_pinvoke_cleanup, NULL, NULL, &InputFrame_t648463210_0_0_0 } /* Wrld.MapCamera.CameraInputHandler/InputFrame */,
	{ DelegatePInvokeWrapper_SendActionDelegate_t3936809903, NULL, NULL, NULL, NULL, NULL, &SendActionDelegate_t3936809903_0_0_0 } /* Wrld.MapCamera.CameraInputHandler/SendActionDelegate */,
	{ NULL, CameraState_t2128520787_marshal_pinvoke, CameraState_t2128520787_marshal_pinvoke_back, CameraState_t2128520787_marshal_pinvoke_cleanup, NULL, NULL, &CameraState_t2128520787_0_0_0 } /* Wrld.MapCamera.CameraState */,
	{ DelegatePInvokeWrapper_AddMeshCallback_t1037702503, NULL, NULL, NULL, NULL, NULL, &AddMeshCallback_t1037702503_0_0_0 } /* Wrld.MapGameObjectScene/AddMeshCallback */,
	{ DelegatePInvokeWrapper_DeleteMeshCallback_t4229142016, NULL, NULL, NULL, NULL, NULL, &DeleteMeshCallback_t4229142016_0_0_0 } /* Wrld.MapGameObjectScene/DeleteMeshCallback */,
	{ DelegatePInvokeWrapper_VisibilityCallback_t2021865822, NULL, NULL, NULL, NULL, NULL, &VisibilityCallback_t2021865822_0_0_0 } /* Wrld.MapGameObjectScene/VisibilityCallback */,
	{ NULL, KeyboardData_t2354704333_marshal_pinvoke, KeyboardData_t2354704333_marshal_pinvoke_back, KeyboardData_t2354704333_marshal_pinvoke_cleanup, NULL, NULL, &KeyboardData_t2354704333_0_0_0 } /* Wrld.MapInput.AppInterface.KeyboardData */,
	{ NULL, KeyboardInputEvent_t410871953_marshal_pinvoke, KeyboardInputEvent_t410871953_marshal_pinvoke_back, KeyboardInputEvent_t410871953_marshal_pinvoke_cleanup, NULL, NULL, &KeyboardInputEvent_t410871953_0_0_0 } /* Wrld.MapInput.Mouse.KeyboardInputEvent */,
	{ NULL, TouchInputEvent_t172715690_marshal_pinvoke, TouchInputEvent_t172715690_marshal_pinvoke_back, TouchInputEvent_t172715690_marshal_pinvoke_cleanup, NULL, NULL, &TouchInputEvent_t172715690_0_0_0 } /* Wrld.MapInput.Touch.TouchInputEvent */,
	{ NULL, ApplyTextureToMaterialRequest_t638715235_marshal_pinvoke, ApplyTextureToMaterialRequest_t638715235_marshal_pinvoke_back, ApplyTextureToMaterialRequest_t638715235_marshal_pinvoke_cleanup, NULL, NULL, &ApplyTextureToMaterialRequest_t638715235_0_0_0 } /* Wrld.Materials.MaterialRepository/ApplyTextureToMaterialRequest */,
	{ DelegatePInvokeWrapper_AllocateTextureBufferCallback_t109657048, NULL, NULL, NULL, NULL, NULL, &AllocateTextureBufferCallback_t109657048_0_0_0 } /* Wrld.Materials.TextureLoadHandler/AllocateTextureBufferCallback */,
	{ DelegatePInvokeWrapper_BeginUploadTextureBufferCallback_t3016670851, NULL, NULL, NULL, NULL, NULL, &BeginUploadTextureBufferCallback_t3016670851_0_0_0 } /* Wrld.Materials.TextureLoadHandler/BeginUploadTextureBufferCallback */,
	{ DelegatePInvokeWrapper_ReleaseTextureCallback_t388032167, NULL, NULL, NULL, NULL, NULL, &ReleaseTextureCallback_t388032167_0_0_0 } /* Wrld.Materials.TextureLoadHandler/ReleaseTextureCallback */,
	{ DelegatePInvokeWrapper_AllocateUnpackedMeshCallback_t3754430302, NULL, NULL, NULL, NULL, NULL, &AllocateUnpackedMeshCallback_t3754430302_0_0_0 } /* Wrld.Meshes.MeshUploader/AllocateUnpackedMeshCallback */,
	{ DelegatePInvokeWrapper_UploadUnpackedMeshCallback_t3790783597, NULL, NULL, NULL, NULL, NULL, &UploadUnpackedMeshCallback_t3790783597_0_0_0 } /* Wrld.Meshes.MeshUploader/UploadUnpackedMeshCallback */,
	{ NULL, Building_t1491635829_marshal_pinvoke, Building_t1491635829_marshal_pinvoke_back, Building_t1491635829_marshal_pinvoke_cleanup, NULL, NULL, &Building_t1491635829_0_0_0 } /* Wrld.Resources.Buildings.Building */,
	{ DelegatePInvokeWrapper_BuildingReceivedCallback_t1209487313, NULL, NULL, NULL, NULL, NULL, &BuildingReceivedCallback_t1209487313_0_0_0 } /* Wrld.Resources.Buildings.BuildingsApi/BuildingReceivedCallback */,
	{ NULL, BuildingRequest_t1598655962_marshal_pinvoke, BuildingRequest_t1598655962_marshal_pinvoke_back, BuildingRequest_t1598655962_marshal_pinvoke_cleanup, NULL, NULL, &BuildingRequest_t1598655962_0_0_0 } /* Wrld.Resources.Buildings.BuildingsApi/BuildingRequest */,
	{ NULL, HighlightRequest_t1416441333_marshal_pinvoke, HighlightRequest_t1416441333_marshal_pinvoke_back, HighlightRequest_t1416441333_marshal_pinvoke_cleanup, NULL, NULL, &HighlightRequest_t1416441333_0_0_0 } /* Wrld.Resources.Buildings.BuildingsApi/HighlightRequest */,
	{ DelegatePInvokeWrapper_NativeBuildingReceivedCallback_t2577383538, NULL, NULL, NULL, NULL, NULL, &NativeBuildingReceivedCallback_t2577383538_0_0_0 } /* Wrld.Resources.Buildings.BuildingsApi/NativeBuildingReceivedCallback */,
	{ DelegatePInvokeWrapper_NativeHighlightMeshClearCallback_t813633652, NULL, NULL, NULL, NULL, NULL, &NativeHighlightMeshClearCallback_t813633652_0_0_0 } /* Wrld.Resources.Buildings.BuildingsApi/NativeHighlightMeshClearCallback */,
	{ DelegatePInvokeWrapper_NativeHighlightMeshUploadCallback_t608941254, NULL, NULL, NULL, NULL, NULL, &NativeHighlightMeshUploadCallback_t608941254_0_0_0 } /* Wrld.Resources.Buildings.BuildingsApi/NativeHighlightMeshUploadCallback */,
	{ DelegatePInvokeWrapper_NativeHighlightReceivedCallback_t2749649652, NULL, NULL, NULL, NULL, NULL, &NativeHighlightReceivedCallback_t2749649652_0_0_0 } /* Wrld.Resources.Buildings.BuildingsApi/NativeHighlightReceivedCallback */,
	NULL,
};
